const webpackMerge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const commonConfig = require('./webpack.common.js');
const helpers = require('./helpers');
const config = require('../config.json');
const conf = config.envs[config.default_env];

module.exports = webpackMerge(commonConfig, {
  devtool: 'cheap-module-eval-source-map',

  entry: {
    'app': './src/main.student.ts'
  },

  output: {
    // replace is only for windows!!! FIXME
    path: `${conf.url}/student/`.replace('/', '\\') ,
    publicPath: '/',
    //filename: 'student-[name].[hash].js',
    filename: 'student-[name].js',
    chunkFilename: '[id].[hash].chunk.js'
  },


  plugins: [
    new ExtractTextPlugin('[name].css')
  ],

  devServer: {
    historyApiFallback: true,
    stats: 'minimal'
  }
});