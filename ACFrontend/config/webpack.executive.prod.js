  
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const commonConfig = require('./webpack.common.js');
const helpers = require('./helpers');
const config = require('../config.json');
const conf = config.envs[config.default_env];

const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

console.log('dist root', helpers.root('dist'));
module.exports = webpackMerge(commonConfig, {
  devtool: 'source-map',

  entry: {
    'app': './src/main.executive.ts'
  },

  output: {
    // replace is only for windows FIXME
    path: `${conf.url}/executive/`.replace('/', '\\') ,
    publicPath: '/',
    //filename: 'executive-[name].[hash].js',
    filename: 'executive-[name].js',
    chunkFilename: '[id].[hash].chunk.js'
  },

  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.UglifyJsPlugin({ // https://github.com/angular/angular/issues/10618
      mangle: {
        keep_fnames: true
      }
    }),
    new ExtractTextPlugin('[name].[hash].css'),
    new webpack.DefinePlugin({
      'process.env': {
        'ENV': JSON.stringify(ENV)
      }
    }),
    new webpack.LoaderOptionsPlugin({
      htmlLoader: {
        minimize: false // workaround for ng2
      }
    })
  ]
});