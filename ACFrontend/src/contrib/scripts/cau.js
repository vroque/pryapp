$(function () {
  "use strict";
  /**
   * Event that shows or hides a specific block
   * data-divtoggle => name of block, not is the class
   * data-divtoggleview => name of block to show or hides
   * data-divtoggle == data-divtoggleview, the name of the two must be equal
   * data-class == styles by insert o delete class of element
   * extra is data-divtogglerotate = > is the icon the action with the possibility of rotate
   */
  $('body').on("click", "[data-divtoggle]", function (event) {
    event.preventDefault();
    if ($(this).data("divtogglerotate") === true) {
      $(this).toggleClass("rotate-180");
    }
    if ($(this).data("class") != undefined && $(this).data("class") != '') {
      $(this).toggleClass($(this).data("class"));
    }
    $('[data-divtoggleview="' + $(this).data("divtoggle") + '"]').slideToggle("slow");
  });

  /**
   * Evento para cuando un usuario(administrativo) haga clic en una fila y este sea resaltado
   * esto ayuda al usuario para resaltar la fila y este sea diferenciada de las demás
   */
  $('body').on("click", ".table-selected table tbody tr", function (e) {
    $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
  });


});
