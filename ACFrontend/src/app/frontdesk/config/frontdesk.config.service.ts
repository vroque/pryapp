import { Injectable } from '@angular/core';

@Injectable()
export class FrontdeskConfigService {
    _apiURI: string;
    _list_document_reasons: Array<any>;
    _errors_messages: any;

    constructor() {
        this._apiURI = 'http://localhost:5000/api/';
        this._list_document_reasons = [
          { id: 'Bachiller',
            message: 'Solicito el presente documento para realizar trámites de bachiller.'},
          { id: 'ONP',
            message: 'Solicito el presente documento para realizar trámites en la ONP.'},
          { id: 'Colegiatura',
            message: 'Solicito el presente documento para realizar trámites de Colegiatura.'},
          { id: 'Traslado externo',
            message: 'Solicito el presente documento para realizar trámites de Traslado Externo.'},
          { id: 'Trámite laboral',
            message: 'Solicito el presente documento para realizar trámites Laborales.'},
          { id: 'Trámites para el extranjero',
            message: 'Solicito el presente documento para realizar trámites en el Extranjero.'},
          { id: 'Trámite personal',
            message: 'Solicito el presente documento para realizar trámites personales.'},
          { id: 'Otros',
            message: 'Solicito el presente documento para...'}
        ];
        this._errors_messages = {
          'solicitud_error': 'Lo sentimos tenemos problemas tecnicos, estamos trabajando para resolverlos.',
          'solicitud_issue': 'Lo sentimos tenemos problemas tecnicos, estamos trabajando para resolverlos.'
        };
     }

     listDocumentReasons (): Array<any> {
       return this._list_document_reasons;
     }
     error_messages (): any {
       return this._errors_messages;
     }
}
