import { Component, Input, OnInit } from '@angular/core';
import { DebtService } from '../../shared/services/accounting/debt.service';
import { Requeriment } from '../../shared/services/admision/requeriment';
import { RequerimentService } from '../../shared/services/admision/requeriment.service';
import { Student } from '../../shared/services/academic/student';
import { StudentFactory } from '../../shared/services/academic/student.factory';
import { Debt } from '../../shared/services/accounting/debt';

@Component({
  providers: [RequerimentService, DebtService ],
  selector: 'view-notifications',
  templateUrl: './notifications.component.html'
})
export class ViewNotificationsComponent  implements OnInit {

  @Input()type = 1;
  student: Student;
  requeriments: Requeriment[] = [];
  debts: Debt[] = [];
  notifications = [];

  constructor(
    private requerimentService: RequerimentService,
    private debtService: DebtService,
  ) {}

  ngOnInit(): void {
    this.student =  StudentFactory.getInstance().get();
    this.getpostulantRequeriments();
    this.getDebtStudent();
  }

  /**
   * Obtiene los requerimientos pendientes de un estudiante
   */
  getpostulantRequeriments() {
    const std = {
      college: this.student.profile.college.id,
      department: this.student.profile.department,
      person_id: this.student.person_id,
      program: this.student.profile.program.id,
    };
    this.requerimentService.query(std, false)
      .subscribe(
        (data) => {
          if (data != null) {
            this.requeriments = data;
          }
        },
        (err) => console.log(err)
      );
  }

  /**
   * Obtiene las deudas pendientes de un estudiante
   */
  getDebtStudent() {
    this.debtService.query(this.student.person_id)
      .subscribe(
        (data) => {
          if (data != null) {
            this.debts = data;
          }
        },
        (err) =>  console.log(err)
      );
  }
}
