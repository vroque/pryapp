import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { DebtService } from '../../shared/services/accounting/debt.service';
import { Student } from '../../shared/services/academic/student';
import { StudentFactory } from '../../shared/services/academic/student.factory';

@Component({
  selector: 'restriction-debt-by-term',
  templateUrl: './restriction-debt-by-term.component.html',
  providers: [DebtService]
})
export class RestrictionDebtByTermComponent implements OnChanges {

  student: Student;
  @Input() term = '';
  @Input() validateDebt: boolean;
  @Output() getHaveDebt = new EventEmitter<boolean>();

  constructor(
    private debtService: DebtService,
  ) { }

  ngOnChanges() {
    this.getDebt(this.term);
  }

  getDebt(term: string) {
    this.student = StudentFactory.getInstance().get();
    this.debtService.restrictionDebtbyTerm(term, this.student)
      .subscribe(
        (data) => this.validateDebt = data,
        (err) => {
          this.validateDebt = false;
          console.log(err);
        },
        () => {
          this.getHaveDebt.emit(this.validateDebt);
        }
      );
  }

}
