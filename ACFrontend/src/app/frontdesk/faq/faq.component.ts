import { Component, OnInit } from '@angular/core';
import { stringify } from 'querystring';

/**
 *  Frecuently Answer Question Component
 */
@Component({
  selector: 'faq-frontdesk',
  templateUrl: './faq.component.html',
})
export class FAQComponent implements OnInit {

  faqs: {
    option: number,
    question: string
  }[];
  question = 0;
  filterInput: any;

  constructor() { }

  ngOnInit() {
    this.faqs = [
      {
        option: 1,
        question: '¿Cuál es el horario de atención del Centro de Atención y Soluciones?'
      },
      {
        option: 2,
        question: '¿Qué vías existen para realizar un trámite administrativo o académico?'
      },
      {
        option: 3,
        question: '¿Cómo puedo ver mi boleta de notas?'
      },
      {
        option: 4,
        question: '¿Cómo puedo ver el Simulador de pago?'
      },
      {
        option: 5,
        question: '¿Cómo puedo rellenar la Ficha socioeconómica?'
      },
      {
        option: 6,
        question: '¿Cuáles son los costos de los trámites documentarios?'
      },
      {
        option: 7,
        question: '¿Cuánto es el tiempo de atención de los trámites académicos?'
      },
      {
        option: 8,
        question: '¿Cómo hago seguimiento a los trámites solicitados en el Portal del Estudiante – Servicios Virtuales?'
      },
      {
        option: 9,
        question: '¿Cómo realizó el trámite de otras solicitudes académicas desde el autoservicio?'
      },
      {
        option: 10,
        question: '¿Cómo compruebo si el documento tramitado online es válido?'
      },
      {
        option: 11,
        question: '¿En qué tiempo se procesa la convalidación?'
      },
      {
        option: 12,
        question: '¿Quién evalúa las asignaturas a convalidar?'
      },
      {
        option: 13,
        question: '¿Cuáles son los criterios de convalidación?'
      },
      {
        option: 14,
        question: '¿Se convalidan asignaturas matriculadas y desaprobadas?'
      },
      {
        option: 15,
        question: '¿Cómo verificar las asignaturas convalidadas?'
      },
      {
        option: 16,
        question: '¿Cómo puedo dar seguimiento a la convalidación?'
      },
      {
        option: 17,
        question: '¿Cómo solicitar la Resolución de convalidación?'
      },
      {
        option: 18,
        question: '¿Cómo solicitar desde el Portal del Estudiante - Servicios Virtuales mi examen sustitutorio?'
      }
    ];
  }

  /**
   * Devuelve la pregunta de una opción
   * @param option
   */
  getQuestionFAQ(option: number): string {
    let question: string;
    this.faqs.forEach((f, index) => {
      if (f.option === option) {
        question = option + '. ' + f.question;
      }
    });
    return question;
  }
}
