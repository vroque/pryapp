import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Menu } from '../../shared/menu';
import { MenuService } from '../../shared/menu.service';

@Component({
  selector: 'cash-box',
  templateUrl: './cash-box.component.html',
  providers: [MenuService]
})
export class CashBoxComponent implements OnInit {

  error_message: string;
  menus: Array<Menu>;
  area = '';

  constructor(
    private router: Router,
    private _route: ActivatedRoute,
    private menuService: MenuService

  ) { }

  ngOnInit(): void {
    this._route.params.forEach((params: Params) => {
      this.area = params['area'] || '';
      this.processMenu(this.menuService.query());
    });
  }

  processMenu(menus: Array<Menu>): void {
    if (this.area === '') {
      this.getAreas(menus);
    } else {
      this.getDocuments(menus, this.area);
    }
  }

  getAreas(menus: Array<Menu>): void {
    for (const menu of menus) {
      if (menu.uri === 'caja') {
        this.menus = menu.smenus;
      }
    }
  }

  getDocuments(menus: Array<Menu>, area: string): void {
    for (const menu of menus) {
      if (menu.uri === 'caja') {
        for (const smenu of menu.smenus) {
          if (smenu.uri === area) {
            this.menus = smenu.smenus;
          }
        }
      }
    } // endfor
  }

}
