import { Component, Input, OnInit } from '@angular/core';

import { BackofficeConfigService } from '../../backoffice/config/config.service';
import { ConfigService } from '../../config/config.service';
import { EconomicStatus } from '../../shared/services/accounting/economicStatus';
import { EconomicStatusService } from '../../shared/services/accounting/economicStatus.service';

@Component({
  providers: [BackofficeConfigService],
  selector: 'payment-methods',
  templateUrl: './payment-methods.component.html',
})

export class PaymentMethodsComponent implements OnInit {

  loading = false;
  bancks: Array<{ address: string; banck: string; local: string; type: string; }>;
  @Input() paymentData: EconomicStatus;
  title = 'Formas de Pago';

  constructor(
    private config: BackofficeConfigService,
    private _config: ConfigService,
    private economicStatusService: EconomicStatusService
  ) { }

  ngOnInit(): void {
    this.bancks = [
      {
        address: 'Estacion Ferroviaria De Huancayo, Avenida Ferrocarril, Junin',
        banck: 'BBVA',
        local: 'Real Plaza Huancayo',
        type: 'Agente',
      },
      {
        address: 'Amarilis 110, Huancayo',
        banck: 'BCP',
        local: 'Bodega Doña Feli',
        type: 'Agente',
      },
      {
        address: 'Jirón Antonio Lobato 192-306, Huancayo',
        banck: 'BCP',
        local: 'Don Lucho',
        type: 'Agente',
      },
      {
        address: 'Calle Real 631-639, Huancayo',
        banck: 'BBVA',
        local: 'BBVA Banco Continental',
        type: 'Banco',
      },
    ];
  }

  payment(): void {
    this.loading = true;
    let paymentId: string;
    this.economicStatusService.getPaymentOnlineID(this.paymentData)
      .subscribe(
        (paymentID) => paymentId = paymentID.payment_online_id,
        (err) => this.loading = false,
        () => {
          window.open(`${this.config.PAYMENT_GATEWAY}${paymentId}`, '_blank');
          this.loading = false;
        },
      );
  }

}
