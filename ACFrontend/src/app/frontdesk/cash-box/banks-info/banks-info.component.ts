import { Component, Input, OnInit } from '@angular/core';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { ConfigService } from '../../../config/config.service';


@Component({
  providers: [BackofficeConfigService,],
  selector: 'banks-info',
  templateUrl: './banks-info.component.html'
})
export class BanksInfoComponent implements OnInit {

  constructor( private config: ConfigService ) { }
  ngOnInit() { }

}
