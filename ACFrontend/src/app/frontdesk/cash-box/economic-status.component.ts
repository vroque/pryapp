import { Component, OnInit, trigger } from '@angular/core';

import { BackofficeConfigService } from '../../backoffice/config/config.service';
import { EconomicStatus } from '../../shared/services/accounting/economicStatus';
import { EconomicStatusService } from '../../shared/services/accounting/economicStatus.service';
import { Student } from '../../shared/services/academic/student';
import { StudentFactory } from '../../shared/services/academic/student.factory';

@Component({
  providers: [BackofficeConfigService, EconomicStatusService],
  selector: 'economic-status',
  templateUrl: './economic-status.component.html'
})

export class EconomicStatusComponent implements OnInit {

  shows: {
    debts: boolean;
    debtsAndPayments: boolean,
    debtDetail: boolean;
    paymentMethod: boolean;
    payments: boolean;
  };
  loading = true;
  error_message = '';
  student: Student = StudentFactory.getInstance().get();
  terms: string[] = [];
  term = '----';
  list_accounts_debts: EconomicStatus[] = [];
  list_accounts_enrolment_debts: EconomicStatus[] = [];
  list_accounts_payments: EconomicStatus[] = [];
  list_accounts_enrolment_payments: EconomicStatus[] = [];
  listDocumentReceivable: EconomicStatus[] = [];
  detail_debt: EconomicStatus;
  current_term: string;
  paymentData: EconomicStatus;

  constructor(private config: BackofficeConfigService, private economicStatusService: EconomicStatusService) { }

  ngOnInit() {
    this.getTerms();
    this.shows = {
      debtDetail: false,
      debts: true,
      debtsAndPayments: true,
      paymentMethod: false,
      payments: true,
    };
  }

  /**
   * Obtiene los periodos en los que estuvo el estudiante
   */
  getTerms(): void {
    this.economicStatusService.getTermsEconomicStatus(this.student.person_id)
      .subscribe(
        (terms) => {
          this.terms = terms;
          if (this.terms.length > 0) {
            this.getGroupedDebts(this.terms[0]);
            this.current_term = this.terms[0];
          } else {
            this.error_message = 'Error al obtener períodos de cuentas corrientes';
          }
        },
        (err) => {
          this.error_message = 'Error al obtener períodos de cuentas corrientes';
        },
        () => this.loading = false,
      );
  }

  getGroupedDebts(term: string): void {
    this.shows.debts = false;
    this.shows.payments = false;
    this.current_term = term;
    this.economicStatusService.getGroupedDebtsPayments(term, this.student.person_id, 'debts')
      .subscribe(
        (list) => {
          this.assigmentAccounts(list, term, 1);
          this.shows.debts = true;
        },
        (err) => {
          console.log(err);
          this.error_message = 'Error al obtener Deudas';
        },
        () => {
          this.term = term;
          this.getPayments(term);
        },
      );
  }

  getPayments(term: string): void {
    this.economicStatusService.getGroupedDebtsPayments(term, this.student.person_id, 'payments')
      .subscribe(
        (list) => {
          this.assigmentAccounts(list, term, 2);
          this.shows.payments = true;
        },
        (err) => {
          console.log(err);
          this.error_message = 'Error al obtener Deudas';
        },
        () => this.term = term,
      );
  }

  assigmentAccounts(list: EconomicStatus[], term: string, type: number): void {
    const enrolmentConcepts: string[] = [
      'C00', 'C01', 'DES', 'RMA', 'CAR', 'TL1', 'EXM', 'SME', 'GA1', 'IN1',
    ];

    if (type === 1) {
      this.listDocumentReceivable = list.filter(f => f.documentReceivable === true);
      list = list.filter(f => f.documentReceivable === false);
      this.list_accounts_enrolment_debts = list.filter(
        (debt) => enrolmentConcepts.includes(debt.concept_id) && debt.term.replace('-', '') + '0' === term,
      );

      this.list_accounts_debts = list.filter(
        (debt) => !enrolmentConcepts.includes(debt.concept_id) || debt.term.replace('-', '') + '0' !== term,
      );
      if (this.student.profile.status === 2) {
        this.list_accounts_enrolment_debts.forEach(item => {
          item.mat = true;
        });
        this.list_accounts_debts.forEach(item => {
          item.details_grouped_gebts.forEach(f => {
            if (f.concept_id.trim() === 'C00') {
              item.mat = true;
            }
          });
        });
      }


    } else if (type === 2) {
      const validateReceivable: string[] = [];
      this.listDocumentReceivable.forEach(item => {
        validateReceivable.push(item.div + item.term + item.campus + item.idSeccionC + item.fecInic + item.concept_id);
      });
      list = list.filter(f => validateReceivable.indexOf(f.div + f.term + f.campus + f.idSeccionC + f.fecInic + f.concept_id) < 0);
      this.list_accounts_enrolment_payments = list.filter((payment) => enrolmentConcepts.includes(payment.concept_id));
      this.list_accounts_payments = list.filter((payment) => !enrolmentConcepts.includes(payment.concept_id));
    }
    this.shows.payments = false;
  }

  getTotalList(accounts: EconomicStatus[], type: number): number {
    let total: number = 0;
    for (const item of accounts) {
      if (type === 1) {
        total += item.total;
      }
      if (type === 2) {
        total += item.deposit;
      }
    }
    return total;
  }

  getDateNow(): Date {
    const dateNow: Date = new Date();
    return dateNow;
  }

  compareDates(date1: Date, date2: Date): boolean {
    let value: boolean;
    date1 = new Date(date1);
    date2 = new Date(date2);
    if (date1.getTime() < date2.getTime()) {
      value = true;
    } else {
      value = false;
    }
    return value;
  }

  /**
   * Obtiene una deuda
   * @param data Deuada detallada
   */
  getPaymentOnlineId(data: EconomicStatus): void {
    this.paymentData = data;
    this.updateShow('paymentMethod');
  }

  /**
   * Switch para ver un detalle de una determinada deuda
   * @param debt 
   */
  viewDetailsDebt(debt: EconomicStatus): void {
    this.detail_debt = debt;
    this.updateShow('debtDetail');
  }

  /**
   * Switch para mostrar determinado que mostrar
   * @param name Nombre del campo para mostrar
   */
  updateShow(name: string): void {
    this.shows = {
      debtDetail: false,
      debts: true,
      debtsAndPayments: false,
      paymentMethod: false,
      payments: true,
    };
    this.shows[name] = true;
  }

  /**
   * Imprimir la página
   */
  print(): any {
    window.print();
  }

}
