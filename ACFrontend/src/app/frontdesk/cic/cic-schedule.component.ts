import { BackofficeConfigService } from '../../backoffice/config/config.service';
import { CICOffer } from '../../shared/services/languages-center/cic-offer';
import { CICScheduleService } from '../../shared/services/languages-center/cic-schedule.service';
import { CICStatus } from '../../shared/services/languages-center/cic-status';
import { CICStatusService } from '../../shared/services/languages-center/cic-status.service';
import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../config/config.service';
import { Schedule } from '../../shared/services/academic/schedule';
import { Student } from '../../shared/services/academic/student';
import { StudentFactory } from '../../shared/services/academic/student.factory';
import { CICEnrollmentService } from '../../shared/services/languages-center/enrollment/cic-enrollment.service';
import { Message } from 'primeng/primeng';

@Component({
  providers: [CICScheduleService, CICStatusService, ConfigService, BackofficeConfigService,CICEnrollmentService],
  selector: 'cic-enroll-schedule',
  templateUrl: './cic-schedule.component.html'
})
export class CICScheduleComponent implements OnInit {

  errorMessage: string;
  student: Student;
  offers: CICOffer[];
  currentOffer: CICOffer;
  modalities: any[];
  schedule: Schedule[];
  loadingSchedule = true;
  loadingStatus = true;
  status: CICStatus;
  viewSchedule = false;
  seeEnrollment = false;
  arrayHours: string[] = [];
  timeStart = '23:00';
  timeEnd = '01:00';
  heightDay: string;
  matriculaConfirmado: false;
  primeMsg: Message[];
  runEnrollmentStatus = false;
  successEnrollment = false;
  warningCIC: boolean = false;
  warningCICTitle: string = "";
  warningCICDescription: string ="";
  hideStatus = false;
  alreadyEnrolled = false;
  constructor(
    private cicStatus: CICStatusService,
    private cicScheduleService: CICScheduleService,
    private cicEnrollmentService: CICEnrollmentService,
    private config: ConfigService,
    private st: BackofficeConfigService
  ) { }

  ngOnInit() {
    this.student =  StudentFactory.getInstance().get();
    this.getStatus();
  }

  getStatus() {
    this.loadingStatus = true;
    this.cicStatus.get()
      .subscribe(
        (status) => {
          this.status = status;
          this.warningCIC=false;
          if(this.status.modality=='X'){
            this.loadingStatus = false;    
            this.warningCIC=true;
            this.warningCICTitle='¡Examen de clasificación!';
            this.warningCICDescription='Has dado un examen de clasificación, deberás matricularte de forma presencial.';
            this.hideStatus = true;
            return;
          }
          else if(this.status.enrollment_status=='pagopendiente'){
            this.loadingStatus = false;    
            this.warningCIC=true;
            this.warningCICTitle='¡Atención!';
            this.warningCICDescription='Tienes una prematrícula con pago pendiente.';
            this.showScheduleEnrolled();

          }
          else if(this.status.enrollment_status=='matriculavigente'){
            this.loadingStatus = false;
            this.warningCIC=true;
            this.warningCICTitle='Matrícula vigente';
            this.warningCICDescription='Ya tienes una matrícula registrada.';
            this.showScheduleEnrolled();
            return;
          }
          else if(this.status.enrollment_status=='sinprematriculas'){
            if (this.status != null || this.status !== undefined) {
              this.getOffer();
            }
          }
        },
        (error) => { this.loadingStatus = false;},
      );
  }

  showScheduleEnrolled(){
    this.cicScheduleService.getStudentScheduleLast(this.student.person_id)
    .subscribe(
      (data) => {
        this.alreadyEnrolled = true;
        this.schedule = data.schedule;
        this.currentOffer = data.offer;
        this.seeEnrollment=false;
        this.viewSchedule = true;
        this.getLimitHours(this.schedule);
        this.getStylesToSchedule(this.schedule);
        return;
      },
      (error) =>  {
        this.errorMessage = error;
        this.loadingSchedule = false;
        
      },
      () => this.loadingSchedule = false,
    );
  }

  getOffer() {
    this.cicScheduleService.getOffersByCicle(
        this.student.profile.campus.id,
        this.status.language, // idioma
        this.status.cycle, // ciclo
      ).subscribe(
        (response)  =>  {
          if (response != null || response.length > 0) {
            this.offers = response;
            this.listOffers();
          }
          this.loadingStatus = false;
        },
        (error) =>  {
          this.errorMessage = error;
          this.loadingStatus = false;
        },
      );
  }

  listOffers() {
    const modalities: any[] = this.st.CIC_MODALITIES;
    for (const mod of modalities) {
      mod.offers = this.offers.filter((f) => f.modality === mod.id);
    }
    this.modalities = modalities;
    this.loadingStatus = false;
  }

  getSchedule(offer: CICOffer) {

    this.viewSchedule = true;
    this.loadingSchedule = true;
    this.schedule = null;
    this.currentOffer = offer;
    this.cicScheduleService.getSchedule(offer)
      .subscribe(
      (schedule) => {
          console.log(schedule);
        this.schedule = schedule;
        if(this.schedule.length===undefined)
        {
          this.viewSchedule = false;
          this.loadingSchedule = false;
          this.primeMsg = [{
            detail: 'Sección sin horarios programados.',
            severity: 'warn',
            summary: 'Sin Horarios',
          }];
        }
        else if(offer.teacherID=='N00'){
          this.viewSchedule = false;
          this.loadingSchedule = false;
          this.primeMsg = [{
            detail: 'Sección sin docente asignado.',
            severity: 'warn',
            summary: 'Sin Docente',
          }];
        }
        else{
          this.loadingSchedule = false;
          this.getLimitHours(this.schedule);
          this.getStylesToSchedule(this.schedule);
        } 
        return;
        
      },
      (error) =>  {
        this.errorMessage = error;
        this.loadingSchedule = false;
      },
      );
  }


  getLimitHours(schedule: Schedule[]): void {
    for (const item of schedule) {
      for (const course of item.courses) {
        if (this.transformTime(this.timeStart) > this.transformTime(course.start)) { this.timeStart = course.start; }
        if (this.transformTime(this.timeEnd) < this.transformTime(course.end)) { this.timeEnd = course.end; }
      }
    }
    if (this.transformTime(this.timeStart) % 60 > 30) {
      this.timeStart = Math.trunc(this.transformTime(this.timeStart) / 60) + ':00';
    } else {
      this.timeStart = Math.trunc(this.transformTime(this.timeStart) / 60) + ':00';
    }
    if (this.transformTime(this.timeEnd) % 60 > 30) {
      this.timeEnd = (Math.trunc(this.transformTime(this.timeEnd) / 60) + 1) + ':00';
    } else {
      this.timeEnd = Math.trunc(this.transformTime(this.timeEnd) / 60) + ':30';
    }
    this.getArrayHours();
  }

  /**
   * [getArrayHours description]
   * get arrayHours for the list of hours
   */
  getArrayHours() {
    this.arrayHours=[];// OKO
    for (let time = this.transformTime(this.timeStart); time <= this.transformTime(this.timeEnd); time = time + 30) {
      const hora = (Math.trunc(time / 60) > 9) ? Math.trunc(time / 60) : '0' + Math.trunc(time / 60);
      const minutos = (time % 60 === 30) ? time % 60 : '00';
      this.arrayHours.push(hora + ':' + minutos);
    }
    this.heightDay = (this.arrayHours.length - 1) * 60 - 2 + 'px';
  }

  getStylesToSchedule(schedule: Schedule[]) {
    for (const item of schedule) {
      if (item.courses.length > 0) {
        for (const course of item.courses) {
          course.height = ((this.transformTime(course.end) - this.transformTime(course.start))) * 2 + 'px';
          course.top = (((this.transformTime(course.start) - this.transformTime(this.timeStart))) * 2 - 2) + 'px';
        }
      }
    }
    this.loadingSchedule = false;
  }

  transformTime(time: string): number {
    time = time.replace(/ /g, '');
    const timeArray = time.split(':');
    const timeStamp = parseInt(timeArray[0]) * 60 + parseInt(timeArray[1]);
    return timeStamp;
  }

  returnSectionsList(){
    this.viewSchedule = false;
    this.seeEnrollment = false;
  }
  
  returnSchedule(){
    this.seeEnrollment = false;
  }

  getEnrollmentView(){
    this.matriculaConfirmado = false;
    this.seeEnrollment = true;
  }

  runEnrollment(section: string){
    if(this.matriculaConfirmado){

      var rpta;
      this.runEnrollmentStatus = true;
      this.cicEnrollmentService.postEnrollment(this.student.person_id,section)
      .subscribe(
      (resultado) => {
        rpta=resultado;
        console.log(rpta);
        
        this.runEnrollmentStatus = false;

        if(rpta == 'debedarexamen'){
          this.primeMsg = [{
            detail: 'Has excedido los 3 meses desde tu último ciclo cursado, necesitas tomar el examen de clasificación.',
            severity: 'warn',
            summary: 'Examen de Clasificación',
          }];
        }
        else if(rpta == 'tienedeudas'){
          this.primeMsg = [{
            detail: 'Tienes deudas en el Centro de Idiomas, necesitas cancelarlas para continuar.',
            severity: 'warn',
            summary: 'Deudas Pendientes',
          }];
        }
        else if(rpta == 'docentenoasignado'){
          this.primeMsg = [{
            detail: 'Sección sin docente asignado.',
            severity: 'warn',
            summary: 'Sin docente.',
          }];
        }
        else if(rpta == 'yaprematriculado'){
          this.primeMsg = [{
            detail: 'Ya te encuestas prematriculado.',
            severity: 'warn',
            summary: 'prematriculado',
          }];
        }
        else if(rpta == 'docentenoasignadoseccion'){
          this.primeMsg = [{
            detail: 'Sección sin docente asignado.',
            severity: 'warn',
            summary: 'Sin docente.',
          }];
        }
        else if(rpta == 'ok'){
          this.successEnrollment=true;
          this.matriculaConfirmado = false;
          this.seeEnrollment = false;

          this.primeMsg = [{
            detail: 'Te has prematriculado satisfatoriamente.',
            severity: 'success',
            summary: 'Premtricula realizada.',
          }];
        }
        else{
          alert("no identificado");
        }
      },
      (error) =>  {
          this.errorMessage = error;
          this.loadingSchedule = false;
        },
      );      
    }
    else{
      this.primeMsg = [{
        detail: 'Debes leer los "Términos y condiciones" y confirmar que estás de acuerdo haciendo clic sobre el checkbox.',
        severity: 'warn',
        summary: '¡Términos y condiciones!.',
      }];
    }
  }
}
