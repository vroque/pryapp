import { BackofficeConfigService } from '../../backoffice/config/config.service';
import { CICOffer } from '../../shared/services/languages-center/cic-offer';
import { CICScheduleService } from '../../shared/services/languages-center/cic-schedule.service';
import { CICStatus } from '../../shared/services/languages-center/cic-status';
import { CICStatusService } from '../../shared/services/languages-center/cic-status.service';
import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../config/config.service';
import { Schedule } from '../../shared/services/academic/schedule';
import { Student } from '../../shared/services/academic/student';
import { StudentFactory } from '../../shared/services/academic/student.factory';

@Component({
  providers: [CICScheduleService, CICStatusService, ConfigService, BackofficeConfigService],
  selector: 'cic-my-schedule',
  templateUrl: './cic-my-schedule.component.html'
})
export class CICMyScheduleComponent implements OnInit {

  errorMessage: string;
  student: Student;
  offers: CICOffer[];
  currentOffer: CICOffer;
  modalities: any[];
  schedule: Schedule[];
  loadingSchedule = false;
  loadingStatus = false;
  status: CICStatus;
  viewSchedule = false;
  arrayHours: string[] = [];
  timeStart = '23:00';
  timeEnd = '01:00';
  heightDay: string;

  constructor(
    private cicStatus: CICStatusService,
    private cicScheduleService: CICScheduleService,
    private config: ConfigService,
    private st: BackofficeConfigService
  ) { }

  ngOnInit(): void {
    this.student =  StudentFactory.getInstance().get();
    this.getStatus();
  }

  /**
   * Obtiene el estado
   */
  getStatus(): void {
    this.loadingStatus = true;
    this.cicStatus.get()
      .subscribe(
        (status) => this.status = status,
        (error) =>  this.loadingStatus = false,
        () => {
          this.getSchedule();
          this.loadingStatus = false;
        }
      );
  }

  /**
   * Obtiene el horario
   */
  getSchedule(): void {
    this.loadingSchedule = true;
    this.cicScheduleService.getStudentSchedule(this.student.person_id)
      .subscribe(
        (data) => {
          this.schedule = data.schedule;
          this.currentOffer = data.offer;
        },
        (error) =>  {
          this.errorMessage = error;
          this.loadingSchedule = false;
        },
        () => this.loadingSchedule = false,
      );
  }

  getLimitHours(schedule: Schedule[]): void {
    for (const item of schedule) {
      for (const course of item.courses) {
        if (this.transformTime(this.timeStart) > this.transformTime(course.start)) { this.timeStart = course.start; }
        if (this.transformTime(this.timeEnd) < this.transformTime(course.end)) { this.timeEnd = course.end; }
      }
    }
    if (this.transformTime(this.timeStart) % 60 > 30) {
      this.timeStart = Math.trunc(this.transformTime(this.timeStart) / 60) + ':00';
    } else {
      this.timeStart = Math.trunc(this.transformTime(this.timeStart) / 60) + ':00';
    }
    if (this.transformTime(this.timeEnd) % 60 > 30) {
      this.timeEnd = (Math.trunc(this.transformTime(this.timeEnd) / 60) + 1) + ':00';
    } else {
      this.timeEnd = Math.trunc(this.transformTime(this.timeEnd) / 60) + ':30';
    }
    this.getArrayHours();
  }

  /**
   * [getArrayHours description]
   * get arrayHours for the list of hours
   */
  getArrayHours() {
    for (let time = this.transformTime(this.timeStart); time <= this.transformTime(this.timeEnd); time = time + 30) {
      const hora = (Math.trunc(time / 60) > 9) ? Math.trunc(time / 60) : '0' + Math.trunc(time / 60);
      const minutos = (time % 60 === 30) ? time % 60 : '00';
      this.arrayHours.push(hora + ':' + minutos);
    }
    this.heightDay = (this.arrayHours.length - 1) * 60 - 2 + 'px';
  }

  getStylesToSchedule(schedule: Schedule[]) {
    for (const item of schedule) {
      if (item.courses.length > 0) {
        for (const course of item.courses) {
          course.height = ((this.transformTime(course.end) - this.transformTime(course.start))) * 2 + 'px';
          course.top = (((this.transformTime(course.start) - this.transformTime(this.timeStart))) * 2 - 2) + 'px';
        }
      }
    }
    this.loadingSchedule = false;
  }

  transformTime(time: string): number {
    time = time.replace(/ /g, '');
    const timeArray = time.split(':');
    const timeStamp = parseInt(timeArray[0]) * 60 + parseInt(timeArray[1]);
    return timeStamp;
  }
}
