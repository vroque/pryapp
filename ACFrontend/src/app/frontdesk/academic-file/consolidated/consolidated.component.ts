import { Component, OnInit } from '@angular/core';
import { Course } from '../../../shared/services/academic/course';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { EnrollmentTermService } from '../../../shared/services/academic/enrollmentTerm.service';
import { Callout } from '../../../shared/components/callout/callout';
import { ConsolidatedService } from '../../../shared/services/academic/consolidated';
import { CloseEnrollment } from '../../../shared/services/academic/closeEnrollment';

@Component({
  providers: [EnrollmentTermService, ConsolidatedService],
  selector: 'consolidated-component',
  templateUrl: './consolidated.component.html'
})
export class ConsolidatedComponent implements OnInit {

  student: Student;
  consolidate: { courses: Course[], closeEnrollment: CloseEnrollment };
  periods: string[] = [];
  currentPeriod: string;
  errorPeriods: { error: boolean, data: Callout } = {error: true, data: new Callout};
  loadingPeriods = true;

  errorConsolidate: { error: boolean, data: Callout } =  { error: true, data: new Callout };
  loadingConsolidate = true;
  totalHt = 0;
  totalHp = 0;
  totalCredits = 0;

  constructor(private enrollmentTermService: EnrollmentTermService, private consolidatedService: ConsolidatedService) { }

  ngOnInit() {
    this.student = StudentFactory.getInstance().get();
    this.getAllPeriodsEnrollment();
  }

  /**
   * Ontención de los períodos en los cuales el estudiante estuvo
   */
  getAllPeriodsEnrollment(): void {
    this.enrollmentTermService.getAllTermsEnrollment()
      .subscribe(
        (data) => {
          if (data.length > 0) {
            this.periods = data;
            this.periods = this.periods.filter(f => parseInt(f, 10) >= 201710);
            this.currentPeriod = this.periods[0];
            this.getConsolidatedByPeriod(this.currentPeriod);
            this.loadingPeriods = false;
            this.errorPeriods.error = false;
          } else {
            this.getErrorPeriods();
          }
        },
        (error) => {
          this.getErrorPeriods();
        }
      );
  }

  /**
   * Define el error al no obtener los períodos
   */
  getErrorPeriods(): void {
    this.errorPeriods.data.title = 'Error al obtener los períodos';
    this.errorPeriods.data.type = 'info';
    this.errorPeriods.data.data = [
      'Lo sentimos ocurrio un problema al obtener sus períodos.',
      'Esto podría deberse a un error de red.',
      'Pruebe a consultar dentro de un momento.'];
    this.errorPeriods.error = true;
    this.loadingPeriods = false;
  }

  /**
   * Obtener los cursos llevados en un período y con una carrera
   * @param period período
   */
  getConsolidatedByPeriod(period: string): void {
    this.currentPeriod = period;
    this.loadingConsolidate = true;
    this.consolidate = {courses: [], closeEnrollment: new CloseEnrollment};

    this.consolidatedService.getCconsolidatedByTerm(period)
      .subscribe(
        (data) => {
          this.consolidate = data;
          if (this.consolidate != null && this.consolidate.courses.length > 0) {
            this.calculateTotals(this.consolidate.courses);
            this.consolidate.closeEnrollment.activityDate = new Date(this.consolidate.closeEnrollment.activityDate as any);
            this.errorConsolidate.error = false;
          } else {
            this.getErrorConsolidated();
          }
          this.loadingConsolidate = false;
        },
        (error) => {
          this.getErrorConsolidated();
        }
      );
  }

  /**
   * Define el error al no obtener el consolidado
   */
  getErrorConsolidated(): void {
    this.errorConsolidate.data.title = 'Error al obtener el consolidado para el período ' + this.currentPeriod;
    this.errorConsolidate.data.type = 'info';
    this.errorConsolidate.data.data = [
      'Lo sentimos pero tenemos problemas en obtener el consolidado.',
      'Esto podría deberse a un error de red.',
      'Pruebe a consultar dentro de un momento.'];
    this.errorConsolidate.error = true;
    this.loadingConsolidate = false;
  }

  /**
   * Calculamos los totales
   * @param data consolidado
   */
  calculateTotals(data: Course[]): void {
    this.totalCredits = 0;
    this.totalHp = 0;
    this.totalHt = 0;
    for (const course of data) {
      this.totalCredits = this.totalCredits + course.credits;
      this.totalHp += course.practicalHours;
      this.totalHt += course.theoreticalHours;
    }
  }

}
