import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';
import { Request } from '../../../shared/services/atentioncenter/request';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { StudiesProof } from '../../../shared/services/atentioncenter/studies.proof';
import { StudiesProofService } from '../../../shared/services/atentioncenter/studies.proof.service';

/**
 * Componente para generar Solicitud de constancia de ingreso.
 */
@Component({
  selector: 'graduation-proof',
  templateUrl: './graduationProof.component.html',
  providers: [StudiesProofService, FrontdeskConfigService, ConfigService],
})
export class ReqGraduationProofComponent implements OnInit {

  student: Student;
  error_message: string;
  document: StudiesProof;
  document_stat: string;
  request: Request;
  description: string;
  list_document_reasons: any[];
  seeDetails = false;
  seeDemo = false;
  configServiceFile: any;

  constructor(
    private studiesProofService: StudiesProofService,
    private config: FrontdeskConfigService,
    private configService: ConfigService,
  ) {
    this.error_message = '';
    this.configServiceFile = this.configService;
  }

  ngOnInit() {
    this.student = StudentFactory.getInstance().get();
    this.document_stat = 'pre';
    this.getDocument();
    this.list_document_reasons = this.config.listDocumentReasons();
  }

  getDocument() {
    // 4 es constancia de estudion por carrera culminada
    this.studiesProofService.get(4)
      .subscribe(
        document => this.document = document,
        error => this.error_message = <any>error,
        () => this.document_stat = 'on'
      );
  }

  createDocument(event: Event, document: StudiesProof) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.studiesProofService.create(this.description, 4, [])
      .subscribe(
        request => { this.request = request; this.document_stat = 'post'; },
        error => { this.error_message = this.config.error_messages().solicitud_error; },
        () => { target.disabled = false; }
      );
  }

  setReason(reason: string) {
    this.description = reason;
  }
}
