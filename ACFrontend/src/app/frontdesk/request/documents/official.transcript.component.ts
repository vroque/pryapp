import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';
import { OfficialTranscript } from '../../../shared/services/atentioncenter/official.transcript';
import { OfficialTranscriptService } from '../../../shared/services/atentioncenter/official.transcript.service';
import { Request } from '../../../shared/services/atentioncenter/request';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';

/**
 * Componente para generar solicitud de historial academico.
 */
@Component({
  providers: [OfficialTranscriptService, FrontdeskConfigService, ConfigService],
  selector: 'official-transcript',
  templateUrl: './official.transcript.component.html',
})

export class ReqOfficialTranscriptComponent implements OnInit {

  student: Student;
  error_message: string;
  document: OfficialTranscript;
  document_stat: string;
  request: Request;
  description: string;
  list_document_reasons: any[];
  seeDetails = false;
  seeDemo = false;
  configServiceFile: any;

  constructor(
    private officialTranscriptService: OfficialTranscriptService,
    private config: FrontdeskConfigService,
    private configService: ConfigService,
  ) {
    this.error_message = '';
    this.configServiceFile = this.configService;
  }

  ngOnInit() {
    this.student =  StudentFactory.getInstance().get();
    this.document_stat = 'pre';
    this.getDocument();
    this.list_document_reasons = this.config.listDocumentReasons();
  }

  getDocument() {
    this.officialTranscriptService.get()
      .subscribe(
        (document) => this.document = document,
        (error) => this.error_message = error,
        () => this.document_stat = 'on',
      );
  }

  createDocument(event: Event, document: OfficialTranscript) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.officialTranscriptService.create(this.description)
      .subscribe(
        (request) => { this.request = request; this.document_stat = 'post'; },
        (error) => { this.error_message = this.config.error_messages().solicitud_error; },
        () => target.disabled = false,
      );
  }

  formCost(): number {
    if (this.document.period_list && this.document.period_list.length > 0) {
      return this.document.cost.amount + 1;
    }
    return 1;
  }

  setReason(reason: string) {
    this.description = reason;
  }
}
