import { Component, OnInit } from '@angular/core';
import { Student } from '../../../../shared/services/academic/student';
import { StudentFactory } from '../../../../shared/services/academic/student.factory';
import { DuplicateProofGraduateService } from '../../../../shared/services/atentioncenter/graduates/duplicate.proof.graduate.service';
import { FrontdeskConfigService } from '../../../config/frontdesk.config.service';
import { DuplicateProofGraduate } from '../../../../shared/services/atentioncenter/graduates/duplicate.proof.graduate';
import { Request } from '../../../../shared/services/atentioncenter/request';
import { ConfigService } from '../../../../config/config.service';

@Component({
  selector: 'request-duplicate-proof-graduate',
  templateUrl: './duplicate.proof.graduate.component.html',
  providers: [ DuplicateProofGraduateService, FrontdeskConfigService, ConfigService ]
})

export class RequestDuplicateProofGraduateComponent implements OnInit {
  student: Student;
  errorMessage: string;
  document: DuplicateProofGraduate;
  documentStatus: string;
  listDocumentReasons: any[];
  description: string;
  request: Request;
  loading: Boolean;
  seeDetails = false;
  seeDemo = false;

  constructor( private duplicateProofGraduateService: DuplicateProofGraduateService,
               private config: FrontdeskConfigService, private configService: ConfigService) { }
  ngOnInit(): void {
    this.student =  StudentFactory.getInstance().get();
    this.documentStatus = 'pre';
    this.listDocumentReasons = this.config.listDocumentReasons();
    this.getDocument();
  }

  /**
   * Obtener información de documento
   */
  getDocument(): void {
    this.duplicateProofGraduateService.get()
      .subscribe(
        document => { this.document = document; },
        error =>  this.errorMessage = <any>error,
        () => this.documentStatus = 'on'
      );
  }

  /**
   * Crear Solicitud
   * @param event evento
   */
  createDocument(event: Event): void {
    console.log('set document');
    this.loading = true;
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.duplicateProofGraduateService.create(this.description)
      .subscribe(
        (request) => { this.request = request; this.documentStatus = 'post'; this.loading = false; },
        (error) => { this.errorMessage = this.config.error_messages().solicitud_error; this.loading = false; },
        () => { target.disabled = false; this.loading = false; }
      );
  }
}
