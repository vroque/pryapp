import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';
import { Proof3510Promotional } from '../../../shared/services/atentioncenter/proof.3510.promotional';
import { Proof3510PromotionalService } from '../../../shared/services/atentioncenter/proof.3510.promotional.service';
import { Request } from '../../../shared/services/atentioncenter/request';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';


/**
 * Componente de solicitud de constancia de 3ro 5to y 10mo superior promocional.
 */
@Component({
  selector: 'proof-3510-promotional',
  templateUrl: './proof.3510.promotional.component.html',
  providers: [Proof3510PromotionalService, FrontdeskConfigService, ConfigService],
})
export class ReqProof3510PromotionalComponent implements OnInit {

  student: Student;
  error_message: string;
  document: Proof3510Promotional;
  document_stat: string;
  request: Request;
  description: string;
  list_document_reasons: any[];
  set_period_list: boolean[];
  seeDetails = false;
  seeDemo = false;
  configServiceFile: any;

  constructor(
    private proof3510PromotionalService: Proof3510PromotionalService,
    private config: FrontdeskConfigService,
    private configService: ConfigService,
  ) {
    this.error_message = '';
    this.configServiceFile = this.configService;
  }

  ngOnInit(): void {
    this.student = StudentFactory.getInstance().get();
    this.document_stat = 'pre';
    this.getDocument();
    this.set_period_list = [true];
    this.list_document_reasons = this.config.listDocumentReasons();
  }


  getDocument(): void {
    console.log('get document');
    this.proof3510PromotionalService.get()
      .subscribe(
        (document) => this.document = document,
        (error) => this.error_message = <any>error,
        () => {
          this.document_stat = 'on';
          this.composeOrders();
        },
      );
  }

  createDocument(event: Event, document: Proof3510Promotional): void {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.proof3510PromotionalService.create(this.description)
      .subscribe(
        (request) => { this.request = request; this.document_stat = 'post'; },
        (error) => { this.error_message = this.config.error_messages().solicitud_error; }, // <any>error,
        () => target.disabled = false,
      );
  }

  formCost(): number {
    return this.document.cost.amount + 1;
  }

  composeOrders(): void {
    if (this.document.promo !== null) {
      if (this.document.promo.order === 10) {
        (this.document.promo as any).orders = '3°., 5.° y 10.° superior.';
      }
      if (this.document.promo.order === 5) {
        (this.document.promo as any).orders = '3.° y 5.° superior.';
      }
      if (this.document.promo.order === 3) {
        (this.document.promo as any).orders = '3.° superior.';
      }
    }
  }

  setReason(reason: string): void {
    this.description = reason;
  }
}
