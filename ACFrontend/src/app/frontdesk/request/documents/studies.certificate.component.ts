import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';
import { Request } from '../../../shared/services/atentioncenter/request';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { StudiesCertificate } from '../../../shared/services/atentioncenter/studies.certificate';
import { StudiesCertificateService } from '../../../shared/services/atentioncenter/studies.certificate.service';

@Component({
  providers: [StudiesCertificateService, FrontdeskConfigService, ConfigService],
  selector: 'studies-certificate',
  templateUrl: './studies.certificate.component.html',
})

export class ReqStudiesCertificateComponent implements OnInit {

  student: Student;
  error_message: string;
  document: StudiesCertificate;
  document_stat: string;
  request: Request;
  description: string;
  set_period_list: boolean[];
  list_document_reasons: any[];
  option: string;
  seeDetails = false;
  seeDemo = false;
  configServiceFile: any;

  constructor(
    private studiesCertificateService: StudiesCertificateService,
    private config: FrontdeskConfigService,
    private configService: ConfigService,
  ) {
    this.configServiceFile = this.configService;
  }

  ngOnInit(): void {
    this.student =  StudentFactory.getInstance().get();
    this.error_message = '';
    // this.option = (this.student.profile.status_name === "EGRESADO")? "":"detallado";
    this.set_period_list = [true];
    this.document_stat = 'pre';
    this.getDocument();
    this.list_document_reasons = this.config.listDocumentReasons();
  }

  getDocument(): void {
    console.log('get document');
    this.studiesCertificateService.get()
      .subscribe(
        (document) => { this.document = document; },
        (error) =>  this.error_message = error,
        () => this.document_stat = 'on',
      );
  }

  createDocument(event: Event, document: StudiesCertificate): void {
    console.log('set document');
    const list: any[] = [];
    for (let i = 0; i < this.set_period_list.length; i++) {
        if (this.set_period_list[i]) {
          list.push(this.document.period_list[i]);
        }
    }
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.studiesCertificateService.create(this.description, list)
      .subscribe(
        (request) => { this.request = request; this.document_stat = 'post'; },
        (error) => { this.error_message = this.config.error_messages().solicitud_error; },
        () => target.disabled = false,
      );
  }

  formCost(): number {
    if (this.document.period_list.length === 0 ) {
      return 1;
    }
    let num = 0;
    for (let i = 0; i < this.set_period_list.length; i++) {
        if (this.set_period_list[i]) {
          num++;
        }
    }
    return this.document.cost.amount * num + 1;
  }

  setReason(reason: string): void {
      this.description = reason;
  }
}
