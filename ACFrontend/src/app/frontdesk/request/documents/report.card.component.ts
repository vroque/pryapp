import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';
import { ReportCard } from '../../../shared/services/atentioncenter/report.card';
import { ReportCardService } from '../../../shared/services/atentioncenter/report.card.service';
import { Request } from '../../../shared/services/atentioncenter/request';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';

@Component({
  providers: [ReportCardService, FrontdeskConfigService, ConfigService],
  selector: 'report-card',
  templateUrl: './report.card.component.html',
})

export class ReqReportCardComponent implements OnInit {

  student: Student;
  error_message: string;
  document: ReportCard;
  document_stat: string;
  request: Request;
  description: string;
  list_document_reasons: any[];
  set_term_list: boolean[];
  seeDetails = false;
  seeDemo = false;
  configServiceFile: any;

  constructor(
    private reportCardService: ReportCardService,
    private config: FrontdeskConfigService,
    private configService: ConfigService,
  ) {
    this.error_message = '';
    this.configServiceFile = this.configService;
  }

  ngOnInit(): void {
    this.student =  StudentFactory.getInstance().get();
    this.document_stat = 'pre';
    this.getDocument();
    this.set_term_list = [true];
    this.list_document_reasons = this.config.listDocumentReasons();
  }

  getDocument(): void {
    this.reportCardService.get()
      .subscribe(
        (document) => this.document = document,
        (error) => this.error_message = error,
        () => this.document_stat = 'on',
      );
  }

  createDocument(event: Event, document: ReportCard): void {
    const list: any[] = [];
    for (let i: any = 0; i < this.set_term_list.length; i++) {
      if (this.set_term_list[i]) {
        list.push(this.document.term_list[i]);
      }
    }
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;

    this.reportCardService.create(this.description, list)
      .subscribe(
        (request) => { this.request = request; this.document_stat = 'post'; },
        (error) => { this.error_message = this.config.error_messages().solicitud_error; }, // <any>error,
        () => target.disabled = false,
      );
  }

  formCost(): number {
    if (this.document.term_list.length === 0) {
      return 1;
    } else {
      let num = 0;
      for (let i = 0; i < this.set_term_list.length; i++) {
        if (this.set_term_list[i]) {
          num++;
        }
      }
      return this.document.cost.amount * num + 1;
    }
  }

  setReason(reason: string): void {
      this.description = reason;
  }
}
