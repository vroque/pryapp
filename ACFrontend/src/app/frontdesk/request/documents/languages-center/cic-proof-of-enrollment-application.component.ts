import {Component, OnInit} from "@angular/core";

// tslint:disable-next-line:max-line-length
import {CICProofOfEnrollmentService} from "../../../../shared/services/languages-center/cic-proof-of-enrollment.service";
import {FrontdeskConfigService} from "../../../config/frontdesk.config.service";

import {Student} from "../../../../shared/services/academic/student";
import {StudentFactory} from "../../../../shared/services/academic/student.factory";
import {Request} from "../../../../shared/services/atentioncenter/request";
import {CICProofOfEnrollment} from "../../../../shared/services/languages-center/cic-proof-of-enrollment";

@Component({
  providers: [
    FrontdeskConfigService,
    CICProofOfEnrollmentService,
  ],
  selector: "cic-proof-of-enrollment-application",
  templateUrl: "./cic-proof-of-enrollment-application.component.html",
})
export class CICProofOfEnrollmentApplicationComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  created: boolean;
  errorMessage: string;
  documentReasonsList: any[];

  periodo: string;

  student: Student;
  description: string;

  document: CICProofOfEnrollment;
  request: Request;

  constructor(private frontDeskConfigService: FrontdeskConfigService,
              private proofOfEnrollmentService: CICProofOfEnrollmentService) {
    this.errorMessage = "";
  }

  ngOnInit() {
    this.student = StudentFactory.getInstance().get();
    this.documentReasonsList = this.frontDeskConfigService.listDocumentReasons();
    this.getDocument();
  }

  /**
   * Info básica del docuento
   */
  getDocument(): void {
    this.loading = true;
    this.proofOfEnrollmentService.get()
      .subscribe(
        (document) => this.document = document,
        (error) => {
          this.errorMessage = error as any;
          this.loading = false;
        },
        () => {
          this.loading = false;
          console.log("document => ", this.document);
          console.log("+--------------------------+");
        },
      );
  }

  /**
   * Costo de la solicitud
   * @returns {number}
   */
  formCost(): number {
    if (this.document.cycleList.length === 0) {
      return 1;
    } else {
      return this.document.cost.amount;
    }
  }

  /**
   * Asigna motivo de solicitud.
   * @param {string} reason
   */
  setReason(reason: string): void {
    this.description = reason;
  }

  createRequest(): void {
    this.processing = true;
    console.log("periodo => ", this.periodo);
    console.log("descripción => ", this.description);
    this.proofOfEnrollmentService.save(this.periodo, this.description)
      .subscribe(
        (request) => {
          this.request = request;
          console.log("request => ", this.request);
        },
        (error) => {
          this.created = false;
          this.processing = false;
          this.errorMessage = error as any;
        },
        () => {
          this.created = true;
          this.processing = false;
          console.log("Solicitud registrada con éxito!");
        },
      );
  }
}
