import {Component, OnInit} from "@angular/core";

import {CICDiplomaService} from "../../../../shared/services/languages-center/cic-diploma.service";
import {FrontdeskConfigService} from "../../../config/frontdesk.config.service";

import {Student} from "../../../../shared/services/academic/student";
import {StudentFactory} from "../../../../shared/services/academic/student.factory";
import {Request} from "../../../../shared/services/atentioncenter/request";
import {CICDiploma} from "../../../../shared/services/languages-center/cic-diploma";

@Component({
  providers: [
    FrontdeskConfigService,
    CICDiplomaService,
  ],
  selector: "cic-diplomas-application",
  templateUrl: "./cic-diplomas-application.component.html",
})
export class CICDiplomasApplicationComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  created: boolean;
  errorMessage: string;
  documentReasonsList: any[];

  student: Student;
  nivel: string;
  description: string;

  document: CICDiploma;
  request: Request;

  constructor(private frontDeskConfigService: FrontdeskConfigService,
              private diplomaService: CICDiplomaService) {
    this.errorMessage = "";
  }

  ngOnInit() {
    this.student = StudentFactory.getInstance().get();
    this.nivel = undefined;
    this.documentReasonsList = this.frontDeskConfigService.listDocumentReasons();
    this.getDocument();
  }

  /**
   * Info del documento
   */
  getDocument(): void {
    this.loading = true;
    this.diplomaService.get()
      .subscribe(
        (document) => this.document = document,
        (error) => {
          this.errorMessage = error as any;
          this.loading = false;
        },
        () => {
          this.loading = false;
          console.log("document => ", this.document);
          console.log("+--------------------------+");
        },
      );
  }

  /**
   * Costo de la solicitud
   * @returns {number}
   */
  formCost(): number {
    if (this.document.languageLevels.length === 0) {
      return 1;
    } else {
      return this.document.cost.amount;
    }
  }

  /**
   * Asigna motivo de solicitud.
   * @param {string} reason
   */
  setReason(reason: string): void {
    this.description = reason;
  }

  /**
   * Creación de solicitud de Diploma
   */
  createRequest(): void {
    this.processing = true;
    console.log("nivel => ", this.nivel);
    console.log("description => ", this.description);
    this.diplomaService.save(this.nivel, this.description)
      .subscribe(
        (request) => {
          this.request = request;
          console.log(request);
        },
        (error) => {
          this.created = false;
          this.processing = false;
          this.errorMessage = error as any;
        },
        () => {
          this.created = true;
          this.processing = false;
          console.log("Solicitud Registrada con éxito!");
        },
      );
  }
}
