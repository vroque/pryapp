import { Component, OnInit } from '@angular/core';

// tslint:disable-next-line:max-line-length
import { CICProofOfForeignLanguageService } from '../../../../shared/services/languages-center/cic-proof-of-foreign-language.service';
import { FrontdeskConfigService } from '../../../config/frontdesk.config.service';

import { Student } from '../../../../shared/services/academic/student';
import { StudentFactory } from '../../../../shared/services/academic/student.factory';
import { Request } from '../../../../shared/services/atentioncenter/request';
import { CICProofOfForeignLanguage } from '../../../../shared/services/languages-center/cic-proof-of-foreign-language';

@Component({
  providers: [
    FrontdeskConfigService,
    CICProofOfForeignLanguageService,
  ],
  selector: 'cic-proof-of-foreign-language-application',
  templateUrl: './cic-proof-of-foreign-language-application.component.html',
})
export class CICProofOfForeignLanguageApplicationComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  created: boolean;
  errorMessage: string;
  documentReasonsList: any[];

  student: Student;
  languageId: string;
  description: string;
  cycle: any;

  document: CICProofOfForeignLanguage;
  request: Request;

  constructor(private frontDeskConfigService: FrontdeskConfigService,
              private proofOfForeignLanguageService: CICProofOfForeignLanguageService) {
    this.errorMessage = '';
  }

  ngOnInit() {
    this.student = StudentFactory.getInstance().get();
    this.languageId = undefined;
    this.documentReasonsList = this.frontDeskConfigService.listDocumentReasons();
    this.getDocument();
  }

  /**
   * Info del documento
   */
  getDocument(): void {
    this.loading = true;
    this.proofOfForeignLanguageService.get()
      .subscribe(
        (document) => this.document = document,
        (error) => {
          this.errorMessage = error as any;
          this.loading = false;
        },
        () => {
          this.loading = false;
          console.log('document => ', this.document);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Language info
   */
  getLanguageInfo(): void {
    const langInfo = this.document.languageLevels.filter((i) => i.languageId === this.languageId);
    this.cycle = langInfo[0].lastCycle;
  }

  formCost(): number {
    if (this.document.languageLevels.length === 0) {
      return 1;
    } else {
      return this.document.cost.amount;
    }
  }

  /**
   * Asigna motivo de solicitud.
   * @param {string} reason
   */
  setReason(reason: string): void {
    this.description = reason;
  }

  /**
   * Creación de solicitud Constancia de Idioma Extranjero Uso Externo
   */
  createRequest(): void {
    this.processing = true;
    const type = 'caieExtRequest';
    console.log('languageId => ', this.languageId);
    console.log('description => ', this.description);
    this.proofOfForeignLanguageService.createRequestForCaieExt(type, this.languageId, this.cycle, this.description)
      .subscribe(
        (request) => this.request = request,
        (error) => {
          this.created = false;
          this.processing = false;
          this.errorMessage = error as any;
        },
        () => {
          this.created = true;
          this.processing = false;
          console.log('Solicitud Registrada con éxito!');
        },
      );
  }
}
