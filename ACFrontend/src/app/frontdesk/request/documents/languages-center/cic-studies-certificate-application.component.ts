import {Component, OnInit} from "@angular/core";

// tslint:disable-next-line:max-line-length
import {CICStudiesCertificateService} from "../../../../shared/services/languages-center/cic-studies-certificate.service";
import {FrontdeskConfigService} from "../../../config/frontdesk.config.service";

import {Student} from "../../../../shared/services/academic/student";
import {StudentFactory} from "../../../../shared/services/academic/student.factory";
import {Request} from "../../../../shared/services/atentioncenter/request";
import {CICStudentLanguageRecord} from "../../../../shared/services/languages-center/cic-student-language-record";
import {CICStudiesCertificate} from "../../../../shared/services/languages-center/cic-studies-certificate";

@Component({
  providers: [
    FrontdeskConfigService,
    CICStudiesCertificateService,
  ],
  selector: "cic-studies-certificate-application",
  templateUrl: "./cic-studies-certificate-application.component.html",
})
export class CICStudiesCertificateApplicationComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  created: boolean;
  errorMessage: string;
  documentReasonsList: any[];

  idioma: any;
  ciclos: CICStudentLanguageRecord[];

  student: Student;
  description: string;

  document: CICStudiesCertificate;
  request: Request;

  constructor(private frontDeskConfigService: FrontdeskConfigService,
              private studiesCertificateService: CICStudiesCertificateService) {
    this.errorMessage = "";
  }

  ngOnInit() {
    this.student = StudentFactory.getInstance().get();
    this.documentReasonsList = this.frontDeskConfigService.listDocumentReasons();
    this.getDocument();
  }

  /**
   * Info básica del docuento
   */
  getDocument(): void {
    this.loading = true;
    this.studiesCertificateService.get()
      .subscribe(
        (document) => this.document = document,
        (error) => {
          this.errorMessage = error as any;
          this.loading = false;
        },
        () => {
          this.loading = false;
          console.log("document => ", this.document);
          console.log("+--------------------------+");
        },
      );
  }

  /**
   * Costo de la solicitud
   * @returns {number}
   */
  formCost(): number {
    if (this.document.studiedLanguagesList.length === 0) {
      return 1;
    } else {
      return this.document.cost.amount;
    }
  }

  /**
   * Asigna motivo de solicitud.
   * @param {string} reason
   */
  setReason(reason: string): void {
    this.description = reason;
  }

  createRequest(): void {
    this.processing = true;
    console.log("idioma => ", this.idioma);
    console.log("description => ", this.description);
    this.ciclos = this.document.studiedLanguagesList.filter((f) => f.language.languageId === this.idioma);
    console.log("ciclos", this.ciclos);
    this.studiesCertificateService.save(this.idioma, this.description, this.ciclos)
      .subscribe(
        (request) => {
          this.request = request;
          console.log("request => ", this.request);
        },
        (error) => {
          this.created = false;
          this.processing = false;
          this.errorMessage = error as any;
        },
        () => {
          this.created = true;
          this.processing = false;
          console.log("Solicitud Registrada con éxito!");
        },
      );
  }
}
