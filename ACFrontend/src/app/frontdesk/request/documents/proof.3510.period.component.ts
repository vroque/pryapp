import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { Proof3510Period } from '../../../shared/services/atentioncenter/proof.3510.period';
import { Proof3510PeriodService } from '../../../shared/services/atentioncenter/proof.3510.period.service';
import { Request } from '../../../shared/services/atentioncenter/request';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';

@Component({
  providers: [Proof3510PeriodService, FrontdeskConfigService, ConfigService],
  selector: 'proof-3510-period',
  templateUrl: './proof.3510.period.component.html'
})

export class ReqProof3510PeriodComponent implements OnInit {

  student: Student;
  error_message = '';
  document: Proof3510Period;
  document_stat: string;
  request: Request;
  description: string;
  list_document_reasons: any[];
  set_term_list: boolean[];
  seeDetails = false;
  seeDemo = false;
  configServiceFile: any;

  constructor(
    private config: FrontdeskConfigService,
    private configService: ConfigService,
    private proof3510PeriodService: Proof3510PeriodService
  ) {
    this.configServiceFile = this.configService;
  }

  ngOnInit() {
    this.student =  StudentFactory.getInstance().get();
    this.document_stat = 'pre';
    this.getDocument();
    this.set_term_list = [true];
    this.list_document_reasons = this.config.listDocumentReasons();
  }

  getDocument() {
    this.proof3510PeriodService.get()
      .subscribe(
        (document) => this.document = document,
        (error) => this.error_message = error,
        () => {
          this.document_stat = 'on';
          this.composeOrders();
        },
      );
  }

  createDocument(event: Event, document: Proof3510Period) {
    const list: any[] = [];
    for (let i = 0; i < this.set_term_list.length; i++) {
      if (this.set_term_list[i]) {
          list.push(this.document.term_list[i].term );
      }
    }
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.proof3510PeriodService.create(this.description, list)
      .subscribe(
        (request) => { this.request = request; this.document_stat = 'post'; },
        (error) => { this.error_message = this.config.error_messages().solicitud_error; },
        () => { target.disabled = false; },
      );
  }

  formCost(): number {
    if (this.document.term_list.length === 0) {
      return 1;
    } else {
      let num = 0;
      for (let i = 0; i < this.set_term_list.length; i++) {
        if (this.set_term_list[i]) {
          num++;
        }
      }
      return this.document.cost.amount * num + 1;
    }
  }

  setReason(reason: string) {
    this.description = reason;
  }

  composeOrders() {
    for (const period of this.document.term_list) {
      if (period.order === 10) {
        (period as any).orders = '3.°, 5.° y 10.° superior.';
      }
      if (period.order === 5) {
        (period as any).orders = '3.° y 5.° superior.';
      }
      if (period.order === 3) {
        (period as any).orders = '3.° superior.';
      }
    }
  }
}
