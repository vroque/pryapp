import { Component, OnInit } from '@angular/core';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';
import { Request } from '../../../shared/services/atentioncenter/request';
import { Student } from '../../../shared/services/academic/student';
import { StudentCardDuplicate } from '../../../shared/services/atentioncenter/student.card.duplicate';
import { StudentCardDuplicateService } from '../../../shared/services/atentioncenter/student.card.duplicate.service';
import { StudentFactory } from '../../../shared/services/academic/student.factory';

@Component({
  providers: [ StudentCardDuplicateService, FrontdeskConfigService ],
  selector: 'studetn-card-duplicate',
  templateUrl:  './student.card.duplicate.component.html',
})
export class ReqStudentCardDuplicateComponent implements OnInit {

  student: Student;
  error_message: string;
  document: StudentCardDuplicate;
  document_stat: string;
  request: Request;
  description: string;
  constructor(
    private studentCardDuplicateService: StudentCardDuplicateService,
    private config: FrontdeskConfigService,
  ) {
    this.error_message = '';
  }

  ngOnInit(): void {
    this.student =  StudentFactory.getInstance().get();
    this.document_stat = 'pre';
    this.getDocument();
  }

  getDocument(): void {
    this.studentCardDuplicateService.get()
      .subscribe(
        document => this.document = document,
        error =>  this.error_message = <any>error,
        () => this.document_stat = 'on'
      );
  }

  createDocument(event: Event, document: StudentCardDuplicate): void {
    // console.log("set document");
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.studentCardDuplicateService.create(this.description)
      .subscribe(
        request => { this.request = request; this.document_stat = 'post'; console.log(this.document); },
        error => { this.error_message = this.config.error_messages().solicitud_error; },
        () => { target.disabled = false; }
      );
  }

}
