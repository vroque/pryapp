import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { ProofOfEnrollment } from '../../../shared/services/atentioncenter/proof.of.enrollment';
import { ProofOfEnrollmentService } from '../../../shared/services/atentioncenter/proof.of.enrollment.service';
import { Request } from '../../../shared/services/atentioncenter/request';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';

@Component({
  providers: [ProofOfEnrollmentService, FrontdeskConfigService, ConfigService],
  selector: 'proof-of-enrollment',
  templateUrl: './enrollmentProofWithDates.component.html'
})
export class ReqEnrollmentProofWithDatesComponent implements OnInit {

  student: Student;
  error_message = '';
  document: ProofOfEnrollment;
  document_stat: string;
  request: Request;
  description: string;
  list_document_reasons: any[];
  set_period_list: boolean[];
  seeDetails = false;
  seeDemo = false;
  configServiceFile: any;

  constructor(
    private proofOfEnrollmentService: ProofOfEnrollmentService,
    private config: FrontdeskConfigService,
    private configService: ConfigService,
  ) {
    this.configServiceFile = this.configService;
  }

  ngOnInit() {
    this.student = StudentFactory.getInstance().get();
    this.document_stat = 'pre';
    this.getDocument();
    this.set_period_list = [true];
    this.list_document_reasons = this.config.listDocumentReasons();
  }

  getDocument() {
    this.proofOfEnrollmentService.getWihDates()
      .subscribe(
      (document) => this.document = document,
      (error) => this.error_message = error,
      () => this.document_stat = 'on',
      );
  }

  createDocument(event: Event, document: ProofOfEnrollment) {
    const list: any[] = [];
    for (let i = 0; i < this.set_period_list.length; i++) {
      if (this.set_period_list[i]) {
        list.push(this.document.period_list[i]);
      }
    }
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.proofOfEnrollmentService.createWihDates(this.description, list)
      .subscribe(
      (request) => { this.request = request; this.document_stat = 'post'; },
      (error) => { this.error_message = this.config.error_messages().solicitud_error; },
      () => target.disabled = false,
      );
  }

  formCost(): number {
    if (this.document.period_list.length === 0) {
      return 1;
    } else {
      let num = 0;
      for (let i = 0; i < this.set_period_list.length; i++) {
        if (this.set_period_list[i]) {
          num++;
        }
      }
      return this.document.cost.amount * num + 1;
    }
  }

  setReason(reason: string) {
    this.description = reason;
  }
}
