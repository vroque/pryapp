import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { EntryProof } from '../../../shared/services/atentioncenter/entry.proof';
import { EntryProofService } from '../../../shared/services/atentioncenter/entry.proof.service';
import { Request } from '../../../shared/services/atentioncenter/request';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';

@Component({
  providers: [EntryProofService, FrontdeskConfigService, ConfigService],
  selector: 'entry-proof',
  templateUrl: './entry.proof.component.html'
})
export class ReqEntryProofComponent implements OnInit {

  student: Student;
  errorMessage = '';
  document: EntryProof;
  documentStatus: string;
  request: Request;
  description: string;
  listDocumentReasons: any[];
  seeDetails = false;
  seeDemo = false;
  configServiceFile: any;

  constructor(
    private config: FrontdeskConfigService,
    private configService: ConfigService,
    private entryProofService: EntryProofService
  ) {
    this.configServiceFile = this.configService;
  }

  ngOnInit(): void {
    this.student =  StudentFactory.getInstance().get();
    this.documentStatus = 'pre';
    this.getDocument();
    this.listDocumentReasons = this.config.listDocumentReasons();
  }

  getDocument(): void {
    console.log('get document');
    this.entryProofService.get()
      .subscribe(
        (document) => this.document = document,
        (error) =>  this.errorMessage = error,
        () => this.documentStatus = 'on',
      );
  }

  createDocument(event: Event, document: EntryProof): void {
    console.log('set document');
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.entryProofService.create(this.description)
      .subscribe(
        (request) => { this.request = request; this.documentStatus = 'post'; },
        (error) => { this.errorMessage = this.config.error_messages().solicitud_error; },
        () => { target.disabled = false; }
      );
  }

  setReason(reason: string): void {
      this.description = reason;
  }
}
