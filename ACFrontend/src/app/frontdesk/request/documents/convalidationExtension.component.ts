import { Component, Input, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { Convalidation } from '../../../shared/services/atentioncenter/convalidation';
import { ConvalidationExtentionService } from '../../../shared/services/atentioncenter/convalidationExtension.service';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';
import { RemainingCourseService } from '../../../shared/services/academic/remainingCourse.service';
import { Request } from '../../../shared/services/atentioncenter/request';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { Transfer } from '../../../shared/services/atentioncenter/transfer';

/**
 * Componente para generar Solicitud de Convalidacion
 */
@Component({
  providers: [ConvalidationExtentionService, RemainingCourseService, FrontdeskConfigService],
  selector: 'convalidation-extension',
  styles: ['.text-center { text-align: center; }'],
  templateUrl: './convalidationExtension.component.html'
})
export class ReqConvalidationExtensionComponent implements OnInit {

  @Input()student: Student;
  ampliaciones: any[];
  courseName: string;
  courses: any[];
  description: string;
  document_stat: string;
  document: Convalidation;
  error_message = '';
  list_document_reasons: any[];
  request: Request;
  select_transfer: Transfer;
  set_convalidation_list: boolean[];
  seeDemo = false;
  filter: any;
  configServiceFile: any;

  constructor(
    private configService: ConfigService,
    private config: FrontdeskConfigService,
    private convalidationService: ConvalidationExtentionService,
    private courseService: RemainingCourseService
  ) {
    this.configServiceFile = this.configService;
  }

  ngOnInit() {
    this.listRemainigCourses();
    this.document_stat = 'pre';
    this.set_convalidation_list = [];
    this.getDocument();
    this.list_document_reasons = this.config.listDocumentReasons();
    this.student =  StudentFactory.getInstance().get();
    this.ampliaciones = [];
  }

  listRemainigCourses() {
    this.courseService.list()
      .subscribe(
        (data) => this.courses = data,
        (err) => this.error_message = 'No se pueden obtener las asignaturas',
      );
  }

  getDocument() {
    this.convalidationService.get()
      .subscribe(
        (document) => this.document = document,
        (error) => this.error_message = <any>error,
        () => this.document_stat = 'on'
      );
  }

  createDocument(event: Event, document: Convalidation) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    const list: any[] = [];
    for (const conva of this.ampliaciones) {
      list.push({uc: {id: conva.id, name: conva.name, credits: conva.credits}, ext: conva.course_convalidator});
    }
    this.convalidationService.create(this.description, this.select_transfer, list)
      .subscribe(
        (request) => { this.request = request; this.document_stat = 'post'; },
        (error) => { this.error_message = this.config.error_messages().solicitud_error; },
        () => { target.disabled = false; },
      );
  }

  setReason(reason: string) {
    this.description = reason;
  }

  addCourseConvalidate(course: any[]) {
    this.ampliaciones.push(JSON.parse(JSON.stringify(course)));
  }

  removeCourseConvalidate(ampliacion: any[]) {
    this.ampliaciones = this.ampliaciones.filter(
      (item) => item.id !== ampliacion['id']);
    this.courses.push({id: ampliacion['id'], name: ampliacion['name']});
  }

  validator(): boolean {
    if (this.select_transfer && this.ampliaciones
        && this.ampliaciones.length > 0) {
      return false;
    }
    return true;
  }
}
