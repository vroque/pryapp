import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { Request } from '../../../shared/services/atentioncenter/request';
import { StudiesProof } from '../../../shared/services/atentioncenter/studies.proof';
import { StudiesProofService } from '../../../shared/services/atentioncenter/studies.proof.service';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';

@Component({
  providers: [StudiesProofService, FrontdeskConfigService, ConfigService ],
  selector: 'studies-proof',
  templateUrl: './studiesProof.component.html'
})
export class ReqStudiesProofComponent implements OnInit {

  student: Student;
  errorMessage: string;
  type = 0;
  document: StudiesProof;
  documentStatus: string;
  request: Request;
  description: string;
  setTermList: boolean[];
  listDocumentReasons: any[];
  option: string;
  seeDemo = false;
  seeDetails = false;
  configServiceFile: any;

  constructor(
    private config: FrontdeskConfigService,
    private configService: ConfigService,
    private studiesProofService: StudiesProofService,
  ) {
    this.configServiceFile = this.configService;
  }

  ngOnInit(): void {
    this.student =  StudentFactory.getInstance().get();
    this.setTermList = [true];
    this.documentStatus = 'pre';
    this.listDocumentReasons = this.config.listDocumentReasons();
    this.getDocument();
  }

  getDocument(): void {
    this.studiesProofService.get(this.type)
      .subscribe(
        (document) => this.document = document,
        (error) =>  this.errorMessage = error,
        () => this.documentStatus = 'on'
      );
  }

  createDocument(event: Event, description: string): void {
    const list: any[] = [];
    if (this.type === 2) {
      for (let i = 0; i < this.setTermList.length; i++) {
        if (this.setTermList[i]) {
            list.push(this.document.term_list[i]);
        }
      }
      }
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.studiesProofService.create(this.description, this.type, list)
      .subscribe(
        (request) => { this.request = request; this.documentStatus = 'post'; },
        (error) => { this.errorMessage = this.config.error_messages().solicitud_error; },
        () => { target.disabled = false; }
      );
  }

  setReason(reason: string) {
    this.description = reason;
  }

  formCost(): number {
    if (this.document.term_list.length === 0) {
      return 1;
    } else {
      let num = 0;
      for (let i = 0; i < this.setTermList.length; i++) {
        if (this.setTermList[i]) {
          num++;
        }
      }
      return this.document.cost.amount * num + 1;
    }
  }

}
