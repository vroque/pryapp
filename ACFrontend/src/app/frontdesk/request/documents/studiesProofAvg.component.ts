import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { GPAService } from '../../../shared/services/academic/gpa.service';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { Request } from '../../../shared/services/atentioncenter/request';
import { StudiesProof } from '../../../shared/services/atentioncenter/studies.proof';
import { StudiesProofService } from '../../../shared/services/atentioncenter/studies.proof.service';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';

@Component({
  providers: [StudiesProofService, FrontdeskConfigService, GPAService, ConfigService],
  selector: 'studies-proof',
  templateUrl: './studiesProofAvg.component.html'
})

export class ReqStudiesProofAvgComponent implements OnInit {

  student: Student;
  error_message: string;
  type = 3;
  document: StudiesProof;
  document_stat: string;
  request: Request;
  description: string;
  set_term_list: boolean[];
  list_document_reasons: any[];
  option: string;
  gpa: number;
  seeDetails = false;
  seeDemo = false;
  configServiceFile: any;

  constructor(
    private config: FrontdeskConfigService,
    private configService: ConfigService,
    private gpaService: GPAService,
    private studiesProofService: StudiesProofService
  ) {
    this.configServiceFile = this.configService;
  }

  ngOnInit() {
    this.student =  StudentFactory.getInstance().get();
    this.set_term_list = [true];
    this.document_stat = 'pre';
    this.list_document_reasons = this.config.listDocumentReasons();
    this.getDocument();
  }

  getDocument() {
    this.studiesProofService.get(this.type)
      .subscribe(
        (document) => this.document = document,
        (error) =>  this.error_message = error,
        () => { this.document_stat = 'on'; this.getGPA(); },
      );
  }

  createDocument(event: Event, description: string) {
    const list: any[] = [];
    if (this.type === 2) {
      for (let i = 0; i < this.set_term_list.length; i++) {
        if (this.set_term_list[i]) {
            list.push(this.document.term_list[i]);
        }
      }
      }
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    console.log(list);
    this.studiesProofService.create(this.description, this.type, list)
      .subscribe(
        (request) => { this.request = request; this.document_stat = 'post'; },
        (error) => this.error_message = this.config.error_messages().solicitud_error,
        () => target.disabled = false,
      );
  }

  setReason(reason: string) {
    this.description = reason;
  }

  formCost(): number {
    if (this.document.term_list.length === 0) {
        return 1;
    } else {
      let num = 0;
      for (let i = 0; i < this.set_term_list.length; i++) {
        if (this.set_term_list[i]) {
            num++;
        }
      }
      return this.document.cost.amount * num + 1;
    }
  }

  getGPA() {
    this.gpaService.get()
      .subscribe(
        (gpa) => this.gpa = gpa.gpa || 0,
        (error) => this.error_message = 'no se pudo obtener el promedio.',
      );
  }

}
