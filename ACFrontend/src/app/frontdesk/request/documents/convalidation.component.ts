import { Component, Input, OnInit } from '@angular/core';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { Convalidation } from '../../../shared/services/atentioncenter/convalidation';
import { ConvalidationService } from '../../../shared/services/atentioncenter/convalidation.service';
import { Request } from '../../../shared/services/atentioncenter/request';

import { ConfigService } from '../../../config/config.service';
import { FrontdeskConfigService } from '../../config/frontdesk.config.service';

@Component({
  providers: [ConfigService, ConvalidationService, FrontdeskConfigService],
  selector: 'convalidation-request',
  templateUrl: './convalidation.component.html'
})
export class ReqConvalidationComponent implements OnInit {

  @Input()student: Student;
  description: string;
  document_stat: string;
  document: Convalidation;
  error_message = '';
  list_document_reasons: any[];
  request: Request;
  set_transfers_list: any[];
  seeDemo = false;
  configServiceFile: any;

  constructor(
    private configService: ConfigService,
    private config: FrontdeskConfigService,
    private convalidationService: ConvalidationService,
  ) {
    this.configServiceFile = this.configService;
  }

  ngOnInit() {
    this.student =  StudentFactory.getInstance().get();
    this.set_transfers_list = [true];
    this.document_stat = 'pre';
    this.getDocument();
    this.list_document_reasons = this.config.listDocumentReasons();
  }

  getDocument() {
    this.convalidationService.get()
      .subscribe(
        (document) => this.document = document,
        (error) =>  this.error_message = error,
        () => this.document_stat = 'on',
      );
  }

  createDocument(event: Event, document: Convalidation) {
    const list: any[] = [];
    for (let i = 0; i < this.set_transfers_list.length; i++) {
        if (this.set_transfers_list[i]) {
          list.push(this.document[i]);
        }
    }

    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.convalidationService.create(this.description)
      .subscribe(
        (request) => { this.request = request; this.document_stat = 'post'; },
        (error) => { this.error_message = this.config.error_messages().error; },
        () => { target.disabled = false; },
      );
  }

  setReason(reason: string) {
    this.description = reason;
  }
}
