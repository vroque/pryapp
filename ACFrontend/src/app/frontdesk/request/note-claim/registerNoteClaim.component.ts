import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { OEANoteClaim } from '../../../shared/services/learning-assessment/OEANoteClaim';
import { OEANoteClaimService } from '../../../shared/services/learning-assessment/oeaNoteClaim.service';
import { OEAFileNoteClaimService } from '../../../shared/services/learning-assessment/oeaFileNoteClaim.service';
@Component({
  selector: 'OEA-register-note-claim',
  templateUrl: './registerNoteClaim.component.html',
  providers: [BackofficeConfigService, OEANoteClaimService, OEAFileNoteClaimService]
})

export class OEARegisterNoteClaimComponent implements OnInit {
  loading: boolean;
  msgError: string;
  term: string;
  nrc: string;
  department: string;
  componentId: string;
  subComponentId: string;
  noteClaim: OEANoteClaim;
  files: HTMLInputElement[] = [];

  claimSavedId: number;
  constructor(private backofficeConfigService: BackofficeConfigService, private route: ActivatedRoute,
    private oeaNoteClaimService: OEANoteClaimService, private oeaFileNoteClaimService: OEAFileNoteClaimService,
    private location: Location) { }
  ngOnInit(): void {

    this.route.queryParams.subscribe(params => {
      this.term = params['term'].trim();
      this.nrc = params['nrc'].trim();
      this.department = params['dp'].trim();
      this.componentId = params['cId'].trim();
      this.subComponentId = params['sCId'].trim();
    });
    if (this.term === undefined || this.term == null || this.term.trim() === ''
      || this.nrc === undefined || this.nrc == null || this.nrc.trim() === ''
      || this.department === undefined || this.department == null || this.department.trim() === ''
      || this.componentId === undefined || this.componentId == null || this.componentId.trim() === ''
      || this.subComponentId === undefined || this.subComponentId == null || this.subComponentId.trim() === '') {
      this.noteClaim = <OEANoteClaim>{
        isValid: false,
        msgValid: 'Valores seleccionados incorrectos'
      };
    } else {
      this.getDetailSubject();
    }
  }

  /**
   * Obtener detalles de asignatura
   */
  getDetailSubject(): void {
    this.loading = true;
    this.oeaNoteClaimService.getSubject(this.department, this.term, this.nrc, this.componentId, this.subComponentId)
      .subscribe(
        data => {
          this.noteClaim = data;
          this.noteClaim.type = 'Selecciona una opción';
          this.loading = false;
        },
        err => {
          console.log(err);
          this.loading = false;
        }
      );
  }

  /**
   * Asignar archivo en un array
   * @param event evento del inputo file
   * @param index indice del array
   */
  assignFile(event: any, index: number): void {
    this.files[index] = <HTMLInputElement>event.target.files[0];
    if (this.files[index].size > 3000000) {
      this.files[index] = null;
      alert('Máximo 2mb por archivo.');
    }
  }

  /**
   * Validar si se puede guardar
   */
  validateSave(): boolean {
    let validate = true;
    if (this.noteClaim == null || this.noteClaim === undefined) {
      validate = false;
    }
    /// Validar componentes
    if (this.noteClaim.component == null || this.noteClaim.componentData === ''
      || this.noteClaim.componentData == null) {
      validate = false;
    }
    /// sValidar tipo
    if (this.noteClaim.type == null
      || this.backofficeConfigService.OEA_TYPES_NOTE_CLAIM.filter(f => f.id === this.noteClaim.type).length <= 0) {
      validate = false;
    }
    /// validar motivo
    if (this.noteClaim.reason == null || this.noteClaim.reason.trim().length < 5) {
      validate = false;
    }
    return validate;
  }

  /**
   * Guardar Reclamo
   */
  saveClaim(): void {
    // tslint:disable-next-line:max-line-length
    if (this.validateSave() && confirm('¿Estás seguro de enviar tu reclamo?\nRecuerda que, si no adjuntas evidencia alguna es posible que tu reclamo sea rechazado.')) {
      this.loading = true;
      this.noteClaim.reason = this.noteClaim.reason.trim();
      this.oeaNoteClaimService.saveClaim(this.noteClaim)
        .subscribe(
          (res) => {
            // this.noteClaim = res;
            this.loading = false;
            this.saveAndUploadFile(res.id);

          },
          (err) => {
            console.log(err);
            console.log('antes -->', this.files);
            this.files = this.files.filter(Boolean);
            console.log('Despues -->', this.files);
            this.loading = false;
          }
        );
    }
  }

  /**
   * Subior y registrar archivo
   * @param noteClaimId Id de reclamo
   */
  saveAndUploadFile(noteClaimId: number): void {
    this.loading = true;
    this.files = this.files.filter(Boolean).filter(f => f.name.length > 0 && f.size > 0);
    let count = 0;
    if (this.files.length > 0) {
      this.files.forEach(file => {
        this.oeaFileNoteClaimService.SaveAndSubmitFile(noteClaimId, null, file)
          .subscribe(
            (submit) => {
              console.log(submit);
              count++;
              if (count >= this.files.length) {
                this.claimSavedId = noteClaimId;
              }
            },
            (err) => {
              console.log('Error al subir archivo -> ', err);
              count++;
              if (count >= this.files.length) {
                this.claimSavedId = noteClaimId;
                this.loading = false;
              }
            }
          );
      });
    } else {
      this.claimSavedId = noteClaimId;
    }
  }

  /**
 * que hacer cuando el subcomponente detalle termina de cargar
 * @param event Retorno de componente de detalle de reclamo
 */
  chargueDetailsClaim(event): void {
    if (event[0]) {
      this.msgError = null;
    } else {
      this.msgError = event[1];
    }
    this.loading = false;
  }

}
