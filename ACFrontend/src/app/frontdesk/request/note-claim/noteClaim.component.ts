import { Component, OnInit } from '@angular/core';
import { OEANoteClaim } from '../../../shared/services/learning-assessment/OEANoteClaim';
import { OEANoteClaimService } from '../../../shared/services/learning-assessment/oeaNoteClaim.service';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';

@Component({
  selector: 'oea-student-register-note-claim',
  templateUrl: './noteClaim.component.html',
  providers: [OEANoteClaimService, BackofficeConfigService]
})

export class OEAStudentNoteClaimComponent implements OnInit {
  term: string;
  loading = false;
  noteClaims: OEANoteClaim[];
  claimIdSelected: number;
  constructor(private oeaNoteClaimService: OEANoteClaimService,
    private backofficeConfigService: BackofficeConfigService) { }
  ngOnInit(): void {
  }

  /**
   * Obtener periodo
   * @param event Evento
   */
  getTerm(event: any): void {
    this.term = event;
    this.getClaims();
  }

  /**
   * Obtener reclamos de un periodo
   */
  getClaims(): void {
    this.loading = true;
    this.noteClaims = null;
    this.oeaNoteClaimService.getListNotesClaimStudentByTerm(this.term)
      .subscribe(
        (data) => {
          this.noteClaims = data;
          this.loading = false;
        },
        (err) => {
          console.log(err);
          this.loading = false;
        }
      );
  }

  /**
 * que hacer cuando el subcomponente detalle termina de cargar
 * @param event Retorno de componente de detalle de reclamo
 */
  chargueDetailsClaim(event): void {
    this.loading = false;
  }
}
