import { BackofficeConfigService } from '../../backoffice/config/config.service';
import { Component, Input, OnInit } from '@angular/core';
import { Request } from '../../shared/services/atentioncenter/request';
import { Student } from '../../shared/services/academic/student';
import { RequestService } from '../../shared/services/atentioncenter/request.service';

@Component({
  selector: 'abstract-request',
  templateUrl: './abstract.component.html',
  providers: [BackofficeConfigService, RequestService]
})
export class RequestAbstractComponent {

  errorMessageCancelRequest: string;
  loading: Boolean;

  @Input() student: Student;
  @Input() request: Request;
  @Input() document: string;
  @Input() description: string;
  configFile: any;

  constructor(
    private config: BackofficeConfigService,
    private requestService: RequestService,
  ) {
    this.configFile = this.config;
  }

  cancelRequest(data: Request) {
    this.loading = true;
    this.requestService.cancelRequest(data)
      .subscribe(
        (request) => this.request = request,
        (error) => {
          this.errorMessageCancelRequest = error as any;
          console.log(error);
          this.loading = false;
        },
        () => this.loading = false
      );
  }

}
