import { Component, OnInit } from '@angular/core';
import { OEANrcForSubstituteExamService } from '../../../shared/services/learning-assessment/oeaNrcForSubstituteExam.service';
import { OEANrcForSubstituteExam } from '../../../shared/services/learning-assessment/oeaNrcForSubstituteExam';
import { OEANrcProgrammned } from '../../../shared/services/learning-assessment/oeaNrcProgrammned';
import { OEASubstituteExamProgrammed } from '../../../shared/services/learning-assessment/oeaSubstituteExamProgrammed';
import { OEASubstituteExamProgrammedService } from '../../../shared/services/learning-assessment/oeaSubstituteExamProgrammed.service';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';

@Component({
  selector: 'request-susti',
  templateUrl: './substituteExam.component.html',
  providers: [OEANrcForSubstituteExamService, OEASubstituteExamProgrammedService, BackofficeConfigService]
})
export class SubstituteExamComponent implements OnInit {
  loading: boolean;
  msgError: string;
  listSubjetPending: OEANrcForSubstituteExam[];
  listRequestedSubjet: OEASubstituteExamProgrammed[] = [];
  justification: string;
  constructor(private oeaNrcForSubstituteExamService: OEANrcForSubstituteExamService,
    private oeaSubstituteExamProgrammedService: OEASubstituteExamProgrammedService,
    private backofficeConfigService: BackofficeConfigService) {
  }

  ngOnInit() {
    this.getSubstituteExam();
  }

  /**
   * obtener Nrcs con examen
   */
  getSubstituteExam(): void {
    this.loading = true;
    this.oeaNrcForSubstituteExamService.getByStudent()
      .subscribe(
        data => {
          this.listSubjetPending = data;
          this.msgError = null;
          this.loading = false;
          this.getProgramExam();
        },
        error => {
          // this.msgError = 'Error al obtener asignaturas con examen sustitutorio.';
          console.log(error);
          this.loading = false;
        }
      );
  }

  /**
   * Obtener asignaturas programadas por el estudiante
   */
  getProgramExam(): void {
    this.loading = true;
    this.oeaSubstituteExamProgrammedService.getByStudent()
      .subscribe(
        data => {
          this.listRequestedSubjet = data;
          this.msgError = null;
          this.loading = false;
        },
        error => {
          this.msgError = 'Error al obtener asignaturas programadas.';
          console.log(error);
          this.loading = false;
        }
      );
  }

  /**
   * Seleccionar o deseleccionar programación
   * @param subjet nrc
   * @param programmnedSelected  fecha
   */
  selectSchedule(subjet: OEANrcForSubstituteExam, programmnedSelected: OEANrcProgrammned): void {
    subjet.scheduledSelected = null;
    if (programmnedSelected.selected) {
      subjet.scheduled.forEach(schedule => {
        schedule.selected = false;
      });
      programmnedSelected.selected = false;
    } else {
      if (subjet.absenceNrc != null && subjet.absenceNrc.percentage >= 30 && subjet.department === this.backofficeConfigService.DEPA_REG) {
        alert('Porcentaje de inasistencia en ' + subjet.subjetName + ' sobrepasado.');
      } else if ((this.listSubjetPending.filter(f => f.scheduledSelected != null)
        .filter(s => s.department === this.backofficeConfigService.DEPA_REG).length) +
        (this.listRequestedSubjet.filter(s => s.substituteExam.department === this.backofficeConfigService.DEPA_REG).length) > 1) {
        alert('Usted ya selecciono dos asignaturas presenciales.');
      } else {
        subjet.scheduled.forEach(schedule => {
          schedule.selected = false;
        });
        programmnedSelected.selected = true;
        subjet.scheduledSelected = programmnedSelected;
      }
    }
  }

  /**
   * Funcion para habilitar boton generar
   */
  validateBtnGenerate(): number {
    let result = 0;
    if (this.listSubjetPending != null && this.listSubjetPending.filter(f => f.scheduledSelected != null).length > 0
      && this.justification !== undefined && this.justification.trim().length > 3) {
      result = 1;
    }
    return result;
  }

  /**
   * Solicitar examen sustitutorio
   */
  sendSusti(): void {
    let listSend: OEANrcForSubstituteExam[];
    listSend = this.listSubjetPending.filter(f => f.scheduledSelected != null);
    if (this.validateBtnGenerate() === 0) {
      alert('Selecciones asignatura e ingrese justificación.');
    } else if ((listSend.filter(s => s.department === this.backofficeConfigService.DEPA_REG).length
      + this.listRequestedSubjet.filter(s => s.substituteExam.department === this.backofficeConfigService.DEPA_REG).length) > 2) {
      alert('Usted ya selecciono más de dos asignaturas presenciales.');
    } else {
      this.loading = true;
      this.oeaSubstituteExamProgrammedService.saveProgrambyStudent(listSend, this.justification)
        .subscribe(
          data => {
            console.log(data);
            this.msgError = null;
            this.loading = false;
            this.justification = '';
            this.getSubstituteExam();
          },
          error => {
            this.msgError = 'Error al guardar asignaturas programadas, vuélvelo a intentar en unos minutos.';
            console.log(error);
            this.loading = false;
          }
        );
    }
  }
}
