import { DOCUMENT } from "@angular/common";
import { Component, Inject, OnInit } from "@angular/core";

@Component({
  selector: "update-profile-photo",
  templateUrl: "./updateProfilePhoto.component.html",
})
export class UpdateProfilePhotoComponent implements OnInit {
  link: string;

  constructor(@Inject(DOCUMENT) private document: any) {
    this.link = "https://campusvirtual.continental.edu.pe/ucregular/IAL/UCCI/ActualizarFoto.aspx";
  }

  ngOnInit() {
    this.document.location.href = this.link;
  }
}
