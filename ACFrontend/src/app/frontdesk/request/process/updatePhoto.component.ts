import { Component, OnInit } from "@angular/core";

import { ConfigService } from "../../../config/config.service";
import { Student } from "../../../shared/services/academic/student";
import { StudentFactory } from "../../../shared/services/academic/student.factory";

@Component({
  selector: "student-update-photo",
  templateUrl: "./updatePhoto.component.html",
  providers: [ConfigService],
})

export class StudentUpdatePhoto  implements OnInit {

  load: boolean = false;
  error_message: string;
  student: Student =  StudentFactory.getInstance().get();

  constructor(private config: ConfigService) {}
  ngOnInit(): void {
    console.log(this.student);
  }
}
