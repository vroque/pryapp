import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Incident } from '../../../shared/services/incident/incident';
import { IncidentCategory } from '../../../shared/services/incident/incident-category/incident-category';
import { IncidentCategoryService } from '../../../shared/services/incident/incident-category/incident-category.service';
import { IncidentRequestSource } from '../../../shared/services/incident/incident-request-source/incident-request-source';
import { IncidentService } from '../../../shared/services/incident/incident.service';
import { IncidentType } from '../../../shared/services/incident/incident-type/incident-type';
import { IncidentTypeService } from '../../../shared/services/incident/incident-type/incident-type.service';
import { takeUntil } from 'rxjs/operator/takeUntil';
declare const $: any;
@Component({
  selector: 'register-incident-student',
  templateUrl: './register.incident.component.html',
  providers: [IncidentTypeService, IncidentCategoryService, IncidentService]
})

export class RegisterIncidentStudentComponent implements OnInit {
  @Input() redirect = true;
  loading: boolean;
  processing: boolean;
  msgError: string;
  saved: boolean;

  incidentSave: Incident;
  incidentTypes: IncidentType[];
  incidentCategory: IncidentCategory[];

  constructor(private incidentTypeService: IncidentTypeService, private incidentCategoryService: IncidentCategoryService,
              private incidentService: IncidentService, private location: Location, private router: Router) {
  }

  ngOnInit(): void {
    this.getIncidentTypes();
  }

  /**
   * Obtener tipos de incidentes
   */
  getIncidentTypes(): void {
    this.loading = true;
    this.incidentTypeService.getAllIncidentType()
      .subscribe(
        (types) => {
          this.incidentTypes = types;
          this.msgError = null;
          this.loading = false;
          this.getIncidentCategory();
        },
        (err) => {
          console.log(err);
          this.msgError = 'Error al obtener tipos de incidentes.';
          this.loading = false;
        }
      );
  }

  /**
   * Obtener categorias de incidentes
   */
  getIncidentCategory(): void {
    this.loading = true;
    this.incidentCategoryService.getAllIncidentCategory()
      .subscribe(
        (categorys) => {
          this.incidentCategory = categorys;
          this.incidentSave = new Incident();
          this.msgError = null;
          this.loading = false;
        },
        (err) => {
          console.log(err);
          this.msgError = 'Error al obtener categorias de incidentes.';
          this.loading = false;
        }
      );
  }

  /**
   * Validar si se debe guardar
   */
  validateSave(): boolean {
    let isValid = true;
    if (this.incidentSave == null) {
      isValid = false;
    }
    if (this.incidentSave.title == null || this.incidentSave.title.trim().length <= 0) {
      isValid = false;
    }
    if (this.incidentSave.typeId == null || this.incidentSave.typeId <= 0) {
      isValid = false;
    }
    if (this.incidentSave.description == null || this.incidentSave.description.trim().length <= 0) {
      isValid = false;
    }
    return isValid;
  }

  /**
   * Guardar incidente
   */
  save(): void {
    if (this.validateSave()) {
      this.incidentSave.title = this.incidentSave.title.trim();
      this.incidentSave.description = this.incidentSave.description.trim();
      this.incidentSave.assignedToTeamId = '00000000';
      this.incidentSave.requestSource = IncidentRequestSource.WEB;
      this.processing = true;
      this.incidentService.save(this.incidentSave)
        .subscribe(
          (data) => {
            this.incidentSave = data;
            this.msgError = null;
            this.saved = true;
          },
          (err) => {
            console.log(err);
            this.msgError = 'Error al guardar incidente.';
            this.processing = false;
          },
          () => this.goToIncidents()
        );
    }
  }

  /**
   * Go to incident list
   */
  goToIncidents(): void {
    const pathName: string[] = this.location.path().split('/');
    if (pathName.length >= 3 && this.redirect) {
      const link: string [] = [`${pathName[1]}/${pathName[2]}/historial`];
      this.router.navigate(link);
    } else {
      this.incidentSave.title = '';
      this.incidentSave.typeId = null;
      this.incidentSave.description = '';
      this.loading = false;
      this.processing = false;
      // $('.modal').modal('hide');
      // console.log('pathName: ', pathName);
    }
  }
}
