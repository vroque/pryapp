import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { FunctionalUnitService } from '../../../../shared/services/adryan/functional-unit/functional-unit.service';
import { Incident } from '../../../../shared/services/incident/incident';
import { IncidentCategory } from '../../../../shared/services/incident/incident-category/incident-category';
import { IncidentCategoryService } from '../../../../shared/services/incident/incident-category/incident-category.service';
import { IncidentLog } from '../../../../shared/services/incident/incident-log/incident-log';
import { IncidentLogService } from '../../../../shared/services/incident/incident-log/incident-log.service';
import { IncidentService } from '../../../../shared/services/incident/incident.service';
import { IncidentStatus } from '../../../../shared/services/incident/incident-status/incident-status';
import { IncidentStatusService } from '../../../../shared/services/incident/incident-status/incident-status.service';
import { IncidentSubcategory } from '../../../../shared/services/incident/incident-subcategory/incident-subcategory';
import { IncidentSubcategoryService } from '../../../../shared/services/incident/incident-subcategory/incident-subcategory.service';
import { IncidentType } from '../../../../shared/services/incident/incident-type/incident-type';
import { IncidentTypeService } from '../../../../shared/services/incident/incident-type/incident-type.service';
import { MomentService } from '../../../../shared/moment.service';

@Component({
  selector: 'incident-info',
  templateUrl: './incident-info.component.html',
  providers: [
    FunctionalUnitService, IncidentLogService, IncidentService, IncidentStatusService,
    IncidentTypeService, IncidentTypeService, IncidentCategoryService, IncidentSubcategoryService, MomentService
  ]
})
export class IncidentInfoComponent implements OnInit {
  loading: boolean;
  incident: Incident;
  incidentStatus: IncidentStatus;
  incidentType: IncidentType;
  incidentLogList: IncidentLog[] = [];
  lastActivity: Date;

  incidentParentCategory: IncidentCategory;
  incidentCategory: IncidentCategory;
  incidentSubcategory: IncidentSubcategory;

  constructor(private activatedRoute: ActivatedRoute, private incidentService: IncidentService,
              private moment: MomentService, private incidentStatusService: IncidentStatusService,
              private incidentTypeService: IncidentTypeService, private incidentLogService: IncidentLogService,
              private incidentCategoryService: IncidentCategoryService,
              private incidentSubcategoryService: IncidentSubcategoryService) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.forEach((params: Params) => {
      const incidentId: number = (<any>params).incident;
      this.getIncident(incidentId);
    });
  }

  /**
   * Get incident info
   * @param incidentId
   */
  getIncident(incidentId: number) {
    this.loading = true;
    this.incidentService.getById(incidentId)
      .subscribe(
        (result) => this.incident = result,
        (error) => {
          console.log('error => ', error);
          this.loading = false;
        },
        () => {
          this.loading = false;

          if (this.incident !== null) {
            this.refreshIncident(this.incident);
          }

        }
      );
  }

  /**
   * Refresh incident info
   * @param incident
   */
  refreshIncident(incident: Incident): void {
    this.getIncidentLog(incident.id);
    this.getIncidentStatusById(incident.statusId);
    this.getIncidentTypeById(incident.typeId);

    if (incident.categoryId !== null) {
      this.getIncidentCategory(incident.categoryId);
    }

    if (incident.subCategoryId !== null) {
      this.getIncidentSubcategory(incident.subCategoryId);
    }
  }

  /**
   * Get incident log
   * @param incidentId
   */
  getIncidentLog(incidentId: number) {
    this.incidentLogService.getAllIncidentLogByIncidentId(incidentId)
      .subscribe(
        (result) => this.incidentLogList = result,
        (error) => console.log('error => ', error),
        () => {
          if (this.incidentLogList.length > 0) {
            this.lastActivity = this.incidentLogList[this.incidentLogList.length - 1].publicationDate;
          } else {
            this.lastActivity = this.incident.publicationDate;
          }
        }
      );
  }

  /**
   * Get incident status info
   * @param incidentStatusId
   */
  getIncidentStatusById(incidentStatusId: number): void {
    this.incidentStatusService.getById(incidentStatusId)
      .subscribe(
        (result) => this.incidentStatus = result,
        (error) => console.log('error => ', error)
      );
  }

  /**
   * Get incident type info
   * @param incidentTypeId
   */
  getIncidentTypeById(incidentTypeId: number): void {
    this.incidentTypeService.getById(incidentTypeId)
      .subscribe(
        (result) => this.incidentType = result,
        (error) => console.log('error => ', error)
      );
  }

  /**
   * get incident category information
   * @param categoryId
   */
  getIncidentCategory(categoryId: number): void {
    this.incidentCategoryService.getById(categoryId)
      .subscribe(
        (result) => this.incidentCategory = result,
        (error) => console.log('error => ', error),
        () => {
          if (this.incidentCategory.parentId !== null) {
            this.getIncidentParentCategory(this.incidentCategory.parentId);
          }
        }
      );
  }

  /**
   * Get incident parent category information
   * @param categoryId
   */
  getIncidentParentCategory(categoryId: number): void {
    this.incidentCategoryService.getById(categoryId)
      .subscribe(
        (result) => this.incidentParentCategory = result,
        (error) => console.log('error => ', error)
      );
  }

  /**
   * get incident subcategory information
   * @param subcategoryId
   */
  getIncidentSubcategory(subcategoryId: number): void {
    this.incidentSubcategoryService.getById(subcategoryId)
      .subscribe(
        (result) => this.incidentSubcategory = result
      );
  }

  /**
   * Tiempo relativo a partir de una fecha tipo YYYYMMDD
   * p.e.: momento("20101225") retorna un string: "hace X años"
   * @param {string} date
   * @returns {string}
   */
  momento(date: string): string {
    return this.moment.fromNow(date);
  }
}
