import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Incident } from '../../../../shared/services/incident/incident';
import { IncidentStatus } from '../../../../shared/services/incident/incident-status/incident-status';
import { IncidentType } from '../../../../shared/services/incident/incident-type/incident-type';
import { IncidentTypeService } from '../../../../shared/services/incident/incident-type/incident-type.service';
import { SearchIncidentService } from '../../../../shared/services/incident/search-incident.service';
import { MomentService } from '../../../../shared/moment.service';
import { IncidentStatusService } from '../../../../shared/services/incident/incident-status/incident-status.service';
import { UtilService } from '../../../../shared/util.service';

@Component({
  selector: 'incident-history',
  templateUrl: './incident-history.component.html',
  providers: [MomentService, SearchIncidentService, UtilService, IncidentTypeService, IncidentStatusService]
})
export class IncidentHistoryComponent implements OnInit {
  loading: boolean;

  myIncidents: Incident[] = [];
  iOpenData: Incident[];
  iInProgressData: Incident[];
  iSolvedData: Incident[];
  iRejectedData: Incident[];
  iCancelledData: Incident[];
  iClosedData: Incident[];

  incidentActiveView: Incident[];
  inbox: string;

  incidentTypes: IncidentType[];
  incidentStatuses: IncidentStatus[];

  constructor(private incidentStatusService: IncidentStatusService,
              private incidentTypeService: IncidentTypeService,
              private searchIncidentService: SearchIncidentService, private utilService: UtilService,
              private moment: MomentService, private location: Location, private router: Router) {
    this.inbox = 'iInProgress';
  }

  ngOnInit(): void {
    this.getIncidentStatuses();
  }

  setInbox(type: string): void {
    this.inbox = type;
    switch (type) {
      case ('iOpen'):
        this.incidentActiveView = this.iOpenData;
        break;
      case ('iInProgress'):
        this.incidentActiveView = this.iInProgressData;
        break;
      case ('iSolved'):
        this.incidentActiveView = this.iSolvedData;
        break;
      case ('iRejected'):
        this.incidentActiveView = this.iRejectedData;
        break;
      case ('iCancelled'):
        this.incidentActiveView = this.iCancelledData;
        break;
      case ('iClosed'):
        this.incidentActiveView = this.iClosedData;
        break;
    }
  }

  /**
   * Get all incident statuses
   */
  getIncidentStatuses(): void {
    this.loading = true;
    this.incidentStatusService.getAllIncidentStatus()
      .subscribe(
        (result) => this.incidentStatuses = result,
        (error) => {
          console.log('error => ', error);
          this.loading = false;
        },
        () => {
          console.log('incident statuses => ', this.incidentStatuses);
          this.getIncidentTypes();
        }
      );
  }

  /**
   * Get incident status name
   * @param id Incident status id
   */
  getIncidentStatusName(id: number): string {
    for (const item of this.incidentStatuses) {
      if (item.id === id) {
        return item.name;
      }
    }
  }

  /**
   * Get all incident types
   */
  getIncidentTypes(): void {
    this.incidentTypeService.getAllIncidentType()
      .subscribe(
        (result) => this.incidentTypes = result,
        (error) => console.log('error => ', error),
        () => {
          console.log('incident types => ', this.incidentTypes);
          this.getIncidentsReportedByStudent();
        }
      );
  }

  /**
   * Get incident type name
   * @param id Incident type id
   */
  getIncidentTypeName(id: number): string {
    for (const item of this.incidentTypes) {
      if (item.id === id) {
        return item.name;
      }
    }
  }

  /**
   * Get incidents reported by a student
   */
  getIncidentsReportedByStudent(): void {
    this.loading = true;
    this.searchIncidentService.getByStudent()
      .subscribe(
        (result) => this.myIncidents = result,
        (error) => {
          console.log('error => ', error);
          this.loading = false;
        },
        () => {
          this.loading = false;
          this.iOpenData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.OPEN);
          this.iInProgressData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.IN_PROGRESS);
          this.iSolvedData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.SOLVED);
          this.iRejectedData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.REJECTED);
          this.iCancelledData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.CANCELLED);
          this.iClosedData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.CLOSED);
          this.incidentActiveView = this.iInProgressData;
        }
      );
  }

  /**
   * Tiempo relativo a partir de una fecha tipo YYYYMMDD
   * p.e.: momento("20101225") retorna un string: "hace X años"
   * @param {string} date
   * @returns {string}
   */
  momento(date: string): string {
    return this.moment.fromNow(date);
  }

  /**
   * Go to incident detail
   * @param incidentId
   */
  gotoIncidentDetail(incidentId: number): void {
    const pathName: string[] = this.location.path().split('/');
    if (pathName.length >= 3) {
      const link: string[] = [`${pathName[1]}/${pathName[2]}/${pathName[3]}/${incidentId}`];
      this.router.navigate(link);
    } else {
      console.log('pathName: ', pathName);
    }
  }

  /**
   * Go to form to create a new incident
   */
  gotoCreateNewIncident(): void {
    const pathName: string[] = this.location.path().split('/');
    if (pathName.length >= 3) {
      const link: string[] = [`${pathName[1]}/${pathName[2]}/registrar`];
      this.router.navigate(link);
    } else {
      console.log('pathName: ', pathName);
    }
  }
}
