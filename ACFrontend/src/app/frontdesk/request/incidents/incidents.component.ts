import { Component, OnInit } from '@angular/core';

import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { Menu } from '../../../shared/menu';
import { MenuService } from '../../../shared/menu.service';


@Component({
  selector: 'incident-student',
  templateUrl: './incidents.component.html',
  providers: [MenuService, BackofficeConfigService]
})

export class IncidentStudentComponent implements OnInit {
  menus: Menu[];

  constructor(private config: BackofficeConfigService, private menuService: MenuService) {
  }

  ngOnInit(): void {
    this.menus = this.config.getRouteOfNameAndUri('Incidentes', ['incidentes'], this.menuService);
  }
}
