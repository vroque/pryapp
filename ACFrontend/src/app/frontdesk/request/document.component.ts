import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router }   from "@angular/router";

import { Debt } from "../../shared/services/accounting/debt";
import { Requeriment } from "../../shared/services/admision/requeriment";
import { Student } from "../../shared/services/academic/student";

import { DebtService } from "../../shared/services/accounting/debt.service";
import { RequerimentService } from "../../shared/services/admision/requeriment.service";
import { StudentFactory } from "../../shared/services/academic/student.factory";

@Component({
  selector: "request-document",
  templateUrl: "./document.component.html",
  providers: [DebtService, RequerimentService],
  // viewProviders: [StudentFactory]
})

export class RequestDocumentComponent implements OnInit {
  area: string;
  document: string;
  error_message: string;
  debts: Debt[];
  requeriments: Requeriment[];
  student: Student;

  constructor(
    private router: Router,
    private _route: ActivatedRoute,
    private debtService: DebtService,
    private requerimentService: RequerimentService,
    // private studentService: StudentFactory
  ) {
      this.debts = new Array<Debt>();
      this.requeriments = new Array<Requeriment>();
      // this.studentService.subscribe( st => this.student = st);
    }

  ngOnInit(): void {
    this.student = null;
    this.student =  StudentFactory.getInstance().get();
    console.log("Document Component", this.student);
    this._route.params.forEach((params: Params) => {
      console.log("params", params);
      this.area = params["area"];
      this.document = params["document"];
    });
    this.getDebts();
    this.getRequeriments();
    // this.student = this.studentService.get();
  }

  getDebts(): void {
    this.debtService.query(this.student.person_id)
      .subscribe(
        debts => this.debts = debts,
        error =>  this.error_message = <any>error,
        () => console.log("debts -> ", this.debts)
      );
  }

  getRequeriments(): void {
    let req = {
      person_id: this.student.person_id,
      department: this.student.profile.department,
      program: this.student.profile.program.id,
      college: this.student.profile.college.id
    };
    this.requerimentService.query(req, false)
      .subscribe(
        requeriments => this.requeriments = requeriments,
        error =>  this.error_message = <any>error,
      );
  }
  gotoDetail(): void {
    let link = ["/detail"];
    this.router.navigate(link);
  }
}
