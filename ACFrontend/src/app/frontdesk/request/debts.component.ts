import { Component, Input, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router }   from "@angular/router";
import { Observable }     from "rxjs/Observable";
import { Subject } from "rxjs/Subject";

import { Debt } from "../../shared/services/accounting/debt";
import { Requeriment } from "../../shared/services/admision/requeriment";
import { Student } from "../../shared/services/academic/student";

import { DebtService } from "../../shared/services/accounting/debt.service";
import { RequerimentService } from "../../shared/services/admision/requeriment.service";
// import { StudentFactory } from "../../shared/services/academic/student.factory";

@Component({
  selector: "req-debts-message",
  templateUrl: "./subdocument.component.html",
  providers: [DebtService, RequerimentService],
  // viewProviders: [StudentFactory]
})

export class ReqDebtComponent implements OnInit {
  @Input()student: Student;
  area: string;
  document: string;
  subdocument: string;
  error_message: string;
  debts: Debt[] = new Array<Debt>();
  requeriments: Requeriment[] = new Array<Requeriment>();

  constructor(
    private router: Router,
    private _route: ActivatedRoute,
    private debtService: DebtService,
    private requerimentService: RequerimentService,
    // private studentService: StudentFactory
  ) { }

  ngOnInit(): void {
    if(!this.student.profile.adm_type.is_pronabec){
      this.getDebts();
      this.getRequeriments();
    }
  }

  getDebts(): void {
    this.debtService.query(this.student.person_id)
      .subscribe(
        debts => this.debts = debts,
        error =>  this.error_message = <any>error,
        () => console.log("debts -> ", this.debts)
      );
  }

  getRequeriments(): void {
    let req = {
      person_id: this.student.person_id,
      department: this.student.profile.department,
      program: this.student.profile.program.id,
      college: this.student.profile.college.id
    };
    this.requerimentService.query(req, false)
      .subscribe(
        requeriments => this.requeriments = requeriments,
        error =>  this.error_message = <any>error,
      );
  }
}
