import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// shared components
import { SharedScheduleComponent } from '../shared/components/academic/schedule.component';
import { CAUBreadcrumbComponent } from '../shared/components/breadcrumb.component';
import { LoadingComponent } from '../shared/components/loading.component';
import { RedirectComponent } from '../shared/components/redirect.component';
import { CalloutComponent } from '../shared/components/callout/callout.component';
import { BORequestAbstractComponent } from '../shared/components/request/boRequestAbstract.component';
import { PollComponent } from '../shared/components/poll/poll.component';

const components: any[] = [
  SharedScheduleComponent,
  CAUBreadcrumbComponent,
  LoadingComponent,
  RedirectComponent,
  CalloutComponent,
  BORequestAbstractComponent,
  PollComponent
];

@NgModule({
  imports: [CommonModule],
  declarations: [components],
  exports: [
    CommonModule,
    components
  ]// directives, pipes, modules
})
export class SharedModule { }
