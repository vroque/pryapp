import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Menu } from '../../shared/menu';
import { MenuService } from '../../shared/menu.service';

@Component({
  selector: 'admision',
  templateUrl: './admision.component.html',
  providers: [ MenuService ],
})
export class AdmisionComponent implements OnInit {
  error_message: string;
  menus: Menu[];
  option = '';

  constructor(
    private _route: ActivatedRoute,
    private menuService: MenuService
   ) { }

  ngOnInit(): void {
    this._route.params.forEach((params: Params) => {
      this.option = params.option || '';
      this.processMenu( this.menuService.query() );
    });
  }

  processMenu(menus: Menu[]): void {
    if (this.option === '') {
      this.getAreas(menus);
    } else {
      this.getDocuments(menus, this.option);
    }
  }

  getAreas(menus: Menu[]): void {
    for (const menu of menus) {
      if (menu.uri === 'admision') {
          this.menus = menu.smenus;
      }
    }
  }

  getDocuments(menus: Menu[], option: any): void {
    for (const menu of menus) {
      if (menu.uri === 'admision') {
        for (const smenu of menu.smenus) {
          if (smenu.uri === option) {
            this.menus = smenu.smenus;
          }
        }
      }
    }
  }
}
