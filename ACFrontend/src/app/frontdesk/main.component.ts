import { Component, OnInit } from '@angular/core';

import { BackofficeConfigService } from '../backoffice/config/config.service';
import { ConsumeApiService } from '../shared/services/consume-api/consume-api.service';
import { LockCardsService } from '../shared/services/lock-cards/lock-cards.service';
import { Student } from '../shared/services/academic/student';
import { StudentFactory } from '../shared/services/academic/student.factory';
import { StudentService } from '../shared/services/academic/student.service';

@Component({
  selector: 'fd-main',
  templateUrl: './views/index.html',
  providers: [
    StudentService, BackofficeConfigService, LockCardsService, ConsumeApiService
  ]
})
export class MainComponent implements OnInit {
  student: Student;
  configFile: any;
  studentComplianceEnabled: boolean;
  academicSummaryEnabled: boolean;

  constructor(
    private studentService: StudentService,
    private config: BackofficeConfigService,
    private lockCardsService: LockCardsService,
    private consumeApiService: ConsumeApiService) {
    this.configFile = this.config;
  }

  ngOnInit() {
    this.getStudent();
    this.complianceStudentIsBlocked();
    this.academicSummaryIsActive();
  }

  getStudent(): void {
    this.studentService.get()
      .subscribe(
        (student) => this.setStudent(student),
        (error) => console.log(error),
      );
  }

  setStudent(student: Student): void {
    this.student = student;
    StudentFactory.getInstance().set(student);
  }

  /**
   * Check if student compliance card is locked
   */
  complianceStudentIsBlocked(): void {
    this.lockCardsService.isBlocked('student-compliance')
      .subscribe(
        (result) => this.studentComplianceEnabled = !result,
        (error) => console.log(error)
      );
  }

  /**
   * Check if academic summary block is locked
   */
  academicSummaryIsActive(): void {
    this.consumeApiService.isActive('academic-summary-api')
      .subscribe(
        (result) => this.academicSummaryEnabled = result,
        (error) => console.log(error)
      );
  }

}
