import { Component, OnInit } from '@angular/core';

import { Student } from '../../shared/services/academic/student';
import { StudentFactory } from '../../shared/services/academic/student.factory';
import { Wifi } from '../../shared/services/extra/wifi';
import { WifiService } from '../../shared/services/extra/wifi.service';

@Component({
  selector: 'wifi-password',
  templateUrl: './wifi.component.html',
  providers: [WifiService],
})

export class WifiComponent implements OnInit {

  loading = true;
  showKey = false;
  student: Student = StudentFactory.getInstance().get();
  wifi: Wifi;

  constructor(private wifiService: WifiService) {
  }

  ngOnInit() {
    this.get();
  }

  get() {
    this.wifiService.get(this.student.person_id)
      .subscribe(
      (data) => {
        this.wifi = data;
        console.log('WIFI => ', this.wifi);
        this.loading = false;
      },
      (err) => {
        console.log(err);
        this.loading = false;
      }
      );
  }

  copy(text: string) {
    const selBox = document.createElement('textarea');
    selBox.value = text;
    document.body.appendChild(selBox);
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
}
