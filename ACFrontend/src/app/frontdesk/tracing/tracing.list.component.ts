import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackofficeConfigService } from '../../backoffice/config/config.service';
import { AcademicProfile } from '../../shared/services/academic/academic.profile';
import { DocumentState } from '../../shared/services/atentioncenter/document.state';
import { DocumentStateService } from '../../shared/services/atentioncenter/document.state.service';
import { Request } from '../../shared/services/atentioncenter/request';
import { RequestByAreaService } from '../../shared/services/atentioncenter/request.area.service';
import { RequestLog } from '../../shared/services/atentioncenter/request.log';
import { RequestLogService } from '../../shared/services/atentioncenter/request.log.service';
import { RequestService } from '../../shared/services/atentioncenter/request.service';

@Component({
  providers: [BackofficeConfigService, DocumentStateService, RequestByAreaService, RequestLogService, RequestService],
  selector: 'tracing',
  templateUrl: './tracing.list.component.html'
})
export class TracingListComponent implements OnInit {

  errorMessage: string;
  req_id = '';
  errorMessageCancelRequest: string;
  loading: Boolean;
  requests: Request[] = [];
  request: Request;
  flow: DocumentState[];
  ideal_flow: DocumentState[];
  logs: RequestLog[];
  req: any = { id: '' };
  showDetails = false;
  @Input() profile: AcademicProfile;
  filter: any;
  configFile: any;

  constructor(
    private config: BackofficeConfigService,
    private documentStateService: DocumentStateService,
    private requestByAreaService: RequestByAreaService,
    private requestLogService: RequestLogService,
    private requestService: RequestService,
    private router: Router
  ) {
    this.configFile = this.config;
  }

  ngOnInit() {
    this.request = null;
    this.flow = [];
    this.ideal_flow = [];
    this.logs = [];
    this.getRequests();
  }

  getRequests() {
    this.requestByAreaService.getByProfile(this.profile) // a 0 => request for login student
      .subscribe(
        (requests) => this.completeData(requests),
        (error) =>  this.errorMessage = error as any,
      );
  }

  completeData(requests: Request[]) {
    for (const r of requests) {
      (r as any).document_name =  this.config.getDocumentName(r.document_id);
      (r as any).state_name =  this.config.getDocumentState(r.state).name || '--';
    }
    this.requests = requests;
  }

  goDetails(requestId: string) {
    this.getRequest(requestId);
    this.getRequestLogs(requestId);
    this.showDetails = true;
  }

  closeDetails() {
    this.showDetails = false;
    this.request = null;
  }

  getRequest(requestId: string) {
    this.requestService.get(requestId)
      .subscribe(
        (request) => this.request = request,
        (error) => this.errorMessage = error as any,
        () => this.getDocumentFlow(this.request.document_id)
      );
  }

  getRequestLogs(requestId: string) {
    this.requestLogService.query(requestId)
      .subscribe(
        (logs) =>  this.logs = logs,
        (error) =>  this.errorMessage = error as any
      );
  }

  getDocumentFlow(documentId: number) {
    this.documentStateService.query(documentId)
      .subscribe(
        (flow) => this.flow = flow,
        (error) => this.errorMessage = error as any,
        () => this.getIdealFlow(this.flow),
      );
  }

  getIdealFlow(flow: DocumentState[]) {
    this.ideal_flow = flow.filter((state) => (state.order !== -1));
  }

  docSt(id: number): any {
    return this.config.getDocumentState(id);
  }

  cancelRequest(data: Request) {
    this.loading = true;
    this.requestService.cancelRequest(data)
      .subscribe(
        (request) => this.request = request,
        (error) => {
          this.errorMessageCancelRequest = error as any;
          console.log(error);
          this.loading = false;
        },
        () => this.loading = false
      );
  }
}
