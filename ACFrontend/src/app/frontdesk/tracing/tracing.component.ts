import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { DocumentState } from '../../shared/services/atentioncenter/document.state';
import { Request } from '../../shared/services/atentioncenter/request';
import { RequestLog } from '../../shared/services/atentioncenter/request.log';

import { BackofficeConfigService } from '../../backoffice/config/config.service';
import { DocumentStateService } from '../../shared/services/atentioncenter/document.state.service';
import { GenerateDocumentService } from '../../shared/services/atentioncenter/generate.document.service';
import { RequestLogService } from '../../shared/services/atentioncenter/request.log.service';
import { RequestService } from '../../shared/services/atentioncenter/request.service';
import { SignatoriesService } from '../../shared/services/atentioncenter/signatories.service';

@Component({
  selector: 'request-list',
  templateUrl: './tracing.component.html',
  providers: [
    RequestService,
    RequestLogService,
    DocumentStateService,
    SignatoriesService,
    GenerateDocumentService,
    BackofficeConfigService,
  ],
})
export class TracingComponent implements OnInit {
  errorMessageCancelRequest: string;
  loading: Boolean;
  area: string = '';
  doc_status: string = 'pre'; // pre, on, err
  error_message: string;
  flow: Array<DocumentState>;
  ideal_flow: Array<DocumentState>;
  logs: Array<RequestLog>;
  request: Request;
  signatories: Array<string>;

  constructor(
    private _route: ActivatedRoute,
    private requestService: RequestService,
    private requestLogService: RequestLogService,
    private documentStateService: DocumentStateService,
    private signatoriesService: SignatoriesService,
    private generateDocumentService: GenerateDocumentService,
    private config: BackofficeConfigService,
  ) { }

  ngOnInit(): void {
    this.request = null;
    this.flow = [];
    this.ideal_flow = [];
    this.logs = [];
    this._route.params.forEach((params: Params) => {
      let request_id: string = (params as any).request;
      this.getRequest(request_id);
      this.getRequestLogs(request_id);
    });
  }

  getRequest(request_id: string): void {
    this.requestService.get(request_id)
      .subscribe(
        (request) => this.request = request,
        (error) =>  this.error_message = error,
        () => this.getDocumentFlow(this.request.document_id),
      );
  }

  getRequestLogs(request_id: string): void {
    this.requestLogService.query(request_id)
      .subscribe(
        (logs) => this.logs = logs,
        (error) =>  this.error_message = <any>error,
        () => console.log('logs -> ', this.logs),
      );
  }

  getDocumentFlow(document_id: number): void {
    this.documentStateService.query(document_id)
      .subscribe(
        (flow) => this.flow = flow,
        (error) =>  this.error_message = <any>error,
        () => { this.getIdealFlow(this.flow);}
      );
  }

  getIdealFlow(flow: Array<DocumentState>): void {
    this.ideal_flow = flow.filter((state) => { return (state.order !== -1); } );
  }

  getLogTrace(state: number): RequestLog {
    for (let i: number = 0; i < this.logs.length; i++) {
      if (this.logs[i].old_state === state) {
        return this.logs[i];
      }
    }
    return null;
  }

  docSt(id: number): any {
    return this.config.getDocumentState(id);
  }

  cancelRequest(data: Request) {
    this.loading = true;
    this.requestService.cancelRequest(data)
      .subscribe(
        (request) => this.request = request,
        (error) => {
          this.errorMessageCancelRequest = error as any;
          console.log(error);
          this.loading = false;
        },
        () => this.loading = false
      );
  }
}
