import { Component } from '@angular/core';
import '../rxjs-operators';

@Component({
  selector: 'fd-sidebar',
  templateUrl: './views/sidebar.html',
})

export class SidebarComponent {
  title = 'Sidebar';
}
