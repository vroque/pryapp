import { AcademicProfile } from '../../shared/services/academic/academic.profile';
import { AcademicProgressService } from '../../shared/services/academic/progress.service';
import { Component, OnInit } from '@angular/core';
import { CurriculumService } from '../../shared/services/academic/curriculum.service';
import { EnrollmentCourseService } from '../../shared/services/academic/enrollmentCourse.service';
import { Student } from '../../shared/services/academic/student';
import { StudentFactory } from '../../shared/services/academic/student.factory';
import { BackofficeConfigService } from '../../backoffice/config/config.service';

@Component({
  providers: [CurriculumService, AcademicProgressService, EnrollmentCourseService, BackofficeConfigService],
  selector: 'orientation-curriculum',
  templateUrl: './curriculum.component.html'
})
export class OrientationCurriculumComponent implements OnInit {

  errorMessage: string;
  student: Student;
  plan: any;
  progress: any;
  curriculum: any;
  enrollemnts: any[] = [];
  diplomaeds: any;
  loading = true;

  constructor(
    private curriculumService: CurriculumService,
    private progressService: AcademicProgressService,
    private enrollmentCourseService: EnrollmentCourseService,
    private backofficeConfigService: BackofficeConfigService) {
  }

  ngOnInit(): void {
    this.student = StudentFactory.getInstance().get();
    this.getProgress();
  }

  /**
   * Obtiene el avance academico del alumno en sesion
   */
  getProgress(): void {
    this.progressService.get(
      this.student.person_id,
      this.student.profile.program.id)
      .subscribe(
        (progress) => this.progress = progress,
        (err) => this.errorMessage = err,
        () => this.getPlan(),
      );
  }

  /**
   * Obtiene el detalle todal de los diplomandos del catalogo
   */
  getDiplomaed(): void {
    this.curriculumService.getDiplomaed(
      this.student.profile.department,
      this.student.profile.program.id,
      this.student.profile.campus.id,
      this.student.profile.term_catalg)
      .subscribe(
        (diplomaeds) => this.createDiplomaed(diplomaeds),
        (err) => this.errorMessage = err,
        () => this.createCurriculum(),
      );
  }

  /**
   * Obtiene el curriculo(plan de estudios) del alumno en sesion
   */
  getPlan(): void {
    this.curriculumService.getByPerson(
      this.student.person_id,
      this.student.profile.program.id)
      .subscribe(
        (plan) => this.plan = plan,
        (err) => this.errorMessage = err,
        () => {
          this.getEnrollmentCourses(this.student.profile);
          this.loading = false;
        },
      );
  }

  /**
   * Obtiene los asignaturas matriculados
   * @param profile
   */
  getEnrollmentCourses(profile: AcademicProfile): void {
    this.enrollmentCourseService.list(profile)
      .subscribe(
        (data) => this.enrollemnts = data,
        (err) => this.errorMessage = err,
        () => this.getDiplomaed(),
      );
  }

  /**
   * Avance Academico del alumno
   * fusiona el curriculo(plan de estudios) con el historial de asignaturas del
   * alumno para formar su avance academico
   */
  createCurriculum(): void {
    let courses_taken_mandatory = 0;
    let courses_taken_elective = 0;
    let credits_taken_mandatory = 0;
    let credits_taken_elective = 0;
    // activities
    for (const cycle of this.plan.cycles) {
      // end activities
      const cycle_coures: any[] = this.progress.courses.filter((c: any) => c.cycle === cycle.id);
      cycle.taken = this.sumCredits(cycle_coures);
      // console.log('taken courses')
      for (const course of cycle.courses) {
        const take_courses: Array<any> = this.progress.courses.filter(
          (c: any) => c.id === course.id || c.id === course.equivalent || c.equivalent === course.id,
        );
        course.equivalent = course.equivalent || '';
        course.taken = 0;
        let take_course: any;
        if (take_courses.length > 0) {
          course.taken = take_courses.length;
          take_course = this.getBetter(take_courses);
          if (take_course.id === take_course.equivalent) {
            course.is_equi = false;
          } else {
            course.is_equi = true;
          }

        }

        if (take_course) {
          course.score = take_course.score || 0;
          course.is_aproved = take_course.is_aproved || false;
          // excepcion para actividades
          course.is_conva = take_course.nrc === 'CONV';
          if (take_course.type === 'O' && take_course.is_aproved) {
            courses_taken_mandatory++;
            credits_taken_mandatory += take_course.credits;
          }
          if (take_course.type === 'E' && take_course.is_aproved) {
            courses_taken_elective++;
            credits_taken_elective += take_course.credits;
          }
          // solo en actividades que han llevado
          // dos veces para una sola actividad por ciclo
          if (take_course.id.startsWith('ACUC')) {
            course.name = take_course.name;
            course.id = take_course.id;
            course.is_equi = false;
          }
          course.id = take_course.id;
        }
        /*
        * sino buscar si estan matriculados
        * solo considerar asignaturas matriculados que no hayan sido aprobados
        * NOTE: También se podría filtrar con «c.score === "-"» en lugar de «approvedCourseOfTheCycle.length === 0»
        */
        const approvedCourseOfTheCycle = cycle_coures.filter((c) => c.id === course.id && c.is_aproved);
        const enrollment = this.enrollemnts
          .filter((c) => c.id === course.id && approvedCourseOfTheCycle.length === 0);
        if (enrollment.length > 0) { // si esta llevando esta asignatura
          const periodo = enrollment[0].term; // período actual
          const disapprovedCourseOfTheCycle = cycle_coures
            .filter((c) => c.id === course.id && c.term === periodo && !c.is_aproved);
          if (disapprovedCourseOfTheCycle.length === 0) {
            course.enrollment = true;
            course.taken++;
            course.score = null;
          }
        }
      }
      this.plan.courses_taken_mandatory = courses_taken_mandatory;
      this.plan.courses_taken_elective = courses_taken_elective;
      this.plan.credits_taken_mandatory = credits_taken_mandatory;
      this.plan.credits_taken_elective = credits_taken_elective;
    }

    // Actividades matriculados
    for (const enroll of this.enrollemnts) {
      let used = false;
      if (enroll.subject === 'ACUC') {
        // para actividad de ciclo 1
        for (const act1 of this.plan.cycles[0].courses) {
          if ((act1.id.startsWith('ACTI') || act1.id.startsWith('ACUC') || act1.subject === 'ACUC') &&
            !act1.is_aproved && !used) {
            act1.id = enroll.id;
            act1.nrc = enroll.nrc;
            act1.name = enroll.name;
            act1.enrollment = true;
            act1.score = null;
            // console.log("act1 => ", act1);
            used = true;
          }
        }

      }
    }

    const acti1 = this.plan.cycles[0].courses
      .filter((f) => f.id.startsWith('ACTI') || f.id.startsWith('ACUC') || f.subject === 'ACUC');

    for (const enroll of this.enrollemnts) {
      let used2 = false;
      if (enroll.subject === 'ACUC') {
        // para actividad de ciclo 2
        for (const act2 of this.plan.cycles[1].courses) {
          if ((act2.id.startsWith('ACTI') || act2.id.startsWith('ACUC') || act2.subject === 'ACUC') &&
            !act2.is_aproved && !used2 && acti1[0].id !== enroll.id) {
            act2.id = enroll.id;
            act2.nrc = enroll.nrc;
            act2.name = enroll.name;
            act2.enrollment = true;
            act2.score = null;
            // console.log("act2  => ", act2);
            used2 = true;
          }
        }
        // console.log(enroll);
      }
    }
  }

  /**
   * Crea la vista de diplomados
   * @param diplomaeds Lista de diplomados del plan de estudios
   */
  createDiplomaed(diplomaeds: any): void {
    for (const diplomaed of diplomaeds) {
      for (const course of diplomaed.courses) {
        const takeCourses: any[] = this.progress.courses.filter((c: any) => c.equivalent === course.id);
        course.taken = takeCourses.length;
        if (takeCourses.length > 0) {
          const takeCourse = this.getBetter(takeCourses);
          course.score = takeCourse.score || 0;
          course.is_aproved = takeCourse.is_aproved || false;
        }
        const enrollment = this.enrollemnts.filter((c) => c.id === course.id);
        console.log(course.id, enrollment);
        if (enrollment.length > 0) { // si esta llevando esta asignatura
          course.enrollment = true;
        }
      }
    }
    this.diplomaeds = diplomaeds;
  }

  /**
   * Busca la asignatura con mayor creditaje de todas
   * las veces que se llevo esta asignatura
   * @param courses lista de veces que se llevo una asignatura
   * @return course el asignatura con mayor promedio de la lista
   */
  getBetter(courses: any[]): any {
    let c: any = courses[0];
    for (const course of courses) {
      if (c.score < course.score) {
        c = course;
      }
    }
    return c;
  }

  /**
   * suma creditos de la lista de asignaturas
   */
  sumCredits(courses: any[]): number {
    let sum = 0;
    for (const course of courses) {
      if (course.is_aproved) {
        sum += course.credits;
      }
    }
    return sum;
  }
}
