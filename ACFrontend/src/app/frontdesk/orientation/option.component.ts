import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router }   from "@angular/router";
// import { Observable }     from "rxjs/Observable";
// import { Subject } from "rxjs/Subject";

import { Student } from "../../shared/services/academic/student";
import { StudentFactory } from "../../shared/services/academic/student.factory";

@Component({
  selector: "orientation-option",
  templateUrl: "./option.component.html",
  providers: [],
  // viewProviders: [StudentFactory]
})

export class OrientationOptionComponent implements OnInit {
  area: string;
  option: string;
  error_message: string;
  student: Student;

  constructor(
    private router: Router,
    private _route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.student = null;
    this.student = StudentFactory.getInstance().get();
    this._route.params.forEach((params: Params) => {
      this.area = (<any>params).area;
      this.option = (<any>params).option;
    });

  }


}
