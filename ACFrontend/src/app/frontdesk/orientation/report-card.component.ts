import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Course } from '../../shared/services/academic/course';
import { EnrollmentTermService } from '../../shared/services/academic/enrollmentTerm.service';
import { ReportCardService } from '../../shared/services/academic/reportCard.service';
import { BackofficeConfigService } from '../../backoffice/config/config.service';
import { query } from '@angular/animations';

@Component({
  providers: [ReportCardService, EnrollmentTermService, BackofficeConfigService],
  selector: 'report-card2',
  templateUrl: './report-card.component.html'
})
export class ReportCardComponent implements OnInit {
  @Input() claimNote = false;
  @Output() termSelected = new EventEmitter();
  loading = true;
  loadingNotes = true;
  error = false;
  errorNotes = false;
  allCourses: Course[] = [];
  courses: Course[] = [];
  minorityCourses: Course[] = [];
  type = 'simple';
  terms: string[] = [];
  currentTerm: string;
  errorMessage: string;
  averagePendin = 0; // promedios pendientes

  saveId: number;

  constructor(
    private router: Router,
    private reportCardService: ReportCardService,
    private enrollmentTermService: EnrollmentTermService,
    private backofficeConfigService: BackofficeConfigService) { }

  ngOnInit() {
    this.getAllTermsEnrollment();
  }

  /**
   * Ontención de los períodos en los cuales el estudiante estuvo
   */
  getAllTermsEnrollment(): void {
    this.enrollmentTermService.getAllTermsEnrollment()
      .subscribe(
        (terms) => {
          this.terms = terms;
          if (this.terms.length > 0) {
            this.currentTerm = this.terms[0];
            this.getReportCard(this.currentTerm);
          } else {
            this.error = true;
            this.errorNotes = true;
            this.loadingNotes = false;
          }
          this.loading = false;
        },
        (error) => {
          this.errorMessage = 'no se pudo obtener las matrículas.';
          this.error = true;
          this.errorNotes = true;
          this.loading = false;
          this.loadingNotes = false;
        }
      );
  }

  /**
   * Obtiene la boleta de notas del período
   * @param term período del que se requiere la boleta
   */
  getReportCard(term: string) {
    this.loadingNotes = true;
    this.errorNotes = false;
    this.currentTerm = term;
    this.minorityCourses = [];
    this.averagePendin = 0;
    this.reportCardService.get(term)
      .subscribe(
        (courses) => {
          this.allCourses = courses;
          this.verifyCourse(term);
          this.verifyAveragePendin();
          this.termSelected.emit(term);
          this.loadingNotes = false;
          this.loading = false;
        },
        (error) => {
          this.errorNotes = true;
          this.loadingNotes = false;
          this.errorMessage = 'No se pudieron obtener las asignaturas.';
          this.error = true;
          this.loading = false;
        },
        () => {
          this.verifyCourse(term);
        },
      );

  }

  /**
   * Procesa la informacion de la asignatura
   * *para período 201710 en adelante
   */
  verifyCourse(term: string) {
    this.minorityCourses = [];
    this.courses = [];
    // boleta de BANNER
    if (term >= '201710') {
      for (const course of this.allCourses) {
        const exist: any[] = course.components.filter(
          (component) =>
            component.code === '2-C1' ||
            component.code === '3-EP' ||
            component.code === '4-C2' ||
            component.code === '5-EF',
        );
        if (!exist || exist.length < 4) {
          // completar componentes a 6
          course.components[0] = course.components[0] || {};
          course.components[1] = course.components[1] || {};
          course.components[2] = course.components[2] || {};
          course.components[3] = course.components[3] || {};
          course.components[4] = course.components[4] || {};
          course.components[5] = course.components[5] || {};
          this.minorityCourses.push(course);
        } else {
          this.courses.push(course);
        }
      }
    } else if (term >= '201510') {// boleta de APEC
      for (const course of this.allCourses) {
        course.observation = '';
        course.components = [];
        course.components[0] = { code: '2-C1', name: 'CONSOLIDADO 1', score: course.score_detail.c1 };
        course.components[1] = { code: '3-EP', name: 'EVALUACIÓN PARCIAL', score: course.score_detail.ep };
        course.components[2] = { code: '4-C2', name: 'CONSOLIDADO 2', score: course.score_detail.c2 };
        course.components[3] = { code: '5-EF', name: 'EVALUACIÓN FINAL', score: course.score_detail.ef };
        course.components[4] = course.components[4] || {};
        course.components[5] = course.components[5] || {};
        this.courses.push(course);
      }
    } else {
      for (const course of this.allCourses) {
        course.observation = '';
        course.components = [];
        course.components[0] = { code: '2-C1', name: 'CONTROL DE LECTURA 1', score: course.score_detail.c1 };
        course.components[1] = { code: '3-EP', name: 'EVALUACIÓN PARCIAL', score: course.score_detail.ep };
        course.components[2] = { code: '4-C2', name: 'CONTROL DE LECTURA 2', score: course.score_detail.c2 };
        course.components[3] = { code: '5-EF', name: 'EVALUACIÓN FINAL', score: course.score_detail.ef };
        course.components[4] = { code: '0-T1', name: 'TAREA ACADEMICA 1', score: course.score_detail.ta1 };
        course.components[5] = { code: '0-T2', name: 'TAREA ACADEMICA 2', score: course.score_detail.ta2 };
        this.courses.push(course);
      }
    }
  }

  getComponent(course: Course, component: string): any {
    for (const cp of course.components) {
      const cpa: any = cp.code;
      if (cpa === component) {
        return cp;
      }
    }
    return {
      code: '',
      date: '',
      id: '',
      observation: '',
      score: '',
      sub: [],
      weight: '',
    };
  }

  /**
   * Devuelve el Promedio General Acumulado (PGA)
   */
  getPGA(): number {
    let credits = 0;
    let sum = 0;
    for (const course of this.allCourses) {
      credits += course.credits;
      sum += ((course.score !== '-') ? +course.score : 0) * course.credits;
    }
    if (credits <= 0) {
      credits = 1;
    }
    return (sum / credits);
  }

  /**
   * Calcula la cantidad de asignaturas que no tienen promedio final
   */
  verifyAveragePendin(): void {
    for (const course of this.allCourses) {
      if (course.score === null || course.score === '-') {
        this.averagePendin++;
      }
    }
  }

  /**
   * Validar Reclamo de nota
   * @param course Asignatura
   * @param component Componente reclamado
   */
  validateNoteClaim(course: Course, component: any): boolean {
    let validate = true;
    if (component.date == null || component.data === '' || component.score === '-' || component.score == null || !this.claimNote) {
      validate = false;
    }
    if ((course.department === this.backofficeConfigService.DEPA_REG && component.daysUploadNote > 1)
      || (course.department !== this.backofficeConfigService.DEPA_REG && component.daysUploadNote > 5)) {
      validate = false;
    }
    return validate;
  }

  /**
   * Enviar a formulario de reclamo
   * @param course Asignatura
   * @param component Componente reclamado
   */
  sendNoteClaim(course: Course, component: any): void {
    const componentId = (component.gcomId === undefined || component.gcomId === null) ? component.id : component.gcomId;
    const subComponentId = (component.gcomId === undefined || component.gcomId === null) ? '-' : component.id;
    if (this.validateNoteClaim(course, component)) {
      this.router.navigate([`frontdesk/tramite/reclamo-notas/Registrar`], {
        queryParams: {
          term: course.term,
          nrc: course.nrc,
          dp: course.department,
          cId: componentId,
          sCId: subComponentId
        }
      });
    }
  }

}
