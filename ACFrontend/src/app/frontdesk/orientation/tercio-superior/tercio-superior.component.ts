import { Component, OnInit } from '@angular/core';
import { Proof3510Period } from '../../../shared/services/atentioncenter/proof.3510.period';
import { Proof3510PeriodService } from '../../../shared/services/atentioncenter/proof.3510.period.service';
import { Callout } from '../../../shared/components/callout/callout';

@Component({
  providers: [Proof3510PeriodService],
  selector: 'tercio-superior',
  templateUrl: './tercio-superior.component.html'
})
export class TercioSuperiorComponent implements OnInit {

  document: Proof3510Period;
  errorMessage: string;
  loading = true;
  error = false;
  notFound = false;
  callout: Callout;

  constructor(private proof3510PeriodService: Proof3510PeriodService) { }

  ngOnInit() {
    this.getDocument();
  }

  getDocument() {
    console.log('get document');
    this.proof3510PeriodService.get()
      .subscribe(
        (data) => {
          this.document = data;
          if (this.document.term_list.length > 0) {
            this.composeOrders();
          } else {
            this.notFound = true;
            this.loading = false;
            this.callout = {
              title: 'Mensaje',
              type: 'warning',
              data: ['Usted no esta dentro del tercio, quinto o décimo superior aún.']
            };
          }
        },
        (error) => {
          this.error = true;
          this.errorMessage = error;
          this.loading = false;
          this.callout = {
            title: 'Alerta',
            type: 'warning',
            data: [
              'No se encontraron registros pertenecientes al Tercio, quinto y décimo superior.',
              'Estamos trabajando en resolver esto.'
            ]
          };
        },
      );
  }

  composeOrders() {
    for (const period of this.document.term_list) {
      if (period.order === 10) {
        (period as any).orders = '3.°, 5.° y 10.° superior.';
      }
      if (period.order === 5) {
        (period as any).orders = '3.° y 5.° superior.';
      }
      if (period.order === 3) {
        (period as any).orders = '3.° superior.';
      }
    }
    this.loading = false;
  }
}
