import { Component, OnInit } from '@angular/core';
import { PersonaService } from '../../shared/services/persona/persona.service';
import { Persona } from '../../shared/services/persona/persona';

@Component({
  selector: 'data-person',
  templateUrl: './data-person.component.html',
  providers: [PersonaService]
})

export class DataPersonComponent implements OnInit {
  loading: boolean;
  person: Persona;
  constructor(private personaService: PersonaService) { }
  ngOnInit(): void {
    this.get();
  }

  /**
   * Obtener data de la persona
   */
  get(): void {
    this.loading = true;
    this.personaService.get()
      .subscribe(
        (data) => {
          this.person = data;
          this.loading = false;
        },
        (err) => {
          console.log(err);
          this.loading = false;
          }
      );
  }
}
