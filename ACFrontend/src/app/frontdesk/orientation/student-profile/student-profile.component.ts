import { DOCUMENT } from "@angular/common";
import { Component, Inject, OnInit } from "@angular/core";

@Component({
  selector: "student-profile",
  templateUrl: "./student-profile.component.html",
})
export class StudentProfileComponent implements OnInit {
  link: string;

  constructor(@Inject(DOCUMENT) private document: any) {
    this.link = "https://campusvirtualxe02.continental.edu.pe/StudentSSB/";
  }

  ngOnInit() {
    this.document.location.href = this.link;
  }
}
