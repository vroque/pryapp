import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { PaymentSimulator } from '../../shared/services/accounting/paymentSimulator';
import { PaymentSimulatorService } from '../../shared/services/accounting/paymentSimulator.service';
import { Student } from '../../shared/services/academic/student';
import { StudentFactory } from '../../shared/services/academic/student.factory';

@Component({
  selector: 'payment-simulator',
  templateUrl: './paymentSimulator.component.html',
  providers: [
    PaymentSimulatorService,
  ],
})
export class PaymentSimulatorComponent implements OnInit {

  error_message: string;
  student: Student;
  payment: PaymentSimulator;
  credits: number;
  credits_first: number;
  credits_second: number;
  credits_vir_gt = 22;
  chk_group_a: boolean;
  chk_group_b: boolean;
  creditsNumberByPlan: number; // número de créditos de 1ra cuota a pagar según plan de estudios
  loading: boolean;
  confirmed: boolean;
  @Input() modal = false;

  constructor(private paymentSimulatorService: PaymentSimulatorService,
    private location: Location) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.confirmed = false;
    this.student = StudentFactory.getInstance().get();
    // from database
    this.paymentSimulatorService.get()
      .subscribe(
        (data) => this.payment = data,
        (error) => {
          this.error_message = error;
          this.loading = false;
        },
        () => {
          this.getInit();
          this.loading = false;
        },
    );
    console.log('init payment simulator component');
  }

  acceptTermsAndConditions(): void {
    this.confirmed = true;
  }

  rejectTermsAndConditions(): void {
    this.location.back();
  }

  /**
   * Cálculo de número de créditos a pagar en la primera cuota según el plan de estudios.
   * Inicialización de variables para la simulación de pensiones
   */
  getInit(): void {
    if (this.payment.studyPlan < 201800) {
      this.credits = 22;
      this.credits_first = 11;
      this.credits_second = 11;
      if (this.payment.department === 'UREG') {
        this.creditsNumberByPlan = 4;
      } else {
        this.creditsNumberByPlan = 5; // 5.5
      }
    } else {
      this.credits = 20;
      this.credits_first = 10;
      this.credits_second = 10;
      if (this.payment.department === 'UREG') {
        this.creditsNumberByPlan = 4;
      } else {
        this.creditsNumberByPlan = 5;
      }
    }
  }

  uregFirstPayment(): number {
    let total = this.payment.credit_cost * this.creditsNumberByPlan;
    total += this.payment.enrollment_cost;
    total += this.payment.student_card_cost;
    total += this.payment.health_insurance_cost;
    return total;
  }

  uregOtherPayment(): number {
    let cost = ((this.credits - this.creditsNumberByPlan) * this.payment.credit_cost) / 4;
    if (cost < 0) {
      cost = 0;
    }
    return cost;
  }

  uregTotal(): number {
    let total = this.payment.credit_cost * this.credits;
    total += this.payment.enrollment_cost;
    total += this.payment.student_card_cost;
    total += this.payment.health_insurance_cost;
    return total;
  }

  uvirFirstPayment(): number {
    let total = this.payment.credit_cost * this.creditsNumberByPlan;
    total += this.payment.enrollment_cost;
    total += this.payment.student_card_cost;
    total += this.payment.health_insurance_cost;
    return total;
  }

  /// 0 -> Total; 1 -> Primera cuota; 2 -> 2da cuota; 3 -> 3ta cuota; 4 -> 4ta couta
  uvirAllPayment(numberCuote: number): number {
    /*if (this.credits_vir_gt < this.creditsNumberByPlan) {
      this.credits_vir_gt = this.creditsNumberByPlan;
    }*/
    let total = this.payment.credit_cost * (this.credits_vir_gt);
    total += this.payment.enrollment_cost;
    total += this.payment.student_card_cost;
    total += this.payment.health_insurance_cost;

    const others = (this.credits_vir_gt - this.creditsNumberByPlan) * this.payment.credit_cost;
    let first = this.uvirFirstPayment();
    let second = 0;
    let thirth = 0;
    let fourth = 0;

    if (!this.chk_group_a && !this.chk_group_b) {
      first = 0;
      total = 0;
    } else if (this.chk_group_a && !this.chk_group_b && others > 0) {
      second = others;
    } else if (!this.chk_group_a && this.chk_group_b && others > 0) {
      fourth = others;
    } else if (this.chk_group_a && this.chk_group_b && others > 0) {
      second = others / 3;
      thirth = others / 3;
      fourth = others / 3;
    }

    switch (numberCuote) {
      case 0:
        return total;
      case 1:
        return first;
      case 2:
        return second;
      case 3:
        return thirth;
      case 4:
        return fourth;
      default:
        break;
    }
  }
  uvirSecondPayment(): number {
    return (this.credits_first - this.creditsNumberByPlan) * this.payment.credit_cost;
  }

  uvirThirthAndFourthPayment(): number {
    return (this.credits_second / 2) * this.payment.credit_cost;
  }

  uvirTotal(): number {
    let total = this.payment.credit_cost * (this.credits_first + this.credits_second);
    total += this.payment.enrollment_cost;
    total += this.payment.student_card_cost;
    total += this.payment.health_insurance_cost;
    return total;
  }

  /**
   * Validar creditos ingresados
   */
  validateInputCredits(): void {
    if (this.credits < 1 || this.credits_vir_gt < 1) {
      this.credits = 1;
      this.credits_vir_gt = 1;
    } else if (this.credits > 30 || this.credits_vir_gt > 30) {
      this.credits = 30;
      this.credits_vir_gt = 30;
    }
  }

  /**
   * Obtener fecha de pago por id
   * @param cuotaID Id
   */
  getDateByCuota(cuotaID: string): Date {
    let date: Date;
    const data = this.payment.dataCuotas.filter(f => f.concept_id === cuotaID)[0];
    if (data != null && data.paymen_date != null) {
      date = data.paymen_date;
    }
    return date;
  }
}
