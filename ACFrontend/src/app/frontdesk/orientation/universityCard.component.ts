import { Component, OnInit } from '@angular/core';
import { TCUUniversityCardService } from '../../shared/services/extra/universityCard.service';

@Component({
  selector: 'tcu-university-card',
  templateUrl: './universityCard.component.html',
  providers: [TCUUniversityCardService]
})

export class TCUUniversityCardComponent implements OnInit {
  loading: boolean;
  card: any;
  constructor( private tcuUniversityCardService: TCUUniversityCardService) { }
  ngOnInit(): void {
    this.get();
  }

  /**
   * Obtener datos
   */
  get(): void {
    this.loading = true;
    this.tcuUniversityCardService.get()
    .subscribe(
      (data) => {
        this.card = data;
        this.loading = false;
      },
      (err) => {
        console.log(err);
        this.loading = false;
      }
    );
  }
}
