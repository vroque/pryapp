import { Component, Input, OnInit } from "@angular/core";

import { Student } from "../../../shared/services/academic/student";

@Component({
  selector: "soc-record",
  templateUrl: "./socrecord.component.html",
  providers: []
})
export class SocRecordComponent implements OnInit {
  @Input()student: Student;
  error_message: string = "";

  constructor( ) { }

  ngOnInit(): void { }

}
