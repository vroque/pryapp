import { Component, Input, OnInit } from "@angular/core";

import { Student } from "../../../shared/services/academic/student";

@Component({
  selector: "soc-record-guide",
  templateUrl: "./socrecord.guide.component.html",
})
export class SocRecordGuideComponent {
  @Input()student: Student;
  error_message: string = "";
  constructor( ) {  }
}
