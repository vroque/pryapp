import { Component, OnInit } from '@angular/core';

import { BackofficeConfigService } from '../../backoffice/config/config.service';
import { ConfigService } from '../../config/config.service';
import { RequestService } from '../../shared/services/atentioncenter/request.service';

import { Request } from '../../shared/services/atentioncenter/request';

@Component({
  selector: 'orientation-resolution',
  templateUrl: './resolution.component.html',
  providers: [RequestService, BackofficeConfigService]
})
export class OrientationResolutionComponent implements OnInit {
  errorMessage: string;
  resolutions: Array<Request>;
  pendings: Array<Request>;
  loading = true;

  constructor(
    private requestService: RequestService,
    private config: BackofficeConfigService,
    private _c: ConfigService
  ) {
  }

  ngOnInit(): void {
    this.requestService.resolutions()
      .subscribe(
        (requests) => this.divide(requests),
        (error) => this.errorMessage = 'no se pudieron obtener las solicitudes',
        () => this.loading = false
      );
  }

  divide(requests: Array<Request>): void {
    this.resolutions = requests.filter(r =>
      r.state === this.config.STATE_FINAL);
    this.pendings = requests.filter(r =>
      r.state !== this.config.STATE_FINAL);
  }

}
