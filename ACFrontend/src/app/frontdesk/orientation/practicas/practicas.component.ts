import { Component, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'orientacion-practicas',
  templateUrl: './practicas.component.html'
})
export class PracticasComponent implements OnInit {
  link: string;

  constructor(@Inject(DOCUMENT) private document: any) {
    this.link = 'https://ucontinental.edu.pe/oportunidades-laborales/practicas/';
  }

  ngOnInit() {
    this.document.location.href = this.link;
  }
}
