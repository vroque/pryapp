import { Component, OnInit } from '@angular/core';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { ACTermService } from '../../../shared/services/atentioncenter/term.service';
import { PlatformLocation } from '@angular/common';

import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { EconomicStatus } from '../../../shared/services/accounting/economicStatus';
import { ACTerm } from '../../../shared/services/atentioncenter/term';
import { EnrollmentTermService } from '../../../shared/services/academic/enrollmentTerm.service';

import { InscriptionGroupService } from '../../../shared/services/enrollment/inscription-group.service';
import { InscriptionGroup } from '../../../shared/services/enrollment/inscription-group';

import { StatusStudent } from '../../../shared/services/enrollment/status-student';
import { StatusStudentService } from '../../../shared/services/enrollment/status-student.service';

import { Mat123TermEnrollment } from '../../../shared/services/enrollment/term-enrollment';
import { Mat123TermEnrollmentService } from '../../../shared/services/enrollment/term-enrollment.service';


declare const $: any;

@Component({
  selector: 'enrollment-123',
  templateUrl: './main.component.html',
  providers: [
    BackofficeConfigService,
    EnrollmentTermService,
    InscriptionGroupService,
    StatusStudentService,
    ACTermService,
    Mat123TermEnrollmentService],
})
export class Enrollment123Component implements OnInit {
  loading = false;
  student = StudentFactory.getInstance().get();
  debts: EconomicStatus[] = [];
  debtsActives = false;
  debtsActivesCount = 0;
  term: Mat123TermEnrollment; // periodo actual
  errorMessage: string;
  modalType = 'none';
  show = false;
  stepNow: number;
  steps: {
    title: string,
    icon: string,
    step: number,
    complete: boolean
  }[];

  items: { component: string, size: string, title: string }[];
  modal: { component: string, size: string, title: string };
  inscriptionGroup: InscriptionGroup;
  status: StatusStudent;

  constructor(
    private config: BackofficeConfigService,
    private aCTermService: ACTermService,
    private inscriptionGroupService: InscriptionGroupService,
    private statusStudentService: StatusStudentService,
    private platformLocation: PlatformLocation,
    private mat123TermEnrollmentService: Mat123TermEnrollmentService
  ) {
    this.platformLocation.onPopState(() => $('.modal').modal('hide'));
  }

  ngOnInit(): void {

    this.getStatus();

    this.steps = [
      {
        title: 'Prepárate',
        icon: 'inquest',
        step: 1,
        complete: true
      },
      {
        title: 'Realiza tus pagos',
        icon: 'payment',
        step: 2,
        complete: false
      },
      {
        title: 'Matricúlate',
        icon: 'survey',
        step: 3,
        complete: false
      },
    ];


    // agregamos los modales disponibles
    this.items = [
      {
        component: 'orientation-curriculum',
        size: 'modal-xl',
        title: 'Plan de estudios'
      },
      {
        component: 'payment-simulator',
        size: 'modal-xl',
        title: 'Simulador de pago'
      },
      {
        component: 'faq-frontdesk',
        size: 'modal-xl',
        title: 'Preguntas Frecuentes'
      },
      {
        component: 'matricula-presencial',
        size: 'modal-xl',
        title: 'Video Matrícula Presencial'
      },
      {
        component: 'matricula-semipresencial',
        size: 'modal-xl',
        title: 'Video Matrícula Semipresencial'
      },
      {
        component: 'register-incident-student',
        size: 'modal-xl',
        title: 'Registra tu incidente'
      }
    ];
    // designamos por defecto el primer modal
    this.modal = {
      component: 'none',
      size: 'modal-xl',
      title: 'Sin definir'
    };
  }

  /**
   * Obtiene el estado del estudiante
   */
  getStatus(): void {
    this.loading = true;
    this.statusStudentService.getStatusStudent(this.student.person_id)
      .subscribe(
        (data) => {
          this.status = data;
          this.loading = false;
          this.getTermCurrentEnrollment();
        },
        (err) => {
          console.log(err);
          console.log('Error al obtener el estado del estudiante');
          this.loading = false;
        }
      );
  }

  /**
   * Obtener el último periodo para su matrícula
   */
  getTermCurrentEnrollment(): void {
    this.loading = true;
    this.mat123TermEnrollmentService.getStatusStudent(this.student.person_id).subscribe(
      (term) => {
        this.term = term;
        this.loading = false;
        if (this.validateTermEnrollment()) {
          this.getInscriptionGroup(term.term);
        }
      },
      (error) => {
        this.loading = false;
        console.log('error => ', error);
      }
    );
  }

  /**
   * Obtiene el grupo de matrícula del estudiante
   */
  getInscriptionGroup(term: string): void {
    this.loading = true;
    this.inscriptionGroupService.getInscriptionGroup(this.student.person_id, term)
      .subscribe(
        (data) => {
          this.inscriptionGroup = data;
          this.loading = false;
          if (data != null) {
            this.inscriptionGroup.endDate = new Date(String(data.endDate));
            this.inscriptionGroup.beginDate = new Date(String(data.beginDate));
            if (data.enrollment) {
              this.stepNow = 3;
            } else {
              this.stepNow = 1;
            }
          } else {
            this.stepNow = 1;
          }
        },
        (err) => {
          console.log(err);
          console.log('Error al obtener el grupo de inscripción');
        }
      );
  }

  /**
   * Define el titulo y el modal a abrir
   * @param component componente name
   */
  openModal(component: string): void {
    for (const item of this.items) {
      if (item.component === component) {
        this.modal = item;
      }
    }
    $('#modal-enrrollment-main').modal('show');
  }

  /**
   * Cambio de paso
   * @param step Paso
   */
  changeStep(step: number): void {
    this.stepNow = step;
  }

  /**
   * Verifica y devuelve si el estudiante no tiene grupo de inscripción
   */
  noInscriptionGroup(): boolean {
    if (this.inscriptionGroup == null || this.inscriptionGroup === undefined) {
      return false;
    }
    return true;
  }

  /**
   * Validar si existe periodo de matricula
   */
  validateTermEnrollment(): boolean {
    let validate = true;
    if (this.term == null || this.term === undefined || this.term.term == null
        || this.term.term === undefined || this.term.term.trim().length <= 0) {
      validate = false;
    }
    return validate;
  }

  /**
   * Validar Estado del estudiante
   */
  validateStatusStudent(): boolean {
    let validate = true;
    if (this.status == null || this.status === undefined || this.status.active == null
        || !this.status.active) {
      validate = false;
    }
    return validate;
  }

}
