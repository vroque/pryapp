import { Component, Input, OnInit } from '@angular/core';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { ACTerm } from '../../../shared/services/atentioncenter/term';
import { ConfigService } from '../../../config/config.service';
import { Student } from '../../../shared/services/academic/student';
import { RequerimentService } from '../../../shared/services/admision/requeriment.service';
import { Requeriment } from '../../../shared/services/admision/requeriment';

import { InscriptionGroup } from '../../../shared/services/enrollment/inscription-group';

import { RetentionService } from '../../../shared/services/enrollment/retention.service';
import { Retention } from '../../../shared/services/enrollment/retention';
import { StatusStudent } from '../../../shared/services/enrollment/status-student';

declare var $: any;

@Component({
  providers: [BackofficeConfigService, ConfigService, RetentionService, RequerimentService],
  selector: 'enrollment-step1',
  templateUrl: './step1.component.html'
})
export class EnrollmentStep1Component implements OnInit {

  @Input() student: Student;
  @Input() title: string;
  @Input() inscriptionGroup: InscriptionGroup;
  @Input() status: StatusStudent;
  requeriments: Requeriment[] = [];
  items: { component: string, title: string, size: string }[];
  modal: { component: string, title: string, size: string };
  retentions: Retention[] = [];
  postsPending: WpInfo[];
  postsProcedure: WpInfo[];
  postId: number;
  postAction: string;
  pdfType: string;
  today = new Date();

  constructor(
    private configService: ConfigService,
    private retentionService: RetentionService,
    private requerimentService: RequerimentService
  ) { }

  ngOnInit() {
    //  hlddCode: ['01','04], type: 'deuda'
    //  hlddCode: '02', type: 'document-pending'
    //  hlddCode: '07', type: 'change-plan'
    //  hlddCode: '08', type: 'permanence'
    //  hlddCode: '09', type: 'tricado'

    this.getRetentions();
    this.items = [
      {
        component: 'economic-status',
        title: 'Deuda pendiente',
        size: 'modal-xl'
      },
      {
        component: 'document-pending',
        title: 'Documentos pendientes',
        size: 'modal-xl'
      },
      {
        component: 'academic-regulation',
        title: 'Reglamento académico',
        size: 'modal-xl'
      },
      {
        component: 'institucional-email',
        title: 'Sobre el uso del correo electrónico institucional',
        size: 'modal-md'
      }
    ];
    this.modal = {
      component: 'none',
      title: 'Sin definir',
      size: 'none'
    };

    this.postsProcedure = [
      // No hay en json
      // {
      //   id: null,
      //   title: 'Reincorporación',
      //   link: 'https://estudiantes.ucontinental.edu.pe/oficinas/bienestar-universitario/programa-de-reincorporacion',
      //   // tslint:disable-next-line:max-line-length
      // tslint:disable-next-line:max-line-length
      //   description: 'Si dejaste tus estudios por uno o más periodos académicos, con el programa de reincorporación podrás facilitar tu retorno o reinserción  a la Universidad.',
      //   pending: false
      // },
      {
        id: 'https://estudiantes.ucontinental.edu.pe/wp-json/wp/v2/tramites/128',
        title: 'Traslado interno',
        action: '',
        link: 'https://estudiantes.ucontinental.edu.pe/tramites/traslado-interno/',
        description: 'Este proceso te permite cambiar de Escuela Académico Profesional dentro de nuestra Universidad.',
        pending: false
      },
      {
        id: 'https://estudiantes.ucontinental.edu.pe/wp-json/wp/v2/pages/554',
        title: 'Descuentos por convenios',
        action: '',
        link: 'https://estudiantes.ucontinental.edu.pe/oficinas/bienestar-universitario/servicio-social/descuentos-por-convenios/',
        // tslint:disable-next-line:max-line-length
        description: 'Nuestra Universidad suscribe convenios con diferentes organizaciones para generar descuentos a nuestros estudiantes beneficiarios.',
        pending: false
      },
      {
        id: 'https://estudiantes.ucontinental.edu.pe/wp-json/wp/v2/tramites/1109',
        title: 'Modificación de cuota inicial',
        action: '',
        link: 'https://estudiantes.ucontinental.edu.pe/tramites/solicitar-la-modificacion-de-tu-cuota-inicial/',
        description: 'Esta solicitud te permitirá reducir la cantidad de créditos a pagar en la primera cuota.',
        pending: false
      },
      {
        id: 'https://estudiantes.ucontinental.edu.pe/wp-json/wp/v2/tramites/129',
        title: 'Reserva de matrícula',
        action: '',
        link: 'https://estudiantes.ucontinental.edu.pe/tramites/reserva-de-matricula/',
        // tslint:disable-next-line:max-line-length
        description: 'Mediante este trámite ejerces tu derecho de postergar tu matrícula y  así evitar la pérdida de tu condición de estudiante en nuestra Universidad.',
        pending: false
      },
    ];
  }

  /**
   * Obtiene los requerimientos pendientes de un estudiante
   */
  getpostulantRequeriments() {
    const std = {
      college: this.student.profile.college.id,
      department: this.student.profile.department,
      person_id: this.student.person_id,
      program: this.student.profile.program.id,
    };
    this.requerimentService.query(std, false)
      .subscribe(
        (data) => {
          if (data != null) {
            this.requeriments = data;
          }
        },
        (err) => console.log(err)
      );
  }

  /**
   * Obtiene todas las retenciones del estudiante
   */
  getRetentions(): void {
    this.retentionService.getRetencionByPidm(this.student.person_id)
      .subscribe(
        (data) => {
          this.retentions = data;
          this.setPostsPending();
        },
        (err) => {
          console.log(err);
          console.log('Error al obtener las retenciones');
        }
      );
  }

  /**
   * Inicializamos la lista de pendientes
   */
  setPostsPending(): void {
    this.postsPending = [
      {
        id: 'https://estudiantes.ucontinental.edu.pe/wp-json/wp/v2/tramites/124',
        title: 'Solicita tu cambio de plan de estudios',
        link: 'https://estudiantes.ucontinental.edu.pe/tramites/cambio-de-plan/',
        action: '',
        // tslint:disable-next-line:max-line-length
        description: 'A través de este trámite podrás solicitar el cambio del plan de estudios con el que iniciaste por uno vigente o actual, y así culminar tus estudios universitarios.',
        pending: this.retentionActive('07')
      },
      {
        id: 'https://estudiantes.ucontinental.edu.pe/wp-json/wp/v2/tramites/1738',
        title: 'Solicita tu permanencia en tu plan de estudios',
        action: '',
        link: 'https://estudiantes.ucontinental.edu.pe/tramites/permanencia-de-plan-de-estudios/',
        description: 'Con este trámite, podrás solicitar tu permanencia en el plan de estudios  que sigues actualmente.',
        pending: this.retentionActive('08')
      },
    ];
  }

  /**
   * Devuelve el total de retenciones, contando  codigo 01 y 04 como uno
   */
  getAllRetentions(): number {
    let contador = this.retentions.length;
    if (this.retentions.filter(f => f.hlddCode === '01' || f.hlddCode === '04').length === 2 ) {
      contador--;
    }
    return contador;
  }

  /**
   * Verificamos que tenga la retención
   */
  retentionActive(code: string): boolean {
    for (const retention of this.retentions) {
      if (retention.hlddCode === code) {
        return true;
      }
    }
    return false;
  }

  /**
   * Define el titulo y el modal a abrir
   * @param component componente name
   */
  openModal(component: string): void {
    for (const item of this.items) {
      if (item.component === component) {
        if (item.component === 'document-pending') {
          this.getpostulantRequeriments();
        }
        this.modal = item;
      }
    }
    $('#modal-step-1').modal('show');
  }

  /**
   * Verifica y devuelve si el estudiante no tiene grupo de inscripción
   */
  noInscriptionGroup(): boolean {
    if (this.inscriptionGroup == null || this.inscriptionGroup === undefined) {
      return false;
    }
    return true;
  }
}

interface WpInfo {
  id: string;
  title: string;
  link: string;
  description: string;
  pending: boolean;
  action: string;
}

interface PdfInfo {
  url: string;
  title: string;
}
