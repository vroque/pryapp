import { Component, Input, OnInit, ɵConsole } from '@angular/core';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { EconomicStatus } from '../../../shared/services/accounting/economicStatus';
import { PaymentSimulatorService } from '../../../shared/services/accounting/paymentSimulator.service';
import { PaymentSimulator } from '../../../shared/services/accounting/paymentSimulator';
import { EconomicStatusService } from '../../../shared/services/accounting/economicStatus.service';
import { Student } from '../../../shared/services/academic/student';
@Component({
  providers: [BackofficeConfigService, PaymentSimulatorService, EconomicStatusService],
  selector: 'enrollment-step2',
  templateUrl: './step2.component.html'
})
export class EnrollmentStep2Component implements OnInit {
  @Input() title: string;
  @Input() term: string; // periodo actual
  @Input() student: Student;
  debts: EconomicStatus[] = [];
  payments: EconomicStatus;
  enrollmentDebt: EconomicStatus;
  payment: PaymentSimulator;
  paymentLoader: LoadHelper = {};
  stimatedDebt: number;
  errorMessage: string;
  creditsNumberByPlan = 0;
  loading = true;

  constructor(
    private paymentSimulatorService: PaymentSimulatorService,
    private economicStatusService: EconomicStatusService,
    private config: BackofficeConfigService,
  ) {}

  ngOnInit() {
    this.getDebts();
    // Al inicio verificar deudas y documentos pendientes
    this.getSimluatorPaymentDetails();
  }

  /**
   * Obtiene todas sus deudas del estudiante
   */
  getDebts(): void {
    this.economicStatusService.getGroupedDebtsPayments(this.term, this.student.person_id, 'debts')
      .subscribe(
        (data) => {
          data = data.filter( f => f.term.replace('-', '').concat('0') === this.term).filter( x => x.concept_id.toUpperCase() === 'MAT');
          if (data != null && data.length > 0) {
            this.enrollmentDebt = data[0];
            this.debts = (data);
          } else {
            this.getPayment();
          }
        },
        (err) => {
          console.log(err);
          this.errorMessage = 'Error al obtener Deudas';
        }
      );
  }

  /**
   * Obtiene los pagos
   */
  getPayment(): void {
    this.economicStatusService.getGroupedDebtsPayments(this.term, this.student.person_id, 'payments')
    .subscribe(
      (data) => {
        const enrolmentConcepts = ['C00', 'C01', 'DES', 'RMA', 'CAR', 'TL1', 'EXM', 'SME', 'GA1', 'IN1'];
        data = data.filter( f => enrolmentConcepts.indexOf(f.concept_id.toUpperCase()) > -1);
        if (data != null && data.length > 0) {
          const payment: EconomicStatus = data[0];
          payment.total = 0;
          data.forEach(f => {
            payment.total = payment.total + f.ammount;
          });
          this.payments = payment;
        }
        console.log(' => ', this.payments);
      },
      (err) => {
          console.log(err);
          this.errorMessage = 'Error al obtener Pagos';
        }
    );
  }

  /**
   * Diferencias Deudas de matricula (c00, c01, etc)
   */
  inspectDebts(debts: EconomicStatus[]): void {
    for (const debt of debts) {
      if (debt.details_grouped_gebts.length >= 2) {
        let hasC00 = false;
        let hasC01 = false;
        for (const detail of debt.details_grouped_gebts) {
          if (!hasC00) {
            hasC00 = (detail.concept_id.toLowerCase() === 'c00');
          }
          if (!hasC01) {
            hasC01 = (detail.concept_id.toLowerCase() === 'c01');
          }
        }
        if (hasC00 && hasC01) {
          this.enrollmentDebt = debt;
        } else {
          this.debts.push(debt);
        }
      }
    }
  }

  /**
   * Obtener Información de simulador
   */
  getSimluatorPaymentDetails(): void {
    this.paymentLoader = {
      isActive: true,
      isLoading: true,
    };
    this.paymentSimulatorService.get()
      .subscribe(
        (data) => {
          if (data != null && data.term.toString() === this.term) {
            this.payment = data;
          }
        },
        (error) => {
          this.paymentLoader.hasError = error;
          this.paymentLoader.isLoading = false;
        },
        () => {
          this.paymentLoader.isActive = false;
          this.paymentLoader.isLoading = false;
          if (this.payment != null && this.payment.term.toString() === this.term) {
            this.stimatedDebt = this.calcStimatedDebt();
          }
        },
    );
    this.loading = false;
  }

  calcStimatedDebt(): number {
    this.creditsNumberByPlan = this.payment.department === 'UREG' ? 4 : 5;

    const total = this.payment.credit_cost * this.creditsNumberByPlan
                + this.payment.enrollment_cost
                + this.payment.student_card_cost
                + this.payment.health_insurance_cost;
    return total;
  }

  goContiPay(): void {
    let paymentId: string;
    this.economicStatusService.getPaymentOnlineID(this.enrollmentDebt)
      .subscribe(
        (paymentID) => paymentId = paymentID.payment_online_id,
        (err) => console.log(err),
        () => {
          window.open(`${this.config.PAYMENT_GATEWAY}${paymentId}`, '_blank');
          // this.loading = false;
        },
      );
  }

  /**
   * Devuelve si tiene o no deudas generadas
   */
  hasDebts(): boolean {
    return this.enrollmentDebt ? true : false;
  }

  /**
   * Devuelve si tiene o no deuda estimada
   */
  hasPayment(): boolean {
    return (this.payments && !this.hasDebts()) ? true : false;
  }

  /**
   * Devuelve si tiene o no deuda estimada
   */
  hasEstimatedDebt(): boolean {
    return (this.stimatedDebt && !this.hasDebts()  && !this.hasPayment()) ? true : false;
  }

}

interface LoadHelper {
  isActive?: boolean;
  isLoading?: boolean;
  hasError?: string;
}
