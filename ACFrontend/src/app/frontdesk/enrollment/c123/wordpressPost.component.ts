import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  providers: [],
  selector: 'enrollment-wordpress-post',
  templateUrl: './wordpressPost.component.html',
})
export class EnrollmentWpPostComponent implements OnInit {
  @Input()url: string;
  // tslint:disable-next-line: variable-name
  private _id: string;

  @Input('id')
  set id(id: string) {
     this._id = id;
     if (id) {
      this.getInfo();
     }
  }
  get id(): string { return this._id; }

  post: any;

  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit() {
    // this.getInfo();
  }

  getInfo() {
    this.post = null;
    fetch(this.id)
      .then(res => res.json())
      .then(
        data => {
          const htmlDirty = data.content.rendered;
          const cleanHtml = this.vcPseudoParser(htmlDirty, data.imagenes_contenido);
          const safeHtml = this.sanitizer.bypassSecurityTrustHtml(cleanHtml);
          data.content.rendered = safeHtml;
          this.post = data;
        }
      );
  }

  vcPseudoParser(dirtyHtml: string, postImageUrls?: string[]): string {
    postImageUrls = postImageUrls || [];
    // console.log('img list ', postImageUrls);
    // Cambiar las imagenes del VC por imagenes html
    const postImageTags = dirtyHtml.match(/\[vc_single_image[^\]]*\]/g) || [];
    for (const text of postImageTags) {
      const postImages = text.match(/image=&#([0-9]{2,5});([0-9]{2,5})&#/) || [];
      // console.log('img list ', postImages);
      const id = postImages[2] || 0;
      // traer la imagen real
      for (const realImg of postImageUrls) {
        if (realImg[id]) {
          dirtyHtml = dirtyHtml.replace(text, `<div class="img-holder"><img src="${realImg[id]}" alt="${id}"><div>`);
        }
      }
    }
    // Cambiar las caberas de los pasos por algo legible
    const poststepSectionTags = dirtyHtml.match(/\[vc_tta_section[^\]]*\]/g) || [];
    for (const text of poststepSectionTags) {
      const postStepTitles = text.match(/title=&#([0-9]{2,5});([a-zA-Z0-9 ]{2,10}):&#/) || [];
      const title = postStepTitles[2] || null;
      if (title) {
        dirtyHtml = dirtyHtml.replace(text, `<article class="step-layout"> <h4 class="step-title">${title}</h4>`);
      }
    }
    // remplazar los cierres de vc_tta_section endingd
    dirtyHtml = dirtyHtml.replace(/\[\/vc_tta_section\]/g, '</article>');
    // console.log('preprocess', dirtyHtml);
    // remplazar pasos
    dirtyHtml = dirtyHtml.replace(/\[vc_column_text\]/g, '<p class="paragraph">');
    dirtyHtml = dirtyHtml.replace(/\[\/vc_column_text\]/g, '</p>');
    // remplazar separador ... Really??
    dirtyHtml = dirtyHtml.replace(/\[vc_separator\]/g, '<hr class="separator">');
    // quitamos [vc_ ] [/vc_ ] de visul conposer

    // remove all vc things
    dirtyHtml = dirtyHtml.replace(/\[vc_[^\]]*\]|\[\/vc_[^\]]*\]/g, '');
    // others
    // dirtyHtml = dirtyHtml.replace(/ src=/, 'no-src=');
    // dirtyHtml = dirtyHtml.replace(/ data-src=/, 'src=');

    return dirtyHtml;
  }
}
