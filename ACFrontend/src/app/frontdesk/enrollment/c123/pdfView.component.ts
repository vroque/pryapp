import { Component, Input, OnInit } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  providers: [],
  selector: 'enrollment-pdf-viewer',
  templateUrl: './pdfView.component.html',
})
export class ModalPdfViewerComponent implements OnInit {
  // @Input()type: string;
  private _type: string;

  @Input('type')
  set type(type: string) {
     this._type = type;
     if (type) {
      this.pdf = this.pdfs[this.type] || null;
      const dirtyHtml = `
      <object data="${this.pdf.url}" type="application/pdf" width="100%" height="100%">
          <embed src="${this.pdf.url}" type="application/pdf"  width="100%" height="100%" />
      </object>
      `;
      this.html = this.sanitizer.bypassSecurityTrustHtml(dirtyHtml);
     }
  }
  get type(): string { return this._type; }

  html: SafeHtml;
  pdfs: any;
  pdf: any;

  constructor(
    private configService: ConfigService,
    private sanitizer: DomSanitizer,
  ) {}

  ngOnInit() {
    this.pdfs = {
      'cambioDePlan': {
        url: `${this.configService._BASE_URI}Content/static/pdfs/matricula123/cambioDePlan.pdf`,
        title: 'Cambio de Plan de estudios 2015 al 2018'
      },
      'reincorporacion': {
        url: `${this.configService._BASE_URI}Content/static/pdfs/matricula123/reincorporacion.pdf`,
        title: 'Reincorporación'
      },
      'trica': {
        url: `${this.configService._BASE_URI}Content/static/pdfs/matricula123/trica.pdf`,
        title: 'Asignaturas desaprobadas por segunda a más veces'
      },
    };
  }
}
