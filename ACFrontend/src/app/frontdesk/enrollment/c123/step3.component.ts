import { Component, Input, OnInit } from '@angular/core';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { InscriptionGroup } from '../../../shared/services/enrollment/inscription-group';
import { Student } from '../../../shared/services/apoderapp/student';

declare var $: any;

@Component({
  providers: [BackofficeConfigService, ],
  selector: 'enrollment-step3',
  templateUrl: './step3.component.html'
})
export class EnrollmentStep3Component implements OnInit {

  @Input() title: string;
  @Input() student: Student;
  @Input() inscriptionGroup: InscriptionGroup;
  items: {component: string, title: string}[];
  modal: { component: string, title: string };
  postsEnroll: WpInfo[];
  requestMultiModal: WpInfo;
  postsRectify: WpInfo[];
  postId: number;
  postAction: string;
  today = new Date();

  constructor() {}
  ngOnInit() {
    this.items = [
      {
        component: 'schedule-student',
        title: 'Vertifica tu Horario'
      },
      {
        component: 'economic-status',
        title: 'Estado económico'
      },
      {
        component: 'satisfaction-survey',
        title: 'Encuesta de satisfacción'
      },
      {
        component: 'banner-enrollment',
        title: 'Matrícula'
      }
    ];
    this.modal = {
      component: 'none',
      title: 'Sin definir'
    };

    this.requestMultiModal = {
      id: 'https://estudiantes.ucontinental.edu.pe/wp-json/wp/v2/tramites/383',
      title: 'Solicita llevar asignatura multimodal',
      action: '',
      link: 'https://estudiantes.ucontinental.edu.pe/tramites/asignatura-multimodal/',
      // tslint:disable-next-line:max-line-length
      description: 'Este trámite te permitirá llevar una asignatura de tu plan de estudios en una modalidad diferente a la que perteneces.',
      pending: false
    };

    this.postsEnroll = [
      {
        id: 'https://estudiantes.ucontinental.edu.pe/wp-json/wp/v2/tramites/380',
        title: 'Solicita llevar asignatura dirigida',
        action: '',
        link: 'https://estudiantes.ucontinental.edu.pe/tramites/asignatura-dirigida/',
        // tslint:disable-next-line:max-line-length
        description: 'Las asignaturas dirigidas se desarrollan a través de horarios personalizados. Para ello, es importante la coordinación constante con el docente asignado. Ten en cuenta que solo puedes llevar como máximo tres asignaturas dirigidas durante tu vida académica.',
        pending: false
      },
    ];

    this.postsRectify = [
      {
        id: 'https://estudiantes.ucontinental.edu.pe/wp-json/wp/v2/tramites/378',
        title: 'Rectificación de matrícula',
        action: '',
        link: 'https://estudiantes.ucontinental.edu.pe/tramites/rectificacion-de-matricula/#guia',
        // tslint:disable-next-line:max-line-length
        description: 'Este trámite te permite incluir, cambiar y/o retirar asignaturas luego de haber  realizado el proceso de matrícula de manera regular. Para solicitarlo, no olvides revisar antes los horarios y asignaturas disponibles.',
        pending: false
      },
    ];
  }

  /**
   * Define el titulo y el modal a abrir
   * @param component componente name
   */
  openModal(component: string): void {
    for (const item of this.items) {
      if (item.component === component) {
        this.modal = item;
      }
    }
    $('#modal-step-3').modal('show');
  }

  /**
   * Verifica y devuelve si el estudiante no tiene grupo de inscripción
   */
  noInscriptionGroup(): boolean {
    if (this.inscriptionGroup == null || this.inscriptionGroup === undefined) {
      return false;
    }
    return true;
  }

  /**
   * Cierra el modal de mensaje para multimodal y abre el respectivo modal
   */
  modalAutoservicioBanner(): void {
    $('#modal-autoservicio-banner').modal('hide');
    $('#modal-worpress-info').modal('show');
  }
}

interface WpInfo {
  id: string;
  title: string;
  link: string;
  description: string;
  pending: boolean;
  action: string;
}
