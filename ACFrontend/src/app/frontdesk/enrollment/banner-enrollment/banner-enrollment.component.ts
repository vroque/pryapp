import { Component, OnInit } from '@angular/core';

import { AcceptedTerms } from '../../../shared/services/accepted-terms/accepted-terms';
import { AcceptedTermsService } from '../../../shared/services/accepted-terms/accepted-terms.service';
import { BannerEnrollment } from '../../../shared/services/banner-enrollment/banner-enrollment';
import { BannerEnrollmentService } from '../../../shared/services/banner-enrollment/banner-enrollment.service';
import { ConfigService } from '../../../config/config.service';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { Student } from '../../../shared/services/academic/student';

declare var $: any;

@Component({
  selector: 'banner-enrollment',
  templateUrl: './banner-enrollment.component.html',
  providers: [
    BannerEnrollmentService, ConfigService, AcceptedTermsService
  ]
})
export class BannerEnrollmentComponent implements OnInit {
  loading = false;
  processing = false;
  link = 'https://campusvirtualxe11.continental.edu.pe/StudentRegistrationSsb';
  bannerEnrollment: BannerEnrollment = new BannerEnrollment();
  acceptedTerms: AcceptedTerms;
  student: Student;

  showModal: boolean;
  constructor(
    private bannerEnrollmentService: BannerEnrollmentService,
    private acceptedTermsService: AcceptedTermsService,
    private configService: ConfigService) {
  }

  ngOnInit() {
    this.hasPendingTerms();
    this.student = StudentFactory.getInstance().get();
  }

  /**
   * Verify if a student has terms for enrollment pending
   */
  hasPendingTerms(): void {
    this.loading = true;
    this.bannerEnrollmentService.hasPendingTerms()
      .subscribe(
        (res) => this.bannerEnrollment = res,
        (error) => {
          console.log(error);
          this.loading = false;
        },
        () => {
          this.loading = false;
          if (this.bannerEnrollment.termsAndConditions !== undefined && this.bannerEnrollment.termsAndConditions !== null) {
          }
        }
      );
  }

  acceptTerm(): void {
    this.processing = true;
    this.closeModal();
    this.acceptedTerms = new AcceptedTerms();
    this.acceptedTerms.termsId = this.bannerEnrollment.termsAndConditions.id;
    this.acceptedTerms.accepted = true;
    this.acceptedTerms.acceptedDate = new Date();
    this.acceptedTermsService.save(this.acceptedTerms)
      .subscribe(
        (res) => this.acceptedTerms = res,
        (error) => {
          console.log(error);
          this.processing = false;
        },
        () => {
          console.log('saved => ', this.acceptedTerms);
          this.processing = false;
          this.hasPendingTerms();
        }
      );
  }

  /**
   * Ocultar Modal
   */
  closeModal(): void {
    $('#modal-default').modal('hide');
  }
}
