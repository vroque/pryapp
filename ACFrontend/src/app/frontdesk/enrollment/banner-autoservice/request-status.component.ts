import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'bn-autoservice-request-status',
  templateUrl: '../../../shared/template/redirect.component.html',
})
export class BnAutoserviceRequestStatusComponent implements OnInit {
  link: string;

  constructor(@Inject(DOCUMENT) private document: any) {
    this.link = 'https://campusvirtualxe00.continental.edu.pe/ssomanager/c/SSB?pkg=bvgkptcl.P_DispView_Protocols';
  }

  ngOnInit() {
    this.document.location.href = this.link;
  }
}
