import { Component, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'bn-autoservice-new-request',
  templateUrl: '../../../shared/template/redirect.component.html',
})
export class BnAutoserviceNewRequestComponent implements OnInit {

  link: string;

  constructor(@Inject(DOCUMENT) private document: any) {
    this.link = 'https://campusvirtualxe00.continental.edu.pe/ssomanager/c/SSB?pkg=bvgkptcl.P_Disp_Apply_Protocol';
  }

  ngOnInit() {
    this.document.location.href = this.link;
  }
}
