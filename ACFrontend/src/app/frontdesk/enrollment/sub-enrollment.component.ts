import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sub-enrollment',
  template: '<submodule-menu moduleUri="matricula"></submodule-menu>',
})
export class SubEnrollmentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // Eliminar este valor cuando el usuario sale del catálogo de actividades extracurriculares, con el fin de
    // mostrar al estudiante las políticas de servicio.
    localStorage.removeItem('accept-terms-vuc');
  }

}
