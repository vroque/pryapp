import { Component, Input, OnInit } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Message } from 'primeng/primeng';

import { ConfigService } from '../../../../config/config.service';

declare var $: any;

@Component({
  providers: [ConfigService],
  selector: 'modal-message-course',
  templateUrl: './modal-message.component.html',
})
export class ModalMessageComponent implements OnInit {

  @Input() term: string = null;
  @Input() forceMessage = false;
  primeMsg: Message[] = [];
  fech: Date;

  constructor(private config: ConfigService, private platformLocation: PlatformLocation) {
    this.platformLocation.onPopState(() => $('.modal').modal('hide'));
  }

  ngOnInit() {
    this.primeMsg.push({
      detail: 'Los datos a continuación no tienen validez a efectos de matrícula, el objetivo es orientarte sobre los '
        + 'horarios de las asignaturas disponibles según el período académico',
      severity: 'info',
      summary: 'Mensaje',
    });
    this.fech = new Date();
    if (this.forceMessage) {
      localStorage.setItem('messageCourseOffer', 'true');
      $('#appModal').modal('show');
    } else if (!localStorage.getItem('messageCourseOffer') && this.term != null) {
      localStorage.setItem('messageCourseOffer', 'true');
      $('#appModal').modal('show');
    }
  }

}
