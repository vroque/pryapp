import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AcademicOfferService } from '../../../../shared/services/course-display/AcademicOffer.service';
import { ConfigService } from '../../../../config/config.service';

import { AcademicOffer } from '../../../../shared/services/course-display/AcademicOffer';

@Component({
  providers: [AcademicOfferService, ConfigService],
  selector: 'academic-offer',
  templateUrl: './academic-offer.component.html'
})
export class AcademicOfferComponent implements OnInit {

  courses: AcademicOffer[];
  nameCourse: string;
  loading = true;
  errorMessage: string;
  fech: Date;
  message: string;
  term: string;

  constructor(private academicService: AcademicOfferService,
              private route: ActivatedRoute, private router: Router,
              private config: ConfigService, private redirect: Router) {
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.getCourses(params.list);
    });
  }

  getCourses(data: string): void {
    const dataUrl: string[] = data.split('-');
    this.academicService.get(dataUrl[0], dataUrl[1], dataUrl[2])
      .subscribe(
        (responseData) => {
          this.courses = responseData;
          if (responseData.length > 0) {
            this.nameCourse = this.courses[0].namecourse;
            this.term = this.courses[0].term;
          } else {
            this.errorMessage = 'Usted no cuenta con una oferta académica para esta asignatura.';
          }
          this.loading = false;
        },
        (error) => {
          console.log(error);
          this.errorMessage = 'No se encontró asignaturas disponibles';
          this.loading = false;
        },
      );
  }

  redirectSchedule(course: AcademicOffer) {
    const dataUrl = course.nrc + '-' + course.term + '-' + course.subject + '-' + course.course + '-' + course.codliga;
    if (course.vacanciesavailable > 0) {
      const link: Array<string> = [`frontdesk/matricula/asignaturas-disponibles/horarios/${dataUrl}`];
      this.router.navigate(link);
    }
  }
}
