import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { AcademicOfferService } from '../../../../shared/services/course-display/AcademicOffer.service';
import { ConfigService } from '../../../../config/config.service';
import { ScheduleService } from '../../../../shared/services/academic/schedule.service';

import { Schedule } from '../../../../shared/services/academic/schedule';
import { AcademicOffer } from '../../../../shared/services/course-display/AcademicOffer';

@Component({
  providers: [ScheduleService, AcademicOfferService, ConfigService],
  selector: 'schedules-available',
  templateUrl: './schedules-available.component.html',
})

export class SchedulesAvailableComponent implements OnInit {

  dataURL: { nrcpadre: any; term: any; subject: any; course: any; codliga: any; };
  schedule: Schedule[] = [];

  scheduleBackup: Schedule[] = [];
  coursesChildren: AcademicOffer[] = [];
  contentChild = true;

  parentSchedule: Schedule[] = [];
  contentParentSchedule: boolean;

  childSchedule: Schedule[] = [];
  contentChildSchedule: boolean;
  nrcChild: string;

  courseName: string;
  errorMessage: string;
  loadingSchedule = true;
  loading = true;
  fech: Date;

  constructor(private route: ActivatedRoute, private scheduleService: ScheduleService,
              private academicService: AcademicOfferService, private config: ConfigService) {
  }

  ngOnInit() {
    this.fech = new Date();
    this.route.params.forEach((params: Params) => {
      const paramUrl: string[] = params.data.split('-');
      this.dataURL = {nrcpadre: paramUrl[0], term: paramUrl[1], subject: paramUrl[2], course: paramUrl[3], codliga: paramUrl[4]};
    });

    this.getParentSchedule(this.dataURL.nrcpadre, this.dataURL.term, this.dataURL.subject, this.dataURL.course, this.dataURL.codliga);
    this.getCoursesChildren(this.dataURL.nrcpadre, this.dataURL.term, this.dataURL.subject, this.dataURL.course, this.dataURL.codliga);
  }

  /**
   * Mensaje de error al no obtener un horario valido y terminar la carga
   */
  getErrorMessage(): void {
    this.errorMessage = 'No se encontraron horarios disponibles';
    this.loading = false;
  }

  /**
   * Obtiene el horario del padre
   * @param nrcpadre
   * @param term
   * @param subject
   * @param course
   * @param codliga
   */
  getParentSchedule(nrcpadre: string, term: string, subject: string, course: string, codliga: string) {
    this.scheduleService.getScheduleOffer(nrcpadre, term, subject, course, codliga)
      .subscribe(
        (responseData) => {
          if (responseData != null && responseData.length > 0) {
            this.parentSchedule = responseData;
            this.courseName = this.getCourseName(this.parentSchedule);
            this.contentParentSchedule = this.iscontentSchedule(this.parentSchedule);
            if (this.contentParentSchedule) {
              this.parentSchedule = this.addDataParent(this.parentSchedule, true);
              this.scheduleBackup = JSON.parse(JSON.stringify(responseData));
            } else {
              this.getErrorMessage();
              this.loadingSchedule = false;
            }
          } else {
            this.getErrorMessage();
            this.loadingSchedule = false;
          }
        },
        (error) => {
          this.getErrorMessage();
          this.loadingSchedule = false;
        },
      );
  }

  /**
   * Añade un horario a otro
   * @param data Horario
   * @param type true=es el horario del padre
   */
  addDataParent(data: Schedule[], type: boolean): Schedule[] {
    for (const schedule of data) {
      for (const item of schedule.courses) {
        item.parent = type;
      }
    }
    return data;
  }

  /**
   * Obtiene los datos de la asignatura
   * @param schedule Horario del cual obtener
   */
  getCourseName(schedule: Schedule[]): string {
    for (const item of schedule) {
      if (item.courses.length > 0) {
        return item.courses[0].name;
      }
    }
  }

  /**
   * Obtiene los ligados de un horario
   * @param nrcpadre
   * @param term
   * @param subject
   * @param course
   * @param codliga
   */
  getCoursesChildren(nrcpadre: string, term: string, subject: string, course: string, codliga: string) {
    this.academicService.getCoursesChildren(nrcpadre, term, subject, course, codliga)
      .subscribe(
        (responseData) => {
          this.coursesChildren = responseData;
          if (this.coursesChildren.length > 0) {
            this.getChildSchedule(this.coursesChildren[0].nrc);
          } else {
            this.contentChild = false;
            this.schedule = this.scheduleBackup;
            this.loading = false;
            this.loadingSchedule = false;
          }
        },
        (error) => {
          this.loading = false;
          this.loadingSchedule = false;
          this.schedule = this.parentSchedule;
        },
      );
  }

  /**
   * Obtiene el horario de un hijo
   * @param nrc
   */
  getChildSchedule(nrc: string) {
    this.loadingSchedule = true;
    this.schedule = [];
    if (nrc === 'reset') {
      setTimeout(() => {
        this.parentSchedule = this.scheduleBackup;
        this.loadingSchedule = false;
      }, 1000);
    } else {
      setTimeout(() => {
        // tslint:disable-next-line:max-line-length
        this.scheduleService.getScheduleOffer(nrc, this.dataURL.term, this.dataURL.subject, this.dataURL.course, this.dataURL.codliga)
          .subscribe(
            (responseData) => {
              this.nrcChild = nrc;
              this.childSchedule = responseData;
              this.childSchedule = this.addDataParent(this.childSchedule, false);
              this.addChildToFather(responseData);
              this.loading = false;
              this.loadingSchedule = false;
            },
            (error) => {
              this.loading = false;
            },
          );
      }, 1000);
    }
  }

  /**
   * Añade el horario de un hijo al del padre
   * @param childSchedule Horario hijo
   */
  addChildToFather(childSchedule: Schedule[]) {
    const scheduleChilParent: Schedule[] = JSON.parse(JSON.stringify(this.scheduleBackup));
    for (const child of childSchedule) {
      if (child.courses.length > 0) {
        for (const father of scheduleChilParent) {
          if (father.day === child.day) {
            for (const item of child.courses) {
              father.courses.push(item);
            }
          }
        }
      }
    }
    this.schedule = scheduleChilParent;
  }

  /**
   * Devuelve (true/false) si el horario tiene asignaturas en sus días
   * @param schedule
   */
  iscontentSchedule(schedule: Schedule[]): boolean {
    let validate = false;
    for (const iterator of schedule) {
      if (iterator.courses.length > 0) {
        validate = true;
      }
    }
    return validate;
  }
}
