import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

import { ConfigService } from '../../../config/config.service';
import { CourseOfferedService } from '../../../shared/services/course-display/courseOffered.service';
import { CourseOffered } from '../../../shared/services/course-display/courseOffered';
import { OffertAcademic } from '../../../shared/services/course-display/offertAcademic';
import { Callout } from '../../../shared/components/callout/callout';

declare var $: any;
@Component({
  providers: [CourseOfferedService, ConfigService, DatePipe],
  selector: 'subject-available',
  templateUrl: './subject-available.component.html'
})
export class SubjectAvailableComponent implements OnInit {

  lisCoursesOffered: CourseOffered[];
  loading = true;
  error = false;
  fech: Date;
  cycles: number[] = [];
  callout: Callout;
  terms: string;
  termActive: string;

  constructor(private courseService: CourseOfferedService,
    private config: ConfigService, private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.getCourses();
  }

  /**
   * Obtiene la oferta académica para un estudiante
   */
  getCourses(): void {
    this.fech = new Date();
    this.courseService.get()
      .subscribe(
      (response) => {
        if (response !== null && response.length > 0) {
          this.lisCoursesOffered = response;
          if (this.lisCoursesOffered[0].active && this.lisCoursesOffered[0].offertAcademic.length > 0) {
            this.lisCoursesOffered = this.validateTermAsignature(this.lisCoursesOffered);
            this.separateByClycle();
          } else {
            this.callout = {
              title: 'Mensaje',
              type: 'warning',
              data: ['Por el momento aún no cuenta con asignaturas disponibles; disponible sólo de '
                + this.datePipe.transform(this.lisCoursesOffered[0].startFech, 'h:mm a')
                + ' a ' + this.datePipe.transform(this.lisCoursesOffered[0].endFech, 'h:mm a')]
            };
            this.error = true;
          }
        } else {
          this.callout = {
            title: 'Alerta',
            type: 'warning',
            data: [
              'No se pudo obtener las asignaturas.',
              'Es probable que aún no esten disponibles, vuelva a intentarlo dentro de un momento.'
            ]
          };
          this.error = true;
        }
        this.loading = false;
      },
      (error) => {
        this.callout = {
          title: 'Alerta',
          type: 'warning',
          data: ['No se pudo obtener las asignaturas.', 'Tenemos problemas al obtener las asignaturas, estamos trabajando en resolverlo.']
        };
        this.error = true;
        this.loading = false;
      },
    );
  }

  /**
   * Valida que las ofertas tengan asignaturas
   * en caso de que el período no tenga asignaturas se elimina
   */
  validateTermAsignature(offerts: CourseOffered[]): CourseOffered[] {
    for (let position = 0; position < offerts.length; position++) {
      if (offerts[position].offertAcademic == null || offerts[position].offertAcademic.length === 0) {
        offerts.splice(position, 1);
        position--;
      }
    }
    return offerts;
  }

  /**
   * Ordena las ofertas por ciclo(período)
   * omite los duplicados
   */
  separateByClycle(): void {
    this.termActive = this.lisCoursesOffered[0].term;
    let duplicate: boolean;
    // eliminamos las ofertas duplicadas
    for (const iterator of this.lisCoursesOffered) {
      for (const offert of iterator.offertAcademic) {
        duplicate = false;
        if (this.cycles.length === 0) {
          this.cycles.push(parseInt(offert.cycle, 10));
        } else {
          for (const cycle of this.cycles) {
            if (cycle === parseInt(offert.cycle, 10)) {
              duplicate = true;
            }
          }
          if (!duplicate) {
            this.cycles.push(parseInt(offert.cycle, 10));
          }
        }
      }
    }

    // order by ascending
    this.cycles.sort(function (a, b) { return a - b; });

    let courses: OffertAcademic[];
    let cycleName: string;
    // inicializamos las asignaturas por ciclo
    for (const iterator of this.lisCoursesOffered) {
      iterator.coursesByCycles = [];
    }

    for (const cycle of this.cycles) {
      courses = [];
      for (const item of this.lisCoursesOffered) {
        for (const course of item.offertAcademic) {
          if (cycle === parseInt(course.cycle, 10)) {
            courses.push(course);
            cycleName = course.cycle;
          }
        }
        item.coursesByCycles.push({ cycle: cycleName, courses: courses });
      }
    }
    this.terms = this.getTermMessage();
  }

  /**
   * Devuelve una cadena con el(los) período(s)
   * con las ofertas académicas
   */
  getTermMessage(): string {
    const TERMSCOUNT = this.lisCoursesOffered.length;
    let terms = '';
    for (let x = 0; x < this.lisCoursesOffered.length; x++) {
      if (x === 0) {
        terms = terms + this.lisCoursesOffered[x].term;
      } else {
        terms = terms + ' - ' + this.lisCoursesOffered[x].term;
      }
    }
    return terms;
  }
}
