import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/finally';

import { CategoryService } from '../../../shared/services/continental-project/category.service';
import { Category } from '../../../shared/services/continental-project/category.model';

@Component({
    selector: 'pry-front-categories',
    templateUrl: './categories.componen.html',
    providers: [CategoryService]
})

export class PRYCategoriesComponent implements OnInit {
    categories: Category[];
    categorySelected: Category;

    isLoadingCategories: boolean;

    constructor(private categoryService: CategoryService) { }

    ngOnInit() {
        this.getCategories();
    }

    /**
     * Gets categories
     */
    getCategories(): void {
        this.isLoadingCategories = true;
        this.categoryService.getAll()
            .finally(() => this.isLoadingCategories = false)
            .subscribe(
                (categories: Category[]) => this.categories = categories,
                (error) => console.log(error));
    }

    /**
     * Sets category
     * @param idCategory Id de la categoria
     */
    setCategory(categorySelected: Category): void {
        this.categorySelected = categorySelected;
    }
}
