import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/finally';

import { Activity } from '../../../shared/services/extracurricular-activities/activity.model';
import { ActivityService } from '../../../shared/services/extracurricular-activities/activity.service';
import { EnrolledProgramActivity } from '../../../shared/services/extracurricular-activities/enrolled-program-activity.model';
import { EnrollmentService } from '../../../shared/services/extracurricular-activities/enrollment.service';
import { Message } from 'primeng/primeng';
import { MessageServer } from '../../../shared/services/academic/messageServer';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { ExtracurricularEnrollmentService } from '../../../shared/services/banner-enrollment/extracurricular-enrollment.service';
import { isNullOrUndefined } from 'util';

@Component({
  providers: [ActivityService, EnrollmentService, ExtracurricularEnrollmentService],
  selector: 'vuc-activity-details',
  templateUrl: './details-activity-program.component.html',
})

export class DetailsActivityProgramComponent implements OnInit {
  activateEnrollment: boolean;              // Estado para habilitar el botón de matricularse
  activity: Activity;                       // Modelo de la actividad
  enrollments: EnrolledProgramActivity[];   // Matriculas del estudiante
  id: number;                               // Id de la actividad se obtiene mediante la URL
  isAlreadyRegistered: boolean;             // Estado que indica que ya se encuentra registrado en la actividad
  isBusy: boolean;                          // Estado que indica si el servicio se encuentra ocupado
  maxEnrollements: number;                  // Número máximo de matriculas de un estudiante
  messageServer: MessageServer;             // Model de respuesta del servidor
  primeMsg: Message[];                      // Mensajes que se muestra al usuario final
  student: Student;                         // Estudiante
  quantityEnrollments: number;              // Cantidad de matriculas (Programas + talleres)
  isLoadingEnrollments: boolean;

  constructor(private route: ActivatedRoute, private router: Router, private enrollmentService: EnrollmentService,
    private activityService: ActivityService, private extracurricularEnrollmentService: ExtracurricularEnrollmentService) { }

  ngOnInit(): void {
    this.maxEnrollements = 2;
    this.student = StudentFactory.instance.get();
    this.checkTerms();
    this.route.params.subscribe(params => this.id = params['idActivity']);
    this.getEnrollments();
    this.getActivity(this.id);
  }

  /**
   * Obtiene las matriculas del estudiante
   */
  getEnrollments(): void {
    this.isLoadingEnrollments = true;
    this.enrollmentService.getEnrollments()
      .finally(() => { this.isLoadingEnrollments = false; this.checkEnrollment(); })
      .subscribe(
        (enrollments) => this.enrollments = enrollments,
        (error) => console.log(error));
  }

  /**
   * Verifica si un estudiante haya aceptado un determinado terminos y condiciones
   */
  checkTerms(): void {
    this.extracurricularEnrollmentService.get(this.student.person_id)
      .subscribe(
        (acceptedTerms) => {
          if (acceptedTerms === null) {
            this.back();
          }
        },
        (error) => console.log(error));
  }

  /**
   * Regresa al catálogo de ejes y actividades
   */
  back(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  /**
   * Revisa si un estudiante se encuentra matriculado en un horario
   */
  private checkEnrollment(): void {
    if (!isNullOrUndefined(this.enrollments) && !isNullOrUndefined(this.activity)
      && !isNullOrUndefined(this.activity.programmingActivities)) {
      // Esta variable verifica que el estudiante ya se encuentra matriculado en la actividad consultada
      this.isAlreadyRegistered = this.enrollments.some(enrollment => enrollment.programmingActivity.idActivity === Number(this.id));
      // Cantidad de matriculas (Programas + talleres)
      this.quantityEnrollments = this.enrollments.length;

      // Determinar en que horario se encuentra matriculado si es que esta.
      if (this.activity.programmingActivities !== null && this.activity.programmingActivities !== undefined) {
        this.activity.programmingActivities.forEach(programming =>
          programming.isEnrolled = this.enrollments.some(enr => enr.idProgramActivity === programming.id));
      }
    }
  }

  /**
   * Registra a un estudiante en una actividad
   * @param idProgrammingActivity Id de la programación de la actividad
   * @param idSede Código de la sede (S01)
   * @param idSchedule => id del horario
   */
  enrollActiProg(idProgrammingActivity: number): void {
    if (!confirm('¿Segur@ que deseas matricularte en el horario seleccionado?')) { return; }

    this.isBusy = true;
    this.enrollmentService.enrollStudent(idProgrammingActivity)
      .finally(() => this.isBusy = false)
      .subscribe(
        (messageServer) => {
          if (messageServer.status === 'success') {
            let message: string;

            if (this.quantityEnrollments + 1 === 2) {
              message = `Ya te inscribiste en el máximo de actividades permitidas por periodo.`;
            } else {
              message = `Aún puedes inscribirte en una actividad extracurricular`;
            }

            const programming = this.activity.programmingActivities.filter(x => x.id === idProgrammingActivity)[0];
            programming.isEnrolled = true;
            programming.maxQuantityVacancy = programming.maxQuantityVacancy - 1;
            this.isAlreadyRegistered = true; // Ya se encuentra inscrito en esta actividad
            this.primeMsg = [{
              detail: messageServer.message, severity: messageServer.status, summary: messageServer.title
            }, {
              detail: message,
              severity: 'info',
              summary: 'Actividades restantes'
            }];
          } else {
            this.primeMsg = [{
              detail: messageServer.message, severity: messageServer.status, summary: messageServer.title
            }];
          }
        },
        (error) => console.log(error));
  }

  /**
   * Obtiene la actividad para un estudiante con sus respectivas programaciones de horarios
   * @param id => id de la actividad
   * @param idSedeActiProg => id de la sede en la que se consultara
   */
  getActivity(idActivity: number): void {
    this.isBusy = true;
    this.activityService.getProgrammmingActivities(idActivity)
      .finally(() => this.isBusy = false)
      .subscribe(
        (activity) => {
          this.activity = activity;

          // Para planes anteriores a 2015 se establecen como 2015
          let plan = Number(this.student.profile.term_catalg.substr(0, 4));
          if (plan <= 2015) {
            plan = 2015;
          }

          // Hacemos el cruce entre el plan del estudiante, el/los planes de la programación y las convalidaciones
          // de la actividad para determinar que se le va a mostrar a cada estudiante (si acredita proyección social,
          // extracurricular o no le acredita nada).
          this.activity.programmingActivities.forEach(programming => {
            if (programming.programmingActivityCurriculum.some(x => Number(x.idCurriculum.substr(0, 4)) === plan)) {
              if (activity.convalidationTypes.some(x => x.id === 1) && plan >= 2018) {
                programming.isSocialProjection = false;
                programming.isExtracurricular = true;
              } else if (activity.convalidationTypes.some(x => x.id === 2) && plan <= 2015) {
                programming.isSocialProjection = true;
                programming.isExtracurricular = false;
              } else {
                programming.isSocialProjection = false;
                programming.isExtracurricular = false;
              }
            }
          });

          // Revisar si ya se encuentra matriculado en la actividad que consulta
          this.checkEnrollment();
        },
        (error) => console.log(error));
  }
}
