import { Component, OnInit } from '@angular/core';

import { AcceptedTermsService } from '../../../shared/services/accepted-terms/accepted-terms.service';
import { AcceptedTerms } from '../../../shared/services/accepted-terms/accepted-terms';
import { ExtracurricularEnrollmentService } from '../../../shared/services/banner-enrollment/extracurricular-enrollment.service';
import { Axi } from '../../../shared/services/extracurricular-activities/axi.model';
import { AxiService } from '../../../shared/services/extracurricular-activities/axi.service';
import { Student } from '../../../shared/services/academic/student';
import { StudentFactory } from '../../../shared/services/academic/student.factory';
import { ExtracurricularEnrollment } from '../../../shared/services/banner-enrollment/extracurricular-enrollment';
declare var $: any;

@Component({
  providers: [AxiService, ExtracurricularEnrollmentService, AcceptedTermsService],
  selector: 'list-activities-programs.component',
  templateUrl: './list-activities-programs.component.html'
})

export class ListActivitiesProgramsComponent implements OnInit {
  extracurricularEnrollment: ExtracurricularEnrollment = new ExtracurricularEnrollment();
  acceptedTerms: AcceptedTerms;
  accepTerms: boolean;                        // Indicador si acepto los términos y condiciones (formulario)
  axis: Axi[];                                // Lista de ejes con sus actividades
  error: boolean;                             // Indica si existe algún error
  errorMessage: string;                       // Mensaje del error
  isBusy: boolean;                            // Indicador si el componente se encuentra ocupado
  student: Student;                           // Estudiante

  constructor(
    private axiService: AxiService, private extracurricularEnrollmentService: ExtracurricularEnrollmentService,
    private acceptedTermsService: AcceptedTermsService,
  ) { }

  ngOnInit(): void {
    this.student = StudentFactory.instance.get();
    this.checkTermAndCondition();
  }

  /**
   * Verifica si un estudiante tiene términos de inscripción extracurricular pendientes.
   */
  checkTermAndCondition(): void {
    this.isBusy = true;
    this.student = StudentFactory.instance.get();
    this.extracurricularEnrollmentService
      .checkTermAndCondition()
      .finally(() => (this.isBusy = false))
      .subscribe(
        data => {
          this.extracurricularEnrollment = data;
          if (!data.hasPendingTerms) {
            this.getAxis();
          }
        },
        error => console.log(error)
      );
  }

  /**
   * Registra los terminos y condiciones de un determinado estudiante
   */
  saveTerm(): void {
    this.acceptedTerms = new AcceptedTerms();
    this.acceptedTerms.termsId = this.extracurricularEnrollment.termsAndConditions.id;
    this.acceptedTerms.accepted = true;
    this.acceptedTerms.acceptedDate = new Date();
    this.acceptedTermsService.save(this.acceptedTerms).subscribe(
      res => {
        if (res !== null) {
          this.extracurricularEnrollment.hasPendingTerms = false;
          this.getAxis();
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  /**
   * Expande el eje seleccionado y contrae los otros
   * @param i Indice del eje
   */
  changeMenu(i: number): void {
    for (let index = 0; index < this.axis.length; index++) {
      if (i === index) {
        $('[data-divtoggle="content-' + index + '"]').slideToggle('fast');
      } else {
        $('[data-divtoggle="content-' + index + '"]').slideUp('fast');
      }
    }
  }

  /**
   * Obtiene los eje y sus respectivas actividades
   */
  getAxis(): void {
    this.isBusy = true;
    this.axiService.getAxis()
      .finally(() => (this.isBusy = false))
      .subscribe(
        (data) => {
          if (data === null || data === undefined) {
            this.error = true;
            this.errorMessage = 'Lo sentimos, por el momento no se disponen de actividades disponibles.';
          } else {
            this.axis = data;
          }
        },
        (error) => {
          this.error = true;
          this.errorMessage = 'Ocurrio un error, pruebe en otro momento.';
        }
      );
  }
}
