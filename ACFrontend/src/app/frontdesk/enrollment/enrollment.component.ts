import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../config/config.service';
import { PlatformLocation, Time } from '@angular/common';
import { StudentFactory } from '../../shared/services/academic/student.factory';

declare var $: any;

@Component({
  selector: 'enrollment-component',
  templateUrl: './enrollment.component.html',
})
export class EnrollmentComponent implements OnInit {
  student = StudentFactory.getInstance().get();
  baseUrl: string;
  option: string;
  // properties for the efect to steps chang color step 1
  stepClass = {
    step1: 'bg-primary text-white',
    step2: 'bg-white text-dark',
    step3: 'bg-white text-dark',
  };
  intervalId: any;

  constructor(
    private configService: ConfigService,
    private platformLocation: PlatformLocation,
  ) {
    this.baseUrl = configService._BASE_URI;
    this.platformLocation.onPopState(() => $('.modal').modal('hide'));
  }

  ngOnInit(): void {
    // Eliminar este valor cuando el usuario sale del catálogo de actividades extracurriculares, con el fin de
    // mostrar al estudiante las políticas de servicio.
    localStorage.removeItem('accept-terms-vuc');

    // optenemos la opción seleccionada por el estudiante
    if (localStorage.getItem('set-matricula')) {
      this.option = localStorage.getItem('set-matricula');
    } else {
      this.initEfectSecuency();
    }
  }

  /**
   * Definimos la opción a mostrar
   * @param option
   */
  setOption(option: string): void {
    this.option = option;
    localStorage.setItem('set-matricula', option);
    // Detiene el efecto de secuencia de colores
    clearInterval(this.intervalId);
  }

  /**
   * Inicia el proceso de efecto de cambio de color para realizarlo cada 2s
   */
  initEfectSecuency(): void {
    const active = 'bg-primary text-white';
    const dissable = 'bg-white text-dark';
    this.intervalId = setInterval(
      () => {
        this.stepClass.step1 = (this.stepClass.step1 === active) ? dissable :
                                (this.stepClass.step2 === dissable && this.stepClass.step3 === dissable) ? active : dissable;
        this.stepClass.step2 = (this.stepClass.step2 === active) ? dissable :
                                (this.stepClass.step1 === dissable && this.stepClass.step3 === dissable) ? active : dissable;
        this.stepClass.step3 = (this.stepClass.step3 === active) ? dissable :
                                (this.stepClass.step1 === dissable && this.stepClass.step2 === dissable) ? active : dissable;
        this.stepClass.step1 = (this.stepClass.step1 === dissable &&
                                this.stepClass.step2 === dissable && this.stepClass.step3 === dissable) ? active : dissable;
      }, 2000);
  }

}
