import { enableProdMode } from '@angular/core';
import { FrontdeskRoutingModule } from './frontdesk-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// dd
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';

// primeng modules
import { ChartModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputSwitchModule } from 'primeng/primeng';

// guards
import { HasAccessFullGuard } from '../shared/guards/has-access-full.guard';
import { HasAccessGuard } from '../shared/guards/has-access.guard';

// Google Analytics
import { Angulartics2Module } from 'angulartics2';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';

// shared components
import { SharedScheduleComponent } from '../shared/components/academic/schedule.component';
import { CAUBreadcrumbComponent } from '../shared/components/breadcrumb.component';
import { LoadingComponent } from '../shared/components/loading.component';
import { RedirectComponent } from '../shared/components/redirect.component';
import { CalloutComponent } from '../shared/components/callout/callout.component';
import { BORequestAbstractComponent } from '../shared/components/request/boRequestAbstract.component';
import { PollComponent } from '../shared/components/poll/poll.component';
import { InfoEnrollmentComponent } from '../shared/components/info-enrollment/info-enrollment.component';
import { SvgIconComponent } from '../shared/components/svg/svg-icon.component';
import { SvgImgComponent } from '../shared/components/svg/svg-img.component';

// module componentes
import { RequestFlowComponent } from '../shared/components/request/flow.component';
import { AdmisionComponent } from './admision/admision.component';
import { FrontdeskComponent } from './frontdesk.component';
import { MainComponent } from './main.component';
import { MenuListComponent } from './menulist.component';
import { OrientationCurriculumComponent } from './orientation/curriculum.component';
import { OrientationOptionComponent } from './orientation/option.component';
import { OrientationComponent } from './orientation/orientation.component';
import { RequestAbstractComponent } from './request/abstract.component';
import { RequestDocumentComponent } from './request/document.component';
import { RequestComponent } from './request/request.component';
import { RequestSubDocumentComponent } from './request/subdocument.component';
import { SidebarComponent } from './sidebar.component';
// Consolidado
import { ConsolidatedComponent } from './academic-file/consolidated/consolidated.component';

// orientation
import { AssistanceComponent } from '../shared/components/assistance/assistance.component';
import { ScheduleComponent } from '../shared/components/schedule/schedule.component';
import { ViewNotificationsComponent } from './notifications/notifications.component';
import { SocRecordComponent } from './orientation/buw/socrecord.component';
import { SocRecordGuideComponent } from './orientation/buw/socrecord.guide.component';
import { StudentComplianceComponent } from '../shared/components/student-compliance/student-compliance.component';
import { PaymentSimulatorComponent } from './orientation/paymentSimulator.component';
import { ReportCardComponent } from './orientation/report-card.component';
import { OrientationResolutionComponent } from './orientation/resolution.component';
import { TercioSuperiorComponent } from './orientation/tercio-superior/tercio-superior.component';

// Alertas - Notificaciones
import { RestrictionDebtByTermComponent } from './notifications/restriction-debt-by-term.component';

// solicitudes
import { BnAutoserviceNewRequestComponent } from './enrollment/banner-autoservice/new-request.component';
import { BnAutoserviceRequestStatusComponent } from './enrollment/banner-autoservice/request-status.component';
import { ReqDebtComponent } from './request/debts.component';
import { ReqConvalidationComponent } from './request/documents/convalidation.component';
import { ReqConvalidationExtensionComponent } from './request/documents/convalidationExtension.component';
import { ReqEnrollmentProofWithDatesComponent } from './request/documents/enrollmentProofWithDates.component';
import { ReqEntryProofComponent } from './request/documents/entry.proof.component';
import { ReqGraduationProofComponent } from './request/documents/graduationProof.component';
import { ReqOfficialTranscriptComponent } from './request/documents/official.transcript.component';
import { ReqProof3510PeriodComponent } from './request/documents/proof.3510.period.component';
import { ReqProof3510PromotionalComponent } from './request/documents/proof.3510.promotional.component';
import { ReqProofOfEnrollmentComponent } from './request/documents/proof.of.enrollment.component';
import { ReqReportCardComponent } from './request/documents/report.card.component';
import { ReqStudentCardDuplicateComponent } from './request/documents/student.card.duplicate.component';
import { ReqStudiesCertificateComponent } from './request/documents/studies.certificate.component';
import { ReqStudiesProofComponent } from './request/documents/studiesProof.component';
import { ReqStudiesProofAvgComponent } from './request/documents/studiesProofAvg.component';
import { ReqStudiesProofCustomComponent } from './request/documents/studiesProofCustom.component';
import { ReqStudiesProofGraduationComponent } from './request/documents/studiesProofGraduation.component';
import { ReqStudiesProofHistoryComponent } from './request/documents/studiesProofHistory.component';
import { ReqStudiesProofTermComponent } from './request/documents/studiesProofTerm.component';
import { ReqStudiesProofWithDatesComponent } from './request/documents/studiesProofWithDates.component';

// Reclamo de notas
import { OEAStudentNoteClaimComponent } from './request/note-claim/noteClaim.component';
import { OEARegisterNoteClaimComponent } from './request/note-claim/registerNoteClaim.component';
import { OEAViewNoteClaimComponent } from '../shared/components/note-claim/viewNoteClaim.component';

// Centro de Idiomas - CIC
import { CICDiplomasApplicationComponent } from './request/documents/languages-center/cic-diplomas-application.component';
// tslint:disable-next-line:max-line-length
import { CICProofOfEnrollmentApplicationComponent, } from './request/documents/languages-center/cic-proof-of-enrollment-application.component';
// tslint:disable-next-line:max-line-length
import { CICProofOfForeignLanguageApplicationComponent } from './request/documents/languages-center/cic-proof-of-foreign-language-application.component';
// tslint:disable-next-line:max-line-length
import { CICStudiesCertificateApplicationComponent } from './request/documents/languages-center/cic-studies-certificate-application.component';

// Documentos para egresados
import { RequestDuplicateProofGraduateComponent } from './request/documents/graduates/duplicate.proof.graduate.component';
// request of process
import { StudentUpdatePhoto } from './request/process/updatePhoto.component';

// Caja
import { CashBoxComponent } from './cash-box/cash-box.component';
import { EconomicStatusComponent } from './cash-box/economic-status.component';
import { PaymentMethodsComponent } from './cash-box/payment-methods.component';
import { BanksInfoComponent } from './cash-box/banks-info/banks-info.component';

// cic
import { CICMyScheduleComponent } from './cic/cic-my-schedule.component';
import { CICScheduleComponent } from './cic/cic-schedule.component';

// tracing
import { TracingComponent } from './tracing/tracing.component';
import { TracingListComponent } from './tracing/tracing.list.component';

// extras
import { Page401Component } from '../shared/components/page401.component';
import { Page404Component } from '../shared/components/page404.component';
import { ModalMessageComponent } from './enrollment/subject-available/modal-message-component/modal-message.component';
import { FAQComponent } from './faq/faq.component';
import { SubmoduleMenuComponent } from '../shared/components/submodule-menu/submodule-menu.component';
import { MessageStudentComponent } from '../shared/components/message-student/message-student.component';

// servicios
import { ConfigService } from '../config/config.service';
import { StudentService } from '../shared/services/academic/student.service';

// subject display
import { AcademicOfferComponent } from './enrollment/subject-available/academic-offer/academic-offer.component';
import { SubjectAvailableComponent } from './enrollment/subject-available/subject-available.component';
import { SchedulesAvailableComponent } from './enrollment/subject-available/schedules-available/schedules-available.component';

// pipes
import { CaseFunctionSpecial } from '../shared/pipes/case-function-special';
import { CapitalizeCasePipe } from '../shared/pipes/capitalize-case.pipe';
import { LowerCasePipe } from '../shared/pipes/lower-case.pipe';
import { TitleCasePipe } from '../shared/pipes/title-case.pipe';
import { UpperCasePipe } from '../shared/pipes/upper-case.pipe';
import { TimeTo12Pipe } from '../shared/pipes/time-to-12.pipe';
import { JsonParsePipe } from '../shared/pipes/json-parse.pipe';
import { LoopFilterPipe } from '../shared/pipes/loopfilter.pipe';

// trash for SUNEDU
import { PracticasComponent } from './orientation/practicas/practicas.component';
import { StudentProfileComponent } from './orientation/student-profile/student-profile.component';
import { BannerEnrollmentComponent } from './enrollment/banner-enrollment/banner-enrollment.component';
import { SubstituteExamComponent } from './request/substitute-exam/substituteExam.component';
import { UpdateProfilePhotoComponent } from './request/update-profile-photo/updateProfilePhoto.component';

// Contraseña Wifi
import { WifiComponent } from './wifi/wifi.component';

import { SurveysComponent } from '../shared/components/surveys/surveys.component';
import { TermsAndConditionsComponent } from '../shared/components/terms-and-conditions/terms-and-conditions.component';
// componente para matriculas
import { EnrollmentComponent } from './enrollment/enrollment.component';
import { Enrollment123Component } from './enrollment/c123/main.component';
import { EnrollmentStep1Component } from './enrollment/c123/step1.component';
import { EnrollmentStep2Component } from './enrollment/c123/step2.component';
import { EnrollmentStep3Component } from './enrollment/c123/step3.component';
import { EnrollmentWpPostComponent } from './enrollment/c123/wordpressPost.component';
import { SubEnrollmentComponent } from './enrollment/sub-enrollment.component';
// actividades extracurriculares
import { ListActivitiesProgramsComponent } from './enrollment/extracurricular-activities/list-activities-programs.component';
import { DetailsActivityProgramComponent } from './enrollment/extracurricular-activities/details-activity-program.component';
import { ModalPdfViewerComponent } from './enrollment/c123/pdfView.component';

// Registro de incidentes
import { IncidentHistoryComponent } from './request/incidents/incident-history/incident-history.component';
import { IncidentInfoComponent } from './request/incidents/incident-info/incident-info.component';
import { IncidentStudentComponent } from './request/incidents/incidents.component';
import { RegisterIncidentStudentComponent } from './request/incidents/register.incident.component';

// Datos personales
import { DataPersonComponent } from './orientation/data-person.component';
// Carnet Universitario
import { TCUUniversityCardComponent } from './orientation/universityCard.component';
// Proyectos Continental
import { PRYCategoriesComponent } from './enrollment/continental-project/categories.component';
import { PRYListProjectComponent } from './enrollment/continental-project/list-project.component';
import { PRYDetailProjectComponent } from './enrollment/continental-project/detail-project.component';

enableProdMode();
@NgModule({
  bootstrap: [
    FrontdeskComponent,
    SidebarComponent,
    CAUBreadcrumbComponent,
    ViewNotificationsComponent
  ],
  declarations: [
    FrontdeskComponent,
    MainComponent,
    MenuListComponent,
    RedirectComponent,
    CalloutComponent,
    BORequestAbstractComponent,
    PollComponent,
    InfoEnrollmentComponent,
    SvgIconComponent,
    SvgImgComponent,
    SocRecordComponent,
    SocRecordGuideComponent,
    BnAutoserviceNewRequestComponent,
    BnAutoserviceRequestStatusComponent,
    PracticasComponent,
    SubstituteExamComponent,
    UpdateProfilePhotoComponent,
    StudentProfileComponent,
    BannerEnrollmentComponent,
    ReqEntryProofComponent,
    ReqStudentCardDuplicateComponent,
    ReqEntryProofComponent,
    ReqStudentCardDuplicateComponent,
    ReqStudiesCertificateComponent,
    ReqReportCardComponent,
    ReqOfficialTranscriptComponent,
    ReqProof3510PeriodComponent,
    ReqProof3510PromotionalComponent,
    ReqProofOfEnrollmentComponent,
    ReqConvalidationComponent,
    ReqConvalidationExtensionComponent,
    RequestComponent,
    RequestAbstractComponent,
    RequestDocumentComponent,
    RequestSubDocumentComponent,
    ReqGraduationProofComponent,
    ReqStudiesProofComponent,
    ReqStudiesProofAvgComponent,
    ReqStudiesProofCustomComponent,
    ReqStudiesProofGraduationComponent,
    ReqStudiesProofHistoryComponent,
    ReqStudiesProofTermComponent,
    ReqEnrollmentProofWithDatesComponent,
    ReqStudiesProofWithDatesComponent,
    ReqDebtComponent,
    // Reclamo de notas
    OEAStudentNoteClaimComponent,
    OEARegisterNoteClaimComponent,
    OEAViewNoteClaimComponent,
    CICDiplomasApplicationComponent,
    CICProofOfEnrollmentApplicationComponent,
    CICProofOfForeignLanguageApplicationComponent,
    CICStudiesCertificateApplicationComponent,
    RequestDuplicateProofGraduateComponent,
    OrientationComponent,
    OrientationOptionComponent,
    OrientationCurriculumComponent,
    OrientationResolutionComponent,
    TercioSuperiorComponent,
    AssistanceComponent,
    ScheduleComponent,
    ReportCardComponent,
    PaymentSimulatorComponent,
    StudentComplianceComponent,
    ViewNotificationsComponent,
    RestrictionDebtByTermComponent,
    AdmisionComponent,
    SidebarComponent,
    TracingListComponent,
    TracingComponent,
    FAQComponent,
    RequestFlowComponent,
    LoadingComponent,
    CAUBreadcrumbComponent,
    // request process
    StudentUpdatePhoto,
    // caja
    CashBoxComponent,
    EconomicStatusComponent,
    PaymentMethodsComponent,
    BanksInfoComponent,
    // cic
    CICMyScheduleComponent,
    CICScheduleComponent,
    // others
    RedirectComponent,
    Page401Component,
    Page404Component,
    ModalMessageComponent,
    SubmoduleMenuComponent,
    MessageStudentComponent,
    // subject display
    SubjectAvailableComponent,
    AcademicOfferComponent,
    SchedulesAvailableComponent,
    // pipes
    JsonParsePipe,
    LoopFilterPipe,
    UpperCasePipe,
    LowerCasePipe,
    TitleCasePipe,
    CapitalizeCasePipe,
    TimeTo12Pipe,
    // actividades extracurriculares
    ListActivitiesProgramsComponent,
    DetailsActivityProgramComponent,
    SharedScheduleComponent,
    WifiComponent,
    SurveysComponent,
    TermsAndConditionsComponent,
    // matrícula
    EnrollmentComponent,
    Enrollment123Component,
    EnrollmentStep1Component,
    EnrollmentStep2Component,
    EnrollmentStep3Component,
    EnrollmentWpPostComponent,
    SubEnrollmentComponent,
    ModalPdfViewerComponent,
    // consolidado
    ConsolidatedComponent,
    // Incidentes
    IncidentHistoryComponent,
    IncidentInfoComponent,
    IncidentStudentComponent,
    RegisterIncidentStudentComponent,
    // Datos personales
    DataPersonComponent,
    // Carnet Universitario
    TCUUniversityCardComponent,
    // Proyectos continental
    PRYCategoriesComponent,
    PRYListProjectComponent,
    PRYDetailProjectComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    JsonpModule,
    FrontdeskRoutingModule,
    BrowserAnimationsModule,
    ChartModule,
    GrowlModule,
    RadioButtonModule,
    InputSwitchModule,
    Angulartics2Module.forRoot([Angulartics2GoogleAnalytics], { pageTracking: { clearQueryParams: true } })
  ],
  providers: [
    HasAccessFullGuard,
    HasAccessGuard,
    ConfigService,
    StudentService,
    CaseFunctionSpecial
  ],
})
export class FrontdeskModule { }
