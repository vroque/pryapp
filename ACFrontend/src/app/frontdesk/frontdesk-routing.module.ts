import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// guards
import { HasAccessFullGuard } from '../shared/guards/has-access-full.guard';
import { HasAccessGuard } from '../shared/guards/has-access.guard';

import { AssistanceComponent } from '../shared/components/assistance/assistance.component';
import { ScheduleComponent } from '../shared/components/schedule/schedule.component';
import { AdmisionComponent } from './admision/admision.component';
import { FAQComponent } from './faq/faq.component';
import { MainComponent } from './main.component';
import { StudentComplianceComponent } from '../shared/components/student-compliance/student-compliance.component';
import { OrientationCurriculumComponent } from './orientation/curriculum.component';
import { OrientationComponent } from './orientation/orientation.component';
import { PaymentSimulatorComponent } from './orientation/paymentSimulator.component';
import { ReportCardComponent } from './orientation/report-card.component';
import { OrientationResolutionComponent } from './orientation/resolution.component';
import { TercioSuperiorComponent } from './orientation/tercio-superior/tercio-superior.component';
import { RequestComponent } from './request/request.component';
import { TracingListComponent } from './tracing/tracing.list.component';

// documentos
import { ReqConvalidationComponent } from './request/documents/convalidation.component';
import { ReqConvalidationExtensionComponent } from './request/documents/convalidationExtension.component';
import { ReqOfficialTranscriptComponent } from './request/documents/official.transcript.component';
import { ReqReportCardComponent } from './request/documents/report.card.component';
import { ReqStudentCardDuplicateComponent } from './request/documents/student.card.duplicate.component';

import { ReqEnrollmentProofWithDatesComponent } from './request/documents/enrollmentProofWithDates.component';
import { ReqEntryProofComponent } from './request/documents/entry.proof.component';
import { ReqProofOfEnrollmentComponent } from './request/documents/proof.of.enrollment.component';

import { ReqStudiesProofComponent } from './request/documents/studiesProof.component';
import { ReqStudiesProofAvgComponent } from './request/documents/studiesProofAvg.component';
import { ReqStudiesProofCustomComponent } from './request/documents/studiesProofCustom.component';
import { ReqStudiesProofGraduationComponent } from './request/documents/studiesProofGraduation.component';
import { ReqStudiesProofHistoryComponent } from './request/documents/studiesProofHistory.component';
import { ReqStudiesProofTermComponent } from './request/documents/studiesProofTerm.component';
import { ReqStudiesProofWithDatesComponent } from './request/documents/studiesProofWithDates.component';
// Reclamo de notas
import { OEAStudentNoteClaimComponent } from './request/note-claim/noteClaim.component';
import { OEARegisterNoteClaimComponent } from './request/note-claim/registerNoteClaim.component';
// Rutas: Centro de Idiomas - CIC
import { CICDiplomasApplicationComponent, } from './request/documents/languages-center/cic-diplomas-application.component';
// tslint:disable-next-line:max-line-length
import { CICProofOfEnrollmentApplicationComponent, } from './request/documents/languages-center/cic-proof-of-enrollment-application.component';
// tslint:disable-next-line:max-line-length
import { CICProofOfForeignLanguageApplicationComponent, } from './request/documents/languages-center/cic-proof-of-foreign-language-application.component';
// tslint:disable-next-line:max-line-length
import { CICStudiesCertificateApplicationComponent, } from './request/documents/languages-center/cic-studies-certificate-application.component';

// request of process
import { StudentUpdatePhoto } from './request/process/updatePhoto.component';

// Caja
import { CashBoxComponent } from './cash-box/cash-box.component';
import { EconomicStatusComponent } from './cash-box/economic-status.component';

// cic
import { CICMyScheduleComponent } from './cic/cic-my-schedule.component';
import { CICScheduleComponent } from './cic/cic-schedule.component';

// Documentos para egresados
import { RequestDuplicateProofGraduateComponent } from './request/documents/graduates/duplicate.proof.graduate.component';

// Consolidado
import { ConsolidatedComponent } from './academic-file/consolidated/consolidated.component';

// extra components
import { Page401Component } from '../shared/components/page401.component';
import { Page404Component } from '../shared/components/page404.component';
import { RedirectComponent } from '../shared/components/redirect.component';

// certificado promocional
import { ReqProof3510PeriodComponent } from './request/documents/proof.3510.period.component';
import { ReqProof3510PromotionalComponent } from './request/documents/proof.3510.promotional.component';
import { ReqStudiesCertificateComponent } from './request/documents/studies.certificate.component';

// courses display
import { AcademicOfferComponent } from './enrollment/subject-available/academic-offer/academic-offer.component';
import { SubjectAvailableComponent } from './enrollment/subject-available/subject-available.component';
import { SchedulesAvailableComponent } from './enrollment/subject-available/schedules-available/schedules-available.component';

// Banner autoservice
import { BnAutoserviceNewRequestComponent } from './enrollment/banner-autoservice/new-request.component';
import { BnAutoserviceRequestStatusComponent } from './enrollment/banner-autoservice/request-status.component';

// trash for SUNEDU
import { PracticasComponent } from './orientation/practicas/practicas.component';
import { StudentProfileComponent } from './orientation/student-profile/student-profile.component';
import { SubstituteExamComponent } from './request/substitute-exam/substituteExam.component';

// Contraseña Wifi
import { WifiComponent } from './wifi/wifi.component';
import { TermsAndConditionsComponent } from '../shared/components/terms-and-conditions/terms-and-conditions.component';
import { SurveysComponent } from '../shared/components/surveys/surveys.component';

// componente para matriculas
import { EnrollmentComponent } from './enrollment/enrollment.component';
import { SubEnrollmentComponent } from './enrollment/sub-enrollment.component';
import { BannerEnrollmentComponent } from './enrollment/banner-enrollment/banner-enrollment.component';

// Componentes de extracurriculares
import { ListActivitiesProgramsComponent } from './enrollment/extracurricular-activities/list-activities-programs.component';
import { DetailsActivityProgramComponent } from './enrollment/extracurricular-activities/details-activity-program.component';

// Registro de incidentes
import { IncidentStudentComponent } from './request/incidents/incidents.component';
import { IncidentHistoryComponent } from './request/incidents/incident-history/incident-history.component';
import { IncidentInfoComponent } from './request/incidents/incident-info/incident-info.component';
import { RegisterIncidentStudentComponent } from './request/incidents/register.incident.component';

// Data de persona
import { DataPersonComponent } from './orientation/data-person.component';
// Carnet universitario
import { TCUUniversityCardComponent } from './orientation/universityCard.component';

// Proyectos continental
import { PRYCategoriesComponent } from './enrollment/continental-project/categories.component';

const routes: Routes = [
  { path: '', redirectTo: '/frontdesk', pathMatch: 'full' },
  { path: 'frontdesk', component: MainComponent },
  // rutas de vida academica
  { canActivate: [HasAccessGuard], path: 'frontdesk/vida-academica', component: OrientationComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/3-5-10-superior', component: Page404Component },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/boleta-notas', component: ReportCardComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/difusion-cumplimiento', component: StudentComplianceComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/ficha-socioconomica', component: RedirectComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/horario', component: ScheduleComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/inasistencia', component: AssistanceComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/plan-estudios', component: OrientationCurriculumComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/requisito-egresado', component: Page404Component },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/resoluciones', component: OrientationResolutionComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/simulador-pension', component: PaymentSimulatorComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/tercio-superior', component: TercioSuperiorComponent },
  // trash for SUNEDU
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/estado-economico', component: EconomicStatusComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/perfil-estudiante', component: StudentProfileComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/practicas', component: PracticasComponent },
  // Proyectos continental
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/proyectos-uc', component: PRYCategoriesComponent },
  // rutas de CIC
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/cic/horarios', component: CICScheduleComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/cic/mi-horario', component: CICMyScheduleComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/consolidado', component: ConsolidatedComponent },
  // Datos personales
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/datos-personales', component: DataPersonComponent },
  /*************
   * Carnet Universitario *
   *************/
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/carne-universitario', component: TCUUniversityCardComponent },
  // => Forma de enrutar los submodulos dentro de otro submodulo OjO siempre debe de ir al final del módulo
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/vida-academica/:area', component: OrientationComponent },

  // Rutas para los Trámites
  { canActivate: [HasAccessGuard], path: 'frontdesk/tramite', component: RequestComponent },
  // trash for SUNEDU
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/boleta-notas', component: ReqReportCardComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/convalidacion', component: ReqConvalidationComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/convalidacion/ampliacion', component: ReqConvalidationExtensionComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/convalidacion/convalidacion', component: ReqConvalidationComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/duplicado-carne', component: ReqStudentCardDuplicateComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/historial-academico', component: ReqOfficialTranscriptComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/programar-recuperacion', component: SubstituteExamComponent },
  // certificados
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/certificados/certificado-estudios', component: ReqStudiesCertificateComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/certificados/certificado-estudios-promocional', component: Page404Component },
  // constancias
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias/cons-ingreso', component: ReqEntryProofComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias/cons-matricula', component: ReqProofOfEnrollmentComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias/cons-estudios', component: ReqStudiesProofComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias/cons-3510', component: ReqProof3510PeriodComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias-esp/cons-matricula-fecha', component: ReqEnrollmentProofWithDatesComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias-esp/cons-estudios-fecha', component: ReqStudiesProofWithDatesComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias-esp/cons-estudios-historico', component: ReqStudiesProofHistoryComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias-esp/cons-estudios-periodo', component: ReqStudiesProofTermComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias-esp/cons-estudios-promedio', component: ReqStudiesProofAvgComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias-esp/cons-estudios-personalizado', component: ReqStudiesProofCustomComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias-esp/cons-estudios-culminado', component: ReqStudiesProofGraduationComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias-esp/cons-3510-promocional', component: ReqProof3510PromotionalComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/seguimiento', component: TracingListComponent },
  // rutas documentos para egresados
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/constancias/duplicado-constancia-egresado',
    component: RequestDuplicateProofGraduateComponent },
  // Reclamo de nota
  { canActivate: [HasAccessGuard], path: 'frontdesk/tramite/reclamo-notas', component: OEAStudentNoteClaimComponent },
  { canActivate: [HasAccessGuard], path: 'frontdesk/tramite/reclamo-notas/Registrar', component: OEARegisterNoteClaimComponent },
  // Centro de Idiomas - CIC
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], component: CICProofOfForeignLanguageApplicationComponent, path: 'frontdesk/tramite/centro-idiomas/constancia-idioma-extranjero' },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], component: CICProofOfEnrollmentApplicationComponent, path: 'frontdesk/tramite/centro-idiomas/constancia-matricula' },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], component: CICStudiesCertificateApplicationComponent, path: 'frontdesk/tramite/centro-idiomas/certificado-estudios' },
  { canActivate: [HasAccessFullGuard], component: CICDiplomasApplicationComponent, path: 'frontdesk/tramite/centro-idiomas/diploma' },

  // request of process
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/actualizar-foto', component: StudentUpdatePhoto },
  // => Forma de enrutar los submodulos dentro de otro submodulo OjO siempre debe de ir al final del módulo
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/tramite/:area', component: RequestComponent },

  { canActivate: [HasAccessGuard], path: 'frontdesk/admision', component: AdmisionComponent },
  { canActivate: [HasAccessGuard], path: 'frontdesk/admision/:option', component: AdmisionComponent },

  // caja
  { canActivate: [HasAccessGuard], path: 'frontdesk/caja', component: CashBoxComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/caja/estado-economico', component: EconomicStatusComponent },

  // FAQ
  { path: 'frontdesk/faq', component: FAQComponent },

  // Contraseña Wifi
  { path: 'frontdesk/wifi', component: WifiComponent },

  // Matrículas
  { canActivate: [HasAccessGuard], path: 'frontdesk/matricula', component: EnrollmentComponent },

  // VUC
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessGuard], path: 'frontdesk/matricula/actividades-extracurriculares', component: ListActivitiesProgramsComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessGuard], path: 'frontdesk/matricula/actividades-extracurriculares/:idActivity', component: DetailsActivityProgramComponent },

  // subjects availables
  { canActivate: [HasAccessGuard], path: 'frontdesk/matricula/asignaturas-disponibles', component: SubjectAvailableComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessGuard], path: 'frontdesk/matricula/asignaturas-disponibles/horarios/:data', component: SchedulesAvailableComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessGuard], path: 'frontdesk/matricula/asignaturas-disponibles/oferta-academica/:list', component: AcademicOfferComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/matricula/autoservicio-banner/nueva-solicitud', component: BnAutoserviceNewRequestComponent },
  // tslint:disable-next-line:max-line-length
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/matricula/autoservicio-banner/seguimiento', component: BnAutoserviceRequestStatusComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/matricula/inscripcion', component: BannerEnrollmentComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/matricula/estado-economico', component: EconomicStatusComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/matricula/simulador-pension', component: PaymentSimulatorComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/matricula/:area', component: SubEnrollmentComponent },


  /*************
   * INCIDENTS *
   *************/
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/incidentes', component: IncidentStudentComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/incidentes/registrar', component: RegisterIncidentStudentComponent },
  { canActivate: [HasAccessFullGuard], path: 'frontdesk/incidentes/historial', component: IncidentHistoryComponent },
  { canActivate: [HasAccessGuard], path: 'frontdesk/incidentes/historial/:incident', component: IncidentInfoComponent },

  // page 40X
  { path: '404', component: Page404Component },
  { path: 'frontdesk/404', component: Page404Component },
  { path: '401', component: Page401Component },
  { path: 'frontdesk/401', component: Page401Component },
  { path: 'terms', redirectTo: 'frontdesk/terms', pathMatch: 'full' },
  { path: 'surveys', redirectTo: 'frontdesk/surveys', pathMatch: 'full' },
  { path: 'frontdesk/terms', component: TermsAndConditionsComponent },
  { path: 'frontdesk/surveys', component: SurveysComponent },

  // default page
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class FrontdeskRoutingModule { }
