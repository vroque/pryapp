import { Injectable } from '@angular/core';
import { Menu } from './menu';

@Injectable()
export class MenuService {
  data: Menu[];

  query(): Menu[] {
    return (window as any).choices;
  }
}
