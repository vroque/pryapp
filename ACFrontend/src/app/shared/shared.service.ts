import { Observable } from "rxjs/Observable";

export class MySharedService {
  data: any;
  dataChange: Observable<any>;

  constructor() {
  }

  setData(data: any) {
    this.data = data;
  }

  getData(): any {
    return this.data;
  }
}
