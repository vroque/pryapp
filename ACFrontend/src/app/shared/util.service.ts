import { Injectable } from '@angular/core';

@Injectable()
export class UtilService {
  constructor() {
  }

  /**
   * Slice text
   * @param {string} text
   * @param {number} len
   * @returns {string}
   */
  getSlice(text: string, len: number): string {
    if (text !== null) {
      return text.length <= len ? text : text.slice(0, len).concat('...');
    } else {
      return '';
    }
  }

  /**
   * Remove duplicates items from string (from text input form) and convert to array
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
   * http://stackoverflow.com/a/9229821
   * @param inputString
   */
  stringToArrayWithoutDuplicates(inputString: string): string[] {
    return inputString.split(' ')
      .filter(// Filter blank space
        (id) => id !== '')
      .filter(// Filter duplicates
        (id, position, self) => self.indexOf(id) === position);
  }
}
