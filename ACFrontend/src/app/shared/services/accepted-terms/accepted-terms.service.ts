import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';
import { AcceptedTerms } from './accepted-terms';

@Injectable()
export class AcceptedTermsService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Extra/AcceptedTerms`;
  }

  /**
   * Save new accepted terms
   * @param acceptedTerms Accepted terms to save
   */
  save(acceptedTerms: AcceptedTerms): Observable<AcceptedTerms> {
    return this.http.post<AcceptedTerms>(this.apiUrl, acceptedTerms);
  }
}
