export class AcceptedTerms {
  id: number;
  pidm: number;
  termsId: number;
  accepted: boolean;
  acceptedDate: Date;
}
