import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';

@Injectable()
export class LockCardsService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Shared/LockCards`;
  }

  /**
   * Check if a card is locked
   * @param cardAbbr
   */
  isBlocked(cardAbbr): Observable<boolean> {
    const api = `${this.apiUrl}/${cardAbbr}`;
    return this.http.get<boolean>(api);
  }
}
