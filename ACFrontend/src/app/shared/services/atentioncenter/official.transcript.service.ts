import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { OfficialTranscript } from './official.transcript';
import { Request } from './request';
import { Graduated } from '../academic/graduated';

@Injectable()
export class OfficialTranscriptService {
  private url: string;  // a URL to web API

  constructor(private httpClient: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}atentioncenter/officialtranscript`;
  }

  get(): Observable<OfficialTranscript> {
    return this.httpClient.get<OfficialTranscript>(this.url);
  }

  /**
   * Generar solicitud
   * @param description Descripcion
   */
  create(description: String): Observable<Request> {
    const data: any = { description: description };
    return this.httpClient.post<Request>(this.url, data);
  }

  /**
   * Generar documento sin deuda
   * @param student Estudiante
   */
  createBatch(student: Graduated): Observable<Graduated> {
    return this.httpClient.put<Graduated>(this.url, student);
  }
}
