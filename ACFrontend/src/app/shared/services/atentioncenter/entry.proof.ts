import { Cost } from '../accounting/cost';
export class EntryProof {
  first_request: boolean;
  id: number;
  name: string;
  is_valid: boolean;
  cost: Cost;
  stack_error: string[];
}
