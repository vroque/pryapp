import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class RequestDescriptionService {
  private url: string;  // URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}convalidation/requestAbstract`;
  }

  update (id: number, data: any): Observable<any> {
    const opts = this.config.urlFormat(data);
    return this.http
    .post(`${this.url}/${id}`, data, this.config.getHeaders('json'))
    .map(this.config.jsonExtractData)
    .catch(this.config.handleError);
  }
}
