import { Cost } from '../../accounting/cost';

export class DuplicateProofGraduate {
  id: number;
  name: string;
  // tslint:disable-next-line:variable-name
  is_valid: boolean;
  cost: Cost;
  // tslint:disable-next-line:variable-name
  stack_error: string[];
}
