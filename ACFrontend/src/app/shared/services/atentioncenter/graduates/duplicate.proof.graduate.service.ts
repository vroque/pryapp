import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { DuplicateProofGraduate } from './duplicate.proof.graduate';
import { Request } from './../request';
import { ConfigService } from '../../../../config/config.service';


@Injectable()
export class DuplicateProofGraduateService {
  private url: string;

  constructor(private httpClient: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}atentioncenter/DuplicateProofGraduate`;
  }

  /**
   * Obtener datos de documento
   */
  get(): Observable<DuplicateProofGraduate> {
    return this.httpClient.get<DuplicateProofGraduate>(this.url);
  }

  /**
   * Crear Solicitus
   * @param description Descripcion
   */
  create (description: string): Observable<Request> {
    const data = { description: description };
    return this.httpClient.post<Request>(this.url, data);
  }
}
