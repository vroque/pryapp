import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Proof3510Promotional } from './proof.3510.promotional';
import { Request } from './request';
import { Graduated } from '../academic/graduated';

@Injectable()
export class Proof3510PromotionalService {
  private url: string;  // URL to web API

  constructor(private httpClient: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}atentioncenter/proofof3510promotional`;
  }

  get(): Observable<Proof3510Promotional> {
    return this.httpClient.get<Proof3510Promotional>(this.url);
  }

  /**
   * Generar Solicitud
   * @param description Descripcion
   */
  create(description: String): Observable<Request> {
    const data = { description: description };
    return this.httpClient.post<Request>(this.url, data);
  }

  /**
   * Generar documento sin deuda
   * @param student Estudiante
   */
  createBatch(student: Graduated): Observable<Graduated> {
    return this.httpClient.put<Graduated>(this.url, student);
  }
}
