import { Cost } from '../accounting/cost';
export class OfficialTranscript {
  id: number;
  name: string;
  is_valid: boolean;
  cost: Cost;
  period_list: string[];
  stack_error: string[];
}
