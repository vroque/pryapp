import { Cost } from '../accounting/cost';
export class StudentCardDuplicate {
  id: number;
  name: string;
  is_valid: boolean;
  cost: Cost;
  stack_error: string[];
  has_photo: boolean;
}
