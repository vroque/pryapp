export class RequestLog {
  id: number;
  request_id: number;
  old_state: number;
  new_state: number;
  observation: string;
  user_id: string;
  office_user_id: number;
  office_id: number;
  office_name: string;
  approved: string;
  update_date: Date;
}
