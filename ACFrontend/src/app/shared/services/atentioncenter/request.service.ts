import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {ConfigService} from '../../../config/config.service';
import {Http} from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Request} from './request';

@Injectable()
export class RequestService {
  private url: string;

  constructor(private http: Http, private config: ConfigService) {
    this.url = `${config.getApiURI()}atentioncenter/request`;
  }

  query(): Observable<Request[]> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  get(id: string): Observable<Request> {
    return this.http
      .get(`${this.url}/${id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  save(id: string, data: any): Observable<Request> {
    return this.http
      .post(`${this.url}/${id}`, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  update(id: string, data: any): Observable<Request> {
    return this.http
      .put(`${this.url}/${id}`, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * Busca Solicitud
   * @param data
   * @returns {Observable<Request[]>}
   */
  search(data: any): Observable<Request[]> {
    const body: string = this.config.urlFormat(data);
    const searchUrl = `${this.config.getApiURI()}atentioncenter/SearchRequest?${body}`;
    return this.http
      .get(searchUrl, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  updateAbstract(id: number, data: any[]): Observable<Request> {
    const url = `${this.config.getApiURI()}atentioncenter/requestAbstract/${id}`;
    return this.http
      .put(url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  byArea(area: number): Observable<Request[]> {
    const url = `${this.config.getApiURI()}atentioncenter/requestByArea/${area}`;
    return this.http
      .get(url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  resolutions(): Observable<Request[]> {
    const url = `${this.config.getApiURI()}atentioncenter/resolutionRequest`;
    return this.http
      .get(url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  cancelRequest(data: Request): Observable<Request> {
    const url = `${this.config.getApiURI()}atentioncenter/requestByArea/`;
    return this.http
      .put(url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
