import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Request } from './request';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class RequestByStateService {
  private url: string;

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}atentioncenter/requestByState`;
  }

  query (area: number): Observable<Request[]> {
    return this.http
      .get(`${this.url}/${area}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
