import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Proof3510Period } from './proof.3510.period';
import { Request } from './request';
import { Graduated } from '../academic/graduated';

@Injectable()
export class Proof3510PeriodService {
  private url: string;  // URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}atentioncenter/proofof3510perperiod`;
  }

  get (): Observable<Proof3510Period> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  create (description: String, period_list: string[]): Observable<Request> {
    const data = { description: description, period_list: period_list };
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  createBatch (term: string, student: Graduated): Observable<Graduated> {
    const dataBatch = { term: term, student : student};
    return this.http
      .put(this.url, dataBatch, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
