import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Attachment } from './attachment';

@Injectable()
export class AttachmentService {
  private url: string;  // the URL to web API
  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}atentioncenter/requestAttachment`;
  }

  query (request_id: number): Observable<Attachment[]> {
    return this.http
      .get(`${this.url}/${request_id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
