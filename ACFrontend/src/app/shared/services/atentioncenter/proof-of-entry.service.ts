import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ConfigService} from '../../../config/config.service';
import {Graduated} from '../academic/graduated';

@Injectable()
export class ProofOfEntryService {

  private apiBaseUri: string;
  private apiUri: string;

  constructor(private http: Http,
              private config: ConfigService) {
    this.apiBaseUri = config.getApiURI();
    this.apiUri = `${this.apiBaseUri}academic`;
  }

  /**
   * Constancias de Ingreso
   * @param {string} type Tipo de request
   * @param students Estudiantes a procesar
   * @returns {Observable<Graduated>}
   */
  getProofOfEntry(type: string, students: any): Observable<Graduated[]> {
    const api = `${this.apiBaseUri}AtentionCenter/ProofOfEntry`;
    const data = {Type: type, Students: students};
    return this.http.post(api, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
