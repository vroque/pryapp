import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { StudentCardDuplicate } from './student.card.duplicate';
import { Request } from './request';

@Injectable()
export class StudentCardDuplicateService {
  private url: string;

  constructor(
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}atentioncenter/StudentCardDuplicate`;
  }

  get(): Observable<StudentCardDuplicate> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  create(description: String): Observable<Request> {
    const data = { description: description };
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
