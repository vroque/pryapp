import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { StudiesCertificate } from './studies.certificate';
import { Request } from './request';

@Injectable()
export class StudiesCertificateService {
  private url: string;

  constructor(
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}atentioncenter/studiesCertificate`;
  }

  get(): Observable<StudiesCertificate> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  create(description: String, period_list: Array<string>): Observable<Request> {
    const data = { description: description, period_list: period_list };
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
