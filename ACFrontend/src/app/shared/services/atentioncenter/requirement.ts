export class ACRequirement {
  constructor(
    public id: number,
    public document_id: number,
    public category_id: number,
    public description: string,
    public order: number,
    public min_files: number,
    public max_files: number,
    public files: Array<ACRequirementFile>,
  ) { }
}

export class ACRequirementFile {
  constructor(
    public id: number,
    public requirement_id: number,
    public request_id: number,
    public description: string,
    public url: string,
  ) { }
}
