import { Cost } from '../accounting/cost';
import { ThirdFifthTenthPeriod } from '../academic/third.fifth.tenth.period';
export class Proof3510Period {
  id: number;
  name: string;
  is_valid: boolean;
  has_3510: boolean;
  cost: Cost;
  term_list: ThirdFifthTenthPeriod[];
  stack_error: string[];
}
