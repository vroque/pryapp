import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ACTerm } from './term';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class ACTermService {
  private url: string;

  constructor(
    private http: Http,
    private httpClient: HttpClient,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}atentioncenter/term`;
  }

  query(): Observable<ACTerm[]> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  create(data: ACTerm): Observable<ACTerm> {
    return this.http
      .post(`${this.url}`, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  update(data: ACTerm): Observable<ACTerm> {
    return this.http
      .put(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * Obtiene el periodo actual con de acuerdo a la fecha y su perfil
   * @param {Term} term Term to delete
   * @returns {Observable<boolean>}
   */
  getTermCurrentEnrollment(campusId: string, departament: string): Observable<ACTerm> {
    const actionName = this.url + '/term-current-enrollment/' + campusId + '/' + departament;
    return this.http
      .get(actionName, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
