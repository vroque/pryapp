import { Cost } from '../accounting/cost';
export class ProofOfEnrollment {
  id: number;
  name: string;
  is_valid: boolean;
  cost: Cost;
  period_list: string[];
  stack_error: string[];
  is_first_week: boolean[];
}
