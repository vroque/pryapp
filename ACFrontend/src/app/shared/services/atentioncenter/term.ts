export class ACTerm {
  term: string;
  edit: boolean;
  part_term: string;
  campus_id: string;
  department: string;
  start_date: Date;
  end_date: Date;
  start_classes_date: Date;
  end_classes_date: Date;
  start_enroll_date: Date;
  end_enroll_date: Date;
  start_substitute_exam_date: Date;
  end_substitute_exam_date: Date;
}
