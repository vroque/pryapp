import { Cost } from '../accounting/cost';
import { Transfer } from './transfer';
export class Convalidation {
  id: number;
  name: string;
  is_valid: boolean;
  transfers: Transfer[];
  cost: Cost;
  stack_error: string[];
}
