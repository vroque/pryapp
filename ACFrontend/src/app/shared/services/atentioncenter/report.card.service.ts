import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { ScholarshipStudent } from '../academic/scholarship-student';
import { ReportCard } from './report.card';
import { Request } from './request';

@Injectable()
export class ReportCardService {
  private url: string;

  constructor(private http: Http,
              private config: ConfigService) {
    this.url = `${config.getApiURI()}atentioncenter/reportcard`;
  }

  get(): Observable<ReportCard> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  create(description: string, termList: string[]): Observable<Request> {
    const data: any = {description: description, term_list: termList};
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * Boleta de Notas PRONABEC
   * @param pronabec
   * @param {string} pronabecType
   * @param {string} schoolId
   * @param {string} academicPeriod
   * @returns {Observable<ScholarshipStudent>}
   */
  getPronabecReportCard(pronabec: any, pronabecType: string, schoolId: string,
                        academicPeriod: string): Observable<ScholarshipStudent[]> {
    const type = 'pronabec';
    const api = `${this.config.getApiURI()}AtentionCenter/PronabecReportCard`;
    const data: any = {
      Type: type,
      ScholarshipStudents: pronabec,
      PronabecType: pronabecType,
      SchoolId: schoolId,
      AcademicPeriod: academicPeriod,
    };
    return this.http.post(api, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
