export class Proof3510 {
  id: number;
  icon: string;
  uri: string;
  name: string;
  smenus: Proof3510[];
}
