import { Cost } from '../accounting/cost';
export class StudiesProof {
  id: number;
  name: string;
  is_valid: boolean;
  type: number;
  cost: Cost;
  term_list: string[];
  stack_error: string[];
  promocional_cost: Cost;
  promocional_id: number;
}
