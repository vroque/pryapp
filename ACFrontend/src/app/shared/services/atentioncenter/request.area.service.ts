import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { AcademicProfile } from '../academic/academic.profile';
import { Request } from './request';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class RequestByAreaService {
  private url: string;  // URL to web API

  constructor(
    private http: Http,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}atentioncenter/requestByArea`;
  }

  query(area: number): Observable<Request[]> {
    return this.http
      .get(`${this.url}/${area}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getByProfile(profile: AcademicProfile): Observable<Request[]> {
    return this.http.post(this.url, profile, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
