export class Request {
  amount: number;
  approved: boolean;
  campus: string;
  concept_id: string;
  cost: number;
  creation_date: Date;
  delivery_date: Date;
  description: string;
  divs: string;
  document_id: number;
  document_number: number;
  document_uri: string[];
  id: number;
  office_id: string;
  office_user_id: number;
  online_payment_id: string;
  period_id: string;
  person_id: number;
  program: string;
  relationship_id: number;
  state: number;
  student_id: string;
  student_name: string;
  term: string;
  update_date: Date;
}
