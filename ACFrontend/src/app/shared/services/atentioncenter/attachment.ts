export class Attachment {
  request_id: number;
  requeriment_id: number;
  requeriment_name: number;
  delivered: boolean;
  documents: AttachmentDocument[];
}

class AttachmentDocument {
  number: number;
  name: string;
  url: string;
}
