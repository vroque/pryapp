import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ConfigService} from '../../../config/config.service';
import {Graduated} from '../academic/graduated';
import {Request} from './request';

@Injectable()
export class PromotionalStudyCertificateService {

  private apiBaseUri: string;
  private apiAttentionCenterUri: string;

  constructor(private http: Http, private config: ConfigService) {
    this.apiBaseUri = this.config.getApiURI();
    this.apiAttentionCenterUri = `${this.apiBaseUri}AtentionCenter`;
  }

  /**
   * Creación de solicitud para Constancia de Estudio Promocional
   * @returns {Observable<Graduated>}
   */
  createRequest(students: Graduated[]): Observable<Graduated[]> {
    const apiUri = `${this.apiAttentionCenterUri}/PromotionalStudyCertificate`;
    const type = 'createRequest';
    const data: any = {Type: type, Students: students};
    return this.http.post(apiUri, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * Lista de Solicitudes de "Certificado de Estudios Promocional"
   * @returns {Observable<Request[]>}
   */
  getRequestList(): Observable<Request[]> {
    const apiUri = `${this.apiAttentionCenterUri}/PromotionalStudyCertificate/request`;
    return this.http.get(apiUri, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
