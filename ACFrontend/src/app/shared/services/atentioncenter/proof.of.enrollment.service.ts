import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Graduated } from '../academic/graduated';
import { ScholarshipStudent } from '../academic/scholarship-student';
import { ProofOfEnrollment } from './proof.of.enrollment';
import { Request } from './request';

@Injectable()
export class ProofOfEnrollmentService {
  private urlApi: string;
  private urlWithDates: string;

  constructor(private http: Http,
              private config: ConfigService) {
    this.urlApi = `${config.getApiURI()}AtentionCenter/ProofOfEnrollment`;
    this.urlWithDates = `${config.getApiURI()}atentioncenter/proofOfEnrollmentWithDate`;
  }

  get(): Observable<ProofOfEnrollment> {
    return this.http
      .get(this.urlApi, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  create(type: string, description: string, periodList: string[]): Observable<Request> {
    const data: any = {Type: type, Description: description, PeriodList: periodList};
    return this.http
      .post(this.urlApi, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getWihDates(): Observable<ProofOfEnrollment> {
    return this.http
      .get(this.urlWithDates, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  createWihDates(description: string, periodList: string[]): Observable<Request> {
    const data: any = {description: description, period_list: periodList};
    return this.http
      .post(this.urlWithDates, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * Constancia de Matrícula Promocional
   * @param {string} type Tipo de "request" para ser procesado por el controller
   * @param {string} academicPeriod Período académico para generar la constancia de matrícula
   * @param {Graduated} graduates Arreglo de egresados UC
   * @returns {Observable<Graduated>}
   */
  promotional(type: string, academicPeriod: string, graduates: Graduated[]): Observable<Graduated[]> {
    const data: any = {Type: type, AcademicPeriod: academicPeriod, Graduates: graduates};
    return this.http.post(this.urlApi, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * Constancia de Matrícula PRONABEC
   * @param scholarshipStudent Estudiantes PRONABEC
   * @param {string} pronabecType Tipo PRONABEC
   * @param {string} schoolId Escuela Académico Profesional
   * @param {string} academicPeriod Período académico
   * @returns {Observable<ScholarshipStudent>}
   */
  getPronabecProofOfEnrollment(scholarshipStudent: any, pronabecType: string, schoolId: string,
                               academicPeriod: string): Observable<ScholarshipStudent[]> {
    const type = 'pronabec';
    const data: any = {
      Type: type,
      ScholarshipStudents: scholarshipStudent,
      ScholarshipType: pronabecType,
      SchoolId: schoolId,
      AcademicPeriod: academicPeriod,
    };
    return this.http.post(this.urlApi, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
