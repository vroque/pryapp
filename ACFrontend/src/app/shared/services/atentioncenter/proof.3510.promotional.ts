import { Cost } from '../accounting/cost';
import { ThirdFifthTenthPromotional } from '../academic/third.fifth.tenth.promotional';
export class Proof3510Promotional {
  id: number;
  name: string;
  is_valid: boolean;
  has_3510: boolean;
  cost: Cost;
  promo: ThirdFifthTenthPromotional;
  stack_error: string[];
}
