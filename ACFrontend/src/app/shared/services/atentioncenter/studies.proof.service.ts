import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { StudiesProof } from './studies.proof';
import { Request } from './request';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class StudiesProofService {
  private url: string;

  constructor(
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}atentioncenter/studiesproof`;
  }

  /**
    * 0:"general",
    * 1:"history",
    * 2:"by_term",
    * 3:"avarage_score",
    * 4:"career",
    * 5:"custom"
   */
  get(type: number): Observable<StudiesProof> {
    return this.http
      .get(`${this.url}/${type}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  create(description: string, type: number, termList: Array<string>): Observable<Request> {
    const data: any = { description: description, type: type, term_list: termList };
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
