import { Cost } from '../accounting/cost';
export class ReportCard {
  id: number;
  name: string;
  is_valid: boolean;
  cost: Cost;
  term_list: string[];
  stack_error: string[];
}
