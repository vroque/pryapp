export class DocumentState {
  document_id: number;
  state_id: number;
  order: number;
  next: number;
}
