import { Cost } from '../accounting/cost';
export class StudiesCertificate {
  id: number;
  name: string;
  is_valid: boolean;
  cost: Cost;
  period_list: string[];
  stack_error: string[];
  promocional_cost: Cost;
  promocional_id: number;
}
