import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { ACRequirement } from './requirement';
import { ACRequirementFile } from './requirement';

@Injectable()
export class ACRequirementService {
  private url: string;  // the URL to web API
  private urlFile: string;  // the URL to web API
  constructor (private http: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}atentioncenter/requirement`;
    this.urlFile = `${config.getApiURI()}atentioncenter/requirementfile`;
  }

  byDocument (documentId: number): Observable<ACRequirement[]> {
    return this.http.get<ACRequirement[]>(`${this.url}/${documentId}`);
  }

  byRequest (requestId: number, documentId: number): Observable<ACRequirement[]> {
    return this.http.get<ACRequirement[]>(`${this.url}/${documentId}?request_id=${requestId}`);
  }

  addFile (requestId: number, file: ACRequirementFile ): Observable<ACRequirementFile> {
    return this.http.post<ACRequirementFile>(`${this.urlFile}/${requestId}`, file);
  }

  updateFile (requestId: number, file: ACRequirementFile ): Observable<ACRequirementFile> {
    return this.http.put<ACRequirementFile>(`${this.urlFile}/${requestId}`, file);
  }

  deleteFile (fileId: number): Observable<ACRequirementFile> {
    return this.http.delete<ACRequirementFile>(`${this.urlFile}/${fileId}`);
  }

}
