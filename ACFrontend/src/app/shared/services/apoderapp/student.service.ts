import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ConfigService} from '../../../config/config.service';
import {Student} from './student';
import {StudentDetail} from './studentDetail';

@Injectable()
export class StudentService {
  private url: string;

  constructor(private http: Http, private config: ConfigService) {
    this.url = `${config.getApiURI()}apoderapp/Student`;
  }

  /**
   * Búsqueda por número de documento (DNI).
   * Muestra datos básicos (Código de estudiante, PIDM, Nombres) para ser usados en Apoderapp
   * @param {string} type
   * @param {Student[]} studentCodes
   * @returns {Observable<Student>}
   */
  searchByDocumentNumber(type: string, studentCodes: Student[]): Observable<Student> {
    const data: any = {Type: type, StudentCodes: studentCodes};
    return this.http.post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * Detalle de Estudiante/Apoderado
   * @param {string} studentId
   * @returns {Observable<StudentDetail>}
   */
  getStudentDetail(studentId: string): Observable<StudentDetail> {
    const apiUri = `${this.url}/${studentId}`;
    return this.http.get(apiUri).map(this.config.jsonExtractData).catch(this.config.handleError);
  }

}
