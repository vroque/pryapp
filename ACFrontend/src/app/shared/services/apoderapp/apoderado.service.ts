import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ConfigService} from '../../../config/config.service';
import {Persona} from '../persona/persona';
import {Apoderado} from './apoderado';
import {Student} from './student';
import {StudentDetail} from './studentDetail';

@Injectable()
export class ApoderadoService {

  private uri: string;

  constructor(private http: Http, private config: ConfigService) {
    this.uri = `${config.getApiURI()}apoderapp/Apoderado`;
  }

  /**
   * Búsqueda de persona por número de documento para obtener info si son apoderados del estudiante o no
   * @param {string[]} documentNumber
   * @param {Student} student
   * @returns {Observable<any | any>}
   */
  searchStudentApoderadoByDocumentNumber(documentNumber: string[], student: Student) {
    const type = 'searchStudentApoderado';
    const data: any = {Type: type, DocumentNumber: documentNumber, Student: student};
    return this.http.post(this.uri, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * Asigna nuevo apoderado a un estudiante
   * @param {string} type
   * @param {Student} student
   * @param {Persona} apoderado
   * @param {number} parentesco
   * @param {string} comment
   * @returns {Observable<Student>}
   */
  addApoderado(student: Student, persona: Persona, parentesco: number, comment: string): Observable<Student> {
    const type = 'new';
    const data: any = {Type: type, Student: student, Person: persona, KinshipId: parentesco, Comment: comment};
    return this.http.post(this.uri, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * Cambia el acceso de un apoderado
   * @param {Student} student
   * @param {Apoderado} apoderado
   * @param {boolean} hasAccess
   * @param {string} comment
   * @returns {Observable<Student>}
   */
  updateApoderadoAccess(student: Student, apoderado: Apoderado, hasAccess: boolean, comment: string): Observable<StudentDetail> {
    const type = 'update';
    apoderado.has_access = hasAccess;
    apoderado.comment = comment;
    const data: any = {Type: type, Student: student, Apoderado: apoderado};
    return this.http.post(this.uri, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
