import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';
import { ExtracurricularEnrollment } from './extracurricular-enrollment';
import { AcceptedTerms } from '../accepted-terms/accepted-terms';

@Injectable()
export class ExtracurricularEnrollmentService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Shared/ExtracurricularEnrollment`;
  }

  /**
   * Verificar si un estudiante tiene términos de inscripción extracurricular pendientes.
   */
  checkTermAndCondition(): Observable<ExtracurricularEnrollment> {
    return this.http.get<ExtracurricularEnrollment>(this.apiUrl);
  }

  /**
 * verifica que el estudiante haya aceptado un determinado terminos y condiciones
 * @param pidm pidm del estudiante 
 */
  get(pidm: number): Observable<AcceptedTerms> {
    return this.http.get<AcceptedTerms>(`${this.apiUrl}?pidm=${pidm}`);
  }
}
