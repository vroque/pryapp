import { TermsAndConditions } from '../terms-and-conditions/terms-and-conditions';

export class ExtracurricularEnrollment {
  hasPendingTerms: boolean;
  termsAndConditions: TermsAndConditions;
}
