import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';
import { BannerEnrollment } from './banner-enrollment';

@Injectable()
export class BannerEnrollmentService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Shared/BannerEnrollment`;
  }

  /**
   * Verify if a student has terms for enrollment pending
   */
  hasPendingTerms(): Observable<BannerEnrollment> {
    return this.http.get<BannerEnrollment>(this.apiUrl);
  }
}
