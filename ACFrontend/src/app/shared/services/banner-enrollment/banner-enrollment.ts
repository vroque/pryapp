import { TermsAndConditions } from '../terms-and-conditions/terms-and-conditions';

export class BannerEnrollment {
  hasPendingTerms: boolean;
  termsAndConditions: TermsAndConditions;
  hasPendingRequirements: boolean;
  requirements: any[];
}
