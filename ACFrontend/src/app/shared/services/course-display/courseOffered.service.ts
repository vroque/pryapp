import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { CourseOffered } from './courseOffered';

@Injectable()
export class CourseOfferedService {
  datachange: Observable<any>;
  private url: string;

  constructor(
    private http: Http,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}academic/coursesoffered`;
  }

  get(): Observable<CourseOffered[]> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
