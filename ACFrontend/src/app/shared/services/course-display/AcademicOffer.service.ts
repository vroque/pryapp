import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { AcademicOffer } from './AcademicOffer';

@Injectable()
export class AcademicOfferService {
  datachange: Observable<any>;
  private url: string;

  constructor(
    private http: Http,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}academic/academicoffercourse`;
  }

  get(term: string, subject: string, course: string): Observable<AcademicOffer[]> {
    return this.http
      .get(`${this.url}?term=${term}&subject=${subject}&course=${course}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getCoursesChildren(nrcpadre: string, term: string, subject: string, course: string, codliga: string): Observable<AcademicOffer[]> {
    const uri = `${this.config.getApiURI()}academic/academicoffercoursechildren`;
    const formattedData: string = this.config.urlFormat({ nrcpadre, term, subject, course, codliga });
    return this.http
      .get(
      `${uri}?${formattedData}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
