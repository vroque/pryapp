export class OffertAcademic {
  campus: string;
  credits: string;
  course: string;
  cycle: string;
  departament: string;
  nameCourse: string;
  pidm: number;
  program: string;
  subject: string;
  term: string;
  recommended: string;
  elective: string;
}
