import {OffertAcademic} from './offertAcademic';
export class CourseOffered {
  startFech: Date;
  endFech: Date;
  active: boolean;
  term: string;
  offertAcademic: OffertAcademic[];
  coursesByCycles: { cycle: string; courses: OffertAcademic[] }[];
}
