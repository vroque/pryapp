export class AcademicOffer {
  term: string;
  campus: string;
  departament: string;
  partterm: string;
  program: string;
  nrc: string;
  subject: string;
  course: string;
  credits: number;
  nroligas: number;
  nrcpadre: string;
  codliga: string;
  maxvacancies: number;
  vacanciesavailable: number;
  idseccion: number;
  namecourse: string;
  pidmteacher: number;
  idteacher: string;
  teacher: string;
}
