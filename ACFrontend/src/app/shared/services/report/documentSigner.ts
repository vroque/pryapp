export class DocumentSigner {
  document_id: number;
  quantity: number;
  year: number;
  month: number;
  sign_as: string;
}
