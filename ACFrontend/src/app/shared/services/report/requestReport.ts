export class RequestReport {
  document_id: number;
  quantity: number;
  year: number;
  month: number;
}
