import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Requeriment } from './requeriment';

@Injectable()
export class RequerimentService {
  private url: string;  // URL to web API
  data: Array<Requeriment>;
  dataChange: Observable<any>; // admision/requeriment

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${ config.getApiURI()}admision/requeriment`;
  }

  query (profile: any, all: boolean): Observable<Requeriment[]> {
    const body = this.config.urlFormat(profile);
    return this.http
      .get(`${this.url}?${body}&all=${all}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
