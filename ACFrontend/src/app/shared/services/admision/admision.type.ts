export class AdmisionType {
  id: number;
  name: string;
  is_pronabec: boolean;
}
