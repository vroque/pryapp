import { PollQuestion } from './poll-question';
export class Poll {
  active: boolean;
  beginDate: Date;
  description: string;
  endDate: Date;
  id: number;
  isRequest: boolean;
  name: string;
  publicationDate: Date;
  questions: PollQuestion[];
  requestState: number;
}
