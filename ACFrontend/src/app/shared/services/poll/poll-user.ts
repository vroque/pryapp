export class PollUser {
  completed: boolean;
  id: number;
  omitted: boolean;
  order: number;
  pidm: number;
  pollDate: Date;
  pollId: number;
  requestId: number;
}
