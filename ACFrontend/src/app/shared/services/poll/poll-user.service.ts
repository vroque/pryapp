import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { PollUser } from './poll-user';
import { PollChoiceDetail } from './poll-choice-detail';

@Injectable()
export class PollUserService {
  private url: string;  // URL to web API

  constructor(private http: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}Poll/pollUser`;
  }

  /**
   *  Validar si a esta solicitud se le debe tomar encuesta
   * @param id Numero de solicitud
   */
  validateMustPollRequest(id: number): Observable<boolean> {
    return this.http.get<boolean>(`${this.url}?requestId=${id}`);
  }

  savePollUser(pollUser: PollUser, pollChoiceDetail: PollChoiceDetail[]): Observable<PollUser> {
    const data: any = { data: pollUser, dataPollChoiceDetail: pollChoiceDetail };
    return this.http.post<PollUser>(this.url, data);
  }
}
