export class PollChoice {
  choiceText: string;
  id: number;
  isComment: boolean;
  orderChoice: number;
  pollQuestionId: number;
  votes: number;
  selected: boolean;
}
