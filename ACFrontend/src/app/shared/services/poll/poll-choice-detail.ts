export class PollChoiceDetail {
    id: number;
    pollChoiceId: number;
    pidm: number;
    requestId: number;
    comment: string;
    voteDate: Date;
}
