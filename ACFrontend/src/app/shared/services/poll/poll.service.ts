import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Poll } from './poll';

@Injectable()
export class PollService {
  private url: string;  // URL to web API

  constructor(private http: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}Poll/poll`;
  }

  /**
   * Obtiene la encuesta de un estado de solicitud
   * @param state Estado de solicitud
   */
  getPoll(state: number): Observable<Poll> {
    return this.http.get<Poll>(`${this.url}?requestState=${state}`);
  }

}
