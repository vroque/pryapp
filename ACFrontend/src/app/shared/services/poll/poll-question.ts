import { PollChoice } from './poll-choice';
export class PollQuestion {
  choices: PollChoice[];
  id: number;
  isComment: boolean;
  pollId: number;
  questionOrder: number;
  questionTitle: string;
  comment: string;
}
