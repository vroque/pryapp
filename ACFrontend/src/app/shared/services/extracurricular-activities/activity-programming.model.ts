import { Activity } from './activity.model';
import { ProgrammingActivityCurriculum } from './programming-activity-curriculum.model';
import { Department } from '../institutional/department';
import { Campus } from '../institutional/campus';
export interface ActivityProgramming {
    id: number;
    idActivity: number;
    startDateEnrollment: Date;
    endDateEnrollment: Date;
    startDateActivity: Date;
    endDateActivity: Date;
    cost: number;
    currency: string;
    studentProfile: string;
    startPeriod: string;
    endPeriod: string;
    idScheduleType: string;
    scheduleDescription: string;
    sessionDay: string;
    startHour: string;
    endHour: string;
    sessionPlace: string;
    quantityVacancy: number;
    maxQuantityVacancy: number;
    documentTeacher: string;
    nameTeacher: string;
    idCampus: string;
    idModality: string;
    replicated: boolean;
    nrc: string;

    hasTracing: boolean;
    hasSelected: boolean;
    programmingActCurriculumId: string[];
    activity: Activity;
    department: Department;
    campus: Campus;
    programmingActivityCurriculum: ProgrammingActivityCurriculum[];
    isEnrolled: boolean; // Estado para determinar si un estudiante esta inscrito en esta programación
    isSocialProjection: boolean; // Determina si el horario programacdo acredita proyección social
    isExtracurricular: boolean; // Determina si el horario programado acredita extracurricular
}
