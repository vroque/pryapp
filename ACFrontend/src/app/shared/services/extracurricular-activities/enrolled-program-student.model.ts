import { ActivityProgramming } from './activity-programming.model';
import { Campus } from '../academic/campus';

export interface EnrolledProgramStudent {
    activityType: string;
    name: string;
    startDateActivity: Date;
    endDateActivity: Date;
    idCampus: string;
    documentNumber: string;
    fullName: string;
    eap: string;
    cycle: number;
    curriculum: string;
    pidmStudent: number;
    term: string;
    confirmed: boolean;
    dateConfirmed: Date;
    programmingActivity: ActivityProgramming;
    startPeriod: string;
    idAlumno: string;
    nrc: string;
    campus: Campus;
    phone: string;
}
