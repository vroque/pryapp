import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { ScheduleType } from './schedule-type.model';

@Injectable()
export class ScheduleTypeService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/ScheduleTypes`;
  }

  /**
  * Obtiene los tipos de horarios (Descriptivo y no descriptivo)
  */
  getScheduleTypes(): Observable<ScheduleType[]> {
    return this.http.get<ScheduleType[]>(this.apiUri);
  }
}
