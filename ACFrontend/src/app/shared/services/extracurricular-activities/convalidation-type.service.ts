import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { ConvalidationType } from './convalidation-type.model';

@Injectable()
export class ConvalidationTypeService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/ConvalidateTypes`;
  }

  /**
  * Lista los tipos de convalidaciones (Extra curricular, Proyección Social)
  * @returns {Observable<ConvalidationType>}
  */
  get(): Observable<ConvalidationType[]> {
    return this.http.get<ConvalidationType[]>(`${this.apiUri}`);
  }
}
