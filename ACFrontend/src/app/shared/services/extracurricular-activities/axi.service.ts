import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { Axi } from './axi.model';

@Injectable()
export class AxiService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/Axis`;
  }

  /**
  * Lista Ejes
  * @returns {Observable<Axi[]>} ejes
  */
  getAxis(): Observable<Axi[]> {
    return this.http.get<Axi[]>(`${this.apiUri}`);
  }

  /**
  * Obtiene un eje por su id
  * @param {number} idAxi Id del eje
  */
  getByAxiId(idAxi: number): Observable<Axi> {
    const api = `${this.apiUri}/${idAxi}`;
    return this.http.get<Axi>(api);
  }

  /**
  * Obtiene un eje por su nombre
  * @param {string} axiName Nombre del eje
  */
  getByAxiName(axiName: string): Observable<Axi> {
    const api = `${this.apiUri}/GetByName/${axiName}`;
    return this.http.get<Axi>(api);
  }

  /**
  * Crea un eje
  * @param {Axi} model Modelo de eje
  */
  createAxi(model: Axi): Observable<Axi> {
    const api = this.apiUri;
    return this.http.post<Axi>(api, model);
  }

  /**
  * Actualiza un eje
  * @param {Axi} model Modelo del eje
  */
  updateAxi(model: Axi): Observable<boolean> {
    const api = this.apiUri;
    return this.http.put<boolean>(api, model);
  }

  /**
  * Elimina un eje (cambio de estado active a 0)
  * @param {number} id Id del eje
  */
  deleteAxi(id: number): Observable<boolean> {
    const api = this.apiUri;
    return this.http.delete<boolean>(`${api}/${id}`);
  }
}
