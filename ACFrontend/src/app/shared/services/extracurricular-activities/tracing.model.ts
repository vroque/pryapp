export interface Tracing {
    id: number;
    idProgrammingActivity: number;
    pidmStudent: number;
    hours: number;
    score: number;
    observation: string;
    active: boolean;
    createdAt: Date;
    updatedAt: Date;
}
