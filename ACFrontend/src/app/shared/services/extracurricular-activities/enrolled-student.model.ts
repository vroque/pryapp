export interface EnrolledStudent {
    pidmStudent: number;
    documentNumber: string;
    cycle: number;
    fullName: string;
}
