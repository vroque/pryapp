import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

@Injectable()
export class TermService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/Periods`;
  }

  /**
  * Obtiene los periodos académicos vigentes a la fecha
  */
  getAll(): Observable<any[]> {
    const api = this.apiUri;
    return this.http.get<any[]>(`${api}`);
  }
}
