import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { ActivityProgramming } from './activity-programming.model';

@Injectable()
export class ActivityProgrammingService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/ProgrammingActivity`;
  }

  /**
  * Se obtiene las programaciones de actividades
  */
  getProgrammedActivities(): Observable<ActivityProgramming[]> {
    return this.http.get<ActivityProgramming[]>(`${this.apiUri}/GetAll`);
  }

  /**
  * Retorna una lista del cruce de las tablas Programming Activity
  */
  getProgrammingActivitiesByTerm(term: string): Observable<ActivityProgramming[]> {
    return this.http.get<ActivityProgramming[]>(`${this.apiUri}/GetProgrammingWorkshopActivity?term=${term}`);
  }

  /**
  * Gets by programmed activities id
  * @param {number} idp Id de la programación de la actividad
  * @returns {Observable<ActivityProgramming>}
  */
  getByProgrammedActivitiesId(idp: number): Observable<ActivityProgramming> {
    const api = `${this.apiUri}?id=${idp}`;
    return this.http.get<ActivityProgramming>(api);
  }

  /**
  * Obtener docente por Pidm
  * @param {number} id
  */
  getByPidmTeacher(id: number): Observable<ActivityProgramming[]> {
    return this.http.get<ActivityProgramming[]>(`${this.apiUri}?id=${id}`);
  }

  /**
  * Gets programming acivities by
  * @param {number} idActivityType Id de tipo de actividad
  * @param {string} idModality Id de modalidad
  * @param {string} idCampus Id de campus
  * @param {string} shortName Nombre corto de la actividad
  */
  getProgrammingAcivitiesBy(idActivityType: number, idModality: string, idCampus: string, shortName: string)
    : Observable<ActivityProgramming[]> {
    return this.http.get<ActivityProgramming[]>
      (`${this.apiUri}?idActivityType=${idActivityType}&idModality=${idModality}&idCampus=${idCampus}
      &shortname=${shortName}`);
  }

  /**
  * Gets programming acivities by
  * @param {number} idActivityType Id de tipo de actividad
  * @param {string} term periodo
  * @param {string} idModality Id de modalidad
  * @param {string} idCampus Id de campus
  */
  getProgrammingAcivitiesFilterBy(idActivityType: number, idModality: string, idCampus: string, term: string)
    : Observable<ActivityProgramming[]> {
    return this.http.get<ActivityProgramming[]>
      (`${this.apiUri}?idActivityType=${idActivityType}&idModality=${idModality}&idCampus=${idCampus}&term=${term}`);
  }

  /**
  * Crear una programación de actividad
  * @param {ActivityProgramming} model
  */
  create(model: ActivityProgramming): Observable<ActivityProgramming> {
    const api = `${this.apiUri}/CreateActivityProgramming`;
    return this.http.post<ActivityProgramming>(api, model);
  }

  /**
  * Actualizar una programación de actividad
  * @param {ActivityProgramming} model
  */
  update(model: ActivityProgramming): Observable<boolean> {
    const api = this.apiUri;
    return this.http.put<boolean>(api, model);
  }

  /**
  * Eliminar una programación de actividad
  * @param {number} id Id de una programación de actividad
  */
  delete(id: number): Observable<boolean> {
    const api = this.apiUri;
    return this.http.delete<boolean>(`${api}/${id}`);
  }

  /**
  * Obtiene todas las programaciones en un determinado periodo academico
  * @param {string} term periodo academico
  */
  getProgrammingActivityByTerm(term: string): Observable<ActivityProgramming[]> {
    const api = `${this.apiUri}?term=${term}`;
    return this.http.get<ActivityProgramming[]>(api);
  }

  /**
  * Cambia el estado de replica de actividades de tipo taller
  */
  replicateProgrammingActivity(): Observable<string> {
    const api = `${this.apiUri}/ReplicateActivitySchedules`;
    return this.http.post<string>(api, null);
  }
}
