import { ActivityProgramming } from './activity-programming.model';
import { ActivityType } from './activity-type.model';
import { Axi } from './axi.model';
import { ConvalidationType } from './convalidation-type.model';

export interface Activity {
    id: number;
    shortName: string;
    name: string;
    description: string;
    other: string;
    credits: number;
    imagePath: string;
    imageEncode: string;
    idActivityType: number;
    idAxi: number;
    isReplicated: boolean;
    bannerCode: string;
    visible: boolean;
    active: boolean;
    createdAt: Date;
    updatedAt: Date;

    // Campos de ayuda (no son campos de BD)
    convalidationTypesId: number[];
    convalidationTypes: ConvalidationType[];
    activityType: ActivityType;
    axi: Axi;
    programmingActivities: ActivityProgramming[];
}
