export interface TracingReport {
    activityName: string;
    dni: string;
    pidm: number;
    fullName: string;
    eap: string;
    program: string;
    state: string;
    hours: number;
    score: number;
    observation: string;
    createdAt: Date;
    hasCertificate: boolean;
    certificatePath: string;
}
