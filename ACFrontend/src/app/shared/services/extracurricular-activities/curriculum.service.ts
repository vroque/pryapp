import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { Curriculum } from './curriculum.model';

@Injectable()
export class CurriculumService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/Curriculum`;
  }

  /**
  * Se obtienen todos los planes de estudio
  */
  getCurriculums(): Observable<Curriculum[]> {
    return this.http.get<Curriculum[]>(`${this.apiUri}`);
  }
}
