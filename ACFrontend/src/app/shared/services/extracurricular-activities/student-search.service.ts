import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { StudentSearch } from './student-search.model';

@Injectable()
export class StudentSearchService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${config.getApiURI()}ExtracurricularActivities/SearchStudents`;
  }

  /**
  * Se obtiene al estudiante mediante el dni
  * @param {string[]} studentCodes codigos de estudiante
  */
  getStudentsByDni(studentCodes: string[]): Observable<StudentSearch[]> {
    return this.http.post<StudentSearch[]>(this.apiUri, studentCodes);
  }
}
