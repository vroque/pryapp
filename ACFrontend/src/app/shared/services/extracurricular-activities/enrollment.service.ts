import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { EnrolledProgramActivity } from './enrolled-program-activity.model';
import { MessageServer } from '../academic/messageServer';

@Injectable()
export class EnrollmentService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/Enrollments`;
  }

  /**
  * Obtiene el número de inscripciones que tiene el estudiante en el periodo actual
  */
  getEnrollments(): Observable<EnrolledProgramActivity[]> {
    return this.http.get<EnrolledProgramActivity[]>(this.apiUri);
  }

  /**
  * Matricula a un estudiante en un extracurricular, el estudiante se obtiene en el controlador
  * de la API. Solo se envia el Id de la programación
  * @param {number} idProgrammingActivity Id de la programación de la actividad
  */
  enrollStudent(idProgrammingActivity: number): Observable<MessageServer> {
    return this.http.put<MessageServer>(this.apiUri, idProgrammingActivity);
  }
}
