import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { Recognition } from './recognition.model';

@Injectable()
export class RecognitionService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/Recognitions`;
  }

  /**
  * Registra un reconocimiento
  * @param {Recognition} model
  * @returns {Observable<Recognition>}
  */
  create(model: Recognition): Observable<Recognition> {
    const api = this.apiUri;
    return this.http.post<Recognition>(api, model);
  }
}
