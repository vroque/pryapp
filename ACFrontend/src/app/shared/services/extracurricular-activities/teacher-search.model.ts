export interface TeacherSearch {
    pidm: number;
    documentNumber: string;
    fullName: string;
}
