import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { Activity } from './activity.model';

@Injectable()
export class ActivityService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${config.getApiURI()}ExtracurricularActivities/Activities`;
  }

  /**
  * Lista Actividades
  * @returns {Observable<Activity[]>}
  */
  getActivities(): Observable<Activity[]> {
    return this.http.get<Activity[]>(`${this.apiUri}`);
  }

  /**
  * Lista una actividad por su id
  * @param {number} idActivity Id de la actividad
  */
  getByActivityId(idActivity: number): Observable<Activity> {
    const api = `${this.apiUri}?id=${idActivity}`;
    return this.http.get<Activity>(api);
  }

  /**
  * Lista una actividad por su nombre
  * @param {string} activityName Nombre de la actividad
  */
  getByActivityName(activityName: string): Observable<Activity> {
    const api = `${this.apiUri}/GetByName/${activityName}`;
    return this.http.get<Activity>(api);
  }

  /**
  * Lista una actividad por su nombre corto
  * @param {string} activityShortName Nombre corto de la actividad
  */
  getByActivityShortName(activityShortName: string): Observable<Activity> {
    const api = `${this.apiUri}/GetByShortName/${activityShortName}`;
    return this.http.get<Activity>(api);
  }

  /**
  * Lista todas las actividades que pertenecen a un tipo de actividad (Taller/Programa)
  * @param {number} activityType Id de tipo de actividad
  */
  getByActivityType(activityType: number): Observable<Activity[]> {
    const api = `${this.apiUri}/GetByActivityType/${activityType}`;
    return this.http.get<Activity[]>(api);
  }

  /**
  * Crear una nueva Actividad - VUC
  */
  createActivity(model: Activity): Observable<Activity> {
    const api = `${this.apiUri}/CreateActivity`;
    return this.http.post<Activity>(api, model);
  }

  /**
  * Actualiza una actividad
  * @param {Activity} model modelo de actividad
  * @returns {Observable<boolean>}
  */
  updateActivity(model: Activity): Observable<boolean> {
    const api = this.apiUri;
    return this.http.put<boolean>(api, model);
  }

  /**
  * Desactiva una actividad por su id. Se envia null como segundo parámetro de post para que llegue a la acción.
  * @param {number} id Id de la actividad
  */
  desactivateActivity(id: number): Observable<string> {
    const api = `${this.apiUri}/Desactivate/${id}`;
    return this.http.post<string>(api, null);
  }

  /**
  * Elimina una actividad por su id
  * @param {number} id Id de la actividad
  */
  deleteActivity(id: number): Observable<Activity> {
    const api = this.apiUri;
    return this.http.delete<Activity>(`${api}/${id}`);
  }

  /**
  * Cambia el estado de replica de actividades de tipo taller
  * @returns {Observable<string>} activity
  */
  replicateActivity(): Observable<string> {
    const api = `${this.apiUri}/ProcessIsReplicated`;
    return this.http.post<string>(api, null);
  }

  /**
  * Obtiene una actividad con sus respectivas programaciones
  * @param idActivity Id de la actividad
  */
  getProgrammmingActivities(idActivity: number): Observable<Activity> {
    const api = this.apiUri;
    return this.http.get<Activity>(`${api}?idActivity=${idActivity}`);
  }
}
