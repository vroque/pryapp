import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { ActivityType } from './activity-type.model';

@Injectable()
export class ActivityTypeService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/ActivityTypes`;
  }

  /**
  * Obtener los tipos de actividades
  * @returns {Observable<ActivityType[]>} tipos de actividades
  */
  getActivityTypes(): Observable<ActivityType[]> {
    return this.http.get<ActivityType[]>(`${this.apiUri}`);
  }
}
