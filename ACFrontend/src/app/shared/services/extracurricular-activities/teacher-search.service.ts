import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { TeacherSearch } from './teacher-search.model';

@Injectable()
export class TeacherSearchService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${config.getApiURI()}ExtracurricularActivities/SearchTeacher`;
  }

  /**
  * Se obtiene a un docente mediante el dni.
  * @param {string} data Dni del docente
  */
  getTeacherByDni(data: string): Observable<TeacherSearch> {
    const api = `${this.apiUri}?documentNumber=${data}`;
    return this.http.get<TeacherSearch>(api);
  }
}
