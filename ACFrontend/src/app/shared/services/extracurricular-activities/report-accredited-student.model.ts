export interface ReportAccreditedStudent {
    pidmStudent: number;
    courseName: string;
    finalScore: string;
    percentageAssistance: string;
    fullNameStudent: string;
    documentNumber: string;
}
