import { Activity } from './activity.model';
export interface Axi {
  id: number;
  name: string;
  description: string;
  imagePath: string;
  imageEncode: string;
  activities: Activity[];
}
