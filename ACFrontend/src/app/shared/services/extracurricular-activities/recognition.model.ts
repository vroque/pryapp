export interface Recognition {
    id: number;
    recognitionDate: Date;
    reason: string;
    description: string;
    pathEvidency: string;
    pidmStudents: number[];
    idConvalidationType: number;
    pathCode: string;
    credit: string;
}
