import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { EnrolledProgramActivity } from './enrolled-program-activity.model';
import { EnrolledStudent } from './enrolled-student.model';
import { EnrolledProgramStudent } from './enrolled-program-student.model';
import { SelectedStudent } from './selected-student.model';

@Injectable()
export class EnrolledStudentService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/EnrolledStudents`;
  }

  /**
  * Actualiza el estado de los pre inscritos en un programa
  * @param {SelectedStudent} model
  * @returns {Observable<boolean>}
  */
  updateEnrolledProgram(model: SelectedStudent): Observable<SelectedStudent> {
    return this.http.post<SelectedStudent>(this.apiUri, model);
  }

  /**
  * Matricula a un estudiante en un extracurricular, el estudiante se obtiene en el controlador
  * de la API. Solo se envia el Id de la programación
  * @param {number} idProgrammingActivity Id de la programación de la actividad
  */
  enrollStudent(idProgrammingActivity: number): Observable<boolean> {
    return this.http.put<boolean>(this.apiUri, idProgrammingActivity);
  }

  /**
  * Obtiene los inscritos de un programa
  * @param {number} idActType Id del tipo de actividad
  * @param {number} idProgramActivity Id de la programación de la actividad
  */
  getByEnrolledProgram(idActType: number, idProgramActivity: number): Observable<EnrolledProgramStudent[]> {
    const api = `${this.apiUri}/GetRelationStudent/${idActType}/${idProgramActivity}`;
    return this.http.get<EnrolledProgramStudent[]>(api);
  }

  /**
  * Obtiene los estudiantes inscritos en un programa
  * @param {number} idProgrammingActivity Id de la programación de la actividad
  */
  getEnrolledStudents(idProgrammingActivity: number): Observable<EnrolledProgramActivity[]> {
    const api = this.apiUri;
    return this.http.get<EnrolledProgramActivity[]>(`${api}/${idProgrammingActivity}`);
  }

  /**
  * Obtiene a los estudiantes esncritos en actividad taller
  * @param {number} idProgrammingActivity id de programacion
  * @param {string} term periodo academico
  */
  getStudents(idProgrammingActivity: number, term: string): Observable<EnrolledProgramActivity[]> {
    const api = this.apiUri;
    return this.http.get<EnrolledProgramActivity[]>(`${api}?idProgrammingActivity=${idProgrammingActivity}&term=${term}`);
  }

  /**
   * Obtiene a los estudiantes matriculados en la actividad "Taller"
   */
  getEnrolledStudentsActivity(term: string): Observable<EnrolledProgramStudent[]> {
    const api = this.apiUri;
    return this.http.get<EnrolledProgramStudent[]>(`${api}?term=${term}`);
  }
}
