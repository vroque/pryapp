import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { Activity } from './activity.model';

@Injectable()
export class ReportActivityService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/Activities/GetWorkshopActivities`;
  }

  /**
  * Lista Actividades tipo taller
  * @returns {Observable<Activity>}
  */
  getActivitiesReplicated(): Observable<Activity[]> {
    return this.http.get<Activity[]>(`${this.apiUri}`);
  }
}
