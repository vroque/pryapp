import { ActivityProgramming } from './activity-programming.model';
import { ReportAccreditedStudent } from './report-accredited-student.model';
import { Department } from '../institutional/department';
export interface EnrolledProgramActivity {
    id: number;
    idProgramActivity: number;
    cycle: number;
    malla: string;
    campus: string;
    modality: string;
    dateEnrolled: Date;
    eAP: string;
    confirmed: boolean;
    active: boolean;
    createdAt: Date;
    updatedAt: Date;
    pidmStudent: number;
    dateConfirmed: Date;
    hasCertificate: boolean;
    certificatePath: string;
    idConcept: string;
    student: ReportAccreditedStudent;
    programmingActivity: ActivityProgramming;
    departmend: Department;
    fullName: string;
}
