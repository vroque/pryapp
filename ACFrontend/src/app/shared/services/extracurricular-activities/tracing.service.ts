import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { Tracing } from './tracing.model';
import { TracingReport } from './tracing-report.model';

@Injectable()
export class TracingService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${this.config.getApiURI()}ExtracurricularActivities/Tracing`;
  }

  /**
  * Registra el cumplimiento del programa
  */
  save(tracings: Tracing[]): Observable<Tracing[]> {
    const api = this.apiUri;
    return this.http.post<Tracing[]>(`${api}`, tracings);
  }

  /**
  * Retorna el PDF del programa aprobado
  * @param {number} pidm Pidm del estudiante
  * @param {number} idProgrammingActivity Id de la programación de la actividad
  */
  getActivityCertificate(pidm: number, idProgrammingActivity: number): Observable<string> {
    return this.http.get<string>(`${this.apiUri}/GetCertificate/${pidm}/${idProgrammingActivity}`);
  }

  /**
  * Obtiene la información necesaria para generar el reporte de cumplimiento
  * @param {number} idProgrammingActivity Id de la programación de la actividad
  */
  getReporte(idProgrammmingActivity: number): Observable<TracingReport[]> {
    const api = this.apiUri;
    return this.http.get<TracingReport[]>(`${api}/${idProgrammmingActivity}`);
  }
}
