import { Recognition } from './recognition.model';

export interface StudentSearch {
    idAlumno: number;
    dni: string;
    nomCompleto: string;
    idPerson: number;
    recognitions: Recognition[];
}
