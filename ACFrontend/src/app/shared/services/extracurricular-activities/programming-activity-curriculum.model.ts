export interface ProgrammingActivityCurriculum {
  id: number;
  idProgrammingActivity: number;
  idCurriculum: string;
  active: boolean;
  createdAt: Date;
  updatedAt: Date;
}
