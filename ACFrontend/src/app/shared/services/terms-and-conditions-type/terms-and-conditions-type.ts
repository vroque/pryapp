export class TermsAndConditionsType {
  id: number;
  name: string;
  description: string;
}
