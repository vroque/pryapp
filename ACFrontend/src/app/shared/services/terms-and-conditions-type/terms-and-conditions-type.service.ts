import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';
import { TermsAndConditionsType } from './terms-and-conditions-type';

@Injectable()
export class TermsAndConditionsTypeService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Settings/TermsAndConditionsType`;
  }

  /**
   * Terms and conditions type list
   */
  getTypeList(): Observable<TermsAndConditionsType[]> {
    return this.http.get<TermsAndConditionsType[]>(this.apiUrl);
  }
}
