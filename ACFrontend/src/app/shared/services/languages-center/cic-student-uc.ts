import { CICStudent } from './cic-student';
import { AcademicProfile } from '../academic/academic.profile';

export class CICStudentUC extends CICStudent {
  modalityId: string;
  modalityName: string;
  paymentStatus: string;
  graduateProfile: AcademicProfile;
  graduateProfiles: AcademicProfile[];
}
