import { Persona } from '../persona/persona';
import { CICAcademicProfile } from './cic-academic-profile';

export class CICStudent {
  persona: Persona;
  studentId: string;
  profile: CICAcademicProfile;
  profiles: CICAcademicProfile[];
  isSelected: boolean;
}
