import { Injectable } from '@angular/core';
import {  Http  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../../config/config.service';
import {CICRpta} from "./cic-rpta"
import {CICParamSp} from "./cic-param-sp"


@Injectable()
export class CICEnrollmentService {
    private apiBaseUri: string;
    private apiUri: string;
    private cicParamSp: CICParamSp;

    constructor(
      private http: Http,
      private config: ConfigService,
    ) {
      this.apiBaseUri = config.getApiURI();
      this.apiUri = `${this.apiBaseUri}LanguagesCenter`;
      this.cicParamSp = new CICParamSp();
    }
    
    postEnrollment(pidm: number, section: string): Observable<CICRpta>
    {
        this.cicParamSp.pidm = pidm;
        this.cicParamSp.section = section;

        const api = `${this.apiUri}/Enrollment`;
        var rpta = this.http
            .post(api,this.cicParamSp, this.config.getHeaders('json'))
            .map(this.config.jsonExtractData)
            .catch(this.config.handleError);
        return rpta;
    }
  

}