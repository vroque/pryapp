import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";

import {ConfigService} from "../../../config/config.service";

import {CICSettings} from "./cic-settings";

@Injectable()
export class CICSettingsService {
  private readonly apiBaseUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiBaseUrl = `${config.getApiURI()}LanguagesCenter/Settings`;
  }

  /**
   * Información sobre Nivel de Idioma que se imprimirá en la Constancia de Idioma Extranjero
   * @returns {Observable<CICSettings>}
   */
  getCaieLanguageLevelInfo(): Observable<CICSettings> {
    const apiUrl: string = `${this.apiBaseUrl}/caie`;
    return this.http.get<CICSettings>(apiUrl);
  }

  /**
   * Actualiza el Nivel de Idioma que se imprimirá en la Constancia de Idioma Extranjero
   * @param {CICSettings} settings
   * @returns {Observable<CICSettings>}
   */
  saveCaieLanguageLevelInfo(settings: CICSettings): Observable<CICSettings> {
    const apiUrl: string = `${this.apiBaseUrl}`;
    const data: object = {Type: "caie", Settings: settings};
    console.log("data => ", data);
    return this.http.put<CICSettings>(apiUrl, data);
  }
}
