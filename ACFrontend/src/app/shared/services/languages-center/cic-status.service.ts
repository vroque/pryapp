import { Injectable } from '@angular/core';
import { Headers, Http, Response  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { CICStatus } from './cic-status';

/**
 * Componente de horarios para alumno
 */
@Injectable()
export class CICStatusService {
  private apiBaseUri: string;
  private apiUri: string;

  constructor(
    private http: Http,
    private config: ConfigService,
  ) {
    this.apiBaseUri = config.getApiURI();
    this.apiUri = `${this.apiBaseUri}LanguagesCenter/Status`;
  }

  /**
   * Estado del alumno logeado
   * @return {Observable<CICStatus>} [estadio de CIC]
   */
  get(): Observable<CICStatus> {

    return this.http
      .get(this.apiUri, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * Estado del alumno por personId
   * @return {Observable<CICStatus>} [estadio de CIC]
   */
  getByPersonId(personId: number): Observable<CICStatus> {

    const api = `${this.apiUri}/{personId}`;
    return this.http
      .get(api, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
