import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { CICProofOfForeignLanguage } from './cic-proof-of-foreign-language';
import { ConfigService } from '../../../config/config.service';
import { Request } from '../atentioncenter/request';
import { CICCaie } from './cic-caie';

@Injectable()
export class CICProofOfForeignLanguageService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${config.getApiURI()}LanguagesCenter/ProofOfForeignLanguage`;
  }

  /**
   * Datos básicos para preparar la solicitud de Constancia de Idioma Extranjero de Uso Externo
   */
  get(): Observable<CICProofOfForeignLanguage> {
    return this.http.get<CICProofOfForeignLanguage>(this.apiUrl);
  }

  /**
   * Busca datos de estudiantes para procesar CAIE Batch
   * @param type Tipo de petición
   * @param students Códigos de estudiantes a buscar
   * @param estudiantes Objeto de estudiantes
   */
  getCaieBatch(type: string, students: any[], estudiantes: any): Observable<CICCaie> {
    const data = {Type: type, StudentsId: students, StudentsObject: estudiantes};
    return this.http.post<CICCaie>(this.apiUrl, data);
  }

  /**
   * CAIE Promocional
   * @param graduationPeriod Período de egreso de la UC
   * @param languageId Código de Idioma CIC
   */
  getCAIEPromotional(graduationPeriod: string, languageId: string): Observable<CICCaie> {
    const api = `${this.apiUrl}/${graduationPeriod}/${languageId}`;
    return this.http.get<CICCaie>(api);
  }

  /**
   * Generate Internal CAIE on demand
   * @param customInfo Custom data for CAIE
   * @param extraInfo Extra info for document
   */
  getCaieOnDemandInternal(customInfo: any, extraInfo: CICCaie[]): Observable<any> {
    const data = {Type: 'caieOnDemandInternal', Custom: customInfo, StudentsObject: extraInfo};
    return this.http.post(this.apiUrl, data);
  }

  /**
   * Generate External CAIE on demand
   * @param customInfo Custom data for CAIE
   * @param extraInfo Extra info for document
   */
  getCaieOnDemandExternal(customInfo: any, extraInfo: CICCaie[]): Observable<any> {
    const data = {Type: 'caieOnDemandExternal', Custom: customInfo, StudentsObject: extraInfo};
    return this.http.post(this.apiUrl, data);
  }

  /**
   * Crear nueva solicitud de Constancia de Idioma Extranjero Externo (desde frontdesk)
   * @param type
   * @param languageId
   * @param cycle
   * @param description
   */
  createRequestForCaieExt(type: string, languageId: string, cycle: string,
                          description: string): Observable<Request> {
    const data: object = {Description: description, Cycle: cycle, LanguageId: languageId, Type: type};
    return this.http.post<Request>(this.apiUrl, data);
  }
}
