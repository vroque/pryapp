import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';

@Injectable()
export class CICSecondLanguageService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}LanguagesCenter/SecondLanguage`;
  }

  /**
   * Verifica si un estudiante UC tiene, o no, cursado los ciclos de idiomas
   * requeridos para poder solicitar la constancia de idioma extranjero
   * @param studentPidm PIDM del estudiante
   */
  hasSecondLanguage(studentPidm: number): Observable<boolean> {
    const api = `${this.apiUrl}/${studentPidm}`;
    return this.http.get<boolean>(api);
  }

  /**
   * Verifica si un estudiante UC, de acuerdo a una determinada carrera,
   * tiene, o no, cursado los ciclos de idiomas
   * requeridos para poder solicitar la constancia de idioma extranjero
   * @param studentPidm PIDM del estudiante
   * @param programId ID de la carrera UC
   */
  hasSecondLanguageByProgram(studentPidm: number, programId: string): Observable<boolean> {
    const api = `${this.apiUrl}/${studentPidm}/${programId}`;
    return this.http.get<boolean>(api);
  }
}
