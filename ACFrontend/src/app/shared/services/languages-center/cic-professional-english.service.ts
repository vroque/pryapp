import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';

@Injectable()
export class CICProfessionalEnglishService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}LanguagesCenter/ProfessionalEnglish`;
  }

  /**
   * Verifica si un estudiante ha cursado lso ciclos necesarios del idioma inglés
   * para poder matricularse en la asignatura de inglés profesional en la UC
   * @param studentPidm
   */
  qualifyForEnrollInProfessionalEnglish(studentPidm: number): Observable<boolean> {
    const api = `${this.apiUrl}/${studentPidm}`;
    return this.http.get<boolean>(api);
  }
}
