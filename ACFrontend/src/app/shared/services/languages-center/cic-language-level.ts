export class CICLanguageLevel {
  constructor(public languageLevelId: string,
              public languageId: string,
              public languageName: string,
              public languageLevelName: string,
              public languageProgramId: number,
              public languageProgramName: string,
              public startCycle: number,
              public finalCycle: number) {
  }
}
