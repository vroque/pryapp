import { CICStudentUC } from './cic-student-uc';

export class CICCaie extends CICStudentUC {
  isSelected: boolean;
  docUrl: string;
  download: boolean;
  saveAs: string;
  processing: boolean;
  processingError: boolean;
}
