import { Schedule } from '../academic/schedule';
import { CICOffer } from './cic-offer';
export class CICOfferSchedule {
  schedule: Schedule[];
  offer: CICOffer;
}
