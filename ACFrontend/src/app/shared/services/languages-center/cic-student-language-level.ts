export class CICStudentLanguageLevel {
  languageId: string;
  languageName: string;
  lastCycle: string;
}
