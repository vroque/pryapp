import { Injectable } from '@angular/core';
import {  Http  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Schedule } from '../academic/schedule';
import { CICOffer } from './cic-offer';
import { CICOfferSchedule } from './cic-offer-schedule';

/**
 * Componente de horarios para alumno
 */
@Injectable()
export class CICScheduleService {
  private apiBaseUri: string;
  private apiUri: string;

  constructor(
    private http: Http,
    private config: ConfigService,
  ) {
    this.apiBaseUri = config.getApiURI();
    this.apiUri = `${this.apiBaseUri}LanguagesCenter`;
  }

  /**
   * Ofertas de Idiomas del CIC por campus idioma y ciclo
   * @return {Observable<CICOffer>} [description]
   */
  getOffersByCicle(
    campus: string,
    language: string,
    cycle: number,
  ): Observable<CICOffer[]> {
    const body: string = this.config.urlFormat({
      campus, cycle, language,
    });
    const api = `${this.apiUri}/OfferByCycle?${body}`;
    return this.http
      .get(api, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getSchedule(offer: CICOffer): Observable<Schedule[]> {
    const api = `${this.apiUri}/Schedule`;
    return this.http
      .post(api, offer, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getStudentSchedule(personID: number): Observable<CICOfferSchedule> {
    const api = `${this.apiUri}/Schedule/${personID}`;
    return this.http
      .get(api, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getStudentScheduleLast(personID: number): Observable<CICOfferSchedule> {
    const api = `${this.apiUri}/Schedule/${personID}/last`;
    return this.http
      .get(api, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
