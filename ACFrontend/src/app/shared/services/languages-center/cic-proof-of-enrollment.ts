import {Cost} from "../accounting/cost";
import {CICCycle} from "./cic-cycle";

export class CICProofOfEnrollment {
  constructor(public id: number,
              public name: string,
              public is_valid: boolean,
              public cost: Cost,
              public cycleList: CICCycle[],
              public enrolled: boolean,
              public stack_error: string[],
              public flow: string[]) {
  }
}
