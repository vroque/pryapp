import { CICCycle } from './cic-cycle';

export class CICLanguageRecord {
  headquarters: string;
  term: string;
  classroom: string;
  startDate: Date;
  languageId: string;
  cycle: CICCycle;
  enrolledDate: Date;
  n17: number;
  n18: number;
  n19: number;
  n20: number;
  n40: number;
  n41: number;
  n42: number;
  n43: number;
  n44: number;
  n45: number;
  n46: number;
  n47: number;
  n48: number;
  n49: number;
  n50: number;
  n51: number;
  style: number;
  testCycle: string;
  testComment: string;
  studentId: string;
}
