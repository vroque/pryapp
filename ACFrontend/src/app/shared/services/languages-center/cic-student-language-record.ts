import { CICLanguageRecord } from './cic-language-record';
import { CICLanguage } from './cic-language';

export class CICStudentLanguageRecord {
  language: CICLanguage;
  languageRecord: CICLanguageRecord;
}
