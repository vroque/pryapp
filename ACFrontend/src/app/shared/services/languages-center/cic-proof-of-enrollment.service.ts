import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";

import {ConfigService} from "../../../config/config.service";

import {Request} from "../atentioncenter/request";
import {CICProofOfEnrollment} from "./cic-proof-of-enrollment";

@Injectable()
export class CICProofOfEnrollmentService {

  private apiUrl: string;

  constructor(private http: Http, private configService: ConfigService) {
    this.apiUrl = `${configService.getApiURI()}LanguagesCenter/ProofOfEnrollment`;
  }

  /**
   * Datos básicos para preparar la solicitud de Constancia de Matrícula
   * @returns {Observable<CICProofOfEnrollment>}
   */
  get(): Observable<CICProofOfEnrollment> {
    return this.http
      .get(this.apiUrl, this.configService.getHeaders("json"))
      .map(this.configService.jsonExtractData)
      .catch(this.configService.handleError);
  }

  /**
   * Crear nueva solicitud de Constancia de Matrícula
   * @param {string} classroom
   * @param {string} description
   * @returns {Observable<Request>}
   */
  save(classroom: string, description: string): Observable<Request> {
    const data: object = {Classroom: classroom, Description: description};
    return this.http
      .post(this.apiUrl, data, this.configService.getHeaders("json"))
      .map(this.configService.jsonExtractData)
      .catch(this.configService.handleError);
  }
}
