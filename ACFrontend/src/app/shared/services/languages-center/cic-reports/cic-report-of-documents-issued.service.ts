import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../../config/config.service';

import { CICLanguage } from '../cic-language';
import { CICPeriods } from '../cic-periods';
import { CICLanguageProgram } from '../cic-language-program';

@Injectable()
export class CICReportOfDocumentsIssuedService {

  private readonly apiBaseUri: string;
  private readonly apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiBaseUri = config.getApiURI();
    this.apiUri = `${this.apiBaseUri}LanguagesCenter`;

  }

  /**
   * Idiomas del CIC
   * @return {Observable<CicLanguage>} [description]
   */
  getLanguages(): Observable<CICLanguage> {
    return this.http.get<CICLanguage>(`${this.apiUri}/Languages`);
  }

  /**
   * Períodos académicos del CIC
   * @return {Observable<CICPeriods>} Períodos CIC
   */
  getPeriods(): Observable<CICPeriods[]> {
    return this.http.get<CICPeriods[]>(`${this.apiUri}/Periods`);
  }

  /**
   * Programas de Idiomas del CIC
   * @return {Observable<CICLanguageProgram>} [description]
   */
  getLanguageProgram(): Observable<CICLanguageProgram[]> {
    return this.http.get<CICLanguageProgram[]>(`${this.apiUri}/LanguageProgram`);
  }

}
