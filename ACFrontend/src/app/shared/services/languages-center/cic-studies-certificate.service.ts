import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";

import {ConfigService} from "../../../config/config.service";

import {Request} from "../atentioncenter/request";
import {CICStudentLanguageRecord} from "./cic-student-language-record";
import {CICStudiesCertificate} from "./cic-studies-certificate";

@Injectable()
export class CICStudiesCertificateService {

  private apiUrl: string;

  constructor(private http: Http, private configService: ConfigService) {
    this.apiUrl = `${configService.getApiURI()}LanguagesCenter/StudiesCertificate`;
  }

  /**
   * Datos básicos para preparar la solicitud de Certificado de Estudios
   * @returns {Observable<CICStudiesCertificate>}
   */
  get(): Observable<CICStudiesCertificate> {
    return this.http
      .get(this.apiUrl, this.configService.getHeaders("json"))
      .map(this.configService.jsonExtractData)
      .catch(this.configService.handleError);
  }

  /**
   * Crear nueva solicitud de Certificado de Estudios
   * @param {string} languageId
   * @param {string} description
   * @param {CICStudentLanguageRecord} studentLanguageRecord
   * @returns {Observable<Request>}
   */
  save(languageId: string, description: string,
       studentLanguageRecord: CICStudentLanguageRecord[]): Observable<Request> {
    const data: object = {LanguageId: languageId, Cycles: studentLanguageRecord, Description: description};
    return this.http
      .post(this.apiUrl, data, this.configService.getHeaders("json"))
      .map(this.configService.jsonExtractData)
      .catch(this.configService.handleError);
  }
}
