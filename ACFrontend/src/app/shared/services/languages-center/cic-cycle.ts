import {CICLanguageLevel} from "./cic-language-level";

export class CICCycle {
  constructor(public classroom: string,
              public cycleNumber: number,
              public type: string,
              public begin: Date,
              public end: Date,
              public languageLevel: CICLanguageLevel) {
  }
}