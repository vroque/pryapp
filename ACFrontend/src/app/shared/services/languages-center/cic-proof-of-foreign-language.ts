import { Cost } from '../accounting/cost';
import { CICStudentLanguageLevel } from './cic-student-language-level';

export class CICProofOfForeignLanguage {
  languageLevels: CICStudentLanguageLevel[];
  id: number;
  name: string;
  is_valid: boolean;
  cost: Cost;
  stack_error: string;
  flow: string[];
}
