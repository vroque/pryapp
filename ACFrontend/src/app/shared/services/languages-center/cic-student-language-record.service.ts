import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';
import { CICStudentLanguageRecord } from './cic-student-language-record';

@Injectable()
export class CICStudentLanguageRecordService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiBaseUrl = config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}LanguagesCenter/StudentLanguageRecord`;
  }

  /**
   * Student language record
   * @param studentId
   */
  getStudentLanguageRecord(studentId: string): Observable<CICStudentLanguageRecord[]> {
    const api = `${this.apiUrl}/${studentId}`;
    return this.http.get<CICStudentLanguageRecord[]>(api);
  }
}
