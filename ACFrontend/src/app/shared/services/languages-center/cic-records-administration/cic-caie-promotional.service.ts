import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../../config/config.service';
import { GraduationPeriods } from '../../academic/graduation-periods';

@Injectable()
export class CICCaiePromotionalService {

  private readonly apiBaseUri: string;
  private apiUri: string;

  constructor(private http: HttpClient,
              private config: ConfigService) {
    this.apiBaseUri = config.getApiURI();
    this.apiUri = `${this.apiBaseUri}LanguagesCenter`;
  }

  /**
   * Períodos académicos de la UC en los que existen graduados
   */
  getGraduationPeriods(): Observable<GraduationPeriods> {
    const api = `${this.apiBaseUri}Shared/GraduationPeriods`;
    return this.http.get<GraduationPeriods>(api);
  }

}
