import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../../config/config.service';

import { CICPeriods } from '../cic-periods';
import { CICLanguageProgram } from '../cic-language-program';
import { CICLanguage } from '../cic-language';
import { CICLanguageLevel } from '../cic-language-level';
import { CICEnrolled } from '../cic-enrolled';

@Injectable()
export class CICProofOfEnrollmentService {

  private readonly apiBaseUri: string;
  private readonly apiUri: string;

  constructor(
    private http: HttpClient,
    private config: ConfigService
  ) {
    this.apiBaseUri = config.getApiURI();
    this.apiUri = `${this.apiBaseUri}LanguagesCenter`;
  }

  /**
   * Períodos académicos del CIC
   * @return {Observable<CICPeriods>} Períodos CIC
   */
  getPeriods(): Observable<CICPeriods> {
    const api = `${this.apiUri}/Periods`;
    return this.http.get<CICPeriods>(api);
  }

  /**
   * Programas de Idiomas del CIC
   * @return {Observable<CICLanguageProgram>} [description]
   */
  getLanguageProgram(): Observable<CICLanguageProgram> {
    const api = `${this.apiUri}/LanguageProgram`;
    return this.http.get<CICLanguageProgram>(api);
  }

  /**
   * Idiomas que se dictan en un programa CIC
   * @param  {string}                   languageProgram Programa CIC
   * @return {Observable<CicLanguage>}                 [description]
   */
  getLanguages(languageProgram: string): Observable<CICLanguage> {
    const api = `${this.apiUri}/Languages/${languageProgram}`;
    return this.http.get<CICLanguage>(api);
  }

  /**
   * Niveles de Idiomas según Idioma y Programa CIC
   * @param  {string}                       languageId      Código de Idioma CIC
   * @param  {string}                       languageProgram Programa CIC
   * @return {Observable<CICLanguageLevel>}                 [description]
   */
  getLanguageLevel(languageId: string, languageProgram: string)
    : Observable<CICLanguageLevel> {
    const api = `${this.apiUri}/LanguageLevel/${languageId}/${languageProgram}`;
    return this.http.get<CICLanguageLevel>(api);
  }

  /**
   * Matriculados en el CIC por Idioma-Programa-Nivel
   * @param  {string}                  languageId      Código de Idioma CIC
   * @param  {string}                  languageProgram Código de Programa CIC
   * @param  {string}                  languageLevel   Código de Nivel de Idioma
   * @return {Observable<CICEnrolled>}                 [description]
   */
  getEnrolled(languageId: string, languageProgram: string, languageLevel: string)
    : Observable<CICEnrolled> {
    const api = `${this.apiUri}/Enrolled/${languageId}/${languageProgram}/${languageLevel}`;
    return this.http.get<CICEnrolled>(api);
  }

}
