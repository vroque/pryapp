import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';
import { CICStudent } from './cic-student';

@Injectable()
export class CICSearchService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}LanguagesCenter/Student`;
  }

  /**
   * Search student
   * @param studentIdList
   */
  searchStudent(studentIdList: string[]): Observable<CICStudent[]> {
    const data: object = {Type: 'student', StudentIdList: studentIdList};
    return this.http.post<CICStudent[]>(this.apiUrl, data);
  }
}
