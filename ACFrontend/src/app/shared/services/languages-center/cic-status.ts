export class CICStatus {
  language: string;
  modality: string;
  modality_name: string;
  cycle: number;
  last_cycle: number;
  need_recategorize: boolean;
  enrollment_status: string;
}
