import {Cost} from "../accounting/cost";
import {CICLanguageLevels} from "./cic-language-levels";

export class CICDiploma {
  constructor(public languageLevels: CICLanguageLevels[],
              public id: number,
              public name: string,
              public is_valid: boolean,
              public cost: Cost,
              public stack_error: string,
              public flow: string[]) {
  }
}
