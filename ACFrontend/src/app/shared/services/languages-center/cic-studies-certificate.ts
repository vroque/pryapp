import {Cost} from "../accounting/cost";
import {CICStudentLanguageRecord} from "./cic-student-language-record";

export class CICStudiesCertificate {
  constructor(public studiedLanguagesList: CICStudentLanguageRecord[],
              public id: number,
              public name: string,
              public is_valid: boolean,
              public cost: Cost,
              public stack_error: string[],
              public flow: string[]) {
  }
}
