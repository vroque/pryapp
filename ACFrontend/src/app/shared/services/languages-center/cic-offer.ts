export class CICOffer {
  id: number;
  startDate: Date;
  endDate: Date;
  section: string;
  studyPlan: string;
  course: string;
  teacherID: string;
  teacher: string;
  campus: string;
  program: string;
  modality: string;
  cycle: number;
}
