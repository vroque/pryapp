export class CICConstAcredIdiomExt {
  PIDM: number;
  IDPersona: string;
  IDAlumno: string;
  NomCompleto: string;
  IDEscuela: string;
  Idioma: string;
  Ciclo: string;
  FechaInicioCIC: Date;
  IDEscuelaADM: string;
  NombreModalidad: string;
  updateDate: Date;
}
