export class CICEnrolled {
  constructor(
    public id: number,
    public student_id: string,
    public student_name: string,
    public language_id: string,
    public language_name: string,
    public academic_period: string,
    public cycle: number,
    public classroom: string,
    public start_date: Date,
    public start_date_str: string,
    public end_date: Date,
    public end_date_str: string,
    public enrollment_date: Date,
    public enrollment_date_str: string,
    public type_of_student: string,
    public language_program: string,
    public last_approved_cycle: string
  ) {}
}
