export class CICCustomCaie {
  student_names: string;
  student_id: string;
  college_name: string;
  program_name: string;
  languageName: string;
  languageLevelName: string;
  avg: number;
  totalHours: number;
  startDate: Date;
  endDate: Date;
  graduationModality: string;
}
