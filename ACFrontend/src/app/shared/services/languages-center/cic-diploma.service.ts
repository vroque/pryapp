import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";

import {ConfigService} from "../../../config/config.service";

import {Request} from "../atentioncenter/request";
import {CICDiploma} from "./cic-diploma";

@Injectable()
export class CICDiplomaService {
  private apiUrl: string;

  constructor(private http: Http, private configService: ConfigService) {
    this.apiUrl = `${configService.getApiURI()}LanguagesCenter/Diploma`;
  }

  /**
   * Datos básicos para preparar la solicitud de Diploma
   * @returns {Observable<CICDiploma>}
   */
  get(): Observable<CICDiploma> {
    return this.http
      .get(this.apiUrl, this.configService.getHeaders("json"))
      .map(this.configService.jsonExtractData)
      .catch(this.configService.handleError);
  }

  /**
   * Crear nueva solicitud de Diploma
   * @param {string} languageLevelId
   * @param {string} description
   * @returns {Observable<Request>}
   */
  save(languageLevelId: string, description: string): Observable<Request> {
    const data: object = {LanguageLevelId: languageLevelId, Description: description};
    return this.http
      .post(this.apiUrl, data, this.configService.getHeaders("json"))
      .map(this.configService.jsonExtractData)
      .catch(this.configService.handleError);
  }

}
