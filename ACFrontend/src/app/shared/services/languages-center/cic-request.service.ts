import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";

import {ConfigService} from "../../../config/config.service";

import {Request} from "../atentioncenter/request";

@Injectable()
export class CICRequestService {

  private readonly apiUrl: string;

  constructor(private http: Http, private config: ConfigService) {
    this.apiUrl = `${config.getApiURI()}LanguagesCenter/Request`;
  }

  /**
   * Languages Center request list
   * @returns {Observable<Request[]>}
   */
  get(): Observable<Request[]> {
    return this.http.get(this.apiUrl, this.config.getHeaders("json"))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
