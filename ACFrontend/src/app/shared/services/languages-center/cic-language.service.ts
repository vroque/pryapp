import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { CICLanguage } from './cic-language';

@Injectable()
export class CICLanguageService {
  private apiBaseUri: string;
  private apiUri: string;

  constructor(private http: Http,
              private configService: ConfigService) {
    this.apiBaseUri = configService.getApiURI();
    this.apiUri = `${this.apiBaseUri}LanguagesCenter`;
  }

  /**
   * Idiomas del CIC
   * @returns {Observable<CicLanguage>}
   */
  getLanguages(): Observable<CICLanguage> {
    const api = `${this.apiUri}/Languages`;
    return this.http.get(api)
      .map(this.configService.jsonExtractData)
      .catch(this.configService.handleError);
  }
}