export class CICSettings {
  id: number;
  name: string;
  description: string;
  active: boolean;
}
