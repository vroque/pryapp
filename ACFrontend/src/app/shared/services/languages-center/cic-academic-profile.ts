import { CICLanguage } from './cic-language';

export class CICAcademicProfile {
  studentId: string;
  language: CICLanguage;
  cycle: string;
  startDateAtCic: Date;
}
