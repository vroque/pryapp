import { CICLanguageLevel } from './cic-language-level';
import { CICLanguage } from './cic-language';

export class CICLanguageLevels {
  public languages: CICLanguage;
  public lastCycle: number;
  public languageLevelList: CICLanguageLevel[];
}
