export class SettingsDetail {
  id: number;
  name: string;
  description: string;
  order: number;
  type: string;
  strValue: string;
  numberValue: number;
  dateValue: Date;
  settingsId: number;
}
