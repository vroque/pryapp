import { Injectable } from '@angular/core';
import { ConfigService } from '../../../config/config.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SettingsDetail } from './settings-detail';

@Injectable()
export class SettingsDetailService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Settings/SettingsDetail`;
  }

  /**
   * Settings detail list
   */
  get(): Observable<SettingsDetail[]> {
    return this.http.get<SettingsDetail[]>(this.apiUrl);
  }

  /**
   * Settings detail for terms for enrollment
   */
  getTermsForEnrollment(): Observable<SettingsDetail> {
    const api = `${this.apiUrl}/type/terms-enrollment`;
    return this.http.get<SettingsDetail>(api);
  }

  /**
   * Save new settings detail
   * @param settingsDetail Settings detail to save
   */
  save(settingsDetail: SettingsDetail): Observable<SettingsDetail> {
    return this.http.post<SettingsDetail>(this.apiUrl, settingsDetail);
  }

  /**
   * Update settings detail
   * @param settingsDetail Settings detail to update
   */
  update(settingsDetail: SettingsDetail): Observable<SettingsDetail> {
    return this.http.put<SettingsDetail>(this.apiUrl, settingsDetail);
  }

  /**
   * Delete settings detail
   * @param settingsDetail Settings detail to delete
   */
  delete(settingsDetail: SettingsDetail): Observable<boolean> {
    const api = `${this.apiUrl}/${settingsDetail.id}`;
    return this.http.delete<boolean>(api);
  }
}
