import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MailService {
  private url: string;  // a URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}shared/`;
  }

  requestDocument (request_id: number): Observable<string> {
    return this.http
      .post(`${this.url}mailRequest/${request_id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
  endTransfer (request_id: number): Observable<string> {
    const url = `${this.config.getApiURI()}/convalidation/mailEndTransfer/${request_id}`;
    return this.http
      .post(url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
