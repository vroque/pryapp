export class Survey {
  active: boolean;
  begin_date: Date;
  description: string;
  edit: boolean;
  end_date: Date;
  id: number;
  name: string;
  url: string;
}
