import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { Survey } from './survey';

@Injectable()
export class SurveyService {

  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Settings/Survey`;
  }

  /**
   * Survey list
   * @returns {Observable<Survey[]>}
   */
  getSurveyList(): Observable<Survey[]> {
    return this.http.get<Survey[]>(this.apiUrl);
  }

  /**
   * Save new survey
   * @param {Survey} survey Survey to save
   * @returns {Observable<Survey>} Survey saved
   */
  save(survey: Survey): Observable<Survey> {
    return this.http.post<Survey>(this.apiUrl, survey);
  }

  /**
   * Update survey
   * @param {Survey} survey Survey to update
   * @returns {Observable<Survey>} Survey updated
   */
  update(survey: Survey): Observable<Survey> {
    return this.http.put<Survey>(this.apiUrl, survey);
  }

  /**
   * Delete survey
   * @param {Survey} survey Survey to delete
   * @returns {Observable<boolean>}
   */
  delete(survey: Survey): Observable<boolean> {
    const api = `${this.apiUrl}/${survey.id}`;
    return this.http.delete<boolean>(api);
  }

}
