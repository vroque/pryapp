import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';

@Injectable()
export class ConsumeApiService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Shared/ConsumeApi`;
  }

  /**
   * Check if an API is active for its consumption
   * @param cardAbbr
   */
  isActive(apiAbbr): Observable<boolean> {
    const api = `${this.apiUrl}/${apiAbbr}`;
    return this.http.get<boolean>(api);
  }
}
