import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';
import { Graduated } from './graduated';

@Injectable()
export class GraduatesService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Academic/Graduates`;
  }

  /**
   * Get all graduates
   */
  getAllGraduates(): Observable<Graduated[]> {
    return this.http.get<Graduated[]>(this.apiUrl);
  }

  /**
   * Get graduates by term
   * @param term
   */
  getGraduatesByTerm(term: string): Observable<Graduated[]> {
    return this.http.get<Graduated[]>(`${this.apiUrl}?term=${term}`);
  }

  /**
   * Update graduates
   * @param graduates
   */
  updateGraduates(graduates: Graduated[], bachelorResolutionDate: Date,
                  titleResolutionDate: Date): Observable<Graduated[]> {
    const data = {Graduates: graduates, BachelorResolutionDate: bachelorResolutionDate, TitleResolutionDate: titleResolutionDate};
    return this.http.put<Graduated[]>(this.apiUrl, data);
  }
}
