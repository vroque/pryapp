import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class CurriculumService {
  datachange: Observable<any>;
  private url: string;
  private urlDiplomaed: string;

  constructor(
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}academic/curriculum`;
    this.urlDiplomaed = `${config.getApiURI()}academic/diplomaed`;
  }

  get(department: string, program: string, campus: string, term: string): Observable<any[]> {
    const body: string = this.config.urlFormat({
      'department': department,
      'program': program,
      'campus': campus,
      'term': term,
    });
    return this.http
      .get(`${this.url}?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getByPerson(person_id: number, program: string): Observable<any[]> {
    const body: string = this.config.urlFormat({person_id, program});
    return this.http
      .get(`${this.url}?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getDiplomaed(department: string, program: string, campus: string, term: string): Observable<any[]> {
    const body: string = this.config.urlFormat({
      'department': department,
      'program': program,
      'campus': campus,
      'term': term,
    });
    return this.http
      .get(`${this.urlDiplomaed}?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
