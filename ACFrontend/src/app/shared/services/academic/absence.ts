import { AbsenceCourse } from './absence.course';
export class Absence {
  coursename: string;
  course: string;
  nrc: string;
  percentage: number;
  pidm: string;
  program: string;
  term: string;
  course_absences: AbsenceCourse[];
}
