import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { Period } from './period';
import { AcademicProfile } from './academic.profile';
import { observableToBeFn } from 'rxjs/testing/TestScheduler';

@Injectable()
export class EnrollmentTermService {

  private readonly url: string;

  constructor(private httpClient: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}academic/EnrollmentTerm`;
  }

  /**
   * Lista todos los periodos en los que se a matriculado el alumno
   */
  getAllTermsEnrollment(): Observable<string[]> {
    return this.httpClient.get<string[]>(`${this.url}?type=1`);
  }

  /**
   * muestra todos los periodos concluidos que ah terminado el alumno
   */
  getOldsTermsEnrollment(): Observable<string[]> {
    return this.httpClient.get<string[]>(`${this.url}?type=2`);
  }

  /**
   * Obtiene todos los períodos académicos UC en los que existieron matrícula
   * @returns {Observable<string[]>}
   */
  getAllEnrollmentPeriods(): Observable<Period[]> {
    return this.httpClient.get<Period[]>(`${this.url}?type=3`);
  }

  /**
   * Obtener los periodos matriculados por perfil
   * @param profile Perfil
   */
  getAllTermsEnrollmentByProfile(profile: AcademicProfile): Observable<string[]> {
    return this.httpClient.post<string[]>(this.url, profile);
  }
}
