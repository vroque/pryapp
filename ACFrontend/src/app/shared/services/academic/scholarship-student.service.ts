import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { ScholarshipStudent } from './scholarship-student';

@Injectable()
export class ScholarshipStudentService {
  private apiBaseUri: string; // api base url
  private apiUrl: string;

  constructor(private configService: ConfigService, private http: Http) {
    this.apiBaseUri = configService.getApiURI();
    this.apiUrl = `${configService.getApiURI()}/Academic/ScholarshipStudent`;
  }

  /**
   * Ingresante PRONABEC según tipo PRONABEC, Escuela Académico Profesional y período de matrícula
   * @param {string} pronabecId
   * @param {string} schoolId
   * @param {string} enrolledTerm
   * @returns {Observable<ScholarshipStudent>}
   */
  getPronabecScholarshipStudents(pronabecId: string, schoolId: string, enrolledTerm: string): Observable<ScholarshipStudent[]> {
    const type = 'searchEnrolledByPronabecType';
    const data: any = {Type: type, ScholarshipId: pronabecId, SchoolId: schoolId, EnrolledPeriod: enrolledTerm};
    return this.http.post(this.apiUrl, data, this.configService.getHeaders('json'))
      .map(this.configService.jsonExtractData)
      .catch(this.configService.handleError);
  }

}
