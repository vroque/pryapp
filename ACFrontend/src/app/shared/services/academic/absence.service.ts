import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Absence } from './absence';

@Injectable()
export class AbsenceService {
  private url: string;
  datachange: Observable<any>;

  constructor(
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}academic/absence`;
  }

  get(): Observable<Absence[]> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
