import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Course } from '../../../shared/services/academic/course';
import { CloseEnrollment } from '../../../shared/services/academic/closeEnrollment';

@Injectable()
export class ConsolidatedService {
  private url: string;
  datachange: Observable<any>;

  constructor(
    private http: Http,
    private config: ConfigService
  ) {  this.url = `${config.getApiURI()}academic/consolidated`; }

  getCconsolidatedByTerm(term: string): Observable<{ courses: Course[], closeEnrollment: CloseEnrollment }> {
    const body = this.config.urlFormat({ 'term': term });
    return this.http
      .get(`${this.url}?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
