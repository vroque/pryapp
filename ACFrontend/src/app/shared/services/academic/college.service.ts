import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from '../../../config/config.service';
import { College } from './college';

@Injectable()
export class CollegeService {
  private readonly url: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}academic/college`;
  }

  query(): Observable<College[]> {
    return this.http.get<College[]>(this.url);
  }
}
