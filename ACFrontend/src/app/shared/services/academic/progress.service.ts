import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class AcademicProgressService {
  private url: string;
  datachange: Observable<any>;

  constructor(
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}academic/progress`;
  }

  get(person_id: number, program: string): Observable<any[]> {
    const body = this.config.urlFormat({'program': program});
    return this.http
      .get(`${this.url}/${person_id}?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
