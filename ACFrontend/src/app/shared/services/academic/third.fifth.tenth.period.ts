export class ThirdFifthTenthPeriod {
  term: string;
  order: number;
  orders: number;
  score: number;
  period: string;
}
