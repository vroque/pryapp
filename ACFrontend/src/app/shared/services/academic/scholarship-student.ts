export class ScholarshipStudent {
  studentPidm: number;
  studentId: string;
  studentName: string;
  entryPeriod: string;
  otherPeriod: string;
  otherPeriodDescription: string;
  schoolId: string;
  schoolName: string;
  typeOfIncomeId: number;
  typeOfIncomeRecruiter: string;
  typeOfIncomeName: string;
  modalityId: string;
  modalityName: string;
  isSelected: boolean;
  isPronabec: boolean;
  academicStatus: number;
  docUrl: string;
  download: boolean;
  saveAs: string;
  processing: boolean;
  processingError: boolean;
}
