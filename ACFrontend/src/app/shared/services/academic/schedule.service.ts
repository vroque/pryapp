import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Schedule } from './schedule';

@Injectable()
export class ScheduleService {
  datachange: Observable<any>;
  private url: string;

  constructor(
    private http: Http,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}academic/schedule`;
  }

  /**
   * Obtener horario de estudiante
   * @param term Periodo
   */
  get(term: string): Observable<{ term: string; schedule: Schedule[]}> {
    return this.http
      .get(`${this.url}?term=${term}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  // tslint:disable-next-line:max-line-length
  getScheduleOffer(nrcpadre: string, term: string, subject: string, course: string, codliga: string): Observable<Schedule[]> {
    const uri = `${this.config.getApiURI()}academic/scheduleoffercourse`;
    const formattedData: string = this.config.urlFormat({ nrcpadre, term, subject, course, codliga });
    return this.http
      .get(
        `${uri}?${formattedData}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
