import { AcademicProfile } from './academic.profile';
export class Student {
  id: string;
  person_id: number;
  first_name: string;
  last_name: string;
  gender: string;
  scala: string;
  full_name: string;
  profiles: AcademicProfile[];
  profile: AcademicProfile;
}
