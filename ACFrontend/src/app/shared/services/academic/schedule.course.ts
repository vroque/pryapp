export class ScheduleCourse {
  id: string;
  nrc: string;
  name: string;
  module: string;
  classroom: string;
  start: string;
  end: string;
  dateStart: string;
  dateEnd: string;
  teacher: string;
  height: string;
  top: string;
  parent: boolean;
}
