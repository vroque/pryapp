import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Student } from './student';
import { ConfigService } from '../../../config/config.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FreshmanService {
  private url: string;  // a URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}academic/freshman`;
  }

  find (term: string, program: string, campus: string ): Observable<Student[]> {
    const body: any = this.config.urlFormat(
      {'term': term, 'program': program, 'campus': campus});
    return this.http
      .get(`${this.url}/?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
