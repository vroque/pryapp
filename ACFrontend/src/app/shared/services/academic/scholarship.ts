export class Scholarship {
  typeOfIncomeId: number;
  typeOfIncomeRecruiter: string;
  typeOfIncomeName: string;
  modalityId: string;
  modalityName: string;
  isScholarship: boolean;
  isPronabec: boolean;
}
