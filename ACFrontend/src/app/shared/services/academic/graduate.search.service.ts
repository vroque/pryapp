import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Graduate } from './graduate';

@Injectable()
export class GraduateSearchService {
  private url: string;
  datachange: Observable<any>;

  constructor(
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}academic/graduatesearch`;
  }

  query(search_data:Array<any>): Observable<Graduate[]> {
    return this.http
      .post(this.url, search_data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  searchForRequirements(search_data: any[]): Observable<Graduate[]> {
    return this.http.post(this.url, search_data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  searchForDNIs(dnis: string): Observable<Graduate[]> {
      return this.http.get(`${this.url}/${dnis}`)
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
