import { Student } from './student';

export class StudentFactory {

  constructor() {
    if (!StudentFactory.isCreating) {
        throw new Error('You can\'t call new in Singleton instances! Call SingletonService.getInstance() instead.');
      }
  }
  static instance: StudentFactory;
  static isCreating = false;

  private student: Student;

  static getInstance() {
    if (StudentFactory.instance == null) {
      StudentFactory.isCreating = true;
      StudentFactory.instance = new StudentFactory();
      StudentFactory.isCreating = false;
    }
    return StudentFactory.instance;
  }

  set(student: Student) {
    this.student = student || (window as any).student;
  }

  get(): Student {
    return this.student || (window as any).student;
  }
}
