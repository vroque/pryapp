import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

// services
import { ConfigService } from '../../../config/config.service';

// models
import { ScholarshipStudent } from './scholarship-student';
import { Student } from './student';

@Injectable()
export class StudentService {
  private apiBaseUri: string; // api base uri
  private apiUrl: string;  // a URL to web API

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiBaseUri = this.config.getApiURI();
    this.apiUrl = `${config.getApiURI()}Academic/Student`;
  }

  get(): Observable<Student> {
    return this.http.get<Student>(this.apiUrl);
  }

  find(studentId: string, id: string): Observable<Student> {
    const body: string = this.config.urlFormat({by: id});
    return this.http.get<Student>(`${this.apiUrl}/${studentId}?${body}`);
  }

  findStudents(nombres: string[]): Observable<Student[]> {
    const data: any = {Type: 'studentProfiles', Students: nombres};
    return this.http.post<Student[]>(this.apiUrl, data);
  }

  /**
   * Ingresantes PRONABEC
   * @returns {Observable<ScholarshipStudent>}
   */
  getPronabecStudents(studentsId: string[]): Observable<ScholarshipStudent[]> {
    const type = 'searchPronabec';
    const data: any = {Type: type, Students: studentsId};
    return this.http.post<ScholarshipStudent[]>(this.apiUrl, data);
  }
}
