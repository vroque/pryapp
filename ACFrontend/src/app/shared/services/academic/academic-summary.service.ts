import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { AcademicSummary } from './academic-summary';

@Injectable()
export class AcademicSummaryService {
  private url: string;  // URL to web API

  constructor(
    private http: Http,
    private config: ConfigService,
  ) {
    this.url = `${ config.getApiURI()}academic/AcademicSummary`;
  }

  get(): Observable<AcademicSummary> {
    return this.http
      .get(`${this.url}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getByProfileData(profile: any): Observable<AcademicSummary> {
    return this.http
      .post(this.url, profile, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
