import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ConfigService} from '../../../config/config.service';
import { AcademicProfile } from './academic.profile';
import {Course} from './course';

@Injectable()
export class EnrollmentCourseService {

  private url: string;

  constructor(private http: Http, private config: ConfigService) {
    this.url = `${config.getApiURI()}academic/EnrollmentCourses`;
  }

  list(profile: AcademicProfile): Observable<Course[]> {
    return this.http
      .post(this.url, profile, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
