export class AcademicSummary {
  total_credits: number;
  actual_credits: number;
  total_cycles: number;
  actual_cycle: number;
  pga: number;
}
