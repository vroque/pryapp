export class Entrant {
  student_name: string;
  student_id: string;
  type_of_income_id: number;
  type_of_income_name: string;
  entry_period: Date;
  school_id: string;
  school_name: string;
  modality_id: string;
  modality_name: string;
}
