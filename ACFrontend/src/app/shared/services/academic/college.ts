import { Program } from './program';
export class College {
  id: string;
  name: string;
  programs: Program[];
}
