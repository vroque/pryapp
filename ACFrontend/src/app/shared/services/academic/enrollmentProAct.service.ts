import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {ConfigService} from '../../../config/config.service';

@Injectable()
export class EnrollmentProActService {
  private url: string;
  constructor(private http: Http, private config: ConfigService) {
    this.url = `${config.getApiURI()}academic/EnrollmentCourses`;
  }

}
