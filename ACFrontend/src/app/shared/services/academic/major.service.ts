import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';

@Injectable()
export class MajorService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Academic/Majors`;
  }

  /**
   * Get all UC majors
   */
  getMajors(): Observable<any[]> {
    return this.http.get<any[]>(this.apiUrl);
  }
}
