export class Graduate {
  student_id: string;
  student_name: string;
  graduation_period: Date;
  school_id: string;
  school_name: string;
}
