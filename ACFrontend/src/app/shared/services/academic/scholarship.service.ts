import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from '../../../config/config.service';
import { Scholarship } from './scholarship';

@Injectable()
export class ScholarshipService {
  private readonly apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${config.getApiURI()}Academic/Scholarships`;
  }

  /**
   * Becas Modalidad PRONABEC
   * @returns {Observable<Scholarship[]>}
   */
  getPronabecScholarships(): Observable<Scholarship[]> {
    const apiUrl = `${this.apiUri}/pronabec`;
    return this.http.get<Scholarship[]>(apiUrl);
  }
}
