import { ScheduleCourse } from './schedule.course';
export class Schedule {
  day: string;
  courses: ScheduleCourse[];
}
