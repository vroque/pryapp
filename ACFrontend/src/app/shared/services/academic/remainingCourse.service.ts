import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ConfigService} from '../../../config/config.service';
import {Course} from './course';

@Injectable()
export class RemainingCourseService {
  private url: string;
  constructor(private http: Http, private config: ConfigService) {
    this.url = `${config.getApiURI()}academic/RemainingCourses`;
  }

  list(): Observable<Course[]> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  listByStudent(personId: number, program: string): Observable<Course[]> {
    const data = this.config.urlFormat({
      'person_id': personId,
      'program': program
    });
    return this.http
      .get(`${this.url}?${data}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
