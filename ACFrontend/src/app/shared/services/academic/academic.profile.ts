import { AdmisionType } from '../admision/admision.type';
import { Campus } from './campus';
import { College } from './college';
import { Program } from './program';
export class AcademicProfile {
  div: string;
  department: string;
  subject: string;
  campus: Campus;
  college: College;
  program: Program;
  levl: string;
  adm_type: AdmisionType;
  term_catalg: string;
  status: number;
  status_name: string;
  person_id: number;
  graduateInfo: any;
}
