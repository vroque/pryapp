export class Course {
  components: any;
  credits: number;
  cycle: number;
  date: Date;
  equivalent: string;
  id: string;
  is_active: string;
  is_aproved: boolean;
  name: string;
  nrc: string;
  campus: string;
  department: string;
  daysUploadNote: number;
  observation: string;
  practicalHours: number;
  prerequisite_list: any[];
  score: string;
  score_detail: any;
  score_old: string;
  subject: string;
  term: string;
  theoreticalHours: number;
  type: string;
}
