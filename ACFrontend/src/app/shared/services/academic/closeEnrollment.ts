export class CloseEnrollment {
  term: string;
  pidm: number;
  rgrpCode: string;
  user: string;
  activityDate: Date;
  surrogateId: number;
  version: number;
  userId: string;
  dataOrigin: string;
  vpdiCode: string;
}
