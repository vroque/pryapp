export class Graduated {
  student_pidm: number;
  student_id: string;
  student_name: string;
  entry_period: string;
  graduation_period: string;
  other_period: string;
  other_period_description: string;
  school_id: string;
  school_name: string;
  credits_o: number;
  credits_e: number;
  volunteering: boolean;
  foreign_language: boolean;
  internship: boolean;
  bachiller: boolean;
  titulo: boolean;
  graduation_date: Date;
  is_selected: boolean;
  payment_status: number;
  academic_status: number;
  doc_url: string;
  download: boolean;
  save_as: string;
  processing: boolean;
  processingError: boolean;
}
