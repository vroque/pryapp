export class Assistance {
  pidm: string;
  term: string;
  partterm: string;
  campus: string;
  departament: string;
  program: string;
  nrc: string;
  subject: string;
  course: string;
  namecourse: string;
  absencedate: string;
  percentageinasistence: string;
}
