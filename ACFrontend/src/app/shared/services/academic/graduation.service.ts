import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from '../../../config/config.service';
import { Graduated } from './graduated';
import { Period } from './period';

@Injectable()
export class GraduationService {

  private readonly apiBaseUri: string;
  private readonly apiAcademicUri: string;
  private readonly apiAttentionCenterUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiBaseUri = this.config.getApiURI();
    this.apiAcademicUri = `${this.apiBaseUri}Academic`;
    this.apiAttentionCenterUri = `${this.apiBaseUri}AtentionCenter`;
  }

  /**
   * Períodos Académicos de Estudiantes Egresados UC
   * @returns {Observable<Period>}
   */
  getGraduationPeriods(): Observable<Period[]> {
    const api = `${this.apiAcademicUri}/GraduationPeriods`;
    return this.http.get<Period[]>(api);
  }

  /**
   * Egresados UC
   * @param {string} type Tipo de búsqueda
   * @param {string} schoolId Código de la Escuela Académico Profesional
   * @param {string} graduationPeriod Período de graduación
   * @param {any[]} students Arreglo de códigos de graduados UC
   * @returns {Observable<ProofOfEntry>}
   */
  getGraduates(type: string, schoolId: string, graduationPeriod: string, students: any[]): Observable<Graduated[]> {
    const api = `${this.apiBaseUri}Academic/Graduates`;
    const data: any = {Type: type, SchoolId: schoolId, GraduationPeriod: graduationPeriod, Students: students};
    return this.http.post<Graduated[]>(api, data);
  }

  /**
   * Constancia de Egresado
   * @param {any[]} graduates Egresados
   * @param date Fecha de egresado
   * @returns {Observable<ProofOfEntry>}
   */
  getProofOfGraduation(graduates: any, date: Date): Observable<Graduated[]> {
    const api = `${this.apiBaseUri}AtentionCenter/ProofOfGraduation`;
    const data: any = {Date: date, Graduates: graduates};
    return this.http.post<Graduated[]>(api, data);
  }

}
