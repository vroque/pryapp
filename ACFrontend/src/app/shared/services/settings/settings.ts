import { SettingsType } from '../settings-type/settings-type';
export class Settings {
  abbr: string;
  active: boolean;
  description: string;
  edit: boolean;
  hasDetail: boolean;
  id: number;
  name: string;
  settingsType: SettingsType;
}
