import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';

import { Settings } from './settings';

@Injectable()
export class SettingsService {

  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Settings/Settings`;
  }

  /**
   * Settings list
   * @returns {Observable<Settings[]>} Settings list
   */
  getSettingsList(): Observable<Settings[]> {
    return this.http.get<Settings[]>(this.apiUrl);
  }

  /**
   * Save new settings
   * @param {Settings} settings Settings to save
   * @returns {Observable<Settings>} Settings saved
   */
  save(settings: Settings): Observable<Settings> {
    return this.http.post<Settings>(this.apiUrl, settings);
  }

  /**
   * Update settings
   * @param {Settings} settings Settings to update
   * @returns {Observable<Settings>} Settings updated
   */
  update(settings: Settings): Observable<Settings> {
    return this.http.put<Settings>(this.apiUrl, settings);
  }

  /**
   * Delete settings
   * @param {Settings} settings Settings to delete
   * @returns {Observable<boolean>}
   */
  delete(settings: Settings): Observable<boolean> {
    const api = `${this.apiUrl}/${settings.id}`;
    return this.http.delete<boolean>(api);
  }

}
