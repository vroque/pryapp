import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Transfer } from './transfer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TransferService {
  private url: string;  // a URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}convalidation/transfer`;
  }

  list (person_id: number): Observable<Transfer[]> {
    return this.http
      .get(`${this.url}/${person_id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  byTerm (person_id: number, term: string): Observable<Transfer[]> {
    return this.http
      .get(`${this.url}/${person_id}?term=${term}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  get (person_id: number, term: string, tseq: number, seq: number): Observable<Transfer> {
    const data = this.config.urlFormat({
      term: term, tseq: tseq, seq: seq
    });
    return this.http
      .get(`${this.url}/${person_id}?${data}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
