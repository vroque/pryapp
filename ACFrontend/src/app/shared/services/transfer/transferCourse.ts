export class TransferCourse {
  id: string;
  proc_seq_no: number;
  proc_course_name: string;
  proc_course_credits: number;
  proc_course_score: string;
  uc_seq_no: number;
  uc_course_subj: string;
  uc_course_number: string;
  uc_course_name: string;
  uc_course_credits: number;
  uc_course_score: string;
}
