import { TransferCourse } from './transferCourse';
export class Transfer {
  person_id: number;
  levl: string;
  term: string;
  date: Date;
  institution_id: string;
  institution_name: string;
  program: string;
  seq: string;
  tseq: string;
  TranferCourse: TransferCourse[];
}
