export class PPPractices {
  pidm: number;
  student_id: string;
  student_names: string;
  div: string;
  lvl: string;
  program: string;
  date: Date;
  certified_id: string;
  practice_id: string;
  uri: string;
}
