import { ConfigService } from '../../../config/config.service';
import { Debt } from './debt';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Student } from '../../services/academic/student';

@Injectable()
export class DebtService {
  private url: string;  // a URL to web API

  constructor (
    private http: Http,
    private httpClient: HttpClient,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}accounting/debt`;
  }

  query (personId: number): Observable<Debt[]> {
    return this.http
      .get(`${this.url}/${personId}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  restrictionDebtbyTerm (term: string, student: Student): Observable<boolean> {
    const data = { student: student, term: term };
    return this.httpClient.post<boolean>(this.url, data);
  }
}
