export class EconomicStatus {
  div: string;
  campus: string;
  term: string;
  concept_id: string;
  description: string;
  student_id: string;
  code_banck: number;
  ammount: number;
  deposit: number;
  total: number;
  interest: number;
  paymen_date: Date;
  extension_date: Date;
  documentReceivable: Boolean;
  idSeccionC: string;
  fecInic: Date;
  discount: number;
  payment_online_id: string;
  details_grouped_gebts:EconomicStatus[];
  mat: boolean;
}
