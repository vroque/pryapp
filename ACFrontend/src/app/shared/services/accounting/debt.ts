export class Debt {
  ammount: number;
  concept_id: string;
  deposit: number;
  description: string;
  discount: number;
  document_number: string;
  interest: number;
  part: string;
  penalty: number;
  person_id: number;
  total: number;
  year: string;
}
