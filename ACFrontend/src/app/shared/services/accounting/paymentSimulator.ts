import { EconomicStatus } from './economicStatus';

export class PaymentSimulator {
  department: string;
  scala: number;
  term: number;
  credit_cost: number;
  enrollment_cost: number;
  student_card_cost: number;
  health_insurance_cost: number;
  studyPlan: number;
  dataCuotas: EconomicStatus[];
}
