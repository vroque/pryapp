import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { EconomicStatus } from './economicStatus';

@Injectable()
export class EconomicStatusService{
  private url: string;  // a URL to web API
  constructor(
    private http: Http,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}accounting/EconomicStatus`;
  }

  getGroupedDebtsPayments(term: string, pidm: number, type: string): Observable<EconomicStatus[]> {
    return this.http
      .get(`${this.url}?term=${term}&pidm=${pidm}&type=${type}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getPaymentOnlineID(data: EconomicStatus): Observable<EconomicStatus> {
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getTermsEconomicStatus(pidm: number): Observable<string[]>{
    return this.http
      .get(`${this.url}?pidm=${pidm}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
