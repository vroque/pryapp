import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ConfigService} from '../../../config/config.service';
import {Kinship} from './kinship';

@Injectable()
export class KinshipService {

  private uri: string;

  constructor(private http: Http, private config: ConfigService) {
    this.uri = `${config.getApiURI()}Personal/Kinship`;
  }

  /**
   * Lista de Parentesco
   * @returns {Observable<Kinship>}
   */
  getKinship(): Observable<Kinship> {
    return this.http.get(this.uri).map(this.config.jsonExtractData).catch(this.config.handleError);
  }

}
