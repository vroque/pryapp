export class Persona {
  document_type: string;
  document_number: string;
  first_name: string;
  fathers_last_name: string;
  mothers_last_name: string;
  last_name: string;
  full_name: string;
  address: string;
  gender: string;
  birthdate: Date;
  country: string;
  ubigeo: string;
  marital_status: string;
  id: number;
  cellphone: string;
  rpm: string;
  email1: string;
  email2: string;
  userName: string;
}
