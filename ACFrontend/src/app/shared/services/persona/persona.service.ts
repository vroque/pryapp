import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ConfigService} from '../../../config/config.service';
import {Persona} from './persona';

@Injectable()
export class PersonaService {

  private url: string;

  constructor(private httpClient: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}Personal/Person`;
  }

  /**
   * Obtener datos de la persona logeada
   */
  get(): Observable<Persona> {
    return this.httpClient.get<Persona>(this.url);
  }

  /**
   * Obtiene la información de una persona por su dni
   * @param dni Dni
   */
  getByDni(dni: string): Observable<Persona> {
    return this.httpClient.get<Persona>(`${this.url}?dni=${dni}`);
  }

  /**
   * Búsqueda de persona por número de documento (DNI o CE)
   * @param {string} type
   * @param {Persona[]} people
   * @returns {Observable<Persona>}
   */
  searchPeopleByDocumentNumber(type: string, people: Persona[]): Observable<Persona> {
    const data: any = {Type: type, People: people};
    return this.httpClient.post<Persona>(this.url, data);
  }

  /**
   * Nueva persona
   * @param {Persona} persona
   * @returns {Observable<any>}
   */
  newPerson(persona: Persona) {
    const type = 'new';
    const data: any = {Type: type, Person: persona};
    const apiUri = `${this.url}`;
    return this.httpClient.post<Persona>(apiUri, data);
  }

}
