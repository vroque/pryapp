import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Employee } from '../employee/employee';
@Injectable()
export class LDRCollaboratorService {
  private url: string;  // a URL to web API
  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}claimsbook/Collaborator`;
  }
  searchCollaborators (names: string): Observable<Employee[]> {
    return this.http
      .get(`${this.url}?names=${names}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  listCollaboratorsOfFuntionalUnity (funtionalUnity: string): Observable<Employee[]> {
    return this.http
      .get(`${this.url}?funtional_unity=${funtionalUnity}&xxx=xxx`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
