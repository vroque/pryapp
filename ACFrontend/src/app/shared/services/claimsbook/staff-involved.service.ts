import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { StaffInvolved } from './staff-involved';
@Injectable()
export class LDRStaffInvolvedService {
  private url: string;  // a URL to web API
  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}claimsbook/StaffInvolved`;
  }

  getStaffInvolved(pageId: string) {
    return this.http
      .get(`${this.url}?page_id=${pageId}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getStaffInvolvedByDerivate(pageId: number, derivateId: number): Observable<StaffInvolved> {
    return this.http
      .get(`${this.url}?page_id=${pageId}&derivate_id=${derivateId}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  save (data: StaffInvolved): Observable<any> {
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
