export class SatisfactionSurvey {
  survey_id: number;
  page_id: number;
  survey_answer: number;
  survey_state: string;
  survey_date_create: Date;
  survey_date_resolved: Date;
}
