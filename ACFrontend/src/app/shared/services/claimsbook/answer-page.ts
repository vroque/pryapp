export class AnswerPage {
  answer_id: number;
  page_id: number;
  derivate_id: number;
  answer_person_creator_pidm: string;
  answer_person_validate_pidm: string;
  answer_note: string;
  answer_state: string;
  answer_date_created: Date;
  answer_date_validate: Date;
  answer_names: string;
}
