import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { PageClaimsBook } from './page-claims-book';

@Injectable()
export class LDRPageClaimsBookService {
  private url: string;  // a URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}claimsbook/PageClaimBook`;
  }

  listPagesClaims (type: string): Observable<PageClaimsBook[]> {
    return this.http
      .get(`${this.url}/?type=${type}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getPageClaims (id: string): Observable<PageClaimsBook> {
    return this.http
      .get(`${this.url}/${id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getPagesCampusNumerate(div: string, campus: string, numerate: number): Observable<PageClaimsBook[]> {
    return this.http
      .get(`${this.url}/?div=${div}&campus=${campus}&numerate=${numerate}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  save (data: PageClaimsBook): Observable<any> {
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  update(data: PageClaimsBook): Observable<any> {
    return this.http
      .put(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  updatePersonClaim(data: PageClaimsBook): Observable<any> {
    return this.http
      .patch(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  sendMailCreation(pageId: number): Observable<any> {
    return this.http
    .options(`${this.url}/?page_id=${pageId}`, this.config.getHeaders('json'))
    .map(this.config.jsonExtractData)
    .catch(this.config.handleError);
  }
}
