import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { UnityFuntional } from './unity-funtional';

@Injectable()
export class LDRUnityFuntionalService{
  private url: string;  // a URL to web API
  constructor(
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}claimsbook/UnityFuntional`;
  }
  listUnityFuntional (company: string): Observable<UnityFuntional[]> {
    return this.http
      .get(`${this.url}/?company=${company}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
