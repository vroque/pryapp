import { FilePageClaims } from './file-page-claims';
import { AnswerPage } from './answer-page';
export class DerivationArea {
  derivate_id: number;
  page_id: number;
  derivate_person_creator_pidm: string;
  derivate_funtional_unity: string;
  derivate_mail_recipient: string;
  derivate_type: string;
  derivate_state: string;
  derivate_note_disabled: string;
  derivate_date_created: Date;
  derivate_date_modify: Date;
  funtional_unity_name: string;
  staff_involved_pidm: string;
  staff_position_company: string;
  staff_involved_note: string;
  staff_position_description: string;
  staff_Involved_name: string;
  files_derivate : FilePageClaims[];
  files_answer : FilePageClaims[];
  action_derivate: string;
  answers: AnswerPage[];
}
