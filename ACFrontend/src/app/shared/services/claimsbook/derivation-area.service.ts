import { ConfigService } from '../../../config/config.service';
import { DerivationArea } from './derivation-area';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class LDRDerivationAreaService {
  private url: string;  // a URL to web API
  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}claimsbook/DerivationArea`;
  }
  listDerivationInvolvedArea (pageId: string, type: string): Observable<DerivationArea[]> {
    return this.http
      .get(`${this.url}?page_id=${pageId}&type=${type}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  listDerivationLegalArea (pageId: string, type: string): Observable<DerivationArea> {
    return this.http
      .get(`${this.url}?page_id=${pageId}&type=${type}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  listDerivationPageFuntionalUnity (pageId: string, funtionalUnity: string): Observable<DerivationArea[]> {
    return this.http
      .get(`${this.url}?page_id=${pageId}&funtional_unity=${funtionalUnity}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getDerivationArea(derivateId: number): Observable<DerivationArea> {
    return this.http
      .get(`${this.url}/${derivateId}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  save (data: DerivationArea): Observable<any> {
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  update (data: DerivationArea): Observable<any> {
    return this.http
      .put(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
