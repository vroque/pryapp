import { PageClaimsBook } from './page-claims-book';
export class LDRReport {
  /*========Datos para Obtener Reporte=====*/
  year: number;
  campus: string;
  origin: string;
  number_days: number;
  date_start: Date;
  date_end: Date;
  number_report: number;
  claims_area: PageClaimsBook[];
  /*========Reporte 1=====*/
  area_id: string;
  area_name: string;
  page_of_area: number;
  /*========Reporte 2=====*/
  campus_id: string;
  campus_name: string;
  page_of_campus: number;
  page_of_campus_digital: number;
  page_of_campus_physical: number;
  /*========Reporte 3=====*/
  page_id: number;
  page_campus: string;
  page_year: number;
  page_numeration: number;
  derivate_involved_area: any; // Dictionary;
  answer_involved_area: any; // Dictionary;
  days_legal_area: number;
  days_finalized: number;
  days_end: number;
  /*========Reporte 4=====*/
  date: Date;
  pages_create_date: number;
  pages_end_date: number;
  admitted_claims: PageClaimsBook[];
  claims_completed: PageClaimsBook[];

  /*========Reporte 5=====*/
  number_day: number;
  pages_number_day: number;
}
