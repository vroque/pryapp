import { ConfigService } from '../../../config/config.service';
import { FilePageClaims } from './file-page-claims';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class LDRFilePageClaimsService {
  private url: string;  // a URL to web API
  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}claimsbook/File`;
  }

  listFilesPage (pageId: string): Observable<FilePageClaims[]> {
    return this.http
      .get(`${this.url}/?page_id=${pageId}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  listFilesDerivation (pageId: string, derivationId: string): Observable<FilePageClaims[]> {
    return this.http
      .get(`${this.url}/?page_id=${pageId}&derivation_id=${derivationId}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  listFilesAnswerClaim (pageId: string, answerId: string): Observable<FilePageClaims[]> {
    return this.http
      .get(`${this.url}/?page_id=${pageId}&answer_id=${answerId}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  save (files: any, pageId: string, derivateId: string, answerId: string, action: string): Observable<any> {
    const formData = new FormData();
    if (files.length > 1) {
      let x = 0;
      for (const file of files) {
        formData.append(`data${x}`, file, file.name);
        x++;
      }
    } else {
      formData.append(`data1`, files, files.name);
    }
    formData.append('page_id', pageId);
    formData.append('derivate_id', derivateId);
    formData.append('answer_id', answerId);
    formData.append('action', action);
    return this.http
      .post(this.url, formData, this.config.getHeaders('multipart'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
