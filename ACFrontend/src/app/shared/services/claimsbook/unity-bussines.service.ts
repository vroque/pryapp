import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { UnityBussines } from './unity-bussines';

@Injectable()
export class LDRUnityBussinesService {
  private url: string;  // a URL to web API
  constructor(
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}claimsbook/UnityBussines`;
  }

  listUnityBussines (): Observable<UnityBussines[]> {
    return this.http
      .get(`${this.url}/`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
