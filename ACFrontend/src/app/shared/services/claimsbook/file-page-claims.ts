export class FilePageClaims {
  file_id: number;
  page_id: number;
  derivate_id: number;
  file_name: string;
  file_route: string;
  file_type: string;
  user_pidm: string;
  date_registry: Date;
  file_state: string;
}
