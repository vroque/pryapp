import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
@Injectable()
export class LDRSatisfactionSurveyService{
  private url: string;  // a URL to web API
  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}claimsbook/SatisfactionSurvey`;
  }
  saveSatisfactionSurvey (page_id: number): Observable<any> {
    return this.http
      .get(`${this.url}?page_id=${page_id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
