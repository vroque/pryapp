export class StaffInvolved {
  staff_involved_id: number;
  page_id: number;
  derivate_id: number;
  staff_involved_pidm: string;
  staff_position_company: string;
  staff_involved_note: string;
  staff_involved_type: string;
  staff_derivate_state: string;
  staff_date_created: Date;
  staff_date_modify: Date;
  staff_involved_name_position_company: string;
  staff_involved_user_id: string;
}
