import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { AnswerPage } from './answer-page';
@Injectable()
export class LDRAnswerPageService {
  private url: string;  // a URL to web API
  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}claimsbook/AnswerPage`;
  }

  getAnswerPage(page_id: string): Observable<AnswerPage> {
    return this.http
      .get(`${this.url}?page_id=${page_id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getAnswersByDerivate(page_id: number, derivate_id: number): Observable<AnswerPage[]> {
    return this.http
      .get(`${this.url}?page_id=${page_id}&derivate_id=${derivate_id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  getAnswer(answer_id: number): Observable<AnswerPage> {
    return this.http
      .get(`${this.url}/${answer_id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  saveAnswer (data: AnswerPage): Observable<any> {
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  updateAnswer (data: AnswerPage): Observable<any> {
    return this.http
      .put(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
