import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { LDRReport } from './report';
@Injectable()
export class LDRReportService {
  private url: string;  // a URL to web API
  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}claimsbook/Reports`;
  }

  getReport (dataReport: LDRReport ): Observable<LDRReport[]> {
    return this.http
      .post(this.url, dataReport, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
