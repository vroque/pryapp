import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class CategorizationService {
  private url: string;  // a URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}economic/categorization`;
  }

  categorize (id: number, data: any): Observable<any> {
    return this.http
      .put(`${this.url}/${id}`, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
