export class FunctionalUnit {
  unidad_funcional: string;
  compania: string;
  codigo_sucursal: string;
  centro_costo: string;
  nombre_unidad_funcional: string;
  unidad_funcional_superior: string;
  members: number;
}
