import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../../config/config.service';
import { FunctionalUnit } from './functional-unit';

@Injectable()
export class FunctionalUnitService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Adryan/FunctionalUnit`;
  }

  /**
   * Get functional unit info by id
   * @param functionalUnitId
   */
  getById(functionalUnitId: string): Observable<FunctionalUnit> {
    return this.http.get<FunctionalUnit>(`${this.apiUrl}/${functionalUnitId}`);
  }

  /**
   * Search functional unit
   * @param searchTerm
   */
  search(searchTerm: string): Observable<FunctionalUnit[]> {
    const type = 'name';
    const view = 'uc';
    const params = `s=${searchTerm}&type=${type}&view=${view}`;
    return this.http.get<FunctionalUnit[]>(`${this.apiUrl}?${params}`);
  }
}
