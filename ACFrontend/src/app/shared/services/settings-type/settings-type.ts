export class SettingsType {
  description: string;
  edit: boolean;
  id: number;
  name: string;
}
