import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from '../../../config/config.service';
import { Observable } from 'rxjs';
import { SettingsType } from './settings-type';

@Injectable()
export class SettingsTypeService {

  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Settings/SettingsType`;
  }

  /**
   * Settings type list
   * @returns {Observable<SettingsType[]>}
   */
  getSettingsTypeList(): Observable<SettingsType[]> {
    return this.http.get<SettingsType[]>(this.apiUrl);
  }

  /**
   * Save new settings type
   * @param {SettingsType} settingsType Settings type to save
   * @returns {Observable<SettingsType>} Settings type saved
   */
  save(settingsType: SettingsType): Observable<SettingsType> {
    return this.http.post<SettingsType>(this.apiUrl, settingsType);
  }

  /**
   * Update settings type
   * @param {SettingsType} settingsType Settings type to update
   * @returns {Observable<SettingsType>} Settings type updated
   */
  update(settingsType: SettingsType): Observable<SettingsType> {
    return this.http.put<SettingsType>(this.apiUrl, settingsType);
  }

  /**
   * Delete settings type
   * @param {SettingsType} settingsType Settings type to delete
   * @returns {Observable<boolean>}
   */
  delete(settingsType: SettingsType): Observable<boolean> {
    const api = `${this.apiUrl}/${settingsType.id}`;
    return this.http.delete<boolean>(api);
  }

}
