import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BenefitStudent } from './benefit-student';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class BenefitStudentService {
  private url: string;
  constructor(private http: HttpClient, private config: ConfigService) {
    this.url = this.url = `${config.getApiURI()}benefit/benefitstudent`;
  }

  getBenefitStudent(data: string): Observable<BenefitStudent[]> {
    const uriApi = `${this.url}/${data}`;
    return this.http.get<BenefitStudent[]>(uriApi);
  }

  /**
   * Elimina el beneficio seleccionado
   * @param data modelo de beneficio
   */
  delete(data: BenefitStudent): Observable<Boolean> {
    return this.http.put<Boolean>(this.url, data);
  }
}
