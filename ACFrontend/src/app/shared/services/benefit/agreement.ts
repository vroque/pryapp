export class Agreement {
  divs: string;
  id: string;
  cod_contract: string;
  company_name: string;
  start_date: Date;
  end_date: Date;
  grad_fam: string;
  renovation_auto: boolean;
  disapproved: boolean;
  create_date: Date;
  id_user: string;
  active: number;
}
