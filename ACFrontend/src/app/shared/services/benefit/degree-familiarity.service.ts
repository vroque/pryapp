import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { DegreeFamiliarity } from './degree-familiarity';

@Injectable()
export class DegreeFamiliarityService {
  private url: string;
  constructor(
    private http: Http,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}benefit/degreefamiliarity`;
  }
  listDegFamiliarity(): Observable<DegreeFamiliarity[]> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
