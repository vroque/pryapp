import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Agreement } from './agreement';
import { CreateAgreement } from './create-agreement';

@Injectable()
export class AgreementService {
  private url: string;  // a URL to web API
  constructor(
    private http: Http,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}benefit/agreement`;
  }

  getAgreement(data: string): Observable<Agreement> {
    const uriApi = `${this.url}/${data}`;
    return this.http
        .get(uriApi, this.config.getHeaders('json'))
        .map(this.config.jsonExtractData)
        .catch(this.config.handleError);
  }

  list(): Observable<Agreement[]> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  updateAgreement(data: Agreement): Observable<Agreement> {
    return this.http
      .put(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
  create(data: CreateAgreement): Observable<any> {
    return this.http
        .post(this.url, data, this.config.getHeaders('json'))
        .map(this.config.jsonExtractData)
        .catch(this.config.handleError);
  }
}
