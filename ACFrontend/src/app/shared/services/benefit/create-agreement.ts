export class CreateAgreement {
  cod_contract: string;
  company_name: string;
  start_date: Date;
  end_date: Date;
  grad_fam: string;
  disapproved: boolean;
  renovation_auto: boolean;
  modalityADM: string;
  percent_enrollmentADM: number;
  percent_cuotADM: number;
  modalityADG: string;
  percent_enrollmentADG: number;
  percent_cuotADG: number;
  modalityADV: string;
  percent_enrollmentADV: number;
  percent_cuotADV: number;
}
