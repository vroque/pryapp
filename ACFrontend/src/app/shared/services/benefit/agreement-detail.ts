export class AgreementDetail {
  modality: string;
  percent_enrollment: number;
  percent_cuot: number;
}
