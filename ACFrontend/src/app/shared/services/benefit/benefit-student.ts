export class BenefitStudent {
  campus: string;
  term: string;
  modality: string;
  id: string;
  idBenefit: string;
  benefit: string;
  observation: string;
  user: string;
  date: Date;
  active: number;
}
