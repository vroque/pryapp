export class Discount {
  divs: string;
  discount_type: string;
  discount_id: string;
  discount_name: string;
  description: string;
  desc_enrollment: number;
  desc_cout: number;
  scale: Boolean;
  beca: Boolean;
  half_beca: Boolean;
  third_beca: Boolean;
  id_personal: string;
  active: Boolean;
}
