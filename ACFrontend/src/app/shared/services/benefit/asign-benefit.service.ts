import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { AsignBenefit } from './asign-benefit';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class AsignBenefitService {
  private url: string;
  constructor(private http: HttpClient, private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}benefit/asignbenefit`;
  }
  create(data: AsignBenefit): Observable<any> {
    return this.http.post<any>(this.url, data);
  }
}
