import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { AgreementDetail } from './agreement-detail';

@Injectable()
export class AgreementDetailService {

  private url: string;
  constructor(
    private http: Http,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}benefit/agreementdetail`;
  }

  getDetail(data: string): Observable<AgreementDetail> {
    const uriApi = `${this.url}/${data}`;
    return this.http
      .get(uriApi, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  updateDetail(data: AgreementDetail): Observable<AgreementDetail> {
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
