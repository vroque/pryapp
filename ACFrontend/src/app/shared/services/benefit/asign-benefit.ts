export class AsignBenefit {
  campus: string;
  beca: string;
  discount_id: string;
  modality: string;
  observation: string;
  program: string;
  scale: string;
  schedule: string;
  student_id: string;
  term: string;
  user: string;
}
