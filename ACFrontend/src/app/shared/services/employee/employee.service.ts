import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';
import { Employee } from './employee';

@Injectable()
export class EmployeeService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Adryan/Employee`;
  }

  /**
   * Get employee profile
   * @param pidm
   */
  getByPidm(pidm: number): Observable<Employee> {
    return this.http.get<Employee>(`${this.apiUrl}/${pidm}`);
  }

  /**
   * Get employee profiles
   */
  getAllProfiles(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.apiUrl);
  }

  /**
   * Search employee
   * @param searchTerm
   */
  search(searchTerm: string): Observable<Employee[]> {
    const type = 'userId';
    const companyId = 'uc';
    status = 'active';
    const params = `s=${searchTerm}&type=${type}&view=${companyId}&status=${status}`;
    return this.http.get<Employee[]>(`${this.apiUrl}?${params}`);
  }
}
