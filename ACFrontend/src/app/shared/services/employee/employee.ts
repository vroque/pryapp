export class Employee {
  codigo_unico: string;
  compania: string;
  numero_documento: string;
  apellido_paterno: string;
  apellido_materno: string;
  nombre: string;
  iddependencia: string;
  nombre_unidad_funcional: string;
  descripcion_ubicacion_fisica: string;
  unidad_funcional_organica: string;
  puesto_organica: string;
  isActive: boolean;
  employee_pidm: number;
  employee_dni: string;
  employee_ce: string;
  employee_position_company_description: string;
  employee_user_id: string;
}
