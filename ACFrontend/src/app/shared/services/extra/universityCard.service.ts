import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

@Injectable()
export class TCUUniversityCardService {
  private readonly apiUrl: string;

  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService,
  ) {
    this.apiUrl = `${configService.getApiURI()}Extra/UniversityCard`;
  }

  /**
   * Obtener datos de carnet
   */
  get(): Observable<any> {
    return this.httpClient.get<any>(this.apiUrl);
  }
}
