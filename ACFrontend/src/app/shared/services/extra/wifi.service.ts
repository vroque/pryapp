import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { Wifi } from './wifi';

@Injectable()
export class WifiService {
  private readonly apiUrl: string;

  constructor(
    private http: HttpClient,
    private config: ConfigService,
  ) {
    this.apiUrl = `${config.getApiURI()}Extra/Wifi`;
  }

  get(studentPidm: number): Observable<Wifi> {
    const api = `${this.apiUrl}/${studentPidm}`;
    return this.http.get<Wifi>(api);
  }
}
