export class OPPPracticeDocument {
  IDDependencia: string;
  IDSede: string;
  IDPracticaDocumento: number;
  IDPractica: string;
  FechaCreacion: Date;
  IDPersonaRegistro: string;
  IDAlumno: string;
  pidm: number;
}
