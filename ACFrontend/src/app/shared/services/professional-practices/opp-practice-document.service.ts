import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';
import { OPPPracticeDocument } from './opp-practice-document';

@Injectable()
export class OPPPracticeDocumentService {
  private readonly apiUrl: string;

  constructor (private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${ config.getApiURI()}ProfessionalPractices/PracticeDocument`;
  }

  /**
   * Información sobre prácticas profesionales
   * @param pidm PIDM del estudiante
   * @param idescuela Código de escuela académica
   */
  get (pidm: number, idescuela: string): Observable<OPPPracticeDocument[]> {
    return this.http.get<OPPPracticeDocument[]>(`${this.apiUrl}?pidm=${pidm}&idescuela=${idescuela}`);
  }
}
