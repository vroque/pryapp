import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../../config/config.service';
import { IncidentType } from './incident-type';

@Injectable()
export class IncidentTypeService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Incident/Type`;
  }

  /**
   * Get incident type info by id
   * @param id Incident type id
   */
  getById(id: number): Observable<IncidentType> {
    return this.http.get<IncidentType>(`${this.apiUrl}/${id}`);
  }

  /**
   * Get all incident types
   */
  getAllIncidentType(): Observable<IncidentType[]> {
    return this.http.get<IncidentType[]>(this.apiUrl);
  }

  /**
   * Register a new incident type
   * @param incidentType
   */
  save(incidentType: IncidentType): Observable<IncidentType> {
    return this.http.post<IncidentType>(this.apiUrl, incidentType);
  }

  /**
   * Update an incident type
   * @param incidentType
   */
  update(incidentType: IncidentType): Observable<IncidentType> {
    return this.http.put<IncidentType>(this.apiUrl, incidentType);
  }
}
