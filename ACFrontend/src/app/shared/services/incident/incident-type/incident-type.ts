export class IncidentType {
  static readonly INCIDENT = 1;

  id: number;
  name: string;
  description: string;
}
