export class Incident {
  id: number;
  authorPidm: number;
  reporterPidm: number;
  reporterName: string;
  title: string;
  description: string;
  publicationDate: Date;
  categoryId: number;
  subCategoryId: number;
  typeId: number;
  statusId: number;
  assignedToPidm: number;
  assignedToTeamId: string;
  requestSource: number;
}
