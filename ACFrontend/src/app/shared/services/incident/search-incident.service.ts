import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';
import { Incident } from './incident';

@Injectable()
export class SearchIncidentService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Incident/Search`;
  }

  /**
   * Get all incident reported by a student
   */
  getByStudent(): Observable<Incident[]> {
    return this.http.get<Incident[]>(this.apiUrl);
  }

  /**
   * Search incidents
   * @param reporter
   * @param author
   * @param category
   * @param type
   * @param status
   * @param owner
   * @param team
   * @param diff
   */
  find(reporter: number, author: number = 0, category: number = 0, type: number = 0,
       status: number = 0, owner: number = 0, team: string = 'all', diff: number = 0): Observable<Incident[]> {
    // tslint:disable-next-line:max-line-length
    const params = `reporter=${reporter}&author=${author}&category=${category}&type=${type}&status=${status}&owner=${owner}&team=${team}&diff=${diff}`;
    return this.http.get<Incident[]>(`${this.apiUrl}?${params}`);
  }
}
