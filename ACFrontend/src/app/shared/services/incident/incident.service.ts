import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../config/config.service';
import { Incident } from './incident';

@Injectable()
export class IncidentService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Incident/Incident`;
  }

  /**
   * Get incident info by id
   * @param incidentId
   */
  getById(incidentId: number): Observable<Incident> {
    return this.http.get<Incident>(`${this.apiUrl}/${incidentId}`);
  }

  /**
   * save incident
   * @param incident
   */
  save(incident: Incident): Observable<Incident> {
    return this.http.post<Incident>(this.apiUrl, incident);
  }

  /**
   * update incident
   * @param incident
   */
  update(incident: Incident): Observable<Incident> {
    return this.http.put<Incident>(this.apiUrl, incident);
  }
}
