import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../../config/config.service';
import { IncidentLog } from './incident-log';

@Injectable()
export class IncidentLogService {
  private readonly apiUrl: string;
  private readonly incidentApiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Incident/IncidentLog`;
    this.incidentApiUrl = `${this.config.getApiURI()}Incident/Incident`;
  }

  /**
   * Get incident log info by id
   * @param incidentLogId
   */
  getById(incidentLogId: number): Observable<IncidentLog> {
    return this.http.get<IncidentLog>(`${this.apiUrl}/${incidentLogId}`);
  }

  /**
   * Get all incident logs by incident id
   * @param incidentId
   */
  getAllIncidentLogByIncidentId(incidentId: number): Observable<IncidentLog[]> {
    return this.http.get<IncidentLog[]>(`${this.incidentApiUrl}/${incidentId}?view=log`);
  }

  /**
   * Register a new incident log
   * @param incidentLog
   */
  save(incidentLog: IncidentLog): Observable<IncidentLog> {
    return this.http.post<IncidentLog>(this.apiUrl, incidentLog);
  }

  /**
   * Update an incident log
   * @param incidentLog
   */
  update(incidentLog: IncidentLog): Observable<IncidentLog> {
    return this.http.put<IncidentLog>(this.apiUrl, incidentLog);
  }
}
