export class IncidentLog {
  static readonly CREATE = 1;
  static readonly ASSIGN_DERIVE = 2;
  static readonly CATEGORIZE = 3;
  static readonly TASK = 4;
  static readonly COMMENT = 5;
  static readonly SOLUTION = 6;
  static readonly CLOSE = 7;

  static readonly ACTIVITY_TYPE: {[key: string]: number} = {
    'Comentario': IncidentLog.COMMENT,
    'Tarea': IncidentLog.TASK,
    'Solución': IncidentLog.SOLUTION
  };

  id: number;
  incidentId: number;
  logTypeId: number;
  authorPidm: number;
  authorName: string;
  teamId: string;
  teamName: string;
  comment: string;
  publicationDate: Date;
  isPublic: boolean;
}
