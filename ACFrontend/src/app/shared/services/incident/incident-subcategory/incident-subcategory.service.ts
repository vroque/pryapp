import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../../config/config.service';
import { IncidentSubcategory } from './incident-subcategory';

@Injectable()
export class IncidentSubcategoryService {
  private readonly apiUrl: string;
  private readonly categoryApiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Incident/SubCategory`;
    this.categoryApiUrl = `${this.config.getApiURI()}Incident/Category`;
  }

  /**
   * Get incident subcategory info by id
   * @param id
   */
  getById(id: number): Observable<IncidentSubcategory> {
    return this.http.get<IncidentSubcategory>(`${this.apiUrl}/${id}`);
  }

  /**
   * Get all incident subcategories
   */
  getAllIncidentSubcategories(): Observable<IncidentSubcategory[]> {
    return this.http.get<IncidentSubcategory[]>(this.apiUrl);
  }

  /**
   * Get all incident subcategories by category id
   * @param categoryId
   */
  getAllIncidentSubcategoriesByCategoryId(categoryId: number): Observable<IncidentSubcategory[]> {
    return this.http.get<IncidentSubcategory[]>(`${this.categoryApiUrl}/${categoryId}?view=sub`);
  }

  /**
   * Register a new incident subcategory
   * @param incidentSubcategory
   */
  save(incidentSubcategory: IncidentSubcategory): Observable<IncidentSubcategory> {
    return this.http.post<IncidentSubcategory>(this.apiUrl, incidentSubcategory);
  }

  /**
   * Update a incident subcategory
   * @param incidentSubcategory
   */
  update(incidentSubcategory: IncidentSubcategory): Observable<IncidentSubcategory> {
    return this.http.put<IncidentSubcategory>(this.apiUrl, incidentSubcategory);
  }
}
