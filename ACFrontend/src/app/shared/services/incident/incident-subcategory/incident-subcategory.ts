export class IncidentSubcategory {
  id: number;
  parentId: number;
  name: string;
  description: string;
}
