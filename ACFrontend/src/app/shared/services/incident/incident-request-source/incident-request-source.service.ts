import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from '../../../../config/config.service';
import { Observable } from 'rxjs';
import { IncidentRequestSource } from './incident-request-source';

@Injectable()
export class IncidentRequestSourceService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Incident/IncidentRequestSource`;
  }

  /**
   * Get incident request source by id
   * @param id
   */
  getById(id: number): Observable<IncidentRequestSource> {
    return this.http.get<IncidentRequestSource>(`${this.apiUrl}/${id}`);
  }

  /**
   * Get all incident request source
   */
  getAll(): Observable<IncidentRequestSource[]> {
    return this.http.get<IncidentRequestSource[]>(this.apiUrl);
  }

  /**
   * Register a new incident request source
   * @param incidentRequestSource
   */
  save(incidentRequestSource: IncidentRequestSource): Observable<IncidentRequestSource> {
    return this.http.post<IncidentRequestSource>(this.apiUrl, incidentRequestSource);
  }

  /**
   * Update an incident request source
   * @param incidentRequestSource
   */
  update(incidentRequestSource: IncidentRequestSource): Observable<IncidentRequestSource> {
    return this.http.put<IncidentRequestSource>(this.apiUrl, incidentRequestSource);
  }
}
