export class IncidentRequestSource {
  static readonly DIRECT_COMMUNICATION = 1;
  static readonly EMAIL = 2;
  static readonly SERVICE_DESK = 3;
  static readonly WEB = 4;
  static readonly PHONE = 5;

  id: number;
  name: string;
  description: string;
}
