export class IncidentSubcategoryFunctionalUnit {
  id: number;
  subcategoryId: number;
  subcategoryName: string;
  functionalUnitId: string;
  functionalUnitName: string;
}
