import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../../config/config.service';
import { IncidentSubcategoryFunctionalUnit } from './incident-subcategory-functional-unit';

@Injectable()
export class IncidentSubcategoryFunctionalUnitService {
  private readonly apiUrl: string;
  private readonly subcategoryApiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Incident/SubcategoryFunctionalUnit`;
    this.subcategoryApiUrl = `${this.config.getApiURI()}Incident/SubCategory`;
  }

  /**
   * Get incident subcategory - functional unit
   * @param id
   */
  getById(id: number): Observable<IncidentSubcategoryFunctionalUnit> {
    return this.http.get<IncidentSubcategoryFunctionalUnit>(`${this.apiUrl}/${id}`);
  }

  /**
   * Get all incident subcategory - functional unit by subcategory id
   */
  getAllBySubcategoryId(subcategoryId: number): Observable<IncidentSubcategoryFunctionalUnit[]> {
    return this.http.get<IncidentSubcategoryFunctionalUnit[]>(`${this.subcategoryApiUrl}/${subcategoryId}?view=team`);
  }

  /**
   * Register a new incident subcategory - functional unit
   * @param incidentSubcategoryFunctionalUnit
   */
  save(incidentSubcategoryFunctionalUnit: IncidentSubcategoryFunctionalUnit): Observable<IncidentSubcategoryFunctionalUnit> {
    return this.http.post<IncidentSubcategoryFunctionalUnit>(this.apiUrl, incidentSubcategoryFunctionalUnit);
  }

  /**
   * Update an incident subcategory - functional unit
   * @param incidentSubcategoryFunctionalUnit
   */
  update(incidentSubcategoryFunctionalUnit: IncidentSubcategoryFunctionalUnit): Observable<IncidentSubcategoryFunctionalUnit> {
    return this.http.put<IncidentSubcategoryFunctionalUnit>(this.apiUrl, incidentSubcategoryFunctionalUnit);
  }

  /**
   * Delete an incident subcategory - functional unit relationship
   * @param id
   */
  delete(id: number): Observable<boolean> {
    return this.http.delete<boolean>(`${this.apiUrl}/${id}`);
  }
}
