import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../../config/config.service';
import { IncidentStatus } from './incident-status';

@Injectable()
export class IncidentStatusService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Incident/Status`;
  }

  /**
   * Get incident status info by id
   * @param id Incident status id
   */
  getById(id: number): Observable<IncidentStatus> {
    return this.http.get<IncidentStatus>(`${this.apiUrl}/${id}`);
  }

  /**
   * Get all incident statuses
   */
  getAllIncidentStatus(): Observable<IncidentStatus[]> {
    return this.http.get<IncidentStatus[]>(this.apiUrl);
  }

  /**
   * Register new incident status
   * @param incidentStatus
   */
  save(incidentStatus: IncidentStatus): Observable<IncidentStatus> {
    return this.http.post<IncidentStatus>(this.apiUrl, incidentStatus);
  }

  /**
   * Update an incident status
   * @param incidentStatus
   */
  update(incidentStatus: IncidentStatus): Observable<IncidentStatus> {
    return this.http.put<IncidentStatus>(this.apiUrl, incidentStatus);
  }
}
