export class IncidentStatus {
  static readonly OPEN = 1;
  static readonly IN_PROGRESS = 2;
  static readonly SOLVED = 3;
  static readonly REJECTED = 4;
  static readonly CANCELLED = 5;
  static readonly CLOSED = 6;

  id: number;
  name: string;
  nameEs: string;
  description: string;
}
