import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../../config/config.service';
import { IncidentCategory } from './incident-category';

@Injectable()
export class IncidentCategoryService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Incident/Category`;
  }

  /**
   * Get incident category info by id
   * @param id
   */
  getById(id: number): Observable<IncidentCategory> {
    return this.http.get<IncidentCategory>(`${this.apiUrl}/${id}`);
  }

  /**
   * Get all incident categories
   */
  getAllIncidentCategory(): Observable<IncidentCategory[]> {
    return this.http.get<IncidentCategory[]>(this.apiUrl);
  }

  /**
   * Get principal categories. These categories doesn't have parent category.
   */
  getParentCategories(): Observable<IncidentCategory[]> {
    return this.http.get<IncidentCategory[]>(`${this.apiUrl}?type=parents`);
  }

  /**
   * Get secondary categories. These categories have parent category.
   */
  getChildrenCategories(): Observable<IncidentCategory[]> {
    return this.http.get<IncidentCategory[]>(`${this.apiUrl}?type=children`);
  }

  /**
   * Get children categories by category id
   * @param categoryId
   */
  getChildrenCategoryByCategoryId(categoryId: number): Observable<IncidentCategory[]> {
    return this.http.get<IncidentCategory[]>(`${this.apiUrl}/${categoryId}?view=children`);
  }

  /**
   * Register new incident category
   * @param incidentCategory
   */
  save(incidentCategory: IncidentCategory): Observable<IncidentCategory> {
    return this.http.post<IncidentCategory>(this.apiUrl, incidentCategory);
  }

  /**
   * Update an incident category
   * @param incidentCategory
   */
  update(incidentCategory: IncidentCategory): Observable<IncidentCategory> {
    return this.http.put<IncidentCategory>(this.apiUrl, incidentCategory);
  }
}
