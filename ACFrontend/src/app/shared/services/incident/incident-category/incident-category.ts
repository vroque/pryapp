export class IncidentCategory {
  id: number;
  parentId: number;
  name: string;
  description: string;
}
