export class IncidentLogType {
  id: number;
  name: string;
  description: string;
}
