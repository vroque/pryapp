import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from '../../../../config/config.service';
import { IncidentLogType } from './incident-log-type';

@Injectable()
export class IncidentLogTypeService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUrl = `${this.config.getApiURI()}Incident/LogType`;
  }

  /**
   * Get incident log type info by id
   * @param id
   */
  getById(id: number): Observable<IncidentLogType> {
    return this.http.get<IncidentLogType>(`${this.apiUrl}/${id}`);
  }

  /**
   * Get all incident log types
   */
  getAllIncidentLogType(): Observable<IncidentLogType[]> {
    return this.http.get<IncidentLogType[]>(this.apiUrl);
  }

  /**
   * Register a new incident log type
   * @param incidentLogType
   */
  save(incidentLogType: IncidentLogType): Observable<IncidentLogType> {
    return this.http.post<IncidentLogType>(this.apiUrl, incidentLogType);
  }

  /**
   * Update an incident log type
   * @param incidentLogType
   */
  update(incidentLogType: IncidentLogType): Observable<IncidentLogType> {
    return this.http.put<IncidentLogType>(this.apiUrl, incidentLogType);
  }
}
