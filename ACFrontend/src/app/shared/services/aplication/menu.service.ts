import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Menu } from './menu';

@Injectable()
export class MenuService {
  private url: string;  // a URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}aplication/menu`;
  }

  /**
   * Lista de  menus por el id del modulo
   * @return Array<Menu> lista de menus
   */
  query (id: number): Observable<Menu[]> {
    const queryParams = this.config.urlFormat({module: id});
    return this.http
      .get(`${this.url}?${queryParams}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * create module
   */
  create (menu: Menu): Observable<Menu> {
    return this.http
      .post(this.url, menu, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  update (menu: Menu): Observable<Menu> {
    return this.http
      .put(`${this.url}/${menu.id}`, menu, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  delete (id: number): Observable<any> {
    return this.http
      .delete(`${this.url}/${id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
