import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Users } from '../aplication/user';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UsersService {
  private url: string;  // URL to web API
  private urlExtend: string;  // URL to web API2
  private urlExtendFilter: string;  // URL to web API2

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}aplication/user`;
    this.urlExtend = `${config.getApiURI()}aplication/UserFilter`;
    this.urlExtendFilter = `${config.getApiURI()}aplication/groupUserFilter`;
  }

  query(id: string ): Observable<Users[]> {
    return this.http
      .get(`${this.urlExtendFilter}/${id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  find(id: string ): Observable<Users[]> {

    return this.http
      .get(`${this.urlExtend}?id=${id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  query_all(): Observable<Users[]> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
