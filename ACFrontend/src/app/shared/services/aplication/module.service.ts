import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Module } from './module';

@Injectable()
export class ModuleService {
  private url: string;  // a URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}aplication/module`;
  }

  /**
   * List Backoffice modules
   * @return Array<Module> list of backoffice modules
   */
  list (): Observable<Module[]> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * Bad call of listBackoffice function FIXME
   */
  query (id: string): Observable<Module[]> {
    return this.http
      .get(`${this.url}/${id}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * create module
   */
  add (data: any): Observable<Module[]> {
    const body = this.config.urlFormat(data);
    return this.http
      .post(`${this.url}/id?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
