import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { Groups } from '../aplication/group';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class GroupsService {
  private url: string;  // URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}aplication/group`;
  }

  query (data: any ): Observable<Groups[]> {
    const body = this.config.urlFormat(data);
    return this.http
      .get(`${this.url}/id?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  query_all(): Observable<Groups[]> {
    return this.http
      .get(this.url, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
