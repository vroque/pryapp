import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { GroupModules } from '../aplication/groupModule';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class GroupMolduleService {
  private url: string;  // URL to web API
  private url_add: string;  // URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}aplication/groupModule`;
    this.url_add = `${config.getApiURI()}aplication/addGroupModule`;
  }

  add(data: any): Observable<GroupModules[]> {
    const body = this.config.urlFormat(data);
    return this.http
      .post(`${this.url}/id?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  addGroup(data: any): Observable<GroupModules[]> {
    const body = this.config.urlFormat(data);
    return this.http
      .post(`${this.url_add}/id?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  removeGroupModule(data: any): Observable<GroupModules[]> {
    const body = this.config.urlFormat(data);
    return this.http
      .delete(`${this.url}/id?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
