import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { GroupUsers } from '../aplication/groupUser';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class GroupsUserService {
  private url: string;  // URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}aplication/groupUser`;
  }

  remove(data: any): Observable<GroupUsers[]> {
    const body = this.config.urlFormat(data);
    return this.http
      .delete(`${this.url}/id?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  add(data: any): Observable<GroupUsers[]> {
    const body = this.config.urlFormat(data);
    return this.http
      .post(`${this.url}/id?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
