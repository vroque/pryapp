export class Module {
  id: number;
  app_id: string;
  name: string;
  description: string;
  update_date: string;
  abbr: string;
  fieldSearch: any;
}
