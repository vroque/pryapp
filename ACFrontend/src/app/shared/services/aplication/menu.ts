export class Menu {
  id: number;
  icon: string;
  uri: string;
  name: string;
  parent: number;
  module: number;
  is_active: boolean;
  position: string;
  smenus: Menu[];
  cardStyle: string;
  cardIcoStyle: string;
  order: number;
  description: string;
  externalLink: boolean;
}
