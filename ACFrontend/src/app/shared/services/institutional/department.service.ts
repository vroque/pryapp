import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { Department } from './department';

@Injectable()
export class DepartmentService {
  private apiUri: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.apiUri = `${config.getApiURI()}institutional/Departaments`;
  }

  getDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(`${this.apiUri}`);
  }
}
