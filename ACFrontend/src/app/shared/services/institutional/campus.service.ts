import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Campus } from './campus';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class CampusService {
  private url: string;  // a URL to web API

  constructor (private http: HttpClient,private config: ConfigService) {
    this.url = `${config.getApiURI()}institutional/campus`;
  }

  /**
   * Obtiene la lista de campus de la universidad
   */
  query (): Observable<Campus[]> {
    return this.http.get<Campus[]>(this.url);
  }
}
