export class Campus {
  id: string;
  name: string;
  code: string;
}
