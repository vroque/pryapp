import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { TermsAndConditions } from './terms-and-conditions';

@Injectable()
export class TermsAndConditionsService {
  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Settings/TermsAndConditions`;
  }

  /**
   * Terms and conditions list
   * @returns {Observable<TermsAndConditions[]>} Terms and conditions list
   */
  getTermsList(): Observable<TermsAndConditions[]> {
    return this.http.get<TermsAndConditions[]>(this.apiUrl);
  }

  /**
   * Terms and conditions for modules
   */
  getModuleTerms(): Observable<TermsAndConditions[]> {
    const api = `${this.apiUrl}/type/module`;
    return this.http.get<TermsAndConditions[]>(api);
  }

  /**
   * Terms and conditions by Id
   * @param id Terms and conditions id
   */
  getTermsById(id: number): Observable<TermsAndConditions> {
    const api = `${this.apiUrl}/${id}`;
    return this.http.get<TermsAndConditions>(api);
  }

  /**
   * Save new Terms and conditions
   * @param {TermsAndConditions} termsAndConditions Terms and conditions to save
   * @returns {Observable<TermsAndConditions>} Terms and conditions saved
   */
  save(termsAndConditions: TermsAndConditions): Observable<TermsAndConditions> {
    return this.http.post<TermsAndConditions>(this.apiUrl, termsAndConditions);
  }

  /**
   * Update Terms and conditions
   * @param {TermsAndConditions} termsAndConditions Terms and conditions to update
   * @returns {Observable<TermsAndConditions>} Terms and conditions updated
   */
  update(termsAndConditions: TermsAndConditions): Observable<TermsAndConditions> {
    return this.http.put<TermsAndConditions>(this.apiUrl, termsAndConditions);
  }

  /**
   * Delete Terms and conditions
   * @param {TermsAndConditions} termsAndConditions Terms and conditions to delete
   * @returns {Observable<boolean>}
   */
  delete(termsAndConditions: TermsAndConditions): Observable<boolean> {
    const api = `${this.apiUrl}/${termsAndConditions.id}`;
    return this.http.delete<boolean>(api);
  }
}
