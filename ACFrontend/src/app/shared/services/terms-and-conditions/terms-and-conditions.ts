export class TermsAndConditions {
  id: number;
  type: number;
  name: string;
  isFile: boolean;
  detail: string;
  url: string;
  publicationDate: Date;
  active: boolean;
}
