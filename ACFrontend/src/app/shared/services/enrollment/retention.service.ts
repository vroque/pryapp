import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Retention } from './retention';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class RetentionService {

  private url: string;

  constructor(
    private httpClient: HttpClient,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}enrollment/Retention`;
  }

  /**
	 * Obtiene las retenciones del estudiante
	 * @param pidm
	 */
  getRetencionByPidm(pidm: number): Observable<Retention[]> {
    return this.httpClient.get<Retention[]>(`${this.url}?pidm=${pidm}`);
  }
}
