import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { HttpClient } from '@angular/common/http';
import { Mat123TermEnrollment } from './term-enrollment';

@Injectable()
export class Mat123TermEnrollmentService {

  private url: string;

  constructor(
    private httpClient: HttpClient,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}enrollment/TermEnrollmentStudent`;
  }

  /**
	 * Obtiene el estado del estudiante en un periodo solicitado
	 * @param pidm
	 */
  getStatusStudent(pidm: number): Observable<Mat123TermEnrollment> {
    return this.httpClient.get<Mat123TermEnrollment>(`${this.url}?pidm=${pidm}`);
  }
}
