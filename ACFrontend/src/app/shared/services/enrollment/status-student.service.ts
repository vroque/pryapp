import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { StatusStudent } from './status-student';
import { ConfigService } from '../../../config/config.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class StatusStudentService {

  private url: string;

  constructor(
    private httpClient: HttpClient,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}enrollment/StatusStudent`;
  }

  /**
	 * Obtiene el estado del estudiante en un periodo solicitado
	 * @param pidm
	 */
  getStatusStudent(pidm: number): Observable<StatusStudent> {
    return this.httpClient.get<StatusStudent>(`${this.url}?pidm=${pidm}`);
  }
}
