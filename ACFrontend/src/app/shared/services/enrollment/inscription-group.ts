export class InscriptionGroup {
  pidm: number;
  term: string;
  code: string;
  priority: string;
  beginDate: Date;
  endDate: Date;
  enrollment: boolean;
}
