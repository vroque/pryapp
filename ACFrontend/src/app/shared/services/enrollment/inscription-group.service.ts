import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { InscriptionGroup } from './inscription-group';
import { ConfigService } from '../../../config/config.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class InscriptionGroupService {

  private url: string;

  constructor(
    private httpClient: HttpClient,
    private config: ConfigService,
  ) {
    this.url = `${config.getApiURI()}enrollment/InscriptionGroup`;
  }

  /**
	 * Obtiene el grupo de inscripción de un estudiante
	 * @param pidm
	 */
  getInscriptionGroup(pidm: number, term: string): Observable<InscriptionGroup> {
    return this.httpClient.get<InscriptionGroup>(`${this.url}?pidm=${pidm}&term=${term}`);
  }
}
