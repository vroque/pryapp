export class Retention {
  description: string;
  fromDate: Date;
  hlddCode: string;
  pidm: number;
  reason: string;
  surrogateId: number;
  toDate: Date;
}
