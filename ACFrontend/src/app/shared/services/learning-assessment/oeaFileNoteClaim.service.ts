import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class OEAFileNoteClaimService {
  readonly url: string;  // a URL to web API

  constructor(private httpClient: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}learningassessment/FileNoteClaim`;
  }

  SaveAndSubmitFile(noteClaimId: number, reportNoteClaimId: number, file: any): Observable<boolean> {
    const formData = new FormData();
    formData.append('noteClaimId', noteClaimId.toString());
    formData.append('reportNoteClaimId', (reportNoteClaimId == null ? null : reportNoteClaimId.toString()));
    formData.append(`file`, file, file.name);
    return this.httpClient.post<boolean>(this.url, formData);
  }
}
