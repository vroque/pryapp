import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { OEAReport } from './report';
import { OEANoteClaim } from './OEANoteClaim';
@Injectable()
export class OEAReportService {
  readonly url: string;  // a URL to web API
  constructor (private httpClient: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}learningassessment/reports`;
  }

  /**
   * Obtener periodos
   */
  getTerms(): Observable<string[]> {
    return this.httpClient.get<string[]>(this.url);
  }

  /**
   * Obtener reporte sde sustitutorios programados
   * @param dataReport entradas del reporte
   */
  getReportSusbstituteProgrammed (dataReport: OEAReport ): Observable<OEAReport[]> {
    dataReport.numberReport = 1;
    return this.httpClient.post<OEAReport[]>(this.url, dataReport);
  }

  /**
   * Obtener reporte de NRcs programados para sustitutorio
   * @param dataReport entradas del reporte
   */
  getReportOfNrcsProgrammed (dataReport: OEAReport ): Observable<OEAReport[]> {
    dataReport.numberReport = 2;
    return this.httpClient.post<OEAReport[]>(this.url, dataReport);
  }

  /**
   * Obtener reclamos de notas registrados
   * @param dataReport entradas del reporte
   */
  getNoteClaims (dataReport: OEAReport ): Observable<OEANoteClaim[]> {
    dataReport.numberReport = 3;
    return this.httpClient.post<OEANoteClaim[]>(this.url, dataReport);
  }

  /**
   * Enviar email a los docentes
   * @param dataReport Entradas del reporte
   */
  sendMailNoteClaim (dataReport: OEAReport ): Observable<boolean> {
    dataReport.numberReport = 4;
    return this.httpClient.post<boolean>(this.url, dataReport);
  }
}
