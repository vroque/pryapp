import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { OEANrcForSubstituteExam } from './oeaNrcForSubstituteExam';
import { AcademicProfile } from '../academic/academic.profile';

@Injectable()
export class OEANrcForSubstituteExamService {
  private url: string;  // a URL to web API

  constructor(private httpClient: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}learningassessment/NrcForSubstituteExam`;
  }

  /**
   * Obtener Nrcs programados de estudiante
   */
  getByStudent(): Observable<OEANrcForSubstituteExam[]> {
    return this.httpClient.get<OEANrcForSubstituteExam[]>(this.url);
  }

  /**
   * Obtener algunos detalles de lista de Nrcs
   * @param departament Modalidad
   * @param term Periodo
   * @param nrcs Lista de Nrcs a buscar
   */
  getListNrcs(departament: string, term: string, nrcs: string[]): Observable<OEANrcForSubstituteExam[]> {
    const data: any = {
      Department: departament,
      Term: term,
      ListNrcs: nrcs,
      Type: 1
    };
    return this.httpClient.post<OEANrcForSubstituteExam[]>(this.url, data);
  }

  /**
   * Obtener detalles de un nrc
   * @param departament Modalidad
   * @param term Periodo
   * @param nrc NRc
   */
  getDetailsNrc(departament: string, term: string, nrc: string) {
    return this.httpClient.get<OEANrcForSubstituteExam>(`${this.url}?departament=${departament}&term=${term}&nrc=${nrc}`);
  }

  /**
   * Guardar detalles de nrc
   * @param data Información a guardar
   */
  saveDetailsNrc(data: OEANrcForSubstituteExam): Observable<OEANrcForSubstituteExam> {
    return this.httpClient.put<OEANrcForSubstituteExam>(this.url, data);
  }

  /**
   * Obtener asignaturas disponibles
   * @param profile Perfil de estudiante
   * @param term Periodo
   */
  getByProfile(profile: AcademicProfile, term: string): Observable<OEANrcForSubstituteExam[]> {
    const data: any = {
      Profile: profile,
      Term: term,
      Type: 2
    };
    return this.httpClient.post<OEANrcForSubstituteExam[]>(this.url, data);
  }
}
