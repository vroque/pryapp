import { OEANrcForSubstituteExam } from './oeaNrcForSubstituteExam';
import { Persona } from '../persona/persona';
import { OEAFileNoteClaim } from './oeaFileNoteClaim';
import { OEAReportNoteClaim } from './oeaReportNoteClaim';

export class OEANoteClaim {
  id: number;
  year: string;
  noteClaimNumber: number;
  subjectId: number;
  type: string;
  status: number;
  pidmStudent: number;
  pidmTeacher: number;
  component: number;
  subComponent: number;
  scoreClaim: string;
  reason: string;
  hasPhysicalEvidence: boolean;
  officeId: number;
  active: boolean;
  author: number;
  dateCreated: Date;
  modifiedBy: number;
  dateModified: Date;
  comment: string;
  isValid: boolean;
  msgValid: string;
  subject: OEANrcForSubstituteExam;
  componentData: any;
  subComponentData: any;
  studentData: Persona;
  teacherData: Persona;
  files: OEAFileNoteClaim[];
  reportTeacher: OEAReportNoteClaim;
  isSelected: boolean;
}
