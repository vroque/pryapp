import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { OEAReportNoteClaim } from './oeaReportNoteClaim';

@Injectable()
export class OEAReportNoteClaimService {
  readonly url: string;  // a URL to web API
  constructor(private httpClient: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}learningassessment/ReportNoteClaim`;
  }

  /**
   * Guardar Informe
   * @param report Reporte
   */
  save(report: OEAReportNoteClaim): Observable<OEAReportNoteClaim> {
    return this.httpClient.post<OEAReportNoteClaim>(this.url, report);
  }

  validateReport(report: OEAReportNoteClaim): Observable<OEAReportNoteClaim> {
    return this.httpClient.put<OEAReportNoteClaim>(this.url, report);
  }
}
