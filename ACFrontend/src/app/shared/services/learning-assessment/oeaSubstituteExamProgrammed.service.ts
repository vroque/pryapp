import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { OEASubstituteExamProgrammed } from './oeaSubstituteExamProgrammed';
import { OEANrcForSubstituteExam } from './oeaNrcForSubstituteExam';
import { Student } from '../academic/student';

@Injectable()
export class OEASubstituteExamProgrammedService {
  private url: string;  // a URL to web API

  constructor(private httpClient: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}learningassessment/SubstituteExamProgrammed`;
  }

  /**
   * Obtener nrcs programados por el estudiante
   */
  getByStudent(): Observable<OEASubstituteExamProgrammed[]> {
    return this.httpClient.get<OEASubstituteExamProgrammed[]>(this.url);
  }

  /**
   * solicitar examen
   * @param data solicitados
   */
  saveProgrambyStudent(nrcProgram: OEANrcForSubstituteExam[], justification: string): Observable<boolean> {
    const data: any = {
      NrcProgram: nrcProgram,
      Justification: justification,
    };
    return this.httpClient.post<boolean>(this.url, data);
  }

  /**
   * Obtener nrcs programados por estudiante (perfil) y periodo
   * @param student estudiante
   * @param term Periodo
   */
  getByProfileAndTerm(student: Student, term: string): Observable<OEASubstituteExamProgrammed[]> {
    const data: any = {
      Student: student,
      Term: term,
      Type: 1
    };
    return this.httpClient.put<OEASubstituteExamProgrammed[]>(this.url, data);
  }

  /**
   * solicitar examen
   * @param data solicitados
   */
  saveProgrambyBack(student: Student, term: string, nrcProgram: OEANrcForSubstituteExam[], comment: string): Observable<boolean> {
    const data: any = {
      Student: student,
      Term: term,
      NrcProgram: nrcProgram,
      Comment: comment,
      Type: 2
    };
    return this.httpClient.put<boolean>(this.url, data);
  }

  modifyProgram(student: Student, term: string, substituteExams: OEASubstituteExamProgrammed[], comment: string): Observable<boolean> {
    const data: any = {
      Student: student,
      Term: term,
      SubstituteExams: substituteExams,
      Comment: comment,
      Type: 3
    };
    return this.httpClient.put<boolean>(this.url, data);
  }
}
