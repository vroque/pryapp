import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { OEANoteClaim } from './oeaNoteClaim';

@Injectable()
export class OEANoteClaimService {
  readonly url: string;  // a URL to web API

  constructor(private httpClient: HttpClient, private config: ConfigService) {
    this.url = `${config.getApiURI()}learningassessment/NoteClaim`;
  }

  /**
   * Obtener información del reclamo
   * @param id ID de reclamo
   */
  getById(id: number): Observable<OEANoteClaim> {
    return this.httpClient.get<OEANoteClaim>(`${this.url}/${id}`);
  }

  /**
   * Obtener datos de posible reclamo
   * @param departament Modalidad
   * @param term Periodo
   * @param nrc NRc
   * @param component  ID de componente
   * @param subComponent Id de subcomponente
   */
  getSubject(departament: string, term: string, nrc: string, component: string, subComponent: string): Observable<OEANoteClaim> {
    const api = `${this.url}?department=${departament}&term=${term}&nrc=${nrc}&component=${component}&subComponent=${subComponent}`;
    return this.httpClient.get<OEANoteClaim>(api);
  }

  /**
   * Registrar reclamo
   * @param noteClaim Reclamo
   */
  saveClaim(noteClaim: OEANoteClaim) {
    return this.httpClient.post<OEANoteClaim>(this.url, noteClaim);
  }

  /**
   * Obtener reclamos por estado y oficina
   * @param status estado
   * @param officeId officina
   */
  getListNotesClaimByStatusAndOffice(status: number, officeId: number): Observable<OEANoteClaim[]> {
    const api = `${this.url}?status=${status}&officeId=${officeId}`;
    return this.httpClient.get<OEANoteClaim[]>(api);
  }

  /**
   * Derivar a docente
   * @param noteClaim Reclamo derivado
   */
  saveDerivationTeacher(noteClaim: OEANoteClaim) {
    return this.httpClient.put<OEANoteClaim>(this.url, noteClaim);
  }

  /**
   * Obtener reclamos de docente
   */
  getListNotesClaimByTeacher(): Observable<OEANoteClaim[]> {
    return this.httpClient.get<OEANoteClaim[]>(this.url);
  }

  /**
   * Obtener reclamos de docente
   */
  finalizeClaim(noteClaim: OEANoteClaim): Observable<boolean> {
    return this.httpClient.patch<boolean>(this.url, noteClaim);
  }

  getListNotesClaimStudentByTerm(term: string): Observable<OEANoteClaim[]> {
    return this.httpClient.get<OEANoteClaim[]>(`${this.url}?term=${term}`);
  }
}
