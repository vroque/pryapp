import { OEANrcProgrammned } from './oeaNrcProgrammned';
import { OEANrcForSubstituteExam } from './oeaNrcForSubstituteExam';

export class OEASubstituteExamProgrammed {
  id: number;
  nrcProgrammnedId: number;
  pidm: number;
  programId: number;
  justification: string;
  active: boolean;
  author: string;
  dateCreated: Date;
  modifiedBy: string;
  dateModified: Date;
  comment: string;
  scheduled: OEANrcProgrammned;
  substituteExam: OEANrcForSubstituteExam;
}
