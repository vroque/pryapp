export class OEAFileNoteClaim {
  id: number;
  noteClaimId: number;
  reportNoteClaimId: number;
  path: string;
}
