export class OEAReportNoteClaim {
  id: number;
  noteClaimId: number;
  proceeds: boolean;
  scoreCorrect: string;
  reason: string;
  dateChange: Date;
  status: number;
  active: boolean;
  approved: boolean;
  author: number;
  dateCreated: Date;
  modifiedBy: number;
  dateModified: Date;
  comment: string;
  isValid: boolean;
  msgValid: string;
}
