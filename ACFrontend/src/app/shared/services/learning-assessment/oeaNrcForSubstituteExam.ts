import { OEANrcProgrammned } from './oeaNrcProgrammned';
import { Absence } from '../academic/absence';

export class OEANrcForSubstituteExam {
  id: number;
  department: string;
  campus: string;
  term: string;
  subjetID: string;
  nrc: string;
  partTerm: string;
  module: string;
  subjetName: string;
  active: boolean;
  hasSusti: boolean;
  author: string;
  dateCreated: Date;
  modifiedBy: string;
  dateModified: Date;
  comment: string;
  scheduled: OEANrcProgrammned[];
  absenceNrc: Absence;
  scheduledSelected: OEANrcProgrammned;
  // propiedades para facilitar el trabajo en batch
  selected: boolean;
  state: number;
}
