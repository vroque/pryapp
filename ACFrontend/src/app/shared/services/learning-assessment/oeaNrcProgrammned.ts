export class OEANrcProgrammned {
  id: number;
  nrcForSubstituteExamId: number;
  inicialRegistrationDate: Date;
  finalRegistrationDate: Date;
  examDate: Date;
  active: boolean;
  classRoom: string;
  isPublic: boolean;
  author: string;
  dateCreated: Date;
  modifiedBy: string;
  dateModified: Date;
  comment: string;
  selected: boolean;
}
