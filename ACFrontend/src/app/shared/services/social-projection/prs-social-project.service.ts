import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { PRSSocialProject } from './prs-social-project';

@Injectable()
export class PRSSocialProjectService {
  private url: string;  // URL to web API

  constructor (
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${ config.getApiURI()}SocialProjection/SocialProject`;
  }

  get (pidm: number): Observable<PRSSocialProject[]> {
    return this.http
      .get(`${this.url}?pidm=${pidm}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
}
