export class PRSSocialProject {
  IDProyecto: number;
  NombreProyecto: string;
  Asesor: string;
  Descripcion: string;
  IDEstado: number;
  IDPerAcad: string;
  FechaCreacion: Date;
  IDIntegrante: number;
  IDAlumno: string;
  IDEstadoIntegrante: number;
  Responsable: string;
  FechaRegistro: Date;
}
