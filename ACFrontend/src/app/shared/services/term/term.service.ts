import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';

import { Term } from './term';

@Injectable()
export class TermService {

  private readonly apiBaseUrl: string;
  private readonly apiUrl: string;

  constructor(private config: ConfigService, private http: HttpClient) {
    this.apiBaseUrl = this.config.getApiURI();
    this.apiUrl = `${this.apiBaseUrl}Settings/Term`;
  }

  /**
   * Term list
   * @returns {Observable<Term[]>}
   */
  getTermList(): Observable<Term[]> {
    return this.http.get<Term[]>(this.apiUrl);
  }

  /**
   * Save new term
   * @param {Term} term Term to save
   * @returns {Observable<Term>} Term saved
   */
  save(term: Term): Observable<Term> {
    return this.http.post<Term>(this.apiUrl, term);
  }

  /**
   * Update term
   * @param {Term} term Term to update
   * @returns {Observable<Term>} Term updated
   */
  update(term: Term): Observable<Term> {
    return this.http.put<Term>(this.apiUrl, term);
  }

  /**
   * Delete a term
   * @param {Term} term Term to delete
   * @returns {Observable<boolean>}
   */
  delete(term: Term): Observable<boolean> {
    const api = `${this.apiUrl}/${term.term}`;
    return this.http.delete<boolean>(api);
  }

}
