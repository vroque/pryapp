export class Term {
  term: string;
  term_description: string;
  start_date: Date;
  end_date: Date;
  update_date: Date;
}
