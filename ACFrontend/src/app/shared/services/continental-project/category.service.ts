import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { Category } from './category.model';

@Injectable()
export class CategoryService {
    private apiUri: string;

    constructor(private http: HttpClient, private config: ConfigService) {
        this.apiUri = `${this.config.getApiURI()}ContinentalProject/Categories`;
    }

    /**
     * Obtiene todos los proyectos activos
     */
    getAll(): Observable<Category[]> {
        return this.http.get<Category[]>(this.apiUri);
    }
}
