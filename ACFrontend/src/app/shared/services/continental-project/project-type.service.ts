import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { ProjectType } from './project-type.model';

@Injectable()
export class ProjectTypeService {
    private apiUri: string;

    constructor(private http: HttpClient, private config: ConfigService) {
        this.apiUri = `${this.config.getApiURI()}ContinentalProject/ProjectTypes`;
    }

    /**
     * Obtiene todos los proyectos activos
     */
    getAll(): Observable<ProjectType[]> {
        return this.http.get<ProjectType[]>(this.apiUri);
    }
}
