import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { EnrollProject } from './enroll-project.model';

@Injectable()
export class EnrollmentService {
    private apiUri: string;

    constructor(private http: HttpClient, private config: ConfigService) {
        this.apiUri = `${this.config.getApiURI()}ContinentalProject/Enrollments`;
    }

    /**
     * Inscribe a un estudiante en un perfil de un proyecto
     * @param idProfileProject Id del perfil del proyecto
     */
    enroll(idProfileProject: number): Observable<boolean> {
        return this.http.post<boolean>(this.apiUri, idProfileProject);
    }

    /**
     * Verifica si una persona esta inscrita en un proyecto
     * @param idProject Id del proyecto
     */
    getByIdProject(idProject: number): Observable<boolean> {
        return this.http.get<boolean>(`${this.apiUri}?idProject=${idProject}`);
    }

    /**
     * Actualiza el estado de una inscripcion
     * @param enrollProject Modelo de la inscripcion
     */
    updateStatus(enrollProject: EnrollProject): Observable<boolean> {
        return this.http.put<boolean>(this.apiUri, enrollProject);
    }
}
