import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { Project } from './project.model';

@Injectable()
export class ProjectService {
    private apiUri: string;

    constructor(private http: HttpClient, private config: ConfigService) {
        this.apiUri = `${this.config.getApiURI()}ContinentalProject/Projects`;
    }

    /**
     * Obtiene todos los proyectos activos
     */
    getAll(): Observable<Project[]> {
        return this.http.get<Project[]>(this.apiUri);
    }

    /**
     * Obtiene un proyecto por su id, incluye sus tablas relacionadas
     * @param id Id del proyecto
     */
    getById(id: number): Observable<Project> {
        return this.http.get<Project>(`${this.apiUri}/${id}`);
    }

    /**
     * Obtiene los proyectos de una categoria
     * @param id Id de la categoria
     */
    getByCategoryId(id: number): Observable<Project[]> {
        return this.http.get<Project[]>(`${this.apiUri}?idCategory=${id}`);
    }

    /**
     * Registra un nuevo proyecto
     * @param project Modelo del proyecto a regisrar
     */
    create(project: Project): Observable<Project> {
        return this.http.post<Project>(this.apiUri, project);
    }

    /**
     * Actualiza un proyecto
     * @param project Modelo del proyecto a regisrar
     */
    update(project: Project): Observable<boolean> {
        return this.http.put<boolean>(this.apiUri, project);
    }
}
