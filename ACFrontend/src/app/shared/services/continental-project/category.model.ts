export interface Category {
    id?: number;
    name?: string;
    quantityProjects?: number;
}
