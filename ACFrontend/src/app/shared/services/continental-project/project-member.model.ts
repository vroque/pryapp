import { MemberType } from './member-type.model';
import { ProfileProject } from './profile-project.model';

export interface ProjectMember {
    id?: number;
    div?: string;
    campus?: string;
    campusName?: string;
    program?: string;
    programName?: string;
    department?: string;
    departmentName?: string;
    pidm?: number;
    approved?: boolean;
    idMemberType?: number;
    memberType?: MemberType;
    idProfileProject?: number;
    profileProject?: ProfileProject;
    remarks?: string;
    createdAt?: Date;
    fullName?: string;
    changeStatusDate?: Date;
}
