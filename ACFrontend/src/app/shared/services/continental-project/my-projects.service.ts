import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { Project } from './project.model';

@Injectable()
export class MyProjectService {
    private apiUri: string;

    constructor(private http: HttpClient, private config: ConfigService) {
        this.apiUri = `${this.config.getApiURI()}ContinentalProject/MyProjects`;
    }

    /**
     * Obtiene un proyectos del administrativo logueado
     * @param id Id del proyecto
     */
    getAll(): Observable<Project[]> {
        return this.http.get<Project[]>(this.apiUri);
    }
}
