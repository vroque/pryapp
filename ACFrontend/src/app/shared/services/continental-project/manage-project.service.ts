import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { Category } from './category.model';
import { EnrollProject } from './enroll-project.model';

@Injectable()
export class ManageProjectService {
    private apiUri: string;

    constructor(private http: HttpClient, private config: ConfigService) {
        this.apiUri = `${this.config.getApiURI()}ContinentalProject/ManageProjects`;
    }

    /**
     * Obtiene todos los proyectos activos
     */
    approveProject(project: EnrollProject): Observable<boolean> {
        return this.http.put<boolean>(this.apiUri, project);
    }
}
