import { Status } from './status.model';
import { Category } from './category.model';
import { ProjectType } from './project-type.model';
import { ProjectMember } from './project-member.model';
import { ProfileProject } from './profile-project.model';

export interface Project {
    id?: number;
    div?: string;
    campus?: string;
    title?: string;
    objective?: string;
    description?: string;
    keywords?: string;
    idMicrosoftTeams?: string;
    idStatus?: number;
    status?: Status;
    idCategory?: number;
    category?: Category;
    approved?: boolean;
    idProjectType?: number;
    projectType?: ProjectType;
    projectMembers?: ProjectMember[];
    profileProjects?: ProfileProject[];
    changeStatusDate?: Date;
    active?: boolean;
    createdAt?: Date;
    updatedAt?: Date;
    createdBy?: string;
    updatedBy?: string;
}
