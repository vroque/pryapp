import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { ProjectMember } from './project-member.model';

@Injectable()
export class ProjectMemberService {
    private apiUri: string;

    constructor(private http: HttpClient, private config: ConfigService) {
        this.apiUri = `${this.config.getApiURI()}ContinentalProject/ProjectMembers`;
    }

    /**
     * Obtiene todos los inscritos en un proyecto
     */
    getInscribeds(idProject: number): Observable<ProjectMember[]> {
        return this.http.get<ProjectMember[]>(`${this.apiUri}/${idProject}`);
    }
}
