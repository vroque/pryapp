import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../../config/config.service';
import { SummaryProfile } from './summary-profile.model';

@Injectable()
export class SummaryProjectService {
    private apiUri: string;

    constructor(private http: HttpClient, private config: ConfigService) {
        this.apiUri = `${this.config.getApiURI()}ContinentalProject/SummaryProjects`;
    }

    /**
     * Obtiene el resumen de los perfiles de los proyectos
     * - Nombre del perfil
     * - Cuantas vacantes permite
     * - Cuantas vacantes ya fueron ocupadas
     * @param idProject Id del proyecto
     */
    getByIdProject(idProject: number): Observable<SummaryProfile[]> {
        return this.http.get<SummaryProfile[]>(`${this.apiUri}/${idProject}`);
    }
}
