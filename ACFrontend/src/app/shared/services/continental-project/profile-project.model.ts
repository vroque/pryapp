export interface ProfileProject {
    id?: number;
    title?: string;
    description?: string;
    idProject?: number;
    numberVacancies?: number;
}
