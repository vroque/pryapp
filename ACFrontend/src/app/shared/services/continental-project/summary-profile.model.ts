export interface SummaryProfile {
    idProfileProject: number;
    idProject: number;
    profileName: string;
    quantityVacancy: number;
    busyQuantityVacancy: number;
}
