export interface EnrollProject {
    idProject?: number;
    idProfileProject?: number;
    pidm?: number;
    status?: boolean;
}
