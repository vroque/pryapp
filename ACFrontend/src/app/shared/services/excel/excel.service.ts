import { Injectable } from '@angular/core';
/*import { saveAs } from 'file-saver';
import { WorkSheet, utils, WorkBook, write } from 'xlsx';*/
import { WorkSheet, utils, WorkBook, writeFile } from 'xlsx';

// const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelService {
  constructor() {
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: WorkSheet = utils.json_to_sheet(json);
    const workbook: WorkBook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
    writeFile(workbook, excelFileName + '_export_' + this.getDateForFileName() + EXCEL_EXTENSION);
    /**
     * NOTE: Comentar la línea anterior y descomentar este bloque para
     * usar 'file-saver' en caso exista algún problema, debido a la longitud del archivo, en la descarga
     * const excelBuffer: any = write(workbook, {bookType: 'xlsx', type: 'buffer'});
     * this.saveAsExcelFile(excelBuffer, excelFileName);
     */
  }

  /**
   * NOTE: Descomentar este bloque para usar 'file-saver' en caso exista algún
   * problema, debido a la longitud del archivo, en la descarga
   * private saveAsExcelFile(buffer: any, fileName: string): void {
   *   const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
   *   saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
   * }
   */

  getDateForFileName(): string {
    const now = new Date();
    return `${now.getFullYear().toString()}-` +
      `${(now.getMonth() + 1).toString()}-` +
      `${now.getDate().toString()}_` +
      `${now.getHours().toString()}` +
      `${now.getMinutes().toString()}` +
      `${now.getSeconds().toString()}`;
  }
}
