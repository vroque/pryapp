export class DisplayBlock {
  id: number;
  name: string;
  description: string;
  startFech: Date;
  endFech: Date;
  active: boolean;
}
