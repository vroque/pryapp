import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../../config/config.service';
import { DisplayBlock } from './display-block';

@Injectable()
export class DisplayBlockService {

  data: Observable<any>;
  private url: string;
  constructor(
    private http: Http,
    private config: ConfigService
  ) {
    this.url = `${config.getApiURI()}extra/displayblock`;
  }

  /**
   * get one DisplayBlock
   */
  get(id: number): Observable<DisplayBlock> {
    const type = 'one';
    const body = this.config.urlFormat({ id, type });
    return this.http
      .get(`${this.url}?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }
  /**
   * Get all DisplayBlock
   */
  getAll(id: number = 0): Observable<DisplayBlock[]> {
    const type = 'all';
    const body = this.config.urlFormat({ id, type });
    return this.http
      .get(`${this.url}?${body}`, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * create a DisplayBlock
   */
  post(data: DisplayBlock): Observable<DisplayBlock> {
    return this.http
      .post(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

  /**
   * update DisplayBlock
   */
  put(data: DisplayBlock): Observable<DisplayBlock> {
    return this.http
      .put(this.url, data, this.config.getHeaders('json'))
      .map(this.config.jsonExtractData)
      .catch(this.config.handleError);
  }

}
