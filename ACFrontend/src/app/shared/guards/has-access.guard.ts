import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { Menu } from '../menu';
import { Survey } from '../services/survey/survey';
import { TermsAndConditions } from '../services/terms-and-conditions/terms-and-conditions';

/**
 * Clase tipo Guard que valida que la URL que se va a visitar
 * este en la lista de URL disponibles segun los permisos
 * * solo valida dos niveles devido al registro
 */
@Injectable()
export class HasAccessGuard implements CanActivate {
  menus: Menu[];
  surveys: Survey[];
  terms: TermsAndConditions[];

  constructor(private router: Router) {
    this.menus = (window as any).choices as Menu[];
    this.surveys = (window as any).surveys as Survey[];
    this.terms = (window as any).terms as TermsAndConditions[];
  }

  /**
   * SObreescribe la funcion que impide acceder a ciertas ruta en base
   * a la lista de urls permitidas y rediriga a la pagina 401
   * @param route xyz
   * @param state abc
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // buscar que los menus coincidad en los primeros 2 niveles
    // backoffice/administracion
    const urlList: string[] = state.url.split('/'); // Array [ "", "backoffice", "descuentos", "asignar-descuento" ]
    const access: number = this.menus.filter((f) => f.uri === urlList[2]).length;
    if (access > 0) {
      if (this.terms.length) {
        this.router.navigate(['terms']);
      } else if (this.surveys.length) {
        this.router.navigate(['surveys']);
      } else {
        return true;
      }
    } else {
      this.router.navigate(['401']);
    }
    return false;
  }

}
