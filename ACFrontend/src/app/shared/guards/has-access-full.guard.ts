import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { Menu } from '../menu';
import { Survey } from '../services/survey/survey';
import { TermsAndConditions } from '../services/terms-and-conditions/terms-and-conditions';

/**
 * Clase tipo Guard que valida que la URL que se va a visitar
 * este en la lista de URL disponibles segun los permisos
 * * solo valida dos niveles devido al registro
 */
@Injectable()
export class HasAccessFullGuard implements CanActivate {
  menus: Menu[];
  surveys: Survey[];
  terms: TermsAndConditions[];

  constructor(private router: Router) {
    this.menus = (window as any).choices as Menu[];
    this.surveys = (window as any).surveys as Survey[];
    this.terms = (window as any).terms as TermsAndConditions[];
  }

  /**
   * SObreescribe la funcion que impide acceder a ciertas ruta en base
   * a la lista de urls permitidas y rediriga a la pagina 401
   * @param route xyz
   * @param state abc
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // backoffice/administracion
    // tslint:disable-next-line:prefer-const
    const url = state.url.split('?')[0] || '';
    const urlList: string[] = url.split('/'); // Array [ "", "backoffice", "descuentos", "asignar-descuento" ]
    urlList.shift();
    urlList.shift();

    let hasAccess = true;
    let i = 0;
    let parent: Menu;
    urlList.forEach((menuUri) => {
      if (i === 0) {
        parent = this.verifyUrl(menuUri, this.menus);
      } else {
        parent = this.verifyUrl(menuUri, parent.smenus);
      }
      if (parent === null) {
        hasAccess = false;
      }
      i++;
    });

    if (hasAccess) {
      if (this.terms.length) {
        this.router.navigate(['terms']);
      } else if (this.surveys.length) {
        this.router.navigate(['surveys']);
      } else {
        return true;
      }
    } else {
      this.router.navigate(['401']);
    }
    return false;
  }

  verifyUrl(menuUri: string, parent: Menu[]): any {
    if (parent.some((menu) => menu.uri === 'pry/frontdesk/vida-academica/proyectos-uc') && menuUri === 'proyectos-uc') {
      return parent.filter((menu) => menu.uri === 'pry/frontdesk/vida-academica/proyectos-uc')[0];
    }
    const exist: number = parent.filter((menu) => menu.uri === menuUri).length;
    if (exist > 0) {
      const menuItem: Menu = parent.filter((menu) => menu.uri === menuUri)[0];
      return menuItem;
    } else {
      return null;
    }
  }
}
