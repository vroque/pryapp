import { Component, OnInit, Input } from '@angular/core';
import { Callout } from '../callout/callout';


@Component({
  selector: 'message-student',
  templateUrl: './message-student.component.html'
})
export class MessageStudentComponent implements OnInit {

  @Input() message: Callout;
  @Input() size = 'lg'; // lg => large, md => mediun, sm => small

  constructor() { }

  ngOnInit() { }

}
