import { Component, Input, OnInit } from '@angular/core';
import { ConfigService } from '../../config/config.service';

@Component({
  providers: [ConfigService],
  selector: 'loading',
  template: `
  <div class="loader-box" *ngIf="size=='big'">
    <div *ngIf="ico" class="loader-logo">
			<img [src]="uri+'Content/static/images/isotipo-black.svg'" alt="">
		</div>
  	<div class="loader-circle" [ngClass]="{'extra': ico}"></div>
  	<div class="loader-circle loader-circle-static" [ngClass]="{'extra': ico}"></div>
  	<div class="loader-circumference"></div>
  	<div class="loader-circumference another"></div>
    <div class="loader-text">
      <p class="m-0">{{text}}</p>
      <p *ngIf="longTime">Por favor espere, esto puede llevar un tiempo.</p>
    </div>
  </div>
  <div class="loader" *ngIf="size=='small'">
    <div class="blur"></div>
    <p class="loader-text">{{text || 'Cargando...'}} <span *ngIf="longTime">Por favor espere, esto puede llevar un tiempo.</span></p>
  </div>
  <div class="loader d-inline" *ngIf="size=='tiny'">
    <div class="blur"></div>
  </div>
  `,
})
export class LoadingComponent implements OnInit {

  @Input() text = 'Cargando...';
  @Input() size: string;
  @Input() ico: boolean;
  uri: string;
  longTime = false;

  constructor(private config: ConfigService) {}

  ngOnInit(): void {
    this.size = this.size || 'big';
    this.ico = this.ico || false;
    this.uri = this.config.getBaseURI();

    // después de 3s se mostrara el mensaje para que espere
    setTimeout(() => {
      this.longTime = true;
    }, 3000);
  }

}
