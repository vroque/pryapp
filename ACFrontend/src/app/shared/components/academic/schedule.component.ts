import { Component, Input, OnInit } from '@angular/core';
import { Schedule } from '../../services/academic/schedule';

@Component({
  selector: 'uc-schedule',
  templateUrl: './schedule.component.html',
})
export class SharedScheduleComponent implements OnInit {

  @Input() schedule: Schedule[];
  @Input() noDataMsg: string;
  @Input() title: string;
  modulesNames: Array<{ fech: string; module: string; }>;
  timeStart = '23:00';
  timeEnd = '01:00';
  arrayHours: string[] = [];
  heightDay: string;
  errorMessage: string;
  show = false;
  loading = true;
  fechToday: Date = new Date();

  constructor() {
    // a
  }

  ngOnInit(): void {
    console.log('x-schedule: ', this.schedule);
    this.modulesNames = [{fech: '', module: 'MOD-1'}, { fech: '', module: 'MOD-2'}];
    this.getSchedule();
  }

  /**
   * [transformTime description]
   * @param  {string} time [hora en formato 24]
   * @return {number}      [return timestamp number]
   */
  transformTime(time: string): number {
    time = time.replace(/ /g, '');
    const timeArray = time.split(':');
    const timeStamp = parseInt( timeArray[0]) * 60 + parseInt(timeArray[1] );
    return timeStamp;
  }

  /**
   * [getLimitHours description]
   * get hour timeStart and timeEnd
   */
  getLimitHours(schedule: Schedule[]): void {
    for (const item of schedule) {
      for (const course of item.courses) {
        if (this.transformTime(this.timeStart) > this.transformTime(course.start)) {
          this.timeStart = course.start;
        }
        if (this.transformTime(this.timeEnd) < this.transformTime(course.end)) {
          this.timeEnd = course.end;
        }
      }
    }
    if (this.transformTime(this.timeStart) % 60 > 30) {
      this.timeStart = Math.trunc(this.transformTime(this.timeStart) / 60) + ':00';
    } else {
      this.timeStart = Math.trunc(this.transformTime(this.timeStart) / 60) + ':00';
    }

    if (this.transformTime(this.timeEnd) % 60 > 30) {
      this.timeEnd = (Math.trunc(this.transformTime(this.timeEnd) / 60) + 1) + ':00';
    } else {
      this.timeEnd = Math.trunc(this.transformTime(this.timeEnd) / 60) + ':00';
    }

    this.getArrayHours();
  }

  /**
   * [getArrayHours description]
   * get arrayHours for the list of hours
   */
  getArrayHours(): void {
    for (let time = this.transformTime(this.timeStart); time <= this.transformTime(this.timeEnd); time = time + 30) {
      const hora = (Math.trunc(time / 60) > 9) ? Math.trunc(time / 60) : '0' + Math.trunc(time / 60);
      const minutos = (time % 60 === 30) ? time % 60 : '00';
      this.arrayHours.push(hora + ':' + minutos);
    }
    this.heightDay = (this.arrayHours.length - 1) * 60 - 2 + 'px';
  }

  /**
   * [getSchedule description]
   * get data schedule
   */
   getSchedule(): void {
    if (this.schedule.length > 0) {
      this.deletModules(this.schedule);
      this.show = true;
    } else {
      this.show = false;
    }
  }

  /**
   * [getStylesToSchedule description]
   * get styles for li.single-event [heigth, top]
   */
  getStylesToSchedule(schedule: Schedule[]): void {
    for (const item of schedule) {
      if (item.courses.length > 0) {
        for (const course of item.courses) {
          course.height = ((this.transformTime(course.end) - this.transformTime(course.start))) * 2 + 'px';
          course.top = (((this.transformTime(course.start) - this.transformTime(this.timeStart))) * 2 - 2) + 'px';
        }
      }
    }
  }

  deletModules(schedule: Schedule[]): void {
    for (const iterator of schedule) {
      if (iterator.courses.length > 0) {
        // tslint:disable-next-line:prefer-for-of
        for (let position = 0; position < iterator.courses.length; position++) {
          /*Si es antes del inicio de clases se borra solo el MOD-2 en todo caso si lo hubiera*/
          if (iterator.courses[position].module === 'MOD-2' &&
            this.fechToday < new Date(iterator.courses[position].dateStart) &&
            this.fechToday < new Date(iterator.courses[position].dateEnd)) {
            iterator.courses.splice(position, 1);
            position--;
          } else if (iterator.courses[position].module === 'MOD-2' && /* Si es fuera del rango del MOD-2 se borrará*/
            this.fechToday < new Date(iterator.courses[position].dateStart) &&
            this.fechToday < new Date(iterator.courses[position].dateEnd)) {
              iterator.courses.splice(position, 1);
              position--;
          } else if (iterator.courses[position].module === 'MOD-1' && /*En caso de estar en MOD-2 se borra MOD-1*/
            this.fechToday > new Date(iterator.courses[position].dateStart) &&
            this.fechToday > new Date(iterator.courses[position].dateEnd)) {
            iterator.courses.splice(position, 1);
            position--;
            }
        }
      }
    }
    this.getLimitHours(this.schedule);
    this.getStylesToSchedule(this.schedule);
    console.log(this.schedule);
    this.loading = false;
  }

}
