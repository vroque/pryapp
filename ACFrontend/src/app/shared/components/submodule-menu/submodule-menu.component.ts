import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Menu } from '../../menu';
import { MenuService } from '../../menu.service';
import { runInDebugContext } from 'vm';

@Component({
  providers: [MenuService],
  selector: 'submodule-menu',
  templateUrl: './submodule-menu.component.html'
})
export class SubmoduleMenuComponent implements OnInit {

  loading = true;
  errorMessage: string;
  submenu: string;
  linkRoute: string;
  @Input() moduleUri: string;
  @Input() area: string;
  @Input() showMessage = false;
  submodule: { moduleUri: string; menu: Menu[] };

  constructor(
    private menuService: MenuService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.submodule = { moduleUri: this.moduleUri, menu: [] };
    this.route.params.forEach((params: Params) => {
      this.submenu = params['area'] || this.area || '';
      this.verifyGetMenu(this.menuService.query());
    });
  }

  /**
   * Definir que se obtiene el menu de un modulo o el menu de un submodulo
   * @param menus Todos los menus disponibles
   */
  verifyGetMenu(menus: Menu[]): void {
    if (this.submenu === '') {
      this.getSubmodules(menus);
    } else {
      this.getMenuSubmodules(menus, this.submenu);
    }
  }

  /**
   * Obtiene los menus de un submodulo
   * @param menu Todos los menus disponibles
   */
  getSubmodules(menu: Menu[]): void {
    for (const menuLocal of menu) {
      if (menuLocal.uri === this.submodule.moduleUri) {
        this.submodule.menu = menuLocal.smenus;
      }
    }
    this.linkRoute = this.submodule.moduleUri;
    (this.submodule.menu.length === 0) ?
    (this.errorMessage = 'Disculpe, tenemos algunos problemas al optener los submodulos, vuelva a intentarlo despues')
      : (this.errorMessage = undefined);
    this.loading = false;
  }

  /**
   * Obtiene el menu de un submodulo
   * @param menus Todos los menus disponibles
   * @param area Nombre del menu dentro del submodulo
   */
  getMenuSubmodules(menus: Menu[], area: string): void {
    for (const menu of menus) {
      if (menu.uri === this.submodule.moduleUri) {
        for (const smenu of menu.smenus) {
          if (smenu.uri === area) {
            this.submodule.menu = smenu.smenus;
          }
        }
      }
    }
    this.linkRoute = this.submodule.moduleUri + '/' + this.submenu;
    this.loading = false;
    this.updateShowMessage();
  }

  /**
   * Actualiza el mostrar el mensaje solo para autoservicio-banner
   */
  updateShowMessage(): void {
    if (this.area !== undefined && this.area === 'autoservicio-banner') {
      this.showMessage = false;
    } else if (this.area === undefined && this.submenu === 'autoservicio-banner') {
      this.showMessage = true;
    }
  }

}
