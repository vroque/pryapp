import { Component, Inject, OnInit } from '@angular/core';
import { DOCUMENT, Location } from '@angular/common';
import { ConfigService } from '../../config/config.service';


@Component({
  selector: 'redirect',
  templateUrl: '../template/redirect.component.html'
})
export class RedirectComponent implements OnInit {

  link: string;

  constructor(
    @Inject(DOCUMENT) private document: any,
    private configService: ConfigService
  ) {
    this.link = this.configService.getBaseURI() + 'frontdesk/vida-academica/ficha-socioconomica';
  }

  ngOnInit() {
    this.document.location.href = this.link;
  }
}
