import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Menu } from '../menu';
import { MenuService } from '../menu.service';
import { BackofficeConfigService } from '../../backoffice/config/config.service';

@Component({
  selector: 'cau-menus-url',
  templateUrl: './menus.url.component.html',
  providers: [MenuService, BackofficeConfigService]
})

export class CauMenusUrlComponent implements OnInit {
  allMenus: Menu[];
  constructor(private router: Router, private menuService: MenuService, private backofficeConfigService: BackofficeConfigService) { }
  ngOnInit(): void {
    this.allMenus = this.backofficeConfigService.getRoutesByRoute(this.router.url, this.menuService);
  }
}
