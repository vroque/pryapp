import { Component, EventEmitter, Input, LOCALE_ID, OnInit, Output } from '@angular/core';

import { MomentService } from '../../moment.service';
import { SignatoriesService } from '../../services/atentioncenter/signatories.service';

import { Request } from '../../services/atentioncenter/request';

@Component({
  selector: 'bo-request-abstract',
  templateUrl: './boRequestAbstract.component.html',
  providers: [
    {provide: LOCALE_ID, useValue: 'es'},
    MomentService,
    SignatoriesService,
  ],
})
export class BORequestAbstractComponent implements OnInit {
  @Input() config: any;
  @Input() request: Request;
  @Output() update = new EventEmitter();
  signatories: string[];
  error_message: string;

  constructor(private momentService: MomentService, private signatoriesService: SignatoriesService) {
  }

  ngOnInit(): void {
    this.getSignatories(this.request.document_id, this.request.program);
  }

  docSt(id: number): any {
    return this.config.getDocumentState(id);
  }

  getSignatories(documentId: number, program: string): void {
    const data: any = {program: program};
    this.signatoriesService.query(documentId, data)
      .subscribe(
        signatories => this.signatories = signatories,
        error => this.error_message = error as any,
        () => this.update.emit('update')
      );
  }

  /**
   * Tiempo relativo a partir de una fecha tipo YYYYMMDD
   * p.e.: momento("20101225") retorna un string: "hace X años"
   * @param {string} date
   * @returns {string}
   */
  moment(date: string): string {
    return this.momentService.fromNow(date);
  }
}
