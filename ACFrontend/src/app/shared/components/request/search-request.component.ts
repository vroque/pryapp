import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { MomentService } from '../../moment.service';
import { Request } from '../../services/atentioncenter/request';
import { RequestService } from '../../services/atentioncenter/request.service';

@Component({
  selector: 'search-request',
  templateUrl: './search-request.component.html',
  providers: [
    BackofficeConfigService, MomentService, RequestService
  ],
})
export class SearchRequestComponent implements OnInit {

  requestList: Request[];
  searching: boolean;
  searchData: any; // model for search inputs
  hasSearchData: boolean;
  hasSearchDataForClean: boolean;
  showMoreSearchOptions: boolean;
  showSearchResult: boolean;
  filter: any;
  configFile: any;

  constructor(private config: BackofficeConfigService,
              private moment: MomentService,
              private requestService: RequestService,
              private router: Router,
              private location: Location) {
    this.configFile = this.config;
  }

  ngOnInit() {
    this.requestList = [];
    this.searching = false;
    this.clean();
  }

  /**
   * Limpia Modelos
   */
  clean() {
    this.searchData = {};
    this.hasSearchData = false;
    this.hasSearchDataForClean = false;
    this.showSearchResult = false;
  }

  /**
   * Verifica si algún campo de búsqueda está con data
   */
  existSearchData() {
    if (this.searchData !== undefined) {
      this.hasSearchDataForClean = true;
      if (this.searchData.id !== undefined) {
        this.searchData.id = this.searchData.id.trim();
        if (this.searchData.id.length > 0) {
          this.hasSearchData = true;
        }
      }
      if (this.searchData.student_id !== undefined) {
        this.searchData.student_id = this.searchData.student_id.trim();
        if (this.searchData.student_id.length > 0) {
          this.hasSearchData = true;
        }
      }
      if (this.searchData.date !== undefined) {
        this.searchData.date = this.searchData.date.trim();
        if (this.searchData.date.length > 0) {
          this.hasSearchData = true;
        }
      }
      if (this.searchData.document_id !== undefined) {
        this.hasSearchData = true;
      }
      if (this.searchData.state !== undefined) {
        this.hasSearchData = true;
      }
    }
  }

  moreSearchOptions(): void {
    this.showMoreSearchOptions = !this.showMoreSearchOptions;
  }

  /**
   * Busca Solicitudes
   * @param data
   */
  searchRequest(data: any): void {
    console.log('Searching request...');
    console.log('this.searchData => ', this.searchData);
    this.searching = true;
    this.showSearchResult = false;
    this.requestService.search(data)
      .subscribe(
        (request) => this.requestList = request,
        (error) => console.log(error),
        () => {
          this.searching = false;
          this.showSearchResult = true;
          console.log('this.requestList => ', this.requestList);
        },
      );
  }

  /**
   * Tiempo relativo a partir de una fecha tipo YYYYMMDD
   * p.e.: momento("20101225") retorna un string: "hace X años"
   * @param {string} date
   * @returns {string}
   */
  momento(date: string): string {
    return this.moment.fromNow(date);
  }

  /**
   * Ir a detalle de solicitud
   * @param {string} requestId
   */
  goToRequestDetail(requestId: string): void {
    const pathName: string[] = this.location.path().split('/');
    if (pathName.length > 3) {
      const link: string[] = [`${pathName[1]}/${pathName[2]}/solicitudes/${requestId}`];
      this.router.navigate(link);
    } else {
      console.log('pathname', pathName);
    }
  }

}
