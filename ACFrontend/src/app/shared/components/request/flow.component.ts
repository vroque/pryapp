import { Component, Input, OnInit } from '@angular/core';
import { DocumentState } from '../../services/atentioncenter/document.state';
import { Request } from '../../services/atentioncenter/request';
import { RequestLog } from '../../services/atentioncenter/request.log';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';

@Component({
  selector: 'request-flow',
  templateUrl: './flow.component.html',
  providers: [BackofficeConfigService],
})
export class RequestFlowComponent {
  @Input('ideal-flow')ideal_flow: DocumentState[];
  @Input()request: Request;
  @Input()logs: RequestLog[];

  constructor(
    private config: BackofficeConfigService
  ) { }

  docSt(id: number): any {
    return this.config.getDocumentState(id);
  }

  getLogTrace(state: number): RequestLog {
    for (let i = 0; i < this.logs.length; i++) {
      if (this.logs[i].old_state === state) {
        return this.logs[i];
      }
    }
    return null;
  }
}
