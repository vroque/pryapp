import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { Request } from '../../services/atentioncenter/request';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { GenerateDocumentService } from '../../../shared/services/atentioncenter/generate.document.service';

@Component({
  selector: 'bo-generate-document',
  templateUrl: './generateDocument.component.html',
  providers: [
    BackofficeConfigService,
    GenerateDocumentService
  ]
})
export class GenerateDocumentComponent implements OnInit {
  @Input()request: Request;
  @Output()updateLogs = new EventEmitter();
  pdf_list: Array<any> = [];
  error_message: string;

  constructor(
    private config: BackofficeConfigService,
    private generateDocumentService: GenerateDocumentService
  ) { }

  ngOnInit(): void {
    console.log('start bo generate')
    // buscar si son varios documentos a generar o si es solo uno y
    //  preparar el objeto para usarlo en la interfaz
    const extendsProperties: any = JSON.parse(this.request.description) || {};

    if (extendsProperties.term_list && extendsProperties.term_list.length > 0
        && (
          this.request.document_id === this.config.DOC_REPORT_CARD
          ||   this.request.document_id === this.config.DOC_STUDIES_PROOF
          ||   this.request.document_id === this.config.DOC_3510SUPERIOR_PROOF
        )
    ) {
      let index = 1;
      for (const term of extendsProperties.term_list){
        const pdf: string = this.request.document_uri.filter( uri => uri.indexOf(`-${index}-`) > 0)[0] || null;
        const obj: any = {term: term, uri: pdf, status: 'pre'}; // buscar si ya tiene el documento
        this.pdf_list.push(obj);
        index++;
      }
    } else {
      const obj: any = {term: null, uri: this.request.document_uri[0] || null, status: 'pre'}; // buscar si ya tiene el documento
      this.pdf_list.push(obj);
    }
    // para carnet de estudiante
    if (this.request.document_id === this.config.DOC_STUDENT_CARD || this.request.document_id === this.config.DOC_CUSTOM_STUDIES_PROOF) {
      this.pdf_list[0].status = 'post';
    }
    // console.log(this.pdf_list);
  }

  generateDocument(requestId: number, pdf: any, i: number): void {
    pdf.status = 'on';
    let ext: any = {};
    if (pdf.term && i !== NaN ) {
      ext = {term: pdf.term, index: i}; // inicia en 1 no en 0
    }

    this.generateDocumentService.create(requestId, ext)
      .subscribe(
        doc => {
          this.request.document_uri = doc.path;
          pdf.uri = doc.path;
        },
        error => { pdf.status = 'err'; this.error_message = <any>error; },
        () => {
          this.updateLogs.emit('complete');
        }
      );
  }

  isValid(): boolean {
    if (this.request.state !== this.config.STATE_PAYMENT_PENDING
      && this.request.state !== this.config.STATE_CANCEL
      && this.request.state !== this.config.STATE_INITIAL ) {
      return true;
    }
    return false;
  }
}
