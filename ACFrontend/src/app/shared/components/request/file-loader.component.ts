import { Component, Input, OnInit } from '@angular/core';
import { Request } from '../../services/atentioncenter/request';
import { ACRequirement } from '../../services/atentioncenter/requirement';
import { ACRequirementFile } from '../../services/atentioncenter/requirement';
import { ACRequirementService } from '../../services/atentioncenter/requirement.service';

@Component({
  selector: 'file-loader',
  templateUrl: './file-loader.component.html',
  providers: [ACRequirementService]
})
export class ACFileLoaderComponent implements OnInit {
  @Input() request: Request;
  docs: Array<ACRequirement>;

  constructor(
    private requirementService: ACRequirementService
  ) { }

  ngOnInit(): void {
    this.requirementService.byRequest(this.request.id, this.request.document_id)
      .subscribe(
        (docs) => this.docs = docs,
        (err) => console.error(err)
      );
  }

  delivered(event: any, requirement: ACRequirement): void {
    // create file
    const file: ACRequirementFile = new ACRequirementFile(
      0, requirement.id, this.request.id, 'new register', null
    );
    this.requirementService.addFile(this.request.id, file)
      .subscribe(
        (data) => requirement.files.push(data),
        (error) => console.error(error),
        () => (<any>requirement).delivered = true
      );
  }

  removeFile(requirement: ACRequirement, file: ACRequirementFile): void {
    const index = requirement.files.indexOf(file);
    if (index > -1) {
      this.requirementService.deleteFile(file.id)
        .subscribe(
          (data) => requirement.files.splice(index, 1),
          (error) => console.error(error),
        );
    }
  }

}
