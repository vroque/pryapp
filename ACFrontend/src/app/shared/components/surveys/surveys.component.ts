import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Survey } from '../../services/survey/survey';
import { AcademicProfile } from '../../services/academic/academic.profile';
import { StudentFactory } from '../../services/academic/student.factory';

@Component({
  selector: 'surveys',
  templateUrl: './surveys.component.html',
})
export class SurveysComponent {
  surveys: Survey[];
  profile: AcademicProfile;
  constructor(private router: Router) {
    this.surveys = (window as any).surveys as Survey[];
    this.goHome();
    this.profile = this.profile || StudentFactory.getInstance().get().profile;
    console.log(this.profile);
  }

  /**
   * Slice text
   * @param {string} text
   * @param {number} len
   * @returns {string}
   */
  getSlice(text: string, len: number): string {
    if (text.length <= len) {
      return text;
    }
    return text.slice(0, len).concat('...');
  }

  /**
   * Go Home
   */
  goHome(): void {
    if (!this.surveys.length) {
      this.router.navigate(['']);
    }
  }
}
