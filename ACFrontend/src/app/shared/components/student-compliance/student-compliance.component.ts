import { Component, Input, OnInit } from '@angular/core';
import { Message } from 'primeng/primeng';

import { AcademicProfile } from '../../services/academic/academic.profile';
import { AcademicSummaryService } from '../../services/academic/academic-summary.service';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { CICProfessionalEnglishService } from '../../services/languages-center/cic-professional-english.service';
import { CICSecondLanguageService } from '../../services/languages-center/cic-second-language.service';
import { OPPPracticeDocumentService } from '../../services/professional-practices/opp-practice-document.service';
import { PRSSocialProjectService } from '../../services/social-projection/prs-social-project.service';
import { RequerimentService } from '../../services/admision/requeriment.service';
import { StudentFactory } from '../../services/academic/student.factory';

@Component({
  selector: 'student-compliance',
  templateUrl: './student-compliance.component.html',
  providers: [
    BackofficeConfigService,
    RequerimentService,
    OPPPracticeDocumentService,
    PRSSocialProjectService,
    AcademicSummaryService,
    CICProfessionalEnglishService,
    CICSecondLanguageService
  ]
})
export class StudentComplianceComponent implements OnInit {
  loadingRequirements = true;
  displayBlock = false;
  errorRequirements = false;
  errorMessage = 'Lo sentimos, tenemos inconvenientes para obtener sus datos, vuelva a intentarlo en un momento.';
  academicProfile: AcademicProfile;

  @Input() set profile(value: AcademicProfile) {
    if (this.academicProfile !== value) {
      this.academicProfile = value;
    }
  }

  @Input() studentComplianceEnabled: boolean;
  @Input() academicSummaryEnabled: boolean;

  get profile(): AcademicProfile {
    return this.academicProfile;
  }

  complianceMessage: Message[] = [];

  socialProjectionId = 'social_projection';
  professionalPracticesId = 'professional_practices';
  creditsCompletedId = 'number_cycles';
  titleId = 'title';
  finishedId = 'finished';

  secondLanguageId = 'second_language';
  secondLanguageMsg = 'Acredita el dominio de un segundo idioma para fortalecer ' +
    'tus competencias laborales del 1ro al 5to período académico.';
  secondLanguageName = 'Segundo idioma';

  professionalEnglishId = 'professional_english';
  professionalEnglishMsg = 'Acredita tus conocimientos de inglés para que puedas ' +
    'matricularte en inglés profesional.';
  professionalEnglishName = 'Inglés (prerequisito para cursar inglés profesional)';

  requirements = {
    actualCredits: 0,
    actualCycle: 0,
    bachelorRequirements: {
      data: [
        {
          id: this.secondLanguageId,
          msg: this.secondLanguageMsg,
          name: this.secondLanguageName,
          status: false,
          loading: false
        },
        {
          id: this.socialProjectionId,
          msg: 'Realiza tu proyección social y ejecuta acciones que contribuyan al '
            + 'desarrollo sostenible de personas y comunidades del 1ro a 10mo período académico.',
          name: 'Proyección social',
          status: false,
          loading: false
        },
        {
          id: this.professionalPracticesId,
          msg: 'Complementa y aplica los conocimientos que recibiste en la Universidad con la experiencia ' +
            'profesional del 8vo a 9no período académico.',
          name: 'Prácticas preprofesionales',
          status: false,
          loading: false
        },
      ],
      title: 'Bachiller',
    },
    graduatedRequirements: {
      data: [
        {
          id: this.secondLanguageId,
          msg: this.secondLanguageMsg,
          name: this.secondLanguageName,
          status: false,
          loading: false
        },
        {
          id: this.socialProjectionId,
          msg: 'Realiza tu proyección social y ejecuta acciones que contribuyan al desarrollo sostenible de personas y comunidades del '
            + '1ro a 10mo período académico.',
          name: 'Proyección social',
          status: false,
          loading: false
        },
        {
          id: this.professionalPracticesId,
          msg: 'Complementa y aplica los conocimientos que recibiste en la Universidad con la experiencia profesional del 8vo a 9no '
            + 'período académico.',
          name: 'Prácticas preprofesionales',
          status: false,
          loading: false
        },
      ],
      title: 'Egresado',
    },
    studentPga: 0,
    postulantRequirements: {
      data: [] as any[],
      title: 'REQUISITOS DE POSTULANTE',
    },
    studentState: 0,
    studentRequirements: {
      data: [
        {
          id: this.professionalEnglishId,
          msg: this.professionalEnglishMsg,
          name: this.professionalEnglishName,
          status: false,
          loading: false
        },
        {
          id: this.secondLanguageId,
          msg: this.secondLanguageMsg,
          name: this.secondLanguageName,
          status: false,
          loading: false
        },
        {
          id: this.socialProjectionId,
          msg: 'Realiza tu proyección social y ejecuta acciones que contribuyan al desarrollo sostenible de personas ' +
            'y comunidades del 1ro a 10mo período académico.',
          name: 'Proyección social',
          status: false,
          loading: false
        },
        {
          id: this.professionalPracticesId,
          msg: 'Complementa y aplica los conocimientos que recibiste en la Universidad con la experiencia profesional ' +
            'del 8vo a 9no período académico.',
          name: 'Prácticas preprofesionales',
          status: false,
          loading: false
        },
        {
          id: this.creditsCompletedId,
          msg: '',
          name: 'Créditos',
          status: false,
          loading: false
        },
      ],
      title: 'Estudiante',
    },
    titledRequirements: {
      data: [
        {id: this.titleId, name: 'Título Profesional', msg: '', status: false, loading: false},
      ],
      title: 'Titulado',
    },
    totalCredits: 0,
    totalCycles: 0,
  };

  constructor(private config: BackofficeConfigService,
              private requirementService: RequerimentService,
              private professionalEnglishService: CICProfessionalEnglishService,
              private secondLanguageService: CICSecondLanguageService,
              private serOPPPracticeDocumentService: OPPPracticeDocumentService,
              private serPRSSocialProjectService: PRSSocialProjectService,
              private academicSummaryService: AcademicSummaryService) {
  }

  ngOnInit(): void {
    console.log(this.profile);
    this.profile = this.profile || StudentFactory.getInstance().get().profile;
    if (this.profile.term_catalg > '2018') {
      this.requirements.studentRequirements.data
        .filter((f) => f.id === this.socialProjectionId)[0].name = 'Actividades Extracurriculares';
      this.requirements.graduatedRequirements.data
        .filter((f) => f.id === this.socialProjectionId)[0].name = 'Actividades Extracurriculares';
      this.requirements.bachelorRequirements.data
        .filter((f) => f.id === this.socialProjectionId)[0].name = 'Actividades Extracurriculares';

      this.requirements.studentRequirements.data
        .filter((f) => f.id === this.socialProjectionId)[0].msg = 'Realiza tus actividades extracurriculares '
        + 'en impacto social, arte y cultura, deportes, formación complementaria, embajadores Continental o intereses.';
      this.requirements.graduatedRequirements.data
        .filter((f) => f.id === this.socialProjectionId)[0].msg = 'Realiza tus actividades extracurriculares en '
        + 'impacto social, arte y cultura, deportes, formación complementaria, embajadores Continental o intereses.';
      this.requirements.bachelorRequirements.data
        .filter((f) => f.id === this.socialProjectionId)[0].msg = 'Realiza tus actividades extracurriculares en '
        + 'impacto social, arte y cultura, deportes, formación complementaria, embajadores Continental o intereses.';
    }
  }

  showDetail(showMoreDetail: boolean): void {
    this.displayBlock = showMoreDetail;
    this.loadingRequirements = !showMoreDetail;
    if (showMoreDetail) {
      this.getApplicantRequirements();
    }
  }

  /**
   * Requisitos de postulante
   */
  getApplicantRequirements(): void {
    this.loadingRequirements = true;
    this.displayBlock = true;
    this.requirements.studentState = this.profile.status;
    const req = {
      college: this.profile.college.id,
      department: this.profile.department,
      person_id: this.profile.person_id,
      program: this.profile.program.id,
    };
    this.requirementService.query(req, true)
      .subscribe(
        (data) => {
          if (data != null && data.length > 0) {
            this.requirements.postulantRequirements.data = data;
            this.errorRequirements = false;
          }
        },
        (err) => {
          console.log('requerimentService => ', err);
          this.loadingRequirements = false;
          this.displayBlock = false;
          this.errorRequirements = true;
        },
        () => {
          this.getOPPPracticeDocument();
          this.getProfessionalEnglishStatus(this.profile.person_id);
          this.getSecondLanguageStatus(this.profile.person_id, this.profile.program.id);
        }
      );
  }

  /**
   * Professional english status
   * @param studentPidm Student PIDM
   */
  getProfessionalEnglishStatus(studentPidm: number): void {
    if (this.profile.term_catalg >= '2015') {
      this.requirements.studentRequirements.data
        .filter((f) => f.id === this.professionalEnglishId)[0].loading = true;
      this.professionalEnglishService.qualifyForEnrollInProfessionalEnglish(studentPidm)
        .subscribe(
          (result) => {
            this.requirements.studentRequirements.data
              .filter((f) => f.id === this.professionalEnglishId)[0].status = result;
          },
          (error) => {
            console.log(error);
            this.requirements.studentRequirements.data
              .filter((f) => f.id === this.professionalEnglishId)[0].loading = false;
          },
          () => {
            this.requirements.studentRequirements.data
              .filter((f) => f.id === this.professionalEnglishId)[0].loading = false;
          }
        );
    } else {
      this.requirements.studentRequirements.data = this.requirements.studentRequirements.data
        .filter(f => f.id !== this.professionalEnglishId);
    }
  }

  /**
   * Second language status
   * @param studentPidm Student PIDM
   * @param programId Program ID
   */
  getSecondLanguageStatus(studentPidm: number, programId: string): void {
    this.requirements.studentRequirements.data
      .filter((f) => f.id === this.secondLanguageId)[0].loading = true;
    this.secondLanguageService.hasSecondLanguageByProgram(studentPidm, programId)
      .subscribe(
        (result) => {
          this.requirements.studentRequirements.data
            .filter((f) => f.id === this.secondLanguageId)[0].status = result;
          this.requirements.graduatedRequirements.data
            .filter((f) => f.id === this.secondLanguageId)[0].status = result;
          this.requirements.bachelorRequirements.data
            .filter((f) => f.id === this.secondLanguageId)[0].status = result;
        },
        (error) => {
          console.log(error);
          this.requirements.studentRequirements.data
            .filter((f) => f.id === this.secondLanguageId)[0].loading = false;
        },
        () => {
          this.requirements.studentRequirements.data
            .filter((f) => f.id === this.secondLanguageId)[0].loading = false;
        }
      );
  }

  getOPPPracticeDocument(): void {
    this.serOPPPracticeDocumentService.get(this.profile.person_id, this.profile.program.id)
      .subscribe(
        (data) => {
          if (data != null && data.length > 0) {
            this.requirements.studentRequirements.data
              .filter((f) => f.id === this.professionalPracticesId)[0].status = true;
            this.requirements.graduatedRequirements.data
              .filter((f) => f.id === this.professionalPracticesId)[0].status = true;
            this.requirements.bachelorRequirements.data
              .filter((f) => f.id === this.professionalPracticesId)[0].status = true;
            this.errorRequirements = false;
          }
        },
        (err) => {
          console.log('serOPPPracticeDocumentService => ', err);
          this.loadingRequirements = false;
          this.displayBlock = false;
          this.errorRequirements = true;
        },
        () => this.getPRSSocialProject(),
      );
  }

  getPRSSocialProject(): void {
    this.serPRSSocialProjectService.get(this.profile.person_id)
      .subscribe(
        (data) => {
          if (data != null && data.length > 0) {
            this.requirements.studentRequirements.data
              .filter((f) => f.id === this.socialProjectionId)[0].status = true;
            this.requirements.graduatedRequirements.data
              .filter((f) => f.id === this.socialProjectionId)[0].status = true;
            this.requirements.bachelorRequirements.data
              .filter((f) => f.id === this.socialProjectionId)[0].status = true;
            this.errorRequirements = false;
          }
        },
        (err) => {
          console.log('serPRSSocialProjectService => ', err);
          this.loadingRequirements = false;
          this.displayBlock = false;
          this.errorRequirements = true;
        },
        () => {
          if (this.academicSummaryEnabled) {
            this.getAcademicSummary();
          }
          this.loadingRequirements = false;
          this.displayBlock = false;
        }
      );
  }

  getAcademicSummary(): void {
    this.academicSummaryService.getByProfileData(this.profile)
      .subscribe(
        (data) => {
          if (data != null) {
            this.requirements.actualCredits = data.actual_credits;
            this.requirements.totalCredits = data.total_credits;
            this.requirements.actualCycle = data.actual_cycle;
            this.requirements.totalCycles = data.total_cycles;
            this.requirements.studentPga = data.pga;
            if (data.actual_credits >= data.total_credits && data.actual_credits > 0) {
              this.requirements.studentRequirements.data
                .filter((f) => f.id === this.creditsCompletedId)[0].status = true;
            }
            this.errorRequirements = false;
          }
        },
        (err) => {
          console.log('serAcademicSummaryService => ', err);
          this.loadingRequirements = false;
          this.displayBlock = false;
          this.errorRequirements = true;
        },
        () => {
          this.getStateStudent();
          this.loadingRequirements = false;
          this.displayBlock = false;
        },
      );
  }

  getStateStudent(): void {
    this.requirements.studentState = this.profile.status;
    if (this.requirements.studentState === this.config.STATE_DEGREE) {
      this.requirements.titledRequirements.data.filter((f) => f.id === this.titleId)[0].status = true;
    }
    this.showMessageCompliance();
  }

  showMessageCompliance(): void {
    this.complianceMessage = [];
    // if (this.requeriments.student_requeriments.data
    //     .filter((f) => f.id === this.id_second_language)[0].status === false && this.requeriments.actual_cycle >= 1) {
    //     this.msg_compliance.push({
    //       detail: this.requeriments.student_requeriments.data.filter((f) => f.id === this.id_second_language)[0].msg,
    //       severity: 'warn',
    //       summary: this.requeriments.student_requeriments.data.filter((f) => f.id === this.id_second_language)[0].name,
    //     });
    // }
    if (this.requirements.studentRequirements.data
      .filter((f) => f.id === this.socialProjectionId)[0].status === false && this.requirements.actualCycle >= 1) {
      this.complianceMessage.push({
        detail: this.requirements.studentRequirements.data.filter((f) => f.id === this.socialProjectionId)[0].msg,
        severity: 'warn',
        summary: this.requirements.studentRequirements.data.filter((f) => f.id === this.socialProjectionId)[0].name,
      });
    }
    if (this.requirements.studentRequirements.data
        .filter((f) => f.id === this.professionalPracticesId)[0].status === false
      && this.requirements.actualCycle >= 8) {
      this.complianceMessage.push({
        detail: this.requirements.studentRequirements.data
          .filter((f) => f.id === this.professionalPracticesId)[0].msg,
        severity: 'warn',
        summary: this.requirements.studentRequirements.data
          .filter((f) => f.id === this.professionalPracticesId)[0].name,
      });
    }
  }

}
