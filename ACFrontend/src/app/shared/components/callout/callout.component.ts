import { Component, OnInit, Input } from '@angular/core';
import { Callout } from './callout';

@Component({
  selector: 'callout-component',
  templateUrl: './callout.component.html'
})
export class CalloutComponent implements OnInit {

  @Input() callout: Callout;
  constructor() { }

  ngOnInit() { }
}
