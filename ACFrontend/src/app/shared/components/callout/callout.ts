export class Callout {
  public data: string[];
  public title: string;
  public type: string;
}
