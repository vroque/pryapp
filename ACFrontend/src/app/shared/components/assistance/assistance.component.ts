import { Absence } from '../../services/academic/absence';
import { AbsenceService } from '../../services/academic/absence.service';
import { Component, OnInit } from '@angular/core';
import { Callout } from '../../components/callout/callout';

@Component({
  providers: [AbsenceService],
  selector: 'assistance-student',
  styles: ['@media print {.absences-by-course { width: calc(100%/3); float: left;} }'],
  templateUrl: './assistance.component.html'
})
export class AssistanceComponent implements OnInit {

  absences: Absence[];
  course: Absence;
  show: boolean;
  showDetail = false;
  errorMessage: string;
  loading = true;
  callout: Callout;

  constructor (private absenceService: AbsenceService) {}

  ngOnInit(): void {
    this.getAssistance();
  }

  getDetalle(absenceCourse): void {
    this.course = absenceCourse;
  }

  getAssistance(): void {
    this.absenceService.get()
      .subscribe(
        (data) => {
          this.absences = data;
          this.loading = false;
          if (this.absences.length > 0) {
            this.show = true;
          } else {
            this.show = false;
            this.callout = {
              title: 'Felicidades',
              type: 'success',
              data: ['Usted no cuenta con inasistencias para este período.']
            };
          }
        },
        (error) => {
          this.callout = {
            title: 'Lo sentimos',
            type: 'warning',
            data: [
              'No se pudieron obtener las inasistencias para las asignaturas.',
              'Estamos trabajando para solucionarlo.'
            ]
          };
        }
      );
  }
}
