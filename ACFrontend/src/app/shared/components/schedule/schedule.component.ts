import { Component, Input, OnInit } from '@angular/core';

import { AcademicOfferService } from '../../services/course-display/AcademicOffer.service';
import { Schedule } from '../../services/academic/schedule';
import { ScheduleService } from '../../services/academic/schedule.service';
import { ScheduleCourse } from '../../services/academic/schedule.course';
import { EnrollmentTermService } from '../../services/academic/enrollmentTerm.service';

@Component({
  providers: [ScheduleService, AcademicOfferService, EnrollmentTermService],
  selector: 'schedule-student',
  templateUrl: './schedule.component.html'
})
export class ScheduleComponent implements OnInit {

  @Input() schedule: Schedule[] = [];
  @Input() offeredCourse = false;
  @Input() verifyAsignature = true;
  scheduleTerm: {
    lstTerm: string[];
    scheduleBA: Schedule[];
    scheduleBB: Schedule[];
    block: string;
    term: string;
    schedule: Schedule[]
  };
  timeStart: string;
  timeEnd: string;
  arrayHours: string[] = [];
  heightDay: string;
  errorMessage: string;
  errorSchedule = false;
  loading = true;
  resumeAsignature: ScheduleCourse[] = [];
  schedulecontent = false;

  constructor(private scheduleService: ScheduleService, private academicService: AcademicOfferService,
    private enrollmentTermService: EnrollmentTermService) { }

  ngOnInit(): void {
    this.scheduleTerm = { lstTerm: [], scheduleBA: [], scheduleBB: [], block: '', term: '', schedule: [] };
    if (this.offeredCourse) {
      if (this.verifyAsignature) {
        this.schedule = this.verifyAsignatureShow(this.schedule);
      }
      this.getLimitHours(this.schedule);
      this.assignTeacher(this.schedule);
      this.getStylesToSchedule(this.schedule);
      this.scheduleTerm.lstTerm = [];
      this.scheduleTerm.term = '';
      this.scheduleTerm.schedule = this.schedule;
      // validamos que tenga contenido
      this.schedulecontent = true;
    } else {
      // this.getAllTermsEnrollment();
      this.getAllTermsEnrollment();
    }
  }

  /**
   * [transformTime description]
   * @param  {string} time [hora en formato 24]
   * @return {number}      [return timestamp number]
   */
  transformTime(time: string): number {
    time = time.replace(/ /g, '');
    const timeArray = time.split(':');
    const timeStamp = parseInt(timeArray[0]) * 60 + parseInt(timeArray[1]);
    return timeStamp;
  }

  /**
   * Verifica las fechas de inicio y fin de las asignaturas
   * para mostrarlos o no al estudiante
   * @param schedule horario
   */
  verifyAsignatureShow(schedule: Schedule[]): Schedule[] {
    const present = new Date();
    let beforeClass = false;
    for (const iterator of schedule) {
      if (iterator.courses.length > 0) {
        for (let position = 0; position < iterator.courses.length; position++) {
          if (iterator.courses[position].module === 'MOD-1' && new Date(iterator.courses[position].dateStart) > present) {
            beforeClass = true;
          }
        }
        if (beforeClass) {
          // Solo eliminamos el MOD-2 para visualizar el MOD-1 al no comensar las clases
          for (let position = 0; position < iterator.courses.length; position++) {
            if (iterator.courses[position].module === 'MOD-2') {
              iterator.courses.splice(position, 1);
              position--;
            }
          }
        } else { // En el caso de estar en el período de clases se visualiza sólo el mod asignado
          for (let position = 0; position < iterator.courses.length; position++) {
            if (!(new Date(iterator.courses[position].dateStart) <= present && present <= new Date(iterator.courses[position].dateEnd))) {
              iterator.courses.splice(position, 1);
              position--;
            }
          }
        }
      }
    }
    return schedule;
  }

  /**
   * [getLimitHours description]
   * get hour timeStart and timeEnd
   */
  getLimitHours(schedule: Schedule[]): void {
    // Resetar la hora
    this.timeStart = '23:00';
    this.timeEnd = '01:00';
    for (const item of schedule) {
      for (const course of item.courses) {
        if (this.transformTime(this.timeStart) > this.transformTime(course.start)) { this.timeStart = course.start; }
        if (this.transformTime(this.timeEnd) < this.transformTime(course.end)) { this.timeEnd = course.end; }
      }
    }
    if (this.transformTime(this.timeStart) % 60 > 30) {
      this.timeStart = Math.trunc(this.transformTime(this.timeStart) / 60) + ':00';
    } else {
      this.timeStart = Math.trunc(this.transformTime(this.timeStart) / 60) + ':00';
    }
    if (this.transformTime(this.timeEnd) % 60 > 30) {
      this.timeEnd = (Math.trunc(this.transformTime(this.timeEnd) / 60) + 1) + ':00';
    } else {
      this.timeEnd = Math.trunc(this.transformTime(this.timeEnd) / 60) + ':30';
    }
    this.getArrayHours();
  }

  /**
   * [getArrayHours description]
   * get arrayHours for the list of hours
   */
  getArrayHours() {
    this.arrayHours = [];
    for (let time = this.transformTime(this.timeStart); time <= this.transformTime(this.timeEnd); time = time + 30) {
      const hora = (Math.trunc(time / 60) > 9) ? Math.trunc(time / 60) : '0' + Math.trunc(time / 60);
      const minutos = (time % 60 === 30) ? time % 60 : '00';
      this.arrayHours.push(hora + ':' + minutos);
    }
    this.heightDay = (this.arrayHours.length - 1) * 60 - 2 + 'px';
  }

  /**
   * Ontención de los períodos en los cuales el estudiante estuvo
   */
  getAllTermsEnrollment(): void {
    this.loading = true;
    this.enrollmentTermService.getAllTermsEnrollment()
      .subscribe(
        (terms) => {
          this.scheduleTerm.lstTerm = terms.filter(f => f >= '201710');
          this.loading = false;
          if (this.scheduleTerm.lstTerm.length > 0) {
            this.getSchedule(this.scheduleTerm.lstTerm[0]);
          } else {
            this.errorSchedule = true;
            this.errorMessage = 'no se pudo obtener las matrículas.';
          }
        },
        (error) => {
          this.errorSchedule = true;
          this.errorMessage = 'no se pudo obtener las matrículas.';
          this.loading = false;
        }
      );
  }

  /**
   * Obtener horario de estudiante por periodo
   * @param term PEriodo
   */
  getSchedule(term: string): void {
    this.errorSchedule = false;
    this.scheduleService.get(term)
      .subscribe(
        (data) => {
          this.scheduleTerm.scheduleBA = [];
          this.scheduleTerm.scheduleBB = [];
          /** Separar asignaturas del bloque A y B */
          data.schedule.forEach(schedule => {
            const scheduleBA = <Schedule>{
              day: schedule.day,
              courses: []
            };
            const scheduleBB = <Schedule>{
              day: schedule.day,
              courses: []
            };
            if (schedule.courses.length > 0) {
              scheduleBA.courses = schedule.courses.filter(f => f.module === 'MOD-1');
              scheduleBB.courses = schedule.courses.filter(f => f.module === 'MOD-2');
            }
            this.scheduleTerm.scheduleBA.push(scheduleBA);
            this.scheduleTerm.scheduleBB.push(scheduleBB);
          });
          this.scheduleTerm.term = data.term;
          this.changeModuleView('MOD-1'); // Asignar
          if (!this.validateScheduleActiveContent()) {
            this.changeModuleView('MOD-2');
          }
          this.loading = false;
        },
        (error) => {
          this.errorSchedule = true;
          this.errorMessage = 'No se encontró ningún horario.';
          this.loading = false;
        }
      );
  }

  /**
   * [getStylesToSchedule description]
   * get styles for li.single-event [heigth, top]
   */
  getStylesToSchedule(schedule: Schedule[]): void {
    for (const item of schedule) {
      if (item.courses.length > 0) {
        for (const course of item.courses) {
          course.height = ((this.transformTime(course.end) - this.transformTime(course.start))) * 2 + 'px';
          course.top = (((this.transformTime(course.start) - this.transformTime(this.timeStart))) * 2 - 2) + 'px';
        }
      }
    }
    this.loading = false;
  }

  /**
   * Establece que si un no ha sido asignado un profesor se le coloca un texto alternativo
   * @param schedule horario del estudiante
   */
  assignTeacher(schedule: Schedule[]): void {
    this.resumeAsignature = [];
    let initResume = true;
    for (const day of schedule) {
      for (const course of day.courses) {
        if (course.teacher === null || course.teacher === '') {
          course.teacher = 'Docente por asignar';
        }
        initResume = true;
        for (const resume of this.resumeAsignature) {
          if (resume.nrc === course.nrc) {
            initResume = false;
          }
        }
        if (initResume) {
          this.resumeAsignature.push(course);
        }
      }
    }
  }

  /**
   * Busca asignaturas dentro de los días para decidir si debe de mostrar el horario o no
   * si existe por lo menos una asignatura en un día se mostrara el horario
   * @param schedule Horario a validar
   */
  // tslint:disable-next-line:max-line-length
  validateSchedule(scheduleTerm: {lstTerm: string[]; scheduleBA: Schedule[]; scheduleBB: Schedule[]; block: string; term: string; schedule: Schedule[]}): void {
    this.schedulecontent = false;
    for (const iterator of scheduleTerm.scheduleBA) {
      if (iterator.courses.length > 0) {
        this.schedulecontent = true;
      }
    }
    for (const iterator of scheduleTerm.scheduleBB) {
      if (iterator.courses.length > 0) {
        this.schedulecontent = true;
      }
    }
    if (!this.schedulecontent) {
      this.errorSchedule = true;
      this.errorMessage = 'No se encontro horario disponible o no tiene horario en el periodo ' + this.scheduleTerm.term + '.';
    }
  }

  /**
   * Retorna true si tiene horario el bloque escogido
   */
  validateScheduleActiveContent(): boolean {
    let valid = false;
    for (const schedule of this.scheduleTerm.schedule) {
      if (schedule.courses.length > 0) {
        valid = true;
      }
    }
    return valid;
  }

  /**
   * Cambiar modulo a mostrar al estudiante
   * @param moduleM Codigo de modulo que se mostrara
   */
  changeModuleView(moduleM: string): void {
    this.scheduleTerm.schedule = [];
    switch (moduleM) {
      case 'MOD-1':
        this.scheduleTerm.schedule = this.scheduleTerm.scheduleBA;
        this.scheduleTerm.block = 'Bloque A';
        break;
      case 'MOD-2':
        this.scheduleTerm.schedule = this.scheduleTerm.scheduleBB;
        this.scheduleTerm.block = 'Bloque B';
        break;
      default:
        console.log('No existe modulo seleccionado.');
        break;
    }
    this.schedulecontent = this.validateScheduleActiveContent();
    console.log('schedule', this.scheduleTerm.schedule);
    if (this.scheduleTerm.schedule != null) {
      // this.scheduleTerm.schedule = this.verifyAsignatureShow(this.scheduleTerm.schedule);
      this.getLimitHours(this.scheduleTerm.schedule);
      this.assignTeacher(this.scheduleTerm.schedule);
      this.getStylesToSchedule(this.scheduleTerm.schedule);
      this.validateSchedule(this.scheduleTerm);
    } else {
      this.errorSchedule = true;
      this.errorMessage = 'No se cuenta con horarios disponibles.';
    }
  }

}
