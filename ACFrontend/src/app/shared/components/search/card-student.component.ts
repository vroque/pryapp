import { Component, Input, OnInit } from '@angular/core';
import { Student } from '../../services/academic/student';

@Component({
  selector: 'card-student',
  templateUrl: './card-student.component.html',
})
export class CardStudentComponent implements OnInit {

  @Input() students: Student[];
  @Input() url: string;

  ngOnInit(): void {
    this.url = this.url || 'student/';
  }

}
