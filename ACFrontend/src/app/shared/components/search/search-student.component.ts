import { Component, OnInit } from '@angular/core';
import { Student } from '../../services/academic/student';
import { StudentService } from '../../services/academic/student.service';

@Component({
  selector: 'search-student',
  templateUrl: './search-student.component.html',
  providers: [StudentService],
})
export class SearchStudentComponent implements OnInit {

  foundStudents: Student[];
  disableSearch = false;
  errorMessage: string;
  validateMessage: string;
  searchBy = 'dni';
  btnActive = true;
  studentSought: string;
  messageType = 'La busqueda por DNI solo aceptara un parametro, asegurece de ingresarlo';
  loading = false;
  constructor(
     private studentService: StudentService,
  ) {}

  ngOnInit(): void {}

  changeSearchBy(typeSearch: string): void {
    this.searchBy = typeSearch;
    if (typeSearch === 'dni') {
      this.btnActive = true;
      this.messageType = 'La busqueda por DNI solo debería de contener un parametro, asegurece de ingresarlo';
    } else {
      this.btnActive = false;
      this.messageType = 'Para un mejor resultado, asegurece de ingresar más de un parametro';
    }
  }

  /* Busca al estudiante por su DNI o nombre */
  searchStudent(studentData: string): void {
    this.disableSearch = true;
    this.errorMessage = null;
    this.loading = true;
    this.studentSought = this.removeSpaces(studentData);
    this.foundStudents = undefined;
    const studentDataArray = this.studentSought.split(' ');

    if (this.btnActive) {
      console.log('Busqueda por DNI');
      this.studentService.find(this.studentSought, 'student_id')
        .subscribe(
        (student) => {
          this.foundStudents = [];
          this.foundStudents.push(student);
          this.disableSearch = false;
        },
          (error) => {
            this.errorMessage = error as any;
            this.foundStudents = undefined;
            this.disableSearch = false;
            this.loading = false;
          },
          () => this.loading = false,
      );
    } else {
      console.log('Busqueda por Nombre');
      this.studentService.findStudents(studentDataArray)
        .subscribe(
          (student) => {
            this.foundStudents = student;
            this.disableSearch = false;
          },
          (error) => {
            this.errorMessage = error as any;
            this.foundStudents = undefined;
            this.disableSearch = false;
            this.loading = false;
          },
          () => this.loading = false,
      );
    }
  }

  removeSpaces(studentData: string): string {
    studentData = studentData.trim();
    const arrayStudentData: string[] = (studentData.trim()).split(' ');
    for (let x = 0; x < arrayStudentData.length; x++) {
      if (arrayStudentData[x] === '') {
        arrayStudentData.splice(x, 1);
        x--;
      }
    }
    return arrayStudentData.join(' ');
  }
}
