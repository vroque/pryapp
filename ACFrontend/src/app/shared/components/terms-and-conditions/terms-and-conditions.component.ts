import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { TermsAndConditions } from '../../services/terms-and-conditions/terms-and-conditions';

@Component({
  selector: 'terms',
  templateUrl: './terms-and-conditions.component.html',
})
export class TermsAndConditionsComponent {
  terms: TermsAndConditions[];

  constructor(private router: Router) {
    this.terms = (window as any).terms as TermsAndConditions[];
    this.goHome();
  }

  /**
   * Slice text
   * @param {string} text
   * @param {number} len
   * @returns {string}
   */
  getSlice(text: string, len: number): string {
    if (text.length <= len) {
      return text;
    }
    return text.slice(0, len).concat('...');
  }

  /**
   * Go Home
   */
  goHome(): void {
    if (!this.terms.length) {
      this.router.navigate(['']);
    }
  }
}
