import { Component, OnInit, Input } from '@angular/core';
import { SVGList } from './svg-list';
import { Svgicon } from './svg-icon';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'svg-icon',
  templateUrl: './svg-icon.component.html',
  providers: [ SVGList ]
})
export class SvgIconComponent implements OnInit {

  @Input() iconName = 'not-found';
  @Input() iconClass: string; // clase para el color
  @Input() strokeWidth: number; // grosor borde
  @Input() stroke = 'currentColor'; // color de borde por defecto dark
  @Input() fill = 'currentColor'; // color de fondo
  @Input() iconStyle = 'light'; // tipo de icono [solid, regular, light] default light
  @Input() fillOpacity: number; // 1 =>> contar con fondo o 0 =>>no
  path: Svgicon;
  pathCircle: Svgicon;
  circle = false;

  constructor(private svg: SVGList) { }

  ngOnInit() {
    this.definedStyle();
    this.definedIconName();
  }

  /**
   * Define el tipo de icono
   */
  definedStyle(): void {
    switch (this.iconStyle) {
      case 'regular': {
        this.strokeWidth = 10;
        this.fillOpacity = 0;
        break;
      }
      case 'solid': {
        this.strokeWidth = 10;
        this.fillOpacity = 1;
        this.stroke = 'currentColor';
        break;
      }
      default: {
        this.strokeWidth = 6;
        this.fillOpacity = 0;
        break;
      }
    }
  }

  /**
   * Define el ciculo al rededor del icono
   */
  definedCircle(): void {
    let components: string[] = [];
    components = this.path.name.split('-');
    if (components.filter(item => item === 'circle' && components.length > 1 ).length === 1) {
      this.pathCircle = this.svg.items.find(f => f.name === 'circle');
      this.circle = true;
    }
  }

  /**
   * define el nombre del icono
   */
  definedIconName(): void {
    if (this.svg.items.find(f => f.name === this.iconName) === undefined) {
      this.path = this.svg.items.find(f => f.name === 'not-found');
    } else {
      this.path = this.svg.items.find(f => f.name === this.iconName);
      this.definedCircle();
    }
  }

}
