import { Component, OnInit, Input } from '@angular/core';
import { SVGList } from './svg-list';
import { Svgicon } from './svg-icon';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'svg-img',
  templateUrl: './svg-img.component.html',
  providers: [SVGList]
})
export class SvgImgComponent implements OnInit {

  @Input() imageName = 'not-found';
  @Input() imageClass = '';
  @Input() width = '100%';
  path: Svgicon;

  constructor(private svg: SVGList) { }

  ngOnInit() {
    if (this.svg.items.find(f => f.name === this.imageName) === undefined) {
      this.path = this.svg.items.find(f => f.name === 'not-found');
    } else {
      this.path = this.svg.items.find(f => f.name === this.imageName);
    }
  }

}
