export class Svgicon {
  name: string;
  d: string;
  viewBox: string;
}
