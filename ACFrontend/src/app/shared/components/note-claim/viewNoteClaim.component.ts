import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OEANoteClaim } from '../../services/learning-assessment/OEANoteClaim';
import { OEANoteClaimService } from '../../services/learning-assessment/oeaNoteClaim.service';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { CampusService } from '../../services/institutional/campus.service';
import { Campus } from '../../services/academic/campus';


@Component({
  selector: 'oea-view-note-claim',
  templateUrl: './viewNoteClaim.component.html',
  providers: [OEANoteClaimService, BackofficeConfigService, CampusService]
})

export class OEAViewNoteClaimComponent implements OnInit {
  @Input() claimId: number;
  @Input() getFlow: boolean;
  @Output() endChargue = new EventEmitter();
  noteClaim: OEANoteClaim;
  allCampus: Campus[];
  status = 4;
  flow = [
    {
      Id: this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_REGISTERED,
      NameStatus: 'Registrado',
      NameOffice: ''
    },
    {
      Id: this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_CAS,
      NameStatus: 'Validando estado',
      NameOffice: 'Centro de atención al usuario'
    },
    {
      Id: this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_TEACHER,
      NameStatus: 'Pend. de informe',
      NameOffice: 'Docente'
    },
    {
      Id: this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_INVOLVED_AREA,
      NameStatus: 'Pend. de validación',
      NameOffice: ''
    },
    {
      Id: this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_RAU,
      NameStatus: 'En proceso',
      NameOffice: 'Registros Academicos'
    },
    {
      Id: 6,
      NameStatus: 'Finalizado',
      NameOffice: ''
    }
  ];
  constructor(private oeaNoteClaimService: OEANoteClaimService,
    private backofficeConfigService: BackofficeConfigService, private campusService: CampusService) { }
  ngOnInit(): void {
    this.getAllCampus();
  }

  /**
   * Obtener información del reclamo
   */
  getNoteClaimbyID(): void {
    this.oeaNoteClaimService.getById(this.claimId)
      .subscribe(
        (claim) => {
          this.noteClaim = claim;
          this.msgEndChargue(this.noteClaim, true, '');
        },
        (err) => {
          console.log(err);
          this.msgEndChargue(null, false, 'Error al obtener información del reclamo.');
        }
      );
  }
  /**
   * Retornar información a componente que invoca
   * @param data Información a retornar
   * @param success Si se cargo satisfactoriamente
   * @param msg Mensaje extra
   */
  msgEndChargue(data: OEANoteClaim, success: boolean, msg: string): void {
    this.endChargue.emit([data, success, msg]);
  }
  /**
   * Obtener nombre de tipo de reclamo
   * @param id Id de tupo
   */
  getNameType(id: string): string {
    return this.backofficeConfigService.OEA_TYPES_NOTE_CLAIM.filter(f => f.id === id)[0].name;
  }

  /**
 * Obtener todas las sedes
 */
  getAllCampus(): void {
    this.campusService.query()
      .subscribe(
        campus => {
          this.allCampus = campus;
          this.getNoteClaimbyID();
        },
        error => {
          console.log(error);
          this.msgEndChargue(null, false, 'Error al obtener sedes.');
        }
      );
  }

  /**
   * obtener nombre de sede por código
   * @param id código de sede
   */
  getNamebyCampus(id: string): string {
    return this.allCampus.filter(f => f.id === id)[0].name.replace('SEDE', '').replace('FILIAL', '');
  }

}
