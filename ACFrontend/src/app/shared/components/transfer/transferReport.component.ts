import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Request } from '../../services/atentioncenter/request';
import { Transfer } from '../../services/transfer/transfer';
import { TransferService } from '../../services/transfer/transfer.service';

@Component({
  selector: 'transfer-report',
  templateUrl: './transferReport.component.html',
  providers: [ TransferService, BackofficeConfigService ]
})
export class TransferReportComponent {
  errorMessage: string;
  transfer: Transfer;
  @Input()personId: number;
  @Input()term: string;
  @Input()tseq: number;
  @Input()seq: number;
  @Output()error = new EventEmitter();

  constructor(
    private config: BackofficeConfigService,
    private transferService: TransferService,
  ) { }

  ngOnChanges() {
    this.transferService.get(this.personId, this.term, this.tseq, this.seq)
      .subscribe(
        (transfer) => this.transfer = transfer,
        (error) =>  this.error.emit(
          `No se encontraron convalidaciones para el período ${this.term} - ${this.tseq} - ${this.seq}`,
        ),
      );
  }

}
