import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { RemainingCourseService } from '../../services/academic/remainingCourse.service';
import { Request } from '../../services/atentioncenter/request';
import { RequestService } from '../../services/atentioncenter/request.service';
import { Transfer } from '../../services/transfer/transfer';
import { TransferService } from '../../services/transfer/transfer.service';

@Component({
  selector: 'transfer-process',
  templateUrl: './transferProcess.component.html',
  providers: [TransferService, BackofficeConfigService, RemainingCourseService, RequestService],
})
export class TransferProcessComponent implements OnInit {
  courses: any[];
  error_message: string;
  mode = 'edit';
  select_transfer: Transfer;
  transfer_abstract: any[];
  transfer_course: any = {course: '', credits: 0, grade: 0};
  transfers: Transfer[];

  @Input()request: Request;
  @Output()error = new EventEmitter();
  @Output()onSave = new EventEmitter();

  constructor(
    private config: BackofficeConfigService,
    private transferService: TransferService,
    private courseService: RemainingCourseService,
    private requestService: RequestService,
  ) { }

  ngOnInit(): void {
    this.listRemainigCourses(this.request.person_id, this.request.program);
  }

  listRemainigCourses(personId: number, program: string): void {
    this.courseService.listByStudent(personId, program)
      .subscribe(
        (data) => this.courses = data.map((f) => {
          (f as any).transferible = false;
          (f as any).transfers = [];
          return f;
        }),
      (err) => this.error_message = 'no se pueden obtener las asignaturas',
      );
  }

  addTransferCourse(transfer: any, course: any) {
    course.transfers.push({
      course: transfer.course,
      credits: transfer.credits,
      grade: transfer.grade,
    });
    course.show_form = false;
    this.transfer_course = {course: '', credits: 0, grade: 0};
  }

  transferAbstract(): any {
    const transferCourses = this.courses
      .filter((f) => f.transferible && f.transfers.length > 0 )
      .map((f) => {
        return {id: f.id, name: f.name, credits: f.credits, transfers: f.transfers };
      });
    return transferCourses;

  }

  setTransferAbstract(event: any) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    const abstract = [{
      name: 'transfer_abstract',
      value: JSON.stringify(this.transferAbstract())},
    ];
    this.requestService.updateAbstract(this.request.id, abstract)
      .subscribe(
        (request) => this.request.description = request.description,
        (error) => this.error_message= 'no se pudo guardar',
        () => {
          target.disabled = false;
          this.composeTransferAbstract();
          this.mode = 'abstract';
        },
      );
  }

  composeTransferAbstract() {
    this.transfer_abstract = [];
    const opts = JSON.parse(this.request.description || '{}');
    const tabstract: any[] = JSON.parse(opts.transfer_abstract || '{}');
    if (tabstract) {
      for (const ta of tabstract) {
        console.log(ta);
        let rowspan = ta.transfers ? ta.transfers.length : 0;
        if (rowspan > 0) {
          for (const oc of ta.transfers) {
            this.transfer_abstract.push({
              ot_credits: oc.credits, ot_grade: oc.grade, ot_name: oc.course,
              rowspan, uc_code: ta.id, uc_credits: ta.credits, uc_name: ta.name,
            });
            rowspan = 0;
          }
        }
      }
    }
  }
}
