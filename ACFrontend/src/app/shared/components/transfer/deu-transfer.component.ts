import { Component, Input } from '@angular/core';

@Component({
  selector: 'transfer-deu',
  templateUrl: './deu-transfer.component.html',
})
export class DeuTransferComponent {
  @Input()transferAbstract: any;
  print() {
    window.print();
  }
}
