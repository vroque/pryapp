import { Component, EventEmitter, Input, OnInit,  Output } from '@angular/core';
import { Transfer } from '../../services/transfer/transfer';
import { TransferService } from '../../services/transfer/transfer.service';

@Component({
  selector: 'transfer-list',
  templateUrl: './transferList.component.html',
  providers: [TransferService]
})
export class TransferListComponent implements OnInit {
  error_message: string;
  transfers: Transfer[];
  select_transfer: Transfer;
  transfer_abstract: any[];
  @Input()personId: number;
  @Input()term: string;
  @Output()error = new EventEmitter();
  @Output()onSelect = new EventEmitter();

  constructor(
    private transferService: TransferService,
  ) { }

  ngOnInit(): void {
    if (this.term === 'all') {
      this.transferService.list(this.personId)
        .subscribe(
          (transfers) => this.transfers = transfers,
          (error) =>  this.error.emit(`No se encontraron convalidaciones`)
        );
    } else {
      this.transferService.byTerm(this.personId, this.term)
        .subscribe(
          (transfers) => this.transfers = transfers,
        (error) => this.error.emit(`No se encontraron convalidaciones para el período ${this.term}`)
        );
    }
  }

  selectTransfer(transfer: Transfer): void {
    this.select_transfer = transfer;
    this.onSelect.emit(transfer);
  }

  unSelectTransfer(): void {
    this.select_transfer = null;
    this.onSelect.emit(null);
  }

  compareTransfer(transfer1: Transfer, transfer2: Transfer): boolean {
    if (!transfer1 || !transfer2) {
      return false;
    }
    if (transfer1.term === transfer2.term
       && transfer1.seq === transfer2.seq
       && transfer1.tseq === transfer2.tseq) {
      return true;
    }
    return false;
  }
}
