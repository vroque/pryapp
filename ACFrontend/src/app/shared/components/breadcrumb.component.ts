import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component } from '@angular/core';
import { ConfigService } from '../../config/config.service';
import { Menu } from '../menu';

const SUBTITLE = 'Servicios virtuales';
const ICON_CLASS = 'fa fa-align-justify';

/**
 * Clase encargada de crear y modificar el breadcrumb de navegacion del sistema
 */
@Component({
  selector: 'cau-breadcrumb',
  templateUrl: './breadcrumb.component.html',
})
export class CAUBreadcrumbComponent {

  menuList: Menu[] = (window as any).choices || [] as Menu[];
  menuFinal: Menu[] = [];
  menus: string[] = [];
  root = '';
  title: string ;
  titleDefault: string;
  subtitle: string = SUBTITLE;

  constructor(
    private configService: ConfigService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
    this.router.events.subscribe((val) => this.getLinks(val));

    if (this.activatedRoute.component['name'] === 'FrontdeskComponent') {
      this.titleDefault = 'Portal del Estudiante';
      this.title = this.titleDefault;
    } else if (this.activatedRoute.component['name'] === 'ParentComponent') {
      this.titleDefault = 'Portal de Padres';
      this.title = this.titleDefault;
    } else {
      this.titleDefault = 'BackOffice';
      this.title = this.titleDefault;
    }
  }

  /**
   * Genera los enlaces para el breadcrub en una lista del
   * objeto Menu
   * @param val RouterEvent ruta actual
   */
  getLinks(val: any): void {
    this.menuFinal = [];
    // get link
    val.url = val.url || '';
    val.url = val.url.split('?')[0];
    this.menus = val.url.split('/');
    this.root = this.menus[1] || '';
    this.menus.shift();
    this.menus.shift();
    // buscamos coincidencia de primer nivel ->
    let i = 0;
    let parent: Menu;
    this.menus.forEach((menuUri) => {
      if (i === 0) {
        parent = this.appendMenu(menuUri, this.menuList, '');
      } else {
        parent = this.appendMenu(menuUri, parent.smenus, (parent as any).base);
      }
      i++;
    });
    this.menuFinal.reverse();
    this.setTitles();
  }

  /**
   * AGrega el menu en orden desde la ruta actual comparandolo
   * con la lista de menus traida desde el sistema
   * @param menuUri uri a evaliar
   * @param parent Menu padre
   * @param uriBase uri Base del padre
   */
  appendMenu(menuUri: string, parent: Menu[], uriBase: string): Menu {
    const exist: number = parent.filter((menu) => menu.uri === menuUri).length;
    if (exist > 0) {
      const menuItem: Menu =  parent.filter((menu) => menu.uri === menuUri)[0];
      (menuItem as any).base = `${uriBase}/${menuUri}`;
      this.menuFinal.push(menuItem);
      return menuItem;
    } else {
      const menuItem: Menu = {
        id: 0,
        icon: ICON_CLASS,
        base: `${uriBase}/${menuUri}`,
        uri: menuUri,
        name: menuUri,
        position: '',
        description: '',
        cardIcoStyle: '',
        cardStyle: '',
        order: 0,
        smenus: [],
        parent: null,
        module: null,
        moduleName: ''
      };
      this.menuFinal.push(menuItem);
      return menuItem;
    }
  }

  /**
   * Establece cual sera el título y subtitulo de la página
   */
  setTitles(): void {
    const last: number = this.menuFinal.length;
    if (last > 1) {
      this.title = this.menuFinal[last - 2].name;
      this.subtitle = this.menuFinal[last - 1].name;
    } else if (last === 1) {
      this.title = this.configService.toCapitalize(this.menuFinal[0].name.replace('-', ' '));
      this.subtitle = this.titleDefault;
    } else {
      this.title = this.titleDefault;
      this.subtitle = SUBTITLE;
    }

  }
}
