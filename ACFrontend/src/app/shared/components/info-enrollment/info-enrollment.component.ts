import { Component, Input, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Student } from '../../services/academic/student';
import { StudentFactory } from '../../services/academic/student.factory';
import { BackofficeConfigService } from '../../../backoffice/config/config.service';
import { Requeriment } from '../../services/admision/requeriment';

import { EconomicStatus } from '../../services/accounting/economicStatus';
import { EconomicStatusService } from '../../services/accounting/economicStatus.service';
import { EnrollmentTermService } from '../../services/academic/enrollmentTerm.service';

import { Course } from '../../services/academic/course';
import { ReportCardService } from '../../services/academic/reportCard.service';

import { ACTerm } from '../../../shared/services/atentioncenter/term';
import { ACTermService } from '../../../shared/services/atentioncenter/term.service';
import { PlatformLocation } from '@angular/common';

declare var $: any;

@Component({
  providers: [BackofficeConfigService, EconomicStatusService, EnrollmentTermService, ReportCardService, ACTermService],
  selector: 'info-enrollment',
  templateUrl: './info-enrollment.component.html',
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        height: '0px',
      })),
      transition('void <=> *', animate(500)),
    ]),
  ]
})
export class InfoEnrollmentComponent implements OnInit {

  show = false;
  student: Student = StudentFactory.getInstance().get();
  @Input() requeriments: Requeriment[] = [];
  debts: EconomicStatus[] = [];
  debtsActives = false;
  debtsActivesCount = 0;
  terms: string[] = []; // periodos del estudiante
  term: ACTerm; // periodo actual
  errorMessage: string;
  courses: Course[] = []; // horario del estudiante para su último periodo
  modalType = 'none';

  constructor(
    private config: BackofficeConfigService,
    private reportCardService: ReportCardService,
    private aCTermService: ACTermService,
    private platformLocation: PlatformLocation,
    private economicStatusService: EconomicStatusService) {
      this.platformLocation.onPopState(() => $('.modal').modal('hide'));
    }

  ngOnInit() {
    // STATE_POSTULANT = 1; // "POSTULANTE";
    // STATE_ENROLMENT = 2; // "INGRESANTE";
    // STATE_STUDENT = 3; // "MATRICULADO";
    if (this.student.profile.status === this.config.STATE_STUDENT) {
      this.getTermCurrentEnrollment();
    }
  }

  /**
   * Obtiene todas sus deudas del estudiante
   */
  getDebts(): void {
    this.economicStatusService.getGroupedDebtsPayments(this.student.profile.term_catalg, this.student.person_id, 'debts')
      .subscribe(
        (data) => {
          this.debts = data;
          this.activesDebts();
        },
        (err) => {
          console.log(err);
          this.errorMessage = 'Error al obtener Deudas';
        }
      );
  }

  /**
   * Retorna true si tiene deudas pendientes o vencidas (activas)
   * @param debts deudas totales
   */
  activesDebts(): void {
    this.debtsActivesCount = 0;
    const currentDate = new Date();
    for (const debt of this.debts) {
      if (new Date(debt.paymen_date) <= currentDate) {
        this.debtsActivesCount++;
        this.debtsActives = true;
      }
    }

    setInterval(
      () => {
        this.show = true;
      }, 3000);
  }

  /**
   * Obtener el ultimo periodo para su matricula
   */
  getTermCurrentEnrollment(): void {
    this.aCTermService.getTermCurrentEnrollment(this.student.profile.campus.id, this.student.profile.department).subscribe(
        (term) => {
          this.term = term;
          this.getDebts();
          this.getReportCard(this.term.term);
        },
        (error) => {
          console.log('error => ', error);
        }
      );
  }

  /**
   * Obtiene la boleta de notas del período
   * @param term período del que se requiere la boleta
   */
  getReportCard(term: string) {
    this.reportCardService.get(term)
      .subscribe(
        (courses) => {
          this.courses = courses;
        },
        (error) => {
          console.log(error);
          this.errorMessage = 'No se pudieron obtener las asignaturas.';
        }
      );

  }

  /**
   * Comprueba que todo este ok para su inicio de clases
   */
  allOkToClass(): boolean {
    if (this.courses.length > 0 && !this.debtsActives) {
      return true;
    }
    return false;
  }

  /**
   * Designa el tipo y abre el modal
   */
  openModal(view: boolean, type: string): void {
    if (view) {
      this.modalType = type;
      $('#infoEnrollmentModal').modal('show');
    }
  }

}
