import { Component, OnInit, Input } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Poll } from '../../services/poll/poll';
import { PollService } from '../../services/poll/poll.service';
import { PollUser } from '../../services/poll/poll-user';
import { PollUserService } from '../../services/poll/poll-user.service';

import { Request } from '../../services/atentioncenter/request';
import { BlockableUI } from 'primeng/primeng';
import { PollChoice } from '../../services/poll/poll-choice';
import { PollChoiceDetail } from '../../services/poll/poll-choice-detail';
declare var $: any;

@Component({
  providers: [PollService, PollUserService],
  selector: 'poll-component',
  templateUrl: './poll.component.html',
})
export class PollComponent implements OnInit {
  @Input() request: Request;
  poll: Poll;
  mustPoll: boolean;
  options: any[];
  optionSelected: PollChoice = new PollChoice();
  send = false;
  selected = false;
  loading = true;

  constructor(
    private platformLocation: PlatformLocation,
    private pollService: PollService,
    private pollUserService: PollUserService) {
    this.platformLocation.onPopState(() => $('.modal').modal('hide'));
  }

  ngOnInit() {
    this.validateMustPollRequest(this.request);
  }

  validateMustPollRequest(data: Request) {
    this.loading = true;
    this.pollUserService.validateMustPollRequest(data.id)
      .subscribe(
        (response) => this.mustPoll = response,
        (err) => this.loading = false,
        () => {
          this.loading = false;
          if (this.mustPoll) {
            this.getPoll(data);
          }
        }
      );
  }

  getPoll(data: Request) {
    this.loading = true;
    this.pollService.getPoll(data.state)
      .subscribe(
        (response) => {
          this.poll = response;
          this.loading = false;
          if (this.poll != null) {
            $('#poll-modal').modal('show');
          }
        },
        (err) => this.loading = false
      );
  }
  /**
   * Definir la opción seleccionada
   * @param option objeto seleccionado
   */
  selectOption(option: PollChoice): void {
    this.selected = true;
    this.poll.questions.forEach(question => {
      question.choices.forEach(choice => {
        choice.selected = false;
      });
    });
    option.selected = true;
    this.optionSelected = option;
  }

  /**
   * Envía la opción seleccionada
   */
  sendPoll(): void {
    if (this.selected) {
      const dataPollUSer: PollUser = new PollUser();
      dataPollUSer.pollId = this.poll.id;
      dataPollUSer.pidm = this.request.person_id;
      dataPollUSer.requestId = this.request.id;
      dataPollUSer.omitted = false;
      const dataPollChoiceDetail: PollChoiceDetail[] = [];
      this.poll.questions.forEach(dataQuestions => {
        dataQuestions.choices.forEach(dataChoices => {
          const dataChoicesSelected: PollChoiceDetail = new PollChoiceDetail();
          dataChoicesSelected.pollChoiceId = dataChoices.id;
          dataChoicesSelected.pidm = this.request.person_id;
          dataChoicesSelected.requestId = this.request.id;
          if (!dataQuestions.isComment && dataChoices.selected) {
            dataPollChoiceDetail.push(dataChoicesSelected);
          } else if (dataQuestions.isComment) {
            dataChoicesSelected.comment = dataQuestions.comment;
            dataPollChoiceDetail.push(dataChoicesSelected);
          }
        });
      });
      this.loading = true;
      this.pollUserService.savePollUser(dataPollUSer, dataPollChoiceDetail)
        .subscribe(
          (data) => data,
          (err) => {
            this.loading = false;
            this.send = true;
            setTimeout(() => {
              $('#poll-modal').modal('hide');
            }, 3000);
          },
          () => {
            this.loading = false;
            this.send = true;
            setTimeout(() => {
              $('#poll-modal').modal('hide');
            }, 3000);
          }
        );
    }
  }

  /**
   * Omite la calificación
   */
  omitPoll(): void {
    const dataPollUSer: PollUser = new PollUser();
    dataPollUSer.pollId = this.poll.id;
    dataPollUSer.pidm = this.request.person_id;
    dataPollUSer.requestId = this.request.id;
    dataPollUSer.omitted = true;
    const dataPollChoiceDetail: PollChoiceDetail[] = [];
    this.loading = true;
    this.pollUserService.savePollUser(dataPollUSer, dataPollChoiceDetail)
      .subscribe(
        (data) => data,
        (err) => {
          this.loading = false;
          $('#poll-modal').modal('hide');
        },
        () => {
          this.loading = false;
          $('#poll-modal').modal('hide');
        }
      );
  }
}
