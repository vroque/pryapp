import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({name: 'loopFilter'})
@Injectable()
export class LoopFilterPipe implements PipeTransform {
  transform(items: any[], value: string, field: string[]): any[] {
    if (!items) { return []; }
    if (value) {
      if (field !== undefined) {
        return items.filter((data) => {
          for (const property in data) {
            if (data.hasOwnProperty(property) && data[property] != null) {
              let verify = 0;
              for (const key of field) {
                if (data[key].toString().toLowerCase().indexOf(value.toString().toLowerCase()) > -1) {
                  verify++;
                }
              }
              if (verify > 0) {
                return true;
              } else {
                return false;
              }
            }
          }
          return false;
        });
      } else {
        return items.filter((data) => {
          for (const property in data) {
            if (data.hasOwnProperty(property) && data[property] != null) {
              if (data[property].toString().toLowerCase().indexOf(value.toString().toLowerCase()) > -1) {
                return true;
              }
            }
          }
          return false;
        });
      }
    } else { return items; }
  }
}
