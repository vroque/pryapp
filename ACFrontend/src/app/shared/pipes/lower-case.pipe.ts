import { Pipe, PipeTransform } from '@angular/core';
import { invalidPipeArgumentError } from './invalid-argument-error';
import { CaseFunctionSpecial } from './case-function-special';

/**
 * Transforms text to lowerCase.
 */
@Pipe({ name: 'lowerCase' })
export class LowerCasePipe implements PipeTransform {

  constructor(private caseFunction: CaseFunctionSpecial) { }

  transform(value: string): string {
    if (!value) { return value; }
    if (typeof value !== 'string') {
      throw invalidPipeArgumentError(LowerCasePipe, value);
    }
    return this.caseFunction.toLowercaseAccent(value);
  }
}
