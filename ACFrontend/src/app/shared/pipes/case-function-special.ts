import { Injectable } from '@angular/core';

@Injectable()
export class CaseFunctionSpecial {

  private uppercases = 'ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛÑÇ';
  private lowercases = 'ãàáäâèéëêìíïîòóöôùúüûñç';

  /**
   * Helper method to transform a single word to titleCase.
   */
  titleCaseWord(word: string) {
    if (!word) { return word; }
    return this.toUppercaseAccent(word[0]) + this.toLowercaseAccent(word.substr(1));
  }
  /**
   * Helper method to transform a single word to uppercase with accent mark.
   */
  toUppercaseAccent(word: string): string {
    for (let i = 0; i < this.lowercases.length; i++) {
      word = word.replace(this.lowercases.charAt(i), this.uppercases.charAt(i));
    }
    return word.toUpperCase();
  }
  /**
   * Helper method to transform a single word to lowercase with accent mark.
   */
  toLowercaseAccent(word: string): string {
    for (let i = 0; i < this.uppercases.length; i++) {
      word = word.replace(this.uppercases.charAt(i), this.lowercases.charAt(i));
    }
    return word.toLowerCase();
  }
}
