import { Pipe, PipeTransform } from '@angular/core';
import { invalidPipeArgumentError } from './invalid-argument-error';

/**
 * Transforms hours of 24 to hours of 12.
 */

@Pipe({ name: 'timeTo12' })
export class TimeTo12Pipe implements PipeTransform {
  transform(value: string): string {
    if (!value) { return value; }
    if (typeof value !== 'string') {
      throw invalidPipeArgumentError(TimeTo12Pipe, value);
    }
    const components: string[] = value.split(':');
    if (parseInt(components[0], 10) >= 12) {
      if (parseInt(components[0], 10) === 12) {
        return '12:' + components[1] + ' pm';
      } else {
        return (parseInt(components[0], 10) - 12) + ':' + components[1] + ' pm';
      }
    } else {
      return parseInt(components[0], 10) + ':' + components[1] + ' am';
    }
  }
}
