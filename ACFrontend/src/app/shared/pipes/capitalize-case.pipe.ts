import { Pipe, PipeTransform } from '@angular/core';
import { invalidPipeArgumentError } from './invalid-argument-error';
import { CaseFunctionSpecial } from './case-function-special';

/**
 * Transforms text to capitalizeCase.
 */
@Pipe({ name: 'capitalizeCase' })
export class CapitalizeCasePipe implements PipeTransform {

  constructor(private caseFunctionSpecial: CaseFunctionSpecial) { }

  transform(value: string): string {
    if (!value) { return value; }
    if (typeof value !== 'string') {
      throw invalidPipeArgumentError(CapitalizeCasePipe, value);
    }
    return this.caseFunctionSpecial.toUppercaseAccent(value[0]) + this.caseFunctionSpecial.toLowercaseAccent(value.substr(1));
  }
}
