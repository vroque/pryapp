import { Pipe, PipeTransform } from '@angular/core';
import { invalidPipeArgumentError } from './invalid-argument-error';
import { CaseFunctionSpecial } from './case-function-special';

/**
 * Transforms text to titleCase.
 */
@Pipe({ name: 'titleCase' })
export class TitleCasePipe implements PipeTransform {

  constructor(private caseFunction: CaseFunctionSpecial) { }

  transform(value: string): string {
    if (!value) { return value; }
    if (typeof value !== 'string') {
      throw invalidPipeArgumentError(TitleCasePipe, value);
    }
    return value.split(' ').map((word) => this.caseFunction.titleCaseWord(word)).join(' ');
  }
}
