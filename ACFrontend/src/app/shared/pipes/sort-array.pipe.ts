import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'sort-array-shortname'
})

export class SortPipe implements PipeTransform {

    transform(array: any[], field: string): any[] {
        array.sort((a: any) => {
          if (!a.shortname.localeCompare(a.shortname)) 
          {
            return a.shortname.localeCompare(a.shortname);
          }
          return a.shortname.localeCompare(a.shortname);
        });
        return array;
      }
}