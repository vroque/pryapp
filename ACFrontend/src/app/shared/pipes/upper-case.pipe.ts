import { Pipe, PipeTransform } from '@angular/core';
import { invalidPipeArgumentError } from './invalid-argument-error';
import { CaseFunctionSpecial } from './case-function-special';

/**
 * Transforms text to upperCase.
 */
@Pipe({ name: 'upperCase' })
export class UpperCasePipe implements PipeTransform {

  constructor(private caseFunction: CaseFunctionSpecial) { }

  transform(value: string): string {
    if (!value) { return value; }
    if (typeof value !== 'string') {
      throw invalidPipeArgumentError(UpperCasePipe, value);
    }
    return this.caseFunction.toUppercaseAccent(value);
  }
}
