import { Injectable } from '@angular/core';
import { distanceInWordsToNow } from 'date-fns';
import langEs = require('date-fns/locale/es');

@Injectable()
export class MomentService {

  /**
   * Tiempo relativo a partir de una fecha
   * p.e.: fromNow(new Date()) retorna un string: "hace X días/meses/etc"
   * @param date
   */
  fromNow(date: string): string {
    return distanceInWordsToNow(date, {locale: langEs, addSuffix: true});
  }
}
