export class Menu {
  constructor(
    public id: number,
    public icon: string,
    public base: string,
    public uri: string,
    public name: string,
    public position: string,
    public description: string,
    public cardStyle: string,
    public cardIcoStyle: string,
    public order: number,
    public smenus: Menu[],
    public parent: number,
    public module: number,
    public moduleName: string,
  ) { }
}
