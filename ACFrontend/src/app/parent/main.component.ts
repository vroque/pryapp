import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { Menu } from '../shared/menu';

declare let $: any;

@Component({
    selector: 'pa-main',
    templateUrl: './main.component.html',
    providers: [ConfigService]
})
export class MainComponent implements OnInit {

    base_uri: string;
    menus: Menu[];

    constructor(private configService: ConfigService) {
        this.base_uri = (<any>window).url_base || '';
        this.menus = (<any>window).choices || [];
    }

    ngOnInit(): void {
    }
}
