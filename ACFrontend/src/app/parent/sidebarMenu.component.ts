import { Component, OnInit } from '@angular/core';
import { Menu } from '../shared/menu';
import { MenuService } from '../shared/menu.service';
import '../rxjs-operators';

@Component({
  selector: 'pt-menu',
  templateUrl: './sidebarMenu.component.html',
  providers: [MenuService],
})

export class SidebarMenuComponent implements OnInit {

  error_message: string;
  menus: Menu[];

  constructor (private menuService: MenuService) {}

  ngOnInit(): void {
    this.query();
  }

  query() {
    this.menus = <Menu[]>JSON.parse(JSON.stringify((<any>window).choices))
      .filter(f => f.position === 'all' || f.position === 'side' );
    for (const menu of this.menus) {
      menu.smenus = menu.smenus.filter(f => f.position === 'all' || f.position === 'side') || [];
    }
  }
}
