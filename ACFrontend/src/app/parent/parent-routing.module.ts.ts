import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// guards
import { HasAccessFullGuard } from '../shared/guards/has-access-full.guard';

import { ReportCardComponent } from '../frontdesk/orientation/report-card.component';
import { Page404Component } from '../shared/components/page404.component';
import { SurveysComponent } from '../shared/components/surveys/surveys.component';
import { TermsAndConditionsComponent } from '../shared/components/terms-and-conditions/terms-and-conditions.component';
import { AssistanceComponent } from '../shared/components/assistance/assistance.component';
import { MainComponent } from './main.component';
import { ScheduleComponent } from '../shared/components/schedule/schedule.component';
import { EconomicStatusComponent } from '../frontdesk/cash-box/economic-status.component';

const routes: Routes = [
  { path: '', redirectTo: '/parent', pathMatch: 'full' },
  { path: 'parent', component: MainComponent },
  { canActivate: [HasAccessFullGuard], path: 'parent/boleta-de-notas', component: ReportCardComponent },
  { canActivate: [HasAccessFullGuard], path: 'parent/inasistencias', component: AssistanceComponent },
  { canActivate: [HasAccessFullGuard], path: 'parent/horario-de-asignaturas', component: ScheduleComponent },
  { canActivate: [HasAccessFullGuard], path: 'parent/estado-economico', component: EconomicStatusComponent },
  { canActivate: [HasAccessFullGuard], path: 'parent/terms', component: TermsAndConditionsComponent },
  { canActivate: [HasAccessFullGuard], path: 'parent/surveys', component: SurveysComponent },

  // page 404
  { path: '404', component: Page404Component },
  { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class ParentRoutingModule { }
