// dd
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule, JsonpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { ParentRoutingModule } from './parent-routing.module.ts';

// components
import { ParentComponent } from './parent.component';
import { MainComponent } from './main.component';

// guards
import { HasAccessFullGuard } from '../shared/guards/has-access-full.guard';

// Google Analytics
import { Angulartics2Module } from 'angulartics2';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';

// shared componentents
import { ReportCardComponent } from '../frontdesk/orientation/report-card.component';
import { AssistanceComponent } from '../shared/components/assistance/assistance.component';
import { ScheduleComponent } from '../shared/components/schedule/schedule.component';
import { LoadingComponent } from '../shared/components/loading.component';
import { CAUBreadcrumbComponent } from '../shared/components/breadcrumb.component';
import { SidebarMenuComponent } from './sidebarMenu.component';
import { TermsAndConditionsComponent } from '../shared/components/terms-and-conditions/terms-and-conditions.component';
import { SurveysComponent } from '../shared/components/surveys/surveys.component';
import { RestrictionDebtByTermComponent } from '../frontdesk/notifications/restriction-debt-by-term.component';
import { CalloutComponent } from '../shared/components/callout/callout.component';
import { EconomicStatusComponent } from '../frontdesk/cash-box/economic-status.component';
import { PaymentMethodsComponent } from '../frontdesk/cash-box/payment-methods.component';
import { BanksInfoComponent } from '../frontdesk/cash-box/banks-info/banks-info.component';

import { RedirectComponent } from '../shared/components/redirect.component';
import { Page404Component } from '../shared/components/page404.component';

// pipes
import { CapitalizeCasePipe } from '../shared/pipes/capitalize-case.pipe';
import { CaseFunctionSpecial } from '../shared/pipes/case-function-special';
import { JsonParsePipe } from '../shared/pipes/json-parse.pipe';
import { LoopFilterPipe } from '../shared/pipes/loopfilter.pipe';
import { LowerCasePipe } from '../shared/pipes/lower-case.pipe';
import { TimeTo12Pipe } from '../shared/pipes/time-to-12.pipe';
import { TitleCasePipe } from '../shared/pipes/title-case.pipe';
import { UpperCasePipe } from '../shared/pipes/upper-case.pipe';

// services
import { ConfigService } from '../config/config.service';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        HttpClientModule,
        JsonpModule,
        ParentRoutingModule,
        BrowserAnimationsModule,
        Angulartics2Module.forRoot([Angulartics2GoogleAnalytics], { pageTracking: { clearQueryParams: true } })
    ],
    declarations: [
        ParentComponent,
        MainComponent,
        ReportCardComponent,
        AssistanceComponent,
        ScheduleComponent,
        RedirectComponent,
        LoadingComponent,
        Page404Component,
        CAUBreadcrumbComponent,
        SidebarMenuComponent,
        TermsAndConditionsComponent,
        SurveysComponent,
        RestrictionDebtByTermComponent,
        CalloutComponent,
        EconomicStatusComponent,
        PaymentMethodsComponent,
        BanksInfoComponent,
        // pipes
        JsonParsePipe,
        LoopFilterPipe,
        UpperCasePipe,
        LowerCasePipe,
        TitleCasePipe,
        CapitalizeCasePipe,
        TimeTo12Pipe
    ],
    providers: [
        HasAccessFullGuard,
        ConfigService,
        CaseFunctionSpecial
    ],
    bootstrap: [ParentComponent, SidebarMenuComponent, CAUBreadcrumbComponent]
})
export class ParentModule { }
