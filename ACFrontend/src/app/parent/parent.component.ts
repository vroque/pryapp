import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';

@Component({
  selector: 'parent',
  template: '<router-outlet></router-outlet>'
})
export class ParentComponent {
  constructor(angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics, private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        ga('set', 'page', event.urlAfterRedirects);
      }
    });
  }
}
