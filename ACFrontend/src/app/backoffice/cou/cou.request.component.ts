import { AcademicProfile } from '../../shared/services/academic/academic.profile';
import { ActivatedRoute, Params } from '@angular/router';
import { BackofficeConfigService } from '../config/config.service';
import { Component, OnInit } from '@angular/core';
import { DocumentState } from '../../shared/services/atentioncenter/document.state';
import { DocumentStateService } from '../../shared/services/atentioncenter/document.state.service';
import { GenerateDocumentService } from '../../shared/services/atentioncenter/generate.document.service';
import { MailService } from '../../shared/services/mail/mail.service';
import { Request } from '../../shared/services/atentioncenter/request';
import { RequestDescriptionService } from '../../shared/services/atentioncenter/request.description.service';
import { RequestLog } from '../../shared/services/atentioncenter/request.log';
import { RequestLogService } from '../../shared/services/atentioncenter/request.log.service';
import { RequestService } from '../../shared/services/atentioncenter/request.service';
import { SignatoriesService } from '../../shared/services/atentioncenter/signatories.service';
import { StudentService } from '../../shared/services/academic/student.service';
import { Transfer } from '../../shared/services/transfer/transfer';

@Component({
  selector: 'request-list',
  templateUrl: './cou.request.component.html',
  providers: [
    StudentService,
    RequestService,
    RequestLogService,
    DocumentStateService,
    SignatoriesService,
    GenerateDocumentService,
    MailService,
    RequestDescriptionService,
    BackofficeConfigService,
  ],
})
export class COURequestComponent implements OnInit {

  conva: any;
  derivate_message = 'Para su atención.';
  description: any;
  doc_status = 'pre'; // pre, on, err
  error_message: string;
  flow: DocumentState[];
  ideal_flow: DocumentState[];
  logs: RequestLog[];
  request: Request;
  signatories: string[];
  student_profile: AcademicProfile;
  select_transfer: Transfer;
  see_res_btn = false;
  transfer_abstract: any[];
  data: any = {
    error: false,
    sended: false,
    sending: false,
  };
  mail: any = {sending: false, sended: false, error: false};
  load: boolean;
  constructor(
    private _route: ActivatedRoute,
    private config: BackofficeConfigService,
    private documentStateService: DocumentStateService,
    private generateDocumentService: GenerateDocumentService,
    private mailService: MailService,
    private requestDescriptionService: RequestDescriptionService,
    private requestLogService: RequestLogService,
    private requestService: RequestService,
    private signatoriesService: SignatoriesService,
    private studentService: StudentService
  ) { }

  ngOnInit() {
    this.request = null;
    this.flow = [];
    this.ideal_flow = [];
    this.logs = [];

    this._route.params.forEach((params: Params) => {
      const requestId: string = (params as any).request;
      this.getRequest(requestId);
      this.getRequestLogs(requestId);
    });
  }

  getRequest(requestId: string) {
    this.requestService.get(requestId)
      .subscribe(
        (request) => this.request = request,
        (error) =>  this.error_message = error,
        () => this.setRequestExtra(),
      );
  }

  /**
   * Carga las opciones extra de la solicitud de convalidaciones
   * para su uso en este componente
   */
  setRequestExtra() {
    this.request.document_uri = this.request.document_uri || [];
    this.description = JSON.parse(this.request.description) || {};
    this.conva = this.description.convalidation || {};
    this.getDocumentFlow(this.request.document_id);
    this.getSignatories(this.request.document_id, this.request.program);
    this.getProfile(
      this.request.divs,
      this.request.program,
      this.request.person_id);
    (this.request as any).opts = JSON.parse(this.request.description);
    (this.request as any).conva_type = JSON.parse(this.request.description).conva_type || '';
    (this.request as any).conva_resolution = JSON.parse(this.request.description).conva_resolution || '';

    this.transfer_abstract = [];
    const tabstract: any[] = JSON.parse((this.request as any).opts.transfer_abstract || '{}');
    if (tabstract) {
      for (const ta of tabstract) {
        console.log(ta);
        let rowspan = ta.transfers ? ta.transfers.length : 0;
        if (rowspan > 0) {
          for (const oc of ta.transfers) {
            this.transfer_abstract.push({
              rowspan, uc_code: ta.id, uc_name: ta.name,
              ot_name: oc.course, ot_grade: oc.grade, ot_credits: oc.credits,
            });
            rowspan = 0;
          }
        }
      }
    }
  }

  updateLogs(event: string) {
    this.getRequestLogs(String(this.request.id));
  }

  getRequestLogs(requestId: string) {
    this.requestLogService.query(requestId)
      .subscribe(
        (logs) => this.logs = logs,
        (error) =>  this.error_message = error,
        () => console.log('logs -> ', this.logs),
      );
  }

  getDocumentFlow(documentId: number) {
    this.documentStateService.query(documentId)
      .subscribe(
        (flow) => this.flow = flow,
        (error) =>  this.error_message = error,
        () => this.getIdealFlow(this.flow),
      );
  }

  getIdealFlow(flow: DocumentState[]): any {
    this.ideal_flow = flow.filter(state => { return (state.order !== -1); } );
  }

  getLogTrace(state: number): RequestLog {
    for (let i = 0; i < this.logs.length; i++) {
      if (this.logs[i].old_state === state) {
        return this.logs[i];
      }
    }
    return null;
  }

  getSignatories(documentId: number, program: string) {
    const data: any = { program: program };
    this.signatoriesService.query(documentId, data)
      .subscribe(
        (signatories) => this.signatories = signatories,
        (error) =>  this.error_message = <any>error,
        () => { this.getIdealFlow(this.flow); }
      );
  }

  /**
   * Obtiene el perfil del estudiante
   */
  getProfile(div: string, program: string, personId: number) {
    this.studentService.find(String(personId), 'person_id')
      .subscribe(
        (response) => {
          console.log('student- > ', response );
          for (const profile of response.profiles) {
            if (profile.program.id === program && profile.div === div) {
              this.student_profile = profile;
            }
          }
        },
        (error) =>  this.error_message = <any>error,
      );
  }

  docSt(id: number): any {
    return this.config.getDocumentState(id);
  }

  /**
   * Marca la solicitud como entregada l
   */
  deliveryRequest(requestId: string) {
    this.requestService.save(requestId, {})
      .subscribe(
        (request) => {
          this.request = request;
          this.getRequestLogs(String(request.id));
        },
        (error) =>  this.error_message = error,
      );
  }

  derivateRequest(requestId: string, message: string) {
    this.requestService.update(requestId, {message, type: 'derive' })
      .subscribe(
        (request) => {
          this.request = request;
          this.getRequestLogs(String(request.id));
        },
        (error) =>  this.error_message = error,
        () => {
          this.getRequestLogs(String(this.request.id));
          this.setRequestExtra();
        },
      );
  }

  cancelRequest(requestId: string, message: string) {
    this.load = true;
    this.requestService.update(requestId, {message, type: 'refuse' })
      .subscribe(
        (request) => {
          this.request = request;
          this.load = false;
        },
        (error) => {
          this.error_message = error;
          this.load = false;
        },
        () => {
          this.getRequestLogs(String(this.request.id));
          this.setRequestExtra();
        },
      );
  }

  setDebt(requestId: string, message: string) {
    this.load = true;
    this.requestService.update(requestId, {message: message, type: 'debt' })
      .subscribe(
        (request) => {
          this.request = request;
          this.getRequestLogs(String(request.id));
          this.load = false;
        },
        (error) =>  {
          this.error_message = <any>error;
          this.load = false;
        },
        () => {
          this.getRequestLogs(String(this.request.id));
          this.load = false;
        }
      );
  }

  /**
   * Terminar flujo de la solicitud
   */
  finishRequest(requestId: string) {
    this.requestService.save(requestId, {})
      .subscribe(
        (request) => {
          this.request = request;
          this.getRequestLogs(String(request.id));
        },
        (error) =>  this.error_message = error,
        () => {
          this.getRequestLogs(String(this.request.id));
        },
      );
  }

  /**
   * Guardar la informacion de institucion en el detalle de la solicitud.
   */
  saveConvalidationData(event: Event, institution: string, program: string, campus: string ) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.description.convalidation = this.conva;
    this.requestDescriptionService.update(this.request.id, this.conva)
      .subscribe(
        (request) => target.disabled = false,
        (error) => {this.data.error = true; this.data.sending = false; },
        () => {this.data.sended = true; this.data.sending = false; },
      );
  }

  /*
   * Enviar el parametro conva_type a la descripción de la solicitud
   */
  setConvaType(type: string) {
    const data: any[] = [{name: 'conva_type', value: type}];
    this.requestService.updateAbstract(this.request.id, data)
      .subscribe(
        (response) => console.log(response),
        (error) =>  this.error_message = <any>error
      );
  }

  setConvaResolution(res: string) {
    const data: any[] = [{name: 'conva_resolution', value: res}];
    this.requestService.updateAbstract(this.request.id, data)
      .subscribe(
        (data2) => (this.request as any).conva_resolution = res,
        (error) =>  this.error_message = error,
        () => this.see_res_btn = false,
      );
  }

  /*
   * enviar correo informando que se acabo la convalidación.
  */
  sendEndTransferMail(event: Event) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.mail.sending = true;
    this.mailService.requestDocument(this.request.id)
      .subscribe(
        (data) => console.log(data),
        (error) => {
          this.mail.sending = false;
          this.mail.error = true;
        }, () => {
          target.disabled = true;
          this.mail.sending = false;
          this.mail.sended = true;
        },
      );
  }

  setTransfer(transfer: Transfer) {
    if (transfer) {
      // guardar los datos en la BD
      const data: any[] = [
        {name: 'term', value: this.request.term},
        {name: 'tseq', value: transfer.tseq},
        {name: 'seq', value: transfer.seq},
      ];
      this.requestService.updateAbstract(this.request.id, data)
        .subscribe(
          (res) => this.select_transfer = transfer, // cargar la conva
          (error) =>  this.error_message = error,
        );
    } else {
      this.select_transfer = null;
    }
  }
}
