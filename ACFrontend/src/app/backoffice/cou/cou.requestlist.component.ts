import { BackofficeConfigService } from '../config/config.service';
import { Component, OnInit, } from '@angular/core';
import { Request } from '../../shared/services/atentioncenter/request';
import { RequestByAreaService } from '../../shared/services/atentioncenter/request.area.service';
import { Router } from '@angular/router';
import { RequestService } from '../../shared/services/atentioncenter/request.service';

@Component({
  providers: [ RequestService, RequestByAreaService, BackofficeConfigService ],
  selector: 'request-list',
  templateUrl: './cou.requestlist.component.html'
})
export class COURequestListComponent implements OnInit {

  error_message: string;
  inbox = 'pending';
  requests: Request[];
  requests_active: Request[] = [];
  r_verification_pending: Request[] = [];
  r_proccess_pending: Request[] = [];
  r_resolution_pending: Request[] = [];
  filter: any;
  configFile: any;
  searh_data: any = {};
  find_requests: Request[] = [];

  constructor(
    private router: Router,
    private requestService: RequestByAreaService,
    private requestServiceRequest: RequestService,
    private config: BackofficeConfigService,
  ) {
    this.configFile = this.config;
  }

  ngOnInit() {
    this.requests = null;
    this.getRequests();
  }

  getRequests(): void {
    this.requestService.query(this.config.OFFICE_COU)
      .subscribe(
        (requests) => this.atentionRequest(requests),
        (error) =>  this.error_message = <any>error
      );
  }

  atentionRequest(requests: Request[]): void {
    this.requests = requests;
    this.requests_active = requests;
    this.r_verification_pending = this.requests.filter(
      (item) => item.state === this.config.STATE_DOCUMENT_VERIFICATION,
    );
    this.r_proccess_pending = this.requests.filter(
      (item) => item.state === this.config.STATE_CONVALIDATION_ACTION,
    );
    this.r_resolution_pending = this.requests.filter(
      (item) => item.state === this.config.STATE_RESOLUTION_PENDING,
    );
  }

  setInbox(event: Event, type: string ): void {
    this.inbox = type;
    switch (type) {
      case('verification_pending'):
        this.requests_active = this.r_verification_pending;
        break;
      case('proccess_pending'):
        this.requests_active = this.r_proccess_pending;
        break;
      case('resolution_pending'):
        this.requests_active = this.r_resolution_pending;
        break;
      default:
        this.requests_active = this.requests;
        break;
    }
  }

  gotoRequestDetail(id: string): void {
    const link: string[] = [`backoffice/cou-personal/solicitudes/${id}`];
    this.router.navigate(link);
  }

  /**
   * Busqueda de solicitudes por filtro avanzado
   * @param data datos enviados
   */
  searchRequests(data: any) {
    this.requestServiceRequest.search(data)
        .subscribe(
          (requests) => this.find_requests = requests,
          (error) => this.error_message = error
        );
  }
}
