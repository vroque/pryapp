import { Component, OnInit, Input } from '@angular/core';
import 'rxjs/add/operator/finally';

import { ProjectService } from '../../../shared/services/continental-project/project.service';
import { Project } from '../../../shared/services/continental-project/project.model';
import { ManageProjectService } from '../../../shared/services/continental-project/manage-project.service';
import { EnrollProject } from '../../../shared/services/continental-project/enroll-project.model';

@Component({
    selector: 'pry-detail-project',
    templateUrl: './detail-project.component.html',
    providers: [ProjectService, ManageProjectService]
})

export class PRYDetailProjectComponent implements OnInit {
    @Input() idProject: number;
    project: Project;
    response: { success: boolean, message: string };

    isLoadingProject: boolean;
    isUpdatingStatus: boolean;

    constructor(private projectService: ProjectService, private manageProjectService: ManageProjectService) { }

    ngOnInit() {
        this.getById(this.idProject);
    }

    getById(idProject: number): void {
        this.isLoadingProject = true;
        this.projectService.getById(idProject)
            .finally(() => this.isLoadingProject = false)
            .subscribe(
                (project: Project) => this.project = project,
                (error) => console.log(error));
    }

    approveProject(status: boolean): void {
        this.response = null;
        this.isUpdatingStatus = true;
        const project: EnrollProject = { idProject: this.project.id, status: status, idProfileProject: 0, pidm: 0};
        console.log(project);
        this.manageProjectService.approveProject(project)
            .finally(() => this.isUpdatingStatus = false)
            .subscribe(
                (res: boolean) => {
                    if (res) {
                        this.response = { success: res, message: 'Se actualizó el estado del proyecto.' };
                        this.project.approved = status;
                        this.project.changeStatusDate = new Date();
                    } else {
                        this.response = { success: res, message: 'No se pudo cambiar el estado del proyecto.' };
                    }
                },
                (error) => console.log(error));
    }
}
