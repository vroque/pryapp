import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/finally';

import { ProjectService } from '../../../shared/services/continental-project/project.service';
import { Project } from '../../../shared/services/continental-project/project.model';

@Component({
    selector: 'pry-list-project',
    templateUrl: './list-project.component.html',
    providers: [ProjectService]
})

export class PRYListProjectComponent implements OnInit {
    projects: Project[];
    idProject: number;

    projectNameFilter: string;
    categoryNameFilter: string;

    isLoadingProjects: boolean;

    constructor(private projectService: ProjectService) { }

    ngOnInit() {
        this.getAllProjects();
    }

    /**
     * Gets all projects
     */
    getAllProjects(): void {
        this.isLoadingProjects = true;
        this.projectService.getAll()
            .finally(() => this.isLoadingProjects = false)
            .subscribe(
                (projects: Project[]) => this.projects = projects,
                (error) => console.log(error));
    }

    /**
     * Muestra el detalle de un proyecto para aprobarlo o rechazarlo
     * @param idProject Id del proyecto
     */
    openDetail(idProject: number): void {
        console.log('Componente padre', idProject);
        this.idProject = idProject;
    }
}