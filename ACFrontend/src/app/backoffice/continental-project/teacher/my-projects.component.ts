import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/finally';

import { MyProjectService } from '../../../shared/services/continental-project/my-projects.service';
import { Project } from '../../../shared/services/continental-project/project.model';

@Component({
    selector: 'pry-my-projects',
    templateUrl: './my-projects.component.html',
    providers: [MyProjectService]
})

export class PRYMyProjectsComponent implements OnInit {
    projects: Project[];
    idSelectedProject: number;

    isFormActive: boolean;
    isLoadingProjects: boolean;
    isInscribedsActive: boolean;

    constructor(private myProjectService: MyProjectService) { }

    ngOnInit() {
        this.getProjects();
    }

    /**
     * Obtiene los proyecto del docente
     */
    getProjects(): void {
        this.isLoadingProjects = true;
        this.myProjectService.getAll()
            .finally(() => this.isLoadingProjects = false)
            .subscribe(
                (projects: Project[]) => this.projects = projects,
                (error) => console.log(error));
    }

    /**
     * Abre el fromulario para registrar un nuevo proyecto
     * @param idProject
     */
    openForm(idProject: number): void {
        this.isFormActive = true;
        this.idSelectedProject = idProject;
    }

    /**
     * Cierra el formulario de registro de proyectos
     * @param value
     */
    closeForm(value: boolean): void {
        this.isFormActive = value;
        this.idSelectedProject = 0;
        this.getProjects();
    }

    /**
     * Obtiene los inscritos del proyecto
     * @param idProject Id del proyecto
     */
    openListInscribeds(idProject: number): void {
        this.isInscribedsActive = true;
        this.isFormActive = false;
        this.idSelectedProject = idProject;
    }

    closeInscribeds(value: boolean): void {
        this.isInscribedsActive = value;
    }
}
