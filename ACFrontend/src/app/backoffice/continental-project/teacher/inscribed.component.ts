import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import 'rxjs/add/operator/finally';
import { ProjectMemberService } from '../../../shared/services/continental-project/project-member.service';
import { ProjectMember } from '../../../shared/services/continental-project/project-member.model';
import { EnrollmentService } from '../../../shared/services/continental-project/enrollment.service';
import { EnrollProject } from '../../../shared/services/continental-project/enroll-project.model';
import { AcademicProfile } from '../../../shared/services/academic/academic.profile';
import { StudentService } from '../../../shared/services/academic/student.service';
import { Student } from '../../../shared/services/academic/student';
import { SummaryProjectService } from '../../../shared/services/continental-project/summary-profile.service';
import { SummaryProfile } from '../../../shared/services/continental-project/summary-profile.model';

declare var $: any;

@Component({
    selector: 'pry-inscribed',
    templateUrl: './inscribed.component.html',
    providers: [ProjectMemberService, EnrollmentService, StudentService, SummaryProjectService]
})

export class PRYInscribedComponent implements OnInit {
    @Input() idProject: number;
    @Output() back: EventEmitter<boolean> = new EventEmitter<boolean>();

    projectMembers: ProjectMember[];
    summaryProfiles: SummaryProfile[];
    isLoadingInscribeds: boolean;
    isLoadingSummary: boolean;
    isUpdatingStatus: boolean;
    isFindingStudent: boolean;
    academicProfile: AcademicProfile;
    student: Student;

    constructor(private projectMemberService: ProjectMemberService, private enrollmentService: EnrollmentService,
        private studentService: StudentService, private summaryProjectService: SummaryProjectService) { }

    ngOnInit() {
        this.getInscribedsByIdProject(this.idProject);
        this.getSummary(this.idProject);
    }

    getSummary(idProject: number): void {
        this.isLoadingSummary = true;
        this.summaryProjectService.getByIdProject(idProject)
            .finally(() => this.isLoadingSummary = false)
            .subscribe(
                (summaryProfile: SummaryProfile[]) => this.summaryProfiles = summaryProfile,
                (error) => console.log(error));
    }

    getInscribedsByIdProject(idProject: number): void {
        this.isLoadingInscribeds = true;
        this.projectMemberService.getInscribeds(idProject)
            .finally(() => this.isLoadingInscribeds = false)
            .subscribe(
                (projectMembers: ProjectMember[]) => this.projectMembers = projectMembers,
                (error) => console.log(error));
    }

    goBack(): void {
        this.back.emit(false);
    }

    /**
     * Aprueba o rechaza la inscripcion de un estudiante
     * @param status Estado true o false
     */
    updateStatus(idProfileProject: number, pidm: number, status: boolean): void {
        const enrollment: EnrollProject = { idProfileProject: idProfileProject, pidm: pidm, status: status };
        this.isUpdatingStatus = true;
        this.enrollmentService.updateStatus(enrollment)
            .finally(() => this.isUpdatingStatus = false)
            .subscribe(
                (res: boolean) => {
                    if (res) {
                        this.projectMembers.filter(x => x.pidm === pidm)[0].approved = status;
                        this.projectMembers.filter(x => x.pidm === pidm)[0].changeStatusDate = new Date();
                    } else {
                        console.log('No se pudo actualizar');
                    }
                },
                (error) => console.log(error));
    }

    showProfile(member: ProjectMember): void {
        console.log('Member :> ', member);
        this.isFindingStudent = true;
        $('#studentDetails').modal('toggle');
        this.studentService.find(member.pidm.toString(), 'person_id')
            .finally(() => this.isFindingStudent = false)
            .subscribe(
                (student: Student) => {
                    if (student) {
                        this.student = student;
                        this.academicProfile = student.profiles.filter(x => x.program.id === member.program)[0];
                    } else {
                        console.log('No se pudo actualizar');
                    }
                },
                (error) => console.log(error));
    }
}
