import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import 'rxjs/add/operator/finally';

import { ProjectService } from '../../../shared/services/continental-project/project.service';
import { Project } from '../../../shared/services/continental-project/project.model';
import { isNullOrUndefined } from 'util';
import { CategoryService } from '../../../shared/services/continental-project/category.service';
import { ProjectTypeService } from '../../../shared/services/continental-project/project-type.service';
import { Category } from '../../../shared/services/continental-project/category.model';
import { ProjectType } from '../../../shared/services/continental-project/project-type.model';
import { PersonaService } from '../../../shared/services/persona/persona.service';
import { Persona } from '../../../shared/services/persona/persona';
import { ProjectMember } from '../../../shared/services/continental-project/project-member.model';
import { ProfileProject } from '../../../shared/services/continental-project/profile-project.model';

@Component({
    selector: 'pry-create-edit-project',
    templateUrl: './create-edit-project.component.html',
    providers: [ProjectService, CategoryService, ProjectTypeService, PersonaService]
})

export class PRYCreateEditProjectComponent implements OnInit {
    @Input() idProject: number;
    @Output() closeForm: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() back: EventEmitter<boolean> = new EventEmitter<boolean>();

    project: Project = {};
    categories: Category[];
    projectTypes: ProjectType[];
    foundProjectMember: ProjectMember = {};
    profileProject: ProfileProject = {};
    keywords: string[];

    isLoadingCategories: boolean;
    isLoadingProjectTypes: boolean;
    isLoadingProject: boolean;
    isSavingProject: boolean;

    searchTeamDni: string;

    constructor(private projectService: ProjectService, private categoryService: CategoryService,
        private projectTypeService: ProjectTypeService, private personService: PersonaService) { }

    ngOnInit() {
        this.project.id = this.idProject;
        this.project.idCategory = this.project.idProjectType = 0;
        this.project.projectMembers = [];
        this.project.profileProjects = [];
        this.getCategories();
        this.getProjectTypes();
        if (this.idProject > 0) {
            this.edit(this.idProject);
        }
    }

    /**
     * Obtiene las categorias
     */
    getCategories(): void {
        this.isLoadingCategories = true;
        this.categoryService.getAll()
            .finally(() => this.isLoadingCategories = false)
            .subscribe(
                (categories: Category[]) => this.categories = categories,
                (error) => console.log(error));
    }

    /**
     * Obtiene los tipos de proyectos
     */
    getProjectTypes(): void {
        this.isLoadingProjectTypes = true;
        this.projectTypeService.getAll()
            .finally(() => this.isLoadingProjectTypes = false)
            .subscribe(
                (projectTypes: ProjectType[]) => this.projectTypes = projectTypes,
                (error) => console.log(error));
    }

    /**
     * Carga el proyecto para ser editado
     * @param idProject Id del proyecto
     */
    edit(idProject: number): void {
        this.isLoadingProject = true;
        this.projectService.getById(idProject)
            .finally(() => { this.isLoadingProject = false; this.splitKeywords(); })
            .subscribe(
                (project: Project) => {
                    this.project = project;
                    console.log(this.project.projectMembers);
                    this.project.projectMembers = this.project.projectMembers.filter(x => x.idMemberType === 2);
                },
                (error) => console.log(error));
    }

    /**
     * Crea un nuevo proyecto
     */
    create(): void {
        this.project.campus = 'S01';
        this.isSavingProject = true;
        this.projectService.create(this.project)
            .finally(() => this.isSavingProject = false)
            .subscribe(
                (project: Project) => {
                    if (isNullOrUndefined(project)) {
                        console.log('No se pudo guardar');
                    } else {
                        this.closeForm.emit(false);
                        this.project = {};
                        console.log('Se guardó correctamente');
                    }
                },
                (error) => console.log(error));
    }

    update(): void {
        this.project.campus = 'S01';
        this.projectService.update(this.project)
            .subscribe(
                (res: boolean) => {
                    if (!res) {
                        console.log('No se pudo actualizar');
                    } else {
                        this.closeForm.emit(false);
                        this.project = {};
                    }
                },
                (error) => console.log(error));
    }

    /**
     * Busca a una persona para ser miembro del equipo
     */
    findTeam(): void {
        if (!this.searchTeamDni) { return; }
        console.log('Buscando...');
        this.personService.getByDni(this.searchTeamDni)
            .subscribe(
                (person: Persona) => this.foundProjectMember = { pidm: person.id, fullName: person.full_name },
                (error) => console.log(error));
    }

    /**
     * Agrega al integrante encontrado
     */
    addTeamMember(): void {
        if (!this.project.projectMembers.some(x => x.pidm === this.foundProjectMember.pidm)) {
            this.project.projectMembers.push(this.foundProjectMember);
            this.foundProjectMember = {};
            this.searchTeamDni = '';
        }
    }

    /**
     * Quita al integrante
     * @param pidm Pidm de la persona
     */
    removeTeamMember(pidm: number): void {
        const member = this.project.projectMembers.filter(x => x.pidm === pidm)[0];
        if (!member) { return; }
        const index = this.project.projectMembers.indexOf(member);
        this.project.projectMembers.splice(index, 1);
    }

    /**
     * Agrega el perfil al modelo del proyecto
     */
    addProfile(): void {
        if (!this.project.profileProjects.some(x => x.title === this.profileProject.title)) {
            this.project.profileProjects.push(this.profileProject);
            this.profileProject = {};
        }
    }

    /**
     * Quita el perfil añadido al proyecto
     * @param profileTitle Titulo del perfil
     */
    removeProfile(profileTitle: string): void {
        const profile = this.project.profileProjects.filter(x => x.title === profileTitle)[0];
        if (!profile) { return; }
        const index = this.project.profileProjects.indexOf(profile);
        this.project.profileProjects.splice(index, 1);
    }

    goBack(): void {
        this.back.emit(false);
    }

    splitKeywords(): void {
        if (this.project.keywords.split(',').length === 0 && this.project.keywords) {
            this.keywords = [this.project.keywords];
            return;
        }

        this.keywords = this.project.keywords.split(',')
            .filter((keyword) => keyword !== '')
            .filter((keyword, position, self) => self.indexOf(keyword) === position);
    }
}
