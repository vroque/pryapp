import { Component, OnInit, Input } from '@angular/core';
import 'rxjs/add/operator/finally';

import { ProjectService } from '../../../shared/services/continental-project/project.service';
import { Project } from '../../../shared/services/continental-project/project.model';
import { EnrollmentService } from '../../../shared/services/continental-project/enrollment.service';

@Component({
    selector: 'pry-back-detail-project',
    templateUrl: './detail-project.component.html',
    providers: [ProjectService, EnrollmentService]
})

export class PRYBackDetailProjectComponent implements OnInit {
    @Input() idProject: number;
    project: Project;
    isEnrolled: boolean;
    isLoadingProject: boolean;
    isCheckingEnroll: boolean;
    isEnrolling: boolean;
    keywords: string[] = [];

    constructor(private projectService: ProjectService, private enrollService: EnrollmentService) { }

    ngOnInit() {
        this.getProjectBydId(this.idProject);
    }

    /**
     * Gets project byd id
     * @param id Id del proyecto
     */
    getProjectBydId(id: number): void {
        this.isLoadingProject = true;
        this.projectService.getById(id)
            .finally(() => {
                this.isLoadingProject = false; this.checkEnroll(this.project.id); this.splitKeywords();
            })
            .subscribe(
                (project: Project) => this.project = project,
                (error) => console.log(error));
    }

    /**
     * Checks enroll
     * @param idProject Id del proyecto
     */
    checkEnroll(idProject: number): void {
        this.isCheckingEnroll = true;
        this.enrollService.getByIdProject(idProject)
            .finally(() => this.isCheckingEnroll = false)
            .subscribe(
                (res: boolean) => this.isEnrolled = res,
                (error) => console.log(error));
    }

    /**
     * Inscribirse en un proyecto
     * @param idProfileProject Id del perfil del proyecto
     */
    enroll(idProfileProject: number): void {
        this.isEnrolling = true;
        this.enrollService.enroll(idProfileProject)
            .finally(() => this.isEnrolling = false)
            .subscribe(
                (res: boolean) => {
                    if (res) {
                        this.isEnrolled = res;
                    } else {
                        console.log('No se pudo inscribirse');
                    }
                },
                (error) => console.log(error));
    }

    splitKeywords(): void {
        if (this.project) {
            this.keywords = this.project.keywords.toUpperCase().split(',');
        }
    }
}
