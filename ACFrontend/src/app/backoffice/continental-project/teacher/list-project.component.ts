import { Component, OnInit, Input } from '@angular/core';
import { Project } from '../../../shared/services/continental-project/project.model';
import { ProjectService } from '../../../shared/services/continental-project/project.service';
import { Category } from '../../../shared/services/continental-project/category.model';

@Component({
    selector: 'pry-back-list-project',
    templateUrl: './list-project.component.html',
    providers: [ProjectService]
})

export class PRYBackListProjectComponent implements OnInit {
    @Input() category: Category;

    projects: Project[];
    idSelectedProject: number;

    isLoadingProjects: boolean;

    constructor(private projectService: ProjectService) { }

    ngOnInit() {
        this.getProjectsByCategoryId(this.category.id);
    }

    /**
     * Gets projects by category id
     * @param idCategory Id de la categoria
     */
    getProjectsByCategoryId(idCategory: number): void {
        this.isLoadingProjects = true;
        this.projectService.getByCategoryId(idCategory)
            .finally(() => this.isLoadingProjects = false)
            .subscribe(
                (projects: Project[]) => this.projects = projects,
                (error) => console.log(error));
    }

    /**
     * Sets project
     * @param id Id del proyecto
     */
    setProject(id: number): void {
        this.idSelectedProject = id;
    }
}