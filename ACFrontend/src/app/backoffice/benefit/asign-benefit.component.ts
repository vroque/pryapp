import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { AcademicProfile } from '../../shared/services/academic/academic.profile';
import { AsignBenefit } from '../../shared/services/benefit/asign-benefit';
import { AsignBenefitService } from '../../shared/services/benefit/asign-benefit.service';
import { BenefitStudent } from '../../shared/services/benefit/benefit-student';
import { BenefitStudentService } from '../../shared/services/benefit/benefit-student.service';
import { Discount } from '../../shared/services/benefit/discount';
import { DiscountService } from '../../shared/services/benefit/discount.service';
import { Message } from 'primeng/primeng';
import { Student } from '../../shared/services/academic/student';
import { StudentService } from '../../shared/services/academic/student.service';

@Component(
  {
    selector: 'benefit-asign-benefit',
    templateUrl: './asign-benefit.component.html',
    providers: [
      { provide: LOCALE_ID, useValue: 'es' },
      StudentService,
      DiscountService,
      AsignBenefitService,
      BenefitStudentService
    ],
  })

export class BenefitAsignBenefitComponent implements OnInit {

  createLoading: boolean;
  id: string;
  list: Discount[];
  listBenefitStudent: BenefitStudent[] = [];
  benefitDelete: BenefitStudent;
  loading: boolean;
  messageCreate: { validate: boolean, message: string };
  newAsignacion: AsignBenefit;
  primeMsg: Message[];
  profile: AcademicProfile;
  selectedDiscount: Discount;
  student: Student;
  studentLoading: boolean;

  constructor(private discountService: DiscountService, private asignbenefitService: AsignBenefitService,
    private benefitstudentService: BenefitStudentService, private router: Router,
    private route: ActivatedRoute, private studentService: StudentService
  ) { }

  ngOnInit(): void {
    this.studentLoading = false;
    this.newAsignacion = new AsignBenefit();
    this.route.params.forEach((params: Params) => {
      this.id = params.dni;
    });

    this.getStudent();
    this.getDiscount();
  }

  getDiscount(): void {
    this.loading = true;
    this.discountService.listDiscount()
      .subscribe(
        (discounts) => this.list = discounts,
        (error) => console.log(error),
        () => this.loading = false,
      );
  }

  back(): void {
    const linkBack: string[] = [`backoffice/descuentos/asignar-descuento`];
    this.router.navigate(linkBack);
  }

  changeDiscount(id: string): void {
    if (id) {
      this.selectedDiscount = this.list.filter((f) => f.discount_id === id)[0];
    }
  }

  getStudent(): void {
    this.studentService.find('' + this.id, 'student_id')
      .subscribe(
        (student) => {
          this.student = student;
          this.getBenefitStudent(this.student.id);
        },
        (error) => {
          console.log(error);
          this.student = null;
          this.studentLoading = false;
        },
        () => {
          if (this.student !== null && this.student !== undefined) {
            this.studentLoading = true;
          } else {
            this.studentLoading = false;
          }
          this.profile = this.student.profile;
          console.log('this.profile => ', this.profile);
          this.loading = false;
        },
      );
  }

  getBenefitStudent(id: string): void {
    this.benefitstudentService.getBenefitStudent(id)
      .subscribe(
        (benefitStudent) => this.listBenefitStudent = benefitStudent,
        (error) => console.log(error),
        () => this.loading = false,
      );
  }

  hiddenMessage(time: number): void {
    setTimeout(() => {
      this.messageCreate = null;
    }, time);
  }

  createAsign(event: any): void {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.newAsignacion.campus = this.student.profile.campus.code;
    this.newAsignacion.student_id = this.student.id;
    this.newAsignacion.program = this.student.profile.program.id;
    this.newAsignacion.discount_id = this.selectedDiscount.discount_id;
    this.newAsignacion.modality = this.student.profile.department;
    const term = this.newAsignacion.term.substr(0, 4);
    if (this.newAsignacion.beca === 'T' && (Number(term) < 2019 || (Number(term) === 2019
     && Number(this.newAsignacion.term.substr(5, 1)) < 1))) {
      this.messageCreate = {validate: false, message: 'El Tercio de Beca no aplica para periodos anteriores a 2019-1.'};
      target.disabled = false;
    } else {
      if (!this.selectedDiscount.scale) {
        this.newAsignacion.scale = 'G';
      }
      this.createLoading = true;
      this.asignbenefitService.create(this.newAsignacion)
        .subscribe(
          (na) => {
            this.messageCreate = na;
            target.disabled = false;
          },
          (error) => console.log(error),
          () => {
            target.disabled = false;
            this.getBenefitStudent(this.student.id);
            this.hiddenMessage(10000);
            this.createLoading = false;
          },
        );
    }
  }

  /**
   * Clears benefit delete
   * Se re-establece el valor null a la variable benefitDelete
   */
  clearBenefitDelete(): void {
    this.benefitDelete = null;
  }

  /**
   * Sets benefit delete
   * Se obtiene y se guarda el beneficio a eliminar
   * @param benefitStudent -> obtiene el beneficio escogido para eliminar
   */
  setBenefitDelete(benefitStudent: BenefitStudent): void {
    this.benefitDelete = benefitStudent;
  }
  /**
   * Deletes benefit
   * Elimina el beneficio selecionado del estudiante
   */
  deleteBenefit(): void {
    if (this.benefitDelete === null || this.benefitDelete === undefined) {
      this.primeMsg = [{
        detail: 'Ha ocurrido un problema al intentar eliminar el beneficio',
        severity: 'danger',
        summary: 'Eliminación de Beneficio',
      }];
      this.clearBenefitDelete();
      return;
    }

    this.benefitstudentService.delete(this.benefitDelete)
      .subscribe(
        (success) => {
          if (success) {
            this.primeMsg = [{
              detail: 'Se eliminó el beneficio.',
              severity: 'success',
              summary: 'Eliminación de Beneficio',
            }];

            const benefit = this.listBenefitStudent.filter(x => x.idBenefit === this.benefitDelete.idBenefit
             && x.term === this.benefitDelete.term)[0];
            const index = this.listBenefitStudent.indexOf(benefit);
            this.listBenefitStudent.splice(index, 1);
          } else {
            this.primeMsg = [{
              detail: 'Ha ocurrido un problema al intentar eliminar el beneficio',
              severity: 'danger',
              summary: 'Eliminación de Beneficio',
            }];
          }
          this.clearBenefitDelete();
        });
  }
}
