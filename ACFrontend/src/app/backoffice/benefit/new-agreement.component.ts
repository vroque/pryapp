import { AgreementService } from '../../shared/services/benefit/agreement.service';
import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { CreateAgreement } from '../../shared/services/benefit/create-agreement';
import { DegreeFamiliarity } from '../../shared/services/benefit/degree-familiarity';
import { DegreeFamiliarityService } from '../../shared/services/benefit/degree-familiarity.service';

@Component(
  {
    selector: 'benefit-new-agreement',
    templateUrl: './new-agreement.component.html',
    providers: [
      DegreeFamiliarityService,
      AgreementService,
      { provide: LOCALE_ID, useValue: 'es' },
    ],
  })

export class BenefitNewAgreementComponent implements OnInit {

  errorMessage: string;
  listFamiliarity: DegreeFamiliarity[];
  messageCreate: { validate: boolean, message: string };
  newAgreement: CreateAgreement;
  disableButton = false;
  loading: boolean;

  constructor(private degFamService: DegreeFamiliarityService,
    private agreementService: AgreementService
  ) { }

  ngOnInit(): void {
    this.getData();
    this.newAgreement = {
      cod_contract: undefined,
      company_name: undefined,
      start_date: new Date(),
      end_date: new Date(),
      grad_fam: 'D',
      disapproved: false,
      renovation_auto: false,
      modalityADM: 'ADM',
      percent_enrollmentADM: 0,
      percent_cuotADM: 0,
      modalityADG: 'ADG',
      percent_enrollmentADG: 0,
      percent_cuotADG: 0,
      modalityADV: 'ADV',
      percent_enrollmentADV: 0,
      percent_cuotADV: 0,
    };
  }

  getData(): void {
    this.getDegFamiliarity();
  }
  getDegFamiliarity(): void {
    this.loading = true;

    this.degFamService.listDegFamiliarity()
      .subscribe(
        (degreeFamiliarity) => this.listFamiliarity = degreeFamiliarity,
        (error) => this.errorMessage = error as any,
        () => this.loading = false,
      );

  }
  createAgreement(): void {
    this.loading = true;
    this.agreementService.create(this.newAgreement)
      .subscribe(
        (data) => {
          this.messageCreate = data;
          this.disableButton = data.validate;
        },
        (error) => {
          this.errorMessage = error as any;
          this.disableButton = false;
        },
        () => {
          this.loading = false;
        },
      );
  }
}
