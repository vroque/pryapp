import { ActivatedRoute, Params } from '@angular/router';
import { Agreement } from '../../shared/services/benefit/agreement';
import { AgreementDetail } from '../../shared/services/benefit/agreement-detail';
import { AgreementDetailService } from '../../shared/services/benefit/agreement-detail.service';
import { AgreementService } from '../../shared/services/benefit/agreement.service';
import { Component, Input, LOCALE_ID, OnInit } from '@angular/core';
import { DegreeFamiliarity } from '../../shared/services/benefit/degree-familiarity';
import { DegreeFamiliarityService } from '../../shared/services/benefit/degree-familiarity.service';
import { Router } from '@angular/router';

@Component({
  selector: 'benefit-agreement-detail',
  templateUrl: './agreement-detail.component.html',
  providers: [
    AgreementService,
    AgreementDetailService,
    DegreeFamiliarityService,
    { provide: LOCALE_ID, useValue: 'es' },
  ],
})
export class BenefitAgreementDetailComponent implements OnInit {

  @Input() agreement: Agreement;
  errorMessage: string;
  updateMessage: string;
  updateMessageDetail: string;
  updateAg: boolean;
  updateAgDetail: boolean;
  agreemnt: Agreement;
  listDetail: AgreementDetail;
  listFamiliarity: DegreeFamiliarity[];
  loading: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private agreementService: AgreementService,
    private agreementDetailService: AgreementDetailService,
    private degreeFamiliarityService: DegreeFamiliarityService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.listDetail = undefined;
    this.getData();
    this.updateAg = undefined;
    this.updateAgDetail = undefined;
  }

  getData() {
    this.activatedRoute.params.forEach((params: Params) => {
      const requestId: string = (params as any).agreementId;
      this.getAgreementDetail(requestId);
      this.getAgreement(requestId);
      this.getDegFamiliarity();
    });
  }
  /**
   * Obtiene la información detallada del convenio
   * @param agreementId
   */
  getAgreementDetail(agreementId: string) {
    this.loading = true;

    this.agreementDetailService.getDetail(agreementId)
      .subscribe(
        (agreementDetail) => {
          this.listDetail = agreementDetail;
        },
        (error) => this.errorMessage = error as any,
        () => { this.loading = false; },
      );
  }

  /**
   * Obtiene la información de cabecera de un convenio
   * @param agreementId
   */
  getAgreement(agreementId: string) {
    this.loading = true;

    this.agreementService.getAgreement(agreementId)
      .subscribe(
        (agreement) => {
          this.agreemnt = agreement;
          this.agreemnt.start_date = new Date(String(this.agreemnt.start_date));
          this.agreemnt.end_date = new Date(String(this.agreemnt.end_date));
        },
        (error) => this.errorMessage = error as any,
        () => this.loading = false,
      );
  }

  /**
   * Obtiene la lista de los grados de familiaridad del convenio
   */
  getDegFamiliarity() {
    this.loading = true;

    this.degreeFamiliarityService.listDegFamiliarity()
      .subscribe(
        (degreeFamiliarity) => this.listFamiliarity = degreeFamiliarity,
        (error) => this.errorMessage = error as any,
        () => this.loading = false,
      );

  }
  backAgreement() {
    const link: string[] = [`backoffice/convenios`];
    this.router.navigate(link);
  }

  updateAgreement(agreemnt: Agreement, listDetail: AgreementDetail, event: any) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.loading = true;
    this.agreementService.updateAgreement(agreemnt)
      .subscribe(
        (hmm) => {
          this.updateAg = true;
          target.disabled = false;
          this.updateMessage = 'Se actualizó el convenio correctamente';
        },
        (error) => {
          this.updateMessage = 'Ocurrió un error. Comunicarse con el encargado para su revisión';
          this.updateAg = false;
        },
        () => {
          this.updateDetailAgreement(listDetail, event);
        },
      );
  }
  hiddenMessage(time: number) {
    setTimeout(() => {
      this.updateAgDetail = null;
      this.updateAg = null;
    }, time);
  }

  updateDetailAgreement(detalle: AgreementDetail, event: any) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    this.loading = true;
    this.agreementDetailService.updateDetail(detalle)
      .subscribe(
        (det) => {
          this.updateAgDetail = true;
          this.updateMessageDetail = 'Se actualizó el detalle del convenio correctamente';
        },
        (error) => {
          this.updateAgDetail = false;
          this.updateMessageDetail = 'Ocurrió un error. Comunicarse con el encargado para su revisión';
        },
        () => {
          this.loading = false;
          target.disabled = false;
          this.hiddenMessage(5000);
          this.getData();
        },
      );
  }
}
