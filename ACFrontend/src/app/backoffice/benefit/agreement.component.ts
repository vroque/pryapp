import { Agreement } from '../../shared/services/benefit/agreement';
import { AgreementService } from '../../shared/services/benefit/agreement.service';
import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  providers: [AgreementService, { provide: LOCALE_ID, useValue: 'es' }],
  selector: 'benefit-agreement',
  templateUrl: './agreement.component.html'
})
export class BenefitAgreementComponent implements OnInit {

  errorMessage: string;
  list: Agreement[];
  loading: boolean;
  filter: any;

  constructor(
    private router: Router,
    private agreementService: AgreementService,
  ) { }

  ngOnInit() {
    this.list = [];
    this.getAgreement();
  }

  getAgreement() {
    this.loading = true;
    this.agreementService.list()
    .subscribe(
      (agreements) => this.list = agreements,
      (error) => this.errorMessage = error as any,
      () => this.loading = false,
    );
  }

  /**
   * Go to agreement detail
   * @param agreementId
   */
  agreementDetail(agreementId: string) {
    const link: string[] = [`backoffice/convenios/lista-convenios/${agreementId}`];
    this.router.navigate(link);
  }

}
