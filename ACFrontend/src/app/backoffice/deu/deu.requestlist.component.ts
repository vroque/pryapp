import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Request } from '../../shared/services/atentioncenter/request';
import { RequestByAreaService } from '../../shared/services/atentioncenter/request.area.service';
import { BackofficeConfigService } from '../config/config.service';

@Component({
  selector: 'request-list',
  templateUrl: './deu.requestlist.component.html',
  providers: [ RequestByAreaService, BackofficeConfigService ],
})
export class DEURequestListComponent implements OnInit {
  error_message: string;
  inbox: string = 'pending';
  requests: Request[];
  requests_active: Request[] = [];
  filter: any;

  constructor(
    private router: Router,
    private requestService: RequestByAreaService,
    private config: BackofficeConfigService
  ) { }

  ngOnInit(): void {
    this.requests = null;
    this.getRequests();
  }

  getRequests(): void {
    this.requestService.query(this.config.OFFICE_DEU)
      .subscribe(
        (requests) => this.atentionRequest(requests),
        (error) =>  this.error_message = error,
      );
  }
  atentionRequest(requests: Request[]): void {
    this.requests = requests;
    this.requests_active = requests;
  }

  setInbox(event, type: string ): void {
    this.inbox = type;
    this.requests_active = this.requests;
  }

  gotoRequestDetail(id: string): void {
    const link = [`backoffice/deu-personal/convalidaciones/${id}`];
    this.router.navigate(link);
  }
}
