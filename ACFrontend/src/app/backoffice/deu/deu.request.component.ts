import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";

import { Student } from "../../shared/services/academic/student";
import { DocumentState } from "../../shared/services/atentioncenter/document.state";
import { Request } from "../../shared/services/atentioncenter/request";
import { RequestLog } from "../../shared/services/atentioncenter/request.log";

import { AcademicProfile } from "../../shared/services/academic/academic.profile";
import { StudentService } from "../../shared/services/academic/student.service";
import { DocumentStateService } from "../../shared/services/atentioncenter/document.state.service";
import { GenerateDocumentService } from "../../shared/services/atentioncenter/generate.document.service";
import { RequestLogService } from "../../shared/services/atentioncenter/request.log.service";
import { RequestService } from "../../shared/services/atentioncenter/request.service";
import { SignatoriesService } from "../../shared/services/atentioncenter/signatories.service";

import { BackofficeConfigService } from "../config/config.service";

@Component({
  providers: [
    StudentService,
    RequestService,
    RequestLogService,
    DocumentStateService,
    SignatoriesService,
    GenerateDocumentService,
    BackofficeConfigService,
  ],
  selector: "request-list",
  templateUrl: "./deu.request.component.html",
})
export class DEURequestComponent implements OnInit {
  conva: any;
  derivate_message: string = "Solicito proceder con la convalidación segun el informe adjunto.\n para su atención";
  doc_status: string = "pre"; // pre, on, err
  error_message: string;
  flow: DocumentState[];
  ideal_flow: DocumentState[];
  logs: RequestLog[];
  malla: any[];
  process: string = "";
  request: Request;
  signatories: string[];
  student_profile: AcademicProfile;

  constructor(
    private _route: ActivatedRoute,
    private requestService: RequestService,
    private requestLogService: RequestLogService,
    private documentStateService: DocumentStateService,
    private signatoriesService: SignatoriesService,
    private generateDocumentService: GenerateDocumentService,
    private studentService: StudentService,
    private config: BackofficeConfigService
  ) { }

  ngOnInit(): void {
    this.request = null;
    this.flow = [];
    this.ideal_flow = [];
    this.logs = [];
    this._route.params.forEach((params: Params) => {
      let request_id: string = (params as any).request;
      this.getRequest(request_id);
      this.getRequestLogs(request_id);
    });
  }

  getRequest(requestId: string): void {
    this.requestService.get(requestId)
      .subscribe(
        (request) => this.refreshRequest(request),
        (error) =>  this.error_message = error,
      );
  }

  refreshRequest(request: Request): void {
    this.request = request;
    this.request.document_uri = this.request.document_uri || [];
    this.getDocumentFlow(this.request.document_id);
    this.getSignatories(this.request.document_id, this.request.program);
    this.getProfile(
      this.request.divs,
      this.request.program,
      this.request.person_id);
    (this.request as any).opts = JSON.parse(this.request.description) || {};
    this.conva = JSON.parse(this.request.description).convalidation || {};
    (this.request as any).conva_type = JSON.parse(this.request.description).conva_type || "";
  }

  updateLogs(event: string): void {
    this.getRequestLogs(String(this.request.id));
    this.request.state = this.config.STATE_DELIVERY_PENDING;
  }

  getRequestLogs(requestId: string): void {
    this.requestLogService.query(requestId)
      .subscribe(
        (logs) => this.logs = logs,
        (error) =>  this.error_message = error,
        () => console.log("logs -> ", this.logs)
      );
  }

  getDocumentFlow(documentId: number): void {
    this.documentStateService.query(documentId)
      .subscribe(
        (flow) => this.flow = flow,
        (error) =>  this.error_message = error,
        () => { this.getIdealFlow(this.flow); },
      );
  }

  getIdealFlow(flow: DocumentState[]): void {
    this.ideal_flow = flow.filter((state) => (state.order !== -1));
  }

  getLogTrace(state: number): RequestLog {
    for (let i: number = 0; i < this.logs.length; i++) {
      if(this.logs[i].old_state === state) {
        return this.logs[i];
      }
    }
    return null;
  }

  getSignatories(documentId: number, program: string): void {
    this.signatoriesService.query(documentId, { program })
      .subscribe(
        (signatories) => this.signatories = signatories,
        (error) =>  this.error_message = error,
        () => { this.getIdealFlow(this.flow); },
      );
  }

  getProfile(div: string, program: string, personId: number): void {
    this.studentService.find(String(personId), "person_id")
      .subscribe(
        (studentReq) => {
          const student: Student = studentReq;
          for (let profile of student.profiles) {
            if (profile.program.id === program && profile.div === div) {
              this.student_profile = profile;
            }
          }
        },
        (error) =>  this.error_message = error,
      );
  }

  docSt(id: number): any {
    return this.config.getDocumentState(id);
  }

  /**
   * Marca la solicitud como entregada l
   */
  deliveryRequest(requestId: string): void {
    this.requestService.save(requestId, {})
      .subscribe(
        (request) => {
          this.request = request;
          this.getRequestLogs(String(request.id));
        },
        (error) =>  this.error_message = error,
      );
  }

  derivateRequest(event: any, requestId: string, message: string):void {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    let office: number;
    switch (this.request.document_id) {
      case(this.config.DOC_STUDIES_CERTIFICATE):
        office = this.config.OFFICE_RAU;
        break;
      case(this.config.DOC_CONVALIDATION):
        office = this.config.OFFICE_COU;
        if(this.request.state === this.config.STATE_CONVALIDATION_VERIFICATION) {
          office = this.config.OFFICE_RAU;
        }
        break;
      default:
        office = this.config.OFFICE_CAU;
    }
    this.requestService.update(requestId, {area_id: office, message, type: "derive" })
      .subscribe(
        (request) => {
          this.request = request;
          this.getRequestLogs(String(request.id));
        },
        (error) =>  this.error_message = error,
        () => target.disabled = false,
      );
  }

  cancelRequest(event: any, requestId: string, message: string): void {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.requestService.update(requestId, {area_id: 0, message, type: "refuse" })
      .subscribe(
        (request) => {
          this.refreshRequest(request);
          this.getRequestLogs(String(request.id));
        },
        (error) =>  this.error_message = error,
        () => target.disabled = false,
      );
  }

}
