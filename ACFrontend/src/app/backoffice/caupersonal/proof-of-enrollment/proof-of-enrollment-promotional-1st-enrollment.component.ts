import { CollegeService } from '../../../shared/services/academic/college.service';
import { Component, OnInit } from '@angular/core';
import { Graduated } from '../../../shared/services/academic/graduated';
import { GraduationService } from '../../../shared/services/academic/graduation.service';
import { ProofOfEnrollmentService } from '../../../shared/services/atentioncenter/proof.of.enrollment.service';

@Component({
  selector: 'caupersonal-proof-of-enrollment-promotional-1st-enrollment',
  templateUrl: './proof-of-enrollment-promotional-1st-enrollment.component.html',
  providers: [
    CollegeService,
    GraduationService,
    ProofOfEnrollmentService,
  ],
})
export class CAUProofOfEnrollmentPromotional1stEnrollmentComponent implements OnInit {

  errorMsg = '';
  isSearching: boolean;
  estudiantes: any;  // modelo para el input de Estudiantes
  periodo: any;  // modelo para el select de Períodos académicos de matrícula UC
  students: any[];  // array de 'estudiantes' a procesar
  graduates: Graduated[];  // graduados UC
  tuqpa: any[];
  isShowStudentsList: boolean;
  isAnyStudent: boolean;
  selected = false;
  existPeriodo = false;
  processing = false;

  constructor(
    private  graduationService: GraduationService,
    private proofOfEnrollmentService: ProofOfEnrollmentService
  ) {  }

  ngOnInit() {
    this.clean();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.errorMsg = '';
    this.isSearching = false;
    this.estudiantes = undefined;
    this.periodo = undefined;
    this.students = [];
    this.graduates = undefined;
    this.tuqpa = [];
    this.isShowStudentsList = false;
    this.isAnyStudent = false;
    this.selected = false;
    this.existPeriodo = false;
    this.processing = false;
  }

  /**
   * Activa/Desactiva el botón "Buscar Estudiantes"
   * @returns {boolean}
   */
  isAllSelected(): boolean {
    return !(this.estudiantes == null || this.estudiantes === undefined || this.estudiantes.trim() === '');
  }

  /**
   * Activa/Desactiva el botón "Limpiar"
   * @returns {boolean}
   */
  isAnySelected(): boolean {
    return !(this.estudiantes == null || this.estudiantes === undefined || this.estudiantes.trim() === '');
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray(): void {
    this.isSearching = false;
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // http://stackoverflow.com/a/9229821
    this.students = this.estudiantes.split(' ')
      .filter(// filter blank space
        (id) => id !== '',
      )
      .filter(// filter duplicates
        (id: any, position: any, self: any) => self.indexOf(id) === position,
      );
    console.log('input students => ', this.students);
    console.log('+--------------------------+');
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList(): void {
    this.isShowStudentsList = !this.isShowStudentsList;
  }

  /**
   * Egresados UC según código de estudiante
   */
  getGraduates(): void {
    console.log('Get Graduates...');
    this.isSearching = true;
    this.tuqpa = [];
    this.graduates = undefined;
    this.isAnyStudent = false;
    this.selected = false;
    this.periodo = undefined;
    this.existPeriodo = false;
    const type = 'searchAllEntrant';
    const schoolId = '';
    const graduationPeriod = '';
    this.processing = true;
    this.graduationService.getGraduates(type, schoolId, graduationPeriod, this.students)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => this.errorMsg = error as any,
        () => {
          this.processing = false;
          console.log('this.graduates => ', this.graduates);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Selecciona/Deselecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll(): void {
    for (const key of Object.keys(this.graduates)) {
      this.graduates[key].is_selected = !this.selected;
    }

    this.selected = !this.selected;
    this.isAnyStudent = this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   * @returns {boolean}
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.graduates) {
      if (this.graduates[key].is_selected) {
        return true;
      }
    }
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * Añade estilo a los estudiantes seleccionados
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any): void {
    if (studentObj.is_selected) {
      studentObj.is_selected = false;
      console.log('Desactivado');
      // quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
      // ahora no todos están seleccionados
      // pero comprobemos que a lo menos alguien esté seleccionado
      this.isAnyStudent = this.isAnyStudentSelected() ? this.isAnyStudentSelected() : false;
    } else {
      studentObj.is_selected = true;
      console.log('Activado');
      // agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);
      // console.log("this.tuqpa => ", this.tuqpa);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
      console.log('+--------------------------+');
    }
  }

  /**
   * Constancia de Matrícula Promocional
   */
  getProofOfEnrollment(): void {
    console.log('Get Proof Of Enrollment...');

    this.getSequentialProofOfEnrollment(0);

    console.log('students => ', this.graduates);
  }

  /**
   * Petición secuencial para generar Constancia de Matrícula Promocional
   * NOTE: No es "LA" solución para este tipo de peticiones pero es lo que hay
   * @param {number} i
   */
  getSequentialProofOfEnrollment(i: number): void {
    console.log('Sequential...');
    const type = '1stEnrollment';

    console.log('proceso => ', i);

    if (this.graduates[i] !== undefined) {
      if (this.graduates[i].is_selected) {
        this.processing = true;
        this.graduates[i].processing = true;
        this.graduates[i].processingError = false;
        this.proofOfEnrollmentService.promotional(type, this.periodo, this.graduates)
          .subscribe(
            (graduates) => this.graduates = graduates,
            (error) => {
              this.errorMsg = error as any;
              this.processing = false;
            },
            () => {
              this.processing = false;
              this.getSequentialProofOfEnrollment(i + 1);
              console.log('enviando el proceso => ', i + 1);
              console.log('+--------------------------+');
            },
          );
      } else {
        console.log('no seleccionado');
        this.getSequentialProofOfEnrollment(i + 1);
      }
    } else {
      console.log('nada para procesar => ', this.graduates[i]);
    }
  }

  /**
   * Constancia de Matrícula Promocional individual
   * @param {Graduated} graduated
   */
  getSingleProofOfEnrollment(graduated: Graduated): void {
    console.log('Get single Proof Of Enrollment...');
    const type = 'promotional';
    this.processing = true;
    graduated.processing = true;
    graduated.doc_url = null;
    graduated.save_as = null;
    graduated.download = false;
    this.proofOfEnrollmentService.promotional(type, graduated.other_period, this.graduates)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => {
          this.errorMsg = error as any;
          this.processing = false;
          graduated.processing = false;
        },
        () => {
          this.processing = false;
          graduated.processing = false;
          console.log('student => ', graduated);
          console.log('+--------------------------+');
        },
      );
  }

}
