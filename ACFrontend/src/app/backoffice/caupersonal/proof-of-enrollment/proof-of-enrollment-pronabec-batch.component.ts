import { Component, OnInit } from '@angular/core';
import { ACTermService } from '../../../shared/services/atentioncenter/term.service';
import { Period } from '../../../shared/services/academic/period';
import { ProofOfEnrollmentService } from '../../../shared/services/atentioncenter/proof.of.enrollment.service';
import { ScholarshipStudent } from '../../../shared/services/academic/scholarship-student';
import { StudentService } from '../../../shared/services/academic/student.service';

@Component({
  selector: 'caupersonal-proof-of-enrollment-pronabec-batch',
  templateUrl: './proof-of-enrollment-pronabec-batch.component.html',
  providers: [
    ACTermService,
    ProofOfEnrollmentService,
    StudentService,
  ],
})
export class CAUProofOfEnrollmentPronabecBatchComponent implements OnInit {

  errorMsg: string;
  isSearching: boolean;
  showSearchResult: boolean;
  estudiantes: any;  // modelo para el input de Estudiantes
  periods: string[];  // períodos de matrícula UC
  periodo: any;  // modelo para el select de Períodos académicos de matrícula UC
  students: any[];  // array de 'estudiantes' a procesar
  pronabec: ScholarshipStudent[] = [];  // pronabec
  tuqpa: any[];
  isShowStudentsList: boolean;
  isAnyStudent: boolean;
  selected = false;
  existPeriodo = false;
  processing = false;

  constructor(private acTermService: ACTermService,
    private proofOfEnrollmentService: ProofOfEnrollmentService,
    private studentService: StudentService) {
    this.errorMsg = '';
  }

  ngOnInit() {
    this.clean();
    this.getPeriods();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.errorMsg = '';
    this.isSearching = false;
    this.showSearchResult = false;
    this.estudiantes = undefined;
    this.periodo = undefined;
    this.students = [];
    this.pronabec = [];
    this.tuqpa = [];
    this.isShowStudentsList = false;
    this.isAnyStudent = false;
    this.selected = false;
    this.existPeriodo = false;
    this.processing = false;
  }

  /**
   * Activa/Desactiva el botón "Buscar Estudiantes"
   * @returns {boolean}
   */
  isAllSelected(): boolean {
    return this.estudiantes !== null && this.estudiantes !== undefined && this.estudiantes.trim() !== '';
  }

  /**
   * Activa/Desactiva el botón "Limpiar"
   * @returns {boolean}
   */
  isAnySelected(): boolean {
    return this.estudiantes !== null || this.estudiantes !== undefined || this.estudiantes.trim() !== '';
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray(): void {
    this.isSearching = false;
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // http://stackoverflow.com/a/9229821
    this.students = this.estudiantes.split(' ')
      .filter(// filter blank space
        (id) => id !== '',
      )
      .filter(// filter duplicates
        (id: any, position: any, self: any) => self.indexOf(id) === position,
      );
    console.log('input students => ', this.students);
    console.log('+--------------------------+');
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList(): void {
    this.isShowStudentsList = !this.isShowStudentsList;
  }

  /**
   * Obtiene todos los períodos académicos UC en los que existieron matrícula
   */
  getPeriods(): void {
    console.log('Get Academic Periods...');
    this.acTermService.query()
      .subscribe(
        (periods) => {
          // tslint:disable-next-line:max-line-length
          this.periods = periods.filter(f => f.term >= '201710').map(f => f.term).filter((value, index, self) => self.indexOf(value) === index);
        },
        (error) => this.errorMsg = 'No se cargaron períodos de matrícula',
      );
    console.log('this.periods => ', this.periods);
    console.log('+--------------------------+');
  }

  /**
   * Selección de nuevo período académico
   */
  newPeriod(): void {
    console.log('this.periodo => ', this.periodo);
    this.existPeriodo = !(this.periodo == null || this.periodo === undefined || this.periodo.trim() === '');
    console.log('+--------------------------+');
  }

  /**
   * Estudiantes PRONABEC según código de estudiante
   */
  getPronabec(): void {
    console.log('Get PRONABEC Students...');
    this.isSearching = true;
    this.showSearchResult = false;
    this.tuqpa = [];
    this.pronabec = [];
    this.isAnyStudent = false;
    this.selected = false;
    this.periodo = undefined;
    this.existPeriodo = false;
    this.processing = true;
    const schoolId = '';
    const graduationPeriod = '';
    this.studentService.getPronabecStudents(this.students)
      .subscribe(
        (respuesta) => this.pronabec = respuesta,
        (error) => {
          this.errorMsg = error as any;
        },
        () => {
          this.processing = false;
          this.isSearching = false;
          this.showSearchResult = true;
        },
      );
    console.log('this.pronabec => ', this.pronabec);
    console.log('+--------------------------+');
  }

  /**
   * Selecciona/Deselecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll(): void {
    for (const key of Object.keys(this.pronabec)) {
      this.pronabec[key].isSelected = !this.selected;
    }

    this.selected = !this.selected;
    this.isAnyStudent = this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   * @returns {boolean}
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.pronabec) {
      if (this.pronabec[key].isSelected) {
        return true;
      }
    }
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * Añade estilo a los estudiantes seleccionados
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any): void {
    if (studentObj.isSelected) {
      studentObj.isSelected = false;
      console.log('Desactivado');
      // quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
      // ahora no todos están seleccionados
      // pero comprobemos que a lo menos alguien esté seleccionado
      this.isAnyStudent = this.isAnyStudentSelected() ? this.isAnyStudentSelected() : false;
    } else {
      studentObj.isSelected = true;
      console.log('Activado');
      // agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);
      // console.log("this.tuqpa => ", this.tuqpa);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
      console.log('+--------------------------+');
    }
  }

  /**
   * Constancia de Matrícula PRONABEC
   */
  getProofOfEnrollment(): void {
    console.log('Get Proof of Enrollment...');
    this.getSequentialProofOfEnrollment(0);
    // console.log('+--------------------------+');
  }

  /**
   * Petición secuencial para generar Constancia de Matrícula PRONABEC
   * NOTE: No es "LA" solución para este tipo de peticiones pero es lo que hay
   * @param {number} i
   */
  getSequentialProofOfEnrollment(i: number): void {
    console.log('Sequential!');
    console.log('proceso => ', i);

    if (this.pronabec[i] !== undefined) {
      if (this.pronabec[i].isSelected) {
        this.pronabec[i].docUrl = undefined;
        this.processing = true;
        this.errorMsg = undefined;
        this.pronabec[i].processing = true;
        this.proofOfEnrollmentService.getPronabecProofOfEnrollment(this.pronabec, '', '', this.periodo)
          .subscribe(
            (pronabec) => this.pronabec = pronabec,
            (error) => {
              this.errorMsg = error as any;
              this.pronabec[i].processingError = true;
              this.processing = false;
              this.pronabec[i].docUrl = undefined;
              this.getSequentialProofOfEnrollment(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
            () => {
              this.processing = false;
              this.pronabec[i].processingError = false;
              this.getSequentialProofOfEnrollment(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
          );
      } else {
        console.log('no seleccionado');
        this.getSequentialProofOfEnrollment(i + 1);
      }
    } else {
      console.log('Nada que procesar => ', this.pronabec[i]);
    }
    console.log('+--------------------------+');
  }

}
