import { Component, OnInit } from '@angular/core';

import { College } from '../../../shared/services/academic/college';
import { CollegeService } from '../../../shared/services/academic/college.service';
import { EnrollmentTermService } from '../../../shared/services/academic/enrollmentTerm.service';
import { Period } from '../../../shared/services/academic/period';
import { ProofOfEnrollmentService } from '../../../shared/services/atentioncenter/proof.of.enrollment.service';
import { ReportCardService } from '../../../shared/services/atentioncenter/report.card.service';
import { Scholarship } from '../../../shared/services/academic/scholarship';
import { ScholarshipService } from '../../../shared/services/academic/scholarship.service';
import { ScholarshipStudent } from '../../../shared/services/academic/scholarship-student';
import { ScholarshipStudentService } from '../../../shared/services/academic/scholarship-student.service';

@Component({
  selector: 'caupersonal-proof-of-enrollment-pronabec',
  templateUrl: './proof-of-enrollment-pronabec.component.html',
  providers: [
    CollegeService,
    EnrollmentTermService,
    ProofOfEnrollmentService,
    ReportCardService,
    ScholarshipStudentService,
    ScholarshipService,
  ],
})
export class CAUProofOfEnrollmentPronabecComponent implements OnInit {

  beca: any; // modelo para el select de modalidad PRONABEC
  escuela: any; // modelo para el input de Escuela Académico Profesional
  periodo: any; // modelo para el select de períodos académicos de matrícula UC

  pronabecScholarships: Scholarship[]; // becas UC
  schools: College[]; // Escuela Académico Profesional UC
  periods: Period[];  // períodos de matrícula UC

  errorMsg: string;
  isSearching: boolean;
  showSearchResult: boolean;
  pronabec: ScholarshipStudent[] = [];  // estudiantes pronabec
  tuqpa: any[];
  isShowStudentsList: boolean;
  isAnyStudent: boolean;
  selected = false;
  processing = false;

  existBeca = false;
  existEscuela = false;
  existPeriodo = false;

  constructor(private collegeService: CollegeService,
              private enrollmentTermService: EnrollmentTermService,
              private proofOfEnrollmentService: ProofOfEnrollmentService,
              private reportCardService: ReportCardService,
              private scholarshipService: ScholarshipService,
              private scholarshipStudentService: ScholarshipStudentService) {
    this.errorMsg = '';
  }

  ngOnInit() {
    this.clean();
    this.getPeriods();
    this.getSchools();
    this.getPronabecScholarships();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.beca = undefined;
    this.escuela = undefined;
    this.periodo = undefined;

    this.errorMsg = '';
    this.isSearching = false;
    this.showSearchResult = false;
    this.pronabec = [];
    this.tuqpa = [];
    this.isShowStudentsList = false;
    this.isAnyStudent = false;
    this.selected = false;
    this.processing = false;
    this.existBeca = false;
    this.existEscuela = false;
    this.existPeriodo = false;
  }

  /**
   * Activa/Desactiva el botón "Buscar Estudiantes"
   * @returns {boolean}
   */
  isAllSelected(): boolean {
    let b: boolean;
    let e: boolean;
    let p: boolean;

    b = this.beca !== null && this.beca !== undefined;
    e = this.escuela !== null && this.escuela !== undefined;
    p = this.periodo !== null && this.periodo !== undefined;

    return (b && e && p);
  }

  /**
   * Activa/Desactiva el botón "Limpiar"
   * @returns {boolean}
   */
  isAnySelected(): boolean {
    let b: boolean;
    let e: boolean;
    let p: boolean;

    b = this.beca !== null && this.beca !== undefined;
    e = this.escuela !== null && this.escuela !== undefined;
    p = this.periodo !== null && this.periodo !== undefined;

    return (b || e || p);
  }

  /**
   * Becas PRONABEC
   */
  getPronabecScholarships(): void {
    console.log('Get PRONABEC scholarships...');
    this.scholarshipService.getPronabecScholarships()
      .subscribe(
        (pronabecScholarships) => this.pronabecScholarships = pronabecScholarships,
        (error) => this.errorMsg = error as any,
        () => {
          console.log('this.pronabecScholarships => ', this.pronabecScholarships);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Escuela Académico Profesional
   */
  getSchools(): void {
    console.log('Get Schools...');
    this.collegeService.query()
      .subscribe(
        (schools) => this.schools = schools,
        (error) => this.errorMsg = error as any,
        () => {
          console.log('this.schools => ', this.schools);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Obtiene todos los períodos académicos UC en los que existieron matrícula
   */
  getPeriods(): void {
    console.log('Get Academic Periods...');
    this.enrollmentTermService.getAllEnrollmentPeriods()
      .subscribe(
        (periods) => this.periods = periods,
        (error) => this.errorMsg = error as any,
        () => {
          console.log('this.periods => ', this.periods);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Selección de nueva beca
   */
  newScholarship(): void {
    console.log('this.beca => ', this.beca);
    this.existBeca = !(this.beca == null || this.beca === undefined);
    console.log('+--------------------------+');
  }

  /**
   * Selección de nueva Escuela Académico Profesional
   */
  newSchool(): void {
    console.log('this.escuela => ', this.escuela);
    this.existEscuela = !(this.escuela == null || this.escuela === undefined);
    console.log('+--------------------------+');
  }

  /**
   * Selección de nuevo período académico
   */
  newPeriod(): void {
    console.log('this.periodo => ', this.periodo);
    this.existPeriodo = !(this.periodo == null || this.periodo === undefined);
    console.log('+--------------------------+');
  }

  /**
   * Estudiantes PRONABEC según modalidad PRONABEC, Escuela Académico Profesional y período académico
   */
  getPronabec(): void {
    console.log('Get PRONABEC Students...');
    this.isSearching = true;
    this.showSearchResult = false;
    this.tuqpa = [];
    this.pronabec = [];
    this.isAnyStudent = false;
    this.selected = false;
    this.existBeca = false;
    this.existEscuela = false;
    this.existPeriodo = false;
    this.processing = true;
    this.scholarshipStudentService.getPronabecScholarshipStudents(this.beca, this.escuela, this.periodo)
      .subscribe(
        (result) => this.pronabec = result,
        (error) => {
          this.errorMsg = error as any;
          this.isSearching = false;
          this.processing = false;
        },
        () => {
          this.processing = false;
          this.isSearching = false;
          this.showSearchResult = true;
          console.log('this.pronabec => ', this.pronabec);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Selecciona/Deselecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll(): void {
    for (const key of Object.keys(this.pronabec)) {
      this.pronabec[key].isSelected = !this.selected;
    }

    this.selected = !this.selected;
    this.isAnyStudent = this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   * @returns {boolean}
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.pronabec) {
      if (this.pronabec[key].isSelected) {
        return true;
      }
    }
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * Añade estilo a los estudiantes seleccionados
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any): void {
    if (studentObj.isSelected) {
      studentObj.isSelected = false;
      console.log('Desactivado');
      // quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
      // ahora no todos están seleccionados
      // pero comprobemos que a lo menos alguien esté seleccionado
      this.isAnyStudent = this.isAnyStudentSelected() ? this.isAnyStudentSelected() : false;
    } else {
      studentObj.isSelected = true;
      console.log('Activado');
      // agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);
      // console.log("this.tuqpa => ", this.tuqpa);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
      console.log('+--------------------------+');
    }
  }

  /**
   * Constancia de Matrícula PRONABEC
   */
  getProofOfEnrollment(): void {
    console.log('Get Proof of enrollment...');
    this.getSequentialProofOfEnrollment(0);
    console.log('pronabec => ', this.pronabec);
  }

  /**
   * Petición secuencial para generar Constancia de Matrícula PRONABEC
   * NOTE: No es "LA" solución para este tipo de peticiones pero es lo que hay
   * @param {number} i
   */
  getSequentialProofOfEnrollment(i: number): void {
    console.log('Sequential!');
    console.log('proceso => ', i);

    if (this.pronabec[i] !== undefined) {
      if (this.pronabec[i].isSelected) {
        this.pronabec[i].docUrl = undefined;
        this.processing = true;
        this.errorMsg = undefined;
        this.pronabec[i].processing = true;
        this.proofOfEnrollmentService.getPronabecProofOfEnrollment(this.pronabec, this.beca, this.escuela, this.periodo)
          .subscribe(
            (pronabec) => this.pronabec = pronabec,
            (error) => {
              this.errorMsg = error as any;
              this.pronabec[i].processingError = true;
              this.processing = false;
              this.pronabec[i].docUrl = undefined;
              this.getSequentialProofOfEnrollment(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
            () => {
              this.processing = false;
              this.pronabec[i].processingError = false;
              this.getSequentialProofOfEnrollment(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
          );
      } else {
        console.log('no seleccionado');
        this.getSequentialProofOfEnrollment(i + 1);
      }
    } else {
      console.log('Nada que procesar => ', this.pronabec[i]);
    }
    console.log('+--------------------------+');
  }

}
