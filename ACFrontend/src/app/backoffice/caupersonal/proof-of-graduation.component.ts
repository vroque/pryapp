import { ConfigService } from '../../config/config.service';

declare let $: any;
import {CollegeService} from '../../shared/services/academic/college.service';
import {Component, OnInit} from '@angular/core';
import {EnrollmentTermService} from '../../shared/services/academic/enrollmentTerm.service';
import {Graduated} from '../../shared/services/academic/graduated';
import {GraduationService} from '../../shared/services/academic/graduation.service';
import {Period} from '../../shared/services/academic/period';
import {ProofOfEntryService} from '../../shared/services/atentioncenter/proof-of-entry.service';

@Component({
  selector: 'caupersonal-proof-of-graduation',
  templateUrl: './proof-of-graduation.component.html',
  providers: [
    CollegeService,
    EnrollmentTermService,
    GraduationService,
    ProofOfEntryService,
    ConfigService,
  ],
})

export class CAUProofOfGraduationComponent implements OnInit {

  errorMsg: string;
  isSearching: boolean;
  estudiantes: any;  // modelo para el input de Estudiantes
  periods: Period;  // períodos de matrícula UC
  fecha: Date;  // modelo para el input de fecha de egresado
  students: any[];  // array de 'estudiantes' a procesar
  graduates: Graduated[];  // graduados UC
  tuqpa: any[];
  isShowStudentsList: boolean;
  isAnyStudent: boolean;
  selected = false;
  existFecha = false;
  es: any;
  wata: number;
  isProcessing: boolean;

  constructor(private  graduationService: GraduationService, private configService: ConfigService) {
    this.errorMsg = '';
  }

  ngOnInit() {
    this.clean();
    this.es = this.configService.getCalendarEs();
    this.wata = (new Date()).getFullYear();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.errorMsg = '';
    this.isSearching = false;
    this.estudiantes = undefined;
    this.fecha = undefined;
    this.students = [];
    this.graduates = undefined;
    this.tuqpa = [];
    this.isShowStudentsList = false;
    this.isAnyStudent = false;
    this.selected = false;
    this.existFecha = false;
    this.isProcessing = false;
  }

  /**
   * Activa/Desactiva el botón "Buscar Estudiantes"
   * @returns {boolean}
   */
  isAllSelected(): boolean {
    return !(this.estudiantes == null || this.estudiantes === undefined || this.estudiantes.trim() === '');
  }

  /**
   * Activa/Desactiva el botón "Limpiar"
   * @returns {boolean}
   */
  isAnySelected(): boolean {
    return !(this.estudiantes == null || this.estudiantes === undefined || this.estudiantes.trim() === '');
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray(): void {
    this.isSearching = false;
    this.isProcessing = false;
    this.errorMsg = undefined;
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // http://stackoverflow.com/a/9229821
    this.students = this.estudiantes.split(' ')
      .filter(// filter blank space
        (id) => id !== '',
      )
      .filter(// filter duplicates
        (id, position, self) => self.indexOf(id) === position,
      );
    console.log('input students => ', this.students);
    console.log('+--------------------------+');
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList(): void {
    this.isShowStudentsList = !this.isShowStudentsList;
  }

  /**
   * Egresados UC según código de estudiante
   */
  getGraduates(): void {
    console.log('Get Graduates...');
    this.isSearching = true;
    this.isProcessing = false;
    this.errorMsg = undefined;
    this.tuqpa = [];
    this.graduates = undefined;
    this.isAnyStudent = false;
    this.selected = false;
    this.fecha = undefined;
    this.existFecha = false;
    const type = 'searchBatch';
    const schoolId = '';
    const graduationPeriod = '';
    this.graduationService.getGraduates(type, schoolId, graduationPeriod, this.students)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => this.errorMsg = error as any,
        () => this.isSearching = false,
      );
    console.log('this.graduates => ', this.graduates);
    console.log('+--------------------------+');
  }

  /**
   * Selecciona/Deselecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll(): void {
    for (const key in this.graduates) {
      if (this.graduates[key].is_selected || !this.graduates[key].is_selected) {
        this.graduates[key].is_selected = !this.selected;
      }
    }

    this.selected = !this.selected;
    this.isAnyStudent = this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   * @returns {boolean}
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.graduates) {
      if (this.graduates[key].is_selected) {
        return true;
      }
    }
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * Añade estilo a los estudiantes seleccionados
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any): void {
    if (studentObj.is_selected) {
      studentObj.is_selected = false;
      console.log('Desactivado');
      // quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
      // ahora no todos están seleccionados
      // pero comprobemos que a lo menos alguien esté seleccionado
      this.isAnyStudent = this.isAnyStudentSelected() ? this.isAnyStudentSelected() : false;
    } else {
      studentObj.is_selected = true;
      console.log('activado');
      // agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
      console.log('+--------------------------+');
    }
  }

  /**
   * Constancia de Egresado
   */
  getProofOfGraduation(): void {
    console.log('Get Proof Of Graduation...');
    this.getSequentialProofOfGraduation(0);
  }

  /**
   * Petición secuencial para generar Constancia de Egresado
   * NOTE: No es "LA" solución para este tipo de peticiones pero es lo que hay
   * @param {number} i
   */
  getSequentialProofOfGraduation(i: number): void {
    console.log('Sequential!');

    console.log('proceso => ', i);

    if (this.graduates[i] !== undefined) {
      if (this.graduates[i].is_selected) {
        this.graduates[i].doc_url = undefined;
        this.isProcessing = true;
        this.errorMsg = undefined;
        this.graduates[i].processing = true;
        this.graduationService.getProofOfGraduation(this.graduates, this.fecha)
          .subscribe(
            (graduates) => this.graduates = graduates,
            (error) => {
              this.errorMsg = error as any;
              this.graduates[i].processingError = true;
              this.isProcessing = false;
              this.graduates[i].doc_url = undefined;
              this.getSequentialProofOfGraduation(i + 1);
              console.log('enviando el proceso => ', i + 1);
            },
            () => {
              this.isProcessing = false;
              this.graduates[i].processingError = false;
              this.getSequentialProofOfGraduation(i + 1);
              console.log('enviando el proceso => ', i + 1);
            },
          );
      } else {
        console.log('no seleccionado');
        this.getSequentialProofOfGraduation(i + 1);
      }
    } else {
      console.log('nada que procesar => ', this.graduates[i]);
    }
    console.log('+--------------------------+');
  }
}
