import { BackofficeConfigService } from '../config/config.service';
import { Component, OnInit } from '@angular/core';
import { Request } from '../../shared/services/atentioncenter/request';
import { RequestService } from '../../shared/services/atentioncenter/request.service';
import { Router } from '@angular/router';
import { GraduateSearchService } from '../../shared/services/academic/graduate.search.service';

@Component({
  selector: 'request-list',
  templateUrl: './graduate.proof.component.html',
  providers: [ RequestService, BackofficeConfigService,GraduateSearchService ],
})
export class CAUGraduateProofComponent implements OnInit {

  dnis: string;
  graduates: any[];
  graduates_to_process: any[] = [];
  error_message: string;
  requests: Request[];
  requests_active: Request[] = [];
  r_atention_pending: Request[] = [];
  r_delivery_pending: Request[] = [];
  r_proccess_pending: Request[] = [];
  r_document_reception: Request[] = [];
  r_final: Request[] = [];
  r_cancelations: Request[] = [];
  r_all: Request[] = [];
  area = '';

  constructor(
    private router: Router,
    private graduateService: GraduateSearchService,
    private requestService: RequestService,
    private config: BackofficeConfigService
  ) { }

  ngOnInit(): void { }

  searchGraduates() {
    console.log(this.dnis);
    this.graduateService.searchForDNIs(this.dnis)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) =>  this.error_message = <any>error,
        () => this.graduates.forEach(function(item, index) {
          (item as any).danger = !item.language && !item.practice && !item.projection;
          (item as any).info = item.language  && item.practice && item.projection  && item.bachelor;
          (item as any).success = item.language  && item.practice && item.projection && item.bachelor !== true && item.degree !== true;
          (item as any).warning = !item.language  || !item.practice  || !item.projection;
        }),
      );
  }
  toProcess(graduate) {
    console.log(graduate);
    graduate.to_process = true;
    this.graduates_to_process.push(graduate);
  }
  removetoProcess(graduate): void {
    console.log(graduate);
    graduate.to_process = false;
    this.graduates_to_process = this.graduates_to_process.filter(
      (obj) => {
        if (obj.dni !== graduate.dni) { return obj; }
      },
    );
  }

  updateDates(generalDate): void {
  this.graduates_to_process.forEach((item, index) => {
    (item as any).date = generalDate;
  });
  }

  generate(): void {
    console.log(this.graduates_to_process);
  }
}
