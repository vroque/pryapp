import { ActivatedRoute, Params } from '@angular/router';
import { BackofficeConfigService } from '../config/config.service';
import { Component, OnInit } from '@angular/core';
import { DocumentState } from '../../shared/services/atentioncenter/document.state';
import { DocumentStateService } from '../../shared/services/atentioncenter/document.state.service';
import { GenerateDocumentService } from '../../shared/services/atentioncenter/generate.document.service';
import { MailService } from '../../shared/services/mail/mail.service';
import { Request } from '../../shared/services/atentioncenter/request';
import { RequestLog } from '../../shared/services/atentioncenter/request.log';
import { RequestLogService } from '../../shared/services/atentioncenter/request.log.service';
import { RequestService } from '../../shared/services/atentioncenter/request.service';

@Component({
  selector: 'request-list',
  templateUrl: './request.component.html',
  providers: [
    MailService,
    RequestService,
    RequestLogService,
    DocumentStateService,
    GenerateDocumentService,
    BackofficeConfigService
  ],
})
export class CAURequestComponent implements OnInit {

  area = '';
  derivate_message = 'Para su atención.';
  doc_status = 'pre'; // pre, on, err
  error_message: string;
  flow: DocumentState[];
  ideal_flow: DocumentState[];
  is_derivate = false;
  logs: RequestLog[];
  request: Request;
  signatories: Array<string>;
  mail: any = {sending: false, sended: false, error: false};

  constructor(
    private _route: ActivatedRoute,
    private requestService: RequestService,
    private requestLogService: RequestLogService,
    private documentStateService: DocumentStateService,
    private generateDocumentService: GenerateDocumentService,
    private mailService: MailService,
    private config: BackofficeConfigService,
  ) { }

  ngOnInit() {
    this.request = null;
    this.flow = [];
    this.ideal_flow = [];
    this.logs = [];
    this._route.params.forEach((params: Params) => {
      const requestId: string = (<any>params).request;
      this.getRequest(requestId);
      this.getRequestLogs(requestId);
    });
  }

  getRequest(requestId: string) {
    this.requestService.get(requestId)
      .subscribe(
        (request) => this.request = request,
        (error) =>  this.error_message = error,
        () => {
            this.request.document_uri = this.request.document_uri || [];
            this.getDocumentFlow(this.request.document_id);
            (this.request as any).conva_type = JSON.parse(this.request.description).conva_type || '';
          }
      );
  }

  updateLogs(event: string) {
    this.getRequestLogs(String(this.request.id));
    this.request.state = this.config.STATE_DELIVERY_PENDING;
  }

  getRequestLogs(requestId: string) {
    this.requestLogService.query(requestId)
      .subscribe(
        (logs) => this.logs = logs,
        (error) =>  this.error_message = <any>error,
        () => console.log('logs -> ', this.logs),
      );
  }

  getDocumentFlow(documentId: number) {
    this.documentStateService.query(documentId)
      .subscribe(
        (flow) => this.flow = flow,
        (error) =>  this.error_message = <any>error,
        () => this.getIdealFlow(this.flow),
      );
  }

  getIdealFlow(flow: DocumentState[]) {
    this.ideal_flow = flow.filter((state) => (state.order !== -1) );
  }

  getLogTrace(state: number): RequestLog {
    for (let i = 0; i < this.logs.length; i++) {
      if (this.logs[i].old_state === state) {
        return this.logs[i];
      }
    }
    return null;
  }

  /**
   * marca la solicitud como entregada l
   */
  deliveryRequest(event: Event, requestId: string) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.requestService.save(requestId, {})
      .subscribe(
        (request) => {
          this.request = request;
          this.getRequestLogs(String(request.id));
        },
        (error) =>  this.error_message = error,
      );
  }

  derivateRequest(event: Event, requestId: string, message: string) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.requestService.update(requestId, {message: message, type: 'derive' })
      .subscribe(
        (request) => {
          this.request = request;
        },
        (error) =>  this.error_message = error,
        () => {
          this.getRequestLogs(String(this.request.id));
          this.is_derivate = true;
          target.disabled = false;
        },
      );
  }

  /*
   * enviar correo con los documentos generados
  */
  sendDocumentByMail(event: Event) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.mail.sending = true;
    this.mailService.requestDocument(this.request.id)
      .subscribe(
        (data) => console.log(data),
        (error) => {
          this.mail.sending = false;
          this.mail.error = true;
        }, () => {
          target.disabled = true;
          this.mail.sending = false;
          this.mail.sended = true;
        },
      );
  }

  /*
   * Enviar el parametro conva_type a la descripción de la solicitud
  */
  setConvaType(type: string) {
    const data: any[] = [{name: 'conva_type', value: type}];
    this.requestService.updateAbstract(this.request.id, data)
      .subscribe(
        (dat) => console.log(dat),
        (error) =>  this.error_message = error,
      );
  }
}
