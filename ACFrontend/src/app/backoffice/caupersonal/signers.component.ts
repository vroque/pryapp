import { BackofficeConfigService } from '../config/config.service';
import { Component, OnInit,  } from '@angular/core';
import { ConfigService } from '../../config/config.service';
import { DocumentSigner } from '../../shared/services/report/documentSigner';
import { DocumentSignerService } from '../../shared/services/report/documentSigner.service';

@Component({
  selector: 'sign-doc-report',
  templateUrl: './signers.component.html',
  providers: [ DocumentSignerService, BackofficeConfigService, ConfigService ]
})
export class CAUSignerDocumentComponent implements OnInit {

  error_message: string;
  report: DocumentSigner[];
  r_dates: any;
  r_docs: any;
  main_backgrounds: string[] = ['#FF6384', '#36A2EB', '#FFCE56'];

  constructor(
    private documentSignerService: DocumentSignerService,
    private config: BackofficeConfigService,
    private app_config: ConfigService
  ) { }

  ngOnInit() { }

  setSigner(personId: string) {
    this.documentSignerService.query(+personId)
      .subscribe(
        (report) => this.report = report,
        (error) =>  this.error_message = <any>error,
        () => {
          this.composeReportByDate();
          this.composeReportByDocument();
        },
      );
  }

  composeReportByDate() {
    const labels: string[] = [];
    const captionLabels: string[] = [];
    for (const item of this.report) {
      const label = `${item.year}-${item.month}`;
      const fl: string[] = labels.filter(element => element === label);
      if (fl.length <= 0) {
        labels.push(label);
        captionLabels.push(`${this.app_config.getMonthName(item.month)} de ${item.year}.`);
      }
    }
    const datas: number[] = [];
    const bgs: string[] = [];
    let i = 0;
    for (const item of labels) {
      const y: number = +item.split('-')[0];
      const m: number = +item.split('-')[1];
      const list: Array<DocumentSigner> = this.report.filter(element => (element.year === y && element.month === m) );
      let data = 0;
      for (const doc of list) { data += doc.quantity; }
      datas.push(data);
      if (i >= this.main_backgrounds.length) { i = 0; }
      bgs.push(this.main_backgrounds[i]);
      i++;
    }
    this.r_dates = {
      labels: captionLabels,
      datasets: [
        {
          data: datas,
          backgroundColor: bgs,
          hoverBackgroundColor: bgs
        }
      ]
    };
  }

  composeReportByDocument() {
    const labels: string[] = [];
    const captionLabels: string[] = [];
    for (const item of this.report) {
      const label: string = item.document_id + '';
      const fl: string[] = labels.filter(element => element === label);
      if (fl.length <= 0) {
        labels.push(label);
        captionLabels.push(this.config.getDocumentName(item.document_id));
      }
    }
    const datas: number[] = [];
    const bgs: string[] = [];
    let i = 0;
    for (const item of labels) {
      const list: Array<DocumentSigner> = this.report.filter(element => element.document_id === +item );
      let data = 0;
      for (const doc of list) { data += doc.quantity; }
      datas.push(data);
      if (i >= this.main_backgrounds.length) { i = 0; }
      bgs.push(this.main_backgrounds[i]);
      i++;
    }
    this.r_docs = {
      labels: captionLabels,
      datasets: [
        {
          data: datas,
          backgroundColor: bgs,
          hoverBackgroundColor: bgs
        }
      ]
    };
  }
}
