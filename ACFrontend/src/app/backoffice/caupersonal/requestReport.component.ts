import { Component, OnInit,  } from '@angular/core';
import { ConfigService } from '../../config/config.service';
import { RequestReport } from '../../shared/services/report/requestReport';
import { RequestReportService } from '../../shared/services/report/requestReport.service';
import { BackofficeConfigService } from '../config/config.service';

@Component({
  selector: 'request-report',
  templateUrl: './requestReport.component.html',
  providers: [ BackofficeConfigService, ConfigService, RequestReportService ]
})
export class CAURequestReportComponent implements OnInit {

  error_message: string;
  main_backgrounds: string[] = ['#FF6384', '#36A2EB', '#FFCE56'];
  report: RequestReport[];
  report_bar: any;
  report_pie: any;

  constructor(
    private config: ConfigService,
    private bo_config: BackofficeConfigService,
    private requestReportService: RequestReportService
  ) { }

  ngOnInit(): void {
    this.requestReportService.query()
    .subscribe(
      (report) => this.report = report,
      (error) =>  this.error_message = <any>error,
      () => {
        this.composeRequestBarReport();
        this.composeRequestPieReport();
      },
    );
  }

  composeRequestBarReport(): void {
    // a comment
    const labels: string[] = [];
    const captionLabels: string[] = [];
    for (const item of this.report) {
      const label = `${item.year}-${item.month}`;
      const fl: string[] = labels.filter(element => element === label);
      if (fl.length <= 0) {
        labels.push(label);
        captionLabels.push(`${this.config.getMonthName(item.month)} de ${item.year}.`);
      }
    }
    const datas: Array<number> = [];
    for (const item of labels) {
      const y: number = +item.split('-')[0];
      const m: number = +item.split('-')[1];
      const list: Array<RequestReport> = this.report.filter(element => (element.year === y && element.month === m) );
      let data = 0;
      for (const doc of list) { data += doc.quantity; }
      datas.push(data);
    }

    this.report_bar = {
      labels: captionLabels,
      datasets: [
        {
          label: 'Reporte de Solicitudes Mensuales',
          backgroundColor: '#42A5F5',
          borderColor: '#1E88E5',
          data: datas
        }
      ]
    };
  }
  composeRequestPieReport(): void {
    const labels: string[] = [];
    const captionLabels: string[] = [];
    for (const item of this.report) {
      const label: string = item.document_id + '';
      const fl: string[] = labels.filter(element => element === label);
      if (fl.length <= 0) {
        labels.push(label);
        captionLabels.push(this.bo_config.getDocumentName(item.document_id));
      }
    }
    const datas: Array<number> = [];
    const bgs: string[] = [];
    let i = 0;
    for (const item of labels) {
      const list: Array<RequestReport> = this.report.filter(element => element.document_id === +item );
      let data = 0;
      for (const doc of list) { data += doc.quantity; }
      datas.push(data);
      if (i >= this.main_backgrounds.length) { i = 0; }
      bgs.push(this.main_backgrounds[i]);
      i++;
    }
    this.report_pie = {
      labels: captionLabels,
      datasets: [
        {
          data: datas,
          backgroundColor: bgs,
          hoverBackgroundColor: bgs
        }
      ],
      options: {
        responsive: true,
        legend: {
            position: 'right',
        },
      }
};
  }

}
