import {CollegeService} from '../../../shared/services/academic/college.service';
import {Component, OnInit} from '@angular/core';
import {Graduated} from '../../../shared/services/academic/graduated';
import {GraduationService} from '../../../shared/services/academic/graduation.service';
import {ProofOfEntryService} from '../../../shared/services/atentioncenter/proof-of-entry.service';

@Component({
  selector: 'caupersonal-proof-of-entry-promotional-batch',
  templateUrl: './proof-of-entry-promotional-batch.component.html',
  providers: [
    CollegeService,
    GraduationService,
    ProofOfEntryService,
  ],
})
export class CAUProofOfEntryPromotionalBatchComponent implements OnInit {

  errorMsg: string;
  isSearching: boolean;
  estudiantes: any;  // modelo para el input de Estudiantes
  students: any[];  // array de 'estudiantes' a procesar
  graduates: Graduated[];  // graduados UC
  tuqpa: any[];
  isShowStudentsList: boolean;
  isAnyStudent: boolean;
  selected = false;
  processing = false;

  constructor(private  graduationService: GraduationService,
              private proofOfEntryService: ProofOfEntryService) {
    this.errorMsg = '';
  }

  ngOnInit() {
    this.clean();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.errorMsg = '';
    this.isSearching = false;
    this.estudiantes = undefined;
    this.students = [];
    this.graduates = undefined;
    this.tuqpa = [];
    this.isShowStudentsList = false;
    this.isAnyStudent = false;
    this.selected = false;
    this.processing = false;
  }

  /**
   * Bandera para activar/desactivar el botón "Buscar Estudiantes"
   * @return {boolean} [description]
   */
  isAllSelected(): boolean {
    return !(this.estudiantes == null || this.estudiantes === undefined || this.estudiantes.trim() === '');
  }

  /**
   * Bandera para activar/desactivar el botón "Limpiar"
   * @return {boolean} [description]
   */
  isAnySelected(): boolean {
    return !(this.estudiantes == null || this.estudiantes === undefined || this.estudiantes.trim() === '');
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray(): void {
    this.isSearching = false;
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // http://stackoverflow.com/a/9229821
    this.students = this.estudiantes.split(' ')
      .filter(// filter blank space
        (id) => id !== '',
      )
      .filter(// filter duplicates
        (id, position, self) => self.indexOf(id) === position,
      );
    console.log('input students => ', this.students);
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList(): void {
    this.isShowStudentsList = !this.isShowStudentsList;
  }

  /**
   * Egresados UC según código de estudiante
   */
  getGraduates(): void {
    console.log('Get Graduates...');
    this.isSearching = true;
    this.tuqpa = [];
    this.graduates = undefined;
    this.isAnyStudent = false;
    this.selected = false;
    const type = 'searchBatch';
    const schoolId = '';
    const graduationPeriod = '';
    this.processing = true;
    this.graduationService.getGraduates(type, schoolId, graduationPeriod, this.students)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => {
          this.errorMsg = error as any;
          this.processing = false;
          this.isSearching = false;
        },
        () => {
          this.processing = false;
          this.isSearching = false;
          console.log('this.graduates => ', this.graduates);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Selecciona/Deselecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll(): void {
    for (const key of Object.keys(this.graduates)) {
      this.graduates[key].is_selected = !this.selected;
    }

    this.selected = !this.selected;
    this.isAnyStudent = this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.graduates) {
      if (this.graduates[key].is_selected) {
        return true;
      }
    }
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * Añade estilo a los estudiantes seleccionados
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any): void {
    if (studentObj.is_selected) {
      studentObj.is_selected = false;
      console.log('Desactivado');
      // quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
      // ahora no todos están seleccionados
      // pero comprobemos que a lo menos alguien esté seleccionado
      this.isAnyStudent = this.isAnyStudentSelected() ? this.isAnyStudentSelected() : false;
    } else {
      studentObj.is_selected = true;
      console.log('activado');
      // agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);
      // console.log("this.tuqpa => ", this.tuqpa);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
      // aquí ya no es necesario verificar si alguien está seleccionado xD
    }
  }

  /**
   * Constancias de Ingreso
   */
  getProofOfEntry(): void {
    console.log('Get Proof of Entry...');
    this.getSequentialProofOfEntry(0);
    console.log('graduates => ', this.graduates);
  }

  /**
   * Petición secuencial para generar Constancia de Ingreso Promocional
   * NOTE: No es "LA" solución para este tipo de peticiones pero es lo que hay
   * @param {number} i
   */
  getSequentialProofOfEntry(i: number): void {
    console.log('Sequential!');
    console.log('proceso => ', i);
    const type = 'promotional';

    if (this.graduates[i] !== undefined) {
      if (this.graduates[i].is_selected) {
        this.graduates[i].doc_url = undefined;
        this.processing = true;
        this.errorMsg = undefined;
        this.graduates[i].processing = true;
        this.proofOfEntryService.getProofOfEntry(type, this.graduates)
          .subscribe(
            (graduates) => this.graduates = graduates,
            (error) => {
              this.errorMsg = error as any;
              this.graduates[i].processingError = true;
              this.processing = false;
              this.graduates[i].doc_url = undefined;
              this.getSequentialProofOfEntry(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
            () => {
              this.processing = false;
              this.graduates[i].processingError = false;
              this.getSequentialProofOfEntry(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
          );
      } else {
        console.log('no seleccionado');
        this.getSequentialProofOfEntry(i + 1);
      }
    } else {
      console.log('Nada que procesar => ', this.graduates[i]);
    }
    console.log('+--------------------------+');
  }

}
