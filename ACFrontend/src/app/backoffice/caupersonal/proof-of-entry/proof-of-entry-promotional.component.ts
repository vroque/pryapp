import {CollegeService} from '../../../shared/services/academic/college.service';
import {Component, OnInit} from '@angular/core';
import {Entrant} from '../../../shared/services/academic/entrant';
import {Graduated} from '../../../shared/services/academic/graduated';
import {GraduationService} from '../../../shared/services/academic/graduation.service';
import {Period} from '../../../shared/services/academic/period';
import {ProofOfEntryService} from '../../../shared/services/atentioncenter/proof-of-entry.service';
import {ProofOfEntry} from '../../../shared/services/atentioncenter/proof-of-entry';

@Component({
  selector: 'caupersonal-proof-of-entry-promotional',
  templateUrl: './proof-of-entry-promotional.component.html',
  providers: [
    CollegeService,
    GraduationService,
    ProofOfEntryService,
  ],
})
export class CAUProofOfEntryPromotionalComponent implements OnInit {

  errorMsg: string;
  isSearching: boolean;
  escuela: any;  // modelo para el select de Escuela Académico Profesional
  periodoEgreso: any;  // modelo para el select de Período académico de egreso a la UC
  schools: any;
  periods: Period[];
  entrants: Entrant;
  graduates: Graduated[];
  yaykuqRunaKuna: ProofOfEntry;
  tuqpa: any[];
  isAnyStudent: boolean;
  selected = false;
  processing = false;

  constructor(private collegeService: CollegeService,
              private graduationService: GraduationService,
              private proofOfEntryService: ProofOfEntryService) {
    this.errorMsg = '';
  }

  ngOnInit() {
    this.clean();
    this.getSchool();
    this.getGraduationPeriods();
  }

  /**
   * Limpiar modelos
   */
  clean() {
    this.errorMsg = '';
    this.isSearching = false;
    this.escuela = '';
    this.periodoEgreso = '';
    this.entrants = undefined;
    this.graduates = undefined;
    this.yaykuqRunaKuna = undefined;
    this.tuqpa = [];
    this.isAnyStudent = false;
    this.selected = false;
    this.processing = false;
  }

  /**
   * Nueva búsqueda.
   * Limpia los resultados de búsqueda
   */
  newSearch() {
    this.isSearching = false;
    this.entrants = undefined;
    this.graduates = undefined;
    this.yaykuqRunaKuna = undefined;
    this.tuqpa = [];
  }

  /**
   * Bandera para activar/desactivar el botón "Buscar Estudiantes"
   * @return {boolean} [description]
   */
  isAllSelected(): boolean {
    let isEscuelaValid: boolean;
    let isPeriodoEgresoValid: boolean;

    isEscuelaValid = !(this.escuela == null || this.escuela === undefined || this.escuela === '');

    isPeriodoEgresoValid =
      !(this.periodoEgreso == null || this.periodoEgreso === undefined || this.periodoEgreso === '');

    return isEscuelaValid && isPeriodoEgresoValid;
  }

  /**
   * Bandera para activar/desactivar el botón "Limpiar"
   * @return {boolean} [description]
   */
  isAnySelected(): boolean {
    let isEscuelaValid: boolean;
    let isPeriodoEgresoValid: boolean;

    isEscuelaValid = !(this.escuela == null || this.escuela === undefined || this.escuela === '');

    isPeriodoEgresoValid =
      !(this.periodoEgreso == null || this.periodoEgreso === undefined || this.periodoEgreso === '');

    return isEscuelaValid || isPeriodoEgresoValid;
  }

  /**
   * Escuela Académico Profesional
   */
  getSchool() {
    console.log('get schools...');
    this.collegeService.query()
      .subscribe(
        (schools) => this.schools = schools,
        (error) => this.errorMsg = error as any,
        () => {
          console.log('this.schools => ', this.schools);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Períodos de Estudiantes Egresados UC
   */
  getGraduationPeriods() {
    console.log('Get Graduation Periods...');
    this.graduationService.getGraduationPeriods()
      .subscribe(
        (periods) => this.periods = periods,
        (error) => this.errorMsg = error as any,
        () => {
          console.log('this.periods => ', this.periods);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Egresados UC
   * @param {string} schoolId Código de Escuela Académico Profesional
   * @param {string} graduationPeriod Período de graduación UC
   */
  getGraduates(schoolId: string, graduationPeriod: string) {
    console.log('Get Graduates...');
    this.isSearching = true;
    this.tuqpa = [];
    this.graduates = undefined;
    const type = 'search';
    const estudiantes: any[] = [];
    this.isAnyStudent = false;
    this.selected = false;
    this.processing = true;
    this.graduationService.getGraduates(type, schoolId, graduationPeriod, estudiantes)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => {
          this.errorMsg = error as any;
          this.processing = false;
          this.isSearching = false;
        },
        () => {
          this.processing = false;
          this.isSearching = false;
          console.log('this.graduates => ');
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Selecciona/Deselecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll() {
    for (const key of Object.keys(this.graduates)) {
      this.graduates[key].is_selected = !this.selected;
    }

    this.selected = !this.selected;
    this.isAnyStudent = this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.graduates) {
      if (this.graduates[key].is_selected) {
        return true;
      }
    }
  }

  /**
   * Estudiantes a procesar
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any) {

    if (studentObj.is_selected) {
      studentObj.is_selected = false;
      console.log('desactivado');
      // quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
    } else {
      studentObj.is_selected = true;
      console.log('activado');
      // agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);
      // console.log("this.tuqpa => ", this.tuqpa);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
    }

  }

  /**
   * Constancias de Ingreso
   * @param {any[]} students Arreglo de Objeto de Estudiantes
   */
  getProofOfEntry(students: any[]) {
    console.log('Get Proof of Entry...');
    this.getSequentialProofOfEntry(0);
    console.log('graduates => ', this.graduates);
  }

  /**
   * Petición secuencial para generar Constancia de Ingreso Promocional
   * NOTE: No es "LA" solución para este tipo de peticiones pero es lo que hay
   * @param {number} i
   */
  getSequentialProofOfEntry(i: number) {
    console.log('Sequential!');
    console.log('proceso => ', i);
    const type = 'promotional';

    if (this.graduates[i] !== undefined) {
      if (this.graduates[i].is_selected) {
        this.graduates[i].doc_url = undefined;
        this.processing = true;
        this.errorMsg = undefined;
        this.graduates[i].processing = true;
        this.proofOfEntryService.getProofOfEntry(type, this.graduates)
          .subscribe(
            (graduates) => this.graduates = graduates,
            (error) => {
              this.errorMsg = error as any;
              this.graduates[i].processingError = true;
              this.processing = false;
              this.graduates[i].doc_url = undefined;
              this.getSequentialProofOfEntry(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
            () => {
              this.processing = false;
              this.graduates[i].processingError = false;
              this.getSequentialProofOfEntry(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
          );
      } else {
        console.log('no seleccionado');
        this.getSequentialProofOfEntry(i + 1);
      }
    } else {
      console.log('Nada que procesar => ', this.graduates[i]);
    }
  }

}
