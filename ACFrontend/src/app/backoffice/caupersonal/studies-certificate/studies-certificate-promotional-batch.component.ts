import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { BackofficeConfigService } from '../../config/config.service';
import { ConfigService } from '../../../config/config.service';
import { Graduated } from '../../../shared/services/academic/graduated';
import { GraduationService } from '../../../shared/services/academic/graduation.service';
import { PromotionalStudyCertificateService } from '../../../shared/services/atentioncenter/promotional-study-certificate.service';
import { MomentService } from '../../../shared/moment.service';
import { Request } from '../../../shared/services/atentioncenter/request';

@Component({
  selector: 'caupersonal-studies-certificate-promotional-batch',
  templateUrl: './studies-certificate-promotional-batch.component.html',
  providers: [
    {provide: LOCALE_ID, useValue: 'es'},
    BackofficeConfigService, GraduationService, MomentService, PromotionalStudyCertificateService,
  ]
})
export class CAUStudiesCertificatePromotionalBatchComponent implements OnInit {

  es: any;  // objeto para uso del datepicker
  wata: number;  // año actual
  errorMsg = '';
  isSearching: boolean; // búsqueda de estudiantes
  estudiantes: any;  // modelo para el input de Estudiantes
  students: any[];  // array de 'estudiantes' a procesar
  graduates: Graduated[];  // graduados UC
  tuqpa: any[]; // códigos de estudiantes a procesar no repetidos
  isShowStudentsList: boolean; // muestra los códigos que se van a procesar
  isAnyStudent: boolean; // para activar/desactivar el boton generar solicitud
  selected = false; // para seleccionar/deseleccionar todos los estudiantes
  gettingRequest: boolean; // procesando registro de solicitud
  requestList: Request[]; // Lista de solicitudes creadas
  showRequestList: boolean; // Mostrar lista se solicitudes
  loadingRequestList: boolean; // cargando lista de solicitudes
  pendingApproval: Request[];
  pendingProcessing: Request[];
  pendingDelivery: Request[];
  requestCancelled: Request[];
  requestViewActive: Request[];
  requestInbox: Request[];
  requestArchive: Request[];
  inbox: string;
  filter: any;
  configFile: any;

  constructor(private config: BackofficeConfigService, private graduationService: GraduationService,
              private moment: MomentService, private router: Router, private location: Location,
              private promotionalStudyCertificateService: PromotionalStudyCertificateService,
              private configService: ConfigService, private route: ActivatedRoute) {
    this.configFile = this.config;
  }

  ngOnInit() {
    this.es = this.configService.getCalendarEs();
    this.wata = (new Date()).getFullYear();
    this.clean();
    this.getRequestList();
    this.showRequestList = true;
    this.inbox = 'inbox';
  }

  /**
   * Tiempo relativo a partir de una fecha tipo YYYYMMDD
   * p.e.: momento("20101225") retorna un string: "hace X años"
   * @param {string} date
   * @returns {string}
   */
  momento(date: string): string {
    return this.moment.fromNow(date);
  }

  /**
   * Limpiar modelos
   */
  clean() {
    this.cleanCreateRequest();
    this.cleanRequestList();
  }

  /**
   * Limpia modelos del bloque "Solicitud"
   */
  cleanCreateRequest() {
    this.errorMsg = '';
    this.isSearching = false;
    this.estudiantes = undefined;
    this.students = [];
    this.graduates = undefined;
    this.tuqpa = [];
    this.isShowStudentsList = false;
    this.isAnyStudent = false;
    this.selected = false;
    this.gettingRequest = false;
    this.requestList = [];
  }

  /**
   * Limpia lista de solicitudes
   */
  cleanRequestList() {
    this.requestList = [];
    this.requestArchive = [];
    this.requestInbox = [];
    this.pendingApproval = [];
    this.pendingProcessing = [];
    this.pendingDelivery = [];
    this.requestCancelled = [];
    this.requestViewActive = [];
  }

  /**
   * Activa bloque para registrar nueva solicitud
   */
  newRequest() {
    this.inbox = undefined;
    this.showRequestList = false;
  }

  /**
   * Desactiva el bloque de creación de nueva solicitud
   */
  cancelCreateRequest() {
    this.getRequestList();
    this.showRequestList = true;
    this.inbox = 'inbox';
  }

  /**
   * Bandera para activar/desactivar el botón "Buscar Estudiantes" del tab "Solicitud"
   * @return {boolean} [description]
   */
  isAllRequestInputCompleted(): boolean {
    return !(this.estudiantes == null || this.estudiantes === undefined || this.estudiantes.trim() === '');
  }

  /**
   * Bandera para activar/desactivar el botón "Limpiar" del tab "Solicitud"
   * @return {boolean} [description]
   */
  isAnyRequestInputCompleted(): boolean {
    return !(this.estudiantes == null || this.estudiantes === undefined || this.estudiantes.trim() === '');
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray() {
    this.isSearching = false;
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // http://stackoverflow.com/a/9229821
    this.students = this.estudiantes.split(' ')
      .filter(// filter blank space
        (id) => id !== '',
      )
      .filter(// filter duplicates
        (id: any, position: any, self: any) => self.indexOf(id) === position,
      );
    console.log('input students => ', this.students);
    console.log('+--------------------------+');
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList() {
    this.isShowStudentsList = !this.isShowStudentsList;
  }

  /**
   * Egresados UC según código de estudiante
   */
  getGraduates() {
    console.log('Get Graduates...');
    this.isSearching = true;
    this.tuqpa = [];
    this.graduates = undefined;
    this.isAnyStudent = false;
    this.selected = false;
    const type = 'searchBatch';
    const schoolId = '';
    const graduationPeriod = '';
    this.graduationService.getGraduates(type, schoolId, graduationPeriod, this.students)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => this.errorMsg = error as any,
        () => this.isSearching = false,
      );
    console.log('this.graduates => ', this.graduates);
    console.log('+--------------------------+');
  }

  /**
   * Selecciona/Deselecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll() {
    for (const key in this.graduates) {
      if (this.graduates[key].is_selected || !this.graduates[key].is_selected) {
        this.graduates[key].is_selected = !this.selected;
      }
    }

    this.selected = !this.selected;
    this.isAnyStudent = this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.graduates) {
      if (this.graduates[key].is_selected) {
        return true;
      }
    }
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * Añade estilo a los estudiantes seleccionados
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any) {
    if (studentObj.is_selected) {
      studentObj.is_selected = false;
      console.log('Desactivado');
      // quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
      // ahora no todos están seleccionados
      // pero comprobemos que a lo menos alguien esté seleccionado
      this.isAnyStudent = this.isAnyStudentSelected() ? this.isAnyStudentSelected() : false;
      console.log('+--------------------------+');
    } else {
      studentObj.is_selected = true;
      console.log('activado');
      // agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
      console.log('+--------------------------+');
    }
  }

  /**
   * Solicitud Certificado de Estudios Promocional
   */
  createRequest() {
    console.log('Get Request...');
    this.createSequentialRequest(0);
    console.log('this.graduates => ', this.graduates);
  }

  /**
   * Petición secuencial para generar Solicitud de Certificado de Estudios Promocional
   * @param {number} i
   */
  createSequentialRequest(i: number) {
    console.log('Sequential...');
    console.log('proceso => ', i);

    if (this.graduates[i] !== undefined) {
      if (this.graduates[i].is_selected) {
        this.gettingRequest = true;
        this.graduates[i].doc_url = undefined;
        this.graduates[i].processing = true;
        this.promotionalStudyCertificateService.createRequest(this.graduates)
          .subscribe(
            (graduates) => this.graduates = graduates,
            (error) => {
              this.errorMsg = error as any;
              this.graduates[i].processingError = true;
              this.createSequentialRequest(i + 1);
              console.log('enviando el proceso => ', i + 1);
            },
            () => {
              this.gettingRequest = false;
              this.graduates[i].processingError = false;
              this.createSequentialRequest(i + 1);
              console.log('enviando el proceso => ', i + 1);
            },
          );
      } else {
        console.log('no seleccionado');
        this.createSequentialRequest(i + 1);
      }
    } else {
      console.log('nada que procesar => ', this.graduates[i]);
    }
    console.log('+--------------------------+');
  }

  /**
   * Lista de "Certificado de Estudios Promocional"
   */
  getRequestList() {
    console.log('Loading request list...');
    this.loadingRequestList = true;
    this.promotionalStudyCertificateService.getRequestList()
      .subscribe(
        (requestList) => this.getRequestListByStatus(requestList),
        (error) => this.errorMsg = error as any,
        () => this.loadingRequestList = false,
      );
    console.log('+--------------------------+');
  }

  /**
   * Lista de solicitudes "Certificado de Estudios Promocional" según estado de solicitud
   * @param {Request[]} requestList
   */
  getRequestListByStatus(requestList: Request[]) {
    this.requestList = requestList;
    this.requestArchive = requestList;
    this.requestInbox = this.requestList.filter(
      (request) => request.state !== this.config.STATE_CANCEL && request.state !== this.config.STATE_FINAL,
    );
    this.pendingApproval = this.requestList.filter(
      (request) => request.state === this.config.STATE_APROVAL_PENDING,
    );
    this.pendingProcessing = this.requestList.filter(
      (request) => request.state === this.config.STATE_PROCESS_PENDING,
    );
    this.pendingDelivery = this.requestList.filter(
      (request) => request.state === this.config.STATE_DELIVERY_PENDING,
    );
    this.requestCancelled = this.requestList.filter(
      (request) => request.state === this.config.STATE_CANCEL,
    );
    this.requestViewActive = this.requestInbox;
  }

  /**
   * Selección de vistas para mostrar solicitudes de un determinado estado
   * @param {Event} event
   * @param {string} type
   */
  setInbox(event: Event, type: string) {
    this.inbox = type;
    switch (type) {
      case ('inbox'):
        this.showRequestList = true;
        this.requestViewActive = this.requestInbox;
        break;
      case ('pendienteAprobacion'):
        this.showRequestList = true;
        this.requestViewActive = this.pendingApproval;
        break;
      case ('pendienteProceso'):
        this.showRequestList = true;
        this.requestViewActive = this.pendingProcessing;
        break;
      case ('pendienteEntrega'):
        this.showRequestList = true;
        this.requestViewActive = this.pendingDelivery;
        break;
      case ('archivo'):
        this.showRequestList = true;
        this.requestViewActive = this.requestArchive;
        break;
      case ('cancelado'):
        this.showRequestList = true;
        this.requestViewActive = this.requestCancelled;
        break;
      case ('search'):
        this.showRequestList = true;
        break;
    }
  }

  /**
   * Ir a detalle de solicitud
   * @param {string} requestId
   */
  gotoRequestDetail(requestId: string) {
    const pathName: string[] = this.location.path().split('/');
    if (pathName.length > 3) {
      const link: string[] = [`${pathName[1]}/${pathName[2]}/solicitudes/${requestId}`];
      this.router.navigate(link);
    } else {
      console.log('pathname', pathName);
    }
  }

}
