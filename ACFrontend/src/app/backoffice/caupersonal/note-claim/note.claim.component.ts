import { Component } from '@angular/core';
import { BackofficeConfigService } from '../../config/config.service';
@Component({
  selector: 'oea-cau-note-claim',
  template: `<oea-modify-note-claim-component
              [officeId]="bConfigService.OFFICE_CAU"
              [status]="bConfigService.OEA_STATUS_NOTE_CLAIM_CAS">
              </oea-modify-note-claim-component>`,
  providers: [BackofficeConfigService]
})

export class OEACauNoteClaimComponent {
  constructor(private bConfigService: BackofficeConfigService) { }
}
