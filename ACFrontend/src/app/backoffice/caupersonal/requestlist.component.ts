import { BackofficeConfigService } from '../config/config.service';
import { Component, OnInit } from '@angular/core';
import { Request } from '../../shared/services/atentioncenter/request';
import { RequestService } from '../../shared/services/atentioncenter/request.service';
import { Router } from '@angular/router';

@Component({
  providers: [RequestService, BackofficeConfigService],
  selector: 'request-list',
  templateUrl: './requestlist.component.html'
})
export class CAURequestListComponent implements OnInit {

  error_message: string;
  inbox = 'pending';
  requests: Request[];
  find_requests: Request[] = [];
  requests_active: Request[] = [];
  r_atention_pending: Request[] = [];
  r_delivery_pending: Request[] = [];
  r_proccess_pending: Request[] = [];
  r_document_reception: Request[] = [];
  r_final: Request[] = [];
  r_cancelations: Request[] = [];
  r_all: Request[] = [];
  area = '';
  searh_data: any = {};
  filter: any;
  configFile: any;

  constructor(
    private router: Router,
    private requestService: RequestService,
    private config: BackofficeConfigService
  ) {
    this.configFile = this.config;
  }

  ngOnInit() {
    this.requests = null;
    this.getRequests();
  }

  getRequests() {
    this.requestService.query()
      .subscribe(
      (requests) => this.atentionRequest(requests),
      (error) => this.error_message = error
      );
  }

  atentionRequest(requests: Request[]) {
    this.requests = requests;
    this.requests_active = requests;
    this.r_atention_pending = this.requests.filter(
      (item) => item.state === this.config.STATE_ATENTION,
    );
    this.r_delivery_pending = this.requests.filter(
      (item) => item.state === this.config.STATE_DELIVERY_PENDING,
    );
    this.r_proccess_pending = this.requests.filter(
      (item) => item.state === this.config.STATE_PROCESS_PENDING,
    );
    this.r_document_reception = this.requests.filter(
      (item) => item.state === this.config.STATE_DOCUMENT_RECEPTION,
    );
  }

  setInbox(event: Event, type: string) {
    this.inbox = type;
    switch (type) {
      case ('atention_pending'):
        this.requests_active = this.r_atention_pending;
        break;
      case ('delivery_pending'):
        this.requests_active = this.r_delivery_pending;
        break;
      case ('proccess_pending'):
        this.requests_active = this.r_proccess_pending;
        break;
      case ('document_reception'):
        this.requests_active = this.r_document_reception;
        break;
      default:
        this.requests_active = this.requests;
    }

  }

  gotoRequestDetail(id: string) {
    const link: string[] = [`backoffice/cau-personal/solicitudes/${id}`];
    this.router.navigate(link);
  }

  searchRequests(data: any) {
    this.requestService.search(data)
      .subscribe(
        (requests) => this.find_requests = requests,
        (error) => this.error_message = error
      );
  }
}
