import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { StudentService } from '../../shared/services/academic/student.service';
import { Student } from '../../shared/services/academic/student';
import { AcademicProfile } from '../../shared/services/academic/academic.profile';
import { EnrollmentTermService } from '../../shared/services/academic/enrollmentTerm.service';
import { OEANrcForSubstituteExam } from '../../shared/services/learning-assessment/oeaNrcForSubstituteExam';
import { OEASubstituteExamProgrammed } from '../../shared/services/learning-assessment/oeaSubstituteExamProgrammed';
import { OEASubstituteExamProgrammedService } from '../../shared/services/learning-assessment/oeaSubstituteExamProgrammed.service';
import { OEANrcForSubstituteExamService } from '../../shared/services/learning-assessment/oeaNrcForSubstituteExam.service';
import { OEANrcProgrammned } from '../../shared/services/learning-assessment/oeaNrcProgrammned';

@Component({
  selector: 'OEA-substitute-exam-student',
  templateUrl: './substitute.exam.student.component.html',
  providers: [StudentService, EnrollmentTermService, OEANrcForSubstituteExamService, OEASubstituteExamProgrammedService]
})

export class OEAsubstituteExamStudentComponent implements OnInit {
  load: boolean;
  msgError: string;
  student: Student;
  terms: string[];
  termSelected: string;

  listSubjetPending: OEANrcForSubstituteExam[];
  listRequestedSubjet: OEASubstituteExamProgrammed[] = [];
  comment: string;

  constructor(private activatedRoute: ActivatedRoute, private studentService: StudentService,
    private enrollmentTermService: EnrollmentTermService, private oeaNrcForSubstituteExamService: OEANrcForSubstituteExamService,
    private oeaSubstituteExamProgrammedService: OEASubstituteExamProgrammedService) { }

  ngOnInit(): void {
    this.getStudent();
  }

  /**
   * Obtener información de estudiante
   */
  getStudent(): void {
    this.load = true;
    this.activatedRoute.params.forEach((params: Params) => {
      const studentId: string = (params as any).studentId;
      this.studentService.find(studentId, 'student_id')
        .subscribe(
          (student) => {
            this.student = student;
            this.msgError = null;
            this.load = false;
            this.getTermsEnrollmentbyProfile();
          },
          (error) => {
            console.log(error);
            this.msgError = 'Error al obtener información de estudiante';
            this.load = false;
          }
        );
    });
  }

  /**
 * Cambiar perfil actual
 * @param profile Perfil seleccionado
 */
  changeProfile(profile: AcademicProfile): void {
    this.student.profile = profile;
    this.getTermsEnrollmentbyProfile();
  }

  /**
   * Obtener periodos matriculados del perfil seleccionado
   */
  getTermsEnrollmentbyProfile(): void {
    this.load = true;
    this.termSelected = null;
    this.terms = null;
    this.enrollmentTermService.getAllTermsEnrollmentByProfile(this.student.profile)
      .subscribe(
        terms => {
          this.terms = terms.filter(item => item >= '201710');
          this.msgError = null;
          this.load = false;
        },
        error => {
          console.log(error);
          this.msgError = 'Error al obtener periodos';
          this.load = false;
        }
      );

  }

  /**
   * Cambiar periodo
   * @param term Periodo Seleccionado
   */
  changeTerm(term: string): void {
    this.termSelected = term;
    this.getSubstituteExam();
  }

  /**
   * obtener Nrcs con examen
   */
  getSubstituteExam(): void {
    this.load = true;
    this.oeaNrcForSubstituteExamService.getByProfile(this.student.profile, this.termSelected)
      .subscribe(
        data => {
          this.listSubjetPending = data;
          this.msgError = null;
          this.load = false;
          this.getProgramExam();
        },
        error => {
          this.msgError = 'Error al obtener asignaturas con examen sustitutorio.';
          console.log(error);
          this.load = false;
        }
      );
  }

  /**
   * Obtener asignaturas programadas por el estudiante
   */
  getProgramExam(): void {
    this.load = true;
    this.oeaSubstituteExamProgrammedService.getByProfileAndTerm(this.student, this.termSelected)
      .subscribe(
        data => {
          this.listRequestedSubjet = data;
          this.msgError = null;
          this.load = false;
        },
        error => {
          this.msgError = 'Error al obtener asignaturas programadas.';
          console.log(error);
          this.load = false;
        }
      );
  }


  /**
 * Seleccionar o deseleccionar programación
 * @param subjet nrc
 * @param programmnedSelected  fecha
 */
  selectSchedule(subjet: OEANrcForSubstituteExam, programmnedSelected: OEANrcProgrammned): void {
    subjet.scheduledSelected = null;
    if (programmnedSelected.selected) {
      subjet.scheduled.forEach(schedule => {
        schedule.selected = false;
      });
      programmnedSelected.selected = false;
    } else {
      subjet.scheduled.forEach(schedule => {
        schedule.selected = false;
      });
      programmnedSelected.selected = true;
      subjet.scheduledSelected = programmnedSelected;
    }
  }

  /**
   * Funcion para habilitar boton guardar sustitutorio
   */
  validateBtnSaveSubstitute(): number {
    let result = 0;
    if (this.listSubjetPending != null && this.listSubjetPending.filter(f => f.scheduledSelected != null).length > 0
      && this.comment !== undefined && this.comment.trim().length > 3) {
      result = 1;
    }
    return result;
  }

  /**
   * Solicitar examen sustitutorio
   */
  saveSubstituteExamn(): void {
    let listSend: OEANrcForSubstituteExam[];
    listSend = this.listSubjetPending.filter(f => f.scheduledSelected != null);
    if (this.validateBtnSaveSubstitute() === 0) {
      alert('Selecciones asignatura e ingrese comentario.');
    } else {
      this.load = true;
      this.oeaSubstituteExamProgrammedService.saveProgrambyBack(this.student, this.termSelected, listSend, this.comment)
        .subscribe(
          data => {
            console.log(data);
            this.msgError = null;
            this.load = false;
            this.comment = '';
            this.getSubstituteExam();
          },
          error => {
            this.msgError = 'Error al guardar asignaturas programadas, vuélvelo a intentar en unos minutos.';
            console.log(error);
            this.load = false;
          }
        );
    }
  }

  /**
   * Validar boton para modificar examen sustitutorio
   */
  validateBtnModifySubstitute(): number {
    let result = 0;
    if (this.comment !== undefined && this.comment.trim().length > 3) {
      result = 1;
    }
    return result;
  }

  /**
   * Guardar modificaciones de examen sustitutorio
   */
  modifySubstituteExamn(): void {
    if (this.validateBtnModifySubstitute() === 0) {
      alert('Ingrese comentario.');
    } else {
      this.load = true;
      this.oeaSubstituteExamProgrammedService.modifyProgram(this.student, this.termSelected, this.listRequestedSubjet, this.comment)
        .subscribe(
          data => {
            console.log(data);
            this.msgError = null;
            this.load = false;
            this.comment = '';
            this.getSubstituteExam();
          },
          error => {
            this.msgError = 'Error al guardar modificaciones en asignaturas programadas, vuélvelo a intentar en unos minutos.';
            console.log(error);
            this.load = false;
          }
        );
    }
  }
}
