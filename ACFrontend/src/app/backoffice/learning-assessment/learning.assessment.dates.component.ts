import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'OEA-learning-assessment-dates',
  template: '<period-date [substituteExam]=true></period-date>'
})

export class OEALearningAssessmentDatesComponent implements OnInit {
  constructor() { }
  ngOnInit(): void { }
}
