import { Component, OnInit } from '@angular/core';
import { BackofficeConfigService } from '../../config/config.service';
import { ACTermService } from '../../../shared/services/atentioncenter/term.service';
import { CampusService } from '../../../shared/services/institutional/campus.service';
import { Campus } from '../../../shared/services/academic/campus';
import { OEAReport } from '../../../shared/services/learning-assessment/report';
import { OEAReportService } from '../../../shared/services/learning-assessment/report.service';

@Component({
  selector: 'OEA-report',
  templateUrl: './report.nrc.programmed.component.html',
  providers: [BackofficeConfigService, ACTermService, CampusService, OEAReportService],
})
export class OEAReportNrcProgrammedComponent implements OnInit {
  load: boolean;
  msgError: string;
  terms: string[];
  term: string;
  group: string;
  modality: string;
  allCampus: Campus[];
  campus: string;
  report: OEAReport[];
  constructor(private config: BackofficeConfigService, private acTermService: ACTermService,
    private campusService: CampusService, private oeaReportService: OEAReportService) { }
  ngOnInit(): void {
    this.getTerms();
  }

  /**
   * Obtener todos los periodos
   */
  getTerms() {
    this.load = true;
    this.acTermService.query()
      .subscribe(
        data => {
          this.terms = data.filter(f => f.term >= '201710').map(f => f.term).filter((value, index, self) => self.indexOf(value) === index);
          this.load = false;
          this.getAllCampus();
        },
        error => {
          console.log(error);
          this.load = false;
        });
  }

  /**
   * Obtener todas las sedes
   */
  getAllCampus(): void {
    this.load = true;
    this.campusService.query()
      .subscribe(
        campus => {
          this.allCampus = campus;
          this.load = false;
        },
        error => {
          console.log(error);
          this.msgError = 'Error al obtener sedes.',
            this.load = false;
        }
      );
  }

  /**
   * obtener nombre de sede por código
   * @param id código de sede
   */
  getNamebyCampus(id: string): string {
    return this.allCampus.filter(f => f.id === id)[0].name.replace('SEDE', '').replace('FILIAL', '');
  }

  /**
   * Obtener nombre de modalidad por codigo
   * @param id Id de modalidad
   */
  getNamebyModality(id: string): string {
    return this.config.DEPARTAMENT_NAMES.filter(f => f.id === id)[0].name;
  }

  /**
   * Obtener reporte
   */
  getReport(): void {
    if (this.term === undefined || this.term.trim().length <= 0) {
      alert('El periodo no puede ser nulo');
      return;
    }

    let modality: string = null;
    let campus: string = null;
    let group: string = null;
    if (this.modality !== undefined && this.modality.trim().length > 0) {
      modality = this.modality;
    }
    if (this.campus !== undefined && this.campus.trim().length > 0) {
      campus = this.campus;
    }
    if (this.group !== undefined && this.group.trim().length > 0) {
      group = this.group;
    }
    this.load = true;
    const dataReport: OEAReport = <OEAReport>{
      input_modality: modality,
      inputCampus: campus,
      input_term: this.term,
      input_group: group
    };
    this.oeaReportService.getReportOfNrcsProgrammed(dataReport)
      .subscribe(
        (report) => this.report = report,
        (err) => console.log(err),
        () => this.load = false,
      );
  }

  /**
   * DEscargar Excel
   */
  downloadReport(): void {
    const datos: any[] = this.report.map(
      (item) => {
        return {
          'Modalidad': this.getNamebyModality(item.nrcForSubstituteExam.department),
          'Sede': this.getNamebyCampus(item.nrcForSubstituteExam.campus),
          'Período': item.nrcForSubstituteExam.term,
          'Grupo': item.nrcForSubstituteExam.module,
          'NRC': item.nrcForSubstituteExam.nrc,
          '¿NRC Activo?': item.nrcForSubstituteExam.active,
          'Parte Periodo': item.nrcForSubstituteExam.partTerm,
          'Asignatura': item.nrcForSubstituteExam.subjetName,
          'Fecha Inicio': item.nrcProgrammned.inicialRegistrationDate.toString().replace('T', ' '),
          'Fecha Fin': item.nrcProgrammned.finalRegistrationDate.toString().replace('T', ' '),
          'Fecha Examen': item.nrcProgrammned.examDate.toString().replace('T', ' '),
          'Aula': item.nrcProgrammned.classRoom,
          '¿Programación Activa?': item.nrcProgrammned.active,
          '¿Publico?': item.nrcProgrammned.isPublic,
        };
      });
    this.config.exportAsExcelFile(datos, 'Reporte NRCs pogramados');
  }
}
