import { BackofficeConfigService } from '../../config/config.service';
import { Component, OnInit } from '@angular/core';
import { OEAReport } from '../../../shared/services/learning-assessment/report';
import { OEAReportService } from '../../../shared/services/learning-assessment/report.service';
import { ACTermService } from '../../../shared/services/atentioncenter/term.service';
declare var $: any;

@Component({
  selector: 'OEA-report-substitute-Programmed',
  templateUrl: './report.substitute.programmed.component.html',
  providers: [BackofficeConfigService, OEAReportService, ACTermService],
})
export class OEAReportSubstituteProgrammedComponent implements OnInit {

  load = false;
  terms: string[];
  term: string;
  group: string;
  modality: string;

  report: OEAReport[];
  constructor(private config: BackofficeConfigService, private oeaReportService: OEAReportService,
    private acTermService: ACTermService
  ) { }
  ngOnInit(): void {
    this.getTerms();
  }
  getTerms() {
    this.load = true;
    this.acTermService.query()
      .subscribe(
        data => {
          this.terms = data.filter(f => f.term >= '201710').map(f => f.term).filter((value, index, self) => self.indexOf(value) === index);
          this.load = false;
        },
        error => {
          console.log(error);
          this.load = false;
        });
  }

  getReport() {
    this.load = true;
    const dataReport: OEAReport = {} as OEAReport;

    if (this.group == '') {
      this.group = null;
    }
    if (this.modality == '') {
      this.modality = null;
    }

    dataReport.input_term = this.term;
    dataReport.input_group = this.group;
    dataReport.input_modality = this.modality;

    if (!dataReport.input_term) {
      alert('Seleccione todos los parametros.');
      this.load = false;
      return;
    }

    this.oeaReportService.getReportSusbstituteProgrammed(dataReport)
      .subscribe(
        (report) => this.report = report,
        (err) => console.log(err),
        () => this.load = false,
      );
  }

  downloadReport(): void {
    const datos: any[] = this.report.map(
      (dato) => {
        return {
          'Código de estudiante': dato.r1_student_id,
          'Nombre': dato.r1_student_name,
          'Facultad': dato.r1_name_faculty,
          'Carrera': dato.r1_school_name,
          'NRC': dato.r1_nrc,
          'Aula': dato.r1ClassRoom,
          'Fecha examen': dato.r1ExamDate.toString().replace('T', ' '),
          'Parte Período': dato.r1_part_period,
          'Modalidad Estudiante': dato.r1_modality,
          'Campus': dato.r1_campus_name,
          // 'Código Asignatura': dato.r1_course_id,
          'Asignatura': dato.r1_course_name,
          /*'C1': dato.r1_score_c1,
          'EP': dato.r1_score_ep,
          'C2': dato.r1_score_c2,
          'EF': dato.r1_score_ef,*/
          'Cargo': dato.r1_ammount,
          'Deuda': dato.r1_debt,
          'Abono': dato.r1_deposit,
          // 'IDSeccion': dato.r1_section_id,
          'Grupo': dato.r1_group,
          'Fecha de solicitud': dato.r1_request_date.toString().replace('T', ' '),
          'Justificación': dato.r1_proof,
        };
      });
    this.config.exportAsExcelFile(datos, 'Reporte examen sustitutorio');
  }
}
