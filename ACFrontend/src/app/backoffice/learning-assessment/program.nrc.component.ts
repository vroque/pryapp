import { Component, OnInit } from '@angular/core';
import { BackofficeConfigService } from '../config/config.service';
import { ACTerm } from '../../shared/services/atentioncenter/term';
import { ACTermService } from '../../shared/services/atentioncenter/term.service';
import { OEANrcForSubstituteExamService } from '../../shared/services/learning-assessment/oeaNrcForSubstituteExam.service';
import { OEANrcForSubstituteExam } from '../../shared/services/learning-assessment/oeaNrcForSubstituteExam';
import { CampusService } from '../../shared/services/institutional/campus.service';
import { Campus } from '../../shared/services/institutional/campus';
import { ConfigService } from '../../config/config.service';

@Component({
  selector: 'OEA-program-nrc',
  templateUrl: './program.nrc.Component.html',
  providers: [BackofficeConfigService, ACTermService, CampusService, OEANrcForSubstituteExamService, ConfigService]
})

export class OEAProgramNrcComponent implements OnInit {
  load: boolean;
  msgError: string;
  allTerms: ACTerm[];
  allCampus: Campus[];
  termsDepartment: string[];
  departamentSelected: string;
  termSelected: string;
  stringNrcs: string;
  listNrcs: string[];
  detailsNrcs: OEANrcForSubstituteExam[];
  nrcSelected: OEANrcForSubstituteExam;

  es: any;
  constructor(private backofficeConfigService: BackofficeConfigService, private acTermService: ACTermService,
    private campusService: CampusService, private oeaNrcForSubstituteExamService: OEANrcForSubstituteExamService,
    private configService: ConfigService) { }
  ngOnInit(): void {
    this.getAllTerms();
    this.es = this.configService.getLocationCalendar();
  }

  /**
   * Obtener todos los periodos
   */
  getAllTerms(): void {
    this.load = true;
    this.acTermService.query()
      .subscribe(
        data => {
          this.allTerms = data.filter(f => f.term >= '201710');
          this.load = false;
          this.getAllCampus();
        },
        error => {
          this.msgError = 'Error al obtener periodos academicos';
          console.log(error);
          this.load = false;
        });
  }

  /**
   * Obtener todas las sedes
   */
  getAllCampus(): void {
    this.load = true;
    this.campusService.query()
      .subscribe(
        campus => {
          this.allCampus = campus;
          this.load = false;
        },
        error => {
          console.log(error);
          this.msgError = 'Error al obtener sedes.',
            this.load = false;
        }
      );
  }

  /**
   * obtener nombre de sede por código
   * @param id código de sede
   */
  getNamebyCampus(id: string): string {
    return this.allCampus.filter(f => f.id === id)[0].name.replace('SEDE', '').replace('FILIAL', '');
  }

  /**
   * Listar nrcs por departamento seleccionado
   */
  listTermByDepartament(): void {
    this.load = true;
    this.termSelected = null;
    this.termsDepartment = null;
    const distinct = (value, index, self) => {
      return self.indexOf(value) === index;
    };
    this.termsDepartment = this.allTerms.filter(f => f.department === this.departamentSelected)
      .map(term => term.term).filter(distinct);
    this.load = false;
  }

  /**
   * Separar Nrcs en lista
   */
  separateNrcs(): void {
    if (this.stringNrcs.trim().length > 0) {
      this.listNrcs = this.stringNrcs.split(' ');
    } else {
      this.listNrcs = undefined;
    }
  }
  /**
   * Limpar formulario
   */
  clear(): void {
    this.stringNrcs = '';
    this.listNrcs = undefined;
    this.departamentSelected = null;
    this.termSelected = null;
    this.listNrcs = null;
    this.detailsNrcs = null;
    this.nrcSelected = null;
  }

  /**
   * Obtener Nrcs Ingresados
   */
  getListNrcs(): void {
    if (this.departamentSelected == null || this.departamentSelected.trim().length <= 0
      || this.termSelected == null || this.termSelected.trim().length <= 0) {
      alert('Seleccione Modalidad y periodo.');
      return;
    }
    this.load = true;
    this.oeaNrcForSubstituteExamService.getListNrcs(this.departamentSelected, this.termSelected, this.listNrcs)
      .subscribe(
        data => {
          this.detailsNrcs = data;
          this.load = false;
        },
        error => {
          console.log(error);
          this.msgError = 'Error al obtener detalle de nrcs';
          this.load = false;
        }
      );
  }
  /**
   * Mostrar detalle de nrc seleccionado
   * @param nrc nrcSeleccionado
   */
  getDetailsNrc(nrc: OEANrcForSubstituteExam): void {
    this.load = true;
    this.oeaNrcForSubstituteExamService.getDetailsNrc(nrc.department, nrc.term, nrc.nrc)
      .subscribe(
        data => {
          if (!data.scheduled) {
            data.scheduled = [];
          }
          data.scheduled.forEach(item => {
            item.inicialRegistrationDate = new Date(item.inicialRegistrationDate as any);
            item.finalRegistrationDate = new Date(item.finalRegistrationDate as any);
            item.examDate = new Date(item.examDate as any);
          });
          data.comment = '';
          this.nrcSelected = data;
          this.load = false;
        },
        error => {
          this.msgError = 'Error al obtener información de nrc';
          console.log(error);
          this.load = false;
        }
      );
  }
  /**
   * Validar antes de guardar
   */
  validateSave(): boolean {
    let result = true;
    if (this.nrcSelected == null) {
      result = false;
    } else if (this.nrcSelected.comment == null || this.nrcSelected.comment.trim().length <= 0) {
      result = false;
    } else {
      this.nrcSelected.scheduled.forEach(prog => {
        if (prog.inicialRegistrationDate == null || prog.finalRegistrationDate == null || prog.examDate == null
          || prog.classRoom == null || prog.classRoom.trim().length <= 0) {
          result = false;
        }
      });
    }
    return result;
  }

  /**
   * Guardar informacion de Nrc
   */
  saveDetailsNrc(): void {
    if (!this.validateSave()) {
      alert('Registre todas las fechas y aulas.');
      return;
    }
    this.nrcSelected.comment = this.nrcSelected.comment.trim();
    const nrcSave: OEANrcForSubstituteExam = JSON.parse(JSON.stringify(this.nrcSelected));
    nrcSave.scheduled.forEach(item => {
      item.inicialRegistrationDate = this.convertToUTC(item.inicialRegistrationDate);
      item.finalRegistrationDate = this.convertToUTC(item.finalRegistrationDate);
      item.examDate = this.convertToUTC(item.examDate);
    });
    this.oeaNrcForSubstituteExamService.saveDetailsNrc(nrcSave)
    .subscribe(
      data => {
        this.msgError = null;
        this.load = false;
        this.getDetailsNrc(this.nrcSelected);
      },
      error => {
        console.log(error);
        this.msgError = 'Error al intentar guardar detalles de Nrc.';
        this.load = false;
        this.getDetailsNrc(this.nrcSelected);
      }
    );
  }

  /**
   * Convertir a fecha local
   * @param data fecha a convertir
   */
  convertToUTC(data: Date): Date {
    data = new Date(data as any);
    const utc: any = new Date(data.getTime() - data.getTimezoneOffset() * 60000).toUTCString();
    return utc;
  }
}

