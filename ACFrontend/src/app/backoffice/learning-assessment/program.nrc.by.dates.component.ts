import { Component, OnInit, ɵConsole } from '@angular/core';
import { BackofficeConfigService } from '../config/config.service';
import { ACTermService } from '../../shared/services/atentioncenter/term.service';
import { CampusService } from '../../shared/services/institutional/campus.service';
import { ACTerm } from '../../shared/services/atentioncenter/term';
import { Campus } from '../../shared/services/academic/campus';
import { OEANrcForSubstituteExamService } from '../../shared/services/learning-assessment/oeaNrcForSubstituteExam.service';
import { OEANrcForSubstituteExam } from '../../shared/services/learning-assessment/oeaNrcForSubstituteExam';
import { ConfigService } from '../../config/config.service';
import { OEANrcProgrammned } from '../../shared/services/learning-assessment/oeaNrcProgrammned';

@Component({
  selector: 'OEA-program-nrc-by-dates',
  templateUrl: './program.nrc.by.dates.component.html',
  providers: [BackofficeConfigService, ACTermService, CampusService, OEANrcForSubstituteExamService, ConfigService]
})

export class OEAProgramNrcByDatesComponent implements OnInit {
  load: boolean;
  msgError: string;
  allTerms: ACTerm[];
  allCampus: Campus[];
  termsDepartment: string[];
  departamentSelected: string;
  termSelected: string;
  stringNrcs: string;
  listNrcs: string[];

  activeNrcs: boolean;
  hasSusti: boolean;

  activeProgram: boolean;
  public: boolean;
  dateInit: Date;
  dateEnd: Date;
  dateExamn: Date;
  classRoom: string;

  detailsNrcs: OEANrcForSubstituteExam[];
  comment: string;

  es: any;

  distinct = (value, index, self) => {
    return self.indexOf(value) === index;
  }
  constructor(private backofficeConfigService: BackofficeConfigService, private acTermService: ACTermService,
    private campusService: CampusService, private oeaNrcForSubstituteExamService: OEANrcForSubstituteExamService,
    private configService: ConfigService) { }

  ngOnInit(): void {
    this.es = this.configService.getLocationCalendar();
    this.getAllTerms();
  }

  /**
   * Obtener todos los periodos
   */
  getAllTerms(): void {
    this.load = true;
    this.acTermService.query()
      .subscribe(
        data => {
          this.allTerms = data.filter(f => f.term >= '201710');
          this.load = false;
          this.getAllCampus();
        },
        error => {
          this.msgError = 'Error al obtener periodos academicos';
          console.log(error);
          this.load = false;
        });
  }

  /**
   * Obtener todas las sedes
   */
  getAllCampus(): void {
    this.load = true;
    this.campusService.query()
      .subscribe(
        campus => {
          this.allCampus = campus;
          this.load = false;
        },
        error => {
          console.log(error);
          this.msgError = 'Error al obtener sedes.',
            this.load = false;
        }
      );
  }

  /**
   * obtener nombre de sede por código
   * @param id código de sede
   */
  getNamebyCampus(id: string): string {
    return this.allCampus.filter(f => f.id === id)[0].name.replace('SEDE', '').replace('FILIAL', '');
  }

  /**
   * Listar nrcs por departamento seleccionado
   */
  listTermByDepartament(): void {
    this.load = true;
    this.termSelected = null;
    this.termsDepartment = null;
    this.termsDepartment = this.allTerms.filter(f => f.department === this.departamentSelected)
      .map(term => term.term).filter(this.distinct);
    this.load = false;
  }

  /**
   * Separar Nrcs en lista
   */
  separateNrcs(): void {
    if (this.stringNrcs.trim().length > 0) {
      this.listNrcs = this.stringNrcs.split(' ').filter(this.distinct);
    } else {
      this.listNrcs = undefined;
    }

    if (this.listNrcs != null && this.listNrcs.length > 300) {
      this.listNrcs = this.listNrcs.slice(0, 300);
    }

    if (this.listNrcs != null) {
      this.stringNrcs = '';
      this.listNrcs.forEach(item => {
        this.stringNrcs = this.stringNrcs + ' ' + item.trim();
      });
    }

  }

  /**
   * Limpar formulario
   */
  clear(): void {
    this.stringNrcs = '';
    this.listNrcs = undefined;
    this.departamentSelected = null;
    this.termSelected = null;
    this.listNrcs = null;
    this.detailsNrcs = null;
    this.comment = '';
    this.classRoom = '';
    this.dateInit = null;
    this.dateEnd = null;
    this.dateExamn = null;
    // this.nrcSelected = null;
  }

  /**
   * Obtener Nrcs Ingresados
   */
  getListNrcs(): void {
    if (this.departamentSelected == null || this.departamentSelected.trim().length <= 0
      || this.termSelected == null || this.termSelected.trim().length <= 0) {
      alert('Seleccione Modalidad y periodo.');
      return;
    }
    this.load = true;
    this.oeaNrcForSubstituteExamService.getListNrcs(this.departamentSelected, this.termSelected, this.listNrcs)
      .subscribe(
        data => {
          this.detailsNrcs = data;
          this.load = false;
        },
        error => {
          console.log(error);
          this.msgError = 'Error al obtener detalle de nrcs';
          this.load = false;
        }
      );
  }

  /**
   * Seleccionar o desseleccionar toda la lista
   */
  selectAll(): void {
    this.load = true;
    const textBtn = ((document.getElementById('btnChangeAll') as HTMLInputElement).value);
    let value: boolean;
    if (textBtn === 'Seleccionar todos') {
      value = true;
      ((document.getElementById('btnChangeAll') as HTMLInputElement).value) = 'Deseleccionar todos';
    } else {
      value = false;
      ((document.getElementById('btnChangeAll') as HTMLInputElement).value) = 'Seleccionar todos';
    }
    this.detailsNrcs.forEach(item => {
      item.selected = value;
    });
    this.load = false;
  }

  /**
   * Validar antes de guardar
   */
  validateSave(): boolean {
    let result = false;
    this.detailsNrcs.forEach(item => {
      if (item.selected) {
        result = true;
      }
    });
    if (this.comment == null || this.comment.trim().length <= 3 || this.dateEnd == null || this.dateInit == null
      || this.dateExamn == null || this.classRoom == null || this.classRoom.trim().length <= 2) {
      result = false;
    }
    return result;
  }

  /**
   * Guardar informacion de Nrc
   */
  saveDetailsNrc(): void {
    this.classRoom = this.classRoom.trim();
    this.comment = this.comment.trim();
    if (!this.validateSave()) {
      alert('Registre todas las fechas, aula y comentario.');
      return;
    }

    this.detailsNrcs.forEach(nrc => {
      this.load = true;
      if (nrc.selected) {
        nrc.active = this.activeNrcs;
        nrc.hasSusti = this.hasSusti;
        nrc.comment = this.comment.trim();
        nrc.scheduled = [];
        nrc.scheduled.push(<OEANrcProgrammned>{
          inicialRegistrationDate: this.convertToUTC(this.dateInit),
          finalRegistrationDate: this.convertToUTC(this.dateEnd),
          examDate: this.convertToUTC(this.dateExamn),
          active: this.activeProgram,
          classRoom: this.classRoom.trim(),
          isPublic: this.public,
          comment: this.comment.trim()
        });
        nrc.state = 1;
        this.oeaNrcForSubstituteExamService.saveDetailsNrc(nrc)
          .subscribe(
            data => nrc.state = 3,
            error => {
              nrc.state = 2;
              console.log(error);
            }
          );
      }
      this.load = false;
    });
  }

  /**
   * Convertir a fecha local
   * @param data fecha a convertir
   */
  convertToUTC(data: Date): Date {
    data = new Date(data as any);
    const utc: any = new Date(data.getTime() - data.getTimezoneOffset() * 60000).toUTCString();
    return utc;
  }
}
