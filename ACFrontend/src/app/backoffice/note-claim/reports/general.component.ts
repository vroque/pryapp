import { Component, OnInit } from '@angular/core';
import { BackofficeConfigService } from '../../config/config.service';
import { ACTermService } from '../../../shared/services/atentioncenter/term.service';
import { Campus } from '../../../shared/services/academic/campus';
import { CampusService } from '../../../shared/services/institutional/campus.service';
import { OEAReportService } from '../../../shared/services/learning-assessment/report.service';
import { OEAReport } from '../../../shared/services/learning-assessment/report';
import { OEANoteClaim } from '../../../shared/services/learning-assessment/OEANoteClaim';

@Component({
  selector: 'oea-note-claim-report',
  templateUrl: './general.component.html',
  providers: [BackofficeConfigService, ACTermService, CampusService, OEAReportService]
})

export class OEANoteClaimReportComponent implements OnInit {
  loading: boolean;
  msgError: string;
  terms: string[];
  allCampus: Campus[];
  report: OEANoteClaim[];

  term: string;
  modality = '';
  type = '';
  campus = '';
  status = 0;
  component = '';
  studentCode = '';

  mailSubject: string;
  msgEmail: string;
  selectedAll = false;
  constructor(private backofficeConfigService: BackofficeConfigService,
    private acTermService: ACTermService, private campusService: CampusService,
    private oeaReportService: OEAReportService) { }
  ngOnInit(): void {
    this.getTerms();
  }

  /**
   * Obtener todos los periodos
   */
  getTerms() {
    this.loading = true;
    this.acTermService.query()
      .subscribe(
        data => {
          this.terms = data.filter(f => f.term >= '201710').map(f => f.term).filter((value, index, self) => self.indexOf(value) === index);
          this.loading = false;
          this.getAllCampus();
        },
        error => {
          console.log(error);
          this.loading = false;
        });
  }

  /**
   * Obtener todas las sedes
   */
  getAllCampus(): void {
    this.loading = true;
    this.campusService.query()
      .subscribe(
        campus => {
          this.allCampus = campus;
          this.loading = false;
        },
        error => {
          console.log(error);
          this.msgError = 'Error al obtener sedes.';
          this.loading = false;
        }
      );
  }

  /**
   * obtener nombre de sede por código
   * @param id código de sede
   */
  getNamebyCampus(id: string): string {
    return this.allCampus.filter(f => f.id === id)[0].name.replace('SEDE', '').replace('FILIAL', '');
  }

  /**
   * Obtener nombre de modalidad por codigo
   * @param id Id de modalidad
   */
  getNamebyModality(id: string): string {
    return this.backofficeConfigService.DEPARTAMENT_NAMES.filter(f => f.id === id)[0].name;
  }

  /**
   * Validar inputs reporte
   */
  validate(): boolean {
    let validate = true;
    if (this.term == null || this.term.trim().length <= 0 || this.modality == null || this.type == null
      || this.campus == null || this.status == null || this.component == null) {
      validate = false;
    }
    return validate;
  }

  /**
   * Obtener Reporte
   */
  getReport(): void {
    this.loading = true;
    const dataReport: OEAReport = <OEAReport>{
      input_modality: this.modality.trim() === '' ? null : this.modality.trim(),
      inputCampus: this.campus.trim() === '' ? null : this.campus.trim(),
      input_term: this.term,
      inputType: this.type.trim() === '' ? null : this.type.trim(),
      inputStatus: this.status <= 0 ? null : this.status,
      inputComponent: this.component.trim() === '' ? null : this.component.trim(),
      inputStudenId: this.studentCode.trim() === '' ? null : this.studentCode.trim(),
    };
    this.oeaReportService.getNoteClaims(dataReport)
      .subscribe(
        (data) => {
          this.loading = false;
          this.selectedAll = false;
          this.report = data;
        },
        (err) => {
          this.loading = false;
          console.log(err);
        }
      );
  }

  /**
   * Marcar Todo
   */
  selectAll(): void {
    this.selectedAll = !this.selectedAll;
    this.report.forEach(item => {
      item.isSelected = this.selectedAll;
    });
  }

  /**
   * Enviar email a los docentes
   */
  sendMailNoteClaim(): void {
    this.loading = true;
    const dataReport: OEAReport = <OEAReport>{
      emailSubject: this.mailSubject.trim(),
      emailBody: this.msgEmail.trim(),
      lstNotesClaim: this.report.filter(f => f.isSelected === true),
    };
    this.oeaReportService.sendMailNoteClaim(dataReport)
      .subscribe(
        (result) => {
          console.log(result);
          this.mailSubject = null;
          this.msgEmail = null;
          this.loading = false;
        },
        (err) => {
          console.log(err);
          this.loading = false;
        }
      );
  }

  /**
   * Obtener nombre de tipo de reclamo
   * @param id Id de tupo
   */
  getNameStatus(id: number): string {
    return this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM.filter(f => f.id === id)[0].name;
  }

  /**
   * Retorna nombre de modalidad
   * @param id Código de modalidad
   */
  getNameDepartment(id: string): string {
    return this.backofficeConfigService.DEPARTAMENT_NAMES.filter( f => f.id === id)[0].name;
  }

  /**
   * Obtener nombre de sede
   * @param id Código de sede
   */
  getNameCampus(id: string): string {
    return this.allCampus.filter( f => f.id === id)[0].name;
  }

  /**
   * Descargar excel
   */
  downloadExcel(): void {
    const datos: any[] = this.report.map(
      (item) => {
        return {
          'Modalidad': this.getNameDepartment(item.subject.department),
          'Sede': this.getNameCampus(item.subject.campus),
          'Periodo': item.subject.term,
          'Número': item.year + '-' + this.backofficeConfigService.completeStringWithCaracter(item.noteClaimNumber.toString(), '0', 6),
          'Tipo': item.type,
          'Estado': this.getNameStatus(item.status),
          'Cód. Asignatura': item.subject.subjetID,
          'Nrc': item.subject.nrc,
          'Asignatura': item.subject.subjetName,
          'Componente': item.componentData.description,
          'Doc. Estudiante': item.studentData.document_number,
          'Estudiante': item.studentData.full_name,
          'Docente': item.teacherData.full_name,
          'Fecha': item.dateCreated.toString().replace('T', ' '),
        };
      });
    this.backofficeConfigService.exportAsExcelFile(datos, 'Reporte reclamo de notas');
  }
}
