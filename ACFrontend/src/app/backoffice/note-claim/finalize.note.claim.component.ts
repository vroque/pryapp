import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BackofficeConfigService } from '../config/config.service';
import { OEANoteClaim } from '../../shared/services/learning-assessment/OEANoteClaim';
import { OEANoteClaimService } from '../../shared/services/learning-assessment/oeaNoteClaim.service';

@Component({
  selector: 'oea-note-claim-finalize-report',
  templateUrl: './finalize.note.claim.component.html',
  providers: [BackofficeConfigService, OEANoteClaimService]
})

export class OEANoteClaimFinalizeReportComponent implements OnInit {
  @Input() claim: OEANoteClaim;
  @Output() returnInfo = new EventEmitter();

  constructor(private backofficeConfigService: BackofficeConfigService,
    private oeaNoteClaimService: OEANoteClaimService) { }

  ngOnInit(): any {
    this.claim.comment = '';
  }
  /**
   * Validar si habilitar o no los botones
   */
  validateBtnSave(): boolean {
    let val = true;
    if (this.claim.reportTeacher == null || this.claim.comment == null
      || this.claim.comment.trim().length <= 0) {
      val = false;
    }
    return val;
  }

  /**
   * Guardar
   */
  save(): void {
    this.returInfoParent(true, false, '');
    if (this.validateBtnSave() ) {
      this.claim.comment = this.claim.comment.trim();
      this.oeaNoteClaimService.finalizeClaim(this.claim)
        .subscribe(
          data => {
            console.log(data);
            this.returInfoParent(false, true, '');
            this.claim = null;
          },
          (err) => {
            console.log(err);
            this.returInfoParent(false, false, 'Error al finalizar reclamo.');
          }
        );
    }
  }

  /**
   * Retornar valores a componente padre
   * @param load Carga?
   * @param success SAtisfactorio?
   * @param msg mensaje
   */
  returInfoParent(load: boolean, success: boolean, msg: string): void {
    this.returnInfo.emit([load, success, msg]);
  }
}
