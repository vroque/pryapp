import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BackofficeConfigService } from '../config/config.service';
import { OEANoteClaim } from '../../shared/services/learning-assessment/OEANoteClaim';
import { OEANoteClaimService } from '../../shared/services/learning-assessment/oeaNoteClaim.service';

@Component({
  selector: 'oea-derive-teacher-note-claim',
  templateUrl: './derive.to.teacher.component.html',
  providers: [BackofficeConfigService, OEANoteClaimService]
})

export class OEANoteClaimDeriveTeacherComponent {
  @Input() claim: OEANoteClaim;
  @Output() returnInfo = new EventEmitter();
  constructor(private backofficeConfigService: BackofficeConfigService,
    private oeaNoteClaimService: OEANoteClaimService) { }


  /**
  * Obtener numero de días pasados hasta hoy
  * @param date Fecha de creación
  */
  countNumberDays(date: string): number {
    const dateCreated = new Date(date).getTime();
    const now = new Date().getTime();
    const diff = now - dateCreated;
    return (diff / (1000 * 60 * 60 * 24));
  }

  /**
   * Validar datos antes de permitir guardar
   */
  validateBtnSave(): boolean {
    let isValid = true;
    if (this.claim == null || this.claim === undefined
      || this.claim.comment == null || this.claim.comment.trim() === '' || this.claim.type == null
      || this.backofficeConfigService.OEA_TYPES_NOTE_CLAIM.filter(f => f.id === this.claim.type).length <= 0) {
      isValid = false;
    }
    return isValid;
  }

  /**
   * Guardar derivación a docente
   */
  saveDerivationTeacher(): void {
    this.returInfoParent(true, false, '');
    if (this.validateBtnSave()) {
      this.claim.comment = this.claim.comment.trim();
      this.oeaNoteClaimService.saveDerivationTeacher(this.claim)
        .subscribe(
          (data) => {
            console.log(data);
            this.returInfoParent(false, true, '');
            this.claim = null;
          },
          (err) => {
            console.log(err);
            this.returInfoParent(false, false, 'Error al derivar a docente.');
          }
        );
    }
  }

  /**
   * Retornar valores a componente padre
   * @param load Carga?
   * @param success SAtisfactorio?
   * @param msg mensaje
   */
  returInfoParent(load: boolean, success: boolean, msg: string): void {
    this.returnInfo.emit([load, success, msg]);
  }
}
