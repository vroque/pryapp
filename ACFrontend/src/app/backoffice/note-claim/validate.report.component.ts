import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BackofficeConfigService } from '../config/config.service';
import { OEANoteClaim } from '../../shared/services/learning-assessment/OEANoteClaim';
import { OEAReportNoteClaimService } from '../../shared/services/learning-assessment/oeaReportNoteClaim.service';


@Component({
  selector: 'oea-note-claim-validate-report',
  templateUrl: './validate.report.component.html',
  providers: [BackofficeConfigService, OEAReportNoteClaimService]
})

export class OEANoteClaimValidateReportComponent {
  @Input() claim: OEANoteClaim;
  @Output() returnInfo = new EventEmitter();
  constructor(private backofficeConfigService: BackofficeConfigService,
    private oeaReportNoteClaimService: OEAReportNoteClaimService) { }

  /**
   * Validar si habilitar o no los botones
   */
  validateBtnSave(): boolean {
    let val = true;
    if (this.claim.reportTeacher == null || this.claim.reportTeacher.comment == null
      || this.claim.reportTeacher.comment.trim().length <= 0) {
      val = false;
    }
    return val;
  }

  /**
   * Guardar
   * @param proceeds Procede?
   */
  save(approved: boolean): void {
    this.returInfoParent(true, false, '');
    if (this.validateBtnSave() && approved != null) {
      this.claim.reportTeacher.comment = this.claim.reportTeacher.comment.trim();
      this.claim.reportTeacher.approved = approved;
      this.oeaReportNoteClaimService.validateReport(this.claim.reportTeacher)
        .subscribe(
          data => {
            console.log(data);
            this.returInfoParent(false, true, '');
            this.claim = null;
          },
          (err) => {
            console.log(err);
            this.returInfoParent(false, false, 'Error al guardar validación');
          }
        );
    }
  }

  /**
   * Retornar valores a componente padre
   * @param load Carga?
   * @param success SAtisfactorio?
   * @param msg mensaje
   */
  returInfoParent(load: boolean, success: boolean, msg: string): void {
    this.returnInfo.emit([load, success, msg]);
  }
}
