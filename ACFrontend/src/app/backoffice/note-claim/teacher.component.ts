import { Component, OnInit } from '@angular/core';
import { BackofficeConfigService } from '../config/config.service';
import { ConfigService } from '../../config/config.service';
import { OEANoteClaim } from '../../shared/services/learning-assessment/OEANoteClaim';
import { OEANoteClaimService } from '../../shared/services/learning-assessment/oeaNoteClaim.service';
import { OEAReportNoteClaimService } from '../../shared/services/learning-assessment/oeaReportNoteClaim.service';
import { OEAReportNoteClaim } from '../../shared/services/learning-assessment/oeaReportNoteClaim';
import { OEAFileNoteClaimService } from '../../shared/services/learning-assessment/oeaFileNoteClaim.service';


@Component({
  selector: 'oea-note-claim-teacher-component',
  templateUrl: './teacher.component.html',
  providers: [BackofficeConfigService, ConfigService, OEANoteClaimService, OEAReportNoteClaimService, OEAFileNoteClaimService]
})

export class OEANoteClaimTeacherComponent implements OnInit {
  loading: boolean;
  msgError: string;
  filter: any;
  listNoteClaims: OEANoteClaim[];
  claimSelected: OEANoteClaim;
  reportTeacher: OEAReportNoteClaim;
  files: HTMLInputElement[] = [];

  es: any;
  minDate = new Date();
  dateChange: Date;
  constructor(private backofficeConfigService: BackofficeConfigService, private configService: ConfigService,
    private oeaNoteClaimService: OEANoteClaimService, private oeaReportNoteClaimService: OEAReportNoteClaimService,
    private oeaFileNoteClaimService: OEAFileNoteClaimService) { }
  ngOnInit(): void {
    this.es = this.configService.getLocationCalendar();
    this.minDate.setDate(this.minDate.getDate() + 2);
    this.getListNoteClaims();
  }

  /**
   * Obtener lista de reclamos
   */
  getListNoteClaims(): void {
    this.loading = true;
    this.oeaNoteClaimService.getListNotesClaimByTeacher()
      .subscribe(
        (data) => {
          this.listNoteClaims = data;
          this.msgError = null;
          this.loading = false;
        },
        (err) => {
          this.msgError = 'Error al obtener lista de reclamos.';
          console.log(err);
          this.loading = false;
        }
      );
  }

  /**
   * Seleccionar un reclamo
   * @param data Reclamo selecciónado
   */
  selectedClaim(data: OEANoteClaim): void {
    this.loading = true,
      this.claimSelected = data;
    this.reportTeacher = new OEAReportNoteClaim();
    this.reportTeacher.noteClaimId = this.claimSelected.id;
    this.files = [];
  }

  /**
   * que hacer cuando el subcomponente detalle termina de cargar
   * @param event Retorno de componente de detalle de reclamo
   */
  chargueDetailsClaim(event): void {
    if (event[0]) {
      this.msgError = null;
      this.claimSelected = event[0];
    } else {
      this.msgError = event[1];
    }
    this.loading = false;
  }

  /**
   * Asignar archivo en un array
   * @param event evento del inputo file
   * @param index indice del array
   */
  assignFile(event: any, index: number): void {
    this.files[index] = <HTMLInputElement>event.target.files[0];
    if (this.files[index].size > 3000000) {
      this.files[index] = null;
      alert('Máximo 2mb por archivo.');
    }
  }

  /**
   * Validar si se puede guardar
   */
  validateSave(): boolean {
    let isValid = true;
    if (this.reportTeacher == null || this.reportTeacher === undefined) {
      isValid = false;
    }
    if (this.reportTeacher.noteClaimId == null || this.reportTeacher.noteClaimId <= 0) {
      isValid = false;
    }
    if (this.reportTeacher.reason == null || this.reportTeacher.reason.trim().length <= 0) {
      isValid = false;
    }
    if (this.reportTeacher.proceeds) {
      if (this.dateChange == null || this.dateChange === undefined
        || this.dateChange < this.minDate) {
        isValid = false;
      }
      if (this.reportTeacher.scoreCorrect == null || this.reportTeacher.scoreCorrect === undefined
        || !/^([0-9])*$/.test(this.reportTeacher.scoreCorrect)
        || parseFloat(this.reportTeacher.scoreCorrect) < 0 || parseFloat(this.reportTeacher.scoreCorrect) > 20) {
        isValid = false;
      }
    }
    if (this.files.filter(Boolean).filter(f => f.name.length > 0 && f.size > 0).length <= 0) {
      isValid = false;
    }
    return isValid;
  }

  /**
   * Guardar Informe
   */
  save(): void {
    if (this.validateSave() && confirm('Recuerda validar lo que esta enviando')) {
      this.loading = true;
      this.reportTeacher.reason = this.reportTeacher.reason.trim();
      if (this.reportTeacher.proceeds) {
        this.reportTeacher.dateChange = this.backofficeConfigService.convertToUTC(this.dateChange);
      }
      this.oeaReportNoteClaimService.save(this.reportTeacher)
        .subscribe(
          (data) => {
            this.loading = false;
            if (data.isValid) {
              this.reportTeacher = data;
              this.msgError = null;
              this.saveAndUploadFile();
            } else {
              this.msgError = data.msgValid;
            }
          },
          (err) => {
            console.log(err);
            this.msgError = 'Error al guardar informe.';
            this.loading = false;
          }
        );
    }
  }

  /**
   * Subir y guardar archivos
   */
  saveAndUploadFile(): void {
    this.loading = true;
    this.files = this.files.filter(Boolean).filter(f => f.name.length > 0 && f.size > 0);
    let count = 0;
    if (this.files.length > 0 && this.reportTeacher.noteClaimId != null && this.reportTeacher.id != null) {
      this.files.forEach(file => {
        this.oeaFileNoteClaimService.SaveAndSubmitFile(this.reportTeacher.noteClaimId, this.reportTeacher.id, file)
          .subscribe(
            (submit) => {
              console.log(submit);
              count++;
              if (count >= this.files.length) {
                this.msgError = null;
                this.loading = false;
                this.getListNoteClaims();
                this.claimSelected = null;
              }
            },
            (err) => {
              console.log('Error al subir archivo -> ', err);
              count++;
              if (count >= this.files.length) {
                this.msgError = null;
                this.loading = false;
                this.getListNoteClaims();
                this.claimSelected = null;
              }
            }
          );
      });
    }
  }
}
