import { Component, OnInit, Input } from '@angular/core';
import { BackofficeConfigService } from '../config/config.service';
import { OEANoteClaim } from '../../shared/services/learning-assessment/OEANoteClaim';
import { OEANoteClaimService } from '../../shared/services/learning-assessment/oeaNoteClaim.service';

@Component({
  selector: 'oea-modify-note-claim-component',
  templateUrl: './modify.note.claim.component.html',
  providers: [BackofficeConfigService, OEANoteClaimService]
})

export class OEAModifyNoteClaimComponent implements OnInit {
  @Input() officeId: number;
  @Input() status: number;

  loading: boolean;
  msgError: string;
  listPending: OEANoteClaim[];
  listToFinalize: OEANoteClaim[];
  listSelected: OEANoteClaim[];
  claimSelected: OEANoteClaim;

  constructor(private backofficeConfigService: BackofficeConfigService,
    private oeaNoteClaimService: OEANoteClaimService) { }

  ngOnInit(): any {
    this.getData();
  }

  /**
   * Obtener Data
   */
  getData(): any {
    this.getListNoteClaimsByStatusAndOffice(this.status, this.officeId);
    if (this.isRAU()) {
      this.getListNoteClaimsByStatusAndOffice(this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_RAU, this.officeId);
    }
  }

  /**
   * Obtener lista de reclamos
   */
  getListNoteClaimsByStatusAndOffice(status: number, officeId: number): void {
    this.loading = true;
    this.oeaNoteClaimService.getListNotesClaimByStatusAndOffice(status, officeId)
      .subscribe(
        (data) => {
          if (this.isRAU() && status === this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_RAU) {
            this.listToFinalize = data;
          } else {
            this.listSelected = this.listPending = data;
          }
          this.loading = false;
        },
        (err) => {
          this.msgError = 'Error al obtener lista de reclamos.';
          console.log(err);
          this.loading = false;
        },
      );
  }

  /**
   * Seleccionar lista
   */
  selectedList(type: number): void {
    switch (type) {
      case 1:
        this.listSelected = this.listPending;
        break;
      case 2:
        this.listSelected = this.listToFinalize;
        break;
      default:
        break;
    }
  }

  /**
   * Seleccionar un reclamo
   * @param data Reclamo selecciónado
   */
  selectedClaim(data: OEANoteClaim): void {
    this.loading = true;
    this.claimSelected = data;
  }

  /**
   * que hacer cuando el subcomponente detalle termina de cargar
   * @param event Retorno de componente de detalle de reclamo
   */
  chargueDetailsClaim(event): void {
    if (event[1]) {
      this.msgError = null;
      this.claimSelected = event[0];
    } else {
      this.msgError = event[1];
    }
    this.loading = false;
  }

  /**
   * Obtener respuesta de componentes hijos
   */
  getInfoChild(event): void {
    this.loading = event[0];
    if (!event[1]) {
      this.msgError = event[2];
    } else {
      this.claimSelected = null;
      this.getData();
    }
  }

  /**
   * Validar si es registros academicos
   */
  isRAU(): boolean {
    let val = false;
    if (this.officeId === this.backofficeConfigService.OFFICE_RAU) {
      val = true;
    }
    return val;
  }

  /**
   * Validar si reclamo esta en pendiente de derivación a docente (CAS)
   */
  derivateToTeacher(): boolean {
    let val = false;
    if (this.officeId === this.backofficeConfigService.OFFICE_CAU
      && this.claimSelected.status === this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_CAS
      && this.claimSelected.reportTeacher == null) {
      val = true;
    }
    return val;
  }

  /**
   * Validar si reclamo esta en pendiente de derivación de reporte
   */
  validateReport(): boolean {
    let val = false;
    if (!this.derivateToTeacher() && this.claimSelected.reportTeacher != null
      && this.officeId !== this.backofficeConfigService.OFFICE_CAU
      && this.claimSelected.status === this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_INVOLVED_AREA
      && this.claimSelected.reportTeacher.status === this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_REPORT_PENDING) {
      val = true;
    }
    return val;
  }

  /**
   * Validar si reclamo esta en pendiente de finalización (RAU)
   */
  validateFinalize(): boolean {
    let val = false;
    if (!this.validateReport() && this.claimSelected.reportTeacher != null
      && this.officeId === this.backofficeConfigService.OFFICE_RAU
      && this.claimSelected.status === this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_RAU
      && this.claimSelected.reportTeacher.status === this.backofficeConfigService.OEA_STATUS_NOTE_CLAIM_REPORT_ACCEPTED) {
      val = true;
    }
    return val;
  }
}
