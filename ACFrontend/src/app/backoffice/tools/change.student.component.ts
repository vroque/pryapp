import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { StudentService } from '../../shared/services/academic/student.service';
import { BackofficeConfigService } from '../config/config.service';

import { Student } from '../../shared/services/academic/student';

/*
 * Componente para Cambiar a perfil de estudiante
 */
@Component({
  selector: 'change-student',
  templateUrl: './change.student.component.html',
  providers: [BackofficeConfigService, StudentService],
})
export class ToolsChangeStudentComponent {
  errorMessage: string;
  findStudents: Student[] = [];
  student: any = {code: ''};
  searching: boolean;

  constructor(private router: Router,
              private config: BackofficeConfigService,
              private studentService: StudentService) {
  }

  /* Busca al estudiante por su DNI */
  searchStudent(studentId: string): void {
    this.errorMessage = null;
    this.findStudents = [];
    this.searching = true;
    this.studentService.find(studentId.trim(), 'student_id')
      .subscribe(
        (student) => {
          this.findStudents = [];
          this.findStudents.push(student);
        },
        (error) => {
          this.errorMessage = error;
          this.findStudents = [];
          this.searching = false;
        },
        () => {
          console.log('students -> ', this.findStudents);
          this.searching = false;
        },
      );
  }

  /* Redirige a la pagina para el cambio a perfil de estudiante */
  changeToStudent(studentId: string): void {
    (window as any).location.href = `${(window as any).url_base}backoffice/cambiar-a-estudiante/${studentId}`;
  }
}
