import { Component, OnInit } from '@angular/core';
import { Graduated } from '../../shared/services/academic/graduated';
import { GraduatesService } from '../../shared/services/academic/graduates.service';
import { GraduationService } from '../../shared/services/academic/graduation.service';

@Component({
  selector: 'graduates-search-graduates',
  templateUrl: './search-graduates.component.html',
  providers: [GraduatesService, GraduationService]
})
export class SearchGraduatesComponent implements OnInit {
  loading: boolean;
  searching: boolean;
  processing: boolean;
  showStudentIdList: boolean;
  showSearchResult: boolean;

  estudiantes: any; // modelo para el input de Estudiantes
  students: any[]; // array de 'estudiantes' a procesar
  graduates: Graduated[]; // egresados UC
  tuqpa: any[];

  constructor(private graduatesService: GraduatesService, private graduationService: GraduationService) {
  }

  ngOnInit(): void {
  }

  clean(): void {
    this.estudiantes = undefined;
    this.searching = false;
    this.processing = false;
    this.students = [];
    this.graduates = [];
    this.tuqpa = [];
    this.showSearchResult = false;
  }

  /**
   * Bandera para activar/desactivar el botón "Buscar Egresados"
   * @return {boolean} [description]
   */
  isAllSelected(): boolean {
    return this.estudiantes !== null && this.estudiantes !== undefined && this.estudiantes.trim() !== '';
  }

  /**
   * Bandera para activar/desactivar el botón "Limpiar"
   * @return {boolean} [description]
   */
  isAnySelected(): boolean {
    return this.isAllSelected();
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray(): void {
    this.searching = false;
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // http://stackoverflow.com/a/9229821
    this.students = this.estudiantes.split(' ')
      .filter(// filter blank space
        (id) => id !== '',
      )
      .filter(// filter duplicates
        (id, position, self) => self.indexOf(id) === position,
      );
    console.log('input students => ', this.students);
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList(): void {
    this.showStudentIdList = !this.showStudentIdList;
  }

  getGraduates(): void {
    console.log('Get Graduates...');
    this.searching = true;
    this.tuqpa = [];
    this.graduates = [];
    this.showSearchResult = false;
    const type = 'searchBatch';
    this.graduationService.getGraduates(type, null, null, this.students)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => {
          this.searching = false;
          this.showSearchResult = true;
        },
        () => {
          this.searching = false;
          this.showSearchResult = true;
        }
      );
  }
}
