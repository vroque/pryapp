import { Component, OnInit } from '@angular/core';

import { GraduationService } from '../../shared/services/academic/graduation.service';
import { Period } from '../../shared/services/academic/period';
import { GraduatesService } from '../../shared/services/academic/graduates.service';
import { MajorService } from '../../shared/services/academic/major.service';
import { ExcelService } from '../../shared/services/excel/excel.service';
import { Graduated } from '../../shared/services/academic/graduated';

@Component({
  selector: 'graduates-graduates-report',
  templateUrl: './graduates-report.component.html',
  providers: [GraduationService, GraduatesService, MajorService, ExcelService]
})
export class GraduatesReportComponent implements OnInit {

  loading: boolean;
  searching: boolean;
  processing: boolean;
  showSearchResult: boolean;
  downloadHistory: boolean;
  majors: any[] = [];
  fileName = '';

  graduates: Graduated[] = [];
  graduatesByTerm: Graduated[] = [];
  graduationTerm: string;
  graduationTerms: Period[] = [];

  constructor(private graduationService: GraduationService, private graduatesService: GraduatesService,
              private majorService: MajorService, private excelService: ExcelService) {
  }

  ngOnInit() {
    this.clean();
    this.getGraduationsTerms();
    this.getMajors();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.graduationTerm = undefined;
    this.showSearchResult = false;
  }

  /**
   * Clean search results and graduates when select new term
   */
  selectNewTerm(): void {
    this.showSearchResult = false;
    this.graduatesByTerm = [];
  }

  /**
   * Show option for get all graduates
   */
  history(): void {
    this.downloadHistory = !this.downloadHistory;
    if (this.graduatesByTerm.length) {
      this.showSearchResult = !this.showSearchResult;
    }
  }

  /**
   * Get all graduates
   */
  getAllGraduates(): void {
    this.processing = true;
    this.graduatesService.getAllGraduates()
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => {
          console.error(error);
          this.processing = false;
        },
        () => {
          this.processing = false;
          for (const key of Object.keys(this.graduates)) {
            this.graduates[key].school_name = this.majors.filter((major) => major.id === this.graduates[key].school_id)[0].name;
          }
        }
      );
    console.log('download history...');
  }

  /**
   * Get all UC majors
   */
  getMajors(): void {
    this.majorService.getMajors()
      .subscribe(
        (majors) => this.majors = majors,
        (error) => console.log(error)
      );
  }

  /**
   * Graduations terms
   */
  getGraduationsTerms(): void {
    this.loading = true;
    this.graduationService.getGraduationPeriods()
      .subscribe(
        (terms) => this.graduationTerms = terms,
        (error) => {
          console.log(error);
          this.loading = !this.loading;
        },
        () => {
          console.log('terms -> ', this.graduationTerms);
          this.loading = !this.loading;
          this.fileName = `egresados_` +
            `${this.graduationTerms[this.graduationTerms.length - 1].periodId}_${this.graduationTerms[0].periodId}`;
        }
      );
  }

  /**
   * Get graduates by term
   * @param term
   */
  getGraduatesByTerm(term: string): void {
    this.graduatesByTerm = [];
    this.searching = true;
    this.showSearchResult = false;
    this.graduatesService.getGraduatesByTerm(term)
      .subscribe(
        (graduatesByTerm) => this.graduatesByTerm = graduatesByTerm,
        (error) => {
          console.log(error);
          this.searching = !this.searching;
          this.showSearchResult = !this.showSearchResult;
        },
        () => {
          this.searching = !this.searching;
          this.showSearchResult = !this.showSearchResult;
          for (const key of Object.keys(this.graduatesByTerm)) {
            this.graduatesByTerm[key].school_name = this.majors.filter((major) => major.id === this.graduatesByTerm[key].school_id)[0].name;
          }
        }
      );
  }

  searchGraduates(): void {
    if (this.graduationTerm !== null && this.graduationTerm !== undefined) {
      this.getGraduatesByTerm(this.graduationTerm);
    } else {
      console.log('Valor inválido: ', this.graduationTerm);
    }
  }

  download(): void {
    if (this.graduates.length) {
      const data: any[] = this.getDataForExport(this.graduates);
      this.excelService.exportAsExcelFile(data, this.fileName);
    }
  }

  downloadGraduates(): void {
    if (this.graduationTerm !== null && this.graduationTerm !== undefined && this.graduatesByTerm.length) {
      const fileName = `egresados-${this.graduationTerm}`;
      const data: any[] = this.getDataForExport(this.graduatesByTerm);
      this.excelService.exportAsExcelFile(data, fileName);
    }
  }

  /**
   * Preparing data for export
   * @param data
   */
  getDataForExport(data: any[]): any[] {
    return data.map(
      (item) => {
        return {
          'Código': item.student_id,
          'Nombres': item.student_name,
          'Período egreso': item.graduation_period,
          'Código Programa': item.school_id,
          'Programa': item.school_name,
          'Sede': item.campusId,
          'Modalidad': item.modality_id === 'UREG' ? 'Regular' : item.modality_id === 'UPGT' ? 'Gente que trabaja' : 'A distancia',
          'Creditos': item.credits_o + item.credits_e,
          'Creditos Obligatorios': item.credits_o,
          'Creditos Electivos': item.credits_e,
          'Proyección': item.volunteering ? 'si' : 'no',
          'Práctica': item.internship ? 'si' : 'no',
          'Idioma': item.foreign_language ? 'si' : 'no',
          'Bachiller': item.bachiller ? 'si' : 'no',
          'Titulo': item.titulo ? 'si' : 'no'
        };
      }
    );
  }

}
