import { Component, OnInit } from '@angular/core';

import { ConfigService } from '../../config/config.service';
import { Graduated } from '../../shared/services/academic/graduated';
import { GraduatesService } from '../../shared/services/academic/graduates.service';
import { GraduationService } from '../../shared/services/academic/graduation.service';

@Component({
  selector: 'bo-graduates',
  templateUrl: './graduates-compliance.component.html',
  providers: [GraduatesService, GraduationService, ConfigService]
})
export class GraduatesComplianceComponent implements OnInit {

  loading: boolean;
  searching: boolean;
  processing: boolean;
  showStudentIdList: boolean;
  showSearchResult: boolean;
  selected: boolean;

  es: any;
  bachelorResolutionDate: Date;  // modelo para el input de fecha de egresado bachiller
  titleResolutionDate: Date;  // modelo para el input de fecha de egresado titulado
  wata: number;

  estudiantes: any; // modelo para el input de Estudiantes
  students: any[]; // array de 'estudiantes' a procesar
  graduates: Graduated[]; // egresados UC
  tuqpa: any[];

  constructor(private graduatesService: GraduatesService, private graduationService: GraduationService,
              private configService: ConfigService) {
  }

  ngOnInit(): void {
    this.es = this.configService.getCalendarEs();
    this.wata = (new Date()).getFullYear();
  }

  clean(): void {
    this.estudiantes = undefined;
    this.searching = false;
    this.processing = false;
    this.students = [];
    this.graduates = [];
    this.tuqpa = [];
    this.showSearchResult = false;
  }

  /**
   * Bandera para activar/desactivar el botón "Buscar Egresados"
   * @return {boolean} [description]
   */
  isAllSelected(): boolean {
    return this.estudiantes !== null && this.estudiantes !== undefined && this.estudiantes.trim() !== '';
  }

  /**
   * Bandera para activar/desactivar el botón "Limpiar"
   * @return {boolean} [description]
   */
  isAnySelected(): boolean {
    return this.isAllSelected();
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray(): void {
    this.searching = false;
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // http://stackoverflow.com/a/9229821
    this.students = this.estudiantes.split(' ')
      .filter(// filter blank space
        (id) => id !== '',
      )
      .filter(// filter duplicates
        (id, position, self) => self.indexOf(id) === position,
      );
    console.log('input students => ', this.students);
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList(): void {
    this.showStudentIdList = !this.showStudentIdList;
  }

  /**
   * Get graduates
   */
  getGraduates(): void {
    console.log('Get Graduates...');
    this.searching = true;
    this.tuqpa = [];
    this.graduates = [];
    this.showSearchResult = false;
    this.selected = false;
    const type = 'searchBatch';
    this.graduationService.getGraduates(type, null, null, this.students)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => {
          this.searching = false;
          this.showSearchResult = true;
        },
        () => {
          this.searching = false;
          this.showSearchResult = true;
        }
      );
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * Añade estilo a los estudiantes seleccionados
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any): void {
    if (studentObj.is_selected) {
      studentObj.is_selected = false;
      console.log('Desactivado');
      // quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
    } else {
      studentObj.is_selected = true;
      console.log('activado');
      // agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);
      // console.log("this.tuqpa => ", this.tuqpa);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
    }
  }

  changeBachiller(graduate: Graduated): void {
    graduate.bachiller = !graduate.bachiller;
  }

  changeTitulo(graduate: Graduated): void {
    graduate.titulo = !graduate.titulo;
  }

  selectAll(): void {
    for (const key of Object.keys(this.graduates)) {
      this.graduates[key].is_selected = !this.selected;
    }

    this.selected = !this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Update graduates
   */
  updateGraduates(): void {
    console.log('update graduates');
    this.bachelorResolutionDate = this.bachelorResolutionDate === undefined ? null : this.bachelorResolutionDate;
    this.titleResolutionDate = this.titleResolutionDate === undefined ? null : this.titleResolutionDate;
    this.updateGraduatesSequentially(0);
  }

  updateGraduatesSequentially(i: number): void {
    if (this.graduates[i] !== undefined) {
      if (this.graduates[i].is_selected) {
        this.processing = true;
        this.graduates[i].processing = true;
        this.graduatesService.updateGraduates(this.graduates, this.bachelorResolutionDate, this.titleResolutionDate)
          .subscribe(
            (graduates) => this.graduates = graduates,
            (error) => {
              this.processing = false;
              this.graduates[i].processingError = true;
              console.warn(error);
            },
            () => {
              this.processing = false;
              this.graduates[i].processingError = false;
              this.updateGraduatesSequentially(i + 1);
            }
          );
      } else {
        console.log('no selected');
        this.updateGraduatesSequentially(i + 1);
      }
    } else {
      console.log('Nada que procesar => ', this.graduates[i]);
      this.bachelorResolutionDate = undefined;
      this.titleResolutionDate = undefined;
    }
  }

}
