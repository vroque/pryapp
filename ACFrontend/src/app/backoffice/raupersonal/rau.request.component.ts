import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Request } from '../../shared/services/atentioncenter/request';
import { RequestLog } from '../../shared/services/atentioncenter/request.log';
import { DocumentState } from '../../shared/services/atentioncenter/document.state';

import { RequestService } from '../../shared/services/atentioncenter/request.service';
import { RequestLogService } from '../../shared/services/atentioncenter/request.log.service';
import { DocumentStateService } from '../../shared/services/atentioncenter/document.state.service';
import { GenerateDocumentService } from '../../shared/services/atentioncenter/generate.document.service';
import { BackofficeConfigService } from '../config/config.service';
import { MailService } from '../../shared/services/mail/mail.service';

@Component({
  selector: 'request-list',
  templateUrl: './rau.request.component.html',
  providers: [
    RequestService, RequestLogService, DocumentStateService,
    GenerateDocumentService, BackofficeConfigService, MailService
  ],
})
export class RAURequestComponent implements OnInit {
  aprovate_message: string = 'Para su atención.';
  area = '';
  doc_status: string = 'pre'; // pre, on, err
  error_message: string;
  flow: Array<DocumentState>;
  ideal_flow: Array<DocumentState>;
  is_derivate: boolean = false;
  logs: Array<RequestLog>;
  request: Request;
  signatories: Array<string>;
  mail: any = {sending: false, sended: false, error: false};

  constructor(private _route: ActivatedRoute,
              private requestService: RequestService,
              private requestLogService: RequestLogService,
              private documentStateService: DocumentStateService,
              private generateDocumentService: GenerateDocumentService,
              private config: BackofficeConfigService, private mailService: MailService) {
  }

  ngOnInit(): void {
    this.request = null;
    this.flow = [];
    this.ideal_flow = [];
    this.logs = [];
    this._route.params.forEach((params: Params) => {
      let request_id: string = (<any>params).request;
      this.getRequest(request_id);
      this.getRequestLogs(request_id);
    });
  }

  getRequest(requestId: string): void {
    this.requestService.get(requestId)
      .subscribe(
        request => this.request = request,
        error => this.error_message = <any>error,
        () => {
          this.getDocumentFlow(this.request.document_id);
          (<any>this.request).opts = JSON.parse(this.request.description);
        }
      );
  }

  updateLogs(event: string) {
    this.getRequestLogs(String(this.request.id));
    this.request.state = this.config.STATE_DELIVERY_PENDING;
  }

  getRequestLogs(request_id: string): void {
    this.requestLogService.query(request_id)
      .subscribe(
        logs => this.logs = logs,
        error => this.error_message = <any>error,
        () => console.log('logs -> ', this.logs)
      );
  }

  getDocumentFlow(document_id: number): void {
    this.documentStateService.query(document_id)
      .subscribe(
        flow => this.flow = flow,
        error => this.error_message = <any>error
      );
  }

  getIdealFlow(flow: Array<DocumentState>): void {
    this.ideal_flow = flow.filter(state => {
      return (state.order !== -1);
    });
  }

  getLogTrace(state: number): RequestLog {
    for (let i: number = 0; i < this.logs.length; i++) {
      if (this.logs[i].old_state === state) {
        return this.logs[i];
      }
    }
    return null;
  }

  docSt(id: number): any {
    return this.config.getDocumentState(id);
  }

  aprovateRequest(event: any, request_id: string, message: string): void {
    let target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.requestService.update(request_id, {area_id: 3, message: message, type: 'aprove'})
      .subscribe(
        request => {
          this.request = request;
          this.getRequestLogs(String(request.id));
        },
        error => this.error_message = <any>error,
        () => this.is_derivate = true
      );
  }

  refuseRequest(event: any, request_id: string, message: string): void {
    let target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.requestService.update(request_id, {area_id: 3, message: message, type: 'refuse'})
      .subscribe(
        request => {
          this.request = request;
          this.getRequestLogs(String(request.id));
        },
        error => this.error_message = <any>error,
        () => this.is_derivate = true
      );
  }

  /**
   * marca la solicitud como entregada l
   * @param event
   * @param requestId
   */
  deliveryRequest(event: Event, requestId: any) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.requestService.save(requestId, {})
      .subscribe(
        (request) => {
          this.request = request;
          this.getRequestLogs(String(request.id));
        },
        (error) => console.log(error),
      );
  }

  derivateRequest(event: any, request_id: string, message: string): void {
    let target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    let office: number;
    switch (this.request.state) {
      case(this.config.STATE_DOCUMENT_VERIFICATION):
        office = this.config.OFFICE_DEU;
        break;
      case(this.config.STATE_CONVALIDATION_VERIFICATION):
        office = this.config.OFFICE_RAU;
        break;
      default:
        office = this.config.OFFICE_CAU;
    }
    this.requestService.update(request_id, {area_id: office, message: message, type: 'derive'})
      .subscribe(
        request => {
          this.request = request;
          this.getRequestLogs(String(request.id));
        },
        error => this.error_message = <any>error,
        () => {
          this.getRequestLogs(String(this.request.id));
          target.disabled = false;
        }
      );
  }

  /**
   * enviar correo con los documentos generados
   * @param event
   */
  sendDocumentByMail(event: Event) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.mail.sending = true;
    this.mailService.requestDocument(this.request.id)
      .subscribe(
        (data) => console.log(data),
        (error) => {
          this.mail.sending = false;
          this.mail.error = true;
          console.log(error);
        }, () => {
          target.disabled = true;
          this.mail.sending = false;
          this.mail.sended = true;
        },
      );
  }
}
