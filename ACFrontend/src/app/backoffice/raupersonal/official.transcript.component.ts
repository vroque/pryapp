import { Component, OnInit } from '@angular/core';
import { Graduated } from '../../shared/services/academic/graduated';
import { GraduationService } from '../../shared/services/academic/graduation.service';
import { OfficialTranscriptService } from '../../shared/services/atentioncenter/official.transcript.service';

@Component({
  selector: 'rau-official-transcript-batch',
  templateUrl: './official.transcript.component.html',
  providers: [GraduationService, OfficialTranscriptService]
})
export class RAUOfficialTranscriptComponent implements OnInit {
  load: boolean;
  errorMsg: string;

  stringCodes: string;
  lstCodes: string[];
  graduates: Graduated[];
  constructor(private graduationService: GraduationService,
    private officialTranscriptService: OfficialTranscriptService) {
  }
  ngOnInit(): void { }

  /**
   * Se parar codigos en Lista
   */
  separateCodes(): void {
    if (this.stringCodes.trim().length > 0) {
      this.lstCodes = this.stringCodes.split(' ');
    } else {
      this.lstCodes = undefined;
    }
  }

  /**
   * Limpiar Formulario
   */
  clear(): void {
    this.stringCodes = '';
    this.lstCodes = undefined;
    this.graduates = undefined;
  }

  /**
   * Seleccionar todos la lista
   */
  selectAll(): void {
    for (const key in this.graduates) {
      if (this.graduates.hasOwnProperty(key)) {
        const e: Graduated = this.graduates[key];
        if (e.school_id != null && e.school_id !== '0' && e.school_id.length > 0
          && e.payment_status !== 3 && e.payment_status !== 2) {
          e.is_selected = true;
        } else {
          e.is_selected = false;
        }
      }
    }
  }

  /**
   * Obtener estudiantes de lista de códigos escritos
   */
  getStudents(): void {
    this.load = true;
    const type = 'searchAllEntrant';
    this.graduationService.getGraduates(type, '', '', this.lstCodes)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => {
          this.errorMsg = 'No se puede obtener estudiantes.';
          console.log(error);
          this.load = false;
        },
        () => {
          this.load = false;
        }
      );
  }

  /**
   * Selecciónar estudiante
   * @param e Estudiante
   */
  setStudentsToProcess(e: Graduated): void {
    if (!e.is_selected && e.school_id != null && e.school_id !== '0'
      && e.school_id.length > 0 && e.payment_status !== 3 && e.payment_status !== 2) {
      e.is_selected = true;
    } else {
      e.is_selected = false;
    }
  }

  /**
   * Generar documentos de estudiantes seleccionados
   */
  generateDocument(): void {
    for (const key in this.graduates) {
      if (this.graduates.hasOwnProperty(key)) {
        const e: Graduated = this.graduates[key];
        if (e.is_selected && e.school_id != null && e.school_id !== '0'
          && e.school_id.length > 0 && e.payment_status !== 3 && e.payment_status !== 2) {
          this.load = true;
          e.processing = true;
          this.officialTranscriptService.createBatch(e)
            .subscribe(
              (rgraduate) => this.graduates[key] = rgraduate,
              (error) => {
                this.graduates[key].processingError = true;
                console.log(error);
                this.load = false;
              },
              () => {
                this.load = false;
              }
            );
        }
      }
    }
  }
}
