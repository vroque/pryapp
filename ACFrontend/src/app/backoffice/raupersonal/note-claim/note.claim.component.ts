import { Component } from '@angular/core';
import { BackofficeConfigService } from '../../config/config.service';
@Component({
  selector: 'oea-rau-note-claim',
  template: `<oea-modify-note-claim-component
              [officeId]="bConfigService.OFFICE_RAU"
              [status]="bConfigService.OEA_STATUS_NOTE_CLAIM_INVOLVED_AREA">
              </oea-modify-note-claim-component>`,
  providers: [BackofficeConfigService]
})

export class OEARauNoteClaimComponent {
  constructor(
    private bConfigService: BackofficeConfigService
  ) { }
}
