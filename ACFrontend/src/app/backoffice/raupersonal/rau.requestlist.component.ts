import {BackofficeConfigService} from '../config/config.service';
import {Component, OnInit} from '@angular/core';
import {RequestByAreaService} from '../../shared/services/atentioncenter/request.area.service';
import {Request} from '../../shared/services/atentioncenter/request';
import {Router} from '@angular/router';

@Component({
  providers: [RequestByAreaService, BackofficeConfigService],
  selector: 'request-list',
  templateUrl: './rau.requestlist.component.html'
})
export class RAURequestListComponent implements OnInit {

  errorMessage: string;
  inbox = 'pending';
  rauId = 3; // area id Registros Academicos
  requests: Request[];
  requestPending: Request[] = [];
  requestGraduationPending: Request[] = [];
  requestViewActive: Request[] = [];
  area = '';
  filter: any;
  configFile: any;

  constructor(
    private router: Router,
    private requestService: RequestByAreaService,
    private config: BackofficeConfigService
  ) {
    this.configFile = this.config;
  }

  ngOnInit() {
    this.requests = null;
    this.getRequests();
  }

  getRequests() {
    this.requestService.query(this.rauId)
      .subscribe(
        (requests) => this.attentionRequest(requests),
        (error) => this.errorMessage = error as any
      );
  }

  attentionRequest(requests: Request[]) {
    this.requests = requests;
    this.requestPending = this.requests.filter(
      (request) => request.document_id !== this.config.DOC_PROMOTIONAL_STUDIES_CERTIFICATE,
    );
    this.requestGraduationPending = this.requests.filter(
      (request) => request.document_id === this.config.DOC_PROMOTIONAL_STUDIES_CERTIFICATE,
    );
    this.requestViewActive = this.requestPending;
  }

  setInbox(event, type: string) {
    this.inbox = type;
    switch (type) {
      case ('pending'):
        this.requestViewActive = this.requestPending;
        break;
      case ('graduation'):
        this.requestViewActive = this.requestGraduationPending;
        break;
    }
  }

  gotoRequestDetail(id: string) {
    const link = [`backoffice/rau-personal/solicitudes/${id}`];
    this.router.navigate(link);
  }
}
