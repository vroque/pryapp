import { Component, OnInit } from '@angular/core';
import { GraduationService } from '../../../shared/services/academic/graduation.service';
import { Graduated } from '../../../shared/services/academic/graduated';
import { Proof3510PromotionalService } from '../../../shared/services/atentioncenter/proof.3510.promotional.service';


@Component({
  selector: 'rau-proof-3510-promotional',
  templateUrl: './proof.3510.promotional.component.html',
  providers: [GraduationService, Proof3510PromotionalService]
})
export class RAUProof3510PromotionalComponent implements OnInit {
  load: boolean;
  errorMsg: string;
  stringCodes: string;
  lstCodes: string[];
  graduates: Graduated[];
  constructor(
    private graduationService: GraduationService,
    private proof3510PeriodPromotionalService: Proof3510PromotionalService
  ) { }

  ngOnInit(): void { }

  /**
   * Se parar codigos en Lista
   */
  separateCodes(): void {
    if (this.stringCodes.trim().length > 0) {
      this.lstCodes = this.stringCodes.split(' ');
    } else {
      this.lstCodes = undefined;
    }
  }

  /**
   * Limpiar Formulario
   */
  clear(): void {
    this.stringCodes = '';
    this.lstCodes = undefined;
    this.graduates = undefined;
  }

  /**
   * Obtener estudiantes de lista de códigos escritos
   */
  getStudents(): void {
    this.load = true;
    const type = 'searchBatch';
    this.graduationService.getGraduates(type, '', '', this.lstCodes)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => {
          this.errorMsg = 'No se puede obtener estudiantes.';
          console.log(error);
          this.load = false;
        },
        () => {
          this.load = false;
        }
      );
  }

  /**
   * Seleccionar todos los estudiantes
   */
  selectAll(): void {
    for (const key in this.graduates) {
      if (this.graduates.hasOwnProperty(key)) {
        const e: Graduated = this.graduates[key];
        if (e.school_id != null && e.school_id !== '0' && e.school_id.length > 0
          && e.payment_status !== 3 && e.payment_status !== 2 && e.academic_status === 4) {
          e.is_selected = true;
        } else {
          e.is_selected = false;
        }
      }
    }
  }

  /**
   * MArcar estudiante
   * @param e Estudiante
   */
  setStudentsToProcess(e: Graduated): void {
    if (!e.is_selected && e.school_id != null && e.school_id !== '0' && e.school_id.length > 0
      && e.payment_status !== 3 && e.payment_status !== 2 && e.academic_status === 4) {
      e.is_selected = true;
    } else {
      e.is_selected = false;
    }
  }

  /**
   * Generar documentos de estudiantes seleccionados
   */
  generateDocument(): void {
    for (const key in this.graduates) {
      if (this.graduates.hasOwnProperty(key)) {
        const e: Graduated = this.graduates[key];
        if (e.is_selected && e.school_id != null && e.school_id !== '0' && e.school_id.length > 0
          && e.payment_status !== 3 && e.payment_status !== 2 && e.academic_status === 4) {
          this.load = true;
          e.processing = true;
          this.proof3510PeriodPromotionalService.createBatch(e)
            .subscribe(
              (rgraduate) => this.graduates[key] = rgraduate,
              (error) => {
                this.graduates[key].processingError = true;
                console.log(error);
                this.load = false;
              },
              () => {
                this.load = false;
              }
            );
        }
      }
    }
  }

}
