import { Component, OnInit } from '@angular/core';

import { EnrollmentTermService } from '../../../shared/services/academic/enrollmentTerm.service';
import { GraduationService } from '../../../shared/services/academic/graduation.service';
import { ProofOfEnrollmentService } from '../../../shared/services/atentioncenter/proof.of.enrollment.service';
import { Graduated } from '../../../shared/services/academic/graduated';
import { Period } from '../../../shared/services/academic/period';
import { Proof3510PeriodService } from '../../../shared/services/atentioncenter/proof.3510.period.service';

@Component({
  selector: 'rau-proof-3510-period',
  templateUrl: './proof.3510.period.component.html',
  providers: [EnrollmentTermService, GraduationService, ProofOfEnrollmentService, Proof3510PeriodService]
})

export class RAUProof3510PeriodComponent implements OnInit {
  load: boolean;
  errorMsg: string;

  stringCodes: string;
  lstCodes: string[];
  lstTerms: Period[];
  term: string;
  graduates: Graduated[];

  constructor(private graduationService: GraduationService,
    private enrollmentTermService: EnrollmentTermService,
    private proof3510PeriodService: Proof3510PeriodService) {
  }
  ngOnInit(): void {
    this.getPeriods();
  }
  /**
   * Se parar codigos en Lista
   */
  separateCodes(): void {
    if (this.stringCodes.trim().length > 0) {
      this.lstCodes = this.stringCodes.split(' ');
    } else {
      this.lstCodes = undefined;
    }
  }

  /**
   * Limpiar Formulario
   */
  clear(): void {
    this.stringCodes = '';
    this.lstCodes = undefined;
    this.term = undefined;
    this.graduates = undefined;
  }

  /**
   *  Obtener todos los periodos
   */
  getPeriods(): void {
    this.load = true;
    this.enrollmentTermService.getAllEnrollmentPeriods()
      .subscribe(
        (terms) => this.lstTerms = terms,
        (error) => {
          this.errorMsg = 'No se cargaron periodos de matrícula';
          this.load = false;
          console.log(error);
        },
        () => {
          this.load = false;
        }
      );
  }

  /**
   * Obtener estudiantes de lista de códigos escritos
   */
  getStudents(): void {
    this.load = true;
    const type = 'searchAllEntrant';
    this.graduationService.getGraduates(type, '', '', this.lstCodes)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => {
          this.errorMsg = 'No se puede obtener estudiantes.';
          console.log(error);
          this.load = false;
        },
        () => {
          this.load = false;
        }
      );
  }

  /**
   * Seleccionar estudiante
   * @param e Estudiante
   */
  setStudentsToProcess(e: Graduated): void {
    if (!e.is_selected && e.school_id != null && e.school_id !== '0'
      && e.school_id.length > 0 && e.payment_status !== 3 && e.payment_status !== 2) {
      e.is_selected = true;
    } else {
      e.is_selected = false;
    }
  }

  /**
   * Seleccionar todos los estudiantes
   */
  selectAll(): void {
    for (const key in this.graduates) {
      if (this.graduates.hasOwnProperty(key)) {
        const e: Graduated = this.graduates[key];
        if (e.school_id != null && e.school_id !== '0' && e.school_id.length > 0
          && e.payment_status !== 3 && e.payment_status !== 2) {
          e.is_selected = true;
        } else {
          e.is_selected = false;
        }
      }
    }
  }

  /**
   * Generar documentos de estudiantes seleccionados
   */
  generateDocument(): void {
    for (const key in this.graduates) {
      if (this.graduates.hasOwnProperty(key)) {
        const e: Graduated = this.graduates[key];
        if (e.is_selected && e.school_id != null && e.school_id !== '0'
          && e.school_id.length > 0 && e.payment_status !== 3 && e.payment_status !== 2) {
          this.load = true;
          e.processing = true;
          this.proof3510PeriodService.createBatch(this.term, e)
            .subscribe(
              (rgraduate) => this.graduates[key] = rgraduate,
              (error) => {
                this.graduates[key].processingError = true;
                console.log(error);
                this.load = false;
              },
              () => {
                this.load = false;
              }
            );
        }
      }
    }
  }

  /**
   * Generar documento de otro periodo
   * @param dato Estudiante
   */
  generateOtherTerm(dato: Graduated): void {
    for (const key in this.graduates) {
      if (this.graduates.hasOwnProperty(key)) {
        const e: Graduated = this.graduates[key];
        if (e === dato) {
          if (e.school_id != null && e.school_id !== '0' && e.school_id.length > 0
            && e.payment_status !== 3 && e.payment_status !== 2 && e.other_period != null) {
            this.load = true;
            e.processing = true;
            this.proof3510PeriodService.createBatch(e.other_period, e)
              .subscribe(
                (rgraduate) => this.graduates[key] = rgraduate,
                (error) => {
                  e.processingError = true;
                  console.log(error);
                  this.load = false;
                },
                () => {
                  this.load = false;
                }
              );
            return;
          }
        }
      }
    }
  }
}
