import { Component, OnInit, LOCALE_ID } from '@angular/core';

import { ConfigService } from '../../../config/config.service';
import { TermService } from '../../../shared/services/term/term.service';

import { Term } from '../../../shared/services/term/term';

@Component({
  selector: 'admin-term',
  templateUrl: './term.component.html',
  providers: [
    {provide: LOCALE_ID, useValue: 'es'},
    TermService,
  ],
})
export class ADMTermComponent implements OnInit {

  es: any;
  wata: number;
  registryTerm: boolean;
  loading: boolean;
  processing: boolean;

  term: Term = new Term();
  termList: Term[] = [];

  constructor(private config: ConfigService, private termService: TermService) {
  }

  ngOnInit(): void {
    this.es = this.config.getCalendarEs();
    this.wata = (new Date().getFullYear()) + 1;
    this.registryTerm = false;
    this.loading = false;
    this.processing = false;
    this.getTermList();
  }

  /**
   * Show form for registry new term
   */
  registryNewTerm(): void {
    this.registryTerm = true;
    this.term = new Term();
  }

  /**
   * Close form update term
   * @param {Term} term
   */
  cancel(term: Term): void {
    (term as any).edit = false;
    this.processing = true;
    this.getTermList();
  }

  /**
   * Close form for registry new term
   */
  close(): void {
    this.registryTerm = false;
    this.processing = true;
    this.getTermList();
  }

  /**
   * Get term list
   */
  getTermList(): void {
    this.loading = true;
    this.termService.getTermList()
      .subscribe(
        (term) => this.termList = term,
        (error) => {
          this.loading = false;
          this.processing = false;
          this.registryTerm = false;
          console.log('error => ', error);
        },
        () => {
          this.loading = false;
          this.processing = false;
          this.registryTerm = false;
          for (const term of this.termList) {
            (term as any).edit = false;
            (term as any).active = false;
            term.start_date = new Date(term.start_date as any);
            term.end_date = new Date(term.end_date as any);
          }
          const actives = this.termList.filter((f) => f.start_date <= new Date() && f.end_date >= new Date());
          for (const term of this.termList) {
            if (actives.length > 0) {
              (term as any).active = actives.filter((f) => f.term === term.term).length > 0;
            }
          }
        },
      );
  }

  /**
   * Save new term
   * @param {Term} term
   */
  save(term: Term): void {
    this.processing = true;
    term.term_description = term.term;
    this.termService.save(term)
      .subscribe(
        (r) => console.log('guardado => ', r),
        (error) => {
          this.processing = false;
          this.registryTerm = false;
          console.log('error => ', error);
        },
        () => {
          (term as any).edit = false;
          this.getTermList();
        },
      );
  }

  /**
   * Update term
   * @param {Term} term Term to update
   */
  update(term: Term): void {
    this.processing = true;
    this.termService.update(term)
      .subscribe(
        (r) => console.log('actualizado => ', r),
        (error) => {
          this.processing = false;
          console.log('error => ', error);
        },
        () => {
          (term as any).edit = false;
          this.getTermList();
        }
      );
  }

  /**
   * Delete term
   * @param {Term} term Term to delete
   */
  delete(term: Term): void {
    const msg = `¿Estás seguro de eliminar el período "${term.term}"?`;
    if (window.confirm(msg)) {
      this.processing = true;
      this.termService.delete(term)
        .subscribe(
          (r) => console.log('eliminado => ', r),
          (error) => {
            this.processing = false;
            console.error('error => ', error);
          },
          () => this.getTermList()
        );
    }
  }

}
