import { Component, OnInit } from '@angular/core';

import { ConfigService } from '../../../config/config.service';
import { SettingsService } from '../../../shared/services/settings/settings.service';
import { SettingsTypeService } from '../../../shared/services/settings-type/settings-type.service';

import { Settings } from '../../../shared/services/settings/settings';
import { SettingsType } from '../../../shared/services/settings-type/settings-type';

@Component({
  selector: 'admin-settings',
  templateUrl: './settings.component.html',
  providers: [
    ConfigService,
    SettingsService,
    SettingsTypeService
  ]
})
export class ADMSettingsComponent implements OnInit {

  registryItem: boolean;
  updateItem: boolean;
  loading: boolean;
  processing: boolean;

  settings: Settings = new Settings();
  settingsList: Settings[] = [];
  settingsTypeList: SettingsType[] = [];
  settingsTypeId: number;

  constructor(private config: ConfigService,
              private settingsService: SettingsService,
              private settingsTypeService: SettingsTypeService) {
  }

  ngOnInit(): void {
    this.registryItem = false;
    this.updateItem = false;
    this.loading = false;
    this.processing = false;
    this.getSettingsTypeList();
    this.getSettingsList();
  }

  /**
   * Show form for registry settings
   */
  registrySettings(): void {
    this.registryItem = true;
    this.updateItem = false;
    this.settings = new Settings();
  }

  /**
   * Show form for update settings
   * @param {Settings} settings Settings to update
   */
  updateSettings(settings: Settings): void {
    this.registryItem = false;
    this.updateItem = true;
    (settings as any).edit = true;
    this.settings = settings;
    this.settingsTypeId = settings.settingsType.id;
  }

  /**
   * Close form for update settings
   * @param {Settings} settings
   */
  cancel(settings: Settings): void {
    (settings as any).edit = false;
    this.close();
  }

  /**
   * Close form for registry new settings
   */
  close(): void {
    this.registryItem = false;
    this.updateItem = false;
    this.processing = false;
    this.getSettingsList();
  }

  /**
   * Update boolean variable values
   * @param {boolean} value
   */
  updateFlags(value: boolean): void {
    this.loading = value;
    this.processing = value;
    this.registryItem = value;
    this.updateItem = value;
  }

  /**
   * Set settings type
   */
  setSettingsType(): void {
    this.settings.settingsType = this.settingsTypeList.filter((f) => f.id.toString() === this.settingsTypeId.toString())[0];
  }

  /**
   * Settings list
   */
  getSettingsList(): void {
    this.loading = true;
    this.settingsService.getSettingsList()
      .subscribe(
        (list) => this.settingsList = list,
        (error) => {
          this.updateFlags(false);
          console.log('error => ', error);
        },
        () => {
          this.updateFlags(false);
          for (const item of this.settingsList) {
            (item as any).edit = false;
          }
        }
      );
  }

  /**
   * Settings type list
   */
  getSettingsTypeList(): void {
    this.settingsTypeService.getSettingsTypeList()
      .subscribe(
        (list) => this.settingsTypeList = list,
        (error) => console.log('error => ', error),
        () => console.log('settingsTypeList => ', this.settingsTypeList)
      );
  }

  /**
   * Save new settings
   * @param {Settings} settings Settings to save
   */
  save(settings: Settings): void {
    this.processing = true;
    this.settingsService.save(settings)
      .subscribe(
        (s) => console.log('guardado => ', s),
        (error) => {
          this.processing = false;
          this.registryItem = false;
          this.updateItem = false;
          console.log('error => ', error);
        },
        () => {
          (settings as any).edit = false;
          this.getSettingsList();
        }
      );
  }

  /**
   * Update settings
   * @param {Settings} settings Settings to update
   */
  update(settings: Settings): void {
    console.log('actualizar => ', settings);
    this.processing = true;
    this.settingsService.update(settings)
      .subscribe(
        (s) => console.log('actualizado => ', s),
        (error) => {
          this.processing = false;
          console.log('error => ', error);
        },
        () => {
          (settings as any).edit = false;
          this.getSettingsList();
        }
      );
  }

  /**
   * Delete settings
   * @param {Settings} settings Settings to delete
   */
  delete(settings: Settings): void {
    const msg = `¿Estás seguro de eliminar "${settings.name}"?`;
    if (window.confirm(msg)) {
      this.processing = true;
      this.settingsService.delete(settings)
        .subscribe(
          (s) => console.log('eliminado => ', s),
          (error) => {
            this.processing = false;
            console.log('error => ', error);
          },
          () => this.getSettingsList()
        );
    }
  }

}
