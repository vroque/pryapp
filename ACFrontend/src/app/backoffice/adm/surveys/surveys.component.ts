import { Component, LOCALE_ID, OnInit } from '@angular/core';

import { ConfigService } from '../../../config/config.service';
import { SurveyService } from '../../../shared/services/survey/survey.service';

import { Survey } from '../../../shared/services/survey/survey';

@Component({
  selector: 'admin-surveys',
  templateUrl: './surveys.component.html',
  providers: [
    {provide: LOCALE_ID, useValue: 'es'},
    ConfigService,
    SurveyService
  ],
})
export class ADMSurveysComponent implements OnInit {

  es: any;
  wata: number;
  registrySurvey: boolean;
  updateSurvey: boolean;
  loading: boolean;
  processing: boolean;

  survey: Survey = new Survey();
  surveyList: Survey[] = [];

  constructor(private config: ConfigService, private surveyService: SurveyService) {
  }

  ngOnInit(): void {
    this.es = this.config.getCalendarEs();
    this.wata = (new Date().getFullYear()) + 1;
    this.registrySurvey = false;
    this.updateSurvey = false;
    this.loading = false;
    this.processing = false;
    this.getSurveyList();
  }

  /**
   * Show form for registry survey
   */
  registryNewSurvey(): void {
    this.registrySurvey = true;
    this.updateSurvey = false;
    this.survey = new Survey();
  }

  /**
   * Show form for update survey
   * @param {Survey} survey Survey to update
   */
  updateCurrentSurvey(survey: Survey): void {
    this.registrySurvey = false;
    this.updateSurvey = true;
    (survey as any).edit = true;
    this.survey = survey;
  }

  /**
   * Close form for update survey
   * @param {Survey} survey
   */
  cancel(survey: Survey): void {
    (survey as any).edit = false;
    this.close();
  }

  /**
   * Close form for registry new survey
   */
  close(): void {
    this.registrySurvey = false;
    this.updateSurvey = false;
    this.processing = true;
    this.getSurveyList();
  }

  /**
   * Update boolean variable values
   * @param {boolean} value
   */
  updateFlags(value: boolean): void {
    this.loading = value;
    this.processing = value;
    this.registrySurvey = value;
    this.updateSurvey = value;
  }

  /**
   * Get survey list
   */
  getSurveyList(): void {
    this.loading = true;
    this.surveyService.getSurveyList()
      .subscribe(
        (list) => this.surveyList = list,
        (error) => {
          this.updateFlags(false);
          console.log('error => ', error);
        },
        () => {
          this.updateFlags(false);
          for (const survey of this.surveyList) {
            (survey as any).edit = false;
            (survey as any).current = false;
            survey.begin_date = new Date(survey.begin_date as any);
            survey.end_date = new Date(survey.end_date as any);
          }
          const actives = this.surveyList.filter((f) => f.begin_date <= new Date() && f.end_date >= new Date());
          for (const survey of this.surveyList) {
            if (actives.length > 0) {
              (survey as any).current = actives.filter((f) => f.id === survey.id).length > 0;
            }
          }
        }
      );
  }

  /**
   * Save new survey
   * @param {Survey} survey Survey to save
   */
  save(survey: Survey): void {
    this.processing = true;
    this.surveyService.save(survey)
      .subscribe(
        (s) => console.log('guardado => ', s),
        (error) => {
          this.processing = false;
          this.registrySurvey = false;
          this.updateSurvey = false;
          console.log('error => ', error);
        },
        () => {
          (survey as any).edit = false;
          this.getSurveyList();
        }
      );
  }

  /**
   * Update survey
   * @param {Survey} survey Survey to update
   */
  update(survey: Survey): void {
    this.processing = true;
    this.surveyService.update(survey)
      .subscribe(
        (s) => console.log('actualizado => ', s),
        (error) => {
          this.processing = false;
          console.log('error => ', error);
        },
        () => {
          (survey as any).edit = false;
          this.getSurveyList();
        }
      );
  }

  /**
   * Delete survey
   * @param {Survey} survey Survey to delete
   */
  delete(survey: Survey): void {
    const msg = `¿Estás seguro de eliminar la encuesta "${survey.name}"?`;
    if (window.confirm(msg)) {
      this.processing = true;
      this.surveyService.delete(survey)
        .subscribe(
          (s) => console.log('eliminado => ', s),
          (error) => {
            this.processing = false;
            console.log('error => ', error);
          },
          () => this.getSurveyList()
        );
    }
  }

}
