import { Component, LOCALE_ID, OnInit } from '@angular/core';

import { ConfigService } from '../../../config/config.service';
import { TermsAndConditions } from '../../../shared/services/terms-and-conditions/terms-and-conditions';
import { TermsAndConditionsService } from '../../../shared/services/terms-and-conditions/terms-and-conditions.service';
import { TermsAndConditionsType } from '../../../shared/services/terms-and-conditions-type/terms-and-conditions-type';
import { TermsAndConditionsTypeService } from '../../../shared/services/terms-and-conditions-type/terms-and-conditions-type.service';

@Component({
  selector: 'admin-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  providers: [
    {provide: LOCALE_ID, useValue: 'es'},
    ConfigService,
    TermsAndConditionsService, TermsAndConditionsTypeService
  ],
})
export class ADMTermsAndConditionsComponent implements OnInit {

  es: any;
  wata: number;
  registryMode: boolean;
  updateMode: boolean;
  loading: boolean;
  processing: boolean;

  termsAndConditions: TermsAndConditions = new TermsAndConditions();
  termsAndConditionsList: TermsAndConditions[] = [];
  termsAndConditionsTypeList: TermsAndConditionsType[] = [];

  constructor(private config: ConfigService,
              private termsAndConditionsService: TermsAndConditionsService,
              private termsAndConditionsTypeService: TermsAndConditionsTypeService) {
  }

  ngOnInit(): void {
    this.es = this.config.getCalendarEs();
    this.wata = (new Date().getFullYear()) + 1;
    this.registryMode = false;
    this.updateMode = false;
    this.loading = false;
    this.processing = false;
    this.getTermsList();
    this.getTermsTypeList();
  }

  /**
   * Show form for registry terms and conditions
   */
  registryNewTerms(): void {
    this.registryMode = true;
    this.updateMode = false;
    this.termsAndConditions = new TermsAndConditions();
    (this.termsAndConditions as any).preview = false;
  }

  /**
   * Show form for update terms and conditions
   * @param {TermsAndConditions} termsAndConditions Terms and conditions to update
   */
  updateCurrentTerms(termsAndConditions: TermsAndConditions): void {
    this.registryMode = false;
    this.updateMode = true;
    (termsAndConditions as any).edit = true;
    this.termsAndConditions = termsAndConditions;
  }

  /**
   * Close form for registry new terms and conditions
   * @param {TermsAndConditions} termsAndConditions
   */
  cancel(termsAndConditions: TermsAndConditions): void {
    (termsAndConditions as any).edit = false;
    this.close();
  }

  /**
   * Close form for registry new terms and conditions
   */
  close(): void {
    this.registryMode = false;
    this.updateMode = false;
    this.processing = true;
    this.getTermsList();
    this.getTermsTypeList();
  }

  /**
   * Update boolean variable values
   * @param {boolean} value
   */
  updateFlags(value: boolean): void {
    this.loading = value;
    this.processing = value;
    this.registryMode = value;
    this.updateMode = value;
  }

  /**
   * Get Terms and conditions type list
   */
  getTermsTypeList(): void {
    this.termsAndConditionsTypeService.getTypeList()
      .subscribe(
        (list) => this.termsAndConditionsTypeList = list,
        (error) => console.log(error),
        () => console.log('type list => ', this.termsAndConditionsTypeList)
      );
  }

  /**
   * Get Terms and conditions list
   */
  getTermsList(): void {
    this.loading = true;
    this.termsAndConditionsService.getTermsList()
      .subscribe(
        (list) => this.termsAndConditionsList = list,
        (error) => {
          this.updateFlags(false);
          console.log('error => ', error);
        },
        () => {
          this.updateFlags(false);
          for (const term of this.termsAndConditionsList) {
            (term as any).edit = false;
            term.publicationDate = new Date(term.publicationDate as any);
          }
        }
      );
  }

  /**
   * Save new Terms and conditions
   * @param {TermsAndConditions} termsAndConditions Terms and conditions to save
   */
  save(termsAndConditions: TermsAndConditions): void {
    this.processing = true;
    if (termsAndConditions.isFile) {
      termsAndConditions.detail = null;
    } else {
      termsAndConditions.url = null;
    }
    this.termsAndConditionsService.save(termsAndConditions)
      .subscribe(
        (s) => console.log('guardado => ', s),
        (error) => {
          this.processing = false;
          this.registryMode = false;
          this.updateMode = false;
          console.log('error => ', error);
        },
        () => {
          (termsAndConditions as any).edit = false;
          this.getTermsList();
        }
      );
  }

  /**
   * Update Terms and conditions
   * @param {TermsAndConditions} termsAndConditions Terms and conditions to update
   */
  update(termsAndConditions: TermsAndConditions): void {
    this.processing = true;
    if (termsAndConditions.isFile) {
      termsAndConditions.detail = null;
    } else {
      termsAndConditions.url = null;
    }
    this.termsAndConditionsService.update(termsAndConditions)
      .subscribe(
        (s) => console.log('actualizado => ', s),
        (error) => {
          this.processing = false;
          console.log('error => ', error);
        },
        () => {
          (termsAndConditions as any).edit = false;
          this.getTermsList();
        }
      );
  }

  /**
   * Delete Terms and conditions
   * @param {TermsAndConditions} termsAndConditions Terms and conditions to delete
   */
  delete(termsAndConditions: TermsAndConditions): void {
    const msg = `¿Estás seguro de eliminiar "${termsAndConditions.name}"?`;
    if (window.confirm(msg)) {
      this.processing = true;
      this.termsAndConditionsService.delete(termsAndConditions)
        .subscribe(
          (s) => console.log('eliminado => ', s),
          (error) => {
            this.processing = false;
            console.log('error => ', error);
          },
          () => {
            this.getTermsList();
          }
        );
    }
  }

}
