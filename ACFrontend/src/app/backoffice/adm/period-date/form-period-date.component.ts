import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { ACTerm } from '../../../shared/services/atentioncenter/term';
import { Campus } from '../../../shared/services/institutional/campus';
import { ConfigService } from '../../../config/config.service';
import { ACTermService } from '../../../shared/services/atentioncenter/term.service';

@Component({
  selector: 'form-period-date',
  templateUrl: './form-period-date.component.html',
  providers: [ACTermService, ConfigService]
})
export class FormPeriodDateComponent implements OnChanges {

  @Output() dataOutput: EventEmitter<{ view: boolean, type: string, data: ACTerm, state: string }> = new EventEmitter();
  @Input() dataImput: { view: boolean, type: string, data: ACTerm, state: string };
  @Input() campuses: Campus[];
  @Input() substituteExam = false;
  backupPeriod: ACTerm = new ACTerm;
  es: any;
  change = false;

  constructor(private termService: ACTermService, private configService: ConfigService) { }

  ngOnChanges() {
    this.es = this.configService.getLocationCalendar();

    if (this.verifyChanges(this.backupPeriod, this.dataImput.data)) {
      this.change = false;
    }
    this.backupPeriod = JSON.parse(JSON.stringify(this.dataImput.data));
  }

  /**
   * Evento del formulario para los botones
   * @param view parametro para ver o no el componente
   * @param type tipo de formulario
   * @param data data que se esta usando
   * @param state estado del formulario
   */
  eventForm(newData: { view: boolean, type: string, data: ACTerm, state: string}): void {
    if (newData.state === 'canceled') {
       newData.data = this.backupPeriod;
    }
    this.dataOutput.emit(newData);
  }

  /**
   * Función para determinar que tipo de acción se realizara con los datos de formulario
   */
  submitForm(): void {
    if (this.dataImput.type === 'edit') {
      this.update();
    } else {
      this.create();
    }
  }

  /**
   * Creación de registro
   */
  create(): void {
    this.termService.create(this.dataImput.data)
      .subscribe(
        (response) => {
          this.dataImput.data = this.formatFech(response);
          this.dataOutput.emit({view: false, type: 'new', data: response, state: 'success'});
        },
        (error) => this.dataImput.state = error
      );
  }

  /**
   * Actualización de registro
   */
  update(): void {
    this.termService.update(this.dataImput.data)
      .subscribe(
        (response) => {
          this.dataOutput.emit({ view: false, type: 'edit', data: response, state: 'success' });
        },
        (error) => this.dataOutput.emit({ view: true, type: 'edit', data: this.dataImput.data, state: 'error' })
      );
  }

  /**
   * Función para verificar si el dato obtenido es diferente al que se tiene
   * @param backup data de respaldo
   * @param original data recivida desde el padre
   */
  verifyChanges(backup: ACTerm, original: ACTerm): boolean {
    if (backup !== original) {
      return true;
    }
    return false;
  }

  verifyChangue(item: string) {
    if (this.backupPeriod[item] === this.dataImput.data[item]) {
      this.change = false;
    } else {
      this.change = true;
    }
  }

  /**
   * Función para formatear las fechas
   * @param data data a formatear
   */
  formatFech(data: ACTerm): ACTerm {
    data.start_date = new Date(data.start_date as any);
    data.end_date = new Date(data.end_date as any);
    data.start_classes_date = new Date(data.start_classes_date as any);
    data.end_classes_date = new Date(data.end_classes_date as any);
    data.start_enroll_date = new Date(data.start_enroll_date as any);
    data.end_enroll_date = new Date(data.end_enroll_date as any);
    if (data.start_substitute_exam_date !== null) {
      data.start_substitute_exam_date = new Date(data.start_substitute_exam_date as any);
    }
    if (data.end_substitute_exam_date !== null) {
      data.end_substitute_exam_date = new Date(data.end_substitute_exam_date as any);
    }

    return data;
  }
}
