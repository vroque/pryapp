import { Component, OnInit, Input } from '@angular/core';

import { ACTerm } from '../../../shared/services/atentioncenter/term';
import { ACTermService } from '../../../shared/services/atentioncenter/term.service';
import { BackofficeConfigService } from '../../config/config.service';
import { Campus } from '../../../shared/services/institutional/campus';
import { CampusService } from '../../../shared/services/institutional/campus.service';

@Component({
  providers: [ACTermService, BackofficeConfigService, CampusService],
  selector: 'period-date',
  templateUrl: './period-date.component.html'
})

export class PeriodDateComponent implements OnInit {

  campuses: Campus[] = [];
  errorMessage: string;
  event = 'none';
  campusSelected = '';
  departmentSelected = '';
  new_term: any = {};
  formPeriodDate: { view: boolean, type: string, data: ACTerm, state: string };
  terms: ACTerm[] = [];
  loading = true;
  periods: string[] = [];
  periodSelected = '';
  position: number;
  @Input() substituteExam = false;

  constructor(
    private config: BackofficeConfigService,
    private termService: ACTermService,
    private campusService: CampusService,
  ) { }

  ngOnInit(): void {
    this.formPeriodDate = { view: false, type: 'new', data: new ACTerm, state: ''};
    this.getCampus();
    this.position = -1;
  }

  /**
   * Función para mostrar el formulario y su tipo
   * @param date datos para el formulario
   */
  changeViewFormDate(newData: { view: boolean, type: string, data: ACTerm, state: string}, position: number): void {
    this.position = position;
    if (newData.type !== 'new') {
      newData.data = this.formatFech(newData.data);
    }
    if (newData.type === 'new') {
      newData.data = new ACTerm;
    }

    this.formPeriodDate = newData;
    const element = document.getElementById('top-component');
    element.scrollIntoView({ behavior: 'smooth' });
  }

  /**
   * Función para actualizar data del origen
   * @param newData data devuelta por el componente hijo
   */
  updateDataReturn(newData: { view: boolean, type: string, data: ACTerm, state: string }): void {
    if (newData.state === 'error') {
      console.log('Error de ' + newData.type);
    }
    if (newData.state === 'canceled' && newData.type === 'edit') {
      this.terms[this.position] = newData.data;
      this.position = -1;
    } else if (newData.type === 'new' && newData.state === 'success') {
      this.terms.unshift(newData.data);
    } else if (newData.type === 'edit' && newData.state === 'success') {
      this.position = -1;
    }
    this.formPeriodDate = newData;
  }

  /**
   * Obtenemos los campus
   */
  getCampus(): void {
    this.campusService.query().subscribe(
      (data) => {
        this.campuses = data;
        this.getTerms();
      },
      (error) => this.errorMessage = error
    );
  }

  /**
   * Obtenemos los períodos
   */
  getTerms(): void {
    this.termService.query().subscribe(
      (terms) => {
        this.terms = terms;
        if (terms !== null && terms.length > 0) {
          this.formatingTerms(terms);
          this.getPeriods(terms);
        } else {
          this.errorMessage = 'Lo sentimos no podemos obtener los registros';
        }
      },
      (error) => this.errorMessage = error,
      () => this.loading = false
    );
  }

  formatingTerms(data: ACTerm[]): void {
    for (let term of data) {
      (term as any).edit = false;
      term = this.formatFech(term);
    }
  }

  /**
   * Obtenemos los períodos de los registros obtenidos
   * @param data registros
   */
  getPeriods(data: ACTerm[]): void {
    this.periods.push(data[0].term);
    for (const item of data) {
      if (this.periods.indexOf(item.term) === -1) {
        this.periods.push(item.term);
      }
    }
  }

  /**
   * Devolvemos los nombres de los campus
   * @param id identificador del campus
   */
  getCampusName(id: string): string {
    const campus: Campus[] = this.campuses.filter(
      (item) => item.id === id);
      if (campus !== null ) {
        return campus[0].name;
      }
      return '--';
  }

  /**
   * Filtrado para los registros de los períodos
   * @param department
   * @param campus
   * @param term período
   */
  termFilter(department: string, campus: string, term: string): boolean {
    if (this.departmentSelected) {
      if (this.departmentSelected !== department) {
        return false;
      }
    }
    if (this.campusSelected) {
      if (this.campusSelected !== campus) {
        return false;
      }
    }
    if (this.periodSelected) {
      if (this.periodSelected !== term) {
        return false;
      }
    }
    return true;
  }

  formatFech(data: ACTerm): ACTerm {
    data.start_date = new Date(data.start_date as any);
    data.end_date = new Date(data.end_date as any);
    data.start_classes_date = new Date(data.start_classes_date as any);
    data.end_classes_date = new Date(data.end_classes_date as any);
    data.start_enroll_date = new Date(data.start_enroll_date as any);
    data.end_enroll_date = new Date(data.end_enroll_date as any);
    if (data.start_substitute_exam_date !== null) {
      data.start_substitute_exam_date = new Date(data.start_substitute_exam_date as any);
    }
    if (data.end_substitute_exam_date !== null) {
      data.end_substitute_exam_date = new Date(data.end_substitute_exam_date as any);
    }

    return data;
  }
}
