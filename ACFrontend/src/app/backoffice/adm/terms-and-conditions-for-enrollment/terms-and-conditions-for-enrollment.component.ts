import { Component, LOCALE_ID, OnInit } from '@angular/core';

import { ConfigService } from '../../../config/config.service';
import { SettingsDetail } from '../../../shared/services/settings-detail/settings-detail';
import { SettingsDetailService } from '../../../shared/services/settings-detail/settings-detail.service';
import { TermsAndConditions } from '../../../shared/services/terms-and-conditions/terms-and-conditions';
import { TermsAndConditionsService } from '../../../shared/services/terms-and-conditions/terms-and-conditions.service';

@Component({
  selector: 'admin-terms-and-conditions-for-enrollment',
  templateUrl: './terms-and-conditions-for-enrollment.component.html',
  providers: [
    {provide: LOCALE_ID, useValue: 'es'},
    ConfigService, TermsAndConditionsService, SettingsDetailService
  ]
})
export class ADMTermsAndConditionsForEnrollmentComponent implements OnInit {
  registerMode = false;
  updateMode = false;
  loading = false;
  processing = false;

  settingsDetail: SettingsDetail = new SettingsDetail();
  termsAndConditionsList: TermsAndConditions[] = [];
  termsAndConditions: TermsAndConditions;

  constructor(private config: ConfigService,
              private settingDetailService: SettingsDetailService,
              private termsAndConditionsService: TermsAndConditionsService) {
  }

  ngOnInit(): void {
    this.getModuleTerms();
    this.getSettingDetailForTermsOfEnrollment();
  }

  /**
   * Terms and conditions for modules
   */
  getModuleTerms(): void {
    this.termsAndConditionsService.getModuleTerms()
      .subscribe(
        (res) => this.termsAndConditionsList = res,
        (error) => console.log(error),
        () => console.log('module terms => ', this.termsAndConditionsList)
      );
  }

  /**
   * Setting detail for terms for enrollment
   */
  getSettingDetailForTermsOfEnrollment(): void {
    this.loading = true;
    this.settingDetailService.getTermsForEnrollment()
      .subscribe(
        (res) => this.settingsDetail = res,
        (error) => {
          console.log(error);
          this.loading = false;
        },
        () => {
          console.log('setting detail => ', this.settingsDetail);
          this.loading = false;
          if (this.settingsDetail !== null) {
            this.getTermsForEnrollment(this.settingsDetail.numberValue);
          }
        }
      );
  }

  /**
   * Terms and conditions for enrollment
   * @param id
   */
  getTermsForEnrollment(id: number): void {
    this.termsAndConditionsService.getTermsById(id)
      .subscribe(
        (res) => this.termsAndConditions = res,
        (error) => console.log(error),
        () => console.log('terms for enrollment => ', this.termsAndConditions)
      );
  }

  /**
   * Save new settings detail for terms for enrollment
   * @param settingsDetail Setting detail for terms for enrollment
   */
  save(settingsDetail: SettingsDetail): void {
    this.processing = true;
    settingsDetail.order = null;
    settingsDetail.type = 'number';
    settingsDetail.strValue = null;
    settingsDetail.dateValue = null;
    settingsDetail.settingsId = 3;
    this.settingDetailService.save(settingsDetail)
      .subscribe(
        (res) => this.settingsDetail = res,
        (error) => {
          console.log(error);
          this.processing = false;
          this.registerMode = false
        },
        () => {
          console.log('created => ', this.settingsDetail);
          this.processing = false;
          this.registerMode = false;
          this.getSettingDetailForTermsOfEnrollment();
        }
      );
  }

  /**
   * Update settings detail for terms for enrollment
   * @param settingsDetail Setting detail for terms for enrollment
   */
  update(settingsDetail: SettingsDetail): void {
    this.processing = true;
    this.settingDetailService.update(settingsDetail)
      .subscribe(
        (res) => this.settingsDetail = res,
        (error) => {
          this.processing = false;
          this.updateMode = false;
          console.log(error);
        },
        () => {
          this.processing = false;
          this.updateMode = false;
          console.log('updated => ', this.settingsDetail);
          this.getSettingDetailForTermsOfEnrollment();
        }
      );
  }

  cancel(): void {
    console.log('cancel');
    this.registerMode = false;
    this.updateMode = false;
  }

  registryTerms(): void {
    this.registerMode = true;
    this.settingsDetail = new SettingsDetail();
  }

  updateTerms(): void {
    this.updateMode = true;
  }
}
