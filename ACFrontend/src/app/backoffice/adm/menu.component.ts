import { BackofficeConfigService } from '../config/config.service';
import { Component, OnInit } from '@angular/core';
import { Menu } from '../../shared/services/aplication/menu';
import { MenuService } from '../../shared/services/aplication/menu.service';
import { Module } from '../../shared/services/aplication/module';
import { ModuleService } from '../../shared/services/aplication/module.service';

@Component({
  providers: [
    BackofficeConfigService,
    MenuService,
    ModuleService
  ],
  selector: 'admin-permission',
  templateUrl: './menu.component.html'
})
export class ADMMenuComponent implements OnInit {

  error_message: string;
  modules: Module[];
  modules_bo: Module[];
  modules_fd: Module[];
  modules_pt: Module[];
  module: Module;
  menus: Menu[];
  menu: Menu;
  newMenu: Menu;
  selectedModuleMenu: Module[] = [];
  activeModuleMenu = '';
  results: Module[];
  resultSelected: Module;
  filterInput: any;
  typeForm: string;
  moduleNivel: number;
  orderMax: number;
  changeFlowCol = [false, false];

  constructor(private config: BackofficeConfigService, private moduleService: ModuleService, private menuService: MenuService) {}

  ngOnInit (): void {
    this.moduleService.list()
      .subscribe(
        (modules) => this.modules = modules,
        (err) => this.error_message = err,
        () => {
          this.modules_bo = this.modules.filter(f => f.name.startsWith('bo_'));
          // Se comenta ya que no tiene modulos anonimos
          // this.modules_bo.push(this.modules.filter(f => f.name.startsWith('ANONYMOUS'))[0]); // Agregar modulo anonimo
          this.modules_fd = this.modules.filter(f => f.name.startsWith('fd_'));
          this.modules_pt = this.modules.filter(f => f.name.startsWith('pt_'));
        }
      );
  }

  /**
   * Selecciona el modulo a editar
   * @param id number id del modulo seleccionado
   */
  selectModule(menuSelected: Module): void {
    this.menu = null;
    this.typeForm = '';
    this.resultSelected = menuSelected;
    this.module = this.modules.filter((module) => module.id === menuSelected.id)[0] || null;
    this.getMenus(menuSelected.id);
  }

  /**
   * Obtener las opciones o menus dentro de un módulo
   * @param moduleMenu menú del módulo a cambiar
   */
  choiseModuleMenu(moduleMenu: Module[], activeModule: string): void {
    this.menus = null;
    this.resultSelected = undefined;
    this.filterInput = '';
    this.typeForm = '';
    if (activeModule === this.activeModuleMenu) {
      this.selectedModuleMenu = [];
      this.activeModuleMenu = '';
    } else {
      this.selectedModuleMenu = moduleMenu;
      this.activeModuleMenu = activeModule;
    }
  }

  /**
   * Selecciona el menu a editar
   * @param orderMax number maximo ordenamiento
   * @param nivel number del nivel 1, 2 o 3
   * @param id number id del menu a editar
   * @param menu_id number (opcional) id del menu de primer nivel (padre/abuelo)
   * @param smenu_id number (opcional) id del menu de segundo nivel (padre)
   */
  showEditMenu(orderMax: number, nivel: number, id: number, menuId?: number, smenuId?: number): void {
    this.moduleNivel = nivel;
    this.typeForm = 'edit';
    this.orderMax = orderMax;
    const element = document.getElementById('create-edit-menu');
    element.scrollIntoView({ behavior: 'smooth' });
    if (menuId) {
      let menu: Menu = this.menu = this.menus.filter( (module) => module.id === menuId)[0] || null;
      if (smenuId) {
        menu = menu.smenus.filter( module => module.id === smenuId)[0] || null;
      }
      this.menu = menu.smenus.filter( module => module.id === id)[0] || null;
    } else {
      this.menu = this.menus.filter( module => module.id === id)[0] || null;
    }
  }

  /**
   * Obtiene desde el servidor los meus de un determinado modulo
   * @param id number Identificador del modulo del cual se quiere ver los menus
   */
  getMenus (id: number): void {
    this.menuService.query(id)
      .subscribe(
        (menus) => this.menus = menus,
        (err) =>  this.error_message = err
      );
  }

  /**
   * ACtualiza un menu
   */
  saveMenu(event: any): void {
    // console.log(event);
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    this.menuService.update(this.menu)
      .subscribe(
        (menu) => {
          this.menu = menu;
          this.getMenus(this.resultSelected.id);
          this.typeForm = '';
        },
        (err) =>  this.error_message = err,
        () => {
          this.menu = null;
          target.disabled = false;
        }
      );
  }

  /**
   * Crea un nuevo menu
   */
  createMenu(event: any): void {
    const target: any = event.target || event.srcElement || event.currentTarget;
    target.disabled = true;
    let parent: number = null;
    if ((<any>this.newMenu).parent_obj !== null) {
      parent = (<any>this.newMenu).parent_obj.id;
    }
    const menu: Menu = {
      id: null,
      icon: this.newMenu.icon,
      uri: this.newMenu.uri,
      name: this.newMenu.name,
      parent: parent,
      module: this.module.id,
      is_active: this.newMenu.is_active,
      position: this.newMenu.position,
      smenus: null,
      cardStyle: this.newMenu.cardStyle,
      cardIcoStyle: this.newMenu.cardIcoStyle,
      order: this.newMenu.order,
      description: this.newMenu.description,
      externalLink: this.newMenu.externalLink
    };

    this.menuService.create(menu)
      .subscribe(
        (response) => {
          this.typeForm = '';
          this.newMenu = response;
          this.getMenus(this.resultSelected.id);
        },
        (err) =>  this.error_message = err,
        () => {
          this.newMenu = null;
          this.getMenus(this.module.id);
          target.disabled = false;
        }
      );
  }

  /**
   * Creación de un nuevo menú primiario, secundario o terciario
   * @param menu Menu con el cual se creara, esto para saber si es primario, secundario o terciario
   * @param order Enviamos el orden final que le corresponde
   * @param nivel Establece si es un módulo principal o uno secundario o terciario
   * @param styleCardIcoStyle estilo heredado del padre para el color del modulo
   */
  showCreateMenu(menu: Menu, order: number, nivel: number, styleCardIcoStyle: string): void {
    this.typeForm = 'create';
    this.moduleNivel = nivel;
    this.newMenu = menu;
    this.newMenu.cardIcoStyle = styleCardIcoStyle;
    this.newMenu.description = null;
    this.newMenu.icon = 'fa fa-question';
    this.newMenu.name = null;
    this.newMenu.order = order;
    this.newMenu.uri = null;
    this.orderMax = order;
    const element = document.getElementById('create-edit-menu');
    element.scrollIntoView({ behavior: 'smooth' });
  }

  /**
   * Cambia la visualización del flujo de modulos y sus
   * contenidos
   */
  changeFlow(): void {
    this.changeFlowCol[0] = !this.changeFlowCol[0];
    this.changeFlowCol[1] = !this.changeFlowCol[1];
  }

}
