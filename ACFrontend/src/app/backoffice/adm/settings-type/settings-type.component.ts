import { Component, OnInit } from '@angular/core';

import { ConfigService } from '../../../config/config.service';
import { SettingsTypeService } from '../../../shared/services/settings-type/settings-type.service';

import { SettingsType } from '../../../shared/services/settings-type/settings-type';

@Component({
  selector: 'admin-settings-type',
  templateUrl: './settings-type.component.html',
  providers: [
    ConfigService,
    SettingsTypeService
  ]
})
export class ADMSettingsTypeComponent implements OnInit {

  registryItem: boolean;
  updateItem: boolean;
  loading: boolean;
  processing: boolean;

  settingsType: SettingsType = new SettingsType();
  settingsTypeList: SettingsType[] = [];

  constructor(private config: ConfigService, private settingsTypeService: SettingsTypeService) {
  }

  ngOnInit(): void {
    this.registryItem = false;
    this.updateItem = false;
    this.loading = false;
    this.processing = false;
    this.getSettingsTypeList();
  }

  /**
   * Show form for registry settings type
   */
  registrySettingsType(): void {
    this.registryItem = true;
    this.updateItem = false;
    this.settingsType = new SettingsType();
  }

  /**
   * Show form for update settings type
   * @param {SettingsType} settingsType Settings type to update
   */
  updateSettingsType(settingsType: SettingsType): void {
    this.registryItem = false;
    this.updateItem = true;
    (settingsType as any).edit = true;
    this.settingsType = settingsType;
  }

  /**
   * Close form for update settings type
   * @param {SettingsType} settingsType
   */
  cancel(settingsType: SettingsType): void {
    (settingsType as any).edit = false;
    this.close();
  }

  /**
   * Close form for registry new settings type
   */
  close(): void {
    this.registryItem = false;
    this.updateItem = false;
    this.processing = false;
  }

  /**
   * Update boolean variable values
   * @param {boolean} value
   */
  updateFlags(value: boolean): void {
    this.loading = value;
    this.processing = value;
    this.registryItem = value;
    this.updateItem = value;
  }

  /**
   * Settings type list
   */
  getSettingsTypeList(): void {
    this.loading = true;
    this.settingsTypeService.getSettingsTypeList()
      .subscribe(
        (list) => this.settingsTypeList = list,
        (error) => {
          this.updateFlags(false);
          console.log('error => ', error);
        },
        () => {
          this.updateFlags(false);
          for (const item of this.settingsTypeList) {
            (item as any).edit = false;
          }
        }
      );
  }

  /**
   * Save new settings type
   * @param {SettingsType} settingsType Settings type to save
   */
  save(settingsType: SettingsType): void {
    this.processing = true;
    this.settingsTypeService.save(settingsType)
      .subscribe(
        (s) => console.log('guardado => ', s),
        (error) => {
          this.processing = false;
          this.registryItem = false;
          this.updateItem = false;
          console.log('error => ', error);
        },
        () => {
          (settingsType as any).edit = false;
          this.getSettingsTypeList();
        }
      );
  }

  /**
   * Update settings type
   * @param {SettingsType} settingsType Settings type to update
   */
  update(settingsType: SettingsType): void {
    this.processing = true;
    this.settingsTypeService.update(settingsType)
      .subscribe(
        (s) => console.log('actualizado => ', s),
        (error) => {
          this.processing = false;
          console.log('error => ', error);
        },
        () => {
          (settingsType as any).edit = false;
          this.getSettingsTypeList();
        }
      );
  }

  /**
   * Delete settings type
   * @param {SettingsType} settingsType Settings type to delete
   */
  delete(settingsType: SettingsType): void {
    const msg = `¿Estás seguro de eliminar "${settingsType.name}"?`;
    if (window.confirm(msg)) {
      this.processing = true;
      this.settingsTypeService.delete(settingsType)
        .subscribe(
          (s) => console.log('eliminado => ', s),
          (error) => {
            this.processing = false;
            console.log('error => ', error);
          },
          () => this.getSettingsTypeList()
        );
    }
  }

}
