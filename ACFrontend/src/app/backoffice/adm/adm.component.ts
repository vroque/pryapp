import { Component } from '@angular/core';

import { MenuService } from '../../shared/services/aplication/menu.service';
import { ModuleService } from '../../shared/services/aplication/module.service';
import { BackofficeConfigService } from '../config/config.service';

/**
 * @module Administración (adm)
 * @name AdminComponent
 * Componente para el proceso de categorizacion en bienestar universitario
 */
@Component({
  selector: 'admin-permission',
  template: `<p>Admninistración de la plataforma`,
  // tslint:disable-next-line:object-literal-sort-keys
  providers: [
    BackofficeConfigService,
    MenuService,
    ModuleService,
  ],
})
export class ADMComponent {

}
