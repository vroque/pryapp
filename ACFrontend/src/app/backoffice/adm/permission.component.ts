import { Component, OnInit } from '@angular/core';

import { BackofficeConfigService } from '../config/config.service';
import { CampusService } from '../../shared/services/institutional/campus.service';
import { GroupMolduleService } from '../../shared/services/aplication/groupModule.service';
import { GroupsService } from '../../shared/services/aplication/group.service';
import { GroupsUserService } from '../../shared/services/aplication/groupUser.service';
import { ModuleService } from '../../shared/services/aplication/module.service';
import { UsersService } from '../../shared/services/aplication/user.service';

import { Campus } from '../../shared/services/institutional/campus';
import { Groups } from '../../shared/services/aplication/group';
import { Module } from '../../shared/services/aplication/module';
import { Users } from '../../shared/services/aplication/user';

/**
 * @module ADM
 * @name AdminPermissionComponent
 * Componente para el proceso de categorizacion en bienestar universitario
 */

@Component({
  providers: [
    BackofficeConfigService,
    CampusService,
    GroupsService,
    GroupsUserService,
    GroupMolduleService,
    ModuleService,
    UsersService,
  ],
  selector: 'adm-permission',
  templateUrl: './permission.component.html'
})
export class ADMPermissionComponent implements OnInit {

  error_message: string;
  campuses: Campus[] = [];
  modules: Module[] = [];
  groups: Groups[] = [];
  groups_all: Groups[] = [];
  users: Users[] = [];
  campus_active = '';
  campus_actadd_moduleive = '';
  module_active = 0 ;
  group_active = 0 ;
  user_active = 0 ;
  add_module = false;
  add_group = false;
  add_user = false;
  module: any[] = [];
  group: any[] = [];
  user: any[] = [];
  user_find: string = null;
  users_found: Users[] = [];

  constructor(
    private campusService: CampusService,
    private moduleService: ModuleService,
    private groupService: GroupsService,
    private groupUserService: GroupsUserService,
    private usersService: UsersService,
    private groupMolduleService: GroupMolduleService,
    private config: BackofficeConfigService
  ) {}

  ngOnInit(): void {
    this.campusService.query().subscribe(
      (campuses) => this.campuses = campuses,
      (error) => this.error_message = error,
    );
    this.groupService.query_all().subscribe(
      (user) => this.groups_all = user,
      (error) => this.error_message = error,
    );
    this.campusActive(0);
  }

  campusActive(id): void {
    this.campus_active = id;
    this.modules = null;
    this.groups = null;
    this.users = null;
    this.module_active = null;
    this.group_active = null;
    this.user_active = null;
    this.moduleService.query(id).subscribe(
      (modules) => this.modules = modules,
      (error) => this.error_message = error,
    );
  }

  moduleActive(id): void {
    this.module_active = id;
    this.groups = null;
    this.users = null;
    this.group_active = null;
    this.user_active = null;
    this.groupService.query(
      {campus_id: this.campus_active, module_id: id},
    ).subscribe(
      (group) => this.groups = group,
      (error) => this.error_message = error,
    );
  }

  groupActive(id): void {
    this.group_active = id;
    this.user_active = null;
    this.usersService.query(id).subscribe(
      (user) => this.users = user,
      (error) => this.error_message = error,
    );
  }

  addModuleOn(): void {this.add_module = true; }
  addModuleOff(): void {this.add_module = false; }
  addGroupOn(): void {this.add_group = true; }
  addGroupOff(): void {this.add_group = false; }
  addUserOn(): void {this.add_user = true; }
  addUserOff(): void {this.add_user = false; }
  // getCampus(id): void { };

  addModule(form: any): void {
    this.moduleService.add(this.module).subscribe(
      (module) => this.module = module,
      (error) => this.error_message = error,
      () => {
        form.reset();
        this.campusActive(1);
      },
    );
  }

  findUser(): void {
    this.usersService.find(this.user_find)
      .subscribe(
        (users) => this.users_found = users,
        (error) => this.error_message = error,
      );
  }

  addUser(id): void {
    this.groupUserService.add({'group_id': this.group_active, 'user_id': id})
      .subscribe(
        () => {
        },
        (error) => this.error_message = error,
        () => {
          this.addUserOff();
          this.groupActive(this.group_active);
        },
      );
  }

  removeUser(id): void {
    this.groupUserService.remove({'group_id': this.group_active, 'user_id': id})
      .subscribe(
        () => {
        },
        (error) => this.error_message = error,
      );
    this.groupActive(this.group_active);
  }

  addGroup(form): void {
    // this.group["name"];
    this.groupMolduleService.add(
      {
        'group_name': this.group['name'],
        'campus_id' : this.campus_active,
        'module_id': this.module_active,
      })
      .subscribe(
        () => {
        },
        (error) => this.error_message = error,
        () => {
          form.reset();
          this.moduleActive(this.module_active);
        },
      );
  }

  addGroupToModule(id): void {
    this.groupMolduleService.addGroup({
      'group_id' : id,
      'campus_id' : this.campus_active,
      'module_id' : this.module_active
    })
      .subscribe(
        () => {
        },
        (error) => this.error_message = error,
        () => {
          this.moduleActive(this.module_active);
        },
      );
  }

  removeGroupToModule(id): void {
    this.groupMolduleService.removeGroupModule({
      'groupID' : id,
      'campus' : this.campus_active,
      'moduleID' : this.module_active
    })
      .subscribe(
        () => console.log(''),
        (error) => this.error_message = error,
        () => {
          this.group_active = null;
          this.moduleActive(this.module_active);
        },
      );
  }
}
