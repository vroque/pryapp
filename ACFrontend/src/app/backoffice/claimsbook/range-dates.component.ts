import { Component, OnInit, ViewChild, Input, Output, EventEmitter  } from "@angular/core";
import { ActivatedRoute, Params }   from "@angular/router";

declare var $;
import * as moment from "moment";
import "moment/locale/es";

@Component({
  selector: "ldr-range-dates",
  templateUrl: "./range-dates.component.html",
})

export class LDRRangeDates  implements OnInit {
  es: any;
  wata: number;

  @Output()setDates = new EventEmitter();
  // @Output()date_start = new EventEmitter();
  // @Output()date_end = new EventEmitter();

  constructor(){}
  ngOnInit(): void{
    this.es = {
            firstDayOfWeek: 1,
            dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
            dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
            dayNamesMin: [ "D","L","M","X","J","V","S" ],
            monthNames: [ "enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre" ],
            monthNamesShort: [ "ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic" ],
            today: 'Hoy',
            clear: 'Borrar'
        }
      this.wata = (new Date()).getFullYear();
      this.initRangeDate();


  }



  initRangeDate(){
    var start = moment().subtract(29, 'days');
    var end = moment();
    //var self = this;
    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        autoApply: true,
        ranges: {
           'Hoy': [moment(), moment()],
           'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Ultimos 7 días': [moment().subtract(6, 'days'), moment()],
           'Ultimos 30 días': [moment().subtract(29, 'days'), moment()],
           'Este mes': [moment().startOf('month'), moment().endOf('month')],
           'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },

       locale: {
           format: "DD/MM/YYYY",
           separator: " - ",
           applyLabel: "Aplicar",
           cancelLabel: "Cancelar",
           fromLabel: "From",
           toLabel: "To",
           customRangeLabel: "Rango Personalizado",
           daysOfWeek: [
               "Dom",
               "Lun",
               "Mar",
               "Mier",
               "Jue",
               "Vie",
               "Sab"
           ],
           monthNames: [
               "Enero",
               "Febrero",
               "Marzo",
               "Abril",
               "Mayo",
               "Junio",
               "Julio",
               "Agosto",
               "Septiembre",
               "Obtubre",
               "Noviembre",
               "Deciembre"
           ],
       },
    }, (d_start: Date, d_end: Date) => {
      /*let dates: Dates = <Dates>{};
      dates.date_start = d_start;
      dates.date_end = d_end;*/
      //console.log(dates);
      let dates =  {
        date_start: d_start,
        date_end: d_end
      }
      this.setDates.emit(dates);
    });
  }

  returnDates(d_start: Date, d_end: Date){

  }

}

/*class Dates{
  constructor(
    public date_start: Date,
    public date_end: Date,
  ){}
}
*/
