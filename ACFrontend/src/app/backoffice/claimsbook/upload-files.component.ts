import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { LDRFilePageClaimsService } from '../../shared/services/claimsbook/file-page-claims.service';

@Component({
  providers: [LDRFilePageClaimsService],
  selector: 'ldr-upload-files',
  templateUrl: './upload-files.component.html'
})

export class LDRUploadFiles  implements OnInit {

  @Input()required: boolean;
  @Input()isValid: boolean;
  @Output()isValidChange = new EventEmitter();
  @Output()loadFile = new EventEmitter();

  uploadedFiles: Array<any> = [];

  constructor(
    public filePageClaimsService: LDRFilePageClaimsService
  ) {}

  ngOnInit() {
    this.changeValidation();
  }

  fileCharge(event) {
    for (const file of event.files) {
        const name = file.name;
        const size = file.size;
        if (this.uploadedFiles.length > 0) {
          let add = true;
          for (const fileupload of this.uploadedFiles) {
              if (name === fileupload.name && size === fileupload.size) {
                add = false;
              }
          }
          if (add) {
            this.uploadedFiles.push(file);
          }
        } else {
          this.uploadedFiles.push(file);
        }
    }
    this.changeValidation();
  }

  fileRemove(event) {
    const name = event.file.name;
    const size = event.file.size;
    for (let i = 0; i < this.uploadedFiles.length; i++) {
      if (this.uploadedFiles[i].name === name && this.uploadedFiles[i].size === size) {
          this.uploadedFiles.splice(i, 1);
      }
    }
    this.changeValidation();
  }
  changeValidation () {
    if (this.required && this.uploadedFiles.length === 0) {
      this.isValid = false;
    } else {
      this.isValid = true;
    }
    this.isValidChange.emit(this.isValid);
  }

  changeLoad(state: boolean){
    this.loadFile.emit(state);
  }

  saveFile(pageId: string, derivateId: string, answerId: string, state: string, callback?: any) {
    if (this.isValid && this.uploadedFiles.length > 0) {
      let val = 0;
      for (const data of this.uploadedFiles) {
        this.filePageClaimsService.save(data, pageId, derivateId, answerId, state)
          .subscribe(
            (response) => data,
            err => console.log(err),
            () => {
                val++;
                // console.log('finish a file', val);
                if (val >= this.uploadedFiles.length) {
                  this.uploadedFiles = [];
                  if (callback != null) {
                    callback();
                    this.changeLoad(false);
                  } else {
                    this.changeLoad(false);
                  }

                }
              }
          );
      }
    } else {
      if (callback != null) {
        callback();
        this.changeLoad(false);
      } else {
        this.changeLoad(false);
      }
    }
  }

}
