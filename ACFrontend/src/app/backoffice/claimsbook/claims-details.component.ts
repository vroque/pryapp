import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { BackofficeConfigService } from "../config/config.service";

import { LDRPageClaimsBookService } from "../../shared/services/claimsbook/page-claims-book.service";
import { PageClaimsBook } from "../../shared/services/claimsbook/page-claims-book";

import { LDRFilePageClaimsService } from "../../shared/services/claimsbook/file-page-claims.service";
import { FilePageClaims } from "../../shared/services/claimsbook/file-page-claims";

@Component({
  selector: "ldr-claims-details",
  templateUrl: "./claims-details.component.html",
  providers: [BackofficeConfigService, LDRPageClaimsBookService, LDRFilePageClaimsService]
})

export class LDRClaimsDetailsComponent  implements OnInit {
  @Input()page_id: string;
  //@Input()state: string;
  @Output()getState = new EventEmitter();
  load: boolean;
  error_message: string;
  page_claims_book: PageClaimsBook;//  <PageClaimsBook>{};

  files_page_created: Array<FilePageClaims> = [];
  files_page_finalized: Array<FilePageClaims> = [];
  constructor(
    private config: BackofficeConfigService,
    private PageClaimsBookService: LDRPageClaimsBookService,
    private FilePageClaimsService: LDRFilePageClaimsService,
  ){}
  ngOnInit(): void{
    this.PageClaimsBookService.getPageClaims(this.page_id)
      .subscribe(
        page_claims_book => this.page_claims_book = page_claims_book,
        err => console.log(err),
        () => {
          this.getState.emit(this.page_claims_book);
          this.getFilesPage();
        }
      )
  }


  getDaysRemaining(datecreate: string){
    let daysremaining:number = this.config.LDR_DAYS_COMPLETION_CLAIM;
    let second=1000, minute=second*60, hour=minute*60, day=hour*24, week=day*7;
    let datecrea = +new Date(datecreate);
    let today = +new Date();
    let timediff = today - datecrea;
    let days: number = Math.floor(timediff / day);
    let restant: number = +daysremaining - +days
    //console.log(restant);
    return restant
  }

  getFinalizationDays(date_create: string, date_finalized: string){
    let second=1000, minute=second*60, hour=minute*60, day=hour*24, week=day*7;
    let datecreate = +new Date(date_create);
    let datefinalized = +new Date(date_finalized);
    let timediff = +datefinalized - +datecreate;
    let days: number = Math.floor(timediff / day);
    let difference: number = +days;
    return difference;
  }

  getFilesPage(){
    this.FilePageClaimsService.listFilesPage(this.page_id)
    .subscribe(
      files =>  this.assignFilesPage(files),
      err => console.log(err),
      () => this.load = true
    )
  }

  getNameOrigin(type: string){
    let name;
    name =  this.config._LDR_TYPE_ORIGIN_PAGE.filter( f => f.id === +type )[0].name || '';
    return name;
  }

  getPageVoid(state: boolean){
    if (state) {
        return "SI";
    }
    else if(!state){
      return "NO";
    }
    else{
      return "-";
    }
  }

  assignFilesPage(files: Array<FilePageClaims>){
    this.files_page_created = files.filter(item => +item.file_state === this.config.LDR_STATE_FILE_CREATE);
    //this.files_page_finalized = files.filter(item => +item.file_state === this.config.LDR_STATE_FILE_RESOLVED);
  }
}
