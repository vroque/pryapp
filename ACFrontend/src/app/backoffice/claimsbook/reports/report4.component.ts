import { Component, OnInit } from "@angular/core";
import { BackofficeConfigService } from "../../config/config.service";

import { CampusService } from "../../../shared/services/institutional/campus.service";
import { Campus } from "../../../shared/services/institutional/campus";

import { LDRReportService } from "../../../shared/services/claimsbook/report.service";
import { LDRReport } from "../../../shared/services/claimsbook/report";

// para dolar de jquery
declare var $;
import * as moment from "moment";
import "moment/locale/es";

@Component({
  selector: "ldr-report4",
  templateUrl: "./report4.component.html",
  providers: [BackofficeConfigService, CampusService, LDRReportService],
})

export class LDRReport4  implements OnInit {

  load: boolean = false;

  number_days: number = 10;
  date_start: Date = new Date($.now());
  date_end: Date = new Date($.now());
  total: number = 0;
  ldr_report: LDRReport[];

  report_details: LDRReport;

  constructor(
    private config: BackofficeConfigService,
    private CampusService: CampusService,
    private LDRReportService: LDRReportService,
  ){}
  ngOnInit(): void{
    this.getReport();
  }

  getDates(dates: any){
    //console.log(dates.date_start);
    //console.log(dates.date_end);
    this.date_start = dates.date_start;
    this.date_end = dates.date_end;
    //this.date_start = new Date(moment(dates.date_start).format('MM/DD/YY'));
    //this.date_end = new Date(moment(dates.date_end).format('MM/DD/YY'));

  }

  getReport(){
    this.load = true;

    let dataReport: LDRReport = <LDRReport>{};
    dataReport.number_days = this.number_days;

    dataReport.date_start = this.date_start;
    dataReport.date_end = this.date_end;
    dataReport.number_report = 4;

    //console.log(dataReport.date_start);
    //console.log(dataReport.date_end);

    let caption_labels: Array<string> = [];
    let datas: Array<number> = [];
    let bgs: Array<string> = [];
    let i: number = 0;

    this.LDRReportService.getReport(dataReport)
    .subscribe(
      list_data => {
                this.total = 0;
                this.ldr_report = list_data;
              },
      err => console.log(err),
      () => this.load = false,
    )


  }

  viewDetails(data: LDRReport){
    this.report_details = data;
  }
  clearDetails(){
    this.report_details = null;
  }

}
