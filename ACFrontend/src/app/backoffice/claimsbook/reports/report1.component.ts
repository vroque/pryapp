import { Component, OnInit } from "@angular/core";
import { BackofficeConfigService } from "../../config/config.service";

import { Campus } from "../../../shared/services/institutional/campus";
import { CampusService } from "../../../shared/services/institutional/campus.service";

import { LDRReport } from "../../../shared/services/claimsbook/report";
import { LDRReportService } from "../../../shared/services/claimsbook/report.service";

@Component({
  selector: "ldr-report1",
  templateUrl: "./report1.component.html",
  providers: [BackofficeConfigService, CampusService, LDRReportService],
})

export class LDRReport1  implements OnInit {

  load: boolean = false;

  year: number = null;
  campus: string = null;
  origin: string = null;

  date_start: Date = null;
  date_end: Date = null;

  total: number = 0;

  list_year: string[]=[];
  list_campus: Campus[]=[];
  list_origins: Array<any> = [];

  report_details: LDRReport;

  list_pagesofarea: LDRReport[];
  data_pie: any;
  main_backgrounds:  Array<string> = ["#FF6384", "#36A2EB", "#FFCE56", "#56ffdd", "#fff356", "#ff5656", "#56a7ff"];

  constructor(
    private config: BackofficeConfigService,
    private CampusService: CampusService,
    private LDRReportService: LDRReportService,
  ) {}
  ngOnInit(): void {
      this.generateYears();
      this.getCampus();
      this.getOrigins();;
      this.getReport(null,null,null);
  }

  generateYears(){
    this.list_year = [];
    this.list_year.push("Todos");
    for (let i = +(new Date).getFullYear(); i >= 2010 ; i--) {
      this.list_year.push(String(i));
    }
  }
  getCampus(){
    // this.load = true;
    this.list_campus = [];
    let campus_all: Campus =  <Campus>{};
    campus_all.code = "all";
    campus_all.id = null;
    campus_all.name = "Todos";
    this.list_campus.push(campus_all);
    this.CampusService.query().subscribe(
      (campuses) => {
          for (let campus of campuses) {
              this.list_campus.push(campus);
          }
        },
      (err) => console.log(err),
    );
  }

  getOrigins(){
    let origin_all: any = {};
    origin_all.id = null;
    origin_all.name= "Todos";
    origin_all.complete= "Todos";
    origin_all.icon= "fa fa-file-o";
    this.list_origins.push(origin_all);
    for (let origin of this.config._LDR_TYPE_ORIGIN_PAGE) {
      this.list_origins.push(origin);
    }
  }
  changeYear(year: string){
    this.date_start = null;
    this.date_end = null;
    if (year == 'Todos') {
      this.year = null;
    }
    if (year != 'Todos') {
      this.year = +year;
    }
    this.getReport(this.year, this.campus, this.origin);


  }

  changeCampus(campus: string){
    if (campus.trim().length <= 0) {
      campus = null;
    }
    this.campus = campus;
    this.getReport(this.year, this.campus, this.origin);
  }

  changeOrigin(origin: string){
    if (origin.trim().length <= 0) {
      origin = null;
    }
    this.origin = origin;
    this.getReport(this.year, this.campus, this.origin);
  }

  getDates(dates: any){
    this.year = null;
    this.date_start = dates.date_start;
    this.date_end = dates.date_end;
    this.getReport(this.year, this.campus, this.origin);
  }

  getReport(year: number, campus: string , origin: string){
    this.load = true;
    let dataReport: LDRReport = <LDRReport>{};

    dataReport.year = year;
    dataReport.campus = campus;
    dataReport.origin = origin;

    dataReport.date_start = this.date_start;
    dataReport.date_end = this.date_end;


    dataReport.number_report = 1;

    let caption_labels: Array<string> = [];
    let datas: Array<number> = [];
    let bgs: Array<string> = [];
    let i: number = 0;

    this.LDRReportService.getReport(dataReport)
    .subscribe(
      list_data => {
                this.total = 0;
                this.list_pagesofarea = list_data;
                if (list_data != null && list_data.length > 0) {

                  for(let data of list_data){
                    caption_labels.push(data.area_name);
                    datas.push(data.page_of_area);
                    this.total = this.total + data.page_of_area;
                    if(i >= this.main_backgrounds.length) { i = 0;}
                    bgs.push(this.main_backgrounds[i]);
                    i++;
                  }

                  this.data_pie = {
                    labels: caption_labels,
                    datasets: [
                      {
                        data: datas,
                        backgroundColor: bgs,
                        hoverBackgroundColor:bgs
                      }
                    ]
                  };
                }

              },
      err => console.log(err),
      () => this.load = false
    )


  }

  viewDetails(data: LDRReport){
    this.report_details = data;
  }
  clearDetails(){
    this.report_details = null;
  }

}
