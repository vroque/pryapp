import { Component, OnInit } from "@angular/core";
import { BackofficeConfigService } from "../../config/config.service";

import { CampusService } from "../../../shared/services/institutional/campus.service";
import { Campus } from "../../../shared/services/institutional/campus";

import { LDRReportService } from "../../../shared/services/claimsbook/report.service";
import { LDRReport } from "../../../shared/services/claimsbook/report";

// para dolar de jquery
declare var $;
import * as moment from "moment";
import "moment/locale/es";

@Component({
  selector: "ldr-report5",
  templateUrl: "./report5.component.html",
  providers: [BackofficeConfigService, CampusService, LDRReportService],
})

export class LDRReport5  implements OnInit {

  load: boolean = false;

  number_days: number = 10;
  total: number = 0;
  ldr_report: LDRReport[];
  data_pie: any;
  main_backgrounds:  Array<string> = ["#FF6384", "#36A2EB", "#FFCE56", "#56ffdd", "#fff356", "#ff5656", "#56a7ff"];

  constructor(
    private config: BackofficeConfigService,
    private CampusService: CampusService,
    private LDRReportService: LDRReportService,
  ){}
  ngOnInit(): void{
    this.getReport();
  }

  getReport(){
    this.load = true;
    let dataReport: LDRReport = <LDRReport>{};
    dataReport.number_days = this.number_days;
    dataReport.number_report = 5;

    let caption_labels: Array<string> = [];
    let datas: Array<number> = [];
    let bgs: Array<string> = [];
    let i: number = 0;

    this.LDRReportService.getReport(dataReport)
    .subscribe(
      list_data => {
                this.total = 0;
                this.ldr_report = list_data;
                if (list_data != null && list_data.length > 0) {

                  for(let data of list_data){
                    if (data.pages_number_day > 0) {
                      caption_labels.push('Resuelto en ' + data.number_day + ' día(s).');
                      datas.push(data.pages_number_day);
                      this.total = this.total + data.pages_number_day;
                      if(i >= this.main_backgrounds.length) { i = 0;}
                      bgs.push(this.main_backgrounds[i]);
                      i++;
                    }
                  }

                  this.data_pie = {
                    labels: caption_labels,
                    datasets: [
                      {
                        data: datas,
                        backgroundColor: bgs,
                        hoverBackgroundColor:bgs
                      }
                    ]
                  };
                }


              },
      err => console.log(err),
      () => this.load = false
    )


  }



}
