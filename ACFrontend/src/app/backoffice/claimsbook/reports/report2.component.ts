import { Component, OnInit } from "@angular/core";
import { BackofficeConfigService } from "../../config/config.service";

import { CampusService } from "../../../shared/services/institutional/campus.service";
import { Campus } from "../../../shared/services/institutional/campus";

import { LDRReportService } from "../../../shared/services/claimsbook/report.service";
import { LDRReport } from "../../../shared/services/claimsbook/report";

@Component({
  selector: "ldr-report2",
  templateUrl: "./report2.component.html",
  providers: [BackofficeConfigService, CampusService, LDRReportService],
})

export class LDRReport2  implements OnInit {

  load: boolean = false;

  year: number = null;
  origin: string = null;

  date_start: Date = null;
  date_end: Date = null;

  total: number = 0;

  list_year: string[]=[];
  list_origins: Array<any> = [];

  list_pagesofarea: LDRReport[];
  data_pie: any;
  main_backgrounds:  Array<string> = ["#FF6384", "#36A2EB", "#FFCE56", "#56ffdd", "#fff356", "#ff5656", "#56a7ff"];

  constructor(
    private config: BackofficeConfigService,
    private CampusService: CampusService,
    private LDRReportService: LDRReportService,
  ){}
  ngOnInit(): void{
      this.generateYears();
      this.getOrigins();;
      this.getReport(null,null);
  }

  generateYears(){
    this.list_year = [];
    this.list_year.push("Todos");
    for (let i = +(new Date).getFullYear(); i >= 2010 ; i--) {
      this.list_year.push(String(i));
    }
  }

  getOrigins(){
    let origin_all: any = {};
    origin_all.id = null;
    origin_all.name= "Todos";
    origin_all.complete= "Todos";
    origin_all.icon= "fa fa-file-o";
    this.list_origins.push(origin_all);
    for (let origin of this.config._LDR_TYPE_ORIGIN_PAGE) {
      this.list_origins.push(origin);
    }
  }
  changeYear(year: string){
    this.date_start = null;
    this.date_end = null;

    if (year == 'Todos') {
      this.year = null;
    }
    if (year != 'Todos') {
      this.year = +year;
    }
    this.getReport(this.year, this.origin);


  }

  changeOrigin(origin: string){
    if (origin.trim().length <= 0) {
      origin = null;
    }
    this.origin = origin;
    this.getReport(this.year, this.origin);
  }

  getDates(dates: any){
    this.year = null;
    this.date_start = dates.date_start;
    this.date_end = dates.date_end;
    this.getReport(this.year, this.origin);
  }

  getReport(year: number, origin: string){
    this.load = true;
    let dataReport: LDRReport = <LDRReport>{};
    dataReport.year = year;
    dataReport.origin = origin;

    dataReport.date_start = this.date_start;
    dataReport.date_end = this.date_end;

    dataReport.number_report = 2;

    let caption_labels: Array<string> = [];
    let datas: Array<number> = [];
    let bgs: Array<string> = [];
    let i: number = 0;

    this.LDRReportService.getReport(dataReport)
    .subscribe(
      list_data => {
                this.total = 0;
                this.list_pagesofarea = list_data;
                if (list_data != null && list_data.length > 0) {
                  for(let data of list_data){
                    caption_labels.push(data.campus_name);
                    datas.push(data.page_of_campus);
                    this.total = this.total + data.page_of_campus;
                    if(i >= this.main_backgrounds.length) { i = 0;}
                    bgs.push(this.main_backgrounds[i]);
                    i++;
                  }

                  this.data_pie = {
                    labels: caption_labels,
                    datasets: [
                      {
                        data: datas,
                        backgroundColor: bgs,
                        hoverBackgroundColor:bgs
                      }
                    ]
                  };
                }


              },
      err => console.log(err),
      () => this.load = false
    )


  }



}
