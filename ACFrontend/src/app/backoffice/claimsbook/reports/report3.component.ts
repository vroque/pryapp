import { Component, OnInit } from "@angular/core";
import { BackofficeConfigService } from "../../config/config.service";

import { CampusService } from "../../../shared/services/institutional/campus.service";
import { Campus } from "../../../shared/services/institutional/campus";

import { LDRReportService } from "../../../shared/services/claimsbook/report.service";
import { LDRReport } from "../../../shared/services/claimsbook/report";

// para dolar de jquery
declare var $;
import * as moment from "moment";
import "moment/locale/es";

@Component({
  selector: "ldr-report3",
  templateUrl: "./report3.component.html",
  providers: [BackofficeConfigService, CampusService, LDRReportService],
})

export class LDRReport3  implements OnInit {

  load: boolean = false;

  year: number = null;
  campus: string = null;
  origin: string = null;

  date_start: Date = null;
  date_end: Date = null;

  list_year: string[]=[];
  list_campus: Campus[]=[];
  list_origins: Array<any> = [];

  ldr_report: LDRReport[];

  constructor(
    private config: BackofficeConfigService,
    private CampusService: CampusService,
    private LDRReportService: LDRReportService,
  ){}
  ngOnInit(): void{
    this.generateYears();
    this.getCampus();
    this.getOrigins();;
    this.getReport();
  }

  generateYears(){
    this.date_start = null;
    this.date_end = null;
    this.list_year = [];
    this.list_year.push("Todos");
    for (let i = +(new Date).getFullYear(); i >= 2010 ; i--) {
      this.list_year.push(String(i));
    }
  }
  getCampus(){
    this.list_campus = [];
    let campus_all: Campus =  <Campus>{};
    campus_all.code = "all";
    campus_all.id = null;
    campus_all.name = "Todos";
    this.list_campus.push(campus_all);
    this.CampusService.query().subscribe(
      campuses => {
          for (let campus of campuses) {
              this.list_campus.push(campus);
          }
        },
      err => console.log(err)
    );
  }

  getOrigins(){
    let origin_all: any = {};
    origin_all.id = null;
    origin_all.name= "Todos";
    origin_all.complete= "Todos";
    origin_all.icon= "fa fa-file-o";
    this.list_origins.push(origin_all);
    for (let origin of this.config._LDR_TYPE_ORIGIN_PAGE) {
      this.list_origins.push(origin);
    }
  }
  changeYear(year: string){
    this.date_start = null;
    this.date_end = null;
    if (year == 'Todos') {
      this.year = null;
    }
    if (year != 'Todos') {
      this.year = +year;
    }
    this.getReport();


  }

  changeCampus(campus: string){
    if (campus.trim().length <= 0) {
      campus = null;
    }
    this.campus = campus;
    this.getReport();
  }

  changeOrigin(origin: string){
    if (origin.trim().length <= 0) {
      origin = null;
    }
    this.origin = origin;
    this.getReport();
  }

  getDates(dates: any){
    this.year = null;
    this.date_start = dates.date_start;
    this.date_end = dates.date_end;
    this.getReport();
  }



  getReport(){
    this.load = true;

    let dataReport: LDRReport = <LDRReport>{};
    dataReport.year = this.year;
    dataReport.campus = this.campus;
    dataReport.origin = this.origin;

    dataReport.date_start = this.date_start;
    dataReport.date_end = this.date_end;

    dataReport.number_report = 3;

    let caption_labels: Array<string> = [];
    let datas: Array<number> = [];
    let bgs: Array<string> = [];
    let i: number = 0;

    this.LDRReportService.getReport(dataReport)
    .subscribe(
      list_data => {
                this.ldr_report = list_data;
                for(let data of this.ldr_report) {
                  let values_derivate = [];
                  let values_answer = [];
                  for(let key in data.derivate_involved_area){
                    values_derivate.push({name_area: key, value: data.derivate_involved_area[key]} )
                  }
                  for(let key in data.answer_involved_area){
                    values_answer.push({name_area: key, value: data.answer_involved_area[key]} )
                  }
                  data.derivate_involved_area = values_derivate;
                  data.answer_involved_area = values_answer;
                }
              },
      err => console.log(err),
      () => this.load = false,
    )


  }



}
