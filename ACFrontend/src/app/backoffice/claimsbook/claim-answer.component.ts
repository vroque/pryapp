import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { BackofficeConfigService } from "../config/config.service";

import { AnswerPage } from "../../shared/services/claimsbook/answer-page";
import { LDRAnswerPageService } from "../../shared/services/claimsbook/answer-page.service";
import { DerivationArea } from "../../shared/services/claimsbook/derivation-area";
import { LDRDerivationAreaService } from "../../shared/services/claimsbook/derivation-area.service";
import { FilePageClaims } from "../../shared/services/claimsbook/file-page-claims";
import { LDRFilePageClaimsService } from "../../shared/services/claimsbook/file-page-claims.service";

@Component({
  selector: "ldr-answer-claim-book",
  templateUrl: "./claim-answer.component.html",
  providers: [LDRAnswerPageService, LDRFilePageClaimsService, LDRDerivationAreaService]
})

export class LDRAnswerClaimBook  implements OnInit {
  @Input()page_id: string;

  @Output()getAnswers = new EventEmitter();
  derivate: DerivationArea;
  answer_page: AnswerPage;

  constructor(
    private config: BackofficeConfigService,
    private LDRAnswerPageService: LDRAnswerPageService,
    private FilePageClaimsService: LDRFilePageClaimsService,
    private DerivationAreaService: LDRDerivationAreaService,
  ){}
  ngOnInit(): void{
    this.getDerivations(details_derivation =>{
      this.derivate = details_derivation;
      //console.log(  this.derivate);
      this.getAnswers.emit(this.derivate.answers);
    });
  }


  getDerivations(call_back_ok){
    let derivation: DerivationArea;
    this.DerivationAreaService.listDerivationLegalArea(this.page_id, String(this.config.LDR_TYPE_USER_LEGAL_AREA))
      .subscribe(
        derivation_claims => derivation = derivation_claims,
        err => console.log(err),
        () => call_back_ok(derivation)
      )
  }


}
