import { ActivatedRoute, Params } from '@angular/router';
import { AnswerPage } from '../../../shared/services/claimsbook/answer-page';
import { BackofficeConfigService } from '../../config/config.service';
import { Component, OnInit } from '@angular/core';
import { DerivationArea } from '../../../shared/services/claimsbook/derivation-area';
import { FilePageClaims } from '../../../shared/services/claimsbook/file-page-claims';
import { LDRAnswerPageService } from '../../../shared/services/claimsbook/answer-page.service';
import { LDRDerivationAreaService } from '../../../shared/services/claimsbook/derivation-area.service';
import { LDRFilePageClaimsService } from '../../../shared/services/claimsbook/file-page-claims.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ldr-details-page-claim-ai',
  templateUrl: './detailsPageClaim.component.html',
  providers: [BackofficeConfigService, LDRDerivationAreaService, LDRFilePageClaimsService, LDRAnswerPageService],
})

export class LDRDetailsPageClaimAI  implements OnInit {

  load = false;
  msg_error: string;
  page_id: number;
  state: number;
  derivate_id: number;
  staff_type: number;
  staff_state: number;
  derivate: DerivationArea = <DerivationArea>{};
  file_derivate: FilePageClaims[] = [];
  file_answer: FilePageClaims[] = [];

  constructor(
    private config: BackofficeConfigService,
    private router: Router,
    private _route: ActivatedRoute,
    private derivationAreaService: LDRDerivationAreaService,
    private filePageClaimsService: LDRFilePageClaimsService,
    private lDRAnswerPageService: LDRAnswerPageService
  ) {}

  ngOnInit() {
    this.load = true;
    this._route.params.forEach((params: Params) => {
      this.page_id = params.page_id;
      this.derivate_id = +params.derivate_id;
    });
  }

  getDetailsDerivation(call_back_ok) {
    let derivate: DerivationArea = <DerivationArea>{};
    this.derivationAreaService.getDerivationArea(this.derivate_id)
    .subscribe(
      data => derivate = data,
      err => console.log(err),
      () => call_back_ok(derivate)
    );
  }

  getAnswersDerivate(call_back_ok) {
    let answerPage: AnswerPage[] = [];
    this.lDRAnswerPageService.getAnswersByDerivate(this.page_id, this.derivate_id)
    .subscribe(
      data => answerPage = data,
      err => console.log(err),
      () => call_back_ok(answerPage)
    );
  }

  getAnswer(answerId: number, call_back_ok) {
    let answerPage: AnswerPage;
    this.lDRAnswerPageService.getAnswer(answerId)
    .subscribe(
      data => answerPage = data,
      err => console.log(err),
      () => call_back_ok(answerPage)
    );
  }

  acceptAnswerDerivate(answerId: number) {
    this.load = true;
    this.getAnswer(answerId, answer_page_accept => {
      if (answer_page_accept === null) {
        this.msg_error = 'El reclamo no tiene respuesta.';
        this.load = false;
      } else if ( answer_page_accept.answer_state !== this.config.LDR_STATE_ANSWER_WAIT) {
        this.msg_error = 'La respuesta no esta en espera.';
        this.load = false;
      } else {
        answer_page_accept.answer_state = String(this.config.LDR_STATE_ANSWER_ACCEPTED);
        this.lDRAnswerPageService.updateAnswer(answer_page_accept)
        .subscribe(
          (data) => data,
          (err) => console.log(err),
          () => {
            this.state = this.config.LDR_STATE_FILE_FINALIZED;
            this.load = false;
            this.returnListPages();
          }
        );
      }
    });
  }

  setStatePage(event) {
    this.staff_type = +event.staff_involved_type;
    this.staff_state = +event.staff_derivate_state;
    this.state = +event.page_state;

    this.getDetailsDerivation(details_derivate => {
      this.derivate = details_derivate;
      this.page_id = details_derivate.page_id;
      this.filePageClaimsService.listFilesDerivation(String(this.page_id), String(this.derivate_id))
      .subscribe(
        (data) => this.assigmentFiles(data),
        (err) => {
          this.msg_error = 'Error al obtener Archivos.';
          this.load = false;
        },
        () => {
          this.msg_error = '';
          this.load = false;
        }
      );
    });
  }

  assigmentFiles(files: FilePageClaims[]) {
    this.file_derivate = files.filter(item => + item.file_state === this.config.LDR_STATE_FILE_DERIVATED);
    this.file_answer = files.filter(item => + item.file_state === this.config.LDR_STATE_FILE_ANSWER);
  }

  createDerivation() {
    const link: string[] = [`backoffice/ldr-area-involucrada/derivacion/${this.state}/${this.page_id}/${this.derivate_id}`];
    this.router.navigate(link);
  }

  gotoPageAnswer() {
    const link: string[] = [`backoffice/ldr-area-involucrada/responder/${this.state}/${this.page_id}/${this.derivate_id}`];
    this.router.navigate(link);
  }

  returnListPages() {
    const link: string[] = [`backoffice/ldr-area-involucrada/libro`];
    this.router.navigate(link);
  }
}
