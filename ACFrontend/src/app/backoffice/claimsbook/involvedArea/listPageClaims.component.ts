import { BackofficeConfigService } from '../../config/config.service';
import { Component, OnInit } from '@angular/core';
import { LDRPageClaimsBookService } from '../../../shared/services/claimsbook/page-claims-book.service';
import { PageClaimsBook } from '../../../shared/services/claimsbook/page-claims-book';
import { Router } from '@angular/router';

@Component({
  providers: [BackofficeConfigService, LDRPageClaimsBookService],
  selector: 'ldr-list-pages-claims-ia',
  templateUrl: './listPageClaims.component.html'
})

export class LDRListPagesClaimsIAComponent  implements OnInit {

  load = false;
  title = 'Todos';
  state: number = this.config.LDR_STATE_PAGE_PENDING;
  claims: PageClaimsBook[] = [] ;
  claims_active: PageClaimsBook[] = [] ;
  pending_claims: PageClaimsBook[] = [] ;
  derivated_claims: PageClaimsBook[] = [] ;
  resolved_claims: PageClaimsBook[] = [] ;
  finalized_claims: PageClaimsBook[] = [] ;
  filter: any;
  configFile: any;

  constructor(
    private router: Router,
    private pageClaimsBookService: LDRPageClaimsBookService,
    private config: BackofficeConfigService
  ) {
    this.configFile = this.config;
  }

  ngOnInit() {
    this.load = true;
    this.pageClaimsBookService.listPagesClaims(String(this.config.LDR_TYPE_USER_INVOLVED_AREA))
      .subscribe(
        (response) => this.assignPagesClaimsBook(response),
        err => console.log(err),
        () => {
          this.load = false;
          this.setState(this.state);
        }
      );
  }

  assignPagesClaimsBook(pages: PageClaimsBook[]) {
    this.claims = pages;
    this.claims_active = pages;
    this.pending_claims = this.claims.filter(
      item => +item.page_state === this.config.LDR_STATE_PAGE_PENDING);
    this.derivated_claims = this.claims.filter(
      item => +item.page_state === this.config.LDR_STATE_PAGE_DERIVATED);
    this.resolved_claims = this.claims.filter(
      item => +item.page_state === this.config.LDR_STATE_PAGE_RESOLVED);
    this.finalized_claims = this.claims.filter(
      item => +item.page_state === this.config.LDR_STATE_PAGE_FINALIZED);
  }

  setState(type: number ) {
    this.state = type;
    switch (type) {
      case(this.config.LDR_STATE_PAGE_PENDING):
        this.claims_active = this.pending_claims;
        this.title =  this.config._LDR_STATUS.filter( f => f.id === this.config.LDR_STATE_PAGE_PENDING )[0].complete;
        break;
      case(this.config.LDR_STATE_PAGE_DERIVATED):
        this.claims_active = this.derivated_claims;
        this.title =  this.config._LDR_STATUS.filter( f => f.id === this.config.LDR_STATE_PAGE_DERIVATED )[0].complete;
        break;
      case(this.config.LDR_STATE_PAGE_RESOLVED):
        this.claims_active = this.resolved_claims;
        this.title =  this.config._LDR_STATUS.filter( f => f.id === this.config.LDR_STATE_PAGE_RESOLVED )[0].complete;
        break;
      case(this.config.LDR_STATE_PAGE_FINALIZED):
        this.claims_active = this.finalized_claims;
        this.title =  this.config._LDR_STATUS.filter( f => f.id === this.config.LDR_STATE_PAGE_FINALIZED )[0].complete;
        break;
      default:
        this.claims_active = this.claims;
        this.title = 'Todos';
      }
    }

  getDaysRemaining(datecreate: string): number {
    const daysremaining = this.config.LDR_DAYS_COMPLETION_CLAIM;
    const second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
    const datecrea = +new Date(datecreate);
    const today = +new Date();
    const timediff = today - datecrea;
    const days: number = Math.floor(timediff / day);
    const restant: number = +daysremaining - +days;
    return restant;
  }

  getNameOrigin(type: string): any {
    let name;
    name =  this.config._LDR_TYPE_ORIGIN_PAGE.filter( f => f.id === +type )[0].name;
    return name;
  }

  gotoPageDetail(pageId: string, derivateId: string) {
    const link: string[] = [`backoffice/ldr-area-involucrada/detalle/${this.state}/${pageId}/${derivateId}`];
    this.router.navigate(link);
  }
}
