import { ActivatedRoute, Params } from '@angular/router';
import { AnswerPage } from '../../../shared/services/claimsbook/answer-page';
import { BackofficeConfigService } from '../../config/config.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DerivationArea } from '../../../shared/services/claimsbook/derivation-area';
import { LDRAnswerPageService } from '../../../shared/services/claimsbook/answer-page.service';
import { LDRDerivationAreaService } from '../../../shared/services/claimsbook/derivation-area.service';
import { Router } from '@angular/router';


import { LDRUploadFiles } from '../upload-files.component';
@Component({
  selector: 'ldr-answer-page-claim-ia',
  templateUrl: '/answerPageClaim.component.html',
  providers: [BackofficeConfigService, LDRAnswerPageService, LDRDerivationAreaService],
})

export class LDRAnswerPageClaimIA  implements OnInit {

  load = false;
  msg_error = '';
  answer = '';
  state: number;
  page_id: number;
  derivate_id: number;
  file_validate: boolean;
  @ViewChild('uploader') uploadFilesComponent: LDRUploadFiles;

  constructor(
    private config: BackofficeConfigService,
    private router: Router,
    private _route: ActivatedRoute,
    private derivationAreaService: LDRDerivationAreaService,
    private lDRAnswerPageService: LDRAnswerPageService,
  ) { }

  ngOnInit() {
    this.load = true;
    this._route.params.forEach((params: Params) => {
      this.derivate_id = params.derivate_id;
    });

    this.getDetailsDerivation(details_derivation => {
      this.state = +details_derivation.derivate_state;
      this.page_id = +details_derivation.page_id;
      this.load = false;
    });

  }

  getDetailsDerivation(call_back_ok) {
    let derivate: DerivationArea = <DerivationArea>{};
    this.derivationAreaService.getDerivationArea(this.derivate_id)
    .subscribe(
      (data) => derivate = data,
      (err) => console.log(err),
      () => call_back_ok(derivate)
    );
  }

  answerDeritate() {
    this.load = true;
    this.answer = this.answer.trim();
    this.getDetailsDerivation(details_derivation => {
      if (this.answer.length <= 0) {
        this.msg_error = 'Escriba una respuesta.';
        this.load = false;
      } else if (this.file_validate !== true) {
        this.msg_error = 'Seleccióne Archivos.';
        this.load = false;
      } else if (details_derivation === null) {
        this.msg_error = 'Error al obtener derivación.';
        this.load = false;
      } else if (details_derivation.derivate_state === this.config.LDR_STATE_DERIVATE_FINALIZED ||
        details_derivation.derivate_state === this.config.LDR_STATE_DERIVATE_DISMISSED) {
        this.msg_error = 'La derivación ya fue finalizada o la desestimaron.';
        this.load = false;
      } else {
        this.msg_error = '';
        const answerPage: AnswerPage = <AnswerPage>{};
        answerPage.page_id = this.page_id;
        answerPage.derivate_id = details_derivation.derivate_id;
        answerPage.answer_note = this.answer.trim();
        answerPage.answer_state = String(this.config.LDR_STATE_ANSWER_WAIT);

        this.lDRAnswerPageService.saveAnswer(answerPage)
        .subscribe(
          (data) => this.uploadFilesComponent.saveFile(
            details_derivation.page_id, details_derivation.derivate_id, data.answer_id, String(this.config.LDR_STATE_FILE_RESOLVED)),
          (err) => console.log(err),
          () => this.state = this.config.LDR_STATE_PAGE_RESOLVED
        );
      }
    });
  }

  finalizedFiles(event) {
    if (event === false) {
        this.load = event;
        this.returnDetailsPage();
    }
  }

  returnDetailsPage() {
    const link: string[] = [`backoffice/ldr-area-involucrada/libro`];
    this.router.navigate(link);
  }
}
