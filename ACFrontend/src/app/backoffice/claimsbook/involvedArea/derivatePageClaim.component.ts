import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Router } from '@angular/router';
import { BackofficeConfigService } from '../../config/config.service';
import { LDRDerivationAreaService } from '../../../shared/services/claimsbook/derivation-area.service';
import { DerivationArea } from '../../../shared/services/claimsbook/derivation-area';
import { LDRCollaboratorService } from '../../../shared/services/claimsbook/collaborator.service';
import { LDRStaffInvolvedService } from '../../../shared/services/claimsbook/staff-involved.service';
import { StaffInvolved } from '../../../shared/services/claimsbook/staff-involved';
import { Employee } from '../../../shared/services/employee/employee';

@Component({
  providers: [BackofficeConfigService, LDRDerivationAreaService, LDRCollaboratorService, LDRStaffInvolvedService],
  selector: 'ldr-derivate-page-claim-ia',
  templateUrl: './derivatePageClaim.component.html'
})

export class LDRDerivatePageClaimIA  implements OnInit {

  load = false;
  state: number;
  page_id: number;
  derivate_id: number;
  msg_error = '';
  note = '';
  derivate: DerivationArea = <DerivationArea>{};
  list_collaborators: Employee[] = [];
  collaborator: Employee =  <Employee>{};

  constructor(
    private router: Router,
    private _route: ActivatedRoute,
    private config: BackofficeConfigService,
    private collaboratorService: LDRCollaboratorService,
    private derivationAreaService: LDRDerivationAreaService,
    private staffInvolvedService: LDRStaffInvolvedService
  ) {}

  ngOnInit() {
    this.load = true;
    this._route.params.forEach((params: Params) => {
      this.derivate_id = params.derivate_id;
    });

    this.getDetailsDerivation(details_derivation => {
      this.derivate = details_derivation;
      this.state = details_derivation.derivate_state;
      this.page_id = details_derivation.page_id;
      this.load = false;
    });

  }

  getDetailsDerivation(call_back_ok){
    let derivate: DerivationArea = <DerivationArea>{};
    this.derivationAreaService.getDerivationArea(this.derivate_id)
    .subscribe(
      (data) => derivate = data,
      (err) => console.log(err),
      () => call_back_ok(derivate)
    );
  }

  filterCollaborators(event) {
    const query = event.query;
    this.collaboratorService.searchCollaborators(query)
      .subscribe(
        (colaborators) => this.list_collaborators = colaborators,
        (err) => {
          this.msg_error = 'Error, verifique datos';
          console.log(err);
        },
        () => this.load = false
      );
  }

  saveAsigmentCollaborator() {
    this.load = true;
    this.note = this.note.trim();
    if (this.collaborator == null) {
      this.msg_error = 'Seleccióne un colaborador.';
      this.load = false;
    } else if (this.collaborator.employee_pidm == null ||
      this.collaborator.employee_pidm === undefined || this.collaborator.employee_pidm.toString().length <= 0) {
      this.msg_error = 'Seleccióne un colaborador.';
      this.load = false;
    } else if (this.note.length <= 0 ) {
      this.msg_error = 'Escriba nota';
      this.load = false;
    } else {
      this.msg_error = '';
      const staffInvolved: StaffInvolved =  <StaffInvolved>{};
      staffInvolved.page_id = +this.page_id;
      staffInvolved.derivate_id = +this.derivate_id;
      staffInvolved.staff_involved_pidm = this.collaborator.employee_pidm.toString();
      staffInvolved.staff_position_company = this.collaborator.puesto_organica;
      staffInvolved.staff_involved_user_id =  this.collaborator.employee_user_id.trim();
      staffInvolved.staff_involved_note = this.note.trim();
      staffInvolved.staff_involved_type = String(this.config.LDR_TYPE_INVOLVED_COLLABORATOR);
      this.staffInvolvedService.save(staffInvolved)
        .subscribe(
          (data) => data,
          (err) => console.log(err),
          () => {
            this.load = false;
            this.returnDetailsPage();
          }
        );
    }

  }

  returnDetailsPage() {
    const link: Array<string> = [`backoffice/ldr-area-involucrada/detalle/${this.state}/${this.page_id}/${this.derivate_id}`];
    this.router.navigate(link);
  }

}
