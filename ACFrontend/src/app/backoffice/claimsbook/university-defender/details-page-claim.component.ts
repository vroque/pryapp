import { ActivatedRoute, Params } from '@angular/router';
import { AnswerPage } from '../../../shared/services/claimsbook/answer-page';
import { BackofficeConfigService } from '../../config/config.service';
import { Component, OnInit } from '@angular/core';
import { LDRAnswerPageService } from '../../../shared/services/claimsbook/answer-page.service';
import { LDRPageClaimsBookService } from '../../../shared/services/claimsbook/page-claims-book.service';
import { Router } from '@angular/router';

@Component({
  providers: [BackofficeConfigService, LDRPageClaimsBookService, LDRAnswerPageService],
  selector: 'ldr-details-page-claim-ud',
  templateUrl: './details-page-claim.component.html'
})

export class LDRDetailsPageClaimUD  implements OnInit {

  load = false;
  msg_error = '';
  page_id: string;
  state: number;

  constructor(
    private config: BackofficeConfigService,
    private router: Router,
    private _route: ActivatedRoute,
    private PageClaimsBookService:LDRPageClaimsBookService,
    private lDRAnswerPageService: LDRAnswerPageService
  ) {}

  ngOnInit() {
    this.load = true;
    this._route.params.forEach((params: Params) => {
      this.page_id = params.page_id;
    });
  }

  setStatePage(event) {
    this.state = +event.page_state;
    this.load = false;
  }

  getAnswerPage(callBackOk) {
    let answerPage: AnswerPage = <AnswerPage>{};
    this.lDRAnswerPageService.getAnswerPage(this.page_id)
    .subscribe(
      data => answerPage = data,
      err => console.log(err),
      () => callBackOk(answerPage)
    );
  }


  returnListPages() {
    const link: string[] = [`backoffice/ldr-defensor-universitario/libro`];
    this.router.navigate(link);
  }

}
