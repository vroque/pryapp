import { ActivatedRoute } from '@angular/router';
import { BackofficeConfigService } from '../config/config.service';
import { Component, OnInit, Input } from '@angular/core';
import { DerivationArea } from '../../shared/services/claimsbook/derivation-area';
import { FilePageClaims } from '../../shared/services/claimsbook/file-page-claims';
import { LDRDerivationAreaService } from '../../shared/services/claimsbook/derivation-area.service';
import { LDRFilePageClaimsService } from '../../shared/services/claimsbook/file-page-claims.service';
import { Router } from '@angular/router';
import { StaffInvolved } from '../../shared/services/claimsbook/staff-involved';

@Component({
  providers: [BackofficeConfigService, LDRDerivationAreaService, LDRFilePageClaimsService],
  selector: 'ldr-claims-derivates',
  templateUrl: './claims-derivates.component.html'
})

export class LDRClaimsDerivateComponent  implements OnInit {

  @Input() page_id: string;
  @Input() state: string;
  load = false;
  error_message: string;
  derivation_claims: DerivationArea[] = [];
  staff_involved: StaffInvolved =  <StaffInvolved>{};
  files_derivate: FilePageClaims[] = [];
  configFile: any;

  constructor(
    private config: BackofficeConfigService,
    private router: Router,
    private _route: ActivatedRoute,
    private derivationAreaService: LDRDerivationAreaService,
    private filePageClaimsService: LDRFilePageClaimsService
  ) {
    this.configFile = this.config;
  }

  ngOnInit() {
    this.getDerivations();
  }

  getDerivations() {
    this.load = true;
    this.derivationAreaService.listDerivationInvolvedArea(this.page_id, String(this.config.LDR_TYPE_USER_INVOLVED_AREA))
      .subscribe(
        (response) => this.assignDerivation(response),
        err => console.log(err),
        () => this.getFilesDerivation()
      );
  }

  assignDerivation(derivations: DerivationArea[]) {
    this.derivation_claims = derivations.filter(
      item => +item.derivate_type === this.config.LDR_TYPE_DERIVATE_INVOLVED_AREA);
  }

  getFilesDerivation() {
    for (let i = 0; i < this.derivation_claims.length; i++) {
      const page_id = this.derivation_claims[i].page_id;
      const derivationId = this.derivation_claims[i].derivate_id;
      this.filePageClaimsService.listFilesDerivation(String(page_id), String(derivationId))
      .subscribe(
        files => {
          this.derivation_claims[i].files_derivate = files.filter(
            item => +item.file_state === this.config.LDR_STATE_FILE_DERIVATED);
          this.derivation_claims[i].files_answer = files.filter(
            item => +item.file_state === this.config.LDR_STATE_FILE_ANSWER);
        },
        err => console.log(err),
        () => this.load = false
      );
    }
  }

  /*+item.file_state === this.config.LDR_STATE_FILE_CREATE);*/
  gotoDisableDerivation(pageId: string, derivationId: string) {
    const link: string[] = [`backoffice/ldr-administracion/deshabilitar-derivacion/${this.state}/${pageId}/${derivationId}`];
    this.router.navigate(link);
  }

}
