import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params }   from "@angular/router";
import { Router }   from "@angular/router";
import { AutoCompleteModule, InputSwitchModule} from 'primeng/primeng';

import { BackofficeConfigService } from "../../config/config.service";

import { LDRAnswerPageService } from "../../../shared/services/claimsbook/answer-page.service";
import { AnswerPage } from "../../../shared/services/claimsbook/answer-page";

import { LDRPageClaimsBookService } from "../../../shared/services/claimsbook/page-claims-book.service";
import { PageClaimsBook } from "../../../shared/services/claimsbook/page-claims-book";

import { LDRCollaboratorService } from "../../../shared/services/claimsbook/collaborator.service";
import { Employee } from "../../../shared/services/employee/employee";


@Component({
  selector: "ldr-page-details-claims",
  templateUrl: "./page-details.component.html",
  providers: [BackofficeConfigService, LDRPageClaimsBookService, LDRAnswerPageService, LDRCollaboratorService],
})

export class LDRPageDetailsComponent  implements OnInit {
  load: boolean = false;
  msg_error: string = "";
  page_id: string;
  state: number;
  answer_id: number;


  answer_page: AnswerPage;

  list_collaborators: Array<Employee> = [];

  collaborator: Employee;
  avoid_state: boolean = false;
  description_avoid: string = "";
  // page_claims_book: PageClaimsBook =  <PageClaimsBook>{};
  constructor(
    private config: BackofficeConfigService,
    private router: Router,
    private _route: ActivatedRoute,
    private PageClaimsBookService:LDRPageClaimsBookService,
    private LDRAnswerPageService: LDRAnswerPageService,
    private CollaboratorService: LDRCollaboratorService,
  ){}
  ngOnInit(): void{
    this.load = true;
    this._route.params.forEach((params: Params) => {
      //this.state = +params.state;
      this.page_id = params.page_id;
    });
  }

  setStatePage(event){
    this.state = +event.page_state;
    this.load = false;
  }

  setAnswer(event){
    for (let answer of event) {
        this.answer_id = answer.answer_id;
    }
  }

  getAnswer(call_back_ok){
    let answer_page: AnswerPage = <AnswerPage>{};
    this.LDRAnswerPageService.getAnswer(this.answer_id)
    .subscribe(
      data => answer_page = data,
      err => console.log(err),
      () => call_back_ok(answer_page)
    )
  }


  acceptAnswerPage(answer_id:number){
    this.load = true;
    this.getAnswer(answer_page_accept =>
    {
      if(answer_page_accept === null){
        this.msg_error = "El reclamo no tiene respuesta.";
        this.load = false;
      }
      else if( answer_page_accept.answer_state != this.config.LDR_STATE_ANSWER_ACCEPTED){
        this.msg_error = "La respuesta no esta en espera.";
        this.load = false;
      }
      else{
        answer_page_accept.answer_state = String(this.config.LDR_STATE_ANSWER_ACCEPTED_FINISH);
        this.LDRAnswerPageService.updateAnswer(answer_page_accept)
        .subscribe(
          data => data,
          err => console.log(err),
          () => {
            this.state = this.config.LDR_STATE_ANSWER_ACCEPTED_FINISH;
            this.load = false;
            this.returnListPages();
          }
        )
      }

    })
  }


  rejectAnswerPage(answer_id:number){
    this.load = true;
    this.getAnswer(answer_page_accept =>
    {
      if(answer_page_accept === null){
        this.msg_error = "El reclamo no tiene respuesta.";
        this.load = false;
      }
      else if( answer_page_accept.answer_state != this.config.LDR_STATE_ANSWER_ACCEPTED){
        this.msg_error = "La respuesta no esta en espera.";
        this.load = false;
      }
      else{
        answer_page_accept.answer_state = String(this.config.LDR_STATE_ANSWER_REJECTED);
        this.LDRAnswerPageService.updateAnswer(answer_page_accept)
        .subscribe(
          data => data,
          err => console.log(err),
          () => {
            this.state = this.config.LDR_STATE_ANSWER_REJECTED;
            this.load = false;
            this.returnListPages();
          }
        )
      }

    })
  }

  saveObservations(){
    this.load = true;
    if (this.collaborator == null || this.collaborator.employee_pidm.toString().length <= 0) {
        this.msg_error = "Seleccione un personal.";
        this.load = false;
    }
    else if (this.avoid_state == null){
      this.msg_error = "Seleccione estado."
      this.load = false;
    }
    else if (this.description_avoid == null || this.description_avoid.trim().length <= 0) {
      this.msg_error = "Escriba una descripcion."
      this.load = false;
    }
    else{
      let page_observation: PageClaimsBook = <PageClaimsBook>{};
      page_observation.page_id = +this.page_id;
      page_observation.page_person_claim = this.collaborator.employee_pidm.toString();
      page_observation.page_avoid = this.avoid_state;
      page_observation.page_avoid_description = this.description_avoid.trim();
      this.PageClaimsBookService.updatePersonClaim(page_observation)
      .subscribe(
        data => data,
        err =>
              {
                console.log(err)
                this.msg_error = "Error al guardar observación."
                this.load = false;
              },
        () =>
              {
                this.msg_error = ""
                this.load = false;
              }
      )
    }

  }

  createDerivation(page_id: string){
    let link: Array<string> = [`backoffice/ldr-administracion/derivacion/${this.state}/${page_id}`];
    this.router.navigate(link);
  }


  filterCollaborators(event) {
        let query = event.query;
        this.CollaboratorService.searchCollaborators(query)
          .subscribe(
            colaborators => this.list_collaborators = colaborators,
            err => {
              this.msg_error = "Error, verifique datos";
              console.log(err);},
              () => this.load = false
          )
    }


  dismissedPage(page_id: string){
    let link: Array<string> = [`backoffice/ldr-administracion/desestimar/${this.state}/${page_id}`];
    this.router.navigate(link);
  }

  gotoPageAnswer(page_id: string): void {
    let link: Array<string> = [`backoffice/ldr-administracion/responder/${this.state}/${page_id}`];
    this.router.navigate(link);
  }

  returnListPages(){
    let link: Array<string> = [`backoffice/ldr-administracion/libro`];
    this.router.navigate(link);
  }

}
