import { Component, OnInit, ViewChild  } from "@angular/core";
import {AutoCompleteModule, FileUploadModule, FileUpload, ChipsModule} from 'primeng/primeng';
import { ActivatedRoute, Params }   from "@angular/router";
import { Router }   from "@angular/router";
import { BackofficeConfigService } from "../../config/config.service";

import { LDRPageClaimsBookService } from "../../../shared/services/claimsbook/page-claims-book.service";
import { PageClaimsBook } from "../../../shared/services/claimsbook/page-claims-book";

import { LDRDerivationAreaService } from "../../../shared/services/claimsbook/derivation-area.service";
import { DerivationArea } from "../../../shared/services/claimsbook/derivation-area";

import { LDRStaffInvolvedService } from "../../../shared/services/claimsbook/staff-involved.service";
import { StaffInvolved } from "../../../shared/services/claimsbook/staff-involved";

import { LDRCollaboratorService } from "../../../shared/services/claimsbook/collaborator.service";

import { LDRUploadFiles } from "../upload-files.component";
import { Employee } from "../../../shared/services/employee/employee";

@Component({
  selector: "ldr-derivate-page-claims",
  templateUrl: "./derive-page.component.html",
  providers: [BackofficeConfigService, LDRPageClaimsBookService, LDRDerivationAreaService, LDRStaffInvolvedService, LDRCollaboratorService]
})

export class LDRDerivatePageClaims  implements OnInit {
  load: boolean = false;
  page_id: string;
  state: number;
  msg_error: string = "";
  list_collaborators: Array<Employee> = [];
  collaborator: Employee; //=  <Collaborator>{};



  mails: string = "";
  note: string = "";

  file_validate: boolean;
  @ViewChild('uploader') uploadFilesComponent: LDRUploadFiles;


  list_string_derivate:  Array<any> = [
    { id: "Atención",
      message: "Para su atención."},
      { id: "Tiempo",
        message: "Favor de atender lo antes Posible."},


  ];



  constructor(
    private router: Router,
    private _route: ActivatedRoute,
    private config: BackofficeConfigService,

    private PageClaimsBookService: LDRPageClaimsBookService,
    private DerivationAreaService: LDRDerivationAreaService,
    private CollaboratorService: LDRCollaboratorService,
    private StaffInvolvedService: LDRStaffInvolvedService
  ){}
  ngOnInit(): void{
    this.load = true;
    this._route.params.forEach((params: Params) => {
      //this.state = +params.state;
      this.page_id = params.page_id;
    });
    this.getDataPageClaim(details_page =>{
      this.state = +details_page.page_state;
      this.load = false;
    });
  }

  filterCollaborators(event) {
        let query = event.query;
        this.CollaboratorService.searchCollaborators(query)
          .subscribe(
            colaborators => this.list_collaborators = colaborators,
            err => {
              this.msg_error = "Error, verifique datos";
              console.log(err);},
              () => this.load = false
          )
    }

  getDataPageClaim(call_back_ok){
    let page_claims_book: PageClaimsBook;
    this.PageClaimsBookService.getPageClaims(this.page_id)
    .subscribe(
      data => page_claims_book = data,
      err => console.log(err),
      () => call_back_ok(page_claims_book)
    )
  }

  listDerivateFuntionalUnity(page_id: string, derivate_funtional_unity: string, cb_ok){
    let data: Array<DerivationArea> = [];
    this.DerivationAreaService.listDerivationPageFuntionalUnity(page_id, derivate_funtional_unity)
      .subscribe(
        derivation => data = derivation,
        err => {
          this.msg_error = "Error, verifique los datos";
          console.log(err);},
          () => cb_ok(data) // solo si no hay error
      )
  }

  saveDerivationArea(){
    this.load = true;
    let derivation_area: DerivationArea=  <DerivationArea>{};
    let staff_involved: StaffInvolved = <StaffInvolved>{};

    derivation_area.page_id = +this.page_id.trim();

    if ( this.collaborator != null && Object.keys(this.collaborator).length > 0){
      derivation_area.derivate_funtional_unity = this.collaborator.unidad_funcional_organica.trim();
      staff_involved.staff_involved_pidm = this.collaborator.employee_pidm.toString().trim();
      staff_involved.staff_position_company = this.collaborator.puesto_organica.trim();
      staff_involved.staff_involved_user_id =  this.collaborator.employee_user_id.trim();
    }
    derivation_area.derivate_mail_recipient = String(this.mails).trim();
    staff_involved.page_id = +this.page_id.trim();
    staff_involved.staff_involved_note = this.note.trim();
    let emails: Array<any> = derivation_area.derivate_mail_recipient.split(",");
    let validate_email: boolean = true;

    for (let email of emails){
      if (this.isEmail(email) === false) {
          validate_email = false;
      }
    }
    this.getDataPageClaim(details_page => {
      this.listDerivateFuntionalUnity(this.page_id, derivation_area.derivate_funtional_unity, list_derivate_funtional_unty => {
        if (this.collaborator === null) {
          this.msg_error = "Seleccione un colaborador.";
          this.load = false;
        }
        else if (Object.keys(this.collaborator).length <= 0) {
          this.msg_error = "Seleccione un colaborador.";
          this.load = false;
        }
        else if ( derivation_area.page_id <= 0) {
          this.msg_error = "Error intente seleccionar un reclamo.";
          this.load = false;
        }
        else if ( derivation_area.derivate_mail_recipient.length <= 0) {
          this.msg_error = "Escriba algun correo.";
          this.load = false;
        }
        else if(validate_email === false){
          this.msg_error = "Verifique que los correos sean correctos.";
          this.load = false;
        }

        else if ( this.file_validate != true) {
          this.msg_error = "Seleccione archivos.";
          this.load = false;
        }
        else if ( staff_involved.staff_involved_pidm.length <= 0) {
          this.msg_error = "Seleccione un colaborador.";
          this.load = false;
        }
        else if ( staff_involved.staff_position_company.length <= 0) {
          this.msg_error = "Seleccione un colaborador.";
          this.load = false;
        }
        else if ( staff_involved.staff_involved_note.length <= 0) {
          this.msg_error = "Escriba la nota de derivación.";
          this.load = false;
        }
        else if  (list_derivate_funtional_unty != null && list_derivate_funtional_unty.length > 0) {
          this.msg_error = "ya existe una derivación al area seleccionada.";
          this.load = false;
        }
        else  if (details_page == null || (details_page.page_state != this.config.LDR_STATE_PAGE_DERIVATED && details_page.page_state != this.config.LDR_STATE_PAGE_PENDING)) {
          this.msg_error = "Solo se puede derivar Reclamos con estado Pendiente o derivado.";
          this.load = false;
        }
        else{
          this.msg_error = "";
          this.DerivationAreaService.save(derivation_area)
          .subscribe(
            data => this.saveStaffInvolved(staff_involved,data.id),
            err => {
              this.msg_error = "Error, verifique los datos";
              console.log(err);},
              //() => console.log("derivacion: ")
          );
        }
      })
    });

  }



  saveStaffInvolved(data: StaffInvolved, derivation_id: string){
    data.derivate_id = +derivation_id;
    data.staff_involved_type = String(this.config.LDR_TYPE_INVOLVED_BOSS);
    //console.log(data);
    this.StaffInvolvedService.save(data)
      .subscribe(
        data => this.uploadFilesComponent.saveFile(this.page_id,derivation_id, "XXX", String(this.config.LDR_STATE_FILE_DERIVATED)),// this.savefile(derivation_id),
        err => console.log(err),
        //() => console.log("personal: ")
      );
  }

  isEmail(email: string) {
    let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  finalizedFiles(Event){
    if (Event === false) {
        this.load = false;
        this.returnDetails();
    }
  }

  returnDetails()
  {
    this.router.navigate([`/backoffice/ldr-administracion/detalle/${this.state}/${this.page_id}`]);
  }

  setReason(reason: string): void {
    this.note = reason;
  }


}
