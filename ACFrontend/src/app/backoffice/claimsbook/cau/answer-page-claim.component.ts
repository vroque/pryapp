import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Params }   from "@angular/router";
import { Router }   from "@angular/router";
import { BackofficeConfigService } from "../../config/config.service";

import { LDRAnswerPageService } from "../../../shared/services/claimsbook/answer-page.service";
import { AnswerPage } from "../../../shared/services/claimsbook/answer-page";

import { LDRSatisfactionSurveyService } from "../../../shared/services/claimsbook/satisfaction_survey.service";
import { SatisfactionSurvey } from "../../../shared/services/claimsbook/satisfaction_survey";

import { LDRUploadFiles } from "../upload-files.component";

@Component({
  selector: "ldr-answer-page-claim",
  templateUrl: "./answer-page-claim.component.html",
  providers: [BackofficeConfigService, LDRAnswerPageService, LDRSatisfactionSurveyService],
})

export class LDRAnswerPageClaim  implements OnInit {
  load: boolean = false;
  msg_error: string = "";
  state: number;
  page_id: string;

  answer: string = "";

  file_validate: boolean;
  @ViewChild('uploader') uploadFilesComponent: LDRUploadFiles;

  constructor(
    private config: BackofficeConfigService,
    private router: Router,
    private _route: ActivatedRoute,
    private LDRAnswerPageService: LDRAnswerPageService,
    private LDRSatisfactionSurveyService: LDRSatisfactionSurveyService,
  ){}
  ngOnInit(): void{
    this._route.params.forEach((params: Params) => {
      this.state = +params.state;
      this.page_id = params.page_id;
    });
  }
  answerPage(){
    this.load = true;
    let answer_page: AnswerPage = <AnswerPage>{};
    answer_page.page_id = +this.page_id.trim();
    answer_page.answer_note = this.answer.trim();
    answer_page.answer_state = String(this.config.LDR_STATE_ANSWER_WAIT);

    if(String(answer_page.page_id).length <= 0){
      this.msg_error = "Error al obterner código de reclamo."
      this.load = false;
    }
    else if(answer_page.answer_note .length <= 0){
      this.msg_error = "La respuesta no puede quedar Vacia."
      this.load = false;
    }
    else if(answer_page.answer_state.length <= 0){
      this.msg_error = "Estado no definido."
      this.load = false;
    }
    else if ( this.file_validate != true) {
      this.msg_error = "Seleccione archivos.";
      this.load = false;
    }
    else{
      this.msg_error = "";
      this.saveAnswer(answer_page);
    }

  }

  saveAnswer(answer_page: AnswerPage){
    this.LDRAnswerPageService.saveAnswer(answer_page)
    .subscribe(
      data => this.saveFile(data.answer_id),
      err => console.log(err),
      () => this.state = this.config.LDR_STATE_PAGE_RESOLVED
    )
  }

  saveFile(answer_id: string){
    this.uploadFilesComponent.saveFile(
                  this.page_id,
                  "XXX",
                  answer_id,
                  String(this.config.LDR_STATE_FILE_RESOLVED),
                  () => this.genererateSatisfactionService(+this.page_id)),
                err => console.log(err)

  }

  genererateSatisfactionService(page_id: number){
    this.load = true;
    this.LDRSatisfactionSurveyService.saveSatisfactionSurvey(page_id)
    .subscribe(
      data => data,
      err => {
          console.log(err)
          this.msg_error = "Error al enviar encuesta de satisfacción";
          this.load = false;
        },
      () => {
            this.state = this.config.LDR_STATE_PAGE_RESOLVED;
            this.returnDetailsPage();
            this.load = false;
          }
    )

  }

  finalizedFiles(event){
    if (event === false) {
        //this.load = event;
        //this.returnDetailsPage();
    }
  }

  returnDetailsPage(){
    let link: Array<string> = [`backoffice/ldr-administracion/detalle/${this.state}/${this.page_id}`];
    this.router.navigate(link);
  }
}
