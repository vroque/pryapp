import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params }   from "@angular/router";
import { Router }   from "@angular/router";
import { BackofficeConfigService } from "../../config/config.service";

import { LDRPageClaimsBookService } from "../../../shared/services/claimsbook/page-claims-book.service";
import { PageClaimsBook } from "../../../shared/services/claimsbook/page-claims-book";

@Component({
  selector: "ldr-dismissed-page",
  templateUrl: "./dismissed-page.component.html",
  providers: [BackofficeConfigService, LDRPageClaimsBookService]
})

export class LDRDismissedPage  implements OnInit {
  load: boolean = false;
  page_id: string;
  state: number;
  msg_error: string = "";
  page_claims_book: PageClaimsBook =  <PageClaimsBook>{};
  note_dissmised: string = "";
  constructor(
    private config: BackofficeConfigService,
    private router: Router,
    private _route: ActivatedRoute,
    private PageClaimsBookService:LDRPageClaimsBookService,
  ){}
  ngOnInit(): void{
    this.load = true;
    this._route.params.forEach((params: Params) => {
      //this.state = +params.state;
      this.page_id = params.page_id;
    });

    this.getDataPageClaim(details_page =>{
      this.state = +details_page.page_state;
      this.page_claims_book = details_page;
      this.load = false;
    });
  }

  getDataPageClaim(call_back_ok){
    let page_claims_book: PageClaimsBook;
    this.PageClaimsBookService.getPageClaims(this.page_id)
    .subscribe(
      data => page_claims_book = data,
      err => console.log(err),
      () => call_back_ok(page_claims_book)
    )
  }


  dismissedPage(){
    this.getDataPageClaim(details_page => {
      this.load = true;
      this.note_dissmised.trim();
      //this.page_claims_book.page_id = +this.page_id.trim();
      this.page_claims_book.page_note_dissmised = this.note_dissmised.trim();
      this.page_claims_book.page_state = String(this.config.LDR_STATE_PAGE_DISMISSED);

      if(String(this.page_claims_book.page_id).length <= 0){
        this.msg_error = "Error al obterner código de reclamo."
        this.load = false;
      }
      else if(this.page_claims_book.page_note_dissmised.length <= 0){
        this.msg_error = "La nota de derivación no puede quedar vacia."
        this.load = false;
      }
      else if(this.page_claims_book.page_state.length <= 0){
        this.msg_error = "Estado no definido."
        this.load = false;
      }
      else if(details_page == null || details_page.page_state != this.config.LDR_STATE_PAGE_PENDING) {
        this.msg_error = "Solo se puede desestimar reclamos en estado pendiente."
        this.load = false;
      }
      else{
        this.msg_error = ""
        this.PageClaimsBookService.update(this.page_claims_book)
        .subscribe(
          data => data,
          err => console.log(err),
          () =>
            {
              this.load = false;
              this.state = +this.page_claims_book.page_state;
              this.returnDetailsPage();
            }
        )
      }

    })




  }

  returnDetailsPage(){
    let link: Array<string> = [`backoffice/ldr-administracion/detalle/${this.state}/${this.page_id}`];
    this.router.navigate(link);
  }


}
