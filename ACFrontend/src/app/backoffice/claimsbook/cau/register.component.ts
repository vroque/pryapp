import { BackofficeConfigService } from '../../config/config.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { LDRFilePageClaimsService } from '../../../shared/services/claimsbook/file-page-claims.service';
import { LDRPageClaimsBookService } from '../../../shared/services/claimsbook/page-claims-book.service';
import { LDRUnityBussinesService } from '../../../shared/services/claimsbook/unity-bussines.service';
import { LDRUnityFuntionalService } from '../../../shared/services/claimsbook/unity-funtional.service';
import { LDRUploadFiles } from '../upload-files.component';
import { PageClaimsBook } from '../../../shared/services/claimsbook/page-claims-book';
import { Router } from '@angular/router';
import { UnityBussines } from '../../../shared/services/claimsbook/unity-bussines';
import { UnityFuntional } from '../../../shared/services/claimsbook/unity-funtional';

@Component({
  providers: [
    BackofficeConfigService,
    LDRFilePageClaimsService,
    LDRUnityBussinesService,
    LDRUnityFuntionalService,
    LDRPageClaimsBookService
  ],
  selector: 'ldr-create-claims-book',
  templateUrl: './register.component.html'
})

export class LDRBookRegisterComponent implements OnInit {

  saved = false;
  load = true;
  msg_error = '';
  list_unity_bussines: UnityBussines[] = [];
  list_unity_funtional: UnityFuntional[] = [];
  page_claims_book: PageClaimsBook = <PageClaimsBook>{};

  // Subir Archivos
  file_validate: boolean;
  @ViewChild('uploader') uploadFilesComponent: LDRUploadFiles;

  es: any;
  date2: any;
  wata: number;
  constructor(
    private router: Router,
    private config: BackofficeConfigService,
    private pageClaimsBookService: LDRPageClaimsBookService,
    private unityBussinesService: LDRUnityBussinesService,
    private UnityFuntionalService: LDRUnityFuntionalService,
    private FilePageClaimsService: LDRFilePageClaimsService
  ) { }
  ngOnInit(): void {

    this.es = {
      firstDayOfWeek: 1,
      dayNames: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
      dayNamesShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
      dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
      monthNames: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio',
      'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
      monthNamesShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
      today: 'Hoy',
      clear: 'Borrar'
    };
    this.wata = (new Date()).getFullYear();
    this.unityBussinesService.listUnityBussines()
      .subscribe(
        (list) => this.list_unity_bussines = list,
        (err) => console.log(err),
        () => this.load = false
      );
  }

  listPagesCampusNumerate(div: string, campus: string, numerate: number, call_back_ok) {
    let listPageNumerate: PageClaimsBook[] = [];
    this.pageClaimsBookService.getPagesCampusNumerate(div, campus, numerate)
      .subscribe(
        (data) => listPageNumerate = data,
        (err) => {
          this.msg_error = 'Error, verifique los datos';
          console.log(err);
        },
        () => call_back_ok(listPageNumerate)
      );
  }


  addclaims(pageForms, event) {
    this.load = true;
    const page: PageClaimsBook = this.page_claims_book;
    page.page_first_name = page.page_first_name.trim();
    page.page_last_name = page.page_last_name.trim();
    page.page_identification = page.page_identification.trim();
    page.page_phone = page.page_phone;
    page.page_home = page.page_home.trim();
    page.page_type_well = page.page_type_well.trim();
    page.page_description_well = page.page_description_well.trim();
    page.page_type_claim = page.page_type_claim.trim();
    page.page_description_claim = page.page_description_claim.trim();
    page.page_numeration = page.page_numeration;

    this.listPagesCampusNumerate(page.page_div, page.page_campus, page.page_numeration, data => {
      if (page.page_email != null) {
        page.page_email = page.page_email.trim();
      }
      if (page.page_legal_guardian != null) {
        page.page_legal_guardian = page.page_legal_guardian.trim();
      }
      if (page.page_bussines_unity.length === 0) { /// === 0
        this.msg_error = 'Seleccióne alguna unidad de negoció.';
        this.load = false;
      } else if (String(page.page_date_create).length <= 0 || page.page_date_create == null) {
        this.msg_error = 'Indicar fecha que se registro reclamo.';
        this.load = false;
      } else if (page.page_first_name.length === 0) {
        this.msg_error = 'Los nombres no pueden estar vacios.';
        this.load = false;
      } else if (page.page_last_name.length === 0) {
        this.msg_error = 'Los apellidos no pueden estar vacios.';
        this.load = false;
      } else if (page.page_identification.length === 0) {
        this.msg_error = 'El número de identificación no puede estar vació.';
        this.load = false;
      } else if (page.page_phone.length === 0) {
        this.msg_error = 'El telefono no puede estar vacio.';
        this.load = false;
      } else if (page.page_home.length === 0) {
        this.msg_error = 'La dirección no puede estar vacio.';
        this.load = false;
      } else if (page.page_type_well.length === 0) {
        this.msg_error = 'Seleccióne un tipo de bién.';
        this.load = false;
      } else if (page.page_description_well.length === 0) {
        this.msg_error = 'la descrición del bien no puede estar vacio.';
        this.load = false;
      } else if (page.page_type_claim.length === 0) {
        this.msg_error = 'Seleccióne el tipo que reclamo.';
        this.load = false;
      } else if (page.page_description_claim.length === 0) {
        this.msg_error = 'La descripción del reclamo no puede estar vacio.';
        this.load = false;
      } else if (page.page_numeration <= 0) {
        this.msg_error = 'El número de hoja no puede ser menor o igual a 0.';
        this.load = false;
      } else if (this.file_validate !== true) {
        this.msg_error = 'Seleccione archivos.';
        this.load = false;
      } else if (data != null && data.length > 0) {
        this.msg_error = 'Existe un reclamo con la numeración indicada.';
        this.load = false;
      } else {
        this.msg_error = '';
        this.pageClaimsBookService.save(page)
          .subscribe(
            (response) => this.uploadFilesComponent.saveFile(
              response.page_id, 'XXX', 'XXX',
              String(this.config.LDR_STATE_FILE_CREATE),
              () => this.sendEmailCreation(response.page_id)),
            (err) => console.log(err)
          );
      }
    });
  }

  sendEmailCreation(pageId: number) {
    this.load = true;
    this.pageClaimsBookService.sendMailCreation(pageId)
      .subscribe(
        (data) => data,
        (err) => {
          this.msg_error = 'Error al enviar Email';
          console.log(err);
          this.load = false;
        },
        () => {
          this.msg_error = '';
          this.load = false;
          this.saved = true;
        }
      );
  }

  newPage() {
    this.page_claims_book = <PageClaimsBook>{};
    this.saved = false;
  }

}
