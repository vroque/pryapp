import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params }   from "@angular/router";
import { Router }   from "@angular/router";
import { BackofficeConfigService } from "../../config/config.service";

import { LDRDerivationAreaService } from "../../../shared/services/claimsbook/derivation-area.service";
import { DerivationArea } from "../../../shared/services/claimsbook/derivation-area";

@Component({
  selector: "ldr-disable-derivation",
  templateUrl: "./disable-derivation.component.html",
  providers: [BackofficeConfigService, LDRDerivationAreaService]
})

export class LDRDisableDerivation  implements OnInit {
  load: boolean = false;
  page_id: string;
  derivation_id: string;
  state: number;
  msg_error: string = "";
  derivation_area: DerivationArea =  <DerivationArea>{};
  note_disable: string ="";
  constructor(
    private config: BackofficeConfigService,
    private router: Router,
    private _route: ActivatedRoute,
    private DerivationAreaService: LDRDerivationAreaService,
  ){}
  ngOnInit(): void{
    this._route.params.forEach((params: Params) => {
      this.state = +params.state;
      this.page_id = params.page_id;
      this.derivation_id = params.derivation_id;
    });
  }

  disablederivation(){
    this.load = true;
    this.derivation_area.page_id =  +this.page_id.trim();
    this.derivation_area.derivate_id =  +this.derivation_id.trim();
    this.derivation_area.derivate_note_disabled = this.note_disable.trim();
    this.derivation_area.derivate_state = "0";
    this.derivation_area.action_derivate = String(this.config.LDR_STATE_DERIVATE_DISMISSED);


    if (String(this.derivation_area.page_id).length <= 0) {
        this.msg_error = "Error al obtener id de reclamo.";
        this.load = false;
    }
    else if (String(this.derivation_area.derivate_id).length <= 0) {
        this.msg_error = "Error al obtener id de derivación.";
        this.load = false;
    }
    else if (this.derivation_area.derivate_note_disabled.length <= 0) {
        this.msg_error = "La nota de retiro no puede quedar vacio.";
        this.load = false;
    }
    else if (this.derivation_area.derivate_state.length <= 0) {
        this.msg_error = "Error al obtener estado.";
        this.load = false;
    }
    else{
        this.msg_error = ""
        this.DerivationAreaService.update(this.derivation_area)
        .subscribe(
          data => data,
          err => console.log(err),
          () =>
            {
              this.returnDetailsPage();
              this.load = false;
            }
        )
    }
  }


  returnDetailsPage(){
    let link: Array<string> = [`backoffice/ldr-administracion/detalle/${this.state}/${this.page_id}`];
    this.router.navigate(link);
  }
}
