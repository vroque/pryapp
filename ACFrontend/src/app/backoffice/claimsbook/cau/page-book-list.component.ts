import { BackofficeConfigService } from '../../config/config.service';
import { Component, OnInit } from '@angular/core';
import { LDRPageClaimsBookService } from '../../../shared/services/claimsbook/page-claims-book.service';
import { PageClaimsBook } from '../../../shared/services/claimsbook/page-claims-book';
import { Router } from '@angular/router';

@Component({
  providers: [LDRPageClaimsBookService, BackofficeConfigService],
  selector: 'ldr-page-book-list-claims',
  templateUrl: './page-book-list.component.html'
})

export class LDRBookActionsComponent  implements OnInit {

  claims: PageClaimsBook[] = [] ;
  claims_active: PageClaimsBook[] = [] ;
  pending_claims: PageClaimsBook[] = [] ;
  derivated_claims: PageClaimsBook[] = [] ;
  derivated_legal_area: PageClaimsBook[] = [] ;
  resolved_claims: PageClaimsBook[] = [] ;
  dismissed_claims: PageClaimsBook[] = [] ;
  finalized_claims: PageClaimsBook[] = [] ;
  error_message: string;
  state: number = this.config.LDR_STATE_PAGE_PENDING;
  title = 'Todos';
  load = false;
  filter: any;
  configFile: any;

  constructor(
    private config: BackofficeConfigService,
    private router: Router,
    private pageClaimsBookService: LDRPageClaimsBookService
  ) {
    this.configFile = this.config;
  }

  ngOnInit() {
    this.load = true;
    this.pageClaimsBookService.listPagesClaims(String(this.config.LDR_TYPE_USER_AD))
      .subscribe(
        (response) => this.assignPagesClaimsBook(response),
        err => console.log(err),
        () => {
          this.load = false;
          this.setState(this.state);
          }
      );
  }

  assignPagesClaimsBook(pages: PageClaimsBook[]) {
    this.claims = pages;
    this.claims_active = pages;
    this.pending_claims = this.claims.filter(
      item => +item.page_state === this.config.LDR_STATE_PAGE_PENDING);

    this.derivated_claims = this.claims.filter(
      item => (+item.page_state === this.config.LDR_STATE_PAGE_DERIVATED));

    this.derivated_legal_area = this.claims.filter(
      item => (+item.page_state === this.config.LDR_STATE_PAGE_ANSWERED));

    this.dismissed_claims = this.claims.filter(
      item => +item.page_state === this.config.LDR_STATE_PAGE_DISMISSED);

    this.resolved_claims = this.claims.filter(
      item => +item.page_state === this.config.LDR_STATE_PAGE_RESOLVED);

    this.finalized_claims = this.claims.filter(
      item => +item.page_state === this.config.LDR_STATE_PAGE_FINALIZED);
  }

  setState(type: number )  {
    this.state = type;
    switch (type) {
      case(this.config.LDR_STATE_PAGE_PENDING):
        this.claims_active = this.pending_claims;
        this.title =  this.config._LDR_STATUS.filter( f => f.id === this.config.LDR_STATE_PAGE_PENDING )[0].complete;
        break;
      case(this.config.LDR_STATE_PAGE_DERIVATED):
        this.claims_active = this.derivated_claims;
        this.title =  this.config._LDR_STATUS.filter( f => f.id === this.config.LDR_STATE_PAGE_DERIVATED )[0].complete;
        break;
      case(this.config.LDR_STATE_PAGE_ANSWERED):
        this.claims_active = this.derivated_legal_area;
        this.title =  this.config._LDR_STATUS.filter( f => f.id === this.config.LDR_STATE_PAGE_ANSWERED )[0].complete;
        break;
      case(this.config.LDR_STATE_PAGE_DISMISSED):
        this.claims_active = this.dismissed_claims;
        this.title =  this.config._LDR_STATUS.filter( f => f.id === this.config.LDR_STATE_PAGE_DISMISSED )[0].complete;
        break;
      case(this.config.LDR_STATE_PAGE_RESOLVED):
        this.claims_active = this.resolved_claims;
        this.title =  this.config._LDR_STATUS.filter( f => f.id === this.config.LDR_STATE_PAGE_RESOLVED )[0].complete;
        break;
      case(this.config.LDR_STATE_FILE_FINALIZED):
        this.claims_active = this.finalized_claims;
        this.title =  this.config._LDR_STATUS.filter( f => f.id === this.config.LDR_STATE_FILE_FINALIZED )[0].complete;
        break;
      default:
        this.claims_active = this.claims;
        this.title = 'Todos';
      }
    }

  gotoPageDetail(pageId: string) {
    const link: string[] = [`backoffice/ldr-administracion/detalle/${this.state}/${pageId}`];
    this.router.navigate(link);
  }

  getNameOrigin(type: string): any {
    let name;
    name =  this.config._LDR_TYPE_ORIGIN_PAGE.filter( f => f.id === +type )[0].name;
    return name;
  }

  getDaysRemaining(dateCreate: string): number {
    const daysremaining = this.config.LDR_DAYS_COMPLETION_CLAIM;
    const second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
    const datecrea = +new Date(dateCreate);
    const today = +new Date();
    const timediff = today - datecrea;
    const days: number = Math.floor(timediff / day);
    const restant: number = +daysremaining - +days;
    return restant;
  }
}
