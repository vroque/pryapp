import { ActivatedRoute, Params } from '@angular/router';
import { BackofficeConfigService } from '../../config/config.service';
import { Component, OnInit } from '@angular/core';
import { DerivationArea } from '../../../shared/services/claimsbook/derivation-area';
import { LDRCollaboratorService } from '../../../shared/services/claimsbook/collaborator.service';
import { LDRDerivationAreaService } from '../../../shared/services/claimsbook/derivation-area.service';
import { LDRStaffInvolvedService } from '../../../shared/services/claimsbook/staff-involved.service';
import { Router } from '@angular/router';
import { StaffInvolved } from '../../../shared/services/claimsbook/staff-involved';
import { Employee } from '../../../shared/services/employee/employee';

@Component({
  providers: [BackofficeConfigService, LDRDerivationAreaService, LDRCollaboratorService, LDRStaffInvolvedService],
  selector: 'ldr-derivate-ia',
  templateUrl: '/derivate.component.html'
})

export class LDRDerivateIA implements OnInit {

  load = false;
  state: number;
  page_id: number;
  derivate_id: number;
  msg_error = '';
  note = '';
  derivate: DerivationArea = <DerivationArea>{};
  list_collaborators: Employee[] = [];
  list_collaborators_filtered: Employee[] = [];
  collaborator: Employee =  <Employee>{};

  constructor(
    private router: Router,
    private _route: ActivatedRoute,
    private config: BackofficeConfigService,
    private collaboratorService: LDRCollaboratorService,
    private derivationAreaService: LDRDerivationAreaService,
    private staffInvolvedService: LDRStaffInvolvedService
  ) {}

  ngOnInit() {
    this.load = true;
    this._route.params.forEach((params: Params) => {
      this.derivate_id = params.derivate_id;
    });

    this.getDetailsDerivation(details_derivation => {
      this.derivate = details_derivation;
      this.state = details_derivation.derivate_state;
      this.page_id = details_derivation.page_id;

      this.listCollaborators(list => {
        this.list_collaborators = list;
        this.load = false;
      });
    });
  }

  getDetailsDerivation(call_back_ok) {
    let derivate: DerivationArea = <DerivationArea>{};
    this.derivationAreaService.getDerivationArea(this.derivate_id)
    .subscribe(
      data => derivate = data,
      err => console.log(err),
      () => call_back_ok(derivate)
    );
  }

  listCollaborators(call_back_ok) {
    let collaborators: Employee[];
    this.collaboratorService.listCollaboratorsOfFuntionalUnity(this.derivate.derivate_funtional_unity)
      .subscribe(
        data => collaborators = data,
        err => {
          this.msg_error = 'Error, verifique datos';
          console.log(err);
        },
        () => call_back_ok(collaborators)
      );
  }

  filterCollaborators(event) {
    this.list_collaborators_filtered = [];
      for (let i = 0; i < this.list_collaborators.length; i++) {
        const collaboratorfil = this.list_collaborators[i];
        if (collaboratorfil.nombre.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
          this.list_collaborators_filtered.push(collaboratorfil);
      }
    }
  }

  saveAsigmentCollaborator(){
    this.load = true;
    this.note = this.note.trim();
    if (this.collaborator == null) {
      this.msg_error = 'Seleccióne un colaborador.';
      this.load = false;
    } else if (
      this.collaborator.employee_pidm == null ||
      this.collaborator.employee_pidm === undefined ||
      this.collaborator.employee_pidm.toString().length <= 0) {
      this.msg_error = 'Seleccióne un colaborador.';
      this.load = false;
    } else if (this.note.length <= 0 ) {
      this.msg_error = 'Escriba nota';
      this.load = false;
    } else {
      this.msg_error = '';
      const staffInvolved: StaffInvolved =  <StaffInvolved>{};
      staffInvolved.page_id = +this.page_id;
      staffInvolved.derivate_id = +this.derivate_id;
      staffInvolved.staff_involved_pidm = this.collaborator.employee_pidm.toString();
      staffInvolved.staff_position_company = this.collaborator.puesto_organica;
      staffInvolved.staff_involved_user_id =  this.collaborator.employee_user_id.trim();
      staffInvolved.staff_involved_note = this.note.trim();
      staffInvolved.staff_involved_type = String(this.config.LDR_TYPE_INVOLVED_COLLABORATOR);
      this.staffInvolvedService.save(staffInvolved)
        .subscribe(
          data => data,
          err => console.log(err),
          () => {
            this.load = false;
            this.returnDetailsPage();
          }
        );
    }
  }

  returnDetailsPage() {
    const link: string[] = [`backoffice/ldr-area-legal/detalle/${this.state}/${this.page_id}/${this.derivate_id}`];
    this.router.navigate(link);
  }

}
