import { ActivatedRoute, Params } from '@angular/router';
import { AnswerPage } from '../../../shared/services/claimsbook/answer-page';
import { BackofficeConfigService } from '../../config/config.service';
import { Component, OnInit } from '@angular/core';
import { DerivationArea } from '../../../shared/services/claimsbook/derivation-area';
import { LDRAnswerPageService } from '../../../shared/services/claimsbook/answer-page.service';
import { LDRDerivationAreaService } from '../../../shared/services/claimsbook/derivation-area.service';
import { LDRPageClaimsBookService } from '../../../shared/services/claimsbook/page-claims-book.service';
import { LDRStaffInvolvedService } from '../../../shared/services/claimsbook/staff-involved.service';
import { PageClaimsBook } from '../../../shared/services/claimsbook/page-claims-book';
import { Router } from '@angular/router';
import { StaffInvolved } from '../../../shared/services/claimsbook/staff-involved';

@Component({
  providers: [BackofficeConfigService, LDRPageClaimsBookService, LDRDerivationAreaService, LDRStaffInvolvedService, LDRAnswerPageService],
  selector: 'ldr-details-page-claim-la',
  templateUrl: './details-page-claim.component.html'
})

export class LDRDetailsPageClaimLA  implements OnInit {

  load = false;
  error_message: string;
  page_id: number;
  derivate_id: number;
  state: number;
  page_claims_book: PageClaimsBook =  <PageClaimsBook>{};
  derivate: DerivationArea;
  staff_ivolved: StaffInvolved;

  constructor(
    private config: BackofficeConfigService,
    private router: Router,
    private _route: ActivatedRoute,
    private PageClaimsBookService:LDRPageClaimsBookService,
    private DerivationAreaService: LDRDerivationAreaService,
    private StaffInvolvedService: LDRStaffInvolvedService,
    private LDRAnswerPageService: LDRAnswerPageService,
  ) {}

  ngOnInit() {
    this.load = true;
    this._route.params.forEach((params: Params) => {
      this.page_id = +params.page_id;
      this.derivate_id = +params.derivate_id;
    });

    this.getDetailsDerivation(details_derivation => {
      this.derivate = details_derivation;
      this.state = +details_derivation.derivate_state;
      this.page_id = +details_derivation.page_id;

      this.getDetailsStaffInvolved(details_staff => {
        this.staff_ivolved = details_staff;
        this.load = false;
      });
    });
  }




  getDetailsDerivation(call_back_ok) {
    let derivate: DerivationArea = <DerivationArea>{};
    this.DerivationAreaService.getDerivationArea(this.derivate_id)
    .subscribe(
      data => derivate = data,
      err => console.log(err),
      () => call_back_ok(derivate)
    );
  }


  getAnswersDerivate(call_back_ok) {
    let answerPage: AnswerPage[] = [];
    this.LDRAnswerPageService.getAnswersByDerivate(this.page_id, this.derivate_id)
    .subscribe(
      data => answerPage = data,
      err => console.log(err),
      () => call_back_ok(answerPage)
    );
  }

  getAnswer(answerId: number, call_back_ok) {
    let answerPage: AnswerPage;
    this.LDRAnswerPageService.getAnswer(answerId)
    .subscribe(
      data => answerPage = data,
      err => console.log(err),
      () => call_back_ok(answerPage)
    );
  }




  getDetailsStaffInvolved(call_back_ok) {
    let staffInvolved: StaffInvolved;
    this.StaffInvolvedService.getStaffInvolvedByDerivate(this.page_id, this.derivate_id)
    .subscribe(
      data => staffInvolved = data,
      err => console.log(err),
      () => call_back_ok(staffInvolved)
    );
  }


  acceptAnswerDerivate(answerId: number) {
    this.load = true;
    this.getAnswer(answerId, answer_page_accept => {
      if (answer_page_accept === null) {
        this.load = false;
      } else if ( answer_page_accept.answer_state !== this.config.LDR_STATE_ANSWER_WAIT) {
        this.load = false;
      } else {
        answer_page_accept.answer_state = String(this.config.LDR_STATE_ANSWER_ACCEPTED);
        this.LDRAnswerPageService.updateAnswer(answer_page_accept)
        .subscribe(
          data => data,
          err => console.log(err),
          () => {
            this.state = this.config.LDR_STATE_FILE_FINALIZED;
            this.load = false;
            this.returnPageDetails();
          }
        );
      }
    });
  }

  resolvedPageClaim() {
    const link: string[] = [`backoffice/ldr-area-legal/resolver/${this.state}/${this.page_id}/${this.derivate_id}`];
    this.router.navigate(link);
  }

  createDerivation() {
    const link: string[] = [`backoffice/ldr-area-legal/derivacion/${this.state}/${this.page_id}/${this.derivate_id}`];
    this.router.navigate(link);
  }


  returnPageDetails() {
    const link: string[] = [`backoffice/ldr-area-legal/libro`];
    this.router.navigate(link);
  }

  setStatePage(event) {
    this.state = +event.page_state;
    this.load = false;
  }

}
