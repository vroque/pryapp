import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Params }   from "@angular/router";
import { Router }   from "@angular/router";
import { BackofficeConfigService } from "../../config/config.service";

import { LDRDerivationAreaService } from "../../../shared/services/claimsbook/derivation-area.service";
import { DerivationArea } from "../../../shared/services/claimsbook/derivation-area";

import { LDRAnswerPageService } from "../../../shared/services/claimsbook/answer-page.service";
import { AnswerPage } from "../../../shared/services/claimsbook/answer-page";


import { LDRUploadFiles } from "../upload-files.component";

@Component({
  selector: "ldr-resolved-page-claim-la",
  templateUrl: "./resolved-page-claim.component.html",
  providers: [BackofficeConfigService, LDRAnswerPageService, LDRDerivationAreaService],
})

export class LDRResolvedPageClaimLA  implements OnInit {
  load: boolean = false;
  msg_error: string = "";
  state: number;
  page_id: number;
  derivate_id: number;

  answer: string = "";

  file_validate: boolean;
  @ViewChild('uploader') uploadFilesComponent: LDRUploadFiles;

  constructor(
    private config: BackofficeConfigService,
    private router: Router,
    private _route: ActivatedRoute,
    private LDRAnswerPageService: LDRAnswerPageService,
    private DerivationAreaService: LDRDerivationAreaService,
  ){}
  ngOnInit(): void{
    this._route.params.forEach((params: Params) => {
      this.state = +params.state;
      this.page_id = +params.page_id;
      this.derivate_id = +params.derivate_id
    });
  }



  getDetailsDerivation(call_back_ok){
    let derivate: DerivationArea = <DerivationArea>{};
    this.DerivationAreaService.getDerivationArea(this.derivate_id)
    .subscribe(
      data => derivate = data,
      err => console.log(err),
      () => call_back_ok(derivate)
    )
  }

  answerPage(){
    this.load = true;
    this.getDetailsDerivation( details_derivation =>{
      let answer_page: AnswerPage = <AnswerPage>{};
      answer_page.page_id = +this.page_id;
      answer_page.answer_note = this.answer.trim();
      answer_page.answer_state = String(this.config.LDR_STATE_ANSWER_WAIT);
      answer_page.derivate_id = details_derivation.derivate_id;

      if(String(answer_page.page_id).length <= 0){
        this.msg_error = "Error al obterner código de reclamo."
        this.load = false;
      }
      else if(answer_page.answer_note.length <= 0){
        this.msg_error = "La respuesta no puede quedar Vacia."
        this.load = false;
      }
      else if(answer_page.answer_state.length <= 0){
        this.msg_error = "Estado no definido."
        this.load = false;
      }
      else if ( this.file_validate != true) {
        this.msg_error = "Seleccione archivos.";
        this.load = false;
      }
      else{
        this.msg_error = "";
        this.saveAnswer(answer_page);
      }
    })

  }

  saveAnswer(answer_page: AnswerPage){
    this.LDRAnswerPageService.saveAnswer(answer_page)
    .subscribe(
      data =>this.saveFile(data.answer_id),
      err => console.log(err),
      () => this.state = this.config.LDR_STATE_PAGE_RESOLVED
    )
  }

  saveFile(answer_id: string){
    this.uploadFilesComponent.saveFile(String(this.page_id),"XXX",answer_id, String(this.config.LDR_STATE_FILE_RESOLVED))
  }

  finalizedFiles(event){
    if (event === false) {
        this.load = event;
        this.returnDetailsPage();
    }
  }
  returnDetailsPage(){
    // let link: Array<string> = [`backoffice/ldr-area-legal/detalle/${this.state}/${this.page_id}`];
    let link: Array<string> = [`backoffice/ldr-area-legal/libro`];
    this.router.navigate(link);
  }
}
