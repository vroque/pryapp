import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Menu } from '../shared/menu';
import { MenuService } from '../shared/menu.service';

@Component({
  providers: [MenuService],
  selector: 'sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
})

export class SidebarMenuComponent implements OnInit {
  error_message: string;
  menus: Menu[];

  constructor(private menuService: MenuService) {}
  ngOnInit() { this.query(); }
  query() {
    this.menus = (JSON.parse(JSON.stringify((window as any).choices)) as Menu[])
    .filter((f) => f.position === 'all' || f.position === 'side' );
    for (const menu of this.menus) {
      menu.smenus = menu.smenus.filter( (f) => f.position === 'all' || f.position === 'side') || [];
    }
  }
}
