import { BackofficeConfigService } from '../config/config.service';
import { Campus } from '../../shared/services/institutional/campus';
import { CampusService } from '../../shared/services/institutional/campus.service';
import { CategorizationService } from '../../shared/services/economic/categorization.service';
import { College } from '../../shared/services/academic/college';
import { CollegeService } from '../../shared/services/academic/college.service';
import { Component, OnInit } from '@angular/core';
import { FreshmanService } from '../../shared/services/academic/freshman.service';
import { SearchCategorizeService } from '../../shared/services/economic/searchCategorize.service';
import { Student } from '../../shared/services/academic/student';

/**
 * @module BUW
 * @name BUWCategorizationComponent
 * Componente para el proceso de categorizacion en bienestar universitario
 */
@Component({
  selector: 'buw-categorization',
  templateUrl: './categorization.component.html',
  providers: [
    CampusService,
    CollegeService,
    FreshmanService,
    CategorizationService,
    SearchCategorizeService,
    BackofficeConfigService,
  ],
})
export class BUWCategorizationComponent implements OnInit {

  error_message: string;
  doc_status = 'pre'; // pre, on, err
  inbox = 'cachimbos';
  colleges: College[] = [];
  campuses: Campus[] = [];
  freshmans: Student[] = [];
  categorizations: any[] = [];
  program_id: string;
  student: any = {code: ''};
  constructor(
    private campusService: CampusService,
    private collegeService: CollegeService,
    private freshmanService: FreshmanService,
    private categorizationService: CategorizationService,
    private searchCategorizeService: SearchCategorizeService,
    private config: BackofficeConfigService
  ) { }

  ngOnInit(): void {
    this.collegeService.query()
      .subscribe(
        (colleges) => this.colleges = colleges,
        (error) =>  this.error_message = error,
      );
    this.campusService.query()
      .subscribe(
        (campuses) => this.campuses = campuses,
        (error) =>  this.error_message = error,
      );
  }

  categorize(): void {
    this.next(0);
    // func
  }

  getPrograms(id: string): any[] {
    for (const col of this.colleges) {
      if (col.id === id) {
        return col.programs;
      }
    }
    return [];
  }

  searchStudents(programId: string, campusId: string): void {
    this.freshmanService.find('201710', programId, campusId)
      .subscribe(
        (freshmans) => this.freshmans = freshmans,
        (error) =>  this.error_message = <any>error
      );
  }

  setInbox(event: Event, type: string): void {
    this.inbox = type;
  }

  // categorization

  next(index: number): void {
    if (index < this.freshmans.length) {
      const mock: Student = this.freshmans[index];
      const data: any = {department: 'UREG', levl: 'PG', program: this.program_id};
      (mock as any).status = 'on';
      this.categorizationService.categorize(mock.person_id, data)
      .subscribe(
        (res) => {
          (mock as any).new_scala = res.category;
        },
        (error) =>  {
          (mock as any).status = 'error';
          this.error_message = error;
          this.next(index + 1);
        },
        () => {
          (mock as any).status = 'post';
          this.next(index + 1);
        },
      );
    }
  }

  searchCategorize(studentCode: number): void {
    this.searchCategorizeService.search(studentCode)
    .subscribe(
      (res) => this.categorizations = res,
      (error) => this.error_message = error
    );
  }
}
