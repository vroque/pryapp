import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/primeng';
import 'rxjs/add/operator/finally';

import { ActivityProgramming } from '../../shared/services/extracurricular-activities/activity-programming.model';
import { ActivityProgrammingService } from '../../shared/services/extracurricular-activities/activity-programming.service';
import { ActivityService } from '../../shared/services/extracurricular-activities/activity.service';
import { ActivityType } from '../../shared/services/extracurricular-activities/activity-type.model';
import { ActivityTypeService } from '../../shared/services/extracurricular-activities/activity-type.service';
import { BackofficeConfigService } from '../config/config.service';
import { Campus } from '../../shared/services/institutional/campus';
import { CampusService } from '../../shared/services/institutional/campus.service';
import { Department } from '../../shared/services/institutional/department';
import { DepartmentService } from '../../shared/services/institutional/department.service';
import { EnrolledProgramStudent } from '../../shared/services/extracurricular-activities/enrolled-program-student.model';
import { EnrolledStudentService } from '../../shared/services/extracurricular-activities/enrolled-student.service';
import { SelectedStudent } from '../../shared/services/extracurricular-activities/selected-student.model';
import { StudentSearch } from '../../shared/services/extracurricular-activities/student-search.model';
import { StudentSearchService } from '../../shared/services/extracurricular-activities/student-search.service';

@Component({
  selector: 'vuc-register-selected-student',
  templateUrl: './register-selected-student.component.html',
  providers: [ActivityTypeService, ActivityService, CampusService, ActivityProgrammingService,
    StudentSearchService, EnrolledStudentService, BackofficeConfigService, DepartmentService]
})

export class VUCRegisterSelectedStudentComponent implements OnInit {
  activities: ActivityProgramming[];                       // Lista de las programaciones
  activitiesTypes: ActivityType[] = [];                    // Tipos de actividades
  activityName: string;                                    // Campo de filtro para la búsqueda
  campus: Campus[];                                        // Campus
  enrolledProgramStudents: EnrolledProgramStudent[];       // Relación de estudiantes inscritos en una actividad
  foundStudents: StudentSearch[] = [];                     // Estudiantes que se han encontrado por búsqueda
  idActivityType: number;                                  // Tipo de actividad
  isShowStudentsList: boolean;                             // Muestra la lista de estudiantes ingresados
  idCampus: string;                                        // ID del campus (Ejem: S01)
  idModality: string;                                      // Tipo de modalidad
  isBusy: boolean;                                         // Estado que indica que el servicio esta ocupado
  modalities: Department[];                                // modalidad
  notFoundStudents: string[] = [];                         // Cadena de dnis que no se han encontrado
  primeMsg: Message[];                                     // Mensaje
  primeMsg1: Message[];                                    // Mensaje
  selectedStudent: SelectedStudent;                        // Modelo que indica a los estudiantes seleccionados
  students: string[];                                      // Array de DNI de los estudiantes a buscar
  studentsByDni: string;                                   // Input donde se ingresan todos los DNI
  isShowRegister: boolean;                                 // bandera para mostrar el formulario de registrar
  isShowSearch: boolean;                                   // bandera para mostrar/ocultar los resultados de la búsqueda de programación de actividades
  isShowReport: boolean;                                   // Bandera para mosrar/ocultar formulario de reporte
  currentIndex: number;                                    // Variable para guardar la numeración de index #
  nameActivity: string;
  isLoadingActivity: boolean;

  constructor(private activityTypeService: ActivityTypeService, private campusService: CampusService,
    private studentSearchService: StudentSearchService, private activityProgrammingService: ActivityProgrammingService,
    private enrolledProgramActivityService: EnrolledStudentService, private config: BackofficeConfigService,
    private departmentService: DepartmentService) { }

  ngOnInit(): void {
    this.getActivityTypes();
    this.getCampus();
    this.getModalityTypes();
    this.idCampus = ''; this.idActivityType = 0; this.activityName = ''; this.idModality = '';
    this.selectedStudent = { idProgramActivity: 0, pidmStudent: [] };
  }

  /**
  * Obtiene los tipos de actividades.
  */
  getActivityTypes(): void {
    this.isBusy = true;
    this.activityTypeService.getActivityTypes().finally(() => this.isBusy = false)
      .subscribe(
        (activitiesTypes) => this.activitiesTypes = activitiesTypes,
        (error) => console.log(error));
  }

  /**
  * Obtiene las modalidades.
  */
  getModalityTypes(): void {
    this.isBusy = true;
    this.departmentService.getDepartments().finally(() => this.isBusy = false)
      .subscribe(
        (modalities) => this.modalities = modalities,
        (error) => console.log(error));
  }

  /**
  * Obteniene los campus.
  */
  getCampus(): void {
    this.isBusy = true;
    this.campusService.query().finally(() => this.isBusy = false)
      .subscribe(
        (campus) => this.campus = campus,
        (error) => console.log(error));
  }

  /**
  * Busca las actividades que cumplan con los filtros elegidos.
  * - Tipo de actividad.
  * - Tipo de Modalidad.
  * - Campus.
  * - Nombre de la actividad (opcional).
  */
  findActivities(): void {
    this.isBusy = true;
    this.isShowSearch = false;
    if (this.idModality === 'UVIR') {
      this.idCampus = 'V00';
    }
    this.activityProgrammingService.getProgrammingAcivitiesBy(this.idActivityType, this.idModality, this.idCampus,
      this.activityName)
      .finally(() => this.isBusy = false)
      .subscribe(
        (activities) => {
          this.activities = activities;
          this.activities.forEach(element => {
            element.campus = this.campus.filter(x => x.id === this.idCampus)[0];
            element.department = this.modalities.filter(x => x.id === this.idModality)[0];
          });
        },
        (error) => console.log(error));
  }

  /**
  * Genera un array de string en base a los DNI ingresados.
  * Divide en base a los espacios entre DNI
  * @returns dni Cadena de dni's
  */
  splitDni(): void {
    if (this.studentsByDni === null || this.studentsByDni === '') { return; }
    this.students = this.studentsByDni.split(' ')
      .filter((estudianteDni) => estudianteDni !== '')
      .filter((estudianteDni, position, self) => self.indexOf(estudianteDni) === position);
  }

  /**
  * Validación en textbox solo numérico.
  * @param {any} event Evento.
  */
  keyPress(event: any): void {
    const pattern = /[0-9 ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      if (event.charCode !== 13) {
        event.preventDefault();
      }
    }
  }

  /**
  * Busca a los estudiantes inscritos al programa por DNIs.
  */
  searchStudents(): void {
    this.isBusy = true;
    this.studentSearchService.getStudentsByDni(this.students.filter(x => x.length === 8))
      .finally(() => this.isBusy = false)
      .subscribe(
        (estudiantelist) => {
          this.foundStudents = estudiantelist; this.students.forEach(student => {
            if (!this.foundStudents.some(x => x.dni === student)) {
              this.notFoundStudents.push(student);
            }
          });
        },
        (error) => console.log(error));
  }

  /**
  * Habilita el registro de los estudiantes seleccionados.
  * @param programActivity Modelo de programación de actividad.
  */
  register(programActivity: ActivityProgramming): void {
    this.isShowRegister = false;
    this.isShowSearch = true;
    this.selectedStudent.idProgramActivity = programActivity.id;
    this.nameActivity = programActivity.activity.shortName;
    this.selectedStudent.pidmStudent = [];
    this.enrolledProgramStudents = [];
  }

  /**
  * Actualiza a los estudiantes que fueron seleccionados a la actividad programa.
  */
  saveStudents(): void {
    this.isBusy = true;
    this.foundStudents.forEach((student) => {
      // Valida en la base de datos si existe el estudiante
      this.selectedStudent.pidmStudent.push(student.idPerson);
    });

    // Cambia el estado de "confirmed" a "True" de los estudiantes seleccionados
    this.enrolledProgramActivityService.updateEnrolledProgram(this.selectedStudent)
      .finally(() => this.isBusy = false)
      .subscribe(
        (result) => {
          if (result) {
            this.primeMsg = [{
              detail: 'Se ha registrado correctamente',
              severity: 'success',
              summary: ' Registro de estudiantes seleccionados',
            }];
            this.foundStudents = [];
            this.studentsByDni = '';
            this.activities = null;
            this.selectedStudent.idProgramActivity = 0;
            this.enrolledProgramStudents = null;
          } else {
            this.primeMsg = [{
              detail: 'Ha ocurrido un error inesperado, vuelva a intentarlo',
              severity: 'danger',
              summary: ' Registro de estudiantes seleccionados',
            }];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Busca los estudiantes inscritos al programa/taller.
  * @param {number} id Id del Programa.
  */
  getByEnrolledProgram(id: number, index: number): void {
    this.isBusy = true;
    this.isShowSearch = true;
    this.isShowReport = false;
    this.currentIndex = index;
    this.selectedStudent.idProgramActivity = 0;
    this.enrolledProgramActivityService.getByEnrolledProgram(id, this.idActivityType)
      .finally(() => this.isBusy = false)
      .subscribe(
        (enrolledProgramStudents) => {
          this.enrolledProgramStudents = enrolledProgramStudents;
          this.enrolledProgramStudents.forEach(element => {
            element.campus = this.campus.filter(x => x.id === this.idCampus)[0];
          });
          if (this.enrolledProgramStudents.length === 0 || this.enrolledProgramStudents === undefined
            || enrolledProgramStudents === [] || enrolledProgramStudents === null) {
            this.isShowSearch = false;
            this.isShowReport = true;
            this.primeMsg1 = [
              {
                detail: 'No existen inscritos en esta actividad.',
                severity: 'warn',
                summary: 'Reporte de seleccionados'
              }
            ];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Formato para descargar en Excel.
  */
  downloadStudentsInformation(): void {
    const reportType = this.enrolledProgramStudents[0].activityType;
    const datos: any[] = this.enrolledProgramStudents.map(
      (dato) => {
        return {
          'Tipo de Actividad': dato.activityType,
          'Nombre de la actividad': dato.name,
          'Periodo Programación': dato.startPeriod,
          'Fecha de inicio y fin de actividad': `${(new Date(dato.startDateActivity))
          .toLocaleDateString()} - ${(new Date(dato.endDateActivity)).toLocaleDateString()}`,
          'DNI': dato.documentNumber,
          'Apellidos y Nombres': dato.fullName,
          'Celular': dato.phone,
          'Ciclo': dato.cycle,
          'Correo': `${dato.documentNumber}@continental.edu.pe`,
          'Plan de Estudios': dato.curriculum,
          'Estado': dato.activityType === 'Programa' ?
            (dato.confirmed ? 'Seleccionado'
              : (dato.dateConfirmed === null ? 'Pre Inscrito' : 'No Seleccionado')) : 'Matriculado'
        };
      });
    this.config.exportAsExcelFile(datos, `Reporte de actividades - ${reportType}`);
  }

  /**
  * Limpiar la búsqueda.
  * @param {NgForm} form Formulario de búsqueda.
  */
  cleanSearch(): void {
    this.enrolledProgramStudents = null;
    this.foundStudents = [];
    this.notFoundStudents = [];
    this.students = [];
    this.studentsByDni = null;

    // Del registro de seleccionados limpiar la data que pueda existir
    this.selectedStudent.pidmStudent = [];
    this.enrolledProgramStudents = null;
  }

  /**
    * Método para limpiar el combobox de Campus por tipo de actividad seleccionado.
  */
  cleanCampus(): void {
    if (this.idModality === 'UVIR') {
      this.idCampus = 'V00';
    } else {
      this.idCampus = '';
    }
  }

  /**
   * Limpian el filtro de busqueda
   */
  clean(): void {
    this.cleanSearch();
    this.activities = null;
    this.idActivityType = 0;
    this.idCampus = '';
    this.idModality = '';
    this.activityName = '';
  }

  /**
   * Regresa al formulario de busqueda
  */
  back(): void {
    this.isShowSearch = false;
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList(): void {
    this.isShowStudentsList = !this.isShowStudentsList;
  }
}
