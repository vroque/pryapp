import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/operator/finally';
import { Message } from 'primeng/primeng';

import { Activity } from '../../shared/services/extracurricular-activities/activity.model';
import { ActivityService } from '../../shared/services/extracurricular-activities/activity.service';
import { ActivityType } from '../../shared/services/extracurricular-activities/activity-type.model';
import { ActivityTypeService } from '../../shared/services/extracurricular-activities/activity-type.service';
import { Axi } from '../../shared/services/extracurricular-activities/axi.model';
import { AxiService } from '../../shared/services/extracurricular-activities/axi.service';
import { BackofficeConfigService } from '../config/config.service';
import { ConfigService } from '../../config/config.service';
import { ConvalidationType } from '../../shared/services/extracurricular-activities/convalidation-type.model';
import { ConvalidationTypeService } from '../../shared/services/extracurricular-activities/convalidation-type.service';

@Component({
  providers: [AxiService, ActivityService, ActivityTypeService, ConvalidationTypeService, ConfigService,
    BackofficeConfigService],
  selector: 'vuc-management',
  templateUrl: './management.component.html'
})

export class VUCManagementComponent implements OnInit {
  // Variables para traer a los modelos.
  activitiesType: ActivityType;            // Variable que trae al modelo de tipos de actividades.
  activitiesTypes: ActivityType[];         // Lista de tipos de actividades.
  activity: Activity;                      // Modelo de actividades.
  activityTmp: Activity;                   // variable temporal para las actividades
  axi: Axi;                                // Modelo de ejes.
  axis: Axi[];                             // Lista de ejes.
  axiTmp: Axi;                             // variable temporal para los ejes.
  convalidationType: ConvalidationType = {
    id: 0, name: ''
  };                                       // Variable que trae al modelo de tipo de convalidaciones.
  convalidationTypes: ConvalidationType[]; // Lista de tipo de convalidaciones.

  // Variables de estado.
  change: boolean;                         // Variable para manejar si existe algun cambio en el formulario.
  isBusy: boolean;                         // Estado que indica que el servicio esta ocupado.
  showCredits: boolean;                    // Estado que permite aparecer/desaparecer el combobox(cantidad de creditos).
  showFormActivity: boolean;               // Variable para manejar los estados del formulario de actividades.
  showFormAxi: boolean;                    // Variable para manejar los estados del formulario de ejes.

  // Variables mensajes.
  activityForm: FormGroup;                 // Variable para manejar las validaciones de las actividades.
  axiForm: FormGroup;                      // Variable para manejar las validaciones de los ejes.
  errorMessage: string;                    // Mensaje de error.
  imageEncode: any;                        // Variable para manejar las imágenes.
  message: string;                         // Variable de mensaje.
  primeMsg: Message[];                     // Mensaje.
  nameActivity: string;                    // Variable para guardar el nombre de actividad

  // Variable para seleccionar una convalidación.
  selectedConvalidation: number[] = [];

  // Variable para poner valores en el combobox (1) y (2).
  quantityCredits: any[] = [
    { value: 0, description: '-- Seleccione una cantidad de créditos --' },
    { value: 1, description: '1' },
    { value: 2, description: '2' }
  ];

  // Validación para los campos del formulario Axi.
  axiValidation = {
    name: {
      required: 'El nombre del eje es requerido',
      minlength: 'El nombre del eje debe de contener por lo menos 3 caracteres',
      maxlength: 'El nombre del eje debe de contener como máximo 50 carateres',
    },
    description: {
      required: 'La descripción del eje es requerida',
      minlength: 'La descripción del eje debe de contener por lo menos 10 caracteres',
      maxlength: 'La descripción del eje debe de contener como máximo 300 caracteres'
    },
    imageEncode: {
      required: 'La imagen del eje es requerida'
    }
  };

  // Validación para los campos del formulario Activity.
  activityValidation = {
    idActivityType: {
      required: 'El tipo de actividad es requerida'
    },
    shortName: {
      required: 'El nombre corto de la actividad es requerido',
      minlength: 'El nombre corto de la actividad debe de contener por lo menos 3 caracteres',
      maxlength: 'El nombre corto de la actividad debe de contener por los menos 30 caracteres',
      existName: 'El nombre corto de la actividad ya existe'
    },
    name: {
      required: 'El nombre de la actividad es requerido',
      minlength: 'El nombre de la actividad debe de contener por lo menos 3 caracteres',
      maxlength: 'El nombre de la actividad debe de contener como máximo 200 caracteres',
      existName: 'El nombre de la actividad ya existe'
    },
    description: {
      required: 'La descripción de la actividad es requerida',
      minlength: 'La descripción de la actividad debe de contener por lo menos 10 caracteres',
      maxlength: 'La descripción de la actividad debe de contener como máximo 300 caracteres'
    },
    other: {
      minlength: 'La descripción de la actividad debe de contener por lo menos 10 caracteres',
      maxlength: 'La descripción de la actividad debe de contener como máximo 300 caracteres'
    },
    convalidationTypes: {
      required: 'Este campo es requerida',
    },
    credits: {
      required: 'Este campo es requerida',
    }
  };

  constructor(private axiService: AxiService, private activityService: ActivityService,
    private activityType: ActivityTypeService, private convalidationTypeService: ConvalidationTypeService) { }

  ngOnInit(): void {
    this.clean();
    this.getAxis();
    this.getActivitiesTypes();
    this.getConvalidationsTypes();

    this.axiForm = new FormGroup({
      name: new FormControl('',
        [
          Validators.required,
          Validators.maxLength(50),
          Validators.minLength(3)
        ]),
      description: new FormControl('',
        [
          Validators.required,
          Validators.maxLength(300),
          Validators.minLength(10)
        ])
    });

    this.activityForm = new FormGroup({
      idActivityType: new FormControl('',
        [
          Validators.required,
        ]),
      shortName: new FormControl('',
        [
          Validators.required,
          Validators.maxLength(30),
          Validators.minLength(3)
        ]),
      name: new FormControl('',
        [
          Validators.required,
          Validators.maxLength(200),
          Validators.minLength(3)
        ]),
      description: new FormControl('',
        [
          Validators.required,
          Validators.maxLength(1000),
          Validators.minLength(10)
        ]),
      other: new FormControl('', [
        Validators.maxLength(500),
        Validators.minLength(10)
      ]),
      credits: new FormControl('',
        [
          Validators.required,
        ])
    });
  }

  /**
  * Método para verificar si esque se realiza algún cambio en el formulario Axi.
  */
  verifyChange(): void {
    if (JSON.stringify(this.axiTmp) === JSON.stringify(this.axi)) {
      this.change = false;
    } else {
      this.change = true;
    }
  }

  /**
  * Método para obtener ejes.
  */
  getAxis(): void {
    this.isBusy = true;
    this.axiService.getAxis().finally(() => this.isBusy = false)
      .subscribe(
        (axis) => this.axis = axis,
        (error) => console.log(error));
  }

  /**
  * Método para obtener ejes por su nombre.
  */
  getAxiByName(): void {
    if (this.axi.name === null || this.axi.name === undefined || this.axi.name.length < 3
      || this.axi.name.length > 50) {
      return;
    }
    if ((this.axi.name === this.axiTmp.name) && this.axi.id > 0) {
      return;
    }
    // Método para verificar si esque ya se registro un eje con el mismo nombre
    this.axiService.getByAxiName(this.axi.name).finally(() => this.isBusy = false)
      .subscribe(
        (axi) => {
          if (axi === null || axi === undefined) {
            this.axiForm.controls['name'].setErrors(null);
          } else {
            this.axiForm.controls['name'].setErrors({ 'existName': true });
          }
        },
        (error) => console.log(error));
  }

  /**
  * Método de estado para crear un eje.
  */
  newAxi(): void {
    this.axiForm.reset();
    this.cleanAxi();
    this.cleanMessages();
    this.showFormActivity = false;
    this.showFormAxi = true;
  }

  /**
  * Método para crear un eje.
  */
  createAxi(): void {
    this.isBusy = true;
    this.axiService.createAxi(this.axi).finally(() => this.isBusy = false)
      .subscribe(
        (axi) => {
          if (axi !== null && axi !== undefined) {
            this.primeMsg = [{
              detail: 'Se ha creado el eje correctamente',
              severity: 'success',
              summary: 'Registro de eje',
            }];
            this.showFormAxi = false;
            this.getAxis();
          } else {
            this.primeMsg = [{
              detail: 'Ha ocurrido un problema al intentar crear el eje',
              severity: 'danger',
              summary: 'Registro de eje',
            }];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Método para editar un eje.
  * @param {number} id Id del eje.
  */
  editAxi(id: number): void {
    this.axiForm.reset();
    this.cleanMessages();
    this.change = false;
    this.isBusy = true;
    this.showFormAxi = true;
    this.axiService.getByAxiId(id)
      .finally(() => this.isBusy = false)
      .subscribe(
        (axi) => { this.axi = axi; this.axiTmp = JSON.parse(JSON.stringify(axi)); },
        (error) => console.log(error));
  }

  /**
  * Método para actualizar un eje, una vez que se haya llevado los datos al formulario
  */
  updateAxi(): void {
    this.change = false;
    this.isBusy = true;
    this.axiService.updateAxi(this.axi)
      .finally(() => this.isBusy = false)
      .subscribe(
        (axi) => {
          if (axi !== null && axi !== undefined) {
            this.primeMsg = [{
              detail: 'Se ha actualizado el eje correctamente',
              severity: 'success',
              summary: 'Actualización de eje',
            }];
            this.getAxis();
            this.showFormAxi = false;
          } else {
            this.primeMsg = [{
              detail: 'Ha ocurrido un problema al actualizar el eje',
              severity: 'danger',
              summary: 'Actualización de eje',
            }];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Método para eliminar un eje por id.
  * @param {number} id Id del eje.
  */
  deleteAxi(id: number): void {
    if (!confirm('¿Esta seguro que desea eliminar el eje?')) { return; }
    this.axiService.deleteAxi(id)
      .finally(() => this.isBusy = false)
      .subscribe(
        (axi) => {
          if (axi !== null && axi !== undefined) {
            this.primeMsg = [{
              detail: 'Se ha eliminado el eje correctamente',
              severity: 'success',
              summary: 'Eliminación de eje',
            }];
            this.getAxis();
            this.showFormAxi = false;
          } else {
            this.primeMsg = [{
              detail: 'Ha ocurrido un problema al tratar de eliminar el eje',
              severity: 'danger',
              summary: 'Eliminación de eje',
            }];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Método que convierte una imagen a base 64 que sube el usuario.
  * @param {File[]} files Files
  */
  onChange(files: File[]): void {
    const image = files[0];
    if (image === undefined || image == null) {
      this.imageEncode = null;
      this.change = false;
      return;
    }
    if (image.type.toLowerCase() === 'image/png' || image.type.toLowerCase() === 'image/jpg' ||
      image.type.toLowerCase() === 'image/jpeg') {
      const myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        this.imageEncode = myReader.result;
        this.axi.imageEncode = this.imageEncode.toString().replace('data:image/jpeg;base64,', '')
          .replace('data:image/jpg;base64,', '').replace('data:image/png;base64,', '');
        if (this.axi.id > 0) {
          this.verifyChange();
        }
      };
      myReader.readAsDataURL(image);
      this.cleanMessages();
    } else {
      this.errorMessage = `El formato ${image.type} no es un formato aceptado para el banner`;
      this.imageEncode = null;
      return;
    }
  }

  /**
  * Método para verificar si el tipo de convalidación existe en los Id del tipo de convalidaciones de la actividad.
  * @param {Activity} activity Modelo de la actividad.
  * @param {number} id Id del tipo de convalidación.
  */
  check(activity: Activity, id: number): boolean {
    if (activity.convalidationTypesId.some(x => x === id)) {
      return true;
    }
    return false;
  }

  /**
  * Método que agrega o remueve un tipo de convalidación del modelo de actividad.
  * @param {number} id Id del tipo de convalidación.
  */
  selectConvalidation(id: number): void {
    const idConvalidationType = this.activity.convalidationTypesId.filter(x => x === id);
    // devuelve un elemento mayor a 0
    if (idConvalidationType.length > 0) {
      // se devuelve el elemento de la matriz
      const index = this.activity.convalidationTypesId.indexOf(idConvalidationType[0]);
      this.activity.convalidationTypesId.splice(index, 1);
    } else {
      // Devuelve la longitud de la matriz
      this.activity.convalidationTypesId.push(id);
    }
    this.checkConvalidation();
    this.verifyChangeActivity();
  }

  /**
  * Método para verificar si se escoge el tipo de convalidación 1 o 2, y de acuerdo a ello se muestra en el select
  * los créditos que le corresponden
  */
  private checkConvalidation(): void {
    if (this.activity.convalidationTypesId.some(x => x === 2) && this.activity.convalidationTypesId.length === 1) {
      this.showCredits = !this.showCredits;
    } else {
      this.showCredits = false;
    }
  }

  /**
  * Método para verificar si existe algún cambio en el formulario de actividades
  */
  verifyChangeActivity(): void {
    if (JSON.stringify(this.activityTmp) === JSON.stringify(this.activity)) {
      this.change = false;
    } else {
      this.change = true;
    }

    if (this.activity.convalidationTypesId.some(x => x === 1) && this.activity.credits <= 0) {
      this.activityForm.controls['credits'].setErrors({ 'setCredits': true });
    } else {
      this.activityForm.controls['credits'].setErrors(null);
    }
  }

  /**
  * Método para obtener los tipos de actividades.
  */
  getActivitiesTypes(): void {
    this.activityType.getActivityTypes()
      .finally(() => this.isBusy = false)
      .subscribe(
        (activitiesTypes) => this.activitiesTypes = activitiesTypes,
        (error) => console.log(error));
  }

  /**
  * Método para obtener una actividad por su nombre.
  */
  getActivityByName(): void {
    if (this.activity.name === null || this.activity.name === undefined || this.activity.name.length < 3
      || this.activity.name.length > 200) {
      return;
    }
    if ((this.activity.name === this.activityTmp.name) && this.activity.id > 0) {
      return;
    }
    // Método para verificar si esque ya se registro una actividad con el mismo nombre.
    this.activityService.getByActivityName(this.activity.name)
      .subscribe(
        (activity) => {
          if (activity === null || activity === undefined) {
            this.activityForm.controls['name'].setErrors(null);
          } else {
            this.activityForm.controls['name'].setErrors({ 'existName': true });
          }
        },
        (error) => console.log(error));
  }

  /**
  * Método para obtener una actividad por el nombre corto
  */
  getActivityByShortName(): void {
    if (this.activity.shortName === null || this.activity.shortName === undefined || this.activity.shortName.length < 3
      || this.activity.shortName.length > 30) {
      return;
    }
    if ((this.activity.shortName === this.activityTmp.shortName) && this.activity.id > 0) {
      return;
    }
    // Método para verificar si esque ya se registro una actividad con el mismo nombre.
    this.activityService.getByActivityShortName(this.activity.shortName)
      .subscribe(
        (activity) => {
          if (activity === null || activity === undefined) {
            this.activityForm.controls['shortName'].setErrors(null);
          } else {
            this.activityForm.controls['shortName'].setErrors({ 'existName': true });
          }
        },
        (error) => console.log(error));
  }

  /**
  * Método para obtener los tipo de convalidaciones.
  */
  getConvalidationsTypes(): void {
    this.convalidationTypeService.get().finally(() => this.isBusy = false)
      .subscribe(
        (convalidationsTypes) => this.convalidationTypes = convalidationsTypes,
        (error) => console.log(error));
  }

  /**
  * Método de estado para crear una actividad.
  * @param {number} idAxi Id de eje.
  */
  newActivity(idAxi: number): void {
    this.activityForm.reset();
    this.cleanMessages();
    this.cleanActivity();
    this.activity.idAxi = idAxi;
    this.showFormActivity = true;
    this.showFormAxi = false;
  }

  /**
  * Método para desactivar una actividad mediante el id de la actividad.
  * @param {number} id Id de la actividad.
  */
  desactivateActivity(id: number): void {
    this.activityService.desactivateActivity(id)
      .finally(() => this.isBusy = false)
      .subscribe(
        (msge) => this.getAxis(),
        (error) => console.log(error));
  }

  /**
  * Método para crear una nueva actividad.
  */
  createActivity(): void {
    this.isBusy = true;
    this.activityService.createActivity(this.activity)
      .finally(() => this.isBusy = false)
      .subscribe(
        (activity) => {
          if (activity !== null && activity !== undefined) {
            this.primeMsg = [{
              detail: 'Se creo la actividad correctamente',
              severity: 'success',
              summary: 'Registro de actividad',
            }];
            this.getAxis();
            this.showFormActivity = false;
          } else {
            this.primeMsg = [{
              detail: 'Ha ocurrido un problema al intentar crear la actividad',
              severity: 'success',
              summary: 'Registro de actividad',
            }];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Método para enviar los datos de una actividad registrada al formulario de actividad.
  * @param {number} id Id de la actividad.
  */
  editActivity(id: number): void {
    this.activityForm.reset();
    this.cleanActivity();
    this.cleanMessages();
    this.change = false;
    this.isBusy = true;
    this.showFormActivity = true;
    this.nameActivity = '';
    this.activityService.getByActivityId(id)
      .finally(() => this.isBusy = false)
      .subscribe(
        (activity) => {
          this.nameActivity = activity.shortName;
          activity.convalidationTypesId = []; // Se asigna un array vacio para evitar que sea null
          activity.convalidationTypes.forEach(convalidationType => {
            activity.convalidationTypesId.push(convalidationType.id);
          });
          this.activity = activity;
          this.activityTmp = JSON.parse(JSON.stringify(activity));
          this.checkConvalidation();
        },
        (error) => console.log(error));
  }

  /**
  * Método para actualizar una actividad
  */
  updateActivity(): void {
    this.isBusy = true;
    this.change = false;
    this.activityService.updateActivity(this.activity).finally(() => this.isBusy = false)
      .subscribe(
        (activity) => {
          if (activity !== null && activity !== undefined) {
            this.primeMsg = [{
              detail: 'Se actualizó la actividad correctamente',
              severity: 'success',
              summary: 'Actualización de actividad',
            }];
            this.showFormActivity = false;
            this.getAxis();
          } else {
            this.primeMsg = [{
              detail: 'Ha ocurrido un problema al intentar actualizar la actividad',
              severity: 'success',
              summary: 'Actualización de actividad',
            }];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Método para eliminar una actividad mediante el id de dicha actividad.
  * @param {number} id Id de la actividad.
  */
  deleteActivity(id: number): void {
    if (!confirm('¿Estas seguro que desea eliminar la actividad?')) { return; }
    this.activityService.deleteActivity(id)
      .finally(() => this.isBusy = false)
      .subscribe(
        (msge) => {
          if (msge !== null || msge !== undefined) {
            this.primeMsg = [{
              detail: 'Se elimino la actividad correctamente',
              severity: 'success',
              summary: 'Se elimino la actividad',
            }];
            this.getAxis();
          } else {
            this.primeMsg = [{
              detail: 'Ha ocurrido un problema al intentar eliminar la actividad actividad',
              severity: 'warn',
              summary: 'Error',
            }];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Método que convierte una imagen a base 64 que sube el usuario.
  * @param {File[]} files Files.
  */
  onChangeActivity(files: File[]): void {
    const image = files[0];
    if (image === undefined || image === null) {
      this.imageEncode = null;
      return;
    }
    if (image.type.toLowerCase() === 'image/png' || image.type.toLowerCase() === 'image/jpg' ||
      image.type.toLowerCase() === 'image/jpeg') {
      const myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        this.imageEncode = myReader.result;
        this.activity.imageEncode = this.imageEncode.toString().replace('data:image/jpeg;base64,', '')
          .replace('data:image/jpg;base64,', '').replace('data:image/png;base64,', '');
        if (this.activity.id > 0) {
          this.verifyChangeActivity();
        }
      };
      myReader.readAsDataURL(image);
      this.cleanMessages();
    } else {
      this.errorMessage = `El archivo seleccionado no es un formato aceptado para la imagen`;
      this.imageEncode = null;
      return;
    }
  }

  /**
   * Limpia el input cuando selecciona proyección social
   */
  resetCredit(idConvalidationType: number): void {
    if (idConvalidationType === 2) {
      this.activity.credits = 0;
    }
  }

  /**
  * Método para cancelar y regresar al formulario principal de administracion de ejes y actividades.
  */
  cancel(): void {
    this.activityForm.reset();
    this.axiForm.reset();
    this.cleanMessages();
    this.showFormActivity = false;
    this.showFormAxi = false;
    this.showCredits = false;
    this.nameActivity = '';
  }

  /**
  * Método para limpiar los modelos.
  */
  clean(): void {
    this.cleanAxi();
    this.cleanActivity();
    this.cleanMessages();
    this.axis = [];
    this.activitiesTypes = [];
    this.nameActivity = '';
  }

  /**
  * Método para limpiar mensajes que se le muestran al usuario.
  */
  cleanMessages(): void {
    this.message = null;
    this.errorMessage = null;
    this.imageEncode = null;
  }

  /**
  * Inicializa los campos del Axi por defecto
  */
  cleanAxi(): void {
    this.axi = this.axiTmp = {
      id: 0,
      description: '',
      name: '',
      imageEncode: '',
      imagePath: '',
      activities: []
    };
  }

  /**
  * Inicializa los campos por defecto
  */
  cleanActivity(): void {
    this.activity = this.activityTmp = {
      id: 0,
      name: null,
      shortName: null,
      idAxi: 0,
      idActivityType: 0,
      description: null,
      imagePath: null,
      imageEncode: null,
      other: null,
      credits: 0,
      visible: false,
      isReplicated: false,
      bannerCode: null,
      active: false,
      createdAt: new Date,
      updatedAt: new Date,
      convalidationTypesId: [],
      convalidationTypes: [],
      activityType: null,
      programmingActivities: [],
      axi: null
    };
  }
}
