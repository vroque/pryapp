import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/finally';

import { ActivityProgrammingService } from '../../shared/services/extracurricular-activities/activity-programming.service';
import { ActivityTypeService } from '../../shared/services/extracurricular-activities/activity-type.service';
import { BackofficeConfigService } from '../config/config.service';
import { CampusService } from '../../shared/services/institutional/campus.service';
import { Department } from '../../shared/services/institutional/department';
import { EnrolledStudentService } from '../../shared/services/extracurricular-activities/enrolled-student.service';
import { TracingService } from '../../shared/services/extracurricular-activities/tracing.service';

import { ActivityProgramming } from '../../shared/services/extracurricular-activities/activity-programming.model';
import { ActivityType } from '../../shared/services/extracurricular-activities/activity-type.model';
import { Campus } from '../../shared/services/academic/campus';
import { DepartmentService } from '../../shared/services/institutional/department.service';
import { EnrolledProgramActivity } from '../../shared/services/extracurricular-activities/enrolled-program-activity.model';
import { Tracing } from '../../shared/services/extracurricular-activities/tracing.model';
import { TracingReport } from '../../shared/services/extracurricular-activities/tracing-report.model';

import { Message } from 'primeng/primeng';

@Component({
  selector: 'vuc-register-tracing',
  templateUrl: './register-tracing.component.html',
  providers: [ActivityTypeService, CampusService, ActivityProgrammingService, EnrolledStudentService, TracingService,
    BackofficeConfigService, DepartmentService]
})

export class VUCRegisterTracingComponent implements OnInit {
  activities: ActivityProgramming[];                      // Lista de Actividades Programadas.
  activitiesTypes: ActivityType[] = [];                   // Lista de Tipo de Actividades.
  campus: Campus[];                                       // Lista de Campus.
  enrolledProgramActivities: EnrolledProgramActivity[];   // Lista de Actividades Programadas y sus inscritos.
  enrolledStudents: EnrolledProgramActivity[];            // Modelo para recibir diversoss tipos de datos.
  modalities: Department[];                               // modalidad
  tracingReport: TracingReport;                           // Modelo de Reporte de Seguimiento
  tracingReports: TracingReport[];                        // Lista de reporte de seguimiento.
  tracingStudents: Tracing[] = [];                        // Lista de Seguimiento
  nameActivity: string;                                   // Variable para guardar nombre de actividad

  // variable de mensaje
  primeMsg: Message[];                                    // Mensaje
  primeMsg1: Message[];                                   // Mensaje

  activityName: string;                                   // Nombre de la actividad
  idActivityType: number;                                 // Tipo de la actividad (Id)
  idCampus: string;                                       // Identificador del campus (Ejem: S01)
  idModality: string;                                     // Identificador de la modalidad (Ejem: UREG)
  idProgramActivityActive: number;                        // Id de la programación activa (seleccionada)
  idSelectedProgAct: number;                              // Id de la programación activa para mostrar estudiantes
  isBlockBoxesActive: boolean;                            // Activa bloques para ingreso masivo de información
  isBusy: boolean;                                        // Estado que indica si esta ocupado el componente
  isRegisterMode: boolean;                                // Estado que indica si esta en estado de registro de notas
  isShowSearch: boolean;                                  // Bnadera para mostar/ocultar los resultados de la búsqueda de programación de actividades
  isShowRegister: boolean;                                // Bandera para mostrar/ocultar el registro de cumplimiento
  isShowReport: boolean;                                  // Bandera para mostrar/ocultar formulario de reporte
  isShowActivity: boolean;                                // Bandera para mostrar/ocultar formulario de reporte "taller"

  // Bloques cuando ingresan información masiva
  dniBox: string;
  dnis: string[] = [];
  hourBox: string;
  hours: string[] = [];
  information: any[] = [];
  observationBox: string;
  observations: string[] = [];
  scoreBox: string;
  scores: string[] = [];

  constructor(
    private config: BackofficeConfigService, private activityTypeService: ActivityTypeService,
    private campusService: CampusService, private activityProgrammingService: ActivityProgrammingService,
    private enrolledStudent: EnrolledStudentService, private tracingService: TracingService,
    private departmentService: DepartmentService) { }

  ngOnInit(): void {
    // Métodos que se ejecutan al iniciar el sistema.
    this.cleanTracing();
    this.getActivityTypes();
    this.getModalityTypes();
    this.getCampus();
    this.activityName = ''; this.idCampus = ''; this.idActivityType = 0; this.idModality = '', this.dniBox = '';
  }

  /**
  * Obtiene los tipos de actividad.
  *  * Programas.
  *  * Talleres.
  */
  getActivityTypes(): void {
    this.isBusy = true;
    this.activityTypeService.getActivityTypes()
      .finally(() => this.isBusy = false)
      .subscribe(
        (activitiesTypes) => this.activitiesTypes = activitiesTypes,
        (error) => console.log(error));
  }

  /**
  * Obtiene las modalidades.
  */
  getModalityTypes(): void {
    this.departmentService.getDepartments().finally(() => this.isBusy = false)
      .subscribe(
        (modalities) => this.modalities = modalities,
        (error) => console.log(error));
  }

  /**
  * Obtiene los campus.
  */
  getCampus(): void {
    this.isBusy = true;
    this.campusService.query()
      .finally(() => this.isBusy = false)
      .subscribe(
        (campus) => this.campus = campus,
        (error) => console.log(error));
  }

  /**
  * Retorna el PDF del programa aprobado.
  * @param {number} pidm Pidm del estudiante.
  * @param {MouseEvent} event Evento de mouse.
  */
  getCertificate(pidm: number, event: MouseEvent): void {
    event.srcElement.children[0].className = '';
    event.srcElement.setAttribute('disabled', 'disabled');
    event.srcElement.children[0].className = 'fa fa-spinner fa-spin';
    // this.isBusy = true;
    this.tracingService.getActivityCertificate(pidm, this.idProgramActivityActive)
      // .finally(() => this.isBusy = false)
      .subscribe(
        (certificatePath) => {
          if (this.idActivityType === 1 && this.tracingReports !== null && event.srcElement.children[0].className !== '') {
            for (let i = 0; i < this.tracingReports.length; i++) {
              if (this.tracingReports[i].pidm === pidm) {
                this.tracingReports[i].hasCertificate = true;
                this.tracingReports[i].certificatePath = certificatePath;
              }
            }
          } else if (this.idActivityType === 2 && event.srcElement.children[0].className !== '') {
            for (let i = 0; i < this.enrolledProgramActivities.length; i++) {
              if (this.enrolledProgramActivities[i].pidmStudent === pidm) {
                this.enrolledProgramActivities[i].hasCertificate = true;
                this.enrolledProgramActivities[i].certificatePath = certificatePath;
              }
            }
          }
        },
        (error) => console.log(error));
  }

  /**
  * Busca las programaciones de las actividades en base a:
  *  * Tipo de actividad.
  *  * Campus.
  *  * nombre de la actividad (opcional).
  */
  findProgrammingActivities(): void {
    this.isBusy = true;
    this.isShowSearch = false;
    this.activityProgrammingService.getProgrammingAcivitiesBy(this.idActivityType, this.idModality, this.idCampus,
      this.activityName)
      .finally(() => this.isBusy = false)
      .subscribe(
        (activities) => {
          this.activities = activities;
          this.activities.forEach(element => {
            element.campus = this.campus.filter(x => x.id === this.idCampus)[0];
            element.department = this.modalities.filter(x => x.id === this.idModality)[0];
          });
        },
        (error) => console.log(error));
  }

  /**
  * Obtiene los estudiantes inscritos de una programación.
  * @param programmingActivity Modelo de la programación de la actividad.
  */
  getEnrolledStudents(programmingActivity: ActivityProgramming): void {
    this.clear();
    this.isRegisterMode = this.isBusy = true;
    this.nameActivity = programmingActivity.activity.shortName;
    // this.isRegisterMode = this.isBusy = true;
    this.enrolledStudents = this.tracingStudents = [];
    this.idSelectedProgAct = programmingActivity.id;
    this.isShowSearch = true;
    this.isShowRegister = false;
    this.enrolledStudent.getEnrolledStudents(programmingActivity.id)
      .finally(() => this.isBusy = false)
      .subscribe(
        (data) => {
          if (data.length === 0 || data === undefined || data === [] || data === null) {
            this.isShowSearch = false;
            this.isShowRegister = true;
            this.primeMsg1 = [
              {
                detail: 'Primero debe de seleccionar a los estudiantes del programa seleccionado.',
                severity: 'warn',
                summary: 'Registro de cumplimiento'
              }
            ];
            return;
          } else {
            this.enrolledStudents = data;
            this.enrolledStudents.forEach(student => {
              student.fullName = student.student.fullNameStudent;
            });
            this.enrolledStudents = this.orderByAsc(this.enrolledStudents, 'fullName');
            this.enrolledStudents.forEach(student => {
              this.tracingStudents.push({
                id: 0,
                pidmStudent: student.pidmStudent,
                idProgrammingActivity: programmingActivity.id,
                hours: 0,
                score: 0,
                observation: null,
                active: true,
                createdAt: null,
                updatedAt: null
              });
            });
          }
        },
        (error) => console.log(error));
  }

  /**
  * Obtiene la información de los estudiantes aprobados(inscritos) - talleres.
  * @param {number} idProgrammingActivity Id de la programación de la actividad.
  * @param {string} term periodo.
  */
  getStudent(programmingActivity: ActivityProgramming, term: string): void {
    this.clear();
    this.idProgramActivityActive = programmingActivity.id;
    this.isBusy = true;
    this.nameActivity = programmingActivity.activity.shortName;
    this.isShowActivity = false;
    this.isShowSearch = true;
    this.enrolledStudent.getStudents(programmingActivity.id, term)
      .finally(() => this.isBusy = false)
      .subscribe(
        (enrolledProgramActivities) => {
          this.enrolledProgramActivities = enrolledProgramActivities;
          this.enrolledProgramActivities = enrolledProgramActivities;
          this.enrolledProgramActivities.forEach(element => {
            element.departmend = this.modalities.filter(x => x.id === this.idModality)[0];
          });
          if (enrolledProgramActivities.length === 0 || enrolledProgramActivities === undefined
            || enrolledProgramActivities === [] || enrolledProgramActivities === null) {
            this.isShowSearch = false;
            this.isShowActivity = true;
            this.primeMsg1 = [
              {
                detail: 'No existen estudiantes inscritos en taller seleccionado.',
                severity: 'warn',
                summary: 'Reporte de cumplimiento "Taller"'
              }
            ];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Registra el cumplimiento del programa.
  */
  saveTracing(): void {
    this.isBusy = true;
    this.tracingService.save(this.tracingStudents)
      .finally(() => this.isBusy = false)
      .subscribe(
        (data) => {
          if (data === null || data === undefined) {
            this.primeMsg = [{
              detail: 'Ha ocurrido un problema al intentar registrar el cumplimiento',
              severity: 'danger',
              summary: 'Registro de cumplimiento'
            }];
            return;
          }
          this.findProgrammingActivities();
          this.primeMsg = [{
            detail: 'Se ha registrado correctamente',
            severity: 'success',
            summary: 'Registro de cumplimiento'
          }];
          this.clear();
        },
        (error) => console.log(error));
  }

  /**
  * Obtiene la información necesaria para generar el reporte de cumplimiento.
  * @param  programmingActivity Modelo de la programación de la actividad.
  */
  getReporte(programmingActivity: ActivityProgramming): void {
    this.clear();
    this.isShowSearch = true;
    this.isShowReport = false;
    this.nameActivity = programmingActivity.activity.shortName;
    this.idProgramActivityActive = programmingActivity.id;
    this.isBusy = true;
    this.isRegisterMode = false;
    this.tracingService.getReporte(programmingActivity.id)
      .finally(() => this.isBusy = false)
      .subscribe(
        (data) => {
          this.tracingReports = data;
          if (data.length === 0 || data === undefined || data === [] || data === null) {
            this.isShowSearch = false;
            this.isShowReport = true;
            this.primeMsg1 = [
              {
                detail: 'Primero debe de registrar el cumplimiento de los estudiantes.',
                severity: 'warn',
                summary: 'Reporte de cumplimiento "Programa"'
              }
            ];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Descarga un Excel del cumplimiento del programa.
  */
  download(): void {
    const data: any[] = this.tracingReports.map(
      (dato) => {
        return {
          'Nombre del Programa': dato.activityName,
          'DNI': dato.dni,
          'Nombre del estudiante': dato.fullName,
          'EAP': dato.eap,
          'Estado': dato.state,
          'Horas': dato.hours,
          'Calificación': dato.score,
          'Observaciones': dato.observation,
          'Fecha de registro': dato.createdAt
        };
      });
    this.config.exportAsExcelFile(data, 'Reporte de cumplimiento');
  }

  /**
  * Descarga un Excel del cumplimiento del taller
  */
  downloadStudentsAccredited(): void {
    const data: any[] = this.enrolledProgramActivities.map(
      (dato) => {
        return {
          'Nombre del taller': dato.student.courseName,
          'DNI': dato.student.documentNumber,
          'Nombre del estudiante': dato.student.fullNameStudent,
          'Calificación': dato.student.finalScore,
          'Asistencia': dato.student.percentageAssistance
        };
      });
    this.config.exportAsExcelFile(data, 'Reporte de estudiantes acreditados');
  }

  /**
  * Genera un array del textarea de dnis, los divide por un saldo de linea.
  */
  splitDni(): void {
    if (this.dniBox === null || this.dniBox === '') { return; }
    this.dnis = this.dniBox.split(/\n/)
      .filter((dni) => dni !== '')
      .filter((dni, position, self) => self.indexOf(dni) === position);
    this.fillInputs();
  }

  /**
  * Genera un array del textarea de hours, los divide por un saldo de linea.
  */
  splitHour(): void {
    if (this.hourBox === null || this.hourBox === '') { return; }
    this.hours = this.hourBox.split(/\n/)
      .filter((hour) => hour !== '');
    this.fillInputs();
  }

  /**
  * Genera un array del textarea de scores, los divide por un saldo de linea.
  */
  splitScore(): void {
    if (this.scoreBox === null || this.scoreBox === '') { return; }
    this.scores = this.scoreBox.split(/\n/)
      .filter((score) => score !== '');
    this.fillInputs();
  }

  /**
  * Genera un array del textarea de observaciones en base a un . (punto).
  */
  splitObservation(): void {
    if (this.observationBox === null || this.observationBox === '') { return; }
    this.observations = this.observationBox.split(/.\n/g)
      .filter((observation) => observation !== '');
    this.fillInputs();
  }

  /**
  * Pasa la información de los bloques de información masiva a las cajas de texto
  * que se muestra en tabla.
  */
  fillInputs(): void {
    this.information = [];
    if (this.dniBox !== null && this.dniBox !== undefined && this.dniBox !== '') {
      for (let index = 0; index < this.dnis.length; index++) {
        this.information.push({
          dni: this.dnis[index],
          hour: this.hours[index],
          score: this.scores[index],
          observation: this.observations[index]
        });
      }
      for (let index = 0; index < this.tracingStudents.length; index++) {
        if (this.information[index] !== undefined) {
          this.tracingStudents[index].hours = this.information[index].hour === undefined ? 0 : this.information[index].hour;
          this.tracingStudents[index].score = this.information[index].score === undefined ? 0 : this.information[index].score;
          this.tracingStudents[index].observation = this.information[index].observation === undefined ? ''
            : this.information[index].observation;
        }
      }
    }
  }

  /**
  * Activa las cajas de texto para el ingreso masivo de información de notas, horas y observaciones.
  */
  activateGroupBoxes(): void {
    this.isBlockBoxesActive = !this.isBlockBoxesActive;
  }

  /**
 * Regresa al formulario de busqueda
*/
  back(): void {
    this.isShowSearch = false;
    this.isBlockBoxesActive = false;
  }

  /**
  * Limpiar los campos de inscritos.
  */
  clear(): void {
    this.tracingReports = null;
    this.tracingStudents = [];
    this.enrolledStudents = null;
    this.enrolledProgramActivities = null;
    this.idSelectedProgAct = 0;
    this.dniBox = this.hourBox = this.scoreBox = this.observationBox = '';
    this.dnis = this.hours = this.scores = this.observations = [];
    this.cleanTracing();
  }

  /**
    * Método para limpiar el combobox de Campus por tipo de actividad seleccionado.
  */
  cleanCampus(): void {
    if (this.idModality === 'UVIR') {
      this.idCampus = 'V00';
    } else {
      this.idCampus = '';
    }
  }

  /**
  * Limpia los campos de la búsqueda.
  */
  cleanSearch(): void {
    this.activityName = '';
    this.idCampus = '';
    this.idModality = '';
    this.idActivityType = 0; // Tipo de actividad : Programa
    this.activities = null;
    this.clear();
  }

  /**
  * Inicializa el modelo del cumplimiento
  */
  cleanTracing(): void {
    this.tracingReport = {
      activityName: '', createdAt: new Date, dni: '', pidm: 0, eap: '', fullName: '', hours: 0,
      observation: '', program: '', state: '', score: 0, hasCertificate: false, certificatePath: ''
    };
  }

  /**
  * Orderna un array de manera ascendente en base a un propiedad
  * @param {any[]} array Array a ordernar
  * @param {string} prop Propiedad por la cual se va a ordernar
  * @returns Retorna el array odernado
  */
  orderByAsc(array: any[], prop: string) {
    return array.sort((a, b) => a[prop] > b[prop] ? 1 : a[prop] === b[prop] ? 0 : -1);
  }
}
