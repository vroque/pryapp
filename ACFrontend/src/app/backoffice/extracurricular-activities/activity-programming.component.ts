import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Message } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

import { Activity } from '../../shared/services/extracurricular-activities/activity.model';
import { ActivityProgramming } from '../../shared/services/extracurricular-activities/activity-programming.model';
import { ActivityProgrammingService } from '../../shared/services/extracurricular-activities/activity-programming.service';
import { ActivityService } from '../../shared/services/extracurricular-activities/activity.service';
import { ActivityType } from '../../shared/services/extracurricular-activities/activity-type.model';
import { ActivityTypeService } from '../../shared/services/extracurricular-activities/activity-type.service';
import { BackofficeConfigService } from '../config/config.service';
import { Campus } from '../../shared/services/institutional/campus';
import { CampusService } from '../../shared/services/institutional/campus.service';
import { ConfigService } from '../../config/config.service';
import { Curriculum } from '../../shared/services/extracurricular-activities/curriculum.model';
import { CurriculumService } from '../../shared/services/extracurricular-activities/curriculum.service';
import { Department } from '../../shared/services/institutional/department';
import { DepartmentService } from '../../shared/services/institutional/department.service';
import { ScheduleType } from '../../shared/services/extracurricular-activities/schedule-type.model';
import { ScheduleTypeService } from '../../shared/services/extracurricular-activities/schedule-type.service';
import { TeacherSearch } from '../../shared/services/extracurricular-activities/teacher-search.model';
import { TeacherSearchService } from '../../shared/services/extracurricular-activities/teacher-search.service';
import { TermService } from '../../shared/services/extracurricular-activities/term.service';

@Component({
  selector: 'vuc-activity-programming',
  templateUrl: './activity-programming.component.html',
  providers: [
    ActivityProgrammingService, ActivityService, ActivityTypeService, BackofficeConfigService, CurriculumService,
    CampusService, DepartmentService, TeacherSearchService, ScheduleTypeService, TermService, ConfigService]
})

export class VUCActivityProgrammingComponent implements OnInit {
  activities: Activity[];                          // Lista de actividades
  activitiesType: ActivityType[];                  // Lista de tipo de actividades.
  activitiesProgramming: ActivityProgramming[];    // Lista de programaciones de actividad.
  activityProgramming: ActivityProgramming;        // Modelo de programaciones de actividad.

  activityProgrammingTmp: ActivityProgramming;     // Variable temporal para las programaciones de actividad.
  curriculum: Curriculum[];                        // Lista de planes de estudio.
  campus: Campus[];                                // Lista de campus.
  departments: Department[];                       // Lista de modalidades.
  teacherInfoSearched: TeacherSearch;              // Variable para la búsqueda de un docente.
  scheduleTypes: ScheduleType[];                   // Lista de tipos de horario.
  activityProgrammingForm: FormGroup;              // Variable para manejar las validaciones del formulario.
  terms: any[] = [];                               // Lista de los periodos académicos

  // Variables para manejar el filtro
  idActivityType: number;                          // Variable para tipo de actividad.
  idModality: string;                              // Variable para modalidad.
  idCampus: string;                                // Variable para campus.
  searchTerm: string;                              // Varible para buscar por un periodo académico

  // Variables de estado.
  change: boolean;                                 // Variable para manejar si existe algún cambio en el formulario.
  isBusy: boolean;                                 // Estado que indica que el servicio esta ocupado.
  notFoundTeacher: boolean;                        // Variable para verificar si esque no encuentra un docente.
  showCurriculums: boolean;                        // Variable para mostrar planes de estudio.
  isLoadingTeacher: boolean;                       // Variable para mostrar el texto de busqueda de docente
  isLoadingActivity: boolean;                      // Variable para mostrar el cargado de actividad

  // - Formularios.
  showCost: boolean;                               // Variable para mostrar los costos.
  showExternalResponsible: boolean;                // Variable para mostrar un responsable externo.
  showFormNewActivityProgramming: boolean;         // Variable para manejar el registro de una programación.
  showListProgrammedActivities: boolean;           // Variable para manejar la lista de programaciones de actividad.
  showScheduleDescription: boolean;                // Variable para manejar la descripción de horario (descriptivo).
  showScheduleNoDescription: boolean;              // Variable para manejar la descripción de horario (no descriptivo).
  showSearchButton: boolean;                       // Variable para mostrar el botón de búsqueda.
  showUniversityTeacher: boolean;                  // Variable para mostrar un docente.

  // Variables globales.
  idActivityTypeSelected: number;                 // Variable para manejar la selección de un tipo de actividad.
  responsibleTypeSelected: number;                // Variable para manejar la selección de un tipo de responsable.

  // Variables para fecha y años.
  es: any;                                        // Configuración del datepicker
  wata: number;                                   // Fecha

  // Variables de mensajes.
  primeMsg: Message[];                            // Mensaje.

  // Variable para poner valores en el combobox de tipo de responsable.
  responsibleTypes: any[] = [
    { value: 0, description: 'Selecciona un tipo de responsable' },
    { value: 1, description: 'Docente de la universidad' },
    { value: 2, description: 'Responsable externo' }, { value: 3, description: 'No asignado' }
  ];

  // Variabes para poner valores en el combobox de Día (para Horario No Descriptivo).
  daysSchedule: any[] = [
    { value: null, description: 'Selecciona un día' }, { value: 'Lunes', description: 'Lunes' },
    { value: 'Martes', description: 'Martes' }, { value: 'Miércoles', description: 'Miércoles' },
    { value: 'Jueves', description: 'Jueves' }, { value: 'Viernes', description: 'Viernes' },
    { value: 'Sábado', description: 'Sábado' }, { value: 'Domingo', description: 'Domingo' }
  ];

  constructor(private activityService: ActivityService, private activityTypeService: ActivityTypeService,
    private activityProgrammingService: ActivityProgrammingService, private configService: ConfigService,
    private curriculumService: CurriculumService, private campusService: CampusService,
    private departmentService: DepartmentService, private teacherSearchService: TeacherSearchService,
    private scheduleTypeService: ScheduleTypeService, private termService: TermService,
    private backofficeConfigService: BackofficeConfigService) { }

  ngOnInit(): void {
    this.initActivityProgramming();
    this.activityProgrammingForm = new FormGroup({});
    this.idActivityTypeSelected = 0;
    this.searchTerm = '';
    this.getTerm();
    this.clean();
    this.getActivitiesType();
    this.getCampus();
    this.getDepartments();
    this.idCampus = ''; this.idActivityType = 0; this.idModality = '';
    this.responsibleTypeSelected = 0;
    this.showFormNewActivityProgramming = false;
    this.showListProgrammedActivities = true;
    this.es = this.configService.getCalendarEs();
    this.wata = new Date().getFullYear();
  }

  /**
   * Inicializa la programación con sus valores por defecto
   */
  initActivityProgramming(): void {
    this.activityProgramming = {
      id: 0,
      idActivity: 0,
      startDateEnrollment: new Date(),
      endDateEnrollment: new Date(),
      startDateActivity: new Date(),
      endDateActivity: new Date(),
      cost: 102,
      currency: 'PEN',
      studentProfile: null,
      startPeriod: null,
      endPeriod: '999999',
      idScheduleType: '',
      scheduleDescription: null,
      sessionDay: null,
      startHour: null,
      endHour: null,
      sessionPlace: null,
      quantityVacancy: 0,
      maxQuantityVacancy: 0,
      documentTeacher: null,
      nameTeacher: null,
      idCampus: '',
      idModality: '',
      replicated: false,
      nrc: null,
      programmingActCurriculumId: [],
      isEnrolled: false,
      isSocialProjection: false,
      activity: null,
      hasSelected: false, hasTracing: false, programmingActivityCurriculum: [],
      isExtracurricular: false, campus: { code: '', id: '', name: '' }, department: { id: '', name: '' }
    };
  }

  /**
   * Método para verificar si se realiza algún cambio en el formulario de Programación de Actividad.
   */
  verifyChangeActivityProgram(): void {
    if (JSON.stringify(this.activityProgramming) === JSON.stringify(this.activityProgramming)) {
      this.change = false;
    } else {
      this.change = true;
    }
  }

  /**
   * Obtiene los periodos académicos
   */
  getTerm(): void {
    this.isBusy = true;
    this.termService.getAll()
      .finally(() => this.isBusy = false)
      .subscribe(
        (terms) => this.terms = terms,
        (error) => console.log(error));
  }

  /**
   * Obtiene los tipos de actividades.
   */
  getActivitiesType(): void {
    this.isBusy = true;
    this.activityTypeService.getActivityTypes()
      .finally(() => this.isBusy = false)
      .subscribe(
        (activitiesType) => this.activitiesType = activitiesType,
        (error) => console.log(error));
  }

  /**
   * Obtiene los planes de estudio.
   */
  getCurriculums(): void {
    this.isBusy = true;
    this.curriculumService.getCurriculums()
      .finally(() => this.isBusy = false)
      .subscribe(
        (curriculum) => this.curriculum = curriculum,
        (error) => console.log(error));
  }

  /**
   * Obtiene los campus (sedes y filiales).
   */
  getCampus(): void {
    this.isBusy = true;
    this.campusService.query()
      .finally(() => this.isBusy = false)
      .subscribe(
        (campus) => this.campus = campus,
        (error) => console.log(error));
  }

  /**
   * Obtiene las modalidades.
   */
  getDepartments(): void {
    this.isBusy = true;
    this.departmentService.getDepartments()
      .finally(() => this.isBusy = false)
      .subscribe(
        (departments) => this.departments = departments,
        (error) => console.log(error));
  }

  /**
   * Obtiene los tipos de horario.
   */
  getScheduleTypes(): void {
    this.isBusy = true;
    this.scheduleTypeService.getScheduleTypes()
      .finally(() => this.isBusy = false)
      .subscribe(
        (scheduleTypes) => this.scheduleTypes = scheduleTypes,
        (error) => console.log(error));
  }

  /**
   * Método que carga el formulario para crear una nueva programación de actividad.
   */
  newActivityProgramming(): void {
    this.initActivityProgramming();
    this.idActivityTypeSelected = 0;
    this.showCost = false;
    this.showExternalResponsible = false;
    this.showFormNewActivityProgramming = true;
    this.showListProgrammedActivities = false;
    this.responsibleTypeSelected = 0;
    this.showScheduleDescription = false;
    this.showScheduleNoDescription = false;
    this.showUniversityTeacher = false;
    this.getCurriculums();
    this.getScheduleTypes();
  }

  /**
   * Método que valida y guarda en la tabla una nueva programación de actividad.
   */
  createActivityProgramming(): void {
    // Se establece ambos valores como igual ya que lo que se va a mostrar es el máximo de vacantes, por cambios del
    // usuario se esta dejando de esta forma para evitar retrasos.
    this.activityProgramming.quantityVacancy = this.activityProgramming.maxQuantityVacancy;
    // Validación de los campos.
    if (this.idActivityTypeSelected === 0) {
      this.primeMsg = [
        {
          detail: 'Selecciona un tipo de actividad para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.idActivity === 0) {
      this.primeMsg = [
        {
          detail: 'Selecciona una actividad para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.startPeriod == null ||
      this.activityProgramming.startPeriod.trim().length !== 6) {
      this.primeMsg = [
        {
          detail: 'Ingresa un período válido para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (Object.keys(this.activityProgramming.programmingActCurriculumId).length === 0) {
      this.primeMsg = [
        {
          detail: 'Selecciona al menos un plan de estudios para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.idModality == null || this.activityProgramming.idModality === '') {
      this.primeMsg = [
        {
          detail: 'Selecciona una modalidad para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if ((this.activityProgramming.idCampus == null || this.activityProgramming.idCampus === '')) {
      this.primeMsg = [
        {
          detail: 'Selecciona un campus para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.studentProfile == null || this.activityProgramming.studentProfile === '' ||
      this.activityProgramming.studentProfile.trim().length === 0 ||
      this.activityProgramming.studentProfile.trim().length > 800) {
      this.primeMsg = [
        {
          detail: 'El perfil es obligario y debe contener como máximo 800 caracteres',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (Number(this.responsibleTypeSelected) === 0) {
      this.primeMsg = [
        {
          detail: 'Selecciona un tipo de responsable para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if ((Number(this.responsibleTypeSelected) === 1 && this.activityProgramming.documentTeacher == null) ||
      (Number(this.responsibleTypeSelected) === 1 && this.activityProgramming.documentTeacher.trim().length !== 8) ||
      (Number(this.responsibleTypeSelected) === 1 && this.teacherInfoSearched == null)) {
      this.primeMsg = [
        {
          detail: 'Ingresa y busca un documento de docente válido para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (Number(this.responsibleTypeSelected) === 2 && (this.activityProgramming.nameTeacher == null ||
      this.activityProgramming.nameTeacher.trim().length === 0 ||
      this.activityProgramming.nameTeacher.trim().length > 100)) {
      this.primeMsg = [
        {
          detail: 'Ingresa un nombre de responsable válido para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.startDateEnrollment == null) {
      this.primeMsg = [
        {
          detail: 'Ingresa una fecha de inicio de matrícula válida para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.endDateEnrollment == null) {
      this.primeMsg = [
        {
          detail: 'Ingresa una fecha de fin de matrícula válida para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.startDateActivity == null) {
      this.primeMsg = [
        {
          detail: 'Ingresa una fecha de inicio de actividad válida para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.endDateActivity == null) {
      this.primeMsg = [
        {
          detail: 'Ingresa una fecha de fin de actividad válida para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.idScheduleType == null) {
      this.primeMsg = [
        {
          detail: 'Selecciona un tipo de horario para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.maxQuantityVacancy <= 0) {
      this.primeMsg = [
        {
          detail: 'Ingresa una cantidad de vacantes mayor a 0',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.idScheduleType === 'D') {
      if (this.activityProgramming.scheduleDescription == null || this.activityProgramming.scheduleDescription === '' ||
        this.activityProgramming.scheduleDescription.trim().length === 0 ||
        this.activityProgramming.scheduleDescription.trim().length > 500) {
        this.primeMsg = [
          {
            detail: 'Ingresa una descripción del horario válido para continuar, no debe de execeder los 500 caracteres',
            severity: 'warn',
            summary: 'Atención'
          }
        ];
        return;
      }
    } else if (this.activityProgramming.idScheduleType === 'ND') {
      if (this.activityProgramming.sessionDay == null || this.activityProgramming.sessionDay.trim().length === 0) {
        this.primeMsg = [
          {
            detail: 'Ingresa un día para continuar',
            severity: 'warn',
            summary: 'Atención'
          }
        ];
        return;
      } else if (this.activityProgramming.startHour == null || this.activityProgramming.startHour.trim().length === 0) {
        this.primeMsg = [
          {
            detail: 'Ingresa una hora de inicio válida para continuar',
            severity: 'warn',
            summary: 'Atención'
          }
        ];
        return;
      } else if (this.activityProgramming.endHour == null || this.activityProgramming.endHour.trim().length === 0) {
        this.primeMsg = [
          {
            detail: 'Ingresa una hora de fin válida para continuar',
            severity: 'warn',
            summary: 'Atención'
          }
        ];
        return;
      } else if (this.activityProgramming.sessionPlace == null ||
        this.activityProgramming.sessionPlace.trim().length === 0 ||
        this.activityProgramming.sessionPlace.trim().length > 30) {
        this.primeMsg = [
          {
            detail: 'El lugar debe contener como máximo 30 caracteres',
            severity: 'warn',
            summary: 'Atención'
          }
        ];
        return;
      }
    }
    this.isBusy = true;
    this.showFormNewActivityProgramming = true;
    this.activityProgrammingService.create(this.activityProgramming)
      .finally(() => this.isBusy = false)
      .subscribe(
        (activityProgramming) => {
          if (activityProgramming !== null && activityProgramming !== undefined) {
            this.primeMsg = [
              {
                detail: 'Se ha creado correctamente la programación de la actividad',
                severity: 'success',
                summary: 'Felicidades'
              }
            ];
            this.clean();
            this.showFormNewActivityProgramming = false;
            this.showListProgrammedActivities = true;
          } else {
            this.primeMsg = [
              {
                detail: 'Ha ocurrido un problema al intentar crear la programación de la actividad',
                severity: 'danger',
                summary: 'Error'
              }
            ];
          }
        },
        (error) => console.log(error));
  }

  /**
   * Método que carga el formulario para editar una programación de actividad.
   */
  editActivityProgramming(id: number, activityTypeId: number): void {
    this.showFormNewActivityProgramming = true;
    this.showListProgrammedActivities = false;
    this.activityProgrammingForm.reset();
    this.isBusy = true;

    Observable.forkJoin([
      this.activityService.getByActivityType(activityTypeId),
      this.activityProgrammingService.getByProgrammedActivitiesId(id),
      this.curriculumService.getCurriculums(),
      this.scheduleTypeService.getScheduleTypes()
    ]).finally(() => this.isBusy = false).subscribe((result: any[]) => {
      this.activityProgramming = result[1];
      this.activities = result[0];
      this.curriculum = result[2];
      this.scheduleTypes = result[3];
      this.activityProgrammingTmp = JSON.parse(JSON.stringify(this.activityProgramming));
      this.idActivityTypeSelected = activityTypeId;
      this.activityProgramming.programmingActCurriculumId = [];
      this.activityProgramming.programmingActivityCurriculum.forEach(curriculum => {
        this.activityProgramming.programmingActCurriculumId.push(curriculum.idCurriculum);
      });
      // Llamando a las fechas guardadas.
      this.activityProgramming.startDateEnrollment = new Date(this.activityProgramming.startDateEnrollment.toString());
      this.activityProgramming.endDateEnrollment = new Date(this.activityProgramming.endDateEnrollment.toString());
      this.activityProgramming.startDateActivity = new Date(this.activityProgramming.startDateActivity.toString());
      this.activityProgramming.endDateActivity = new Date(this.activityProgramming.endDateActivity.toString());
      // Muestra u oculta la sección Financiera por el tipo de actividad
      if (Number(activityTypeId) === 1) {
        this.activityProgramming.cost = this.activityProgramming.currency = null;
        this.showCost = false;
      } else {
        if (Number(activityTypeId) === 2) {
          this.showCost = true;
        }
      }
      // Método scheduleTypeSelected() para Editar.
      if (this.activityProgramming && this.activityProgramming.idScheduleType === 'D') {
        this.activityProgramming.sessionDay = null;
        this.activityProgramming.startHour = null;
        this.activityProgramming.endHour = null;
        this.activityProgramming.sessionPlace = null;
        this.showScheduleDescription = true;
        this.showScheduleNoDescription = false;
      } else if (this.activityProgramming && this.activityProgramming.idScheduleType === 'ND') {
        this.activityProgramming.scheduleDescription = null;
        this.showScheduleNoDescription = true;
        this.showScheduleDescription = false;
      }
      // Obteniendo tipo de responsable y asignando a variable responsibleTypeSelected.
      if (this.activityProgramming.documentTeacher != null && this.activityProgramming.nameTeacher == null) {
        this.responsibleTypeSelected = 1;
      } else if (this.activityProgramming.documentTeacher == null && this.activityProgramming.nameTeacher != null) {
        this.responsibleTypeSelected = 2;
      } else if (this.activityProgramming.documentTeacher == null && this.activityProgramming.nameTeacher == null) {
        this.responsibleTypeSelected = 3;
      }
      // Método responsibleSelected() modificado para Editar.
      if (this.responsibleTypeSelected === 1) {
        this.activityProgramming.nameTeacher = null;
        this.searchTeacher();
        this.showExternalResponsible = false;
        this.showUniversityTeacher = true;
      } else if (this.responsibleTypeSelected === 2) {
        this.activityProgramming.documentTeacher = null;
        this.showExternalResponsible = true;
        this.showUniversityTeacher = false;
      } else if (this.responsibleTypeSelected === 3) {
        this.activityProgramming.nameTeacher = null;
        this.activityProgramming.documentTeacher = null;
        this.showExternalResponsible = false;
        this.showUniversityTeacher = false;
      }
    });
  }

  /**
   * Método para actualizar una programación de actividad, una vez que se haya modificado el formulario.
   */
  updateActivityProgramming(): void {
    // Validación de los campos.
    if (this.idActivityTypeSelected === 0) {
      this.primeMsg = [
        {
          detail: 'Selecciona un tipo de actividad para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.idActivity === 0) {
      this.primeMsg = [
        {
          detail: 'Selecciona una actividad para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.startPeriod == null ||
      this.activityProgramming.startPeriod.trim().length !== 6) {
      this.primeMsg = [
        {
          detail: 'Ingresa un período válido para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (Object.keys(this.activityProgramming.programmingActCurriculumId).length === 0) {
      this.primeMsg = [
        {
          detail: 'Selecciona al menos un plan de estudios para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.idModality == null || this.activityProgramming.idModality === '') {
      this.primeMsg = [
        {
          detail: 'Selecciona una modalidad para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if ((this.activityProgramming.idCampus == null || this.activityProgramming.idCampus === '')) {
      this.primeMsg = [
        {
          detail: 'Selecciona un campus para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.studentProfile == null || this.activityProgramming.studentProfile === '' ||
      this.activityProgramming.studentProfile.trim().length === 0 ||
      this.activityProgramming.studentProfile.trim().length > 800) {
      this.primeMsg = [
        {
          detail: 'El perfil es obligatorio y debe contener como máximo 800 caracteres',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (Number(this.responsibleTypeSelected) === 0) {
      this.primeMsg = [
        {
          detail: 'Selecciona un tipo de responsable para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if ((Number(this.responsibleTypeSelected) === 1 && this.activityProgramming.documentTeacher == null) ||
      (Number(this.responsibleTypeSelected) === 1 && this.activityProgramming.documentTeacher.trim().length !== 8) ||
      (Number(this.responsibleTypeSelected) === 1 && this.teacherInfoSearched == null)) {
      this.primeMsg = [
        {
          detail: 'Ingresa y busca un documento de docente válido para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (Number(this.responsibleTypeSelected) === 2 && (this.activityProgramming.nameTeacher == null ||
      this.activityProgramming.nameTeacher.trim().length === 0 ||
      this.activityProgramming.nameTeacher.trim().length > 100)) {
      this.primeMsg = [
        {
          detail: 'Ingresa un nombre de responsable válido para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.startDateEnrollment == null) {
      this.primeMsg = [
        {
          detail: 'Ingresa una fecha de inicio de matrícula válida para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.endDateEnrollment == null) {
      this.primeMsg = [
        {
          detail: 'Ingresa una fecha de fin de matrícula válida para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.startDateActivity == null) {
      this.primeMsg = [
        {
          detail: 'Ingresa una fecha de inicio de actividad válida para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.endDateActivity == null) {
      this.primeMsg = [
        {
          detail: 'Ingresa una fecha de fin de actividad válida para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.idScheduleType == null) {
      this.primeMsg = [
        {
          detail: 'Selecciona un tipo de horario para continuar',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.maxQuantityVacancy <= 0) {
      this.primeMsg = [
        {
          detail: 'Ingresa una cantidad de vacantes mayor a 0',
          severity: 'warn',
          summary: 'Atención'
        }
      ];
      return;
    } else if (this.activityProgramming.idScheduleType === 'D') {
      if (this.activityProgramming.scheduleDescription == null || this.activityProgramming.scheduleDescription === '' ||
        this.activityProgramming.scheduleDescription.trim().length === 0 ||
        this.activityProgramming.scheduleDescription.trim().length > 500) {
        this.primeMsg = [
          {
            detail: 'Ingresa una descripción del horario válido para continuar, no debe de execeder los 500 caracteres',
            severity: 'warn',
            summary: 'Atención'
          }
        ];
        return;
      }
    } else if (this.activityProgramming.idScheduleType === 'ND') {
      if (this.activityProgramming.sessionDay == null || this.activityProgramming.sessionDay.trim().length === 0) {
        this.primeMsg = [
          {
            detail: 'Ingresa un día para continuar',
            severity: 'warn',
            summary: 'Atención'
          }
        ];
        return;
      } else if (this.activityProgramming.startHour == null || this.activityProgramming.startHour.trim().length === 0) {
        this.primeMsg = [
          {
            detail: 'Ingresa una hora de inicio válida para continuar',
            severity: 'warn',
            summary: 'Atención'
          }
        ];
        return;
      } else if (this.activityProgramming.endHour == null || this.activityProgramming.endHour.trim().length === 0) {
        this.primeMsg = [
          {
            detail: 'Ingresa una hora de fin válida para continuar',
            severity: 'warn',
            summary: 'Atención'
          }
        ];
        return;
      } else if (this.activityProgramming.sessionPlace == null ||
        this.activityProgramming.sessionPlace.trim().length === 0 ||
        this.activityProgramming.sessionPlace.trim().length > 30) {
        this.primeMsg = [
          {
            detail: 'El lugar debe contener como máximo 30 caracteres',
            severity: 'warn',
            summary: 'Atención'
          }
        ];
        return;
      }
    }
    this.isBusy = true;
    this.change = false;
    this.activityProgrammingService.update(this.activityProgramming)
      .finally(() => this.isBusy = false)
      .subscribe(
        (activityProgramming) => {
          if (activityProgramming !== null && activityProgramming !== undefined) {
            this.primeMsg = [
              {
                detail: 'Se ha actualizado correctamente la programación de la actividad',
                severity: 'success',
                summary: 'Felicidades'
              }
            ];
            this.clean();
            this.showFormNewActivityProgramming = false;
            this.showListProgrammedActivities = true;
          } else {
            this.primeMsg = [
              {
                detail: 'Ha ocurrido un problema al intentar actualizar la programación de la actividad',
                severity: 'danger',
                summary: 'Error'
              }
            ];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Método para eliminar una programación de actividad por su id.
  * @param {number} id id de la programación
  */
  deleteActivityProgramming(id: number): void {
    if (!confirm('¿Está seguro que desea eliminar la actividad programada?')) { return; }
    this.isBusy = true;
    this.activityProgrammingService.delete(id)
      .finally(() => this.isBusy = false)
      .subscribe(
        (activityProgramming) => {
          if (activityProgramming !== null && activityProgramming !== undefined) {
            this.primeMsg = [
              {
                detail: 'Se ha eliminado correctamente la programación de la actividad',
                severity: 'success',
                summary: 'Felicidades'
              }
            ];
            this.clean();
          } else {
            this.primeMsg = [
              {
                detail: 'Ha ocurrido un problema al intentar eliminar la programación de la actividad',
                severity: 'danger',
                summary: 'Error'
              }
            ];
          }
        },
        (error) => console.log(error));
  }

  /**
   * Método para buscar un docente por su DNI.
   */
  searchTeacher(): void {
    this.isLoadingTeacher = true;
    if (this.activityProgramming.documentTeacher !== null && this.activityProgramming.documentTeacher !== undefined) {
      this.teacherSearchService.getTeacherByDni(this.activityProgramming.documentTeacher)
        .finally(() => this.isLoadingTeacher = false)
        .subscribe(
          (teacher) => {
            this.teacherInfoSearched = teacher;
            if (this.teacherInfoSearched == null) {
              this.notFoundTeacher = true;
            } else {
              this.notFoundTeacher = false;
            }
          },
          (error) => console.log(error));
    }
  }

  /**
   * Método que valida la búsqueda y desactiva el botón de buscar docente hasta que se ingrese un DNI.
   */
  searchTeacherValidate(): boolean {
    if (this.activityProgramming.documentTeacher == null || this.activityProgramming.documentTeacher.trim().length !== 8) {
      this.showSearchButton = false;
      return true;
    } else {
      this.showSearchButton = true;
      return false;
    }
  }

  /**
   * Método para limpiar los campos y mostrar formulario de costo, por tipo de actividad (para nueva programación).
   */
  changeActivityType(): void {
    this.idModality = this.idCampus = '';
    if (Number(this.idActivityTypeSelected) === 1) {
      this.activityProgramming.cost = null;
      this.activityProgramming.currency = null;
      this.activityProgramming.idActivity = 0;
      this.activityProgramming.idCampus = '';
      this.activityProgramming.idModality = '';
      this.activities = [];
      this.showCost = false;
    } else {
      // Carga estos valores cuando se cambia a una actividad de tipo taller en el combobox
      this.activityProgramming.cost = 102;
      this.activityProgramming.currency = 'PEN';
      this.activityProgramming.idActivity = 0;
      this.activityProgramming.idModality = '';
      this.activityProgramming.idCampus = '';
      this.activities = [];
      this.showCost = true;
    }
    this.isLoadingActivity = true;
    this.activityService.getByActivityType(this.idActivityTypeSelected).finally(() => this.isLoadingActivity = false)
      .subscribe(
        (activities) => this.activities = activities,
        (error) => console.log(error));
  }

  /**
  * Método para limpiar campos y mostrar formulario de horario,
  * por tipo de horario seleccionado (para nueva programación).
  */
  scheduleTypeSelected(): void {
    if (this.activityProgramming && this.activityProgramming.idScheduleType === 'D') {
      this.activityProgramming.sessionDay = null;
      this.activityProgramming.startHour = null;
      this.activityProgramming.endHour = null;
      this.activityProgramming.sessionPlace = null;
      this.showScheduleDescription = true;
      this.showScheduleNoDescription = false;
    } else if (this.activityProgramming && this.activityProgramming.idScheduleType === 'ND') {
      this.activityProgramming.scheduleDescription = null;
      this.showScheduleNoDescription = true;
      this.showScheduleDescription = false;
    }
  }

  /**
  * Método para limpiar campos y mostrar formulario de responsable,
  * por tipo de responsable seleccionado (para nueva programación).
  */
  responsibleSelected(): void {
    if (this.responsibleTypeSelected === 1) {
      this.activityProgramming.nameTeacher = null;
      this.activityProgramming.documentTeacher = null;
      this.showExternalResponsible = false;
      this.showUniversityTeacher = true;
    } else if (this.responsibleTypeSelected === 2) {
      this.activityProgramming.nameTeacher = null;
      this.activityProgramming.documentTeacher = null;
      this.showExternalResponsible = true;
      this.showUniversityTeacher = false;
    } else if (this.responsibleTypeSelected === 3) {
      this.activityProgramming.nameTeacher = null;
      this.activityProgramming.documentTeacher = null;
      this.showExternalResponsible = false;
      this.showUniversityTeacher = false;
    }
  }

  /**
  * Método para verificar si devuelve verdadero un elemento de una matriz.
  * @param {ActivityProgramming} activityProgramming
  * @param {number} id
  * @returns true if check
  */
  check(activityProgramming: ActivityProgramming, id: number): boolean {
    if (activityProgramming.programmingActCurriculumId.some(x => x === id.toString())) {
      return true;
    } else {
      return false;
    }
  }

  /**
  * Método para seleccionar los planes de estudio.
  * @param {number} id id de plan de estudio.
  */
  selectCurriculum(id: number): void {
    const idCurriculum = this.activityProgramming.programmingActCurriculumId.filter(x => x === id.toString());
    if (idCurriculum.length > 0) {
      const index = this.activityProgramming.programmingActCurriculumId.indexOf(idCurriculum[0]);
      this.activityProgramming.programmingActCurriculumId.splice(index, 1);
    } else {
      this.activityProgramming.programmingActCurriculumId.push(id.toString());
    }
    this.checkCurriculum();
    this.verifyChangeActivityProgram();
  }

  // Método para verificar el plan de estudio seleccionado.
  private checkCurriculum(): void {
    if (this.activityProgramming.programmingActCurriculumId.some(x => x === '2') &&
      this.activityProgramming.programmingActCurriculumId.length === 1) {
      this.showCurriculums = !this.showCurriculums;
    } else {
      this.showCurriculums = false;
    }
  }

  /**
   * Método para cancelar y regresar al formulario princial de Actividades Programadas.
   */
  cancel(): void {
    this.clean();
    this.showFormNewActivityProgramming = false;
    this.showListProgrammedActivities = true;
  }

  // Método para limpiar.
  clean(): void {
    this.activitiesProgramming = [];
    this.teacherInfoSearched = null;
    this.notFoundTeacher = false;
    this.idActivityType = 0;
    this.idModality = '';
    this.idCampus = '';
    this.searchTerm = '';
    this.activitiesProgramming = null;
    this.activityProgramming.documentTeacher = null;
  }

  /**
  * Método para validar que la tecla presionada sea un número.
  * @param {any} event
  */
  keyPress(event: any): void {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      if (event.charCode !== 13) {
        event.preventDefault();
      }
    }
  }

  /**
  * Busca las programaciones de actividad que cumplan con los filtros elegidos.
  * - Tipo de actividad.
  * - Periodo
  * - Modalidad.
  * - Campus
  * - perido académico
  */
  findActivities(): void {
    this.isBusy = true;
    this.idCampus = this.idModality === 'UVIR' ? 'V00' : this.idCampus;
    this.activityProgrammingService.getProgrammingAcivitiesFilterBy(this.idActivityType, this.idModality,
      this.idCampus, this.searchTerm)
      .finally(() => this.isBusy = false)
      .subscribe(
        (activitiesProgramming) => {
          this.activitiesProgramming = activitiesProgramming;
          this.activitiesProgramming.forEach(element => {
            element.campus = this.campus.filter(x => x.id === element.idCampus)[0];
            element.department = this.departments.filter(x => x.id === element.idModality)[0];
          });
        },
        (error) => console.log(error));
  }

  /**
   * Método para limpiar el combobox de Campus por tipo de actividad seleccionado.
   */
  cleanCampus(): void {
    if (this.activityProgramming.idModality === this.backofficeConfigService.DEPA_VIR) {
      this.activityProgramming.idCampus = 'V00';
    } else {
      this.activityProgramming.idCampus = '';
    }
  }

  /**
   * Método para limpiar el combobox de modalidad por tipo de actividad seleccionado.
   */
  cleanModality(): void {
    this.activityProgramming.idModality = '';
    this.cleanCampus();
  }
}
