import { Component, OnInit } from '@angular/core';

import { ConvalidationTypeService } from '../../shared/services/extracurricular-activities/convalidation-type.service';
import { RecognitionService } from '../../shared/services/extracurricular-activities/recognition.service';
import { StudentSearchService } from '../../shared/services/extracurricular-activities/student-search.service';

import { ConvalidationType } from '../../shared/services/extracurricular-activities/convalidation-type.model';
import { StudentSearch } from '../../shared/services/extracurricular-activities/student-search.model';
import { Recognition } from '../../shared/services/extracurricular-activities/recognition.model';

import { Message } from 'primeng/primeng';
import { ConfigService } from '../../config/config.service';

@Component({
  selector: 'recognition',
  templateUrl: './recognition.component.html',
  providers: [RecognitionService, ConvalidationTypeService, StudentSearchService]
})

export class VUCRecognitionComponent implements OnInit {
  // Variables de modelos
  convalidationTypes: ConvalidationType[];  // Tipos de convalidaciones (reconocimientos)
  evidency: any;                            // Ruta de la evidencia
  foundStudents: StudentSearch[];           // Estudiantes encontrados
  isBusy: boolean;                          // Variable de estado
  isShowStudentsList: boolean;              // Muestra la lista de estudiantes ingresados
  locale: any;                              // Configuración de los date picker
  notFoundDnis: string[] = [];              // Dni's de estudiantes no encontrados
  primeMsg: Message[];                      // Mensaje de respuesta
  recognition: Recognition;                 // Modelo del reconocimiento
  students: string[];                       // Dni's de estudiantes
  studentsByDni: string;                    // Estudiante por dni.
  idConvalidation: number;                  // Variable para guardar el tipo de convalidación
  wata: number;

  constructor(private convalidationService: RecognitionService, private configService: ConfigService,
    private convalidationTypeService: ConvalidationTypeService,
    private studentSearchService: StudentSearchService) { }

  ngOnInit(): void {
    this.getConvalidationType();
    this.clean();
    this.locale = this.configService.getCalendarEs();
    this.wata = (new Date()).getFullYear();
    this.isBusy = false;
  }

  /**
  * Regitra un reconocimiento
  */
  create(): void {
    this.isBusy = true;
    this.foundStudents.forEach(x => this.recognition.pidmStudents.push(x.idPerson));
    this.convalidationService.create(this.recognition)
      .finally(() => this.isBusy = false)
      .subscribe(
        (data) => {
          if (data !== null && data !== undefined) {
            this.primeMsg = [{
              detail: 'Se registró correctamente el reconocmiento.',
              severity: 'success',
              summary: 'Registro de reconocimiento'
            }];
            this.clean();
          } else {
            this.primeMsg = [{
              detail: 'No se ha podido registrar el/los reconocimientos, vuelva a intentarlo.',
              severity: 'error',
              summary: 'Registro de reconocimiento'
            }];
          }
        },
        (error) => console.log(error));
  }

  /**
  * Busca estudiantes (por dni separados por un espacio).
  */
  searchStudents(): void {
    this.keyPress(event);
    this.isBusy = true;
    this.foundStudents = this.notFoundDnis = [];
    this.studentSearchService.getStudentsByDni(this.students.filter(x => x.length === 8))
      .finally(() => this.isBusy = false)
      .subscribe(
        (estudiantelist) => {
          this.foundStudents = estudiantelist;
          this.students.forEach(student => {
            if (!this.foundStudents.some(x => x.dni === student)) {
              this.notFoundDnis.push(student);
            }
          });
        },
        (error) => console.log(error));
  }

  /**
  * Obtiene los tipos de convalidaciones.
  *  - Extracurricular.
  *  - Proyección social.
  */
  getConvalidationType(): void {
    this.isBusy = true;
    this.convalidationTypeService.get()
      .finally(() => this.isBusy = false)
      .subscribe(
        (convalidationType) => this.convalidationTypes = convalidationType,
        (error) => console.log(error));
  }

  /**
   * Limpia el input cuando selecciona proyección social
   */
  resetCredit(): void {
    if (this.recognition.idConvalidationType === 2) {
      this.recognition.credit = '';
    }
  }

  /**
  * Convierte la evidencia seleccionada(zip) a base 64.
  * @param {File[]} files Files.
  */
  onChange(files: File[]): void {
    const pathEvidency = files[0];
    if (pathEvidency === undefined || pathEvidency === null) {
      this.evidency = null;
      this.recognition.pathCode = null;
      return;
    }
    const splitName = pathEvidency.name.split('.');
    const extensionFile = splitName[splitName.length - 1];
    if (extensionFile.toLowerCase() === 'rar' || extensionFile.toLowerCase() === 'zip') {
      const myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        this.evidency = myReader.result;
        this.recognition.pathCode = this.evidency.toString().replace('data:application/x-zip-compressed;base64,', '')
          .replace('data:application/zip;base64,', '').replace('data:application/octet-stream;base64,', '');
      };
      myReader.readAsDataURL(pathEvidency);
    } else {
      this.evidency = null;
      this.recognition.pathCode = null;
      this.primeMsg = [{
        detail: 'El formato del archivo seleccionado no esta permitido',
        severity: 'warn',
        summary: 'Evidencia'
      }];
      return;
    }
  }

  /**
  * Valida que la tecla presionada sea un número
  * @param {any} event Evento
  */
  keyPress(event: any) {
    const pattern = /[0-9 ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      if (event.charCode !== 13) {
        event.preventDefault();
      }
    }
  }

  /**
  * Separa los dni's ingresados.
  * @returns dni Cadena de dni's separados por espacio.
  */
  splitDni(): void {
    if (this.studentsByDni === null || this.studentsByDni === '') { return; }
    this.students = this.studentsByDni.split(' ')
      .filter((estudianteDni) => estudianteDni !== '')
      .filter((estudianteDni, position, self) => self.indexOf(estudianteDni) === position);
  }

  /**
  * Inicializa el modelo de reconocimiento.
  */
  resetRecognition(): void {
    this.recognition = {
      id: 0,
      description: '',
      idConvalidationType: 0,
      pathCode: '',
      pathEvidency: '',
      credit: '',
      pidmStudents: [],
      reason: '', recognitionDate: new Date
    };
  }

  /**
  * Limpia las variables y modelos.
  */
  clean(): void {
    this.isBusy = false;
    this.studentsByDni = '';
    this.students = null;
    this.foundStudents = [];
    this.resetRecognition();
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList(): void {
    this.isShowStudentsList = !this.isShowStudentsList;
  }
}
