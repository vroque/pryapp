import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'vuc-layout-report',
  templateUrl: './vuc-layout-report.component.html'
})

export class VUCLayoutReportComponent implements OnInit {

  menuOptions: PRSMenu[] = [];

  // menus de reporte de Actividades de tipo Taller
  menu1: PRSMenu = {
    name: 'Talleres',
    description: 'Reporte de talleres',
    icon: 'fa fa-cubes',
    method: Blocks.Activities
  };
  menu2: PRSMenu = {
    name: 'Horarios',
    description: 'Reporte de horarios',
    icon: 'fa fa-calendar',
    method: Blocks.ProgrammingActivities
  };
  menu3: PRSMenu = {
    name: 'Matriculados',
    description: 'Reporte de matriculados',
    icon: 'fa fa-users',
    method: Blocks.Student
  };

  // estados de visualización de formularios
  showActivities: boolean;
  showProgrammingActivity: boolean;
  showStudent: boolean;

  ngOnInit(): void {
    // Inicializar los menús para reporte de Actividades de tipo Taller
    this.menuOptions.push(this.menu1, this.menu2, this.menu3);
    this.showActivities = this.showProgrammingActivity = this.showStudent = false;
  }

  /**
   * Recibe el valor que envia el componente hijo (los componentes de los reportes)
   * @param $event False por defecto para desactivar el componente actual usado
   */
  receiveMessage($event: boolean) {
    this.showActivities = this.showProgrammingActivity = this.showStudent = $event;
  }

  /**
   * Cambia al reporte solicitado
   * @param block Bloque que se activará
   */
  changeMenu(block: Blocks): void {
    switch (block) {
      case Blocks.Activities:
        this.showStudent = this.showProgrammingActivity = false;
        this.showActivities = true;
        break;
      case Blocks.ProgrammingActivities:
        this.showStudent = this.showActivities = false;
        this.showProgrammingActivity = true;
        break;
      case Blocks.Student:
        this.showProgrammingActivity = this.showActivities = false;
        this.showStudent = true;
        break;
      default:
        this.showStudent = this.showProgrammingActivity = this.showActivities = false;
        break;
    }
  }
}

interface PRSMenu {
  name: string;
  description: string;
  icon: string;
  method: Blocks;
}

enum Blocks {
  Activities,
  ProgrammingActivities,
  Student
}
