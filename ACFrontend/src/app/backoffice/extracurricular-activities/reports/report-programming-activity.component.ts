import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Message } from 'primeng/primeng';

import { ActivityProgramming } from '../../../shared/services/extracurricular-activities/activity-programming.model';
import { ActivityProgrammingService } from '../../../shared/services/extracurricular-activities/activity-programming.service';
import { BackofficeConfigService } from '../../config/config.service';
import { TermService } from '../../../shared/services/extracurricular-activities/term.service';

@Component({
  selector: 'vuc-report-programming-activity',
  templateUrl: './report-programming-activity.component.html',
  providers: [BackofficeConfigService, ActivityProgrammingService, TermService]
})

export class VUCReportProgrammingActivityComponent implements OnInit {
  @Output() messageEvent = new EventEmitter<boolean>();

  activitiesProgramming: ActivityProgramming[];        // Lista de programaciones.
  isBusy: boolean;                                     // Estado que indica que el servicio esta ocupado.
  primeMsg: Message[];                                 // Mensaje.
  searchTerm: string;                                  // Varible para buscar por un periodo académico
  terms: any[] = [];                                   // Lista de periodos académicos
  thereIsPendingActivity: boolean;                     // Manejar estado de botón habilitar / deshabilitar.

  constructor(private config: BackofficeConfigService, private activityProgramService: ActivityProgrammingService,
  private termService: TermService) { }

  ngOnInit(): void {
    this.getTerm();
    this.searchTerm = '';
  }

  /**
   * Retorna a las opciones de reportes
   */
  back() {
    this.messageEvent.emit(false);
  }

  /**
   * Obtiene los periodos académicos
   */
  getTerm(): void {
    this.isBusy = true;
    this.termService.getAll()
      .finally(() => this.isBusy = false)
      .subscribe(
        (terms) => this.terms = terms,
        (error) => console.log(error));
  }

  /**
  * Obtenemos programaciones de actividades solo de tipo taller por un periodo.
  */
  getProgrammingActivitiesByTerm(): void {
    this.isBusy = true;
    this.activityProgramService.getProgrammingActivitiesByTerm(this.searchTerm)
      .finally(() => this.isBusy = false)
      .subscribe(
        (activitiesProgramming) => {
          this.activitiesProgramming = activitiesProgramming;
          this.thereIsPendingActivity = activitiesProgramming.some(x => x.replicated === false);
          if (activitiesProgramming.some(x => x.documentTeacher === null)) {
            this.primeMsg = [{
              detail: 'No se habilitara el botón de proceso y descarga, hasta que se asigne a un responsable',
              severity: 'warn',
              summary: 'Asigne a un responsable para continuar con la descarga',
            }];
            this.thereIsPendingActivity = false;
          }
        },
        (error) => console.log(error));
  }

  /**
  *  Método para cambiar el estado de pendiente a replicado de las programaciones de las
  *  actividades de tipo taller y a la vez descarga en un excel de esas actividades replicadas
  */
  replicateProgrammingActivity(): void {
    const programmingActivityReplicate = this.activitiesProgramming.filter(x => x.replicated === false);
    this.activityProgramService.replicateProgrammingActivity()
      .finally(() => this.isBusy = false)
      .subscribe(
        (data) => {
          if (programmingActivityReplicate) {
            this.primeMsg = [{
              detail: 'Se ha generado satisfactoriamente el excel',
              severity: 'success',
              summary: 'Excel'
            }];
            const datos: any[] = programmingActivityReplicate.map(
              (dato) => {
                return {
                  'MATERIA': 'ASEX',
                  'NRC': dato.nrc,
                  'CÓDIGO': dato.activity.bannerCode,
                  'ACTIVIDAD - NOMBRE CORTO': dato.activity.shortName.toUpperCase(),
                  'ACTIVIDAD - NOMBRE LARGO': dato.activity.name.toUpperCase(),
                  'CAMPUS': dato.idCampus.toUpperCase(),
                  'MODALIDAD': dato.idModality.toUpperCase(),
                  'ESCUELA': '06',
                  'INICIO DE ACTIVIDAD': dato.startDateActivity,
                  'FIN DE ACTIVIDAD': dato.endDateActivity,
                  'PERIODO': dato.startPeriod,
                  'DÍA DE SESIÓN DE LA ACTIVIDAD': dato.sessionDay.toUpperCase(),
                  'LUGAR DE LA ACTIVIDAD': dato.sessionPlace.toUpperCase(),
                  'HORA DE INICIO DE LA ACTIVIDAD': dato.startHour,
                  'HORA DE FIN DE LA ACTIVIDAD': dato.endHour,
                  'CANTIDAD MÁX. DE VACANTES': dato.maxQuantityVacancy,
                  'DNI DEL DOCENTE:': dato.documentTeacher,
                  'NOMBRE DEL DOCENTE': dato.nameTeacher.toUpperCase()
                };
              });
            this.config.exportAsExcelFile(datos, 'Reporte de Actividades Programadas de tipo Taller');
            this.getProgrammingActivitiesByTerm();
            this.thereIsPendingActivity = false;
          }
        },
        (error) => console.log(error));
  }
}
