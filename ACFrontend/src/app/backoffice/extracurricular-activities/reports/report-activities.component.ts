import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Message } from 'primeng/primeng';

import { Activity } from '../../../shared/services/extracurricular-activities/activity.model';
import { ActivityService } from '../../../shared/services/extracurricular-activities/activity.service';
import { BackofficeConfigService } from '../../config/config.service';
import { ReportActivityService } from '../../../shared/services/extracurricular-activities/report-activity.service';

@Component({
  selector: 'vuc-report-activities',
  templateUrl: './report-activities.component.html',
  providers: [ReportActivityService, BackofficeConfigService, ActivityService]
})

export class VUCReportActivitiesComponent implements OnInit {
  @Output() messageEvent = new EventEmitter<boolean>();

  activitiesWorkshop: Activity[] = [];               // Lista de actividades.
  isBusy: boolean;                                   // Estado que indica que el servicio esta ocupado.
  thereIsPendingActivities: boolean;                 // Manejar estado de botón habilitar / deshabilitar.
  primeMsg: Message[];                               // Mensaje.

  constructor(private reportActivityService: ReportActivityService, private config: BackofficeConfigService,
  private activityService: ActivityService) { }

  ngOnInit(): void {
    this.getReportWorshopActivity();
  }

  /**
   * Retorna a las opciones de reportes
   */
  back() {
    this.messageEvent.emit(false);
  }

  /**
  * Obtenemos actividades solo de tipo taller, conjuntamente con sus datos.
  */
  getReportWorshopActivity(): void {
    this.isBusy = true;
    this.reportActivityService.getActivitiesReplicated()
      .finally(() => this.isBusy = false)
      .subscribe(
        (activitiesWorkshop) => {
          this.activitiesWorkshop = activitiesWorkshop;
          this.thereIsPendingActivities = activitiesWorkshop.some(x => x.isReplicated === false);
        },
        (error) => console.log(error));
  }

  /**
  *  Método para cambiar el estado de pendiente a replicado de actividades de
  *  tipo taller y a la vez descarga en un excel de esas actividades replicadas
  */
  replicateWorkshopActivity(): void {
    const activityReplicate = this.activitiesWorkshop.filter(x => x.isReplicated === false);
    this.activityService.replicateActivity()
      .finally(() => this.isBusy = false)
      .subscribe(
        (data) => {
          if (activityReplicate) {
            this.primeMsg = [{
              detail: 'Se ha generado el excel',
              severity: 'success',
              summary: 'Excel'
            }];
            const activities: any[] = activityReplicate.map(
              (activity) => {
                return {
                  'MATERIA': 'ASEX',
                  'CÓDIGO': activity.bannerCode,
                  'NOMBRE CORTO DE LA ASIGNATURA': activity.shortName.toUpperCase(),
                  'NOMBRE LARGO DE LA ASIGNATURA': activity.name.toUpperCase(),
                  'ESCUELA': '06',
                  'DIVISIÓN': 'UCCI',
                  'STATUS': 'A',
                  'CRÉDITOS': activity.credits,
                  'HORAS DE TEORÍA': '0',
                  'HORAS DE LABORATORIO': '2',
                  'CONTACTO': '2',
                  'LÍMITE': '2',
                  'STATUS REPT': 'R',
                  'NIVEL': 'PG',
                  'MODO DE CALIFICACIÓN': 'S',
                  'PREDEFINIDO': '1',
                  'TIPO DE HORARIO': 'PPL',
                  'MÉTODO EDUCATIVO': 'P',
                  'CARGA DE TRABAJO': '2'
                };
              });
            this.config.exportAsExcelFile(activities, 'Reporte de talleres pendientes de replicar');
            this.getReportWorshopActivity();
          }
        },
        (error) => console.log(error));
  }
}
