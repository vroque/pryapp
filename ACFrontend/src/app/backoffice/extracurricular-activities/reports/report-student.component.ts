import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BackofficeConfigService } from '../../config/config.service';

import { EnrolledProgramStudent } from '../../../shared/services/extracurricular-activities/enrolled-program-student.model';
import { EnrolledStudentService } from '../../../shared/services/extracurricular-activities/enrolled-student.service';
import { TermService } from '../../../shared/services/extracurricular-activities/term.service';

@Component({
  selector: 'vuc-report-student',
  templateUrl: './report-student.component.html',
  providers: [EnrolledStudentService, BackofficeConfigService, TermService]
})

export class VUCReportStudentComponent implements OnInit {
  @Output() messageEvent = new EventEmitter<boolean>();

  isBusy: boolean;                                   // Estado que indica que el servicio esta ocupado.
  reportEnrolleds: EnrolledProgramStudent[];         // Reporte de estudiantes matriculados en "Taller"
  searchTerm: string;                                // Variable para buscar por un periodo académico
  terms: any[] = [];                                 // Lista de periodos académicos

  constructor(private enrolledStudentService: EnrolledStudentService, private config: BackofficeConfigService,
    private termService: TermService) { }

  ngOnInit(): void {
    this.getTerm();
    this.searchTerm = '';
  }

  /**
   * Retorna a las opciones de reportes
   */
  back() {
    this.messageEvent.emit(false);
  }

  /**
   * Obtiene los periodos académicos
   */
  getTerm(): void {
    this.isBusy = true;
    this.termService.getAll()
      .finally(() => this.isBusy = false)
      .subscribe(
        (terms) => this.terms = terms,
        (error) => console.log(error));
  }

  /**
   * Obtiene estudiantes matriculados de talleres
   */
  getEnrolledStudentsActivity(): void {
    this.isBusy = true;
    this.enrolledStudentService.getEnrolledStudentsActivity(this.searchTerm)
      .finally(() => this.isBusy = false)
      .subscribe((
        reportEnrolleds) => this.reportEnrolleds = reportEnrolleds,
        (error) => console.log(error));
  }

  /**
   * Formato para descargar matriculado en taller
   */
  downloadEnrolled(): void {
    const students: any[] = this.reportEnrolleds.map(
      (student) => {
        return {
          'DNI': student.documentNumber,
          'PIDM': student.pidmStudent,
          'IDALUMNO': student.idAlumno,
          'NRC': student.nrc,
          'PERIODO ACADÉMICO': student.startPeriod
        };
      });
    this.config.exportAsExcelFile(students, 'Reporte de matriculados en talleres');
  }
}
