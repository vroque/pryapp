import { AcademicProfile } from '../../shared/services/academic/academic.profile';
import { ActivatedRoute } from '@angular/router';
import { BackofficeConfigService } from '../../backoffice/config/config.service';
import { Component, OnInit } from '@angular/core';
import { Student } from '../../shared/services/academic/student';
import { StudentFactory } from '../../shared/services/academic/student.factory';
import { StudentService } from '../../shared/services/academic/student.service';

@Component({
  providers: [StudentService, BackofficeConfigService],
  selector: 'search-student',
  styles: ['.widget-user .box-footer { padding-top: 49px; } .widget-user .widget-user-image {max-height: 85px;}'],
  templateUrl: './dashboard-academic-life.component.html',
})
export class DashboardAcademicLifeComponent implements OnInit {

  dni: string;
  listMenu: any;
  tracingActive = false;
  student: Student;
  errorMessage = '';
  showMenuProfiles = false;
  showMenuViews = false;
  views: any;
  configFile: any;

  constructor(
    private studentService: StudentService,
    private route: ActivatedRoute,
    private config: BackofficeConfigService,
  ) {
    this.configFile = this.config;
  }

  ngOnInit() {
    this.getStudent(this.route.snapshot.params.dni);
    this.dni = this.route.snapshot.params.dni;
    this.views = [
      {
        active: true,
        component: 'student-compliance',
        name: 'Vista compacta',
        },
      {
        active: false,
        component: 'tracing',
        name: 'Seguimiento de solicitudes',
      },
      {
        active: false,
        component: 'orientation-curriculum',
        name: 'Avance académico',
      },
    ];
  }

  getStudent(studentDni: string) {
    this.studentService.find(studentDni.trim(), 'student_id')
    .subscribe(
      (student) => this.setStudent(student),
      (error) =>  this.errorMessage = error,
    );
  }

  setStudent(student: Student) {
    this.student = student;
    StudentFactory.getInstance().set(student);
  }

  getRequests() {
    this.tracingActive = !this.tracingActive;
    console.log('obteniendo las solicitudes realizadas por el estudiante');
  }

  changeProfile(estudent: AcademicProfile) {
    this.student.profile = estudent;
    StudentFactory.getInstance().set(this.student);
    this.showMenuProfiles = false;
    this.changeView('student-compliance');
  }

  changeView(component: string) {
    for (const view of this.views) {
      if (view.component === component) {
        view.active = true;
      } else {
        view.active = false;
      }
    }
    this.showMenuViews = false;
  }
}
