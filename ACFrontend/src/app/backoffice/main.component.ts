import {Component, OnInit} from '@angular/core';
import {ConfigService} from '../config/config.service';
import {Menu} from '../shared/menu';

declare let $: any;

@Component({
  selector: 'bo-main',
  templateUrl: './main.component.html',
  providers: [ConfigService],
})
export class MainComponent implements OnInit {

  baseUri: string;
  menus: Menu[];
  filterInput: any;

  constructor(private configService: ConfigService) {
    this.baseUri = (<any>window).url_base || '';
    this.menus = <Menu[]>JSON.parse(JSON.stringify((<any>window).choices)).filter(f => f.position === 'all' || f.position === 'main') || [];
    for (const menu of this.menus) {
      menu.smenus = menu.smenus.filter(f => f.position === 'all' || f.position === 'main') || [];
    }
  }

  ngOnInit(): void {
    // tooltip
    $(document).ready(function (): void {
      $('[data-toggle=\'tooltip\']').tooltip();
    });
  }

  range(n: number): number[] {
    const result: number[] = [];
    for (let i = 0; i < n; i += 1) {
      result.push(i);
    }
    return result;
  }
}
