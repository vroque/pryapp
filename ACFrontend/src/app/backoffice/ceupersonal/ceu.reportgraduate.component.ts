declare var $: any;
import {BackofficeConfigService} from '../config/config.service';
import {Component, OnInit} from '@angular/core';
import {GraduateSearchService} from '../../shared/services/academic/graduate.search.service';
import {Graduate} from '../../shared/services/academic/graduate';
@Component({
  selector : 'ceu-reportgraduate',
  templateUrl: './ceu.reportgraduate.component.html',
  providers: [GraduateSearchService, BackofficeConfigService]
})
export class CEUReportGraduateComponent implements OnInit {

  error_message: string;
  graduates: Graduate[];
  search_data: any[];

  constructor(
    private graduateService: GraduateSearchService,
    private config: BackofficeConfigService,
  ) {
    this.graduates = [];
    this.search_data = [
      {name: 'Bachiller', has: true, hasnt: true},
      {name: 'Título', has: true, hasnt: true},
      {name: 'Proyección', has: true, hasnt: true},
      {name: 'Idioma', has: true, hasnt: true},
      {name: 'Práctica', has: true, hasnt: true},
    ];
  }
  ngOnInit() {
    // this method is need even if is empty
  }
  changeValue(index: number, property: string) {
    this.search_data[index][property] =  !this.search_data[index][property];
  }
  searchGraduates() {
    this.graduateService.searchForRequirements(this.search_data)
      .subscribe(
        (graduates) => this.graduates = graduates,
        (error) => this.error_message = error,
        () => console.log('graduados:', this.graduates),
      );
  }
  table2excel() {
    $('#table-graduates').table2excel({
      exclude: '.excludeThisClass',
      filename: 'Reporte de Egresados', // do not include extension
      name: 'reportgraduate',
    });
  }
}
