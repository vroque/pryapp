import { Component, OnInit, Input } from "@angular/core";
import { BackofficeConfigService } from "../config/config.service";
import { PPPractices } from "../../shared/services/opp/practices";
import { PPPracticesService } from "../../shared/services/opp/practices.service";

@Component({
  selector: "opp-management",
  templateUrl: "./opp.management.component.html",
  providers: [
    BackofficeConfigService,
    PPPracticesService
  ]
})

export class OPPManagemenrComponent implements OnInit {
  error_message: string;
  status: string = "pre"; // pre, on, err
  search_type: string = "student_names";
  state: string = "pre"; //"on" "post" "err"
  practices: Array<PPPractices> = [];
  constructor(
      private config: BackofficeConfigService,
      private practicesService: PPPracticesService,
  ) { }

  ngOnInit(): void { }

  setInbox(event, type: string ): void {
    this.search_type = type;
  }

  listPractices(ext: any): void {
    this.practicesService.query(ext)
      .subscribe(
        (practices) => this.practices = practices,
        (error) =>  this.error_message = error,
        () => console.log("practices -> ", this.practices),
      );
  }
}
