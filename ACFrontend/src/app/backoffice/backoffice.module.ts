import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routing } from './config/routing';

// primeng modules
import { CalendarModule } from 'primeng/primeng';
import { ChartModule } from 'primeng/primeng';
import { FileUploadModule } from 'primeng/primeng';
import { InputSwitchModule } from 'primeng/primeng';
import { AutoCompleteModule } from 'primeng/primeng';
import { ChipsModule } from 'primeng/primeng';
import { SpinnerModule } from 'primeng/primeng';
import { InputMaskModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';

// guards
import { HasAccessGuard } from '../shared/guards/has-access.guard';

// Google Analytics
import { Angulartics2Module } from 'angulartics2';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';

// apoderapp
import { ApoderappAccessControlComponent } from './apoderapp/apoderapp-access-control.component';
import { ApoderappNewApoderadoComponent } from './apoderapp/apoderapp-new-apoderado.component';
import { ApoderappStudentDetailComponent } from './apoderapp/apoderapp-student-detail.component';
import { ApoderappComponent } from './apoderapp/apoderapp.component';

// components
import { BackofficeComponent } from './backoffice.component';
import { MainComponent } from './main.component';
import { SidebarMenuComponent } from './sidebar-menu.component';

// shared componentents
import { CAUBreadcrumbComponent } from '../shared/components/breadcrumb.component';
import { LoadingComponent } from '../shared/components/loading.component';
import { Page401Component } from '../shared/components/page401.component';
import { Page404Component } from '../shared/components/page404.component';
import { RedirectComponent } from '../shared/components/redirect.component';
import { BORequestAbstractComponent } from '../shared/components/request/boRequestAbstract.component';
import { ACFileLoaderComponent } from '../shared/components/request/file-loader.component';
import { RequestFlowComponent } from '../shared/components/request/flow.component';
import { GenerateDocumentComponent } from '../shared/components/request/generateDocument.component';
import { SearchRequestComponent } from '../shared/components/request/search-request.component';
import { CauMenusUrlComponent } from '../shared/components/menus.url.component';

// cau-personal
import { CAUBlockComponent } from './caupersonal/block.component';
import { CAUGraduateProofComponent } from './caupersonal/graduate.proof.component';
// tslint:disable-next-line:max-line-length
import { CAUProofOfEnrollmentPromotional1stEnrollmentComponent } from './caupersonal/proof-of-enrollment/proof-of-enrollment-promotional-1st-enrollment.component';
// tslint:disable-next-line:max-line-length
import { CAUProofOfEnrollmentPromotionalBatchComponent } from './caupersonal/proof-of-enrollment/proof-of-enrollment-promotional-batch.component';
import { CAUProofOfEnrollmentPronabecBatchComponent } from './caupersonal/proof-of-enrollment/proof-of-enrollment-pronabec-batch.component';
import { CAUProofOfEnrollmentPronabecComponent } from './caupersonal/proof-of-enrollment/proof-of-enrollment-pronabec.component';
import { CAUProofOfEntryPromotionalBatchComponent } from './caupersonal/proof-of-entry/proof-of-entry-promotional-batch.component';
import { CAUProofOfEntryPromotionalComponent } from './caupersonal/proof-of-entry/proof-of-entry-promotional.component';
import { CAUProofOfGraduationComponent } from './caupersonal/proof-of-graduation.component';
import { CAUReportCardPronabecBatchComponent } from './caupersonal/report-card/report-card-pronabec-batch.component';
import { CAURequestComponent } from './caupersonal/request.component';
import { CAURequestListComponent } from './caupersonal/requestlist.component';
import { CAURequestReportComponent } from './caupersonal/requestReport.component';
import { CAUSignerDocumentComponent } from './caupersonal/signers.component';
// tslint:disable-next-line:max-line-length
import { CAUStudiesCertificatePromotionalBatchComponent } from './caupersonal/studies-certificate/studies-certificate-promotional-batch.component';
import { CAUStudiesCertificateComponent } from './caupersonal/studies-certificate/studies-certificate.component';

// Rutas para RAU - Registros Academicos
import { RAURequestComponent } from './raupersonal/rau.request.component';
import { RAURequestListComponent } from './raupersonal/rau.requestlist.component';

// Rutas documentos RAU
import { RAUProof3510PeriodComponent } from './raupersonal/proof-3510/proof.3510.period.component';
import { RAUProof3510PromotionalComponent } from './raupersonal/proof-3510/proof.3510.promotional.component';
import { RAUOfficialTranscriptComponent } from './raupersonal/official.transcript.component';

// buw-personal
import { BUWCategorizationComponent } from './buw/categorization.component';

// gyt
// ceu-personal
import { CEUReportGraduateComponent } from './ceupersonal/ceu.reportgraduate.component';

// opp-personal
import { OPPManagemenrComponent } from './opp/opp.management.component';

// cou-personal
import { DeuTransferComponent } from '../shared/components/transfer/deu-transfer.component';
import { TransferListComponent } from '../shared/components/transfer/transferList.component';
import { TransferProcessComponent } from '../shared/components/transfer/transferProcess.component';
import { TransferReportComponent } from '../shared/components/transfer/transferReport.component';
import { COURequestComponent } from './cou/cou.request.component';
import { COURequestListComponent } from './cou/cou.requestlist.component';

// cic
import { CICCaieBatchComponent } from './languages-center/cic-records-administration/cic-caie-batch.component';
import { CICCaieExternalComponent } from './languages-center/cic-records-administration/cic-caie-external.component';
import { CICCaieInstituteComponent } from './languages-center/cic-records-administration/cic-caie-institute.component';
import { CICCaieInternalComponent } from './languages-center/cic-records-administration/cic-caie-internal.component';
import { CICCaieOnDemandComponent } from './languages-center/cic-records-administration/cic-caie-on-demand.component';
import { CICCaiePromotionalComponent } from './languages-center/cic-records-administration/cic-caie-promotional.component';
import { CICDiplomasComponent } from './languages-center/cic-records-administration/cic-diplomas.component';
import { CICExtraLanguageRecordComponent } from './languages-center/cic-extra/cic-extra-language-record.component';
import { CICExtraStudentLanguageRecordComponent } from './languages-center/cic-extra/cic-extra-student-language-record.component';
import { CICProofOfEnrollmentComponent } from './languages-center/cic-records-administration/cic-proof-of-enrollment.component';
import { CICStudiesCertificateComponent } from './languages-center/cic-records-administration/cic-studies-certificate.component';
import { CICReportOfDocumentsIssuedComponent } from './languages-center/cic-reports/cic-report-of-documents-issued.component';
// tslint:disable-next-line:max-line-length
import { CICReportOfStudentsEnrolledBySchoolComponent } from './languages-center/cic-reports/cic-report-of-students-enrolled-by-school.component';
import { CICReportOfStudentsEnrolledComponent } from './languages-center/cic-reports/cic-report-of-students-enrolled.component';
import { CICRequestListComponent } from './languages-center/cic-request/cic-request-list.component';
import { CICSettingsCaieLanguageLevelComponent } from './languages-center/cic-settings/cic-settings-caie-language-level.component';

// VUC
import { VUCManagementComponent } from '../backoffice/extracurricular-activities/management.component';
import { VUCRecognitionComponent } from './extracurricular-activities/recognition.component';
import { VUCActivityProgrammingComponent } from '../backoffice/extracurricular-activities/activity-programming.component';
import { VUCRegisterSelectedStudentComponent } from './extracurricular-activities/register-selected-student.component';
import { VUCRegisterTracingComponent } from './extracurricular-activities/register-tracing.component';
import { VUCReportProgrammingActivityComponent } from './extracurricular-activities/reports/report-programming-activity.component';
import { VUCLayoutReportComponent } from './extracurricular-activities/reports/vuc-layout-report.component';
import { VUCReportActivitiesComponent } from './extracurricular-activities/reports/report-activities.component';
import { VUCReportStudentComponent } from './extracurricular-activities/reports/report-student.component';

// deu-personal
import { DEURequestComponent } from './deu/deu.request.component';
import { DEURequestListComponent } from './deu/deu.requestlist.component';

// Libro de reclamaciones (LDR) CAU
import { LDRAnswerPageClaim } from './claimsbook/cau/answer-page-claim.component';
import { LDRDerivatePageClaims } from './claimsbook/cau/derive-page.component';
import { LDRDisableDerivation } from './claimsbook/cau/disable-derivation.component';
import { LDRDismissedPage } from './claimsbook/cau/dismissed-page.component';
import { LDRBookActionsComponent } from './claimsbook/cau/page-book-list.component';
import { LDRPageDetailsComponent } from './claimsbook/cau/page-details.component';
import { LDRBookRegisterComponent } from './claimsbook/cau/register.component';

// LDR Defensor Universitario
import { LDRDetailsPageClaimUD } from './claimsbook/university-defender/details-page-claim.component';
import { LDRListPagesClaimsUD } from './claimsbook/university-defender/list-page-claims.component';

// LDR general
import { LDRAnswerClaimBook } from './claimsbook/claim-answer.component';
import { LDRClaimsDerivateComponent } from './claimsbook/claims-derivates.component';
import { LDRClaimsDetailsComponent } from './claimsbook/claims-details.component';
import { LDRRangeDates } from './claimsbook/range-dates.component';
import { LDRUploadFiles } from './claimsbook/upload-files.component';

// Libro de reclamaciones (LDR) Area Involucrada
import { LDRAnswerPageClaimIA } from './claimsbook/involvedArea/answerPageClaim.component';
import { LDRDerivatePageClaimIA } from './claimsbook/involvedArea/derivatePageClaim.component';
import { LDRDetailsPageClaimAI } from './claimsbook/involvedArea/detailsPageClaim.component';
import { LDRListPagesClaimsIAComponent } from './claimsbook/involvedArea/listPageClaims.component';

// LDR Area Legal
import { LDRDerivateIA } from './claimsbook/legal-area/derivate.component';
import { LDRDetailsPageClaimLA } from './claimsbook/legal-area/details-page-claim.component';
import { LDRListPagesClaimsLAComponent } from './claimsbook/legal-area/list-page-claims.component';
import { LDRResolvedPageClaimLA } from './claimsbook/legal-area/resolved-page-claim.component';

// LDR Reportes
import { LDRReport1 } from './claimsbook/reports/report1.component';
import { LDRReport2 } from './claimsbook/reports/report2.component';
import { LDRReport3 } from './claimsbook/reports/report3.component';
import { LDRReport4 } from './claimsbook/reports/report4.component';
import { LDRReport5 } from './claimsbook/reports/report5.component';

// administrator
import { ADMComponent } from './adm/adm.component';
import { ADMMenuComponent } from './adm/menu.component';
import { ADMPermissionComponent } from './adm/permission.component';
import { ADMSettingsComponent } from './adm/settings/settings.component';
import { ADMSettingsTypeComponent } from './adm/settings-type/settings-type.component';
import { ADMSurveysComponent } from './adm/surveys/surveys.component';
import { ADMTermComponent } from './adm/terms/term.component';
import { PeriodDateComponent } from './adm/period-date/period-date.component';
import { FormPeriodDateComponent } from './adm/period-date/form-period-date.component';
import { ADMTermsAndConditionsComponent } from './adm/terms-and-conditions/terms-and-conditions.component';
// tslint:disable-next-line:max-line-length
import { ADMTermsAndConditionsForEnrollmentComponent } from './adm/terms-and-conditions-for-enrollment/terms-and-conditions-for-enrollment.component';

// tools
import { ToolsChangeStudentComponent } from './tools/change.student.component';

// pipes
import { CaseFunctionSpecial } from '../shared/pipes/case-function-special';
import { CapitalizeCasePipe } from '../shared/pipes/capitalize-case.pipe';
import { LowerCasePipe } from '../shared/pipes/lower-case.pipe';
import { TitleCasePipe } from '../shared/pipes/title-case.pipe';
import { UpperCasePipe } from '../shared/pipes/upper-case.pipe';
import { TimeTo12Pipe } from '../shared/pipes/time-to-12.pipe';
import { JsonParsePipe } from '../shared/pipes/json-parse.pipe';
import { LoopFilterPipe } from '../shared/pipes/loopfilter.pipe';

// Module academic life
import { StudentComplianceComponent } from '../shared/components/student-compliance/student-compliance.component';
import { OrientationCurriculumComponent } from '../frontdesk/orientation/curriculum.component';
import { TracingListComponent } from '../frontdesk/tracing/tracing.list.component';
import { CardStudentComponent } from '../shared/components/search/card-student.component';
import { SearchStudentComponent } from '../shared/components/search/search-student.component';
import { DashboardAcademicLifeComponent } from './academic-life/dashboard-academic-life.component';

// componentes de modulos

// services
import { ConfigService } from '../config/config.service';

// Convenios
import { BenefitAgreementDetailComponent } from './benefit/agreement-detail.component';
import { BenefitAgreementComponent } from './benefit/agreement.component';
import { BenefitAsignBenefitComponent } from './benefit/asign-benefit.component';
import { BenefitNewAgreementComponent } from './benefit/new-agreement.component';

// Learning Assessment
import { OEALearningAssessmentDatesComponent } from './learning-assessment/learning.assessment.dates.component';
import { OEAProgramNrcComponent } from './learning-assessment/program.nrc.component';
import { OEAProgramNrcByDatesComponent } from './learning-assessment/program.nrc.by.dates.component';
import { OEAReportSubstituteProgrammedComponent } from './learning-assessment/reports/report.substitute.programmed.component';
import { OEAReportNrcProgrammedComponent } from './learning-assessment/reports/report.nrc.programmed.component';
import { OEAsubstituteExamStudentComponent } from './learning-assessment/substitute.exam.student.component';

// add locale for spanish
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

// Display block component
import { DisplayBlockComponent } from './display-block/display-block.component';

import { SurveysComponent } from '../shared/components/surveys/surveys.component';
import { TermsAndConditionsComponent } from '../shared/components/terms-and-conditions/terms-and-conditions.component';

// Graduates
import { GraduatesReportComponent } from './graduates/graduates-report.component';
import { SearchGraduatesComponent } from './graduates/search-graduates.component';
import { GraduatesComplianceComponent } from './graduates/graduates-compliance.component';

// Reclamo de notas
// General
import { OEAModifyNoteClaimComponent } from './note-claim/modify.note.claim.component';
import { OEAViewNoteClaimComponent } from '../shared/components/note-claim/viewNoteClaim.component';
// CAU
import { OEACauNoteClaimComponent } from './caupersonal/note-claim/note.claim.component';
import { OEANoteClaimDeriveTeacherComponent } from './note-claim/derive.to.teacher.component';
// Docente
import { OEANoteClaimTeacherComponent } from './note-claim/teacher.component';
// RAU
import { OEARauNoteClaimComponent } from './raupersonal/note-claim/note.claim.component';
import { OEANoteClaimFinalizeReportComponent } from './note-claim/finalize.note.claim.component';
// OEA
import { OEAOeaNoteClaimComponent } from './learning-assessment/note-claim/note.claim.component';
import { OEANoteClaimValidateReportComponent } from './note-claim/validate.report.component';
// OGD
import { OEAOgdNoteClaimComponent } from './teacher-management/note-claim/note.claim.component';
// Reports
import { OEANoteClaimReportComponent } from './note-claim/reports/general.component';

// Proyectos UC
import { PRYListProjectComponent } from './continental-project/adm/list-project.component';
import { PRYDetailProjectComponent } from './continental-project/adm/detail-project.component';
import { PRYMyProjectsComponent } from './continental-project/teacher/my-projects.component';
import { PRYCreateEditProjectComponent } from './continental-project/teacher/create-edit-project.component';

// Incidents
import { IncidentCategoryComponent } from './incidents/incident-category/incident-category.component';
import { IncidentDetailComponent } from './incidents/incident-detail/incident-detail.component';
import { IncidentLogTypeComponent } from './incidents/incident-log-type/incident-log-type.component';
import { IncidentStatusComponent } from './incidents/incident-status/incident-status.component';
import { IncidentSubcategoryComponent } from './incidents/incident-subcategory/incident-subcategory.component';
// tslint:disable-next-line:max-line-length
import { IncidentSubcategoryFunctionalUnitComponent } from './incidents/incident-subcategory-functional-unit/incident-subcategory-functional-unit.component';
import { IncidentTypeComponent } from './incidents/incident-type/incident-type.component';
import { IncidentsComponent } from './incidents/incidents.component';
import { PRYInscribedComponent } from './continental-project/teacher/inscribed.component';
import { PRYBackCategoriesComponent } from './continental-project/teacher/categories.component';
import { PRYBackDetailProjectComponent } from './continental-project/teacher/detail-project.component';
import { PRYBackListProjectComponent } from './continental-project/teacher/list-project.component';

registerLocaleData(localeEs, 'es');

// enableProdMode();
@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    JsonpModule,
    routing,
    BrowserAnimationsModule,
    CalendarModule,
    ChartModule,
    FileUploadModule,
    InputSwitchModule,
    AutoCompleteModule,
    ChipsModule,
    SpinnerModule,
    InputMaskModule,
    GrowlModule,
    Angulartics2Module.forRoot([Angulartics2GoogleAnalytics], {pageTracking: {clearQueryParams: true}}),
  ],
  declarations: [
    BenefitAgreementComponent,
    BenefitAgreementDetailComponent,
    BenefitNewAgreementComponent,
    BenefitAsignBenefitComponent,
    BackofficeComponent,
    MainComponent,
    SidebarMenuComponent,
    RedirectComponent,
    ApoderappComponent,
    ApoderappAccessControlComponent,
    ApoderappNewApoderadoComponent,
    ApoderappStudentDetailComponent,
    CAURequestListComponent,
    CAURequestComponent,
    CAUGraduateProofComponent,
    CAUBlockComponent,
    CAUSignerDocumentComponent,
    CAURequestReportComponent,
    CAUProofOfEnrollmentPronabecComponent,
    CAUProofOfEnrollmentPromotional1stEnrollmentComponent,
    CAUProofOfEnrollmentPronabecBatchComponent,
    CAUProofOfEnrollmentPromotionalBatchComponent,
    CAUProofOfEntryPromotionalComponent,
    CAUProofOfEntryPromotionalBatchComponent,
    CAUProofOfGraduationComponent,
    CAUReportCardPronabecBatchComponent,
    CAUStudiesCertificateComponent,
    CAUStudiesCertificatePromotionalBatchComponent,
    RAURequestListComponent,
    RAURequestComponent,
    RAUProof3510PeriodComponent,
    RAUProof3510PromotionalComponent,
    RAUOfficialTranscriptComponent,
    CEUReportGraduateComponent,
    OPPManagemenrComponent,
    COURequestComponent,
    COURequestListComponent,
    TransferReportComponent,
    TransferListComponent,
    TransferProcessComponent,
    CICCaieBatchComponent,
    CICCaieExternalComponent,
    CICCaieInstituteComponent,
    CICCaieInternalComponent,
    CICCaieOnDemandComponent,
    CICCaiePromotionalComponent,
    CICDiplomasComponent,
    CICExtraLanguageRecordComponent,
    CICExtraStudentLanguageRecordComponent,
    CICProofOfEnrollmentComponent,
    CICReportOfDocumentsIssuedComponent,
    CICReportOfStudentsEnrolledComponent,
    CICReportOfStudentsEnrolledBySchoolComponent,
    CICRequestListComponent,
    CICSettingsCaieLanguageLevelComponent,
    CICStudiesCertificateComponent,
    DEURequestComponent,
    DEURequestListComponent,
    BUWCategorizationComponent,
    // VUC
    VUCManagementComponent,
    VUCRecognitionComponent,
    VUCActivityProgrammingComponent,
    VUCRegisterSelectedStudentComponent,
    VUCRegisterTracingComponent,
    VUCLayoutReportComponent,
    VUCReportProgrammingActivityComponent,
    VUCReportStudentComponent,
    VUCReportActivitiesComponent,
    // LDR-CAU
    LDRBookRegisterComponent,
    LDRBookActionsComponent,
    LDRPageDetailsComponent,
    LDRDerivatePageClaims,
    LDRDismissedPage,
    LDRAnswerPageClaim,
    LDRDisableDerivation,
    // LDR Area Involucrada
    LDRListPagesClaimsIAComponent,
    LDRDetailsPageClaimAI,
    LDRDerivatePageClaimIA,
    LDRAnswerPageClaimIA,
    // LDR Area Legal
    LDRListPagesClaimsLAComponent,
    LDRDetailsPageClaimLA,
    LDRDerivateIA,
    LDRResolvedPageClaimLA,
    // LDR Defensor Universitario
    LDRListPagesClaimsUD,
    LDRDetailsPageClaimUD,
    // LDR general
    LDRClaimsDetailsComponent,
    LDRClaimsDerivateComponent,
    LDRAnswerClaimBook,
    LDRUploadFiles,
    LDRRangeDates,
    // LDR Reportes
    LDRReport1,
    LDRReport2,
    LDRReport3,
    LDRReport4,
    LDRReport5,

    // Learning Assessment
    OEALearningAssessmentDatesComponent,
    OEAProgramNrcComponent,
    OEAProgramNrcByDatesComponent,
    OEAReportSubstituteProgrammedComponent,
    OEAReportNrcProgrammedComponent,
    OEAsubstituteExamStudentComponent,

    ADMComponent,
    ADMPermissionComponent,
    PeriodDateComponent,
    FormPeriodDateComponent,
    ADMMenuComponent,
    ADMTermsAndConditionsComponent,
    ADMTermsAndConditionsForEnrollmentComponent,
    ADMSettingsComponent,
    ADMSettingsTypeComponent,
    ADMSurveysComponent,
    ADMTermComponent,
    // tools
    BORequestAbstractComponent,
    ToolsChangeStudentComponent,
    RequestFlowComponent,
    GenerateDocumentComponent,
    ACFileLoaderComponent,
    LoadingComponent,
    Page401Component,
    Page404Component,
    CAUBreadcrumbComponent,
    SearchRequestComponent,
    CauMenusUrlComponent,
    // Academic life
    SearchStudentComponent,
    CardStudentComponent,
    DashboardAcademicLifeComponent,
    StudentComplianceComponent,
    TracingListComponent,
    OrientationCurriculumComponent,
    // pipes
    JsonParsePipe,
    LoopFilterPipe,
    UpperCasePipe,
    LowerCasePipe,
    TimeTo12Pipe,
    TitleCasePipe,
    CapitalizeCasePipe,
    DeuTransferComponent,
    // Display Block component
    DisplayBlockComponent,
    SurveysComponent,
    TermsAndConditionsComponent,
    // Egresados
    GraduatesReportComponent,
    SearchGraduatesComponent,
    GraduatesComplianceComponent,
    // Reclamo de notas
    OEAModifyNoteClaimComponent,
    OEANoteClaimDeriveTeacherComponent,
    OEACauNoteClaimComponent,
    OEAViewNoteClaimComponent,
    OEANoteClaimTeacherComponent,
    OEARauNoteClaimComponent,
    OEAOeaNoteClaimComponent,
    OEANoteClaimValidateReportComponent,
    OEAOgdNoteClaimComponent,
    OEANoteClaimFinalizeReportComponent,
    OEANoteClaimReportComponent,
    // Proyectos UC
    PRYListProjectComponent,
    PRYDetailProjectComponent,
    PRYMyProjectsComponent,
    PRYCreateEditProjectComponent,
    PRYInscribedComponent,
    PRYBackCategoriesComponent,
    PRYBackDetailProjectComponent,
    PRYBackListProjectComponent,
    // Incidents
    IncidentDetailComponent,
    IncidentCategoryComponent,
    IncidentLogTypeComponent,
    IncidentStatusComponent,
    IncidentSubcategoryComponent,
    IncidentSubcategoryFunctionalUnitComponent,
    IncidentTypeComponent,
    IncidentsComponent,
  ],
  providers: [
    HasAccessGuard,
    ConfigService,
    CaseFunctionSpecial
  ],
  bootstrap: [
    BackofficeComponent,
    SidebarMenuComponent,
    CAUBreadcrumbComponent,
  ],
})
export class BackofficeModule {
}
