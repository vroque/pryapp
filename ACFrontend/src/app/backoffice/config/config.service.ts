import { Injectable } from '@angular/core';
import { utils, WorkBook, WorkSheet, writeFile } from 'xlsx';
import { Menu } from '../../shared/menu';
import { MenuService } from '../../shared/menu.service';
@Injectable()
export class BackofficeConfigService {
  // tslint:disable-next-line:variable-name
  _apiURI: string;
  _ERRORS_MESSAGES: any;
  _DOCUMENT_STATES: any[];
  _LDR_STATUS: any[];
  _LDR_FILE_STATUS: any[];
  _LDR_TYPE_ORIGIN_PAGE: any[];
  _OFFICES_NAMES: any[];

  PAYMENT_GATEWAY: string = (window as any).paymetgateway;
  STATE_CANCEL = 0;
  STATE_INITIAL = 1;
  STATE_PAYMENT_PENDING = 2;
  STATE_ATENTION = 3;
  STATE_DELIVERY_PENDING = 4;
  STATE_APROVAL_PENDING = 5;
  STATE_PROCESS_PENDING = 6;
  STATE_DOCUMENT_RECEPTION = 7;
  STATE_DOCUMENT_VERIFICATION = 8;
  STATE_CONVALIDATION = 9; // lo hace el dir esc de mentira
  STATE_CONVALIDATION_VERIFICATION = 10; // no existe
  STATE_CONVALIDATION_ACTION = 11; // aqui cau cou convalida
  STATE_CAP_PENDING = 12;
  STATE_RESOLUTION_PENDING = 13;
  STATE_FINAL = 100;

  // LDR tipo origen reclamo
  LDR_ORIGIN_DIGITAL = 0;
  LDR_ORIGIN_PHYSICAL = 1;

  // LDR status page claimsbook
  LDR_STATE_PAGE_DISMISSED = 0;
  LDR_STATE_PAGE_PENDING = 1;
  LDR_STATE_PAGE_DERIVATED = 2;
  LDR_STATE_PAGE_ANSWERED = 4;
  LDR_STATE_PAGE_RESOLVED = 99;
  LDR_STATE_PAGE_FINALIZED = 100;

  // LDR estate answer pages
  LDR_STATE_ANSWER_REJECTED = 0;
  LDR_STATE_ANSWER_WAIT = 1;
  LDR_STATE_ANSWER_ACCEPTED = 99;
  LDR_STATE_ANSWER_ACCEPTED_FINISH = 100;

  // LDR state files
  LDR_STATE_FILE_CREATE = 1;
  LDR_STATE_FILE_DERIVATED = 2;
  LDR_STATE_FILE_DISMISSED = 3; // NO SE UTILIZA
  LDR_STATE_FILE_ANSWER = 4;
  LDR_STATE_FILE_RESOLVED = 99;
  LDR_STATE_FILE_FINALIZED = 100;

  // LDR type involved conllaborator derivation
  LDR_TYPE_INVOLVED_BOSS = 1;
  LDR_TYPE_INVOLVED_COLLABORATOR = 2;

  // LDR state staff derivation
  LDR_STATE_STAFF_DERIVATED = 1;
  LDR_STATE_STAFF_NOT_DERIVATED = 0;

  // LDR state derivation_area
  LDR_STATE_DERIVATE_DISMISSED = 0;
  LDR_STATE_DERIVATE_PENDING = 1;

  LDR_STATE_DERIVATE_RESOLVED = 99;
  LDR_STATE_DERIVATE_FINALIZED = 100;

  // LDR Tipo de derivación
  LDR_TYPE_DERIVATE_INVOLVED_AREA = 0;
  LDR_TYPE_DERIVATE_LEGAL_AREA = 1;

  // ESTADO FANTASMA PARA RECONOCER QUE SE ESTA RECHAZANDO LA RESPUESTA DE LA DERIVACIÓN
  LDR_STATE_DERIVATE_RECHAZED_ANSWER = 200;

  // LDR número de días máximo a resolver un reclamo
  LDR_DAYS_COMPLETION_CLAIM = 15;

  // LDR Type User
  LDR_TYPE_USER_AD = 0;
  LDR_TYPE_USER_INVOLVED_AREA = 1;
  LDR_TYPE_USER_LEGAL_AREA = 2;

  // document Types
  _DOCUMENT_TYPES: any[];
  DOC_STUDIES_CERTIFICATE = 1;
  DOC_STUDIES_PROOF = 2;
  DOC_ACADEMIC_HISTORY = 3;

  // number DOC_ENROLLMENT_PROOF = 4;
  DOC_ENROLLMENT_CONSOLIDATE = 5;
  DOC_ENTRY_PROOF = 6;
  DOC_ENROLLMENT_PROOF = 7;
  DOC_SPECIAL_PROOF = 8;
  DOC_3510SUPERIOR_PROOF = 9;

  // a DOC_ENROLLMENT_PROOF: number = 10;
  // a DOC_ENROLLMENT_PROOF: number = 11;
  DOC_REPORT_CARD = 12;

  // a DOC_ENROLLMENT_PROOF: number = 13;
  DOC_STUDENT_CARD = 14;
  DOC_PROMOTIONAL_3510SUPERIOR_PROOF = 22;
  DOC_PROMOTIONAL_STUDIES_CERTIFICATE = 23;
  DOC_AVERAGE_STUDIES_PROOF = 24; // constancia de estudios con promeido acumulado
  DOC_CUSTOM_STUDIES_PROOF = 25; // constancia de estudios personalizada
  DOC_CULM_CAREER_STUDIES_PROOF = 26;
  DOC_CONVALIDATION = 33;
  DOC_ENROLLMENT_PROOF_WITH_DATE = 51;
  DOC_STUDIES_PROOF_WITH_DATE = 52;
  DOC_CONVALIDATION_EXTENSION = 54;

  // Documentos CIC
  DOC_CIC_PROOF_OF_ENROLLMENT = 55;
  DOC_CIC_STUDIES_CERTICIFATES = 56;
  DOC_CIC_DIPLOMA = 57;
  DOC_CIC_CAIE = 58;
  DOC_CIC_CAIE_EXT = 59;
  DOC_CIC_CAIE_INST = 60;

  // documentos para egresados
  DOC_DUPLICATE_PROOF_GRADUATE = 62;

  // oficinas
  OFFICE_CAW = 0;
  OFFICE_CAU = 1;
  OFFICE_ADM = 2;
  OFFICE_RAU = 3;
  OFFICE_DFI = 4;
  OFFICE_DFCE = 5;
  OFFICE_COU = 8;
  OFFICE_DEU = 9;
  OFFICE_PRS = 10;
  OFFICE_OPP = 11;
  OFFICE_SFU = 12;
  OFFICE_CAJ = 13;
  OFFICE_OEA = 14;
  OFFICE_OGD = 15;

  // Estados de Alumno
  STATE_POSTULANT = 1; // "POSTULANTE";
  STATE_ENROLMENT = 2; // "INGRESANTE";
  STATE_STUDENT = 3; // "MATRICULADO";
  STATE_GRADUATE = 4; // "EGRESADO";
  STATE_BACHELOR = 5; // "BACHILLER";
  STATE_DEGREE = 6; // Titulado con Grado

  CIC_CAIE_LANGUAGE_LEVEL: string[];

  // CIC modalities
  CIC_M_SUPER_INTENSIVO = 'S';
  CIC_M_INTENSIVO = 'I';
  CIC_M_REGULAR = 'R';
  CIC_M_SUPER_INTENSIVE_ONLINE = 'L';
  CIC_MODALITIES: any[] = [
    { id: this.CIC_M_SUPER_INTENSIVO, name: 'Super Intensivo' },
    { id: this.CIC_M_INTENSIVO, name: 'Intensivo' },
    { id: this.CIC_M_REGULAR, name: 'Regular' },
    { id: this.CIC_M_SUPER_INTENSIVE_ONLINE, name: 'Super Intensivo Online' },
  ];

  CIC_L_ENGLISH = 'I01';
  CIC_L_PROTUGESE = 'I02';
  CIC_L_ITALIAN = 'I03';
  CIC_LANGUAGES: any[] = [
    { id: this.CIC_L_ENGLISH, name: 'Inglés' },
    { id: this.CIC_L_PROTUGESE, name: 'Portugués' },
    { id: this.CIC_LANGUAGES, name: 'Italiano' },
  ];

  DEPA_REG = 'UREG';
  DEPA_VIR = 'UVIR';
  DEPA_PGT = 'UPGT';

  DEPARTAMENT_NAMES: any[] = [
    {id: this.DEPA_REG, name: this.getDepartmentName(this.DEPA_REG), partPeriod: 'R'},
    {id: this.DEPA_VIR, name: this.getDepartmentName(this.DEPA_VIR), partPeriod: 'V'},
    {id: this.DEPA_PGT, name: this.getDepartmentName(this.DEPA_PGT), partPeriod: 'W'},
  ];

  // Valores reclamo de notas
  OEA_TYPE_NOTE_CLAIM_REQUALIFICATION = 'RECALIFICACION';
  OEA_TYPE_NOTE_CLAIM_RECTIFICATION = 'RECTIFICACION';
  OEA_TYPES_NOTE_CLAIM: any[] = [
    {id: this.OEA_TYPE_NOTE_CLAIM_REQUALIFICATION, name: 'Recalificación'},
    {id: this.OEA_TYPE_NOTE_CLAIM_RECTIFICATION, name: 'Rectificación'},
  ];

  // Estados de reclamo de nota
  OEA_STATUS_NOTE_CLAIM_REGISTERED = 1;
  OEA_STATUS_NOTE_CLAIM_CAS = 2;
  OEA_STATUS_NOTE_CLAIM_TEACHER = 3;
  OEA_STATUS_NOTE_CLAIM_INVOLVED_AREA = 4;
  OEA_STATUS_NOTE_CLAIM_RAU = 5;
  OEA_STATUS_NOTE_CLAIM_FINALIZED = 6;

  OEA_STATUS_NOTE_CLAIM_REPORT_PENDING = 7;
  OEA_STATUS_NOTE_CLAIM_REPORT_ACCEPTED = 8;
  OEA_STATUS_NOTE_CLAIM_REPORT_REJECT = 9;

  OEA_STATUS_NOTE_CLAIM = [
    {id: this.OEA_STATUS_NOTE_CLAIM_REGISTERED, name: 'Registrado'},
    {id: this.OEA_STATUS_NOTE_CLAIM_CAS, name: 'Pendiente de validación CAS'},
    {id: this.OEA_STATUS_NOTE_CLAIM_TEACHER, name: 'Pendiente de informe docente'},
    {id: this.OEA_STATUS_NOTE_CLAIM_INVOLVED_AREA, name: 'Pendiente de validar informe'},
    {id: this.OEA_STATUS_NOTE_CLAIM_RAU, name: 'Pendiente de Registros Académicos'},
    {id: this.OEA_STATUS_NOTE_CLAIM_FINALIZED, name: 'Finalizado'},
  ];

  OEA_STATUS_NOTE_CLAIM_REPORT = [
    {id: this.OEA_STATUS_NOTE_CLAIM_REPORT_PENDING, name: 'Pendiente de revisión'},
    {id: this.OEA_STATUS_NOTE_CLAIM_REPORT_ACCEPTED, name: 'Aprobado'},
    {id: this.OEA_STATUS_NOTE_CLAIM_REPORT_REJECT, name: 'No Aprobado'},
  ];

  constructor() {
    this._DOCUMENT_STATES = [
      { id: this.STATE_CANCEL, name: 'Cancelado', complete: 'Cancelado', icon: 'fa fa-times' },
      { id: this.STATE_INITIAL, name: 'Registrado', complete: 'Registrado', icon: 'fa fa-pencil-square-o' },
      { id: this.STATE_PAYMENT_PENDING, name: 'Por pagar', complete: 'Pagado', icon: 'fa fa-btc' },
      { id: this.STATE_ATENTION, name: 'Por atender', complete: 'Atendido', icon: 'fa fa-users' },
      { id: this.STATE_DELIVERY_PENDING, name: 'Por entregar', complete: 'Entregado', icon: 'fa fa-paper-plane' },
      { id: this.STATE_APROVAL_PENDING, name: 'Por aprobar', complete: 'Aprobado', icon: 'fa fa-check-square-o' },
      { id: this.STATE_PROCESS_PENDING, name: 'Por procesar', complete: 'Procesado', icon: 'fa fa-truck' },
      { id: this.STATE_DOCUMENT_RECEPTION, name: 'Recepción', complete: 'Recepcionados', icon: 'fa fa-files-o' },
      { id: this.STATE_DOCUMENT_VERIFICATION, name: 'Verificando doc.', complete: 'Verificados', icon: 'fa fa-check-square-o' },
      { id: this.STATE_CONVALIDATION, name: 'Convalidando', complete: 'convalidado', icon: 'fa fa-check-square-o' },
      { id: this.STATE_CONVALIDATION_VERIFICATION, name: 'Verif. conva.', complete: 'Verificada', icon: 'fa fa-check-square-o' },
      { id: this.STATE_CONVALIDATION_ACTION, name: 'Procesando', complete: 'Procesado', icon: 'fa fa-file-o' },
      { id: this.STATE_CAP_PENDING, name: 'Cerrando Proc.', complete: 'Proc. Cerrado', icon: 'fa fa-file-o' },
      { id: this.STATE_RESOLUTION_PENDING, name: 'P. d\' Resolución', complete: 'Resolución', icon: 'fa fa-file-o' },
      { id: this.STATE_FINAL, name: 'Finalizado', complete: 'Finalizado', icon: 'fa fa-flag-checkered' },
    ];
    this._LDR_STATUS = [
      { id: this.LDR_STATE_PAGE_PENDING, name: 'Nuevos Reclamos', complete: 'Pendiente', icon: 'fa fa-file-o' },
      { id: this.LDR_STATE_PAGE_DERIVATED, name: 'Derivados a Áreas', complete: 'Derivado', icon: 'fa fa-check-square-o' },
      { id: this.LDR_STATE_PAGE_ANSWERED, name: 'Derivados a Área Legal', complete: 'Derivado', icon: 'fa fa-check-square-o' },
      { id: this.LDR_STATE_PAGE_RESOLVED, name: 'Revisados por Área Legal', complete: 'Resuelto', icon: 'fa fa-flag-checkered' },
      { id: this.LDR_STATE_PAGE_FINALIZED, name: 'Casos cerrados', complete: 'Finalizado', icon: 'fa fa-flag-checkered' },
      { id: this.LDR_STATE_PAGE_DISMISSED, name: 'Desestimado', complete: 'Desestimado', icon: 'fa fa-flag-checkered' },
    ];
    this._LDR_FILE_STATUS = [
      { id: this.LDR_STATE_FILE_CREATE, name: 'Creación', complete: 'Creación', icon: 'fa fa-file-o' },
      { id: this.LDR_STATE_FILE_DERIVATED, name: 'Derivación', complete: 'Derivación', icon: 'fa fa-check-square-o' },
      { id: this.LDR_STATE_FILE_FINALIZED, name: 'Finalización', complete: 'Finalización', icon: 'fa fa-flag-checkered' },
    ];
    this._LDR_TYPE_ORIGIN_PAGE = [
      { id: this.LDR_ORIGIN_DIGITAL, name: 'Digital', complete: 'Digital', icon: 'fa fa-file-o' },
      { id: this.LDR_ORIGIN_PHYSICAL, name: 'Fisico', complete: 'Fisico', icon: 'fa fa-check-square-o' },
    ];

    this._DOCUMENT_TYPES = [
      { id: this.DOC_STUDIES_CERTIFICATE, name: 'Certificado de estudios' },
      { id: this.DOC_STUDIES_PROOF, name: 'Constancia de estudios' },
      { id: this.DOC_ACADEMIC_HISTORY, name: 'Historial academico' },
      { id: this.DOC_ENROLLMENT_CONSOLIDATE, name: 'Consolidado de matrícula' },
      { id: this.DOC_ENTRY_PROOF, name: 'Constancia de ingreso' },
      { id: this.DOC_ENROLLMENT_PROOF, name: 'Constancia de matrícula' },
      { id: this.DOC_SPECIAL_PROOF, name: 'Constancia especial' },
      { id: this.DOC_3510SUPERIOR_PROOF, name: 'Constancia de 3°, 5° y 10°' },
      { id: this.DOC_REPORT_CARD, name: 'Boleta de notas' },
      { id: this.DOC_STUDENT_CARD, name: 'Duplicado de carné' },
      { id: this.DOC_PROMOTIONAL_3510SUPERIOR_PROOF, name: 'Constancia de 3°, 5° y 10° promocional' },
      { id: this.DOC_PROMOTIONAL_STUDIES_CERTIFICATE, name: 'Certificado de estudios promocional' },
      { id: this.DOC_AVERAGE_STUDIES_PROOF, name: 'Constancia de estudios con promedio acumulado' },
      { id: this.DOC_CUSTOM_STUDIES_PROOF, name: 'Constancia de estudios personalizada' },
      { id: this.DOC_CULM_CAREER_STUDIES_PROOF, name: 'Constancia de estudios de carrera culminada' },
      { id: this.DOC_CONVALIDATION, name: 'Convalidación de asignaturas' },
      { id: this.DOC_ENROLLMENT_PROOF_WITH_DATE, name: 'Constancia de matrícula (con fecha de período)' },
      { id: this.DOC_STUDIES_PROOF_WITH_DATE, name: 'Constancia de estudios (con fecha de período)' },
      { id: this.DOC_CONVALIDATION_EXTENSION, name: 'Ampliación de convalidación' },
      { id: this.DOC_CIC_PROOF_OF_ENROLLMENT, name: 'Constancia de matrícula CIC' },
      { id: this.DOC_CIC_STUDIES_CERTICIFATES, name: 'Certificado de estudios CIC' },
      { id: this.DOC_CIC_DIPLOMA, name: 'Diploma CIC' },
      { id: this.DOC_CIC_CAIE, name: 'Constancia de Idioma Extranjero - UC' },
      { id: this.DOC_CIC_CAIE_EXT, name: 'Constancia de Idioma Extranjero - Uso Externo' },
      { id: this.DOC_CIC_CAIE_INST, name: 'Constancia de Idioma Extranjero - Instituto' },
      { id: this.DOC_DUPLICATE_PROOF_GRADUATE, name: 'Duplicado de Constancia de egresado' },
    ];
    this._OFFICES_NAMES = [
      { id: this.OFFICE_CAW, name: 'Página Web CAU' },
      { id: this.OFFICE_CAU, name: 'Centro de atención' },
      { id: this.OFFICE_ADM, name: 'Admisión' },
      { id: this.OFFICE_RAU, name: 'Registros académicos' },
      { id: this.OFFICE_DFI, name: 'DFI' },
      { id: this.OFFICE_DFCE, name: 'DFCE' },
      { id: this.OFFICE_COU, name: 'Convalidaciones' },
      { id: this.OFFICE_DEU, name: 'Directores del programa' },
      { id: this.OFFICE_PRS, name: 'Proyección social' },
      { id: this.OFFICE_OPP, name: 'Practica profesionales' },
      { id: this.OFFICE_SFU, name: 'SFU' },
      { id: this.OFFICE_CAJ, name: 'Caja' },
    ];
    this._ERRORS_MESSAGES = {
      solicitud_error: 'Lo sentimos tenemos problemas tecnicos, estamos trabajando para resolverlos.',
      solicitud_issue: 'Lo sentimos tenemos problemas tecnicos, estamos trabajando para resolverlos.',
    };
    this.CIC_CAIE_LANGUAGE_LEVEL = ['Básico', 'Intermedio', 'Intermedio I'];
    this.CIC_LANGUAGES = [
      { id: 'I01', name: 'Inglés' },
      { id: 'I02', name: 'Portugués' },
      { id: 'I03', name: 'Italiano' },
      { id: 'I04', name: 'Francés' },
    ];
  }

  /**
   * Get language name
   * @param {string} id
   * @returns {any}
   */
  getLangName(id: string): any {
    for (const item of this.CIC_LANGUAGES) {
      if (item.id === id) {
        return item.name;
      }
    }
  }

  error_messages(): any {
    return this._ERRORS_MESSAGES;
  }

  getDocumentState(id: number): any {
    for (const item of this._DOCUMENT_STATES) {
      if (item.id === id) {
        return item;
      }
    }
  }

  getOfficeName(id: number): any {
    for (const item of this._OFFICES_NAMES) {
      if (item.id === id) {
        return item.name;
      }
    }
  }

  getDocumentName(id: number): any {
    for (const item of this._DOCUMENT_TYPES) {
      if (item.id === id) {
        return item.name;
      }
    }
  }

  getDepartmentName(id: string): string {
    let name: string;
    switch (id) {
      case 'UPGT':
        name = 'SEMIPRESENCIAL (GT)';
        break;
      case 'UVIR':
        name = 'SEMIPRESENCIAL (EV)';
        break;
      case 'UREG':
      default:
        name = 'PRESENCIAL';
        break;
    }
    return name;
  }

  getLanguage(id: string): string {
    for (const item of this.CIC_LANGUAGES) {
      if (item.id === id) {
        return item.name;
      }
    }
  }
  getCICModality(id: string): string {
    for (const item of this.CIC_MODALITIES) {
      if (item.id === id) {
        return item.name;
      }
    }
  }

  /**
   * Exportar Array a Excel.
   * @param data Array a exportar
   * @param fileName Nombre de archivo
   */
  exportAsExcelFile(data: any[], fileName: string): void {
    const dateObj = new Date();
    const dateName: string = ' '
      + (dateObj.getDate().toString()) + '-'
      + (dateObj.getMonth() + 1).toString() + '-'
      + (dateObj.getFullYear().toString()) + ' '
      + (dateObj.getHours().toString()) + '_'
      + (dateObj.getMinutes().toString()) + '_'
      + (dateObj.getSeconds().toString());

    const worksheet: WorkSheet = utils.json_to_sheet(data);
    const workbook: WorkBook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
    writeFile(workbook, fileName + dateName + '.xlsx', { bookType: 'xlsx', type: 'buffer' });
  }

  /**
   * Obtener Uris de una uri padre.
   * @param moduleName Nombre de modulo
   * @param uris Lista de uris
   * @param menuService Servicio de menu
   */
  getRouteOfModuleAndUri(moduleName: string, uris: string[], menuService: MenuService): Array<Menu> {
    const allMenus: Array<Menu> = menuService.query();
    let menus: Array<Menu> = [];
    for (let i = 0; i < allMenus.length; i++) {
      const mod = allMenus[i];
      if (mod.moduleName === moduleName) {
        menus = mod.smenus.filter(f => f.uri != null && f.uri.trim() !== '');
        uris.forEach(uri => {
          menus.forEach(menu => {
            if (menu.uri === uri && menu.smenus.length > 0) {
              menus = menu.smenus.filter(f => f.uri != null && f.uri.trim() !== '');
            }
          });
        });
        break;
      }
    }
    return menus;
  }

  /**
   * Obtener Uris de una uri padre.
   * @param moduleName Nombre de modulo
   * @param uris Lista de uris
   * @param menuService Servicio de menu
   */
  getRouteOfNameAndUri(name: string, uris: string[], menuService: MenuService): Array<Menu> {
    const allMenus: Array<Menu> = menuService.query();
    let menus: Array<Menu> = [];
    for (let i = 0; i < allMenus.length; i++) {
      const mod = allMenus[i];
      if (mod.name === name) {
        menus = mod.smenus.filter(f => f.uri != null && f.uri.trim() !== '');
        uris.forEach(uri => {
          menus.forEach(menu => {
            if (menu.uri === uri && menu.smenus.length > 0) {
              menus = menu.smenus.filter(f => f.uri != null && f.uri.trim() !== '');
            }
          });
        });
        break;
      }
    }
    return menus;
  }

  /**
   * Obtener uris por ruta
   * @param uris Ruta
   * @param menuService MEnu service
   */
  getRoutesByRoute(route: string, menuService: MenuService): Menu[] {
    let allMenus: Menu[] = menuService.query();
    const rutas = (route.replace('/backoffice/', '').replace('/frontdesk/', '').replace('/parent/', '')).split('/');
    allMenus = menuService.query();
    rutas.forEach(ruta => {
      let menusTmp: Array<Menu> = [];
      if (allMenus != null) {
        allMenus.forEach(menu => {
          if (ruta === menu.uri && menu.smenus != null) {
            menusTmp = menu.smenus;
          }
        });
      }
      allMenus = menusTmp;
    });
    return allMenus;
  }

  /**
   * Rellenar cadena con caracter
   * @param str Cadena
   * @param caracter Caracter con el cual se va a completar
   * @param size De que tamaño
   */
  completeStringWithCaracter(str: string, caracter: string, size: number): string {
    str = str.toString();
    caracter = caracter.toString();
    for (let index = str.length; index < size; index++) {
      str = caracter.concat(str);
    }
    return str;
  }

  /**
   * Convertir a fecha local
   * @param data fecha a convertir
   */
  convertToUTC(data: Date): Date {
    data = new Date(data as any);
    const utc: any = new Date(data.getTime() - data.getTimezoneOffset() * 60000).toUTCString();
    return utc;
  }
}
