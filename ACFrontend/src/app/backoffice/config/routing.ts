import { ModuleWithProviders, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// guards
import { HasAccessGuard } from '../../shared/guards/has-access.guard';

// componentes
import { MainComponent } from '../main.component';
import { CauMenusUrlComponent } from '../../shared/components/menus.url.component';

// apoderapp
import { ApoderappAccessControlComponent } from '../apoderapp/apoderapp-access-control.component';
import { ApoderappStudentDetailComponent } from '../apoderapp/apoderapp-student-detail.component';
import { ApoderappComponent } from '../apoderapp/apoderapp.component';

// cau-personal
import { CAUBlockComponent } from '../caupersonal/block.component';
// tslint:disable-next-line:max-line-length
import { CAUProofOfEnrollmentPromotionalBatchComponent } from '../caupersonal/proof-of-enrollment/proof-of-enrollment-promotional-batch.component';
// tslint:disable-next-line:max-line-length
import { CAUProofOfEnrollmentPronabecBatchComponent } from '../caupersonal/proof-of-enrollment/proof-of-enrollment-pronabec-batch.component';
import { CAUProofOfEnrollmentPronabecComponent } from '../caupersonal/proof-of-enrollment/proof-of-enrollment-pronabec.component';
import { CAUProofOfEntryPromotionalBatchComponent } from '../caupersonal/proof-of-entry/proof-of-entry-promotional-batch.component';
import { CAUProofOfEntryPromotionalComponent } from '../caupersonal/proof-of-entry/proof-of-entry-promotional.component';
// tslint:disable-next-line:max-line-length
import { CAUProofOfEnrollmentPromotional1stEnrollmentComponent } from '../caupersonal/proof-of-enrollment/proof-of-enrollment-promotional-1st-enrollment.component';
import { CAUProofOfGraduationComponent } from '../caupersonal/proof-of-graduation.component';
import { CAUReportCardPronabecBatchComponent } from '../caupersonal/report-card/report-card-pronabec-batch.component';
import { CAURequestComponent } from '../caupersonal/request.component';
import { CAURequestListComponent } from '../caupersonal/requestlist.component';
import { CAURequestReportComponent } from '../caupersonal/requestReport.component';
import { CAUSignerDocumentComponent } from '../caupersonal/signers.component';
// tslint:disable-next-line:max-line-length
import { CAUStudiesCertificatePromotionalBatchComponent } from '../caupersonal/studies-certificate/studies-certificate-promotional-batch.component';
import { CAUStudiesCertificateComponent } from '../caupersonal/studies-certificate/studies-certificate.component';

import { BUWCategorizationComponent } from '../buw/categorization.component';
import { CEUReportGraduateComponent } from '../ceupersonal/ceu.reportgraduate.component';
import { COURequestComponent } from '../cou/cou.request.component';
import { COURequestListComponent } from '../cou/cou.requestlist.component';
import { DEURequestComponent } from '../deu/deu.request.component';
import { DEURequestListComponent } from '../deu/deu.requestlist.component';
import { OPPManagemenrComponent } from '../opp/opp.management.component';

// Rutas para RAU - Registros Academicos
import { RAURequestComponent } from '../raupersonal/rau.request.component';
import { RAURequestListComponent } from '../raupersonal/rau.requestlist.component';

// Rutas documentos RAU
import { RAUProof3510PeriodComponent } from '../raupersonal/proof-3510/proof.3510.period.component';
import { RAUProof3510PromotionalComponent } from '../raupersonal/proof-3510/proof.3510.promotional.component';
import { RAUOfficialTranscriptComponent } from '../raupersonal/official.transcript.component';


// adm
import { ADMMenuComponent } from '../adm/menu.component';
import { ADMPermissionComponent } from '../adm/permission.component';
import { ADMSettingsComponent } from '../adm/settings/settings.component';
import { ADMSurveysComponent } from '../adm/surveys/surveys.component';
import { ADMTermComponent } from '../adm/terms/term.component';
import { PeriodDateComponent } from '../adm/period-date/period-date.component';
import { ADMTermsAndConditionsComponent } from '../adm/terms-and-conditions/terms-and-conditions.component';
// tslint:disable-next-line:max-line-length
import { ADMTermsAndConditionsForEnrollmentComponent } from '../adm/terms-and-conditions-for-enrollment/terms-and-conditions-for-enrollment.component';

// rutas para CIC
import { CICCaieBatchComponent } from '../languages-center/cic-records-administration/cic-caie-batch.component';
import { CICCaieInstituteComponent } from '../languages-center/cic-records-administration/cic-caie-institute.component';
import { CICCaiePromotionalComponent } from '../languages-center/cic-records-administration/cic-caie-promotional.component';
import { CICDiplomasComponent } from '../languages-center/cic-records-administration/cic-diplomas.component';
import { CICExtraLanguageRecordComponent } from '../languages-center/cic-extra/cic-extra-language-record.component';
import { CICExtraStudentLanguageRecordComponent } from '../languages-center/cic-extra/cic-extra-student-language-record.component';
import { CICProofOfEnrollmentComponent } from '../languages-center/cic-records-administration/cic-proof-of-enrollment.component';
import { CICStudiesCertificateComponent } from '../languages-center/cic-records-administration/cic-studies-certificate.component';
import { CICReportOfDocumentsIssuedComponent } from '../languages-center/cic-reports/cic-report-of-documents-issued.component';

// rutas para VUC - Vida Universitaria Continental
import { VUCManagementComponent } from '../extracurricular-activities/management.component';
import { VUCRecognitionComponent } from '../extracurricular-activities/recognition.component';
import { VUCLayoutReportComponent } from '../extracurricular-activities/reports/vuc-layout-report.component';
import { VUCActivityProgrammingComponent } from '../extracurricular-activities/activity-programming.component';
import { VUCRegisterSelectedStudentComponent } from '../extracurricular-activities/register-selected-student.component';
import { VUCRegisterTracingComponent } from '../extracurricular-activities/register-tracing.component';


// tslint:disable-next-line:max-line-length
import { CICReportOfStudentsEnrolledBySchoolComponent } from '../languages-center/cic-reports/cic-report-of-students-enrolled-by-school.component';
import { CICReportOfStudentsEnrolledComponent } from '../languages-center/cic-reports/cic-report-of-students-enrolled.component';
import { CICRequestListComponent } from '../languages-center/cic-request/cic-request-list.component';
import { CICSettingsCaieLanguageLevelComponent } from '../languages-center/cic-settings/cic-settings-caie-language-level.component';

// Libro de reclamaciones (LDR)
import { LDRAnswerPageClaim } from '../claimsbook/cau/answer-page-claim.component';
import { LDRDerivatePageClaims } from '../claimsbook/cau/derive-page.component';
import { LDRDisableDerivation } from '../claimsbook/cau/disable-derivation.component';
import { LDRDismissedPage } from '../claimsbook/cau/dismissed-page.component';
import { LDRBookActionsComponent } from '../claimsbook/cau/page-book-list.component';
import { LDRPageDetailsComponent } from '../claimsbook/cau/page-details.component';
import { LDRBookRegisterComponent } from '../claimsbook/cau/register.component';

// LDR (Area Involucrada)
import { LDRAnswerPageClaimIA } from '../claimsbook/involvedArea/answerPageClaim.component';
import { LDRDerivatePageClaimIA } from '../claimsbook/involvedArea/derivatePageClaim.component';
import { LDRDetailsPageClaimAI } from '../claimsbook/involvedArea/detailsPageClaim.component';
import { LDRListPagesClaimsIAComponent } from '../claimsbook/involvedArea/listPageClaims.component';

// LDR (Area Legal)
import { LDRDerivateIA } from '../claimsbook/legal-area/derivate.component';
import { LDRDetailsPageClaimLA } from '../claimsbook/legal-area/details-page-claim.component';
import { LDRListPagesClaimsLAComponent } from '../claimsbook/legal-area/list-page-claims.component';
import { LDRResolvedPageClaimLA } from '../claimsbook/legal-area/resolved-page-claim.component';

// LDR (Defensor Universitario)
import { LDRDetailsPageClaimUD } from '../claimsbook/university-defender/details-page-claim.component';
import { LDRListPagesClaimsUD } from '../claimsbook/university-defender/list-page-claims.component';

// LDR reportes
import { LDRReport1 } from '../claimsbook/reports/report1.component';
import { LDRReport2 } from '../claimsbook/reports/report2.component';
import { LDRReport3 } from '../claimsbook/reports/report3.component';
import { LDRReport4 } from '../claimsbook/reports/report4.component';
import { LDRReport5 } from '../claimsbook/reports/report5.component';

// tools
import { ToolsChangeStudentComponent } from '../tools/change.student.component';

// extra components
import { Page401Component } from '../../shared/components/page401.component';
import { Page404Component } from '../../shared/components/page404.component';
import { RedirectComponent } from '../../shared/components/redirect.component';

// Convenios
import { BenefitAgreementDetailComponent } from '../benefit/agreement-detail.component';
import { BenefitAgreementComponent } from '../benefit/agreement.component';
import { BenefitAsignBenefitComponent } from '../benefit/asign-benefit.component';
import { BenefitNewAgreementComponent } from '../benefit/new-agreement.component';

// Learning Assessment
import { OEALearningAssessmentDatesComponent } from '../learning-assessment/learning.assessment.dates.component';
import { OEAProgramNrcComponent } from '../learning-assessment/program.nrc.component';
import { OEAProgramNrcByDatesComponent } from '../learning-assessment/program.nrc.by.dates.component';
import { OEAReportSubstituteProgrammedComponent } from '../learning-assessment/reports/report.substitute.programmed.component';
import { OEAReportNrcProgrammedComponent } from '../learning-assessment/reports/report.nrc.programmed.component';
import { OEAsubstituteExamStudentComponent } from '../learning-assessment/substitute.exam.student.component';


// module academic life
import { SearchStudentComponent } from '../../shared/components/search/search-student.component';
import { DashboardAcademicLifeComponent } from '../academic-life/dashboard-academic-life.component';

import { DisplayBlockComponent } from '../display-block/display-block.component';

import { CICCaieExternalComponent } from '../languages-center/cic-records-administration/cic-caie-external.component';
import { CICCaieInternalComponent } from '../languages-center/cic-records-administration/cic-caie-internal.component';

import { SurveysComponent } from '../../shared/components/surveys/surveys.component';
import { TermsAndConditionsComponent } from '../../shared/components/terms-and-conditions/terms-and-conditions.component';
import { ADMSettingsTypeComponent } from '../adm/settings-type/settings-type.component';

// graduates
import { GraduatesReportComponent } from '../graduates/graduates-report.component';
import { SearchGraduatesComponent } from '../graduates/search-graduates.component';
import { GraduatesComplianceComponent } from '../graduates/graduates-compliance.component';

// Reclamo de notas
// CAU
import { OEACauNoteClaimComponent } from '../caupersonal/note-claim/note.claim.component';
// Docente
import { OEANoteClaimTeacherComponent } from '../note-claim/teacher.component';
// RAU
import { OEARauNoteClaimComponent } from '../raupersonal/note-claim/note.claim.component';
// OEA
import { OEAOeaNoteClaimComponent } from '../learning-assessment/note-claim/note.claim.component';
// OGD
import { OEAOgdNoteClaimComponent } from '../teacher-management/note-claim/note.claim.component';
// Reports
import { OEANoteClaimReportComponent } from '../note-claim/reports/general.component';
// Proyectos UC
import { PRYListProjectComponent } from '../continental-project/adm/list-project.component';
import { PRYDetailProjectComponent } from '../continental-project/adm/detail-project.component';
import { PRYMyProjectsComponent } from '../continental-project/teacher/my-projects.component';
// incidents
import { IncidentDetailComponent } from '../incidents/incident-detail/incident-detail.component';
import { IncidentCategoryComponent } from '../incidents/incident-category/incident-category.component';
import { IncidentLogTypeComponent } from '../incidents/incident-log-type/incident-log-type.component';
import { IncidentStatusComponent } from '../incidents/incident-status/incident-status.component';
import { IncidentSubcategoryComponent } from '../incidents/incident-subcategory/incident-subcategory.component';
// tslint:disable-next-line:max-line-length
import { IncidentSubcategoryFunctionalUnitComponent } from '../incidents/incident-subcategory-functional-unit/incident-subcategory-functional-unit.component';
import { IncidentTypeComponent } from '../incidents/incident-type/incident-type.component';
import { IncidentsComponent } from '../incidents/incidents.component';
import { PRYBackCategoriesComponent } from '../continental-project/teacher/categories.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'backoffice', pathMatch: 'full' },
  { path: 'backoffice', component: MainComponent },

  /****************************
   *  rutas para ADM - Administrator
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/administracion',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'ajustes', component: ADMSettingsComponent },
      { path: 'encuestas', component: ADMSurveysComponent },
      { path: 'fechas-periodo', component: PeriodDateComponent },
      { path: 'menus', component: ADMMenuComponent },
      { path: 'periodo-academico', component: ADMTermComponent },
      { path: 'permisos', component: ADMPermissionComponent },
      { path: 'servicios', component: DisplayBlockComponent },
      { path: 'terminos-y-condiciones', component: ADMTermsAndConditionsComponent },
      { path: 'terminos-inscripcion', component: ADMTermsAndConditionsForEnrollmentComponent },
      { path: 'tipo-ajustes', component: ADMSettingsTypeComponent },
      { path: 'incidentes',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'tipos-incidente', component: IncidentTypeComponent },
          { path: 'estados-incidente', component: IncidentStatusComponent },
          { path: 'categorias-incidente', component: IncidentCategoryComponent },
          { path: 'subcategorias-incidente', component: IncidentSubcategoryComponent },
          { path: 'areas-subcategorias', component: IncidentSubcategoryFunctionalUnitComponent },
          { path: 'tipo-logs-incidente', component: IncidentLogTypeComponent },
        ]
      },
    ],
  },

  /****************************
   *  rutas para convenios
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/convenios',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'lista-convenios', component: BenefitAgreementComponent },
      { path: 'lista-convenios/:agreementId', component: BenefitAgreementDetailComponent },
      { path: 'nuevo-convenio', component: BenefitNewAgreementComponent },
    ]
  },
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/descuentos',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'asignar-descuento/estudiante/:dni', component: BenefitAsignBenefitComponent },
      { path: 'asignar-descuento', component: SearchStudentComponent },
    ]
  },

  /****************************
   *  rutas para Evaluacion del aprendizaje
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/evaluacion-aprendizaje',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'fechas', component: OEALearningAssessmentDatesComponent },
      { path: 'programar-nrc', component: OEAProgramNrcComponent },
      { path: 'programar-nrc-fechas', component: OEAProgramNrcByDatesComponent },
      { path: 'programar-examen-estudiante' , component: SearchStudentComponent },
      { path: 'programar-examen-estudiante/estudiante/:studentId', component: OEAsubstituteExamStudentComponent },
      { path: 'reportes',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'sustitutorios-programados', component: OEAReportSubstituteProgrammedComponent },
          { path: 'nrcs-programados', component: OEAReportNrcProgrammedComponent },
        ]
      },
      {
        path: 'reclamo-notas',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'reclamos', component: OEAOeaNoteClaimComponent },
          { path: 'reportes', component: OEANoteClaimReportComponent },
        ]
      }
    ]
  },

  // rutas para Apoderapp
  { canActivate: [HasAccessGuard], path: 'backoffice/apoderados', component: ApoderappComponent },
  { canActivate: [HasAccessGuard], path: 'backoffice/apoderados/control-acceso/:request', component: ApoderappStudentDetailComponent },
  { canActivate: [HasAccessGuard], path: 'backoffice/apoderados/control-acceso', component: ApoderappAccessControlComponent },

  /****************************
   *  rutas para CAU - Centro de Atención y Soluciones
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/cau-personal',
    children: [
      { path: '', component: CauMenusUrlComponent },
      {
        path: 'boleta-notas',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'pronabec-batch', component: CAUReportCardPronabecBatchComponent },
        ]
      },
      {
        path: 'certificado-estudios',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'promocional-batch', component: CAUStudiesCertificatePromotionalBatchComponent },
        ]
      },
      {
        path: 'constancia-ingreso',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'promocional', component: CAUProofOfEntryPromotionalComponent },
          { path: 'promocional-batch', component: CAUProofOfEntryPromotionalBatchComponent },
        ]
      },
      { path: 'constancia-egresado', component: CAUProofOfGraduationComponent },

      {
        path: 'constancia-matricula',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'pronabec', component: CAUProofOfEnrollmentPronabecComponent },
          { path: 'pronabec-batch', component: CAUProofOfEnrollmentPronabecBatchComponent },
          { path: 'promocional-batch', component: CAUProofOfEnrollmentPromotionalBatchComponent },
          { path: 'promocional-primera-matricula', component: CAUProofOfEnrollmentPromotional1stEnrollmentComponent },
        ]
      },
      { path: 'reporte-firmas', component: CAUSignerDocumentComponent },
      { path: 'reporte-solicitudes', component: CAURequestReportComponent },
      { path: 'solicitudes', component: CAURequestListComponent },
      { path: 'solicitudes/:request', component: CAURequestComponent },
      { path: 'solicitudes-bloque', component: CAUBlockComponent },
      {
        path: 'reclamo-notas',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'reclamos', component: OEACauNoteClaimComponent },
          { path: 'reportes', component: OEANoteClaimReportComponent },
        ]
      }
    ]
  },

  /****************************
   *  rutas para Registros Academicos *
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/rau-personal',
    children: [
      { path: '', component: CauMenusUrlComponent },
      {
        path: 'boleta-notas',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'pronabec-batch', component: CAUReportCardPronabecBatchComponent },
        ]
      },
      { canActivate: [HasAccessGuard], path: 'solicitudes', component: RAURequestListComponent },
      { canActivate: [HasAccessGuard], path: 'solicitudes/:request', component: RAURequestComponent },
      {
        path: 'constancia-matricula',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'pronabec', component: CAUProofOfEnrollmentPronabecComponent },
          { path: 'pronabec-batch', component: CAUProofOfEnrollmentPronabecBatchComponent },
          { path: 'promocional-batch', component: CAUProofOfEnrollmentPromotionalBatchComponent },
        ]
      },
      {
        path: 'constancia-3510',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'periodo', component: RAUProof3510PeriodComponent },
          { path: 'promocional', component: RAUProof3510PromotionalComponent },
        ]
      },
      { path: 'certificado-estudios-promocional-batch', component: CAUStudiesCertificatePromotionalBatchComponent },
      { path: 'historial-academico', component: RAUOfficialTranscriptComponent },
      {
        path: 'reclamo-notas',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'reclamos', component: OEARauNoteClaimComponent },
          { path: 'reportes', component: OEANoteClaimReportComponent },
        ]
      }
    ]
  },
  // rutas para CEU - Centro de Estadistica
  { canActivate: [HasAccessGuard], path: 'backoffice/ceu-personal', component: CEUReportGraduateComponent },
  { canActivate: [HasAccessGuard], path: 'backoffice/ceu-personal/egresados', component: CEUReportGraduateComponent },

  // rutas para OPP - Practicas pre profecionales
  { canActivate: [HasAccessGuard], path: 'backoffice/opp-personal', component: OPPManagemenrComponent },
  { canActivate: [HasAccessGuard], path: 'backoffice/opp-personal/constancias', component: OPPManagemenrComponent },

  // rutas para COU - Convalidaciones
  { canActivate: [HasAccessGuard], path: 'backoffice/cou-personal', component: COURequestListComponent },
  { canActivate: [HasAccessGuard], path: 'backoffice/cou-personal/solicitudes', component: COURequestListComponent },
  { canActivate: [HasAccessGuard], path: 'backoffice/cou-personal/solicitudes/:request', component: COURequestComponent },

  // rutas para DEU - Directores de Programa
  { canActivate: [HasAccessGuard], path: 'backoffice/deu-personal', component: DEURequestListComponent },
  { canActivate: [HasAccessGuard], path: 'backoffice/deu-personal/convalidaciones', component: DEURequestListComponent },
  { canActivate: [HasAccessGuard], path: 'backoffice/deu-personal/convalidaciones/:request', component: DEURequestComponent },

  // rutas para BUW - Bienestar Universitario
  { canActivate: [HasAccessGuard], path: 'backoffice/buw-personal', component: CauMenusUrlComponent },
  { canActivate: [HasAccessGuard], path: 'backoffice/buw-personal/verificacion', component: RedirectComponent },
  { canActivate: [HasAccessGuard], path: 'backoffice/buw-personal/categorizacion', component: BUWCategorizationComponent },

  /****************************
   *  CIC - Centro de Idiomas *
   ****************************/

  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/centro-idiomas',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'ajustes', component: CauMenusUrlComponent },
      { path: 'ajustes/nivel-idioma-caie', component: CICSettingsCaieLanguageLevelComponent },
      {
        path: 'constancias',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'constancia-matricula', component: CICProofOfEnrollmentComponent },
          { path: 'certificado-estudios', component: CICStudiesCertificateComponent },
          { path: 'diplomas', component: CICDiplomasComponent },
          { path: 'caie-promocional', component: CICCaiePromotionalComponent },
          { path: 'caie-batch', component: CICCaieBatchComponent },
          { path: 'caie-instituto', component: CICCaieInstituteComponent },
          { path: 'caie-externo', component: CICCaieExternalComponent },
          { path: 'caie-interno', component: CICCaieInternalComponent },
        ]
      },
      { path: 'historial-estudiante', component: CICExtraLanguageRecordComponent },
      { path: 'historial-estudiante/:student', component: CICExtraStudentLanguageRecordComponent },
      {
        path: 'reportes',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'reporte-estudiantes-matriculados', component: CICReportOfStudentsEnrolledComponent },
          { path: 'reporte-matriculados-facultad-escuela', component: CICReportOfStudentsEnrolledBySchoolComponent },
          { path: 'reporte-documentos-emitidos', component: CICReportOfDocumentsIssuedComponent },
        ]
      },
      { path: 'solicitudes', component: CICRequestListComponent },
      { path: 'solicitudes/:request', component: CAURequestComponent },
    ]
  },
  /****************************
   *  rutas para VUC - Vida Universitaria Continental *
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/vida-universitaria-continental',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'administracion-ejes-actividades', component: VUCManagementComponent },
      { path: 'programacion-actividades', component: VUCActivityProgrammingComponent },
      { path: 'reconocimientos', component: VUCRecognitionComponent},
      { path: 'reporte-actividades', component: VUCLayoutReportComponent},
      { path: 'registro-seleccionados', component: VUCRegisterSelectedStudentComponent},
      { path: 'registro-cumplimiento', component: VUCRegisterTracingComponent},
    ]
  },

  /****************************
   *  Libro de reclamaciones (LDR) - CAU
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/ldr-administracion',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'registro', component: LDRBookRegisterComponent },
      { path: 'libro', component: LDRBookActionsComponent },
      { path: 'detalle/:state/:page_id', component: LDRPageDetailsComponent },
      { path: 'derivacion/:state/:page_id', component: LDRDerivatePageClaims },
      { path: 'desestimar/:state/:page_id', component: LDRDismissedPage },
      { path: 'responder/:state/:page_id', component: LDRAnswerPageClaim },
      { path: 'deshabilitar-derivacion/:state/:page_id/:derivation_id', component: LDRDisableDerivation },
    ]
  },

  /****************************
   *  LDR Area Involucrada
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/ldr-area-involucrada',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'libro', component: LDRListPagesClaimsIAComponent },
      { path: 'detalle/:state/:page_id/:derivate_id', component: LDRDetailsPageClaimAI },
      { path: 'derivacion/:state/:page_id/:derivate_id', component: LDRDerivatePageClaimIA },
      { path: 'responder/:state/:page_id/:derivate_id', component: LDRAnswerPageClaimIA },
    ]
  },

  /****************************
   *  LDR Area legal-area
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/ldr-area-legal',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'libro', component: LDRListPagesClaimsLAComponent },
      { path: 'detalle/:state/:page_id/:derivate_id', component: LDRDetailsPageClaimLA },
      { path: 'derivacion/:state/:page_id/:derivate_id', component: LDRDerivateIA },
      { path: 'resolver/:state/:page_id/:derivate_id', component: LDRResolvedPageClaimLA  },
    ]
  },

  /****************************
   *  LDR Defensor Universitario
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/ldr-defensor-universitario',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'libro', component: LDRListPagesClaimsUD },
      { path: 'detalle/:state/:page_id', component: LDRDetailsPageClaimUD },
    ]
  },

  /****************************
   *  LDR Reportes
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/ldr-reportes',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'reporte1', component: LDRReport1 },
      { path: 'reporte2', component: LDRReport2 },
      { path: 'reporte3', component: LDRReport3 },
      { path: 'reporte4', component: LDRReport4 },
      { path: 'reporte5', component: LDRReport5 },
    ]
  },

  /****************************
   *  Módulo de Vida académica
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/vida-academica',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'buscar', component: SearchStudentComponent },
      { path: 'buscar/estudiante/:dni', component: DashboardAcademicLifeComponent },
    ]
  },

  /****************************
   *  Rutas de Herramientas
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/herramientas',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'cambiar-estudiante', component: ToolsChangeStudentComponent },
    ]
  },

  /****************************
   *  Rutas de Secretario de facultad
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/secretario-facultad',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'constancia-egresado', component: CAUProofOfGraduationComponent },
    ]
  },


  /*************
   * EGRESADOS *
   *************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/egresados',
    children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'buscar', component: SearchGraduatesComponent },
      { path: 'reporte', component: GraduatesReportComponent },
    ]
  },

  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/cumplimiento-egresados',
    children: [
      { path: '', component: GraduatesComplianceComponent },
      { path: 'actualizar', component: GraduatesComplianceComponent },
    ]
  },

  /****************************
   *  Reclamo de notas - Docente
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/reclamo-notas-docente',
    component: OEANoteClaimTeacherComponent
  },

  /****************************
   *  Reclamo de notas - Gestion Docente
   ****************************/
  {
    canActivate: [HasAccessGuard],
    path: 'backoffice/gestion-docente',
    children: [
      { path: '', component: CauMenusUrlComponent },
      {
        path: 'reclamo-notas',
        children: [
          { path: '', component: CauMenusUrlComponent },
          { path: 'reclamos', component: OEAOgdNoteClaimComponent },
          { path: 'reportes', component: OEANoteClaimReportComponent },
        ]
      }
    ]
  },

  /****************************
   *  Proyectos UC
   ****************************/
  {
    canActivate: [HasAccessGuard], path: 'backoffice/proyectos-continental-adm', children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'proyectos', component: PRYListProjectComponent }
    ]
  },
  {
    canActivate: [HasAccessGuard], path: 'backoffice/proyectos-continental-doc', children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'proyectos', component: PRYMyProjectsComponent }
    ]
  },
  {
    canActivate: [HasAccessGuard], path: 'backoffice/inscripcion', children: [
      { path: '', component: CauMenusUrlComponent },
      { path: 'proyectos', component: PRYBackCategoriesComponent }
    ]
  },

  /*************
   * INCIDENTS *
   *************/
  { canActivate: [HasAccessGuard], path: 'backoffice/incidentes', component: IncidentsComponent },
  { canActivate: [HasAccessGuard], path: 'backoffice/incidentes/:incident', component: IncidentDetailComponent },

  // page 40X
  { path: '404', component: Page404Component },
  { path: 'backoffice/404', component: Page404Component },

  { path: '401', component: Page401Component },
  { path: 'backoffice/401', component: Page401Component },

  { path: 'terms', redirectTo: 'backoffice/terms', pathMatch: 'full' },
  { path: 'surveys', redirectTo: 'backoffice/surveys', pathMatch: 'full' },
  { path: 'backoffice/terms', component: TermsAndConditionsComponent },
  { path: 'backoffice/surveys', component: SurveysComponent },

  // default page
  { path: '**', redirectTo: '/404' },

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
