import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { MomentService } from '../../../shared/moment.service';
import { CICRequestService } from '../../../shared/services/languages-center/cic-request.service';
import { BackofficeConfigService } from '../../config/config.service';

import { Graduated } from '../../../shared/services/academic/graduated';
import { Request } from '../../../shared/services/atentioncenter/request';

@Component({
  selector: 'cic-request-list',
  templateUrl: './cic-request-list.component.html',
  providers: [
    BackofficeConfigService,
    {provide: LOCALE_ID, useValue: 'es'},
    MomentService,
    CICRequestService,
  ],
})
export class CICRequestListComponent implements OnInit {

  es: object; // objeto para uso del datepicker
  wata: number; // año actual
  errorMessage: string;
  searching: boolean;

  estudiantes: any;  // modelo para el input de Estudiantes
  students: any[];  // array de 'estudiantes' a procesar
  graduates: Graduated;  // graduados UC

  tuqpa: any[]; // códigos de estudiantes a procesar no repetidos

  isShowStudentsList: boolean; // muestra los códigos que se van a procesar
  isAnyStudent: boolean; // para activar/desactivar el boton generar solicitud
  selected = false; // para seleccionar/deseleccionar todos los estudiantes
  gettingRequest: boolean; // procesando registro de solicitud

  showRequestList: boolean; // Mostrar lista se solicitudes
  loadingRequestList: boolean; // cargando lista de solicitudes

  requestList: Request[];
  pendingApproval: Request[];
  pendingProcessing: Request[];
  pendingDelivery: Request[];
  requestCancelled: Request[];
  requestViewActive: Request[];
  requestInbox: Request[];
  requestArchive: Request[];

  inbox: string;

  constructor(private config: BackofficeConfigService,
              private moment: MomentService,
              private requestService: CICRequestService,
              private router: Router, private location: Location) {
    this.errorMessage = '';
  }

  ngOnInit() {
    this.es = {
      clear: 'Limpiar',
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
      dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
      firstDayOfWeek: 0,
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre',
        'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
      today: 'Hoy',
    };
    this.wata = (new Date()).getFullYear();
    this.clean();
    this.getRequestList();
    this.showRequestList = true;
    this.inbox = 'inbox';
  }

  /**
   * Tiempo relativo a partir de una fecha tipo YYYYMMDD
   * p.e.: momento("20101225") retorna un string: "hace X años"
   * @param {string} date
   * @returns {string}
   */
  momento(date: string): string {
    return this.moment.fromNow(date);
  }

  /**
   * Limpia modelos
   */
  clean(): void {
    this.cleanCreateRequest();
    this.cleanRequestList();
  }

  /**
   * Limpia modelos del bloque "Solicitud
   */
  cleanCreateRequest(): void {
    this.errorMessage = '';
    this.searching = false;
    this.estudiantes = undefined;
    this.students = [];
    this.graduates = undefined;
    this.tuqpa = [];
    this.isShowStudentsList = false;
    this.isAnyStudent = false;
    this.selected = false;
    this.gettingRequest = false;
    this.requestList = [];
  }

  /**
   * Limpia lista de solicitudes
   */
  cleanRequestList(): void {
    this.requestList = [];
    this.requestArchive = [];
    this.requestInbox = [];
    this.pendingApproval = [];
    this.pendingProcessing = [];
    this.pendingDelivery = [];
    this.requestCancelled = [];
    this.requestViewActive = [];
  }

  getRequestList(): void {
    console.log('Loading request list...');
    this.loadingRequestList = true;
    this.requestService.get()
      .subscribe(
        (data) => {
          this.getRequestListByStatus(data);
          console.log('lista solicitudes => ', data);
        },
        (error) => {
          this.errorMessage = error as any;
        },
        () => {
          this.loadingRequestList = false;
        },
      );
  }

  getRequestListByStatus(requestList: Request[]): void {
    this.requestList = requestList;
    this.requestArchive = requestList;
    this.requestInbox = this.requestList.filter(
      (request) => request.state !== this.config.STATE_CANCEL && request.state !== this.config.STATE_FINAL);
    this.pendingApproval = this.requestList.filter((request) => request.state === this.config.STATE_APROVAL_PENDING);
    this.pendingProcessing = this.requestList.filter((request) => request.state === this.config.STATE_PROCESS_PENDING);
    this.pendingDelivery = this.requestList.filter((request) => request.state === this.config.STATE_DELIVERY_PENDING);
    this.requestCancelled = this.requestList.filter((request) => request.state === this.config.STATE_CANCEL);
    this.requestViewActive = this.requestInbox;
  }

  /**
   * Selección de vistas para mostrar solicitudes de un determinado estado
   * @param {Event} event
   * @param {string} type
   */
  setInbox(event: Event, type: string): void {
    this.inbox = type;
    switch (type) {
      case ('inbox'):
        this.showRequestList = true;
        this.requestViewActive = this.requestInbox;
        break;
      case ('pendienteAprobacion'):
        this.showRequestList = true;
        this.requestViewActive = this.pendingApproval;
        break;
      case ('pendienteProceso'):
        this.showRequestList = true;
        this.requestViewActive = this.pendingProcessing;
        break;
      case ('pendienteEntrega'):
        this.showRequestList = true;
        this.requestViewActive = this.pendingDelivery;
        break;
      case ('archivo'):
        this.showRequestList = true;
        this.requestViewActive = this.requestArchive;
        break;
      case ('cancelado'):
        this.showRequestList = true;
        this.requestViewActive = this.requestCancelled;
        break;
      case ('search'):
        this.showRequestList = true;
        break;
    }
  }

  /**
   * Ir a detalle de solicitud
   * @param {string} requestId
   */
  gotoRequestDetail(requestId: string): void {
    const pathName: string[] = this.location.path().split('/');
    if (pathName.length > 3) {
      const link: string[] = [`${pathName[1]}/${pathName[2]}/solicitudes/${requestId}`];
      this.router.navigate(link);
    } else {
      console.log('pathName', pathName);
    }
  }
}
