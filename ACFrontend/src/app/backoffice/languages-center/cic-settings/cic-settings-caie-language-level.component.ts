import { Component } from '@angular/core';

import { CICSettingsService } from '../../../shared/services/languages-center/cic-settings.service';
import { BackofficeConfigService } from '../../config/config.service';

import { CICSettings } from '../../../shared/services/languages-center/cic-settings';

@Component({
  selector: 'cic-settings-caie-language-level',
  templateUrl: './cic-settings-caie-language-level.component.html',
  providers: [
    BackofficeConfigService,
    CICSettingsService,
  ],
})
export class CICSettingsCaieLanguageLevelComponent {

  errorMessage: string;
  edit: boolean;
  loading: boolean;
  processing: boolean;
  languageLevelName: string;
  successMsg: boolean;
  failMsg: boolean;

  setting: CICSettings;

  constructor(private config: BackofficeConfigService, private settingsService: CICSettingsService) {
  }

  ngOnInit() {
    this.edit = false;
    this.loading = false;
    this.processing = false;
    this.successMsg = false;
    this.failMsg = false;
    this.getInfo();
  }

  mod(): void {
    this.edit = !this.edit;
    this.successMsg = false;
    this.failMsg = false;
  }

  /**
   * Get info
   */
  getInfo(): void {
    this.loading = true;
    this.settingsService.getCaieLanguageLevelInfo()
      .subscribe(
        (document) => this.setting = document,
        (error) => {
          this.errorMessage = error as any;
          this.loading = false;
          this.edit = false;
        },
        () => {
          this.loading = false;
          this.edit = false;
        },
      );
  }

  /**
   * Save CAIE language level info
   */
  save(): void {
    this.processing = true;
    console.log('setting => ', this.setting);
    this.settingsService.saveCaieLanguageLevelInfo(this.setting)
      .subscribe(
        (saved) => this.setting = saved,
        (error) => {
          this.errorMessage = error as any;
          this.failMsg = true;
          this.processing = false;
          this.edit = false;
        },
        () => {
          this.processing = false;
          this.edit = false;
          this.successMsg = true;
        },
      );
  }
}
