import { Component, OnInit } from '@angular/core';

import { CICSearchService } from '../../../shared/services/languages-center/cic-search.service';
import { ConfigService } from '../../../config/config.service';
import { BackofficeConfigService } from '../../config/config.service';
import { UtilService } from '../../../shared/util.service';

import { AcademicProfile } from '../../../shared/services/academic/academic.profile';
import { CICStudent } from '../../../shared/services/languages-center/cic-student';
import { CICCaie } from '../../../shared/services/languages-center/cic-caie';
import { CICCustomCaie } from '../../../shared/services/languages-center/cic-custom-caie';
import { CICAcademicProfile } from '../../../shared/services/languages-center/cic-academic-profile';
import { CICProofOfForeignLanguageService } from '../../../shared/services/languages-center/cic-proof-of-foreign-language.service';
import { Persona } from '../../../shared/services/persona/persona';

@Component({
  selector: 'cic-caie-external',
  templateUrl: './cic-caie-external.component.html',
  providers: [
    CICSearchService, ConfigService, BackofficeConfigService, UtilService, CICProofOfForeignLanguageService
  ]
})
export class CICCaieExternalComponent implements OnInit {
  es: any;
  wata: number;
  searching: boolean;
  processing: boolean;
  showStudentsList: boolean;
  anyStudent: boolean;
  estudiantes: any; // Modelo para el input de students
  students: any; // Array de 'estudiantes'
  tuqpa: any[];
  studentSearchResult: CICStudent[];
  caie: CICCaie;
  studentProfile: CICCustomCaie;
  config: any;
  util: any;
  escuela: string;
  graduateProfiles: AcademicProfile[];
  customMode: boolean;
  errorGeneratingTheFile: boolean;

  constructor(private cicSearchService: CICSearchService, private configService: ConfigService,
              private backConfigService: BackofficeConfigService, private utilService: UtilService,
              private cicProofOfForeignLanguageService: CICProofOfForeignLanguageService) {
    this.config = this.backConfigService;
    this.util = this.utilService;
  }

  ngOnInit(): void {
    this.clean();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.es = this.configService.getCalendarEs();
    this.wata = (new Date().getFullYear()) + 1;
    this.searching = false;
    this.estudiantes = '';
    this.students = undefined;
    this.showStudentsList = false;
    this.tuqpa = [];
    this.anyStudent = false;
    this.processing = false;
    this.studentSearchResult = undefined;
    this.studentProfile = undefined;
  }

  /**
   * Bandera para activar/desactivar el botón "Buscar Estudiantes"
   * @return {boolean} [description]
   */
  isAllSelected(): boolean {
    let isEstudiantesValid: boolean;

    isEstudiantesValid =
      this.estudiantes !== null && this.estudiantes !== undefined && this.estudiantes !== '' &&
      ((this.estudiantes).trim()).length !== 0;

    return isEstudiantesValid;
  }

  /**
   * Bandera para activar/desactivar el botón "Limpiar"
   * @return {boolean} [description]
   */
  isAnySelected(): boolean {
    let isEstudiantesValid: boolean;

    isEstudiantesValid =
      this.estudiantes !== null && this.estudiantes !== undefined && this.estudiantes !== '';

    return isEstudiantesValid;
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray(): void {
    this.studentSearchResult = undefined;
    this.studentProfile = undefined;
    this.searching = false;
    this.students = this.utilService.stringToArrayWithoutDuplicates(this.estudiantes);
    console.log('input students => ', this.students);
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsCodeList(): void {
    this.showStudentsList = !this.showStudentsList;
  }

  /**
   * Busca datos de estudiantes para procesar CAIE Batch
   * @param {any} students Código de estudiantes a buscar
   */
  search(): void {
    console.log('get students for CAIE Batch...');
    this.searching = true;
    this.tuqpa = [];
    this.studentSearchResult = undefined;
    this.studentProfile = undefined;
    this.anyStudent = false;
    this.processing = true;
    this.cicSearchService.searchStudent(this.students)
      .subscribe(
        (searchResult) => this.studentSearchResult = searchResult,
        (error) => {
          console.log('error => ', error);
          this.processing = false;
          this.searching = false;
        },
        () => {
          this.processing = false;
          this.searching = false;
          console.log('studentSearchResult => ', this.studentSearchResult);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Set student detail for generate document
   * @param persona Persona
   * @param profile CIC Profile
   * @param graduateProfiles UC graduate profiles info
   */
  studentDetail(persona: Persona, profile: CICAcademicProfile, graduateProfiles: AcademicProfile[]): void {
    this.caie = new CICCaie();
    this.customMode = true;

    this.graduateProfiles = graduateProfiles;
    this.caie.persona = persona;
    this.caie.profile = profile;

    this.studentProfile = new CICCustomCaie();
    this.studentProfile.student_names = persona.full_name;
    this.studentProfile.student_id = persona.document_number;
    this.studentProfile.languageName = profile.language.languageName;
    this.studentProfile.startDate = new Date();
    this.studentProfile.endDate = new Date();
  }

  /**
   * UC graduate profile for generate document
   */
  selectGraduationProfile(): void {
    this.caie.graduateProfile = this.graduateProfiles.filter(value => value.program.id === this.escuela)[0];
    this.studentProfile.college_name = this.caie.graduateProfile.college.name;
    this.studentProfile.program_name = this.caie.graduateProfile.program.name;
    this.studentProfile.graduationModality = this.caie.graduateProfile.graduateInfo.modalityName;
  }

  /**
   * Generate External CAIE on demand
   */
  generate(): void {
    let response: CICCaie;
    this.processing = true;
    this.caie.docUrl = undefined;
    this.errorGeneratingTheFile = false;
    this.cicProofOfForeignLanguageService.getCaieOnDemandExternal(this.studentProfile, [this.caie])
      .subscribe(
        (result) => response = result,
        (error) => {
          console.log(error);
          this.processing = false;
          this.errorGeneratingTheFile = true;
        },
        () => {
          this.processing = false;
          if (response.docUrl != null) {
            this.caie.docUrl = response.docUrl;
          }
        }
      );
  }

  /**
   * Close detail student
   */
  cancel(): void {
    this.escuela = null;
    this.studentProfile = undefined;
    this.graduateProfiles = [];
    this.customMode = false;
  }

}
