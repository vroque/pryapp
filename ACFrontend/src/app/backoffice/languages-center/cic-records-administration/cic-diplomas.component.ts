import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cic-diplomas',
  templateUrl: './cic-diplomas.component.html'
})
export class CICDiplomasComponent implements OnInit {

  tipoBusqueda: string;
  estudiante: any;

  constructor() { }

  ngOnInit() {
    this.tipoBusqueda = "nombre";
    this.estudiante = "";
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.estudiante = "";
  }

  /**
   * Bandera para activar/desactivar el botón "Buscar Estudiantes"
   * @return {boolean} [description]
   */
  isAllSelected(): boolean {
    let isEstudianteValid: boolean;

    if (this.estudiante == null || this.estudiante == undefined || this.estudiante == "" || ((this.estudiante).trim()).length == 0) {
      isEstudianteValid = false;
    }
    else {
      isEstudianteValid = true;
    }

    if (isEstudianteValid) {
      return true;
    }
    else {
      return false;
    }

  }

  /**
   * Bandera para activar/desactivar el botón "Limpiar"
   * @return {boolean} [description]
   */
  isAnySelected(): boolean {
    let isEstudianteValid: boolean;

    if (this.estudiante == null || this.estudiante == undefined || this.estudiante == "") {
      isEstudianteValid = false;
    }
    else {
      isEstudianteValid = true;
    }

    if (isEstudianteValid) {
      return true;
    }
    else {
      return false;
    }

  }

}
