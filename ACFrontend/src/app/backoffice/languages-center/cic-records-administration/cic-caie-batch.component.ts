import { Component, LOCALE_ID, OnInit } from '@angular/core';

import { CICSettingsService } from '../../../shared/services/languages-center/cic-settings.service';
import { CICProofOfForeignLanguageService } from '../../../shared/services/languages-center/cic-proof-of-foreign-language.service';
import { UtilService } from '../../../shared/util.service';

import { CICCaie } from '../../../shared/services/languages-center/cic-caie';
import { CICSettings } from '../../../shared/services/languages-center/cic-settings';

@Component({
  selector: 'cic-caie-batch',
  templateUrl: './cic-caie-batch.component.html',
  providers: [
    {provide: LOCALE_ID, useValue: 'es'},
    CICSettingsService, UtilService, CICProofOfForeignLanguageService
  ],
})
export class CICCaieBatchComponent implements OnInit {

  errorMsg: string;
  searching: boolean;
  estudiantes: any; // Modelo para el input de students
  students: any; // Array de 'estudiantes'
  isShowStudentsList: boolean;
  caie: CICCaie;
  tuqpa: any[];
  isAnyStudent: boolean;
  selected = false;
  processing = false;
  info: CICSettings;
  util: any;

  constructor(private cicProofOfForeignLanguageService: CICProofOfForeignLanguageService,
              private cicSettingsService: CICSettingsService, private utilService: UtilService) {
    this.errorMsg = '';
    this.util = this.utilService;
  }

  ngOnInit() {
    this.clean();
    this.getInfo();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.errorMsg = '';
    this.searching = false;
    this.estudiantes = '';
    this.students = undefined;
    this.isShowStudentsList = false;
    this.tuqpa = [];
    this.isAnyStudent = false;
    this.selected = false;
    this.processing = false;
    this.caie = undefined;
  }

  /**
   * Get CAIE language level info
   */
  getInfo(): void {
    this.cicSettingsService.getCaieLanguageLevelInfo()
      .subscribe(
        (info) => this.info = info,
        (error) => console.log('error => ', error),
        () => console.log('info => ', this.info)
      );
  }

  /**
   * Bandera para activar/desactivar el botón "Buscar Estudiantes"
   * @return {boolean} [description]
   */
  isAllSelected(): boolean {
    let isEstudiantesValid: boolean;

    isEstudiantesValid =
      this.estudiantes !== null && this.estudiantes !== undefined && this.estudiantes !== '' &&
      ((this.estudiantes).trim()).length !== 0;

    return isEstudiantesValid;
  }

  /**
   * Bandera para activar/desactivar el botón "Limpiar"
   * @return {boolean} [description]
   */
  isAnySelected(): boolean {
    let isEstudiantesValid: boolean;

    isEstudiantesValid =
      this.estudiantes !== null && this.estudiantes !== undefined && this.estudiantes !== '';

    return isEstudiantesValid;
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray(): void {
    this.caie = undefined;
    this.searching = false;
    this.students = this.utilService.stringToArrayWithoutDuplicates(this.estudiantes);
    console.log('input students => ', this.students);
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList(): void {
    this.isShowStudentsList = !this.isShowStudentsList;
  }

  /**
   * Busca datos de estudiantes para procesar CAIE Batch
   * @param {any} students Código de estudiantes a buscar
   */
  search(): void {
    console.log('get students for CAIE Batch...');
    this.searching = true;
    this.tuqpa = [];
    this.caie = undefined;
    this.isAnyStudent = false;
    this.selected = false;
    this.processing = true;
    const type = 'searchBatch';
    this.cicProofOfForeignLanguageService.getCaieBatch(type, this.students, [])
      .subscribe(
        (caie) => this.caie = caie,
        (error) => {
          console.log(error);
          this.processing = false;
          this.searching = false;
        },
        () => {
          this.processing = false;
          this.searching = false;
          console.log('caie => ', this.caie);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Selecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll(): void {
    for (const key of Object.keys(this.caie)) {
      this.caie[key].isSelected = !this.selected;
    }

    this.selected = !this.selected;
    this.isAnyStudent = this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.caie) {
      if (this.caie[key].isSelected) {
        return true;
      }
    }
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * Añade estilo a los estudiantes seleccionados
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any): void {
    if (studentObj.isSelected) {
      studentObj.isSelected = false;
      console.log('desactivado');
      // Quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
      // Ahora no todos están seleccionados
      // Pero comprobemos que a lo menos alguien esté seleccionado
      this.isAnyStudent = this.isAnyStudentSelected() ? this.isAnyStudentSelected() : false;
    } else {
      studentObj.isSelected = true;
      console.log('activado');
      // Agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);
      // console.log("this.tuqpa => ", this.tuqpa);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
    }
  }

  /**
   * Obtiene CAIE generado
   */
  getCAIE(): void {
    console.log('Get CAIE...');
    this.getSequentialCAIE(0);
    console.log('caie => ', this.caie);
  }

  /**
   * Petición secuencial para generar Constancia de Idioma Extranjero
   * NOTE: No es "LA" solución para este tipo de peticiones pero es lo que hay
   * @param {number} i
   */
  getSequentialCAIE(i: number): void {
    console.log('Sequential!');
    console.log('proceso => ', i);
    const type = 'caie';

    if (this.caie[i] !== undefined) {
      if (this.caie[i].isSelected && (this.caie[i].paymentStatus === 1 || this.caie[i].paymentStatus === 0)) {
        this.caie[i].docUrl = undefined;
        this.processing = true;
        this.errorMsg = undefined;
        this.caie[i].processing = true;
        this.cicProofOfForeignLanguageService.getCaieBatch(type, [], this.caie)
          .subscribe(
            (caie) => this.caie = caie,
            (error) => {
              console.log(error);
              this.processing = false;
              this.caie[i].processingError = true;
              this.caie[i].docUrl = undefined;
              this.getSequentialCAIE(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
            () => {
              this.processing = false;
              this.caie[i].processingError = false;
              this.getSequentialCAIE(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
          );
      } else {
        console.log('no seleccionado');
        this.getSequentialCAIE(i + 1);
      }
    } else {
      console.log('Nada que procesar => ', this.caie[i]);
    }
    console.log('+--------------------------+');
  }

}
