import { Component, OnInit } from '@angular/core';

// tslint:disable-next-line:max-line-length
import { CICProofOfEnrollmentService } from '../../../shared/services/languages-center/cic-records-administration/cic-proof-of-enrollment.service';

import { CICEnrolled } from '../../../shared/services/languages-center/cic-enrolled';
import { CICLanguageLevel } from '../../../shared/services/languages-center/cic-language-level';
import { CICLanguageProgram } from '../../../shared/services/languages-center/cic-language-program';
import { CICLanguage } from '../../../shared/services/languages-center/cic-language';

@Component({
  selector: 'cic-proof-of-enrollment',
  templateUrl: './cic-proof-of-enrollment.component.html',
  // tslint:disable-next-line:object-literal-sort-keys
  providers: [CICProofOfEnrollmentService],
})
export class CICProofOfEnrollmentComponent implements OnInit {

  errorMsg: string;
  isSearching: boolean;
  programaIdioma: any;  // Modelo para el select de languageProgram
  idioma: any;  // Modelo para el select de languages
  nivelIdioma: any;  // Modelo para el select de languageLevel
  languagePrograms: CICLanguageProgram;
  languages: CICLanguage;
  languageLevel: CICLanguageLevel;
  enrolled: CICEnrolled;
  tuqpa: any[];
  isAnyStudent: boolean;

  constructor(private cicProofOfEnrollmentService: CICProofOfEnrollmentService) {
    this.errorMsg = '';
  }

  ngOnInit() {
    this.errorMsg = '';
    this.isSearching = false;
    this.programaIdioma = '';
    this.idioma = '';
    this.nivelIdioma = '';
    this.getLanguageProgram();
    this.tuqpa = new Array();
    this.isAnyStudent = false;
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.ngOnInit();
  }

  /**
   * Nueva búsqueda.
   * Limpia los resultados de búsqueda
   */
  newSearch(): void {
    this.isSearching = false;
    this.enrolled = undefined;
  }

  /**
   * Bandera para activar/desactivar el botón "Buscar Matriculados"
   * @return {boolean} [description]
   */
  isAllSelected(): boolean {
    let isProgramaIdiomaValid: boolean;
    let isIdiomaValid: boolean;
    let isNivelIdiomaValid: boolean;

    isProgramaIdiomaValid =
      !(this.programaIdioma == null || this.programaIdioma === undefined || this.programaIdioma === '');

    isIdiomaValid = !(this.idioma == null || this.idioma === undefined || this.idioma === '');

    isNivelIdiomaValid = !(this.nivelIdioma == null || this.nivelIdioma === undefined || this.nivelIdioma === '');

    return isProgramaIdiomaValid && isIdiomaValid && isNivelIdiomaValid;
  }

  /**
   * Bandera para activar/desactivar el botón "Limpiar"
   * @return {boolean} [description]
   */
  isAnySelected(): boolean {
    let isProgramaIdiomaValid: boolean;
    let isIdiomaValid: boolean;
    let isNivelIdiomaValid: boolean;

    isProgramaIdiomaValid =
      !(this.programaIdioma == null || this.programaIdioma === undefined || this.programaIdioma === '');

    isIdiomaValid = !(this.idioma == null || this.idioma === undefined || this.idioma === '');

    isNivelIdiomaValid = !(this.nivelIdioma == null || this.nivelIdioma === undefined || this.nivelIdioma === '');

    return isProgramaIdiomaValid || isIdiomaValid || isNivelIdiomaValid;
  }

  /**
   * Programas de Idiomas del CIC
   */
  getLanguageProgram(): void {
    console.log('get language programs...');
    this.cicProofOfEnrollmentService.getLanguageProgram()
      .subscribe(
        (languagePrograms) => this.languagePrograms = languagePrograms,
        (error) => this.errorMsg = error as any,
        () => console.log('languagePrograms => ', this.languagePrograms),
      );
  }

  /**
   * Idiomas que se dictan en un Programa CIC
   * @param {string} languageProgram [description]
   */
  getLanguages(languageProgram: string): void {
    console.log('get languages by language program...');
    this.idioma = '';
    this.nivelIdioma = '';
    this.newSearch();
    this.cicProofOfEnrollmentService.getLanguages(languageProgram)
      .subscribe(
        (languages) => this.languages = languages,
        (error) => this.errorMsg = error as any,
        () => console.log('languages => ', this.languages),
      );
  }

  /**
   * Nivel de Idiomas según Idioma y Programa CIC
   * @param {string} languageId      Código de Idioma CIC
   * @param {string} languageProgram Programa CIC
   */
  getLanguageLevel(languageId: string, languageProgram: string): void {
    console.log('get language level by languageId and language program...');
    this.newSearch();
    this.cicProofOfEnrollmentService.getLanguageLevel(languageId, languageProgram)
      .subscribe(
        (languageLevel) => this.languageLevel = languageLevel,
        (error) => this.errorMsg = error as any,
        () => console.log('languageLevel => ', this.languageLevel),
      );
  }

  /**
   * Obtiene matriculados
   * @param {string} languageId      Código de idioma CIC
   * @param {string} languageProgram Programa CIC
   * @param {string} languageLevel   Nivel de Idioma CIC
   */
  getEnrolled(languageId: string, languageProgram: string, languageLevel: string): void {
    console.log('get enrolled...');
    this.isSearching = true;
    this.tuqpa = new Array();  // Limpiamos tuqpa en cada nueva búsqueda
    this.isAnyStudent = false;  // Y también limpiamos isAnyStudent para cada nueva búsqueda
    this.cicProofOfEnrollmentService.getEnrolled(languageId, languageProgram, languageLevel)
      .subscribe(
        (enrolled) => this.enrolled = enrolled,
        (error) => this.errorMsg = error as any,
        () => console.log('enrolled => ', this.enrolled),
      );
  }

  /**
   * Selecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll(): void {
    for (const key of Object.keys(this.enrolled)) {
      this.enrolled[key].is_selected = true;
    }

    this.isAnyStudent = true;
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.enrolled) {
      if (this.enrolled[key].is_selected) {
        return true;
      }
    }
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * Añade estilo a los estudiantes seleccionados
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any): void {
    if (studentObj.is_selected) {
      studentObj.is_selected = false;
      console.log('desactivado');
      // Quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
      // Ahora no todos están seleccionados
      // Pero comprobemos que a lo menos alguien esté seleccionado
      this.isAnyStudent = this.isAnyStudentSelected() ? this.isAnyStudentSelected() : false;
    } else {
      studentObj.is_selected = true;
      console.log('activado');
      // Agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);
      // console.log("this.tuqpa => ", this.tuqpa);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
    }
  }

}
