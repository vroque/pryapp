import { Component, LOCALE_ID, OnInit } from '@angular/core';

import { CICProofOfForeignLanguageService } from '../../../shared/services/languages-center/cic-proof-of-foreign-language.service';
import { CICSettingsService } from '../../../shared/services/languages-center/cic-settings.service';
import { UtilService } from '../../../shared/util.service';

import { CICCaie } from '../../../shared/services/languages-center/cic-caie';
import { CICSettings } from '../../../shared/services/languages-center/cic-settings';

@Component({
  selector: 'cic-caie-institute',
  templateUrl: './cic-caie-institute.component.html',
  providers: [
    {provide: LOCALE_ID, useValue: 'es'},
    CICProofOfForeignLanguageService, CICSettingsService, UtilService
  ],
})
export class CICCaieInstituteComponent implements OnInit {
  errorMsg: string;
  searching: boolean;
  estudiantes: any;
  students: any;
  showStudentList: boolean;
  caie: CICCaie;
  tuqpa: any[];
  anyStudent: boolean;
  selected: boolean;
  processing: boolean;
  info: CICSettings;
  util: any;

  constructor(private cicProofOfForeignLanguageService: CICProofOfForeignLanguageService,
              private cicSettingsService: CICSettingsService, private utilService: UtilService) {
    this.errorMsg = '';
    this.util = this.utilService;
  }

  ngOnInit() {
    this.clean();
    this.getInfo();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.errorMsg = '';
    this.searching = false;
    this.estudiantes = '';
    this.students = undefined;
    this.showStudentList = false;
    this.tuqpa = [];
    this.anyStudent = false;
    this.selected = false;
    this.processing = false;
  }

  /**
   * Get CAIE language level info
   */
  getInfo(): void {
    this.cicSettingsService.getCaieLanguageLevelInfo()
      .subscribe(
        (info) => this.info = info,
        (error) => console.log('error => ', error),
        () => console.log('info => ', this.info)
      );
  }

  /**
   * Bandera para activar/desactivar el botón "Buscar Estudiantes"
   * @return {boolean} [description]
   */
  isAllSelected(): boolean {
    let estudiantesValid: boolean;

    estudiantesValid =
      this.estudiantes !== null && this.estudiantes !== undefined && this.estudiantes !== '' &&
      ((this.estudiantes).trim()).length !== 0;

    return estudiantesValid;
  }

  /**
   * Bandera para activar/desactivar el botón "Limpiar"
   * @return {boolean} [description]
   */
  isAnySelected(): boolean {
    let estudiantesValid: boolean;

    estudiantesValid =
      this.estudiantes !== null && this.estudiantes !== undefined && this.estudiantes !== '';

    return estudiantesValid;
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray(): void {
    this.caie = undefined;
    this.searching = false;
    this.students = this.utilService.stringToArrayWithoutDuplicates(this.estudiantes);
    console.log('input students => ', this.students);
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList(): void {
    this.showStudentList = !this.showStudentList;
  }

  /**
   * Busca datos de estudiantes para procesar CAIE Batch
   */
  search(): void {
    console.log('get students for CAIE Batch...');
    this.searching = true;
    this.tuqpa = [];
    this.caie = undefined;
    this.anyStudent = false;
    this.selected = false;
    this.processing = true;
    const type = 'searchInstitute';
    this.cicProofOfForeignLanguageService.getCaieBatch(type, this.students, [])
      .subscribe(
        (caie) => this.caie = caie,
        (error) => {
          console.log(error);
          this.processing = false;
          this.searching = false;
        },
        () => {
          this.processing = false;
          this.searching = false;
          console.log('caie => ', this.caie);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Selecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll(): void {
    for (const key of Object.keys(this.caie)) {
      this.caie[key].isSelected = !this.selected;
    }

    this.selected = !this.selected;
    this.anyStudent = this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.caie) {
      if (this.caie[key].isSelected) {
        return true;
      }
    }
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * Añade estilo a los estudiantes seleccionados
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any): void {
    if (studentObj.isSelected) {
      studentObj.isSelected = false;
      console.log('desactivado');
      // Quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
      // Ahora no todos están seleccionados
      // Pero comprobemos que a lo menos alguien esté seleccionado
      this.anyStudent = this.isAnyStudentSelected() ? this.isAnyStudentSelected() : false;
    } else {
      studentObj.isSelected = true;
      console.log('activado');
      // Agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);
      // console.log("this.tuqpa => ", this.tuqpa);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
    }
  }

  /**
   * Obtiene CAIE generado
   */
  getCAIE(): void {
    console.log('Get CAIE...');
    this.getSequentialCAIE(0);
    console.log('caie => ', this.caie);
  }

  getSequentialCAIE(i: number): void {
    console.log('Sequential!');
    console.log('proceso => ', i);
    const type = 'caieInstitute';

    if (this.caie[i] !== undefined) {
      if (this.caie[i].isSelected && (this.caie[i].paymentStatus === 1 || this.caie[i].paymentStatus === 0)) {
        this.caie[i].docUrl = undefined;
        this.processing = true;
        this.errorMsg = undefined;
        this.caie[i].processing = true;
        this.cicProofOfForeignLanguageService.getCaieBatch(type, [], this.caie)
          .subscribe(
            (caie) => this.caie = caie,
            (error) => {
              console.log(error);
              this.processing = false;
              this.caie[i].processingError = true;
              this.caie[i].docUrl = undefined;
              this.getSequentialCAIE(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
            () => {
              this.processing = false;
              this.caie[i].processingError = false;
              this.getSequentialCAIE(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
          );
      } else {
        console.log('no seleccionado');
        this.getSequentialCAIE(i + 1);
      }
    } else {
      console.log('Nada que procesar => ', this.caie[i]);
    }
    console.log('+--------------------------+');
  }
}
