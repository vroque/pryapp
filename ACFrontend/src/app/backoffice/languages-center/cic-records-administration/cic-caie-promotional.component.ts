import { Component, LOCALE_ID, OnInit } from '@angular/core';

import { CICLanguageService } from '../../../shared/services/languages-center/cic-language.service';
// tslint:disable-next-line:max-line-length
import { CICCaiePromotionalService } from '../../../shared/services/languages-center/cic-records-administration/cic-caie-promotional.service';
import { CICProofOfForeignLanguageService } from '../../../shared/services/languages-center/cic-proof-of-foreign-language.service';
import { CICSettingsService } from '../../../shared/services/languages-center/cic-settings.service';

import { GraduationPeriods } from '../../../shared/services/academic/graduation-periods';
import { CICCaie } from '../../../shared/services/languages-center/cic-caie';
import { CICLanguage } from '../../../shared/services/languages-center/cic-language';
import { CICSettings } from '../../../shared/services/languages-center/cic-settings';

@Component({
  selector: 'cic-caie-promotional',
  templateUrl: './cic-caie-promotional.component.html',
  providers: [
    {provide: LOCALE_ID, useValue: 'es'},
    CICCaiePromotionalService, CICLanguageService, CICSettingsService,
    CICProofOfForeignLanguageService
  ],
})
export class CICCaiePromotionalComponent implements OnInit {

  errorMsg: string;
  searching: boolean;
  periodoGraduacion: any; // Modelo para el select de período graduación
  idioma: any; // Modelo para el select de idioma CIC
  graduationPeriods: GraduationPeriods;
  languages: CICLanguage;
  caie: CICCaie;
  tuqpa: any[];
  isAnyStudent: boolean;
  selected = false;
  processing = false;
  info: CICSettings;

  constructor(private cicProofOfForeignLanguageService: CICProofOfForeignLanguageService,
              private cicCaiePromotionalService: CICCaiePromotionalService,
              private languageService: CICLanguageService,
              private cicSettingsService: CICSettingsService) {
    this.errorMsg = '';
  }

  ngOnInit() {
    this.getGraduationPeriods();
    this.getLanguages();
    this.getInfo();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.errorMsg = '';
    this.searching = false;
    this.periodoGraduacion = '';
    this.idioma = '';
    this.tuqpa = [];
    this.isAnyStudent = false;
    this.selected = false;
    this.processing = false;
    this.caie = undefined;
  }

  /**
   * Get CAIE language level info
   */
  getInfo(): void {
    this.cicSettingsService.getCaieLanguageLevelInfo()
      .subscribe(
        (info) => this.info = info,
        (error) => console.log(error),
        () => console.log('info => ', this.info)
      );
  }

  /**
   * Nueva búsqueda.
   * Limpia los resultados de búsqueda
   */
  newSearch() {
    this.searching = false;
    this.caie = undefined;
    this.tuqpa = [];
  }

  /**
   * Bandera para activar/deactivar el boton "Buscar Estudiantes"
   */
  isAllSelected(): boolean {
    let isPeriodoGraduacionValid: boolean;
    let isIdiomaValid: boolean;

    isPeriodoGraduacionValid =
      this.periodoGraduacion !== null && this.periodoGraduacion !== undefined && this.periodoGraduacion !== '';

    isIdiomaValid = this.idioma !== null && this.idioma !== undefined && this.idioma !== '';

    return isPeriodoGraduacionValid && isIdiomaValid;
  }

  /**
   * Bandera para activar/desactivar el boton "Limpiar"
   */
  isAnySelected(): boolean {
    let isPeriodoGraduacionValid: boolean;
    let isIdiomaValid: boolean;

    isPeriodoGraduacionValid =
      this.periodoGraduacion !== null && this.periodoGraduacion !== undefined && this.periodoGraduacion !== '';

    isIdiomaValid = this.idioma !== null && this.idioma !== undefined && this.idioma !== '';

    return isPeriodoGraduacionValid || isIdiomaValid;
  }

  /**
   * Períodos académicos de la UC en los que existen graduados
   */
  getGraduationPeriods(): void {
    console.log('get graduation periods...');
    this.cicCaiePromotionalService.getGraduationPeriods()
      .subscribe(
        (graduationPeriods) => this.graduationPeriods = graduationPeriods,
        (error) => console.log(error),
        () => {
          console.log('graduationPeriods => ', this.graduationPeriods);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Idiomas del CIC
   */
  getLanguages(): void {
    console.log('get languages...');
    this.languageService.getLanguages()
      .subscribe(
        (languages) => this.languages = languages,
        (error) => console.log(error),
        () => {
          console.log('languages => ', this.languages);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * CAIE Promocional
   * @param {string} graduationPeriods Período de egreso de la UC
   * @param {string} languageId        Código de Idioma CIC
   */
  search(graduationPeriods: string, languageId: string): void {
    console.log('get students for CAIE Promotional...');
    this.searching = true;
    this.tuqpa = [];
    this.caie = undefined;
    this.isAnyStudent = false;
    this.selected = false;
    this.processing = true;
    this.cicProofOfForeignLanguageService.getCAIEPromotional(graduationPeriods, languageId)
      .subscribe(
        (caie) => this.caie = caie,
        (error) => {
          console.log(error);
          this.processing = false;
          this.searching = false;
        },
        () => {
          this.processing = false;
          this.searching = false;
          console.log('caie => ', this.caie);
          console.log('+--------------------------+');
        },
      );
  }

  /**
   * Selecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll(): void {
    for (const key of Object.keys(this.caie)) {
      this.caie[key].isSelected = !this.selected;
    }

    this.selected = !this.selected;
    this.isAnyStudent = this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.caie) {
      if (this.caie[key].isSelected) {
        return true;
      }
    }
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * @param studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any): void {
    if (studentObj.isSelected) {
      studentObj.isSelected = false;
      console.log('desactivado');
      // Quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
      // Ahora no todos están seleccionados
      // Pero comprobemos que a lo menos alguien esté seleccionado
      this.isAnyStudent = this.isAnyStudentSelected() ? this.isAnyStudentSelected() : false;
    } else {
      studentObj.isSelected = true;
      console.log('activado');
      // Agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);
      // console.log("this.tuqpa => ", this.tuqpa);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
    }
  }

  /**
   * Obtiene CAIE generado
   */
  getCAIE(): void {
    console.log('Get CAIE...');
    this.getSequentialCAIE(0);
    console.log('caie => ', this.caie);
  }

  /**
   * Petición secuencial para generar Constancia de Idioma Extranjero
   * NOTE: No es "LA" solución para este tipo de peticiones pero es lo que hay
   * @param {number} i
   */
  getSequentialCAIE(i: number): void {
    console.log('Sequential!');
    console.log('proceso => ', i);
    const type = 'caie';

    if (this.caie[i] !== undefined) {
      if (this.caie[i].isSelected && this.caie[i].paymentStatus === 1) {
        this.caie[i].docUrl = undefined;
        this.processing = true;
        this.errorMsg = undefined;
        this.caie[i].processing = true;
        this.cicProofOfForeignLanguageService.getCaieBatch(type, [], this.caie)
          .subscribe(
            (caie) => this.caie = caie,
            (error) => {
              console.log(error);
              this.processing = false;
              this.caie[i].processingError = true;
              this.caie[i].docUrl = undefined;
              this.getSequentialCAIE(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
            () => {
              this.processing = false;
              this.caie[i].processingError = false;
              this.getSequentialCAIE(i + 1);
              console.log('Enviando el proceso => ', i + 1);
            },
          );
      } else {
        console.log('no seleccionado');
        this.getSequentialCAIE(i + 1);
      }
    } else {
      console.log('Nada que procesar => ', this.caie[i]);
    }
    console.log('+--------------------------+');
  }

}
