import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BackofficeConfigService } from '../../config/config.service';
import { ConfigService } from '../../../config/config.service';
import { CICSearchService } from '../../../shared/services/languages-center/cic-search.service';
import { UtilService } from '../../../shared/util.service';

import { CICStudent } from '../../../shared/services/languages-center/cic-student';

@Component({
  selector: 'cic-extra-student-record',
  templateUrl: './cic-extra-language-record.component.html',
  providers: [
    CICSearchService, ConfigService, BackofficeConfigService, UtilService
  ]
})
export class CICExtraLanguageRecordComponent implements OnInit {
  searching: boolean;
  showStudentsList: boolean;
  estudiantes: string; // Modelo para el input de students
  students: string[]; // Array de 'estudiantes'
  studentSearchResult: CICStudent[];
  config: any;
  util: any;

  constructor(private cicSearchService: CICSearchService,
              private backConfigService: BackofficeConfigService,
              private router: Router, private utilService: UtilService) {
    this.config = this.backConfigService;
    this.util = this.utilService;
  }

  ngOnInit() {
    this.clean();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.searching = false;
    this.estudiantes = '';
    this.students = undefined;
    this.showStudentsList = false;
    this.studentSearchResult = undefined;
  }

  /**
   * Bandera para activar/desactivar los botones "Buscar Estudiantes" y "Limpiar"
   * @return {boolean} [description]
   */
  isValidInput(): boolean {
    let isEstudiantesValid: boolean;

    isEstudiantesValid =
      this.estudiantes !== null && this.estudiantes !== undefined && this.estudiantes !== '' &&
        ((this.estudiantes).trim()).length !== 0;

    return isEstudiantesValid;
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray(): void {
    this.studentSearchResult = undefined;
    this.searching = false;
    this.students = this.utilService.stringToArrayWithoutDuplicates(this.estudiantes);
    console.log('input students => ', this.students);
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsCodeList(): void {
    this.showStudentsList = !this.showStudentsList;
  }

  /**
   * Busca datos de estudiantes para procesar CAIE Batch
   * @param {any} students Código de estudiantes a buscar
   */
  search(): void {
    console.log('search students...');
    this.searching = true;
    this.studentSearchResult = undefined;
    this.cicSearchService.searchStudent(this.students)
      .subscribe(
        (searchResult) => this.studentSearchResult = searchResult,
        (error) => {
          console.log('error => ', error);
          this.searching = false;
        },
        () => {
          this.searching = false;
          console.log('studentSearchResult => ', this.studentSearchResult);
        },
      );
  }

  /**
   * Go to student language record
   * @param studentId
   */
  goToStudentLanguageRecord(studentId: string): void {
    const link = [`backoffice/centro-idiomas/historial-estudiante/${studentId}`];
    this.router.navigate(link);
  }
}
