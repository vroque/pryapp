import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { CICSearchService } from '../../../shared/services/languages-center/cic-search.service';
import { CICStudentLanguageRecordService } from '../../../shared/services/languages-center/cic-student-language-record.service';
import { UtilService } from '../../../shared/util.service';

import { CICStudent } from '../../../shared/services/languages-center/cic-student';
import { CICStudentLanguageRecord } from '../../../shared/services/languages-center/cic-student-language-record';

@Component({
  selector: 'cic-extra-student-language-record',
  templateUrl: './cic-extra-student-language-record.component.html',
  providers: [
    {provide: LOCALE_ID, useValue: 'es'},
    CICSearchService,
    CICStudentLanguageRecordService,
    UtilService
  ]
})
export class CICExtraStudentLanguageRecordComponent implements OnInit {
  student: CICStudent;
  studentSearchResult: CICStudent[];
  studentLanguageRecord: CICStudentLanguageRecord[];
  studentLangRecord: CICStudentLanguageRecord;
  loading: boolean;
  loadingDetail: boolean;
  updating: boolean;
  util: any;

  constructor(private activatedRoute: ActivatedRoute, private cicSearchService: CICSearchService,
              private cicStudentLanguageRecordService: CICStudentLanguageRecordService,
              private utilService: UtilService) {
    this.util = this.utilService;
  }

  ngOnInit() {
    this.getData();
  }

  /**
   * Información básica del estudiante
   */
  getData(): void {
    this.studentLangRecord = undefined;
    this.activatedRoute.params.forEach((params: Params) => {
      const studentId: string = (params as any).student;
      this.getStudentDetail(studentId);
    });
  }

  /**
   * Student detail
   * @param studentId
   */
  getStudentDetail(studentId: string): void {
    this.loading = true;
    this.loadingDetail = true;
    const studentList: string[] = [studentId];
    this.cicSearchService.searchStudent(studentList)
      .subscribe(
        (searchResult) => this.studentSearchResult = searchResult,
        (error) => {
          console.log('error => ', error);
          this.loading = false;
        },
        () => {
          if (this.studentSearchResult.length > 0) {
            this.student = this.studentSearchResult[0];
          }
          console.log('student => ', this.student);
          this.loading = false;
          if (this.student.persona !== null && this.student.profiles.length > 0) {
            this.getStudentLanguageRecord(studentId);
          } else {
            this.loadingDetail = false;
          }
        },
      );
  }

  /**
   * Student language record
   * @param studentId
   */
  getStudentLanguageRecord(studentId: string): void {
    this.loadingDetail = true;
    this.cicStudentLanguageRecordService.getStudentLanguageRecord(studentId)
      .subscribe(
        (result) => this.studentLanguageRecord = result,
        (error) => {
          console.log('error => ', error);
          this.loadingDetail = false;
        },
        () => {
          console.log('studentLanguageRecord => ', this.studentLanguageRecord);
          this.loadingDetail = false;
        }
      );
  }

  /**
   * Filter language record by languageId
   * @param languageId
   */
  getLanguageRecord(languageId: string): void {
    this.studentLangRecord = this.studentLanguageRecord
      .filter((item) => item.language.languageId === languageId)[0];
  }
}
