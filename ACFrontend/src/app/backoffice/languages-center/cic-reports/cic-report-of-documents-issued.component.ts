import { Component, OnInit } from '@angular/core';

// tslint:disable-next-line:max-line-length
import { CICReportOfDocumentsIssuedService } from '../../../shared/services/languages-center/cic-reports/cic-report-of-documents-issued.service';

import { CICLanguage } from '../../../shared/services/languages-center/cic-language';
import { CICLanguageProgram } from '../../../shared/services/languages-center/cic-language-program';
import { CICPeriods } from '../../../shared/services/languages-center/cic-periods';

@Component({
  selector: 'cic-report-documents-issued',
  templateUrl: './cic-report-of-documents-issued.component.html',
  providers: [CICReportOfDocumentsIssuedService]
})
export class CICReportOfDocumentsIssuedComponent implements OnInit {

  errorMsg: string;
  idioma: any;  // Modelo para el select de languages
  periodo: any;  // Modelo para el select de periods
  programaIdioma: any;  // Modelo para el select de languageProgram
  languages: CICLanguage;
  periods: CICPeriods[];
  languagePrograms: CICLanguageProgram[];

  constructor(private cicReportOfDocumentsIssuedService: CICReportOfDocumentsIssuedService) {
    this.errorMsg = '';
  }

  ngOnInit() {
    this.errorMsg = '';
    this.idioma = '';
    this.periodo = '';
    this.programaIdioma = '';
    this.getPeriods();
    this.getLanguages();
    this.getLanguageProgram();
  }

  /**
   * Limpiar modelos
   */
  clean() {
    this.ngOnInit();
  }

  /**
   * Bandera para activar/desactivar el botón "Generar Reporte"
   * @return {boolean} [description]
   */
  isAllSelected(): boolean {
    let isIdiomaValid: boolean;
    let isPeriodoValid: boolean;
    let isProgramaIdiomaValid: boolean;

    if (this.idioma == null || this.idioma === undefined || this.idioma === '') {
      isIdiomaValid = false;
    } else {
      isIdiomaValid = true;
    }

    if (this.periodo == null || this.periodo === undefined || this.periodo === '') {
      isPeriodoValid = false;
    } else {
      isPeriodoValid = true;
    }

    if (this.programaIdioma == null || this.programaIdioma === undefined || this.programaIdioma === '') {
      isProgramaIdiomaValid = false;
    } else {
      isProgramaIdiomaValid = true;
    }

    if (isIdiomaValid && isPeriodoValid && isProgramaIdiomaValid) {
      return true;
    } else {
      return false;
    }

  }

  /**
   * Bandera para activar/desactivar el botón "Limpiar"
   * @return {boolean} [description]
   */
  isAnySelected(): boolean {
    let isIdiomaValid: boolean;
    let isPeriodoValid: boolean;
    let isProgramaIdiomaValid: boolean;

    if (this.idioma == null || this.idioma === undefined || this.idioma === '') {
      isIdiomaValid = false;
    } else {
      isIdiomaValid = true;
    }

    if (this.periodo == null || this.periodo === undefined || this.periodo === '') {
      isPeriodoValid = false;
    } else {
      isPeriodoValid = true;
    }

    if (this.programaIdioma == null || this.programaIdioma === undefined || this.programaIdioma === '') {
      isProgramaIdiomaValid = false;
    } else {
      isProgramaIdiomaValid = true;
    }

    if (isIdiomaValid || isPeriodoValid || isProgramaIdiomaValid) {
      return true;
    } else {
      return false;
    }

  }

  /**
   * Idiomas del CIC
   */
  getLanguages() {
    console.log('get languages...');
    this.cicReportOfDocumentsIssuedService.getLanguages()
      .subscribe(
        languages => this.languages = languages,
        error => this.errorMsg = <any>error,
        () => console.log('languages =>', this.languages)
      );
  }

  /**
   * Períodos académicos del CIC
   */
  getPeriods() {
    console.log('get periods...');
    this.cicReportOfDocumentsIssuedService.getPeriods()
      .subscribe(
        periods => this.periods = periods,
        error => this.errorMsg = <any>error,
        () => console.log('periods => ', this.periods)
      );
  }

  /**
   * Programas de Idiomas del CIC
   */
  getLanguageProgram() {
    console.log('get language programs...');
    this.cicReportOfDocumentsIssuedService.getLanguageProgram()
      .subscribe(
        languagePrograms => this.languagePrograms = languagePrograms,
        error => this.errorMsg = <any>error,
        () => console.log('languagePrograms => ', this.languagePrograms)
      );
  }

}
