import { Component, OnInit } from '@angular/core';
import { ConfigService} from '../../config/config.service';
import { DisplayBlock } from '../../shared/services/display-block/display-block';
import { DisplayBlockService } from '../../shared/services/display-block/display-block.service';
import { Message } from 'primeng/primeng';

@Component({
  providers: [DisplayBlockService, ConfigService],
  selector: 'display-block',
  templateUrl: 'display-block.component.html'
})
export class DisplayBlockComponent implements OnInit {

  displayList: DisplayBlock[] = [];
  editDisplay: DisplayBlock;
  backupEditDisplay: DisplayBlock;
  newDisplay: DisplayBlock;
  loading = true;
  displayOptions: {list: boolean; edit: boolean; create: boolean};
  errorMessage: string;
  fecha: Date;
  es: any;
  position: number;
  change = false;
  primeMsg: Message[];
  filter: any;

  constructor(private displayBlockService: DisplayBlockService, private configService: ConfigService) { }

  ngOnInit() {
    this.updateDisplayOption('list');
    this.getAll();
    this.es = this.configService.getLocationCalendar();
  }

  // función para cambiar de vista
  updateDisplayOption(type: string) {
    this.displayOptions = { list: false, edit: false, create: false };
    this.change = false;
    if (type != null) {
      this.displayOptions[type] = true;
      if (type === 'list') {
        if (this.backupEditDisplay != null) {
          this.displayList[this.position] = this.backupEditDisplay;
        }
      } else if (type === 'create') {
        this.newDisplay = { id: 0, name: '', description: '', endFech: new Date(), startFech: new Date(), active: false };
      }
    }
  }

  changeToEdit(data: DisplayBlock, position: number) {
    this.position = position;
    this.backupEditDisplay = JSON.parse(JSON.stringify(data));
    this.editDisplay = this.convertDate(data);
    this.updateDisplayOption('edit');
  }

  getAll() {
    this.displayBlockService.getAll()
      .subscribe(
        (data) => {
          if (data.length > 0) {
            this.displayList = data;
            this.updateDisplayOption('list');
          } else {
            this.errorMessage = 'No se encontro registros';
          }
          this.loading = false;
        },
        (error) => {
          this.errorMessage = 'No se encontro registros';
          this.loading = false;
        },
    );
  }

  convertDateList(data: DisplayBlock[]): DisplayBlock[] {
    for (let display of data) {
      display = this.convertDate(display);
    }
    return data;
  }
  convertDate(data: DisplayBlock): DisplayBlock {
    data.startFech = new Date(data.startFech as any);
    data.endFech = new Date(data.endFech as any);
    return data;
  }

  create() {
    const create = JSON.parse(JSON.stringify(this.newDisplay));
    this.displayBlockService.post(create)
      .subscribe(
        (data) => {
          this.displayList.unshift(this.convertDate(data));
          this.updateDisplayOption('list');
          this.primeMsg = [{
            detail: 'Se creo el registro',
            severity: 'success',
            summary: 'Enhorabuena',
          }];
        },
        (error) => {
          console.log(error);
          this.primeMsg = [{
            detail: 'No se pudo crear el registro, consulte con el administrador',
            severity: 'warn',
            summary: 'Error',
          }];
        },
    );
  }

  update() {
    const edit = JSON.parse(JSON.stringify(this.editDisplay));
    edit.startFech = this.convertToUTC(this.editDisplay.startFech);
    edit.endFech = this.convertToUTC(this.editDisplay.endFech);

    this.displayBlockService.put(edit)
      .subscribe(
        (data) => {
          data = this.convertDate(data);
          this.backupEditDisplay = this.editDisplay;
          this.updateDisplayOption('list');
          this.primeMsg = [{
            detail: 'Se actualizao el registro',
            severity: 'success',
            summary: 'Enhorabuena',
          }];
        },
        (error) => {
          console.log(error);
          this.primeMsg = [{
            detail: 'No se pudo actualizar el registro, consulte con el administrador',
            severity: 'warn',
            summary: 'Error',
          }];
        },
    );
  }

  verifyChangue(item: string) {
    if (this.editDisplay[item] === this.backupEditDisplay[item]) {
      this.change = false;
    } else {
      this.change = true;
    }
  }

  convertToUTC(data: Date): Date {
    const utc: any = new Date(data.getTime() - data.getTimezoneOffset() * 60000).toUTCString();
    return utc;
  }

}
