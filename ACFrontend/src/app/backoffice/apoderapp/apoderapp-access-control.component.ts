import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {StudentService} from '../../shared/services/apoderapp/student.service';
import {Student} from '../../shared/services/apoderapp/student';

@Component({
  providers: [StudentService],
  selector: 'apoderapp-access-control',
  templateUrl: './apoderapp-access-control.component.html'
})
export class ApoderappAccessControlComponent implements OnInit {

  errorMsg: string;
  isSearching: boolean;
  estudiantes: any;  // modelo para el input de Estudiantes
  students: any[];  // array de 'estudiantes' a procesar
  studentsRpta: Student;
  tuqpa: any[];
  isShowStudentsList: boolean;
  isAnyStudent: boolean;
  selected = false;

  constructor(private router: Router, private studentService: StudentService) {
    this.errorMsg = '';
  }

  ngOnInit() {
    this.clean();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.errorMsg = '';
    this.isSearching = false;
    this.estudiantes = undefined;
    this.students = [];
    this.studentsRpta = undefined;
    this.tuqpa = [];
    this.isShowStudentsList = false;
    this.isAnyStudent = false;
    this.selected = false;
  }

  /**
   * Activa/Desactiva el botón "Buscar Estudiantes"
   * @returns {boolean}
   */
  isAllSelected(): boolean {
    return !(this.estudiantes == null || this.estudiantes === undefined || this.estudiantes.trim() === '');
  }

  /**
   * Activa/Desactiva el botón "Limpiar"
   * @returns {boolean}
   */
  isAnySelected(): boolean {
    return !(this.estudiantes == null || this.estudiantes === undefined || this.estudiantes.trim() === '');
  }

  /**
   * Array de estudiantes
   */
  getStudentsArray(): void {
    this.isSearching = false;
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // http://stackoverflow.com/a/9229821
    this.students = this.estudiantes.split(' ')
      .filter(// filter blank space
        (id) => id !== '',
      )
      .filter(// filter duplicates
        (id, position, self) => self.indexOf(id) === position,
      );
    console.log('input students => ', this.students);
  }

  /**
   * Muestra/Oculta los códigos de estudiantes ingresados para la búsqueda batch
   */
  showStudentsList(): void {
    this.isShowStudentsList = !this.isShowStudentsList;
  }

  /**
   * Estudiantes UC según DNI
   */
  searchStudents(): void {
    console.log('Get Students...');
    this.isSearching = true;
    this.tuqpa = [];
    this.studentsRpta = undefined;
    this.isAnyStudent = false;
    this.selected = false;
    const type = 'search';
    this.studentService.searchByDocumentNumber(type, this.students)
      .subscribe(
        (studentsRpta) => this.studentsRpta = studentsRpta,
        (error) => this.errorMsg = error as any,
      );
    console.log('this.students => ', this.students);
  }

  /**
   * Selecciona/Deselecciona todos los estudiantes para luego generar las constancias
   * Añade estilo a los estudiantes seleccionados
   */
  selectAll(): void {
    for (const student of this.students) {
      student.is_selected = !this.selected;
    }

    this.selected = !this.selected;
    this.isAnyStudent = this.selected;
    this.tuqpa = this.selected ? this.tuqpa : [];
  }

  /**
   * Verifica si algún estudiante se encuentra seleccionado
   * Ayuda a activar el botón de Generar constancias
   * @returns {boolean}
   */
  isAnyStudentSelected(): boolean {
    for (const key in this.students) {
      if (this.students[key].is_selected) {
        return true;
      }
    }
  }

  /**
   * Añade/Quita Estudiantes a procesar.
   * Añade estilo a los estudiantes seleccionados
   * @param {any} studentObj Arreglo de estudiantes a procesar
   */
  setStudentsToProcess(studentObj: any): void {
    if (studentObj.is_selected) {
      studentObj.is_selected = false;
      console.log('Desactivado');
      // quitamos el objecto del array tuqpa
      this.tuqpa = this.tuqpa.filter((el) => el !== studentObj);
      console.log('this.tuqpa => ', this.tuqpa);
      // ahora no todos están seleccionados
      // pero comprobemos que a lo menos alguien esté seleccionado
      this.isAnyStudent = this.isAnyStudentSelected() ? this.isAnyStudentSelected() : false;
    } else {
      studentObj.is_selected = true;
      console.log('activado');
      // agregamos el objeto al array tuqpa
      this.tuqpa.push(studentObj);
      // console.log("this.tuqpa => ", this.tuqpa);

      /*
       * Remove duplicates from an array of objects
       * http://stackoverflow.com/a/36744732
       */
      this.tuqpa = this.tuqpa.filter(
        (tuqpa, index, self) => self.findIndex(
          (t) => {
            return t === tuqpa;
          }) === index);
      console.log('this.tuqpa => ', this.tuqpa);
      // aquí ya no es necesario verificar si alguien está seleccionado xD
    }
  }

  /**
   * Ir a detalle de Estudiante
   * @param {string} dni
   */
  studentDetail(dni: string): void {
    console.log('Detail for => ', dni);
    const link: string[] = [`backoffice/apoderados/control-acceso/${dni}`];
    this.router.navigate(link);
  }

}
