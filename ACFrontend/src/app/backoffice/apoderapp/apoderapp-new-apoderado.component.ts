import {ApoderadoService} from '../../shared/services/apoderapp/apoderado.service';
import {Component, Input, LOCALE_ID, OnInit} from '@angular/core';
import {KinshipService} from '../../shared/services/persona/kinship.service';
import {Kinship} from '../../shared/services/persona/kinship';
import {PersonaService} from '../../shared/services/persona/persona.service';
import {Persona} from '../../shared/services/persona/persona';
import {Router} from '@angular/router';
import {Student} from '../../shared/services/apoderapp/student';

@Component({
  selector: 'apoderapp-new-apoderado',
  templateUrl: './apoderapp-new-apoderado.component.html',
  providers: [
    ApoderadoService,
    KinshipService,
    PersonaService,
    {provide: LOCALE_ID, useValue: 'es'},
  ],
})
export class ApoderappNewApoderadoComponent implements OnInit {

  es: any;
  wata: number;
  errorMsg: string;
  isSearching: boolean;
  showSearchResult: boolean;
  showSavedSuccessfull: boolean;
  showSavedPersonaSuccessfull: boolean;
  personas: any;  // modelo para el input de Personas
  people: any[];  // array de 'personas' a procesar
  runa: Persona;
  tuqpa: any[];
  isShowPeopleList: boolean;
  isAnyPerson: boolean;
  selected = false;
  showDetailPerson: boolean; // mostrar detalle persona
  showDetailNewPerson: boolean; // mostrar detalle registro nueva persona
  sapaRuna: Persona; // Persona detalle
  kinship: Kinship;
  parentesco: number; // Kinship; // modelo para el select de parentesco
  @Input() student: Student;
  comment: string;
  savingApoderado: boolean;
  isProcessing: boolean;
  existParentesco: boolean;
  nuevaPersona: Persona;
  per: Persona;

  constructor(private apoderadoService: ApoderadoService, private kinshipService: KinshipService,
              private personaService: PersonaService, private router: Router) {
    this.errorMsg = '';
  }

  ngOnInit() {
    this.clean();
    this.es = {
      clear: 'Limpiar',
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
      dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
      firstDayOfWeek: 0,
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre',
        'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
      today: 'Hoy',
    };
    this.wata = (new Date()).getFullYear();
    this.getKinship();
  }

  /**
   * Limpiar modelos
   */
  clean(): void {
    this.errorMsg = '';
    this.isSearching = false;
    this.personas = undefined;
    this.people = [];
    this.runa = undefined;
    this.tuqpa = [];
    this.isShowPeopleList = false;
    this.isAnyPerson = false;
    this.selected = false;
    this.showDetailPerson = false;
    this.showDetailNewPerson = false;
    this.sapaRuna = undefined;
    this.kinship = undefined;
    this.parentesco = undefined;
    this.comment = undefined;
    this.savingApoderado = false;
    this.showSearchResult = false;
    this.showSavedSuccessfull = false;
    this.isProcessing = false;
    this.existParentesco = false;
    this.neoPerson();
  }

  neoPerson(): void {
    this.nuevaPersona = {
      fathers_last_name: undefined,
      mothers_last_name: undefined,
      first_name: undefined,
      document_type: undefined,
      document_number: undefined,
      gender: undefined,
      birthdate: undefined,
      last_name: undefined,
      full_name: undefined,
      address: undefined,
      country: undefined,
      ubigeo: undefined,
      marital_status: undefined,
      id: undefined,
      cellphone: undefined,
      rpm: undefined,
      email1: undefined,
      email2: undefined,
      userName: undefined,
    };
  }

  /**
   * Activa/Desactiva el botón "Buscar Personas"
   * @returns {boolean}
   */
  isAllSelected(): boolean {
    return !(this.personas == null || this.personas === undefined || this.personas.trim() === '');
  }

  /**
   * Activa/Desactiva el botón "Limpiar"
   * @returns {boolean}
   */
  isAnySelected(): boolean {
    return !(this.personas == null || this.personas === undefined || this.personas.trim() === '');
  }

  /**
   * Array de personas
   */
  getPeopleArray(): void {
    this.isSearching = false;
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // http://stackoverflow.com/a/9229821
    this.people = this.personas.split(' ')
      .filter(// filter blank space
        (id) => id !== '',
      )
      .filter(// filter duplicates
        (id, position, self) => self.indexOf(id) === position,
      );
    console.log('input people => ', this.people);
    console.log('+--------------------------+');
  }

  /**
   * Muestra/Oculta los códigos de personas ingresados para la búsqueda batch
   */
  showPeopleList(): void {
    this.isShowPeopleList = !this.isShowPeopleList;
  }

  /**
   * Lista de parentesco
   */
  getKinship(): void {
    this.kinshipService.getKinship()
      .subscribe(
        (kinship) => this.kinship = kinship,
        (error) => this.errorMsg = error as any,
      );
    console.log('this.kinship => ', this.kinship);
    console.log('+--------------------------+');
  }

  /**
   * Busca personas... de RPP
   */
  searchPeople(): void {
    console.log('Get People...');
    this.isSearching = true;
    this.tuqpa = [];
    this.runa = undefined;
    this.isAnyPerson = false;
    this.selected = false;
    this.showDetailPerson = false;
    this.showDetailNewPerson = false;
    this.showSavedSuccessfull = false;
    this.savingApoderado = false;
    this.showSavedPersonaSuccessfull = false;
    this.apoderadoService.searchStudentApoderadoByDocumentNumber(this.people, this.student)
      .subscribe(
        (runa) => this.runa = runa,
        (error) => this.errorMsg = error as any,
        () => {
          this.isSearching = false;
          this.showSearchResult = true;
        },
      );
    console.log('this.people => ', this.people);
    console.log('+--------------------------+');
  }

  /**
   * Detalle de Persona.
   * Se usa para añadir nuevo apoderado a un estudiante.
   * @param {Persona} person
   */
  detailPerson(person: Persona): void {
    this.savingApoderado = false;
    this.parentesco = undefined;
    this.comment = undefined;
    this.neoPerson();
    this.showSavedSuccessfull = false;
    this.showDetailPerson = !this.showDetailPerson;
    this.showDetailNewPerson = false;
    this.showSavedPersonaSuccessfull = false;
    this.sapaRuna = undefined;
    if (this.showDetailPerson) {
      this.sapaRuna = person;
    }
  }

  /**
   * Detalle nde nueva Persona
   * @param {Persona} person
   */
  detailNewPerson(person: Persona): void {
    this.showDetailPerson = false;
    this.showDetailNewPerson = !this.showDetailNewPerson;
    this.nuevaPersona.document_number = person.document_number;
  }

  /**
   * Asigna nuevo apoderado a un estudiante
   */
  addApoderado(): void {
    console.log('Saving Apoderado...');
    this.savingApoderado = true;
    this.showSavedPersonaSuccessfull = false;
    this.apoderadoService.addApoderado(this.student, this.sapaRuna, this.parentesco, this.comment)
      .subscribe(
        (student) => this.student = student,
        (error) => {
          this.errorMsg = error as any;
          this.showSavedSuccessfull = false;
        },
        () => {
          this.savingApoderado = false;
          this.showDetailPerson = false;
          this.showSearchResult = false;
          this.showSavedSuccessfull = true;
        },
      );
    console.log('this.student => ', this.student);
    console.log('+--------------------------+');
  }

  /**
   * Validación de Sustento de cambio de acceso del apoderado
   */
  validateComment(): void {
    this.comment = this.comment.trim();
    this.isValidNewApoderadoInput();
  }

  /**
   * Validación de Sustento de cambio de acceso del apoderado
   * @returns {boolean}
   */
  isValidNewApoderadoInput(): boolean {
    const isValidComment: boolean = !(this.comment == null || this.comment === undefined || this.comment.trim() === '');
    return isValidComment && this.existParentesco;
  }

  /**
   * Selección de parentesco
   */
  newParentesco(): void {
    console.log('this.parentesco => ', this.parentesco);
    this.existParentesco = !(this.parentesco == null || this.parentesco === undefined);
  }

  newPerson(): void {
    this.showSavedPersonaSuccessfull = false;
    this.personaService.newPerson(this.nuevaPersona)
      .subscribe(
        (personita) => this.per = personita,
        (error) => this.errorMsg = error as any,
        () => {
          console.log('registro nueva persona finalizado');
          this.showSavedPersonaSuccessfull = true;
        },
      );
  }

}
