import {ActivatedRoute, Params} from '@angular/router';
import {ApoderadoService} from '../../shared/services/apoderapp/apoderado.service';
import {Apoderado} from '../../shared/services/apoderapp/apoderado';
import {Component, LOCALE_ID, OnInit} from '@angular/core';
import {StudentDetail} from '../../shared/services/apoderapp/studentDetail';
import {StudentService as ApoderappStudentService} from '../../shared/services/apoderapp/student.service';

@Component({
  selector: 'apoderapp-student-detail',
  templateUrl: './apoderapp-student-detail.component.html',
  providers: [
    {provide: LOCALE_ID, useValue: 'es'},
    ApoderadoService,
    ApoderappStudentService,
  ],
})
export class ApoderappStudentDetailComponent implements OnInit {

  errorMsg: string;
  student: StudentDetail;
  isAddApoderado: boolean;
  isUpdating: boolean;
  apoderado: Apoderado;
  isChangeAccess: boolean;
  hasAccess: boolean;
  comment: string;
  isProcessing: boolean;
  showSavedSuccessfull: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apoderadoService: ApoderadoService,
    private apoderappStudentService: ApoderappStudentService
  ) {}

  ngOnInit() {
    this.getData();
    this.isAddApoderado = false;
    this.isUpdating = false;
    this.isChangeAccess = false;
    this.hasAccess = false;
    this.comment = undefined;
    this.isProcessing = false;
    this.showSavedSuccessfull = false;
  }

  /**
   * Información básica del estudiante
   */
  getData(): void {
    this.isChangeAccess = false;
    this.showSavedSuccessfull = false;
    this.isAddApoderado = false;
    this.activatedRoute.params.forEach((params: Params) => {
      const requestId: string = (params as any).request;
      this.getStudentDetail(requestId);
    });
  }

  /**
   * Detalle de Estudiante/Apoderado
   */
  getStudentDetail(studentId: string): void {
    console.log('Get Student Details...');
    this.isUpdating = true;
    this.apoderappStudentService.getStudentDetail(studentId)
      .subscribe(
        (student) => this.student = student,
        (error) => this.errorMsg = 'No se cargaron períodos de matrícula',
        () => this.isUpdating = false,
      );
    console.log('this.student => ', this.student);
    console.log('+--------------------------+');
  }

  /**
   * Detalle de Apoderado/Estudiante
   */
  getApoderadoDetail(apoderado: Apoderado): void {
    console.log('Get Apoderado Details...');
    this.isChangeAccess = true;
    this.isAddApoderado = false;
    this.showSavedSuccessfull = false;
    this.apoderado = apoderado;
    this.comment = undefined;
    this.hasAccess = this.apoderado.has_access;
    console.log('this.apoderado => ', this.apoderado);
    console.log('+--------------------------+');
  }

  /**
   * Validación de Sustento de cambio de acceso del apoderado
   */
  validateComment(): void {
    this.comment = this.comment.trim();
    this.isValidComment();
  }

  /**
   * Validación de Sustento de cambio de acceso del apoderado
   * @returns {boolean}
   */
  isValidComment(): boolean {
    return !(this.comment == null || this.comment === undefined || this.comment.trim() === '');
  }

  /**
   * Cambia acceso de apoderado
   */
  updateApoderadoAccess(): void {
    console.log('Update Apoderado Access...');
    this.isProcessing = true;
    this.showSavedSuccessfull = false;
    this.apoderadoService.updateApoderadoAccess(this.student, this.apoderado, this.hasAccess, this.comment)
      .subscribe(
        (student) => this.student = student,
        (error) => this.errorMsg = error as any,
        () => {
          this.isChangeAccess = false;
          this.isProcessing = false;
          this.showSavedSuccessfull = true;
        },
      );
    console.log('this.apoderado => ', this.apoderado);
    console.log('+--------------------------+');
  }

  /**
   * Desactiva el bloque de modificacipon de accesos del apoderado
   */
  cancelUpdateApoderadoAccess(): void {
    this.isChangeAccess = false;
  }

  /**
   * Activa bloque de búsqueda para añadir nuevo apoderado
   */
  activateSearchPersonBox(): void {
    this.isAddApoderado = !this.isAddApoderado;
    this.isChangeAccess = false;
    this.showSavedSuccessfull = false;
  }

}
