import { Component, OnInit } from '@angular/core';

import { IncidentStatus } from '../../../shared/services/incident/incident-status/incident-status';
import { IncidentStatusService } from '../../../shared/services/incident/incident-status/incident-status.service';

@Component({
  selector: 'incident-incident-status',
  templateUrl: './incident-status.component.html',
  providers: [IncidentStatusService]
})
export class IncidentStatusComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  registerIncidentStatus: boolean;
  updateIncidentStatus: boolean;

  filter: any;

  incidentStatus: IncidentStatus;
  incidentStatusList: IncidentStatus[];

  constructor(private incidentStatusService: IncidentStatusService) {
    this.loading = true;
  }

  ngOnInit(): void {
    this.getIncidentStatusList();
  }

  /**
   * Show form for register a new incident status
   */
  registerMode(): void {
    this.registerIncidentStatus = true;
    this.updateIncidentStatus = false;
    this.incidentStatus = new IncidentStatus();
  }

  /**
   * Show form to update an incident status
   * @param incidentStatus
   */
  updateMode(incidentStatus: IncidentStatus): void {
    this.registerIncidentStatus = false;
    this.updateIncidentStatus = true;
    (incidentStatus as any).edit = true;
    this.incidentStatus = incidentStatus;
  }

  /**
   * Close form to update an incident status
   * @param incidentStatus
   */
  cancel(incidentStatus: IncidentStatus): void {
    (incidentStatus as any).edit = false;
    this.close();
  }

  /**
   * Close form to register a new incident status
   */
  close(): void {
    this.registerIncidentStatus = false;
    this.updateIncidentStatus = false;
    this.processing = false;
  }

  /**
   * Update boolean variable value
   * @param value
   */
  updateFlags(value: boolean): void {
    this.loading = value;
    this.processing = value;
    this.registerIncidentStatus = value;
    this.updateIncidentStatus = value;
  }

  /**
   * Get incident status list
   */
  getIncidentStatusList(): void {
    this.loading = true;
    this.incidentStatusService.getAllIncidentStatus()
      .subscribe(
        (result) => this.incidentStatusList = result,
        (error) => {
          console.log('error => ', error);
          this.updateFlags(false);
        },
        () => {
          this.updateFlags(false);
          for (const item of this.incidentStatusList) {
            (item as any).edit = false;
          }
        }
      );
  }

  /**
   * Save new incident status
   * @param incidentStatus
   */
  save(incidentStatus: IncidentStatus): void {
    this.processing = true;
    this.incidentStatusService.save(incidentStatus)
      .subscribe(
        (result) => console.log('guardado => ', result),
        (error) => {
          this.processing = false;
          this.registerIncidentStatus = false;
          this.updateIncidentStatus = false;
          console.log('error => ', error);
        },
        () => {
          (incidentStatus as any).edit = false;
          this.getIncidentStatusList();
        }
      );
  }

  /**
   * Update incident status
   * @param incidentStatus
   */
  update(incidentStatus: IncidentStatus): void {
    this.processing = true;
    this.incidentStatusService.update(incidentStatus)
      .subscribe(
        (result) => console.log('actualizado => ', result),
        (error) => {
          this.processing = false;
          console.log('error => ', error);
        },
        () => {
          (incidentStatus as any).edit = false;
          this.getIncidentStatusList();
        }
      );
  }
}
