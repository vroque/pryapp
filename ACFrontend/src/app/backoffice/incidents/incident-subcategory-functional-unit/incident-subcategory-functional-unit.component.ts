import { Component, OnInit } from '@angular/core';

import { FunctionalUnit } from '../../../shared/services/adryan/functional-unit/functional-unit';
import { FunctionalUnitService } from '../../../shared/services/adryan/functional-unit/functional-unit.service';
import { IncidentSubcategory } from '../../../shared/services/incident/incident-subcategory/incident-subcategory';
// tslint:disable-next-line:max-line-length
import { IncidentSubcategoryFunctionalUnitService } from '../../../shared/services/incident/incident-subcategory-functional-unit/incident-subcategory-functional-unit.service';
import { IncidentSubcategoryService } from '../../../shared/services/incident/incident-subcategory/incident-subcategory.service';
import { IncidentCategoryService } from '../../../shared/services/incident/incident-category/incident-category.service';
import { IncidentCategory } from '../../../shared/services/incident/incident-category/incident-category';
import { IncidentSubcategoryFunctionalUnit } from '../../../shared/services/incident/incident-subcategory-functional-unit/incident-subcategory-functional-unit';

@Component({
  selector: 'incident-incident-subcategory-functional-unit',
  templateUrl: './incident-subcategory-functional-unit.component.html',
  providers: [
    FunctionalUnitService, IncidentCategoryService, IncidentSubcategoryService, IncidentSubcategoryFunctionalUnitService
  ]
})
export class IncidentSubcategoryFunctionalUnitComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  searching: boolean;
  principalActive: number;
  categoryActive: number;
  subcategoryActive: number;

  category: IncidentCategory;
  categories: IncidentCategory[] = [];
  childrenCategories: IncidentCategory[] = [];
  subcategory: IncidentSubcategory;
  subcategories: IncidentSubcategory[] = [];

  showAddTeamForm = false;

  teams: IncidentSubcategoryFunctionalUnit[] = [];
  functionalUnits: FunctionalUnit[];
  teamName: string;

  constructor(private incidentCategoriesService: IncidentCategoryService,
              private incidentSubcategoryService: IncidentSubcategoryService,
              private functionalUnitService: FunctionalUnitService,
              private incidentSubcategoryFunctionalUnitService: IncidentSubcategoryFunctionalUnitService) {
  }

  ngOnInit(): void {
    this.getCategories();
  }

  /**
   * Get all categories
   */
  getCategories(): void {
    this.loading = true;
    this.principalActive = undefined;
    this.incidentCategoriesService.getParentCategories()
      .subscribe(
        (result) => this.categories = result,
        (error) => {
          this.loading = false;
          console.log('error => ', error);
        },
        () => {
          this.loading = false;
          for (const item of this.categories) {
            (item as any).selected = false;
          }
        }
      );
  }

  /**
   * Get children categories by category id
   * @param categoryId
   */
  getChildrenCategories(categoryId): void {
    this.childrenCategories = [];
    this.categoryActive = undefined;
    this.subcategories = [];
    this.subcategoryActive = undefined;
    this.subcategory = undefined;
    this.principalActive = categoryId;
    this.incidentCategoriesService.getChildrenCategoryByCategoryId(categoryId)
      .subscribe(
        (result) => this.childrenCategories = result,
        (error) => console.error('error => ', error)
      );
  }

  /**
   * Get all subcategories by category id
   */
  getSubcategories(categoryId: number): void {
    this.subcategories = [];
    this.subcategoryActive = undefined;
    this.subcategory = undefined;
    this.categoryActive = categoryId;
    this.incidentSubcategoryService.getAllIncidentSubcategoriesByCategoryId(categoryId)
      .subscribe(
        (result) => this.subcategories = result,
        (error) => {
          console.error('error => ', error);
        },
        () => {
        }
      );
  }

  /**
   * Get all functional units by subcategory
   * @param subcategory
   */
  getTeams(subcategory: IncidentSubcategory): void {
    this.subcategory = undefined;
    this.subcategoryActive = subcategory.id;
    this.subcategory = subcategory;
    this.incidentSubcategoryFunctionalUnitService.getAllBySubcategoryId(subcategory.id)
      .subscribe(
        (result) => this.teams = result,
        (error) => console.log('error => ', error)
      );
  }

  /**
   * view add team form
   */
  viewAddTeamForm(): void {
    this.showAddTeamForm = !this.showAddTeamForm;
  }

  /**
   * Search functional units
   * @param searchTeam
   */
  searchTeams(searchTeam): void {
    this.functionalUnits = [];
    this.searching = true;
    this.functionalUnitService.search(searchTeam)
      .subscribe(
        (result) => this.functionalUnits = result,
        (error) => {
          console.log('error => ', error);
          this.searching = false;
        },
        () => this.searching = false
      );
  }

  /**
   * Add subcategory functional unit relationship
   * @param functionalUnit
   */
  addTeam(functionalUnit: FunctionalUnit): void {
    this.processing = true;
    const subcategoryFunctionalUnit: IncidentSubcategoryFunctionalUnit = new IncidentSubcategoryFunctionalUnit();
    subcategoryFunctionalUnit.functionalUnitId = functionalUnit.unidad_funcional;
    subcategoryFunctionalUnit.functionalUnitName = functionalUnit.nombre_unidad_funcional;
    subcategoryFunctionalUnit.subcategoryId = this.subcategory.id;
    subcategoryFunctionalUnit.subcategoryName = this.subcategory.name;
    this.incidentSubcategoryFunctionalUnitService.save(subcategoryFunctionalUnit)
      .subscribe(
        (result) => console.log('saved => ', result),
        (error) => {
          this.processing = false;
          console.log('error => ', error);
        },
        () => {
          this.processing = false;
          this.getTeams(this.subcategory);
        }
      );
  }

  /**
   * Delete subcategory functional unit relationship
   * @param incidentSubcategoryFunctionalUnit
   */
  delete(incidentSubcategoryFunctionalUnit: IncidentSubcategoryFunctionalUnit): void {
    const msg = `¿Estás seguro de eliminar el equipo "${incidentSubcategoryFunctionalUnit.functionalUnitName}"?`;
    if (window.confirm(msg)) {
      this.processing = true;
      this.incidentSubcategoryFunctionalUnitService.delete(incidentSubcategoryFunctionalUnit.id)
        .subscribe(
          (result) => console.log('deleted => ', incidentSubcategoryFunctionalUnit),
          (error) => {
            console.log('error => ', error);
            this.processing = false;
          },
          () => {
            this.processing = false;
            this.getTeams(this.subcategory);
          }
        );
    }
  }
}
