import { Component, OnInit } from '@angular/core';

import { IncidentLogType } from '../../../shared/services/incident/incident-log-type/incident-log-type';
import { IncidentLogTypeService } from '../../../shared/services/incident/incident-log-type/incident-log-type.service';

@Component({
  selector: 'incident-incident-log-type',
  templateUrl: './incident-log-type.component.html',
  providers: [IncidentLogTypeService]
})
export class IncidentLogTypeComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  registerIncidentLogType: boolean;
  updateIncidentLogType: boolean;

  filter: any;

  incidentLogType: IncidentLogType;
  incidentLogTypes: IncidentLogType[];

  constructor(private incidentLogTypeService: IncidentLogTypeService) {
    this.loading = true;
  }

  ngOnInit(): void {
    this.getIncidentLogTypes();
  }

  /**
   * Show form to register a new incident log type
   */
  registerMode(): void {
    this.registerIncidentLogType = true;
    this.updateIncidentLogType = false;
    this.incidentLogType = new IncidentLogType();
  }

  /**
   * Show form to update an incident log type
   * @param incidentLogType
   */
  updateMode(incidentLogType: IncidentLogType): void {
    this.registerIncidentLogType = false;
    this.updateIncidentLogType = true;
    (incidentLogType as any).edit = true;
    this.incidentLogType = incidentLogType;
  }

  /**
   * Close form to update an incident log type
   * @param incidentLogType
   */
  cancel(incidentLogType: IncidentLogType): void {
    (incidentLogType as any).edit = false;
    this.close();
  }

  /**
   * Close form to register new incident log type
   */
  close(): void {
    this.registerIncidentLogType = false;
    this.updateIncidentLogType = false;
    this.processing = false;
  }

  /**
   * Update value of boolean variables
   * @param value
   */
  updateFlags(value: boolean): void {
    this.loading = value;
    this.processing = value;
    this.registerIncidentLogType = value;
    this.updateIncidentLogType = value;
  }

  /**
   * Get incident log type list
   */
  getIncidentLogTypes(): void {
    this.loading = true;
    this.incidentLogTypeService.getAllIncidentLogType()
      .subscribe(
        (result) => this.incidentLogTypes = result,
        (error) => {
          console.log('error => ', error);
          this.updateFlags(false);
        },
        () => {
          this.updateFlags(false);
          for (const item of this.incidentLogTypes) {
            (item as any).edit = false;
          }
        }
      );
  }

  /**
   * Save new incident log type
   * @param incidentLogType
   */
  save(incidentLogType: IncidentLogType): void {
    this.processing = true;
    this.incidentLogTypeService.save(incidentLogType)
      .subscribe(
        (result) => console.log('saved => ', result),
        (error) => {
          this.updateFlags(false);
          console.log('error => ', error);
        },
        () => {
          (incidentLogType as any).edit = false;
          this.getIncidentLogTypes();
        }
      );
  }

  /**
   * Update incident log type
   * @param incidentLogType
   */
  update(incidentLogType: IncidentLogType): void {
    this.processing = true;
    this.incidentLogTypeService.update(incidentLogType)
      .subscribe(
        (result) => console.log('updated => ', result),
        (error) => {
          this.processing = false;
          console.log('error => ', error);
        },
        () => {
          (incidentLogType as any).edit = false;
          this.getIncidentLogTypes();
        }
      );
  }
}
