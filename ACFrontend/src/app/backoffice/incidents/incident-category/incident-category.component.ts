import { Component, OnInit } from '@angular/core';

import { IncidentCategory } from '../../../shared/services/incident/incident-category/incident-category';
import { IncidentCategoryService } from '../../../shared/services/incident/incident-category/incident-category.service';

@Component({
  selector: 'incident-incident-category',
  templateUrl: './incident-category.component.html',
  providers: [IncidentCategoryService]
})
export class IncidentCategoryComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  registerPrincipalCategory: boolean;
  registerSecondaryCategory: boolean;
  updatePrincipalCategory: boolean;
  updateSecondaryCategory: boolean;

  filter: any;

  incidentCategory: IncidentCategory;
  principalCategories: IncidentCategory[] = [];
  secondaryCategories: IncidentCategory[] = [];

  category: IncidentCategory;

  constructor(private incidentCategoryService: IncidentCategoryService) {
    this.loading = true;
  }

  ngOnInit(): void {
    this.getPrincipalCategories();
  }

  /**
   * Show form to register a new category.
   * 0 => principal category (without parent category)
   * 1 => secondary category (with a parent category)
   * @param id
   */
  registerMode(id: number): void {
    if (id === 0) {
      this.registerPrincipalCategory = true;
    } else if (id === 1) {
      this.registerSecondaryCategory = true;
    } else {
      this.registerPrincipalCategory = false;
      this.registerSecondaryCategory = false;
    }
    this.updatePrincipalCategory = false;
    this.updateSecondaryCategory = false;
    this.incidentCategory = new IncidentCategory();
  }

  /**
   * Show form to update an incident category.
   * 0 => principal category (without parent category)
   * 1 => secondary category (with a parent category)
   * @param id
   */
  updateMode(id: number, incidentCategory: IncidentCategory): void {
    this.registerPrincipalCategory = false;
    this.registerSecondaryCategory = false;
    if (id === 0) {
      this.updatePrincipalCategory = true;
    } else if (id === 1) {
      this.updateSecondaryCategory = true;
    } else {
      this.updatePrincipalCategory = false;
      this.updateSecondaryCategory = false;
    }
    (incidentCategory as any).edit = true;
    this.incidentCategory = incidentCategory;
  }

  /**
   * Close form to update an incident category
   */
  cancel(incidentCategory: IncidentCategory): void {
    (this.incidentCategory as any).edit = false;
    this.close();
  }

  /**
   * Close form to register a new incident category
   */
  close(): void {
    this.registerPrincipalCategory = false;
    this.registerSecondaryCategory = false;
    this.updatePrincipalCategory = false;
    this.updateSecondaryCategory = false;
  }

  /**
   * Update value of boolean variables
   * @param value
   */
  updateFlags(value: boolean): void {
    this.loading = value;
    this.processing = value;
    this.registerPrincipalCategory = value;
    this.registerSecondaryCategory = value;
    this.updatePrincipalCategory = value;
    this.updateSecondaryCategory = value;
  }

  /**
   * Get principal categories. These categories doesn't have parent category.
   */
  getPrincipalCategories(): void {
    this.loading = true;
    this.incidentCategoryService.getParentCategories()
      .subscribe(
        (result) => this.principalCategories = result,
        (error) => {
          console.log('error => ', error);
          this.updateFlags(false);
        },
        () => {
          this.updateFlags(false);
          for (const item of this.principalCategories) {
            (item as any).edit = false;
          }
          this.getSecondaryCategories();
        }
      );
  }

  /**
   * Get secondary categories. These categories have parent category.
   */
  getSecondaryCategories(): void {
    this.loading = true;
    this.incidentCategoryService.getChildrenCategories()
      .subscribe(
        (result) => this.secondaryCategories = result,
        (error) => {
          console.log('error => ', error);
          this.updateFlags(false);
        },
        () => {
          this.updateFlags(false);
          for (const item of this.secondaryCategories) {
            (item as any).edit = false;
          }
        }
      );
  }

  /**
   * Get parent category information
   * @param parentCategoryId
   */
  getParentCategoryInfo(parentCategoryId: number): IncidentCategory {
    for (const item of this.principalCategories) {
      if (item.id === parentCategoryId) {
        return item;
      }
    }
  }

  /**
   * Save new incident category
   * @param incidentCategory
   */
  save(incidentCategory: IncidentCategory): void {
    this.processing = true;
    this.incidentCategoryService.save(incidentCategory)
      .subscribe(
        (result) => console.log('saved => ', result),
        (error) => {
          console.log('error => ', error);
          this.updateFlags(false);
        },
        () => {
          (incidentCategory as any).edit = false;
          this.getPrincipalCategories();
        }
      );
  }

  /**
   * Update incident category
   * @param incidentCategory
   */
  update(incidentCategory: IncidentCategory): void {
    this.processing = true;
    this.incidentCategoryService.update(incidentCategory)
      .subscribe(
        (result) => console.log('updated => ', result),
        (error) => {
          console.log('error => ', error);
          this.processing = false;
        },
        () => {
          (incidentCategory as any).edit = false;
          this.getPrincipalCategories();
        }
      );
  }
}
