import { Component, OnInit } from '@angular/core';

import { IncidentType } from '../../../shared/services/incident/incident-type/incident-type';
import { IncidentTypeService } from '../../../shared/services/incident/incident-type/incident-type.service';

@Component({
  selector: 'incident-incident-type',
  templateUrl: './incident-type.component.html',
  providers: [IncidentTypeService]
})
export class IncidentTypeComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  registerIncidentType: boolean;
  updateIncidentType: boolean;

  filter: any;

  incidentType: IncidentType;
  incidentTypeList: IncidentType[];

  constructor(private incidentTypeService: IncidentTypeService) {
    this.loading = true;
  }

  ngOnInit(): void {
    this.getIncidentTypeList();
  }

  /**
   * Show form to register a new incident type
   */
  registerMode(): void {
    this.registerIncidentType = true;
    this.updateIncidentType = false;
    this.incidentType = new IncidentType();
  }

  /**
   * Show form to update an incident type
   * @param incidentType
   */
  updateMode(incidentType: IncidentType): void {
    this.registerIncidentType = false;
    this.updateIncidentType = true;
    (incidentType as any).edit = true;
    this.incidentType = incidentType;
  }

  /**
   * Close form to update an incident type
   * @param incidentType
   */
  cancel(incidentType: IncidentType): void {
    (incidentType as any).edit = false;
    this.close();
  }

  /**
   * Close form to register new incident type
   */
  close(): void {
    this.registerIncidentType = false;
    this.updateIncidentType = false;
    this.processing = false;
  }

  /**
   * Update value of boolean variables
   * @param value
   */
  updateFlags(value: boolean): void {
    this.loading = value;
    this.processing = value;
    this.registerIncidentType = value;
    this.updateIncidentType = value;
  }

  /**
   * Get incident type list
   */
  getIncidentTypeList(): void {
    this.loading = true;
    this.incidentTypeService.getAllIncidentType()
      .subscribe(
        (result) => this.incidentTypeList = result,
        (error) => {
          console.log('error => ', error);
          this.updateFlags(false);
        },
        () => {
          this.updateFlags(false);
          for (const item of this.incidentTypeList) {
            (item as any).edit = false;
          }
        }
      );
  }

  /**
   * Save new incident type
   * @param incidentType
   */
  save(incidentType: IncidentType): void {
    this.processing = true;
    this.incidentTypeService.save(incidentType)
      .subscribe(
        (result) => console.log('guardado => ', result),
        (error) => {
          this.processing = false;
          this.registerIncidentType = false;
          this.updateIncidentType = false;
          console.log('error => ', error);
        },
        () => {
          (incidentType as any).edit = false;
          this.getIncidentTypeList();
        }
      );
  }

  /**
   * Update incident type
   * @param incidentType
   */
  update(incidentType: IncidentType): void {
    this.processing = true;
    this.incidentTypeService.update(incidentType)
      .subscribe(
        (result) => console.log('actualizado => ', result),
        (error) => {
          this.processing = false;
          console.log('error => ', error);
        },
        () => {
          (incidentType as any).edit = false;
          this.getIncidentTypeList();
        }
      );
  }
}
