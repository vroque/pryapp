import { Component, OnInit } from '@angular/core';

import { IncidentCategory } from '../../../shared/services/incident/incident-category/incident-category';
import { IncidentCategoryService } from '../../../shared/services/incident/incident-category/incident-category.service';
import { IncidentSubcategory } from '../../../shared/services/incident/incident-subcategory/incident-subcategory';
import { IncidentSubcategoryService } from '../../../shared/services/incident/incident-subcategory/incident-subcategory.service';

@Component({
  selector: 'incident-incident-subcategory',
  templateUrl: './incident-subcategory.component.html',
  providers: [IncidentSubcategoryService, IncidentCategoryService]
})
export class IncidentSubcategoryComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  registerSubcategory: boolean;
  updateSubcategory: boolean;

  filter: any;

  secondaryCategories: IncidentCategory[];
  incidentSubcategory: IncidentSubcategory;
  incidentSubcategories: IncidentSubcategory[];

  constructor(private incidentSubcategoryService: IncidentSubcategoryService,
              private incidentCategoryService: IncidentCategoryService) {
    this.loading = true;
  }

  ngOnInit(): void {
    this.getSecondaryCategories();
  }

  /**
   * Show form to register a new incident subcategory
   */
  registerMode(): void {
    this.registerSubcategory = true;
    this.updateSubcategory = false;
    this.incidentSubcategory = new IncidentSubcategory();
  }

  /**
   * Show form to update an incident subcategory
   * @param incidentSubcategory
   */
  updateMode(incidentSubcategory: IncidentSubcategory): void {
    this.registerSubcategory = false;
    this.updateSubcategory = true;
    (incidentSubcategory as any).edit = true;
    this.incidentSubcategory = incidentSubcategory;
  }

  /**
   * Close form to update an incident subcategory
   * @param incidentSubcategory
   */
  cancel(incidentSubcategory: IncidentSubcategory): void {
    (incidentSubcategory as any).edit = false;
    this.close();
  }

  /**
   * Close form to register new incident subcategory
   */
  close(): void {
    this.registerSubcategory = false;
    this.updateSubcategory = false;
    this.processing = false;
  }

  /**
   * Update value of boolean variables
   * @param value
   */
  updateFlags(value: boolean): void {
    this.loading = value;
    this.processing = value;
    this.registerSubcategory = value;
    this.updateSubcategory = value;
  }

  /**
   * Get secondary categories. These categories have parent category.
   */
  getSecondaryCategories(): void {
    this.loading = true;
    this.incidentCategoryService.getChildrenCategories()
      .subscribe(
        (result) => this.secondaryCategories = result,
        (error) => {
          console.log('error => ', error);
          this.updateFlags(false);
        },
        () => {
          this.updateFlags(false);
          for (const item of this.secondaryCategories) {
            (item as any).edit = false;
          }
          this.getIncidentSubcategories();
        }
      );
  }

  /**
   * Get all incident subcategories
   */
  getIncidentSubcategories(): void {
    this.loading = true;
    this.incidentSubcategoryService.getAllIncidentSubcategories()
      .subscribe(
        (result) => this.incidentSubcategories = result,
        (error) => {
          console.log('error => ', error);
          this.updateFlags(false);
        },
        () => {
          this.updateFlags(false);
          for (const item of this.incidentSubcategories) {
            (item as any).edit = false;
          }
        }
      );
  }

  /**
   * Get parent category information
   * @param parentCategoryId
   */
  getParentCategoryInfo(parentCategoryId: number): IncidentCategory {
    for (const item of this.secondaryCategories) {
      if (item.id === parentCategoryId) {
        return item;
      }
    }
  }

  /**
   * Save new incident subcategory
   * @param incidentSubcategory
   */
  save(incidentSubcategory: IncidentSubcategory): void {
    this.processing = true;
    this.incidentSubcategoryService.save(incidentSubcategory)
      .subscribe(
        (result) => console.log('saved => ', result),
        (error) => {
          console.log('error => ', error);
          this.updateFlags(false);
        },
        () => {
          (incidentSubcategory as any).edit = false;
          this.getIncidentSubcategories();
        }
      );
  }

  /**
   * Update incident subcategory
   * @param incidentSubcategory
   */
  update(incidentSubcategory: IncidentSubcategory): void {
    this.processing = true;
    this.incidentSubcategoryService.update(incidentSubcategory)
      .subscribe(
        (result) => console.log('updated => ', result),
        (error) => {
          this.processing = false;
          console.log('error => ', error);
        },
        () => {
          (incidentSubcategory as any).edit = false;
          this.getIncidentSubcategories();
        }
      );
  }
}
