import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Employee } from '../../shared/services/employee/employee';
import { EmployeeService } from '../../shared/services/employee/employee.service';
import { Incident } from '../../shared/services/incident/incident';
import { IncidentService } from '../../shared/services/incident/incident.service';
import { IncidentType } from '../../shared/services/incident/incident-type/incident-type';
import { IncidentTypeService } from '../../shared/services/incident/incident-type/incident-type.service';
import { IncidentRequestSource } from '../../shared/services/incident/incident-request-source/incident-request-source';
import { IncidentRequestSourceService } from '../../shared/services/incident/incident-request-source/incident-request-source.service';
import { IncidentStatus } from '../../shared/services/incident/incident-status/incident-status';
import { IncidentStatusService } from '../../shared/services/incident/incident-status/incident-status.service';
import { MomentService } from '../../shared/moment.service';
import { SearchIncidentService } from '../../shared/services/incident/search-incident.service';
import { Student } from '../../shared/services/academic/student';
import { StudentService } from '../../shared/services/academic/student.service';
import { UtilService } from '../../shared/util.service';

@Component({
  selector: 'incidents',
  templateUrl: './incidents.component.html',
  providers: [
    EmployeeService, SearchIncidentService, IncidentStatusService, IncidentTypeService,
    MomentService, UtilService, IncidentRequestSourceService, StudentService, IncidentService
  ]
})
export class IncidentsComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  searching: boolean;
  viewNewIncidentForm: boolean;
  viewForms: boolean;
  cas: boolean;

  ucOpenData: Incident[];
  ucInProgressData: Incident[];
  ucSolvedData: Incident[];
  ucRejectedData: Incident[];
  ucCancelledData: Incident[];
  ucClosedData: Incident[];

  myIncidents: Incident[];
  personalOpenData: Incident[];
  personalInProgressData: Incident[];
  personalSolvedData: Incident[];
  personalRejectedData: Incident[];
  personalCancelledData: Incident[];
  personalClosedData: Incident[];

  myTeamIncidents: Incident[];
  teamOpenData: Incident[];
  teamInProgressData: Incident[];
  teamSolvedData: Incident[];
  teamRejectedData: Incident[];
  teamCancelledData: Incident[];
  teamClosedData: Incident[];

  incidentActiveView: Incident[];

  newIncident: Incident = new Incident();
  msgError: string;

  incidentRequestSources: IncidentRequestSource[] = [];

  inbox: string;

  incidentTypes: IncidentType[];
  incidentStatuses: IncidentStatus[];

  employeeProfiles: Employee[];

  studentId: number;
  student: Student;

  constructor(private employeeService: EmployeeService, private searchIncidentService: SearchIncidentService,
              private incidentStatusService: IncidentStatusService, private incidentTypeService: IncidentTypeService,
              private moment: MomentService, private location: Location, private router: Router,
              private utilService: UtilService, private incidentRequestSourceService: IncidentRequestSourceService,
              private studentService: StudentService, private incidentService: IncidentService) {
    this.inbox = 'personalInProgress';
  }

  ngOnInit(): void {
    this.getIncidentStatuses();
    this.getIncidentRequestSource();
  }

  setInbox(type: string): void {
    this.inbox = type;
    switch (type) {
      case ('ucOpen'):
        this.incidentActiveView = this.ucOpenData;
        break;
      case ('ucInProgress'):
        this.incidentActiveView = this.ucInProgressData;
        break;
      case ('ucSolved'):
        this.incidentActiveView = this.ucSolvedData;
        break;
      case ('ucRejected'):
        this.incidentActiveView = this.ucRejectedData;
        break;
      case ('ucCancelled'):
        this.incidentActiveView = this.ucCancelledData;
        break;
      case ('ucClosed'):
        this.incidentActiveView = this.ucClosedData;
        break;
      case ('personalOpen'):
        this.incidentActiveView = this.personalOpenData;
        break;
      case ('personalInProgress'):
        this.incidentActiveView = this.personalInProgressData;
        break;
      case ('personalSolved'):
        this.incidentActiveView = this.personalSolvedData;
        break;
      case ('personalRejected'):
        this.incidentActiveView = this.personalRejectedData;
        break;
      case ('personalCancelled'):
        this.incidentActiveView = this.personalCancelledData;
        break;
      case ('personalClosed'):
        this.incidentActiveView = this.personalClosedData;
        break;
      case ('teamOpen'):
        this.incidentActiveView = this.teamOpenData;
        break;
      case ('teamInProgress'):
        this.incidentActiveView = this.teamInProgressData;
        break;
      case ('teamSolvedData'):
        this.incidentActiveView = this.teamSolvedData;
        break;
      case ('teamRejected'):
        this.incidentActiveView = this.teamRejectedData;
        break;
      case ('teamCancelledData'):
        this.incidentActiveView = this.teamCancelledData;
        break;
      case ('teamClosedData'):
        this.incidentActiveView = this.teamClosedData;
        break;
      case ('search'):
        break;
    }
  }

  /**
   * Get all incident statuses
   */
  getIncidentStatuses(): void {
    this.loading = true;
    this.incidentStatusService.getAllIncidentStatus()
      .subscribe(
        (result) => this.incidentStatuses = result,
        (error) => {
          console.error('error => ', error);
          this.loading = false;
        },
        () => {
          console.log('incident statuses => ', this.incidentStatuses);
          this.getIncidentTypes();
        }
      );
  }

  /**
   * Get incident status name
   * @param id Incident status id
   */
  getIncidentStatusName(id: number): string {
    for (const item of this.incidentStatuses) {
      if (item.id === id) {
        return item.name;
      }
    }
  }

  /**
   * Get all incident types
   */
  getIncidentTypes(): void {
    this.incidentTypeService.getAllIncidentType()
      .subscribe(
        (result) => this.incidentTypes = result,
        (error) => console.error('error => ', error),
        () => {
          console.log('incident types => ', this.incidentTypes);
          this.getEmployeProfiles();
        }
      );
  }

  /**
   * Get incident type name
   * @param id Incident type id
   */
  getIncidentTypeName(id: number): string {
    for (const item of this.incidentTypes) {
      if (item.id === id) {
        return item.name;
      }
    }
  }

  /**
   * Get employee profiles
   */
  getEmployeProfiles(): void {
    this.loading = true;
    this.employeeService.getAllProfiles()
      .subscribe(
        (result) => this.employeeProfiles = result,
        (error) => console.error('error => ', error),
        () => {
          console.log('profiles => ', this.employeeProfiles);
          this.getMyIncidents();
          this.getMyTeamIncidents();
          if (this.employeeProfiles.filter((item) => item.unidad_funcional_organica === '00000590').length > 0) {
            this.cas = true;
            this.getOpenIncidents();
          } else {
            this.cas = false;
          }
        }
      );
  }

  /**
   * Get open UC incidents without a team assigned.
   */
  getOpenIncidents(): void {
    this.searchIncidentService.find(0, 0, 0, 0, 0, 0, 'none')
      .subscribe(
        (result) => this.ucOpenData = result,
        (error) => console.error('error => ', error),
        () => {
          console.log('open but unassigned => ', this.ucOpenData);
          this.getIncidentsDifferentOfOpen();
        }
      );
  }

  /**
   * Get incidents assigned to other teams
   */
  getIncidentsDifferentOfOpen(): void {
    if (this.employeeProfiles.length > 0) {
      this.searchIncidentService.find(0, 0, 0, 0, 1, 0, 'other', this.employeeProfiles[0].employee_pidm)
        .subscribe(
          (result) => this.ucInProgressData = result,
          (error) => console.error('error => ', error),
          () => {
            console.log('other uc incidents different of open => ', this.ucInProgressData);
            this.ucInProgressData = this.ucInProgressData.filter((item) => item.assignedToTeamId !== '00000000');
            this.searchIncidentService.find(0, 0, 0, 0, 2, 0, 'other', this.employeeProfiles[0].employee_pidm)
              .subscribe(
                (result) => this.ucInProgressData = this.ucInProgressData.concat(result),
                (error) => console.log('error => ', error),
                () => {
                  console.log('other uc incidents different of open => ', this.ucInProgressData);
                  this.ucInProgressData = this.ucInProgressData.filter((item) => item.assignedToTeamId !== '00000000');
                }
              );
          }
        );
    }
  }

  /**
   * Get all my incidents
   */
  getMyIncidents(): void {
    if (this.employeeProfiles.length > 0) {
      this.loading = true;
      this.searchIncidentService.find(0, 0, 0, 0, 0, this.employeeProfiles[0].employee_pidm, 'all')
        .subscribe(
          (result) => this.myIncidents = result,
          (error) => {
            console.error('error => ', error);
            this.loading = false;
          },
          () => {
            console.log('my incidents => ', this.myIncidents);
            this.personalOpenData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.OPEN);
            this.personalInProgressData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.IN_PROGRESS);
            this.personalSolvedData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.SOLVED);
            this.personalRejectedData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.REJECTED);
            this.personalCancelledData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.CANCELLED);
            this.personalClosedData = this.myIncidents.filter((item) => item.statusId === IncidentStatus.CLOSED);
            this.incidentActiveView = this.personalInProgressData;
            this.loading = false;
          }
        );
    }
  }

  /**
   * Get all my team incidents
   */
  getMyTeamIncidents(): void {
    if (this.employeeProfiles.length > 0) {
      this.searchIncidentService.find(0, 0, 0, 0, 0, 0, this.employeeProfiles[0].unidad_funcional_organica)
        .subscribe(
          (result) => this.myTeamIncidents = result,
          (error) => console.error('error => ', error),
          () => {
            console.log('team incidents => ', this.myTeamIncidents);
            this.teamOpenData = this.myTeamIncidents.filter((item) => item.statusId === IncidentStatus.OPEN);
            this.teamInProgressData = this.myTeamIncidents.filter((item) => item.statusId === IncidentStatus.IN_PROGRESS);
            this.teamSolvedData = this.myTeamIncidents.filter((item) => item.statusId === IncidentStatus.SOLVED);
            this.teamRejectedData = this.myTeamIncidents.filter((item) => item.statusId === IncidentStatus.REJECTED);
            this.teamCancelledData = this.myTeamIncidents.filter((item) => item.statusId === IncidentStatus.CANCELLED);
            this.teamClosedData = this.myTeamIncidents.filter((item) => item.statusId === IncidentStatus.CLOSED);
          }
        );
    }
  }

  /**
   * Get all incident request sources
   */
  getIncidentRequestSource(): void {
    this.incidentRequestSourceService.getAll()
      .subscribe(
        (result) => this.incidentRequestSources = result,
        (error) => console.error('error => ', error)
      );
  }

  /**
   * Active form to register new incident
   */
  registerMode(): void {
    this.viewForms = true;
  }

  /**
   * Search student by id
   * @param studentId
   */
  searchStudent(studentId: string): void {
    this.student = undefined;
    this.searching = true;
    this.studentService.find(studentId.trim(), 'student_id')
      .subscribe(
        (result) => this.student = result,
        (error) => {
          console.error('error => ', error);
          this.searching = false;
        },
        () => this.searching = false
      );
  }

  /**
   * Show incident form
   * @param student
   */
  showNewIncidentForm(student: Student): void {
    this.viewNewIncidentForm = true;
    this.newIncident.reporterPidm = student.person_id;
    this.newIncident.reporterName = `${student.last_name.toUpperCase()} ${this.student.first_name.toLocaleUpperCase()}`;
  }

  /**
   * Save new incident
   */
  save(): void {
    this.newIncident.assignedToTeamId = '00000000';
    this.incidentService.save(this.newIncident)
      .subscribe(
        (result) => this.newIncident = result,
        (error) => {
          this.processing = false;
          console.error('error => ', error);
        },
        () => {
          this.processing = false;
          this.gotoIncidentDetail(this.newIncident.id);
        }
      );
  }

  /**
   * Cancel register new incident
   */
  cancel(): void {
    this.viewForms = false;
    this.viewNewIncidentForm = false;
    this.student = undefined;
    this.newIncident = new Incident();
  }

  /**
   * Tiempo relativo a partir de una fecha tipo YYYYMMDD
   * p.e.: momento("20101225") retorna un string: "hace X años"
   * @param {string} date
   * @returns {string}
   */
  momento(date: string): string {
    return this.moment.fromNow(date);
  }

  /**
   * Go to incident detail
   * @param incidentId
   */
  gotoIncidentDetail(incidentId: number): void {
    const pathName: string[] = this.location.path().split('/');
    if (pathName.length >= 3) {
      const link: string[] = [`${pathName[1]}/${pathName[2]}/${incidentId}`];
      this.router.navigate(link);
    } else {
      console.log('pathName: ', pathName);
    }
  }
}
