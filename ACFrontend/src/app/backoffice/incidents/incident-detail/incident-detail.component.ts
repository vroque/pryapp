import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Employee } from '../../../shared/services/employee/employee';
import { EmployeeService } from '../../../shared/services/employee/employee.service';
import { FunctionalUnit } from '../../../shared/services/adryan/functional-unit/functional-unit';
import { FunctionalUnitService } from '../../../shared/services/adryan/functional-unit/functional-unit.service';
import { Incident } from '../../../shared/services/incident/incident';
import { IncidentCategory } from '../../../shared/services/incident/incident-category/incident-category';
import { IncidentCategoryService } from '../../../shared/services/incident/incident-category/incident-category.service';
import { IncidentLog } from '../../../shared/services/incident/incident-log/incident-log';
import { IncidentLogService } from '../../../shared/services/incident/incident-log/incident-log.service';
import { IncidentService } from '../../../shared/services/incident/incident.service';
import { IncidentStatus } from '../../../shared/services/incident/incident-status/incident-status';
import { IncidentStatusService } from '../../../shared/services/incident/incident-status/incident-status.service';
import { IncidentSubcategory } from '../../../shared/services/incident/incident-subcategory/incident-subcategory';
import { IncidentSubcategoryService } from '../../../shared/services/incident/incident-subcategory/incident-subcategory.service';
import { IncidentType } from '../../../shared/services/incident/incident-type/incident-type';
import { IncidentTypeService } from '../../../shared/services/incident/incident-type/incident-type.service';
import { MomentService } from '../../../shared/moment.service';

@Component({
  selector: 'incident-incident-detail',
  templateUrl: './incident-detail.component.html',
  providers: [
    EmployeeService, FunctionalUnitService, IncidentLogService, IncidentService, IncidentStatusService,
    IncidentTypeService, IncidentTypeService, IncidentCategoryService, IncidentSubcategoryService, MomentService
  ]
})
export class IncidentDetailComponent implements OnInit {
  loading: boolean;
  processing: boolean;
  edit: boolean;
  changeAssignedUser: boolean;
  changeAssignedTeam: boolean;
  changeCategory: boolean;
  changeSubcategory: boolean;

  incident: Incident;
  incidentStatus: IncidentStatus;
  incidentType: IncidentType;

  incidentLog: IncidentLog;
  incidentLogList: IncidentLog[] = [];
  lastActivity: Date;

  incidentParentCategory: IncidentCategory = new IncidentCategory();
  incidentParentCategories: IncidentCategory[] = [];
  incidentCategory: IncidentCategory;
  incidentCategories: IncidentCategory[] = [];
  incidentSubcategory: IncidentSubcategory;
  incidentSubcategories: IncidentSubcategory[] = [];

  myEmployeeProfiles: Employee[] = [];
  employee: Employee;
  employees: Employee[] = [];
  searchingEmployees: boolean;

  functionalUnit: FunctionalUnit;
  functionalUnits: FunctionalUnit[] = [];
  searchingFunctionalUnits: boolean;

  iUser: string; // input
  iTeam: string; // input

  incidentLogActivityTypes = IncidentLog.ACTIVITY_TYPE;

  constructor(private activatedRoute: ActivatedRoute, private incidentService: IncidentService,
              private moment: MomentService, private incidentStatusService: IncidentStatusService,
              private incidentTypeService: IncidentTypeService, private incidentLogService: IncidentLogService,
              private employeeService: EmployeeService, private incidentCategoryService: IncidentCategoryService,
              private incidentSubcategoryService: IncidentSubcategoryService,
              private functionalUnitService: FunctionalUnitService) {
    this.loading = true;
    this.incidentLog = new IncidentLog();
  }

  ngOnInit(): void {
    this.incident = null;
    this.getMyEmployeeProfile();
    this.activatedRoute.params.forEach((params: Params) => {
      const incidentId: number = (<any>params).incident;
      this.getIncident(incidentId);
    });
  }

  /**
   * Array of activity type from incident log
   */
  incidentLogKeys(): string[] {
    return Object.keys(IncidentLog.ACTIVITY_TYPE);
  }

  editMode(): void {
    this.edit = true;
    this.changeCategory = true;
    this.changeSubcategory = true;
    this.changeAssignedUser = true;
    this.changeAssignedTeam = true;
    this.employees = [];
  }

  /**
   * Cancel incident edit mode
   */
  cancel(): void {
    this.edit = false;
    this.changeCategory = false;
    this.changeSubcategory = false;
    this.changeAssignedUser = false;
    this.changeAssignedTeam = false;
  }

  /**
   * Update incident
   */
  update(): void {
    this.incidentService.update(this.incident)
      .subscribe(
        (result) => this.incident = result,
        (error) => console.log('error => ', error),
        () => {
          this.cancel();
          if (this.incident !== null) {
            this.refreshIncident(this.incident);
          }
        }
      );
  }

  /**
   * Assign me
   */
  assignMe(): void {
    this.incident.assignedToPidm = this.myEmployeeProfiles[0].employee_pidm;
    this.incident.assignedToTeamId = this.myEmployeeProfiles[0].unidad_funcional_organica;
    this.incidentService.update(this.incident)
      .subscribe(
        (result) => this.incident = result,
        (error) => console.error('error => ', error),
        () => {
          this.cancel();
          if (this.incident !== null) {
            this.refreshIncident(this.incident);
          }
        }
      );
  }

  /**
   * Change assigned
   * @param changeUser
   */
  changeAssigned(changeUser: boolean = false): void {
    if (changeUser) {
      this.changeAssignedUser = !this.changeAssignedUser;
    } else {
      this.changeAssignedTeam = !this.changeAssignedTeam;
    }
  }

  /**
   * Change category and subcategory
   * @param changeSubcategory
   */
  changeCategories(changeSubcategory: boolean = false): void {
    if (changeSubcategory) {
      this.changeSubcategory = !this.changeSubcategory;
    } else {
      this.changeCategory = !this.changeCategory;
      this.changeSubcategory = !this.changeSubcategory;
      this.incident.subCategoryId = undefined;
      this.getIncidentSubcategories(this.incident.categoryId);
    }
  }

  /**
   * Get incident info
   * @param incidentId
   */
  getIncident(incidentId: number) {
    this.loading = true;
    this.incidentService.getById(incidentId)
      .subscribe(
        (result) => this.incident = result,
        (error) => {
          console.log('error => ', error);
          this.loading = false;
        },
        () => {
          this.loading = false;

          if (this.incident !== null) {
            this.refreshIncident(this.incident);
          }

        }
      );
  }

  /**
   * Refresh incident info
   * @param incident
   */
  refreshIncident(incident: Incident): void {
    this.getIncidentLog(incident.id);
    this.getIncidentStatusById(incident.statusId);
    this.getIncidentTypeById(incident.typeId);
    this.getIncidentParentCategories();
    this.incidentLog.incidentId = incident.id;

    if (incident.assignedToPidm !== null) {
      this.getEmployee(incident.assignedToPidm);
    }

    if (incident.assignedToTeamId !== null) {
      this.getFunctionalUnit(incident.assignedToTeamId);
    }

    if (incident.categoryId !== null) {
      this.getIncidentCategory(incident.categoryId);
      this.getIncidentSubcategories(incident.categoryId);
    }

    if (incident.subCategoryId !== null) {
      this.getIncidentSubcategory(incident.subCategoryId);
    }
  }

  /**
   * Get incident log
   * @param incidentId
   */
  getIncidentLog(incidentId: number) {
    this.incidentLogService.getAllIncidentLogByIncidentId(incidentId)
      .subscribe(
        (result) => this.incidentLogList = result,
        (error) => console.log('error => ', error),
        () => {
          if (this.incidentLogList.length > 0) {
            this.lastActivity = this.incidentLogList[this.incidentLogList.length - 1].publicationDate;
          } else {
            this.lastActivity = this.incident.publicationDate;
          }
        }
      );
  }

  /**
   * Get incident status info
   * @param incidentStatusId
   */
  getIncidentStatusById(incidentStatusId: number): void {
    this.incidentStatusService.getById(incidentStatusId)
      .subscribe(
        (result) => this.incidentStatus = result,
        (error) => console.log('error => ', error)
      );
  }

  /**
   * Get incident type info
   * @param incidentTypeId
   */
  getIncidentTypeById(incidentTypeId: number): void {
    this.incidentTypeService.getById(incidentTypeId)
      .subscribe(
        (result) => this.incidentType = result,
        (error) => console.log('error => ', error)
      );
  }

  /**
   * get incident category information
   * @param categoryId
   */
  getIncidentCategory(categoryId: number): void {
    this.incidentCategoryService.getById(categoryId)
      .subscribe(
        (result) => this.incidentCategory = result,
        (error) => console.log('error => ', error),
        () => {
          if (this.incidentCategory.parentId !== null) {
            this.getIncidentParentCategory(this.incidentCategory.parentId);
            this.getIncidentCategories(this.incidentCategory.parentId);
          }
        }
      );
  }

  /**
   * Get incident parent category information
   * @param categoryId
   */
  getIncidentParentCategory(categoryId: number): void {
    this.incidentCategoryService.getById(categoryId)
      .subscribe(
        (result) => this.incidentParentCategory = result,
        (error) => console.log('error => ', error)
      );
  }

  /**
   * Get incident principal categories
   */
  getIncidentParentCategories(): void {
    this.incidentCategoryService.getParentCategories()
      .subscribe(
        (result) => this.incidentParentCategories = result,
        (error) => console.log('error => ', error)
      );
  }

  /**
   * Get incident categories
   */
  getIncidentCategories(parentCategoryId: number): void {
    this.incidentCategoryService.getChildrenCategoryByCategoryId(parentCategoryId)
      .subscribe(
        (result) => this.incidentCategories = result,
        (error) => console.log('error getting incident categories => ', error)
      );
  }

  /**
   * Get incident subcategories by category id
   * @param incidentCategoryId
   */
  getIncidentSubcategories(incidentCategoryId: number): void {
    this.incidentSubcategoryService.getAllIncidentSubcategoriesByCategoryId(incidentCategoryId)
      .subscribe(
        (result) => this.incidentSubcategories = result,
        (error) => console.log('error getting subcategories by category => ', error),
      );
  }

  /**
   * get incident subcategory information
   * @param subcategoryId
   */
  getIncidentSubcategory(subcategoryId: number): void {
    this.incidentSubcategoryService.getById(subcategoryId)
      .subscribe(
        (result) => this.incidentSubcategory = result
      );
  }

  /**
   * Get my employee profiles
   */
  getMyEmployeeProfile(): void {
    this.employeeService.getAllProfiles()
      .subscribe(
        (result) => this.myEmployeeProfiles = result,
        (error) => console.error('error on getting my profiles => ', error)
      );
  }

  /**
   * Get employee information
   * @param pidm
   */
  getEmployee(pidm: number): void {
    this.employeeService.getByPidm(pidm)
      .subscribe(
        (result) => this.employee = result,
        (error) => console.log('error => ', error)
      );
  }

  /**
   * Get functional unit information
   * @param id
   */
  getFunctionalUnit(id: string): void {
    this.functionalUnitService.getById(id)
      .subscribe(
        (result) => this.functionalUnit = result,
        (error) => console.error('error => ', error)
      );
  }

  /**
   * Change incident status to "In progress"
   */
  setInProgress(): void {
    this.incident.statusId = IncidentStatus.IN_PROGRESS;
    this.incidentService.update(this.incident)
      .subscribe(
        (result) => this.incident = result,
        (error) => console.error('error => ', error),
        () => this.refreshIncident(this.incident)
      );
  }

  /**
   * Close a ticket
   */
  setClose(): void {
    this.incident.statusId = IncidentStatus.CLOSED;
    this.incidentService.update(this.incident)
      .subscribe(
        (result) => this.incident = result,
        (error) => console.error('error => ', error),
        () => this.refreshIncident(this.incident)
      );
  }

  /**
   * Create a new incident log
   */
  newIncidentLog(): void {
    this.processing = true;
    this.incidentLog.logTypeId = +this.incidentLog.logTypeId;
    this.incidentLogService.save(this.incidentLog)
      .subscribe(
        (result) => console.log('saved => ', result),
        (error) => {
          console.log('error => ', error);
          this.processing = false;
        },
        () => {
          this.processing = false;

          if (this.incidentLog.logTypeId === IncidentLog.SOLUTION) {
            this.incident.statusId = IncidentStatus.SOLVED;
            this.incidentService.update(this.incident)
              .subscribe(
                (result) => this.incident = result,
                (error) => console.log('error => ', error),
                () => this.getIncidentLog(this.incident.id)
              );
          }

          this.incidentLog = new IncidentLog();
          this.refreshIncident(this.incident);
          this.getIncidentLog(this.incident.id);
        }
      );
  }

  /**
   * Search employee
   * @param userId
   */
  searchEmployee(userId: string): void {
    this.searchingEmployees = true;
    this.employeeService.search(userId)
      .subscribe(
        (result) => this.employees = result,
        (error) => {
          console.log('error => ', error);
          this.searchingEmployees = false;
        },
        () => this.searchingEmployees = false
      );
  }

  /**
   * Search team
   * @param teamId
   */
  searchTeam(teamId: string): void {
    this.searchingFunctionalUnits = true;
    this.functionalUnitService.search(teamId)
      .subscribe(
        (result) => this.functionalUnits = result,
        (error) => {
          console.log('error => ', error);
          this.searchingFunctionalUnits = false;
        },
        () => this.searchingFunctionalUnits = false
      );
  }

  /**
   * Set team for solve tickets
   * @param team
   */
  setTeam(team: FunctionalUnit): void {
    this.functionalUnit = undefined;
    this.employee = undefined;
    this.incident.assignedToTeamId = team.unidad_funcional;
    this.incident.assignedToPidm = null;
  }

  /**
   * Set user and team to ticket
   * @param employee
   */
  setUser(employee: Employee): void {
    this.employee = undefined;
    this.functionalUnit = undefined;
    this.incident.assignedToPidm = employee.employee_pidm;
    this.incident.assignedToTeamId = employee.unidad_funcional_organica;
  }

  /**
   * Set team to ticket
   * @param employee
   */
  setUserTeam(employee: Employee): void {
    this.employee = undefined;
    this.functionalUnit = undefined;
    this.incident.assignedToPidm = null;
    this.incident.assignedToTeamId = employee.unidad_funcional_organica;
  }

  /**
   * Verify if a ticket belongs to me or my team
   */
  isMine(): boolean {
    return this.myEmployeeProfiles[0].employee_pidm === this.incident.assignedToPidm ||
      this.myEmployeeProfiles[0].unidad_funcional_organica === this.incident.assignedToTeamId;
  }

  /**
   * Tiempo relativo a partir de una fecha tipo YYYYMMDD
   * p.e.: momento("20101225") retorna un string: "hace X años"
   * @param {string} date
   * @returns {string}
   */
  momento(date: string): string {
    return this.moment.fromNow(date);
  }
}
