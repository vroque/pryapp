import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ConfigService {
  public _BASE_URI: string;
  private _API_URI: string;
  private _JSON_HEADER = new Headers({'Content-Type': 'application/json', 'Accept': 'text/json'});
  private _MPFD_HEADER = new Headers({enctype: 'multipart/form-data'});

  /**
   * Calendar
   */
  private readonly calendarEs: {
    firstDayOfWeek: number;
    dayNames: string[];
    dayNamesShort: string[];
    dayNamesMin: string[];
    monthNames: string[];
    monthNamesShort: string[];
    today: string;
    clear: string;
  };
  private locationCalendar: { firstDayOfWeek: number; dayNames: string[]; dayNamesShort: string[]; dayNamesMin: string[];
                              monthNames: string[]; monthNamesShort: string[]; today: string; clear: string; };
  private dayNames = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
  private dayNamesShort = ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'];
  private dayNamesMin = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'];
  private monthNames = [
    'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
    'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
  ];
  private monthNamesShort = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
  private browsers = ['Chrome', 'Firefox', 'Opera', 'Safari'];

  constructor() {
    this._BASE_URI = (window as any).url_base || '/';
    this._API_URI = `${this._BASE_URI}api/`;
    this.calendarEs = {
      firstDayOfWeek: 0,
      dayNames: this.dayNames,
      dayNamesShort: this.dayNamesShort,
      dayNamesMin: this.dayNamesMin,
      monthNames: this.monthNames,
      monthNamesShort: this.monthNamesShort,
      today: 'Hoy',
      clear: 'Limpiar'
    };
    this.locationCalendar = this.calendarEs;
  }

  /**
   * Obtiene el nombre del mes desde su número de orden
   * @param id Número del mes de 0 a 11 (segun javascript)
   */
  getMonthName(id: number): string {
    const months = this.monthNames;
    if (id > 0 && id < 13) {
      return months[id - 1];
    }
    return 'fuera de rango';
  }

  /**
   * Spanish calendar for primeng
   * @returns {any}
   */
  getCalendarEs(): any {
    return this.calendarEs;
  }

  /**
   * Devuelve Localization for primeng/calendar
  */
  getLocationCalendar(): any {
    return this.locationCalendar;
  }

  /**
   * Obtiene la url base de las API's
   */
  getApiURI(): string {
   return this._API_URI;
  }

  /**
   * Obtiene la url base de la aplicación
   */
  getBaseURI(): string {
   return this._BASE_URI;
  }

  /**
   * Devuelve el nombre del navegar que se esta utilizando
   */
  getBrowser(): string {
    for (const browser of this.browsers) {
      if (window.navigator.userAgent.indexOf(browser) > -1) {
        return browser;
      }
    }
  }

  /**
   * Devuelve los nombres de los navegadores
   */
  getBrowsers(): string[] {
    return this.browsers;
  }

  /**
   * Obtiene el tipo de cabecera segun un tipo de consulta
   * @param requestType tipo de consulta que se realiza
   */
  getHeaders(requestType: string): RequestOptions {
    let options: RequestOptions;
    switch (requestType) {
      case('json'):
        options = new RequestOptions({headers: this._JSON_HEADER});
        break;
      case('multipart'):
        options = new RequestOptions({headers: this._MPFD_HEADER});
        break;
      default:
        options = new RequestOptions({headers: this._JSON_HEADER});
    }
    return options;
  }

  /**
   * Parsea la respuesta de HTTP a formato JSON
   * @param res HTTP Response para parcear a JSON
   */
  jsonExtractData(res: Response): any {
    const body: any = res.json() || {};
    return body;
  }

  /**
   * Captura el error de una peticion
   * @param error Server error
   */
  handleError(error: any): any {
    const errMsg: any = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    // console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

  /**
   * Convierte un objeto en una cadena
   * {a: 'b', c: 1} -> a=b&c=1
   * @param data objeto a convertir
   */
  urlFormat(data: any): string {
    const urlSearchParams: URLSearchParams = new URLSearchParams();
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        urlSearchParams.append(key, data[key]);
      }
    }
    return urlSearchParams.toString();
  }

  toCapitalize(cadena: string): string {
    return cadena.charAt(0).toUpperCase() + cadena.slice(1);
  }

}
