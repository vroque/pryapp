import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ParentModule } from './app/parent/parent.module';

const ENV = process.env.NODE_ENV = process.env.ENV;
if (ENV === 'production') {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(ParentModule);
