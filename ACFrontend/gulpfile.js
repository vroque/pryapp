const gulp = require('gulp');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const mincss = require('gulp-minify-css');
const streamqueue = require('streamqueue');
const del = require('del');
const chalk = require('chalk');
const config = require('./config.json');

const conf = config.envs[config.default_env];

// personally written JS
// e.g. 'app/app.js', 'app/assets/scripts/**/*.js'
const jsFiles = [
  'src/contrib/scripts/**/*.js',
];

// personally written SASS
// e.g. './src/contrib/sass/app.sass'
const sassFiles = [
  'src/contrib/sass/**/*.sass',
];

/*
  THIRD PARTY VENDOR FILES SECTION
*/
// includes jquery, angular and others with other crucial files, see items
// e.g. 'app/vendor/toastr/toastr.min.js', 'app/vendor/ngprogress/build/ngprogress.min.js',
// 'app/vendor/offline/offline.min.js'
const vendorFiles = [
  './src/contrib/jquerylibs/jquery-2.2.3.min.js',
  './src/contrib/jquerylibs/jquery-ui.js',
  './src/contrib/fixs/fixuibutton.js',
  './node_modules/bootstrap/dist/js/bootstrap.js',
  './src/contrib/jquerylibs/jquery.slimscroll.js',
  './src/contrib/jquerylibs/fastclick.js',
  './node_modules/daterangepicker/moment.min.js',
  './node_modules/daterangepicker/daterangepicker.js',
  './src/contrib/table2excel/jquery.table2excel.min.js',
  './node_modules/chart.js/dist/Chart.min.js',
  './src/contrib/tpl/app.js',
  './src/contrib/scripts/cau.js',
];

// e.g. 'app/vendor/toastr/toastr.min.css', 'app/vendor/ngprogress/ngProgress.css'
const vendorCSS = [
  'bootstrap/dist/css/bootstrap.css',
  'font-awesome/css/font-awesome.css',
  'primeng/resources/primeng.css',
  'primeng/resources/themes/omega/theme.css',
  'daterangepicker/daterangepicker.css',
];

const templateCSS = [
  './src/contrib/tpl/AdminLTE.css',
  './src/contrib/tpl/skins/skin-black.css',
  './src/contrib/tpl/skins/skin-green.css',
  './src/contrib/tpl/skins/skin-purple.css',
];

const templateJS = [
  './src/contrib/tpl/app.js',
];

const fontFiles = [
  './node_modules/font-awesome/fonts/*',
  './node_modules/bootstrap/fonts/*',
  './src/contrib/fonts/*',
  './node_modules/primeng/resources/themes/omega/fonts/*',
];

// refers to my build directory and or files to delete
const filesToDelete = [
  `${conf.url}/*`,
];

// deletes files
async function clean() {
  const deletedFilePaths = await del(filesToDelete, { force: true });
  console.log('[INFO] Deleted:', deletedFilePaths);
}

function copyFont() {
  return gulp.src(fontFiles)
    .pipe(gulp.dest(`${conf.url}/fonts`));
}

function buildCSS() {
  const cssBuilded = gulp.src(vendorCSS, { cwd: 'node_modules/**' })
    .pipe(concat('css-files.css'));

  const templateCSSBuilded = gulp.src(templateCSS)
    .pipe(concat('css-files.css'));

  const sassBuilded = gulp.src('./src/contrib/sass/app.sass')
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(concat('sass-files.css'));

  const allCSSMerged = streamqueue(
    { objectMode: true },
    cssBuilded,
    templateCSSBuilded,
    sassBuilded,
  )
    .pipe(concat('dist.css'))
    .pipe(mincss())
    .pipe(gulp.dest(`${conf.url}/styles`));

  return allCSSMerged;
}

function gwatch(files, dir, callback) {
  gulp.watch(files, dir, callback)
    .on('change', (path) => { console.log(`[INFO] ${chalk.yellow(`File changed: ${path}`)}`); })
    .on('add', (path) => { console.log(`[INFO] ${chalk.green(`File added: ${path}`)}`); })
    .on('unlink', (path) => { console.log(`[INFO] ${chalk.red(`File deleted: ${path}`)}`); });
}

function watchSASS() {
  gwatch(sassFiles, { cwd: './' }, buildCSS);
}

function buildJS() {
  return gulp.src(vendorFiles.concat(templateJS).concat(jsFiles))
    .pipe(concat('dist.js'))
    .pipe(gulp.dest(`${conf.url}/scripts`));
}

function watchJS() {
  gwatch(jsFiles, { cwd: './' }, buildJS);
}

exports.clean = clean;
exports.build = gulp.series(buildCSS, buildJS, copyFont);
exports.watch_sass = watchSASS;
exports.watch_js = watchJS;
