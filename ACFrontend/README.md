CAU Frontend
===

Projecto de angular para el frontesk de la plataforma cau. este proyecto tiene 3 subproyectos:
Plataforma de estudiantes A.K.A frontdesk
Plataforma de administrativos A.K.A backoffice
Plataforma de padres A.K.A parent


Tareas automatizadas
---

>- **npm run build:** Genera/copia todos los estaticos(JS, CSS, fonts).
>- **npm run build-st:** Genera los CSS y JS de librerias y extras y los copia en conjunton a las fuentes.
>- **npm run gsw:** Genera en vivivo los CSS y JS de librerias y extras y los copia en conjunton a las fuentes.
>- **npm run b-st:** Compila el modulo de estudiantes A.K.A. frontdesk.
>- **npm run b-xt:** Compila el modulo de administrativos A.K.A. backoffice.
>- **npm run b-pt:** Compila el modulo de padres A.K.A. parent.
>- **npm run w-st:** Compila en vivo el modulo de estudiantes A.K.A. frontdesk.
>- **npm run w-xt:** Compila en vivo el modulo de administrativos A.K.A. backoffice.
>- **npm run w-pt:** Compila en vivo el modulo de padres A.K.A. parent.

npm run: ejecuta:
-
  > npm build-st

  > npm run b-st

  > npm run b-xt

  > npm run b-pt



Happy compile :)

Forma sugeririda de uso:
---

**Si es la primera vez o se desea volver a generar todo desde cero**
-> verificar que la carpeta destino este vacia o eliminarla para evitar codigo inecesario
-> ejecutar **npm run build**

**Si se esta desarrollando y se desea tener los cambios en vivo**

- asegurarse que ya se ejecuto lo anterior
- ejecutar **npm run w-st** para la paltaforma de estudiantes
- ejecutar **npm run w-xt** para la paltaforma de administrativos
- ejecutar **npm run w-pt** para la paltaforma de padres
- ejecutar **npm run gsw** para los css/sass

**Si se desea hace una compilacion individual**

- **npm run b-st:** Compila el modulo de estudiantes A.K.A. frontdesk.
- **npm run b-xt:** Compila el modulo de administrativos A.K.A. backoffice.
- **npm run b-pt:** Compila el modulo de padres A.K.A. parent.

'''
  Happy Conding
      w
    ( °)>
  (<   )
    | |
'''
