CAU App 2.0
===

Aplicación Web para el Centro de Atención al Usuario

Versión 2 -> Angular 2 y ASP WebApis

Backend
---

Aplicación para administrativos, generalmente comprende funcionalidad en base a las areas 

Frontend
---

Aplicacion para estudiantes. 

Parent
---

Aplicación para padres


Arquitectura
===

Backend
---
- Lenguaje principal: C#
- Framework: ASP MVC 5
- SGDB: MSSQLServer y ORACLE 
- ORM: Entity Framework
- Compilacion: estandar

Frontend
---
- Lenguaje principal: JavaScript
- Framework: Angular 4
- Arquitectura: SPA
- Compilacion: ver [frontend compilation](./ACFrontend/README.md)