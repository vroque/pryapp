﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ACBusiness.AtentionCenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACBusiness.Academic;

namespace ACBusiness.AtentionCenter.Tests
{
    [TestClass()]
    public class ACTermTests
    {
        [TestMethod()]
        public void getCurrentEnrollmentTest()
        {
            Student s = new Student("76808704");
            ACTerm t = new ACTerm();
            AcademicProfile profile = s.profiles.First();
            ACTerm t2 = t.getCurrentEnrollment(profile.campus.id, profile.department);
            Assert.IsNotNull(t2, "es nullo");
            Assert.AreEqual(t2.term, "201800", $"el periodo {t2.term} no es 201800");
        }
    }
}