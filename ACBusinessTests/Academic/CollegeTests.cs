﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ACBusiness.Academic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Academic.Tests
{
    [TestClass()]
    public class CollegeTests
    {
        [TestMethod()]
        public void getTest()
        {
            College ing = College.get("01");
            Assert.IsNotNull(ing, "El college 01 es nulo");
            Assert.AreEqual(ing.name, "INGENIERÍA", "El college 01 no es ingenieria");
            College cs = College.get("03");
            Assert.IsNotNull(cs, "El college 03 es nulo");
            Assert.AreEqual(cs.name, "CIENCIAS DE LA SALUD", "El college 01 no es CS");
        }

        [TestMethod()]
        public void getByProgramIDTest()
        {
            College ing = College.getByProgramID("103");
            Assert.IsNotNull(ing, "El college del pprogram 103 es nulo");
            Assert.AreEqual(ing.name, "INGENIERÍA", "El college no es ingenieria");
            College cs = College.getByProgramID("507");
            Assert.IsNotNull(cs, "El college del program 507 es nulo");
            Assert.AreEqual(cs.name, "CIENCIAS DE LA SALUD", "El college no es CS");
        }

        [TestMethod()]
        public void listCollegeByLvlTest()
        {
            List<College> colleges = College.listCollegeByLvl("PG");
            Assert.IsNotNull(colleges, "no hay colleges para PG");
            Assert.IsTrue(colleges.Count > 0, "no hay colleges para PG");
        }

        [TestMethod()]
        public void listCollegeByLvlWithProgramsTest()
        {
            List<College> colleges = College.listCollegeByLvlWithPrograms("PG");
            Assert.IsNotNull(colleges, "no hay colleges para PG");
            Assert.IsNotNull(colleges.FirstOrDefault(), "no hay colleges para PG");
            Assert.IsNotNull(colleges.FirstOrDefault()?.programs, "El college 01 es nulo");
            Assert.IsTrue(colleges.FirstOrDefault()?.programs?.Count > 0, "El college 01 es nulo");

        }
    }
}