﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ACBusiness.Academic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Academic.Tests
{
    [TestClass()]
    public class SubjectsOfferedTests
    {
        [TestMethod()]
        public void getSubjectsTest()
        {
            SubjectsOffered a = new SubjectsOffered();
            List<SubjectsOffered> b = a.getSubjects();
            Assert.AreEqual(b.Count, 28);
        }
    }
}