﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ACBusiness.LanguagesCenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.LanguagesCenter.Tests
{
    [TestClass()]
    public class CICOffersTests
    {
        [TestMethod()]
        public void getActiveOffersTest()
        {
            CICOffer obj = new CICOffer();
            List<CICOffer> offers = obj.getActiveOffers("S01");
            Assert.IsTrue(offers.Count() > 0, "no existen ofertas horarias activas");
        }

        [TestMethod()]
        public async Task getActiveOffersByLanguageTest()
        {
            CICOffer obj = new CICOffer();
            List<CICOffer> englishOffers = await obj.getActiveOffersByLanguage("S01", "I01"); // ingles
            Assert.IsTrue(englishOffers.Count() > 0, "no existen ofertas horarias activas para ingles");
            List<CICOffer> portugueseOffers = await obj.getActiveOffersByLanguage("S01", "I02"); // portuges
            Assert.IsTrue(portugueseOffers.Count() > 0, "no existen ofertas horarias activas para portuges");
            List<CICOffer> italianOffers = await obj.getActiveOffersByLanguage("S01", "I03"); // italiano
            Assert.IsTrue(italianOffers.Count() > 0, "no existen ofertas horarias activas para italiano");
            
        }
    }
}