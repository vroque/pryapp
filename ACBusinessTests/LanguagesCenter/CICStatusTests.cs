﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ACBusiness.LanguagesCenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.LanguagesCenter.Tests
{
    [TestClass()]
    public class CICStatusTests
    {
        [TestMethod()]
        public async Task getCICStatusTest()
        {
            CICStatus st = new CICStatus();
            CICStatus s = await st.getCICStatus(48331);
            Assert.AreEqual(s.language, "I01", "1 Idioma errado");
            Assert.IsTrue(s.need_recategorize, "1 nececita examen errado");
            Assert.AreEqual(s.cycle, 14, "1 ciclo errado");
            Assert.AreEqual(s.last_cycle, 13, "1 ultimo ciclo errado");
            Assert.AreEqual(s.modality, "I", "1 modalidad errada");
            //95474
            CICStatus s2 = await st.getCICStatus(95474);
            Assert.AreEqual(s2.language, "I01", "2 Idioma errado");
            Assert.IsFalse(s2.need_recategorize, "2 nececita examen errado");
            Assert.AreEqual(s2.cycle, 8, "2 ciclo errado");
            Assert.AreEqual(s2.last_cycle, 8, "2 ultimo ciclo errado");
            Assert.AreEqual(s2.modality, "L", "2 modalidad errada");
        }
    }
}