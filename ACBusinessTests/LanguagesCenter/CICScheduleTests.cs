﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ACBusiness.LanguagesCenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACBusiness.Academic;

namespace ACBusiness.LanguagesCenter.Tests
{
    [TestClass()]
    public class CICScheduleTests
    {
        [TestMethod()]
        public async Task getScheduleTest()
        {
            CICOffer objOffer = new CICOffer();
            List<CICOffer> englishOffers = await objOffer.getActiveOffersByLanguage("S01", "I01"); // ingles
            CICSchedule obj = new CICSchedule();
            CICOffer offer = englishOffers.FirstOrDefault(f => f.section == "18INS0610M");
            List<CICSchedule> list = obj.getSchedule(offer);
            Assert.IsNotNull(list, "Este objeto es nulo");
        }

        [TestMethod()]
        public async Task getScheduleTestByUser()
        {
            CICSchedule obj = new CICSchedule();
            decimal pidm = 95474m;
            CICOfferSchedule data = await obj.getOfferSchedule(pidm);
            Assert.IsNotNull(data, "Este objeto es nulo");

        }
    }
}