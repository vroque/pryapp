﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ACBusiness.LanguagesCenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter.Tests
{
    [TestClass()]
    public class CICSectionTests
    {
        [TestMethod()]
        public void CICSectionTest()
        {
            CICSection sec = new CICSection("11INI0109M");
            Assert.IsTrue(_cic.ACTIVE_LANGUAGES_LIST.Contains(sec.language), "El idioma no esta en la lista de idiomas validos");
            Assert.AreEqual(sec.language, "I01", "Idioma errado"); //ingles
            Assert.IsTrue(_cic.ACTIVE_LANGUAGES_MODALITIES.Contains(sec.modality), "La modalidad no esta en la lista de idiomas validos");
            Assert.AreEqual(sec.modality, "I", "Modalidad errado"); //intensivo
            Assert.AreEqual(sec.cycle, 1, "Modalidad errado"); //intensivo
        }
    }
}