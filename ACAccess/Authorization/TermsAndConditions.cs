﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using Newtonsoft.Json;
using _app = ACTools.Constants.AppConstants;

namespace ACAccess.Authorization
{
    /// <summary>
    /// Terms And Conditions
    /// </summary>
    public class TermsAndConditions
    {
        #region Properties

        public int id { get; set; }
        public string name { get; set; }
        public bool isFile { get; set; }
        public string url { get; set; }
        public DateTime publicationDate { get; set; }
        public bool active { get; set; }

        private ACAuth _acAuth { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Methods

        public TermsAndConditions()
        {
        }

        public TermsAndConditions(ACAuth acAuth)
        {
            _acAuth = acAuth;
        }

        /// <summary>
        /// Terms and conditions list
        /// </summary>
        /// <returns>Terms and conditions list</returns>
        public List<TermsAndConditions> GetTerms()
        {
            var termsAndConditionsList = new List<TermsAndConditions>();

            var activeBlockNavigation =
                _nexoContext.cau_settings.Any(w => w.Id == _app.SETTINGS_TERMS_CONDITIONS && w.Active);

            if (_acAuth == null || !activeBlockNavigation) return termsAndConditionsList;

            List<int> activeTermsAndConditionsIdList =
                _nexoContext.cau_terms_conditions.Where(w => w.Active).Select(s => s.Id).ToList();

            List<int> pendingAcceptedTerms = _nexoContext.cau_accepted_terms
                .Where(w => activeTermsAndConditionsIdList.Contains(w.TermsId) &&
                            !w.Accepted && w.Pidm == _acAuth.person_id)
                .Select(s => s.TermsId).ToList();
            if (pendingAcceptedTerms.Any())
            {
                termsAndConditionsList = _nexoContext.cau_terms_conditions
                    .Where(w => pendingAcceptedTerms.Contains(w.Id)).ToList()
                    .Select(s => (TermsAndConditions) s).ToList();
            }

            return termsAndConditionsList;
        }

        /// <summary>
        /// Terms and conditions list in JSON format
        /// </summary>
        /// <returns>Terms and conditions list in JSON format</returns>
        public string GetTermsJsonConverted()
        {
            var termsSerializeObject = JsonConvert.SerializeObject(GetTerms(), Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

            return termsSerializeObject;
        }

        #endregion Methods

        #region Operator

        public static explicit operator TermsAndConditions(CAUTermsAndConditions cauTermsAndConditions)
        {
            if (cauTermsAndConditions == null) return null;

            var termsAndConditions = new TermsAndConditions
            {
                id = cauTermsAndConditions.Id,
                name = cauTermsAndConditions.Name,
                isFile = cauTermsAndConditions.IsFile,
                url = cauTermsAndConditions.Url,
                publicationDate = cauTermsAndConditions.PublicationDate,
                active = cauTermsAndConditions.Active
            };

            return termsAndConditions;
        }

        #endregion Operator
    }
}