﻿using ACAccess.Authentication;
using ACBusiness.Academic;
using ACBusiness.Institutional;
using System.Collections.Generic;
using System.Linq;
using _app = ACTools.Constants.AppConstants;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACAccess.Authorization
{
    /// <summary>
    /// Objeto de autentificación de  usuario que ser almacenado
    /// en memoria mediante una sesion
    /// </summary>
    public class ACAuth: ACUser
    {
        #region properties
        //public string id { get; set; }
        //public decimal person_id { get; set; }
        //public string last_name { get; set; }
        //public string first_name { get; set; }
        //public string type { get; set; }
        //public string token { get; set; }
        public string url_path { get; set; }   
        // Student
        public AcademicProfile profile { get; set; }
        // Executive
        public List<Module> module_list { get; set; }
        public List<string> group_list { get; set; }
        public Campus campus { get; set; }
        // DEPRECATED
        public string div { get; set; }


        #endregion properties

        public ACAuth()
        {
            this.module_list = new List<Module>();
            this.group_list = new List<string>();
        }

        #region methods
        /// <summary>
        /// Establece un perfil academico dependiento de los parametros
        /// enviados
        /// </summary>
        /// <param name="div">Dependencia</param>
        /// <param name="department">Modalidad</param>
        /// <param name="program">Carrera</param>
        public void syncProfile(string div, string department, string program) 
        {
            Student student = new Student(this.id);
            this.profile = student.profiles.FirstOrDefault(f =>
                f.program.id == program
                && f.department == department
                && f.div == div
           );
        }
        public void syncProfile(string program)
        {
            Student student = new Student(this.id);
            this.profile = student.profiles.FirstOrDefault(f =>
                f.program.id == program
                && f.div == _acad.DEFAULT_DIV
           );
        }

        #endregion methods

        #region statics methods
        public static ACAuth getAsValid(ACUser user)
        {
            ACAuth auth_obj = new ACAuth();
            //Load basic data
            auth_obj.id = user.id;
            auth_obj.person_id = user.person_id;
            auth_obj.last_name = user.last_name;
            auth_obj.first_name = user.first_name;
            auth_obj.token = user.token;
            auth_obj.type = user.type;
            //load authorization data
            if(user.type.Contains(_app.TYPE_EXECUTIVE))
            {
                auth_obj.url_path = "backoffice/";
                Module module = new Module();
                //auth_obj.module_list = module.getByUser()
            }
            else if(user.type.Contains(_app.TYPE_STUDENT) && user.type.Contains(_app.TYPE_PARENT))
            {
                auth_obj.url_path = "parent/";
            }
            else if (user.type.Contains(_app.TYPE_PARENT))
            {
                auth_obj.url_path = "parent/";
            }
            else
            {
                auth_obj.url_path = "frontdesk/";
                Student student = new Student(auth_obj.id);
                if (student.profiles.Count == 1)
                {
                    auth_obj.profile = student.profiles.First();
                    Module module = new Module();
                    auth_obj.module_list = module.getByStudent(auth_obj.profile);
                }
                    
            }

            return auth_obj;
        }
        
        #endregion statics methods
    }
    
}
