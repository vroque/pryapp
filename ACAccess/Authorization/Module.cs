﻿using ACBusiness.Academic;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.ACCESS;
using System.Collections.Generic;
using System.Linq;
using _app = ACTools.Constants.AppConstants;
using _acad = ACTools.Constants.AcademicConstants;
using ACBusiness.Institutional;
using ACPermanence.DataBases.NEXO.DIM;

namespace ACAccess.Authorization
{
    public class Module
    {
        #region properties
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string campus { get; set; }

        private NEXOContext _db = new NEXOContext();
        #endregion properties

        #region methods
        public List<Module> getByUser(string user_id, string campus_id)
        {
            List<Module> list = _db.access_group_users
                .Join(
                    _db.access_group_modules,
                    gu => gu.groupID,
                    gm => gm.groupID,
                    (gu, gm) => new { gu, gm }
                )
                .Join(
                    _db.access_modules,
                    grp => grp.gm.moduleID,
                    mod => mod.moduleID,
                    (grp, mod) => new {grp.gu, grp.gm, mod}
                )
                .Join(
                    _db.access_applications,
                    ugm => ugm.mod.applicationID,
                    app => app.applicationID,
                    (ugm, app) => new { ugm.gu, ugm.gm, ugm.mod, app }
                )
                .Where( f =>
                    f.gu.userID == user_id
                    && f.app.applicationID == _app.CAUAPP_ID
                    && f.gm.campus == campus_id
                    && f.mod.moduleName.StartsWith(_app.BACKOFFICE_PREFIX)
                )
                .Select(f => new Module
                    {
                        id = f.gm.moduleID,
                        name = f.mod.moduleName,
                        campus = f.gm.campus
                    }
                )
                .ToList();
            Module anon_mod = _db.access_modules.FirstOrDefault(f => f.moduleName == _app.ANONYMOUS_MODULE);
            if (anon_mod != null)
                list.Add(anon_mod);
            return list;
        }

        public List<Module> getByStudent(AcademicProfile profile)
        {
            List<Module> list = new List<Module>();
            string module_name = _acad.STUDENT_MODULES[profile.status];
            Module m = _db.access_modules.FirstOrDefault(f =>
                  f.applicationID == 1
                  && f.moduleName == module_name
                );
            // modulo false fd_academic para todos los estudiantes (desde ingresantes a mas)
            Module mock = new Module {
                id = 0,
                name = "fd_academic",
                description = "Academico"
            };
            list.Add(mock);
            list.Add(m);
            return list;
        }

        public List<Campus> listExecutiveCampus(string user_id)
        {
            
            List<string> list = _db.access_group_users
                .Join(
                    _db.access_group_modules,
                    gu => gu.groupID,
                    gm => gm.groupID,
                    (gu, gm) => new { gu, gm }
                )
                .Join(
                    _db.access_modules,
                    grp => grp.gm.moduleID,
                    mod => mod.moduleID,
                    (grp, mod) => new { grp.gu, grp.gm, mod }
                )
                .Join(
                    _db.access_applications,
                    ugm => ugm.mod.applicationID,
                    app => app.applicationID,
                    (ugm, app) => new { ugm.gu, ugm.gm, ugm.mod, app }
                )
                .Where(f =>
                   f.gu.userID == user_id
                   && f.app.applicationID == _app.CAUAPP_ID
                )
                .Select(f => f.gm.campus)
                .Distinct()
                .ToList();
            if (list == null || list.Count == 0)
                list = new List<string> { _app.DEFAULT_CAMPUS };
            List<Campus> campuses = Campus.getByListID(list);
            return campuses;
        }
       
        /// <summary>
        /// List all modules for BACKOFFICE 
        /// </summary>
        /// <returns></returns>
        public List<Module> listBackoffice()
        {
            List<Module> list = _db.access_modules.Where(f =>
                   f.applicationID == _app.CAUAPP_ID
                   && f.moduleName.StartsWith(_app.BACKOFFICE_PREFIX)
                ).ToList()
                .Select<ACCESSTblModule, Module>(f => f)
                .ToList();
           
            return list;
        }
        //public List<Module> listFrontdesk()
        //{

        //}
        #endregion methods

        #region implicite operators

        public static implicit operator Module(ACCESSTblModule obj)
        {
            if (obj == null) return null;
            Module md = new Module();
            md.id = obj.moduleID;
            md.name = obj.moduleName;
            md.description = obj.moduleDescription;

            return md;
        }
        #endregion implicite operators

    }
}
