﻿using ACBusiness.Academic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACAccess.Authorization
{
    interface IAuth
    {
        string id { get; set; }
        decimal person_id { get; set; }
        string last_name { get; set; }
        string first_name { get; set; }
        string type { get; set; }
        //string url_path { get; set; }
        string token { get; set; }
        // Student
        AcademicProfile profile { get; set; }
        // Executive
        int oficina_id { get; set; }
        string group_id { get; set; }
        List<string> group_list { get; set; }
    }
}
