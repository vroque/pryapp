﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.ACCESS;
using System;
using System.Collections.Generic;
using System.Linq;
using _app = ACTools.Constants.AppConstants;
using _acad = ACTools.Constants.AcademicConstants;


namespace ACAccess.Authorization
{
    /// <summary>
    /// Menus de la app
    /// </summary>
    public class Menu
    {
        #region properties

        public int id { get; set; }
        public string name { get; set; }
        public string uri { get; set; }
        public int? parent { get; set; }
        public List<Menu> smenus { get; set; }
        public string icon { get; set; }
        public int module { get; set; }
        public string moduleName { get; set; }
        public bool is_active { get; set; }
        public string position { get; set; }
        public string description { get; set; }
        public string cardStyle { get; set; }
        public string cardIcoStyle { get; set; }
        public int? order { get; set; }
        public bool externalLink { get; set; }

        private readonly NEXOContext _db = new NEXOContext();

        private const string ACTIVE = "1";
        private const string INACTIVE = "0";

        #endregion properties

        #region methods

        /// <summary>
        /// obtiene los menus segun el id del usuario y una Sede
        /// ** Soporta 3 niveles de anidamiento
        /// ** Menu Principal o Menu de Modulo
        /// ** Menu Secundario o SubMenu
        /// ** Menu de tercer nivel o Pagina
        /// </summary>
        /// <param name="campus">Sede ID</param>
        /// <param name="user_id">Usuario ID</param>
        /// <returns></returns>
        public List<Menu> getFromSedeUser(string campus, string user_id)
        {
            //Modulos permitidos por sede/usuario
            List<Tuple<int, string>> moduleList = new List<Tuple<int, string>>();
            moduleList = _db.access_group_users
                .Join(_db.access_group_modules, gu => gu.groupID, gm => gm.groupID, (gu, gm) => new { gu, gm })
                .Join(_db.access_modules, g => g.gm.moduleID, mo => mo.moduleID, (g, mo) => new { g, mo })
                .Where(f => f.g.gu.userID == user_id &&
                            f.g.gm.campus == campus &&
                            f.mo.applicationID == _app.CAUAPP_ID &&
                            f.mo.moduleName.StartsWith("bo") //BackOffice
                )
                .Select(f => new { f.g.gm.moduleID, f.mo.moduleName })
                .AsEnumerable()
                .Select(f => new Tuple<int, string>( f.moduleID, f.moduleName))
                .ToList();
            List<int> moduleListId = moduleList.Select(f => f.Item1).ToList();
            //get menus
            List<ACCESSTblMenu> menuList = _db.access_menus.Where(f => moduleListId.Contains(f.moduleID) &&
                                                                        f.status == ACTIVE).OrderBy(f => f.order)
                .ToList();
            IEnumerable<Menu> n = menuList.Select<ACCESSTblMenu, Menu>(f => (Menu) f);
            List<Menu> m = n.ToList();
            //compose menus
            List<Menu> menu_tree = m.Where(f => f.parent == null).ToList();
            foreach (Menu menu in menu_tree)
            {
                menu.smenus = m.Where(f => f.parent == menu.id).ToList();
                foreach (Menu smenu in menu.smenus)
                {
                    smenu.smenus = m.Where(f => f.parent == smenu.id).ToList();
                }
                menu.moduleName = moduleList.Where(f => f.Item1 == menu.module).Select(f => f.Item2).FirstOrDefault();
            }

            return menu_tree.OrderBy(o => o.order).ThenBy(o => o.name).ToList();
        }

        /// <summary>
        /// Lista de Menus para el Alumno
        /// </summary>
        /// <param name="student_status">Estado del Alumno</param>
        /// <returns></returns>
        public List<Menu> getFromStudentStatus(int student_status)
        {
            string module_name = _acad.STUDENT_MODULES[student_status];
            //Modulos permitidos por alumno/estado
            List<int> module_list = new List<int>();
            module_list = _db.access_modules
                .Where(f => f.applicationID == 1 && f.moduleName == module_name)
                .Select(f => f.moduleID).ToList();
            //get menus
            List<ACCESSTblMenu> menu_list = _db.access_menus.Where(f => module_list.Contains(f.moduleID) &&
                                                                        f.status == ACTIVE).OrderBy(f => f.order)
                .ToList();
            IEnumerable<Menu> n = menu_list.Select<ACCESSTblMenu, Menu>(f => (Menu) f);
            List<Menu> m = n.ToList();
            //compose menus
            List<Menu> menu_tree = m.Where(f => f.parent == null).ToList();
            foreach (Menu menu in menu_tree)
            {
                menu.smenus = m.Where(f => f.parent == menu.id).ToList();
                foreach (Menu smenu in menu.smenus)
                {
                    smenu.smenus = m.Where(f => f.parent == smenu.id).ToList();
                }
            }

            return menu_tree;
        }

        public List<Menu> getFromParentChild()
        {
            //Modulos permitidos por alumno/estado
            List<int> module_list = new List<int>();
            module_list = _db.access_modules
                .Where(f =>
                    f.applicationID == _app.CAUAPP_ID
                    && f.moduleName.StartsWith("pt_")
                )
                .Select(f => f.moduleID)
                .ToList();
            //get menus
            List<ACCESSTblMenu> menu_list = _db.access_menus
                .Where(f =>
                        module_list.Contains(f.moduleID)
                        && f.status == "1" //active
                ).OrderBy(f => f.order).ToList();
            IEnumerable<Menu> n = menu_list.Select<ACCESSTblMenu, Menu>(f => (Menu) f);
            List<Menu> m = n.ToList();
            //.ToList();
            //compose menus
            List<Menu> menu_tree;
            menu_tree = m
                .Where(f => f.parent == null)
                .ToList();
            foreach (Menu menu in menu_tree)
            {
                menu.smenus = m
                    .Where(f => f.parent == menu.id)
                    .ToList();
                foreach (Menu smenu in menu.smenus)
                {
                    smenu.smenus = m
                        .Where(f => f.parent == smenu.id)
                        .ToList();
                }
            }

            return menu_tree;
        }


        /// <summary>
        /// Lista todos los menus de un modulo
        /// </summary>
        /// <param name="module_id"></param>
        /// <returns>Lista de menus</returns>
        public List<Menu> listFromModule(int module_id)
        {
            List<Menu> menus = _db.access_menus.Where(f => f.moduleID == module_id).OrderBy(f => f.order).ToList()
                .Select<ACCESSTblMenu, Menu>(f => f).ToList();
            //compose menu structure
            List<Menu> menu_tree = menus.Where(f => f.parent == null).ToList();
            foreach (Menu menu in menu_tree)
            {
                menu.smenus = menus.Where(f => f.parent == menu.id).ToList();
                foreach (Menu smenu in menu.smenus)
                {
                    smenu.smenus = menus.Where(f => f.parent == smenu.id).ToList();
                }
            }

            return menu_tree;
        }

        // CRUD

        public bool create()
        {
            this.ReOrder("create", this.id, this.module, this.parent, this.order);
            ACCESSTblMenu n_menu = this.compose();
            n_menu.updateDate = DateTime.Now;
            _db.access_menus.Add(n_menu);
            int r = _db.SaveChanges();
            return r > 0;
        }

        public bool update()
        {
            this.ReOrder("update", this.id, this.module, this.parent, this.order);
            ACCESSTblMenu n_menu = _db.access_menus.FirstOrDefault(f => f.menuID == this.id);
            if (n_menu == null) return false;
            n_menu.icon = this.icon;
            n_menu.name = this.name;
            n_menu.uri = this.uri;
            n_menu.position = this.position;
            n_menu.status = this.is_active ? ACTIVE : INACTIVE;
            n_menu.updateDate = DateTime.Now;
            n_menu.description = this.description;
            n_menu.cardStyle = this.cardStyle;
            n_menu.cardIcoStyle = this.cardIcoStyle;
            n_menu.order = this.order;
            n_menu.ExternalLink = this.externalLink;
            var r = _db.SaveChanges();
            return r > 0;
        }

        public bool delete()
        {
            ACCESSTblMenu n_menu = _db.access_menus.FirstOrDefault(f => f.menuID == this.id);
            if (n_menu == null) return false;

            _db.access_menus.Remove(n_menu);
            int r = _db.SaveChanges();
            return r > 0;
        }


        private ACCESSTblMenu compose()
        {
            var newMenu = new ACCESSTblMenu
            {
                icon = this.icon,
                menuID = this.id,
                moduleID = this.module,
                name = this.name,
                uri = this.uri,
                parentID = this.parent,
                position = this.position,
                status = this.is_active ? ACTIVE : INACTIVE,
                description = this.description,
                cardStyle = this.cardStyle,
                cardIcoStyle = this.cardIcoStyle,
                order = this.order,
                ExternalLink = externalLink
            };

            return newMenu;
        }

        private void ReOrder(string type, int menuID, int module, int? parentID, int? newOrder)
        {
            List<ACCESSTblMenu> listMenu =
                _db.access_menus.Where(f => f.moduleID == module && f.parentID == parentID).ToList();

            if (type == "update")
            {
                ACCESSTblMenu existe = _db.access_menus.FirstOrDefault(f => f.menuID == this.id);
                if (existe != null)
                {
                    if (newOrder != existe.order)
                    {
                        foreach (ACCESSTblMenu item in listMenu)
                        {
                            // si el nuevo orden es mayor que el actual
                            if (newOrder > existe.order)
                            {
                                if (existe.order < item.order && item.order <= newOrder)
                                {
                                    item.order--;
                                    int r = _db.SaveChanges();
                                }
                            }
                            else if (newOrder < existe.order) // si el nuevo orden es menor que el actual
                            {
                                if (newOrder <= item.order && item.order < existe.order)
                                {
                                    item.order++;
                                    int r = _db.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            else if (type == "create")
            {
                if (newOrder <= listMenu.Count)
                {
                    foreach (ACCESSTblMenu item in listMenu)
                    {
                        if (item.order >= newOrder)
                        {
                            item.order++;
                            int r = _db.SaveChanges();
                        }
                    }
                }
            }
        }

        #endregion methods

        #region implicit operators

        public static implicit operator Menu(ACCESSTblMenu obj)
        {
            if (obj == null) return null;
            var menu = new Menu
            {
                id = obj.menuID,
                name = obj.name,
                uri = obj.uri,
                parent = obj.parentID,
                module = obj.moduleID,
                icon = obj.icon,
                is_active = obj.status == ACTIVE,
                smenus = null,
                position = obj.position,
                description = obj.description,
                cardStyle = obj.cardStyle,
                cardIcoStyle = obj.cardIcoStyle,
                order = obj.order,
                externalLink = obj.ExternalLink
            };

            return menu;
        }

        #endregion implicit operators
    }
}