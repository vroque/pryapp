﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using Newtonsoft.Json;
using _app = ACTools.Constants.AppConstants;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACAccess.Authorization
{
    /// <summary>
    /// Survey
    /// </summary>
    public class Survey
    {
        #region Properties

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public DateTime begin_date { get; set; }
        public DateTime end_date { get; set; }
        public bool active { get; set; }

        private ACAuth _acAuth { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Methods

        public Survey()
        {
        }

        public Survey(ACAuth acAuth)
        {
            _acAuth = acAuth;
        }

        /// <summary>
        /// Survey list
        /// </summary>
        /// <returns>Survey list</returns>
        public List<Survey> GetSurveys()
        {
            var surveys = new List<Survey>();

            var activeBlockNavigation = _nexoContext.cau_settings.Any(w => w.Id == _app.SETTINGS_SURVEY && w.Active);

            if (_acAuth == null || !activeBlockNavigation) return surveys;

            /*List<int> surveysIdList = _nexoContext.cau_survey.Where(w => w.Active).Select(s => s.Id).ToList();

            List<int> pendingSurveys = _nexoContext.cau_surveyed
                .Where(w => surveysIdList.Contains(w.SurveyId) && !w.Completed && w.Pidm == _acAuth.person_id)
                .Select(s => s.SurveyId).ToList();
            if (pendingSurveys.Any())
            {
                surveys = _nexoContext.cau_survey.Where(w => pendingSurveys.Contains(w.Id)).ToList()
                    .Select<CAUSurvey, Survey>(s => s).ToList();
            }*/

            // NOTE: Esto debería ser temporal y el código comentado arriba debería de regresar, algún día, algún día...
            var div = _acAuth.div ?? _corp.UC;
            List<Survey> pendingSurveys =
                _nexoContext.cau_encuesta_activa_alumno(div, _acAuth.campus.code, _acAuth.id).ToList()
                    .Select(s => new Survey
                    {
                        id = s.Id,
                        name = s.NombreEncuesta,
                        url = UrlEncuesta(s.RutaEncuesta, div, _acAuth.campus.code, _acAuth.id)
                    }).ToList();
            foreach (var pendingSurvey in pendingSurveys)
            {
                surveys.Add(pendingSurvey);
            }

            return surveys;
        }

        /// <summary>
        /// Survey list in JSON format
        /// </summary>
        /// <returns>Survey list in JSON format</returns>
        public string GetSurveysJsonConverted()
        {
            var surveysJsonSerializeObject = JsonConvert.SerializeObject(GetSurveys(), Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

            return surveysJsonSerializeObject;
        }

        /// <summary>
        /// URL encuesta por estudiante
        /// </summary>
        /// <param name="urlEncuesta">URL base de la encuesta</param>
        /// <param name="div">IDDependencia</param>
        /// <param name="campus">Código del campus</param>
        /// <param name="studentId">Código del estudiante</param>
        /// <returns>URL de la encuesta por estudiante</returns>
        private static string UrlEncuesta(string urlEncuesta, string div, string campus, string studentId)
        {
            return $"{urlEncuesta}&IDAlumno={ToBase64Str(studentId)}&IDDependencia={ToBase64Str(div)}&IDSede={ToBase64Str(campus)}";
        }

        /// <summary>
        /// Conversor a Base64
        /// para aplicar la lógica de "seguridad" que tiene TI para las URLs de las encuestas ┌П┐(ಠ_ಠ)
        /// </summary>
        /// <param name="str">Cadena a convertir</param>
        /// <returns>Cadena "encriptada"</returns>
        private static string ToBase64Str(string str)
        {
            return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(str));
        }

        #endregion Methods

        #region Implicit Operator

        public static implicit operator Survey(CAUSurvey cauSurvey)
        {
            if (cauSurvey == null) return null;

            var survey = new Survey
            {
                id = cauSurvey.Id,
                name = cauSurvey.Name,
                description = cauSurvey.Description,
                url = cauSurvey.Url,
                begin_date = cauSurvey.BeginDate,
                end_date = cauSurvey.EndDate,
                active = cauSurvey.Active
            };

            return survey;
        }

        #endregion Implicit Operator
    }
}