﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACAccess.Authentication
{
    /// <summary>
    /// Interface Atencion Center Login
    /// Interfaz que define los parametros para el uso de login
    /// </summary>
    interface IACLogin
    {
        ACUser login(string username, string password);
        bool logout();
        bool refresh();
        bool clean();
    }
}
