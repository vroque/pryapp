﻿using ACAccess.Authorization;
using ACBusiness.Parent;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO.views;
using ACPermanence.DataBases.NEXO.CAU;
using ACTools.SuperString;
using System;
using System.Collections.Generic;
using System.Linq;
using _app = ACTools.Constants.AppConstants;

namespace ACAccess.Authentication
{
    public class ACUser
    {
        #region properties
        /// <summary>
        /// codigo de usuario
        /// generally email whitout @continental
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// PIDM o Codigo de persona
        /// </summary>
        public decimal person_id { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public List<string> type { get; set; }
        public string token { get; set; }
        public string full_name
        {
            get
            {
                return $"{this.last_name}, {this.first_name}".toEachUpperClean();
            }
        }
        //public List<string> groups { get; set; }

        private NEXOContext _nx = new NEXOContext();
        private DBOContext _dbo = new DBOContext();
        #endregion properties

        public ACUser syncByEmail(string email)
        {
            
            if (this.verify_email(email))
            {
                //asignar id
                this.id = email.Split('@')[0].ToLower();
                this.sync_data(); //asign person_id and type
                return this;
            }
            return null;
        }
        private void sync_data()
        {
            this.type = new List<string>();
            Person person_obj = new Person();
            if (this.id != null)
            {
                //Verify if is Executive
                var executive = _nx.access_users.FirstOrDefault(f => f.userID == this.id);
                if (executive != null)
                {
                    this.type.Add(_app.TYPE_EXECUTIVE);
                    var p = new Person(executive.pidm);
                    this.person_id = executive.pidm;
                    this.last_name = p.last_name;
                    this.first_name = p.first_name;
                }
                else
                {
                    bool is_parent;
                    Person person_student = person_obj.getByStudentCode(this.id);

                    Person person_parent = new Person().getByDNI(this.id);
                    if (person_parent != null)
                        is_parent = new Relationship().parentExist(person_parent.id);
                    else is_parent = false;

                    if (person_student != null && !is_parent)
                    {
                        this.type.Add(_app.TYPE_STUDENT);
                        this.person_id = person_student.id;
                        this.last_name = person_student.last_name;
                        this.first_name = person_student.first_name;
                    }
                    else if (is_parent && person_student != null)
                    {
                        this.type.Add(_app.TYPE_PARENT);
                        this.type.Add(_app.TYPE_STUDENT);
                        this.person_id = person_parent.id;
                        this.last_name = person_parent.last_name;
                        this.first_name = person_parent.first_name;
                    }
                    else if(is_parent)
                    {
                        this.type.Add(_app.TYPE_PARENT);
                        this.person_id = person_parent.id;
                        this.last_name = person_parent.last_name;
                        this.first_name = person_parent.first_name;
                    }
                }
            }
        }
        private bool verify_email(string email)
        {
            email = email.ToLower();
            //verify if has a email
            if (email.Split('@').Length == 2 )
            {
                //verify in conti
                if (email.Split('@')[1] == "continental.edu.pe")
                {
                    return true;
                }
            }
            return false;
        }
    }
}
