﻿using ACTools.Request;
using Newtonsoft.Json.Linq;
using System;
namespace ACAccess.Authentication.Login
{
    public class OAuthLogin : IACLogin
    {
        #region implements
        /// <summary>
        /// Login mediante OAuth2
        /// </summary>
        /// <param name="email">No es necesario</param>
        /// <param name="password">Token de Acceso</param>
        /// <returns></returns>
        public ACUser login(string email, string token)
        {
            //se ignora email y se usa el token como password
            //string token = password;
            ACUser user = null;
            string current_user = null;
            bool is_valid_with_token = false;
           
            Requests r = new Requests(string.Format("https://www.googleapis.com/oauth2/v1/tokeninfo/?access_token={0}", token));
            r.GetResponse();
            if (r.Status == "OK")
            {
                JObject response_json = r.ToJson();
                if (response_json != null)
                {
                    try
                    {
                        current_user = (string)response_json["email"];
                        is_valid_with_token = true;
                    }
                    catch
                    {
                        is_valid_with_token = false;
                    }

                }
            }
            if (is_valid_with_token)
            {
                user = new ACUser();
                user.token = token;
                user.syncByEmail(current_user);
                if (user.person_id == 0M) // not in users
                    return null;
            }
            //retornamos usuario
            return user;
        }
        public bool logout()
        {
            return false;
        }
        public bool refresh()
        {
            return false;
        }
        public bool clean()
        {
            return false;
        }

        #endregion implements

    }
}
