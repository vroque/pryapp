﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACAccess.Authentication.Login
{
    /// <summary>
    /// Login bridge para iniciar sesion solo con el nombre 
    /// del usuario (sin contraseña)
    /// </summary>
    public class BridgeLogin: IACLogin
    {
        // password is ignore in this case
        public ACUser login(string username, string password)
        {
            //se ignora password el username es el email que escribio
            ACUser user = new ACUser();
            user.syncByEmail(username);
            if (user.person_id == 0M) // not in users
                return null;
            return user;
        }
        public bool logout()
        {
            return false;
        }
        public bool refresh()
        {
            return false;
        }
        public bool clean()
        {
            return false;
        }
    }
}
