﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.Extra
{
    public class Wifi
    {
        #region Propiedades

        public decimal id { get; set; }
        public string key { get; set; }

        private readonly DBOContext _dboContext = new DBOContext();

        #endregion

        #region Metodos

        /// <summary>
        /// Wifi info
        /// </summary>
        /// <param name="studentPidm">Student PIDM</param>
        /// <returns></returns>
        public async Task<Wifi> GetAsync(decimal studentPidm)
        {
            var info = await _dboContext.banner_spriden.Where(result => result.SPRIDEN_PIDM == studentPidm)
                .ToListAsync();

            return info.Select(result => new Wifi {id = result.SPRIDEN_PIDM, key = result.CONTI_PASS,})
                .FirstOrDefault();
        }

        #endregion
    }
}