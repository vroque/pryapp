using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Admision;
using ACBusiness.Settings;
using ACPermanence.Contexts.NEXO;
using _terms = ACBusiness.TermsAndConditions;
using ACBusiness.Accounts;

namespace ACBusiness.Extra
{
    public class BannerEnrollment
    {
        #region Properties

        public bool HasPendingTerms { get; set; }
        public _terms.TermsAndConditions TermsAndConditions { get; set; }
        public bool HasPendingRequirements { get; set; }
        public List<Retention> Requirements { get; set; } = new List<Retention>();

        #endregion Properties

        #region Methods

        /// <summary>
        /// Verifica si un estudiante tiene terminos y condiciones de inscripción pendiente por aceptar
        /// </summary>
        /// <param name="pidm"></param>
        /// <param name="academicProfile"></param>
        /// <returns></returns>
        public static async Task<BannerEnrollment> HasPendingTermsAndConditions(decimal pidm,
            AcademicProfile academicProfile)
        {
            var nexoContext = new NEXOContext();
            var bannerEnrollment = new BannerEnrollment();

            List<Retention> requirements = new Retention().getRetentionsByPidm(academicProfile.person_id);

            if (requirements.Any())
            {
                bannerEnrollment.HasPendingRequirements = requirements.Any();
                bannerEnrollment.Requirements = requirements;

                return bannerEnrollment;
            }

            var settingsDetailForTermsForEnrollment = await new SettingsDetail().GetTermsForEnrollment();

            if (settingsDetailForTermsForEnrollment == null) return bannerEnrollment;

            var terms = await nexoContext.cau_terms_conditions
                .FirstOrDefaultAsync(w => w.Active && w.Id == settingsDetailForTermsForEnrollment.NumberValue);

            if (terms == null) return bannerEnrollment;

            var acceptedTerms = await nexoContext.cau_accepted_terms
                .AnyAsync(w => w.TermsId == terms.Id && w.Accepted && w.Pidm == pidm);

            bannerEnrollment.HasPendingTerms = !acceptedTerms;

            if (acceptedTerms) return bannerEnrollment;

            bannerEnrollment.TermsAndConditions = (_terms.TermsAndConditions) terms;
            bannerEnrollment.TermsAndConditions.Url = Path.GetFileName(terms.Url);

            return bannerEnrollment;
        }

        #endregion Methods
    }
}