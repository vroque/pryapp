﻿using System.Data.Entity;
using System.IO;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Settings;
using ACPermanence.Contexts.NEXO;
using _terms = ACBusiness.TermsAndConditions;

namespace ACBusiness.Extra
{
    public class ExtracurricularEnrollment
    {
        #region Properties

        public bool HasPendingTerms { get; set; }
        public _terms.TermsAndConditions TermsAndConditions { get; set; }

        #endregion Properties

        #region Methods
        public static async Task<ExtracurricularEnrollment> GetDetailExtracurricular(decimal pidm,
            AcademicProfile academicProfile)
        {
            var nexoContext = new NEXOContext();
            var extEnrollment = new ExtracurricularEnrollment();

            var settingsDetailExt = await new SettingsDetail().GetTermsForExtracurricular();

            if (settingsDetailExt == null) return extEnrollment;

            var terms = await nexoContext.cau_terms_conditions.FirstOrDefaultAsync(x => x.Active 
                && x.Id == settingsDetailExt.NumberValue);

            if (terms == null) return extEnrollment;

            var acceptedTerms = await nexoContext.cau_accepted_terms.AnyAsync(x => x.TermsId == terms.Id 
                && x.Accepted && x.Pidm == pidm);

            extEnrollment.HasPendingTerms = !acceptedTerms;

            if (acceptedTerms) return extEnrollment;

            extEnrollment.TermsAndConditions = (_terms.TermsAndConditions)terms;
            extEnrollment.TermsAndConditions.Url = Path.GetFileName(terms.Url);

            return extEnrollment;
        }
        #endregion Methods
    }
}
