﻿using ACPermanence.Contexts.BDUCCI;
using System.Collections.Generic;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System.Linq;

namespace ACBusiness.Extra
{
    public class DisplayBlock
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public System.DateTime startFech { get; set; }
        public System.DateTime endFech { get; set; }
        public bool active { get; set; }

        readonly NEXOContext dbo = new NEXOContext();

        public DisplayBlock getRegister(int id)
        {
            DisplayBlock display = dbo.cau_display_block.FirstOrDefault(f => f.id==id);

            return display;
        }

        public List<DisplayBlock> getRegisters()
        {
            List<DisplayBlock> displays = dbo.cau_display_block.OrderByDescending(f => f.id).ToList()
                                          .Select<CAUTblDisplayBlock, DisplayBlock>(f =>f).ToList();
            return displays;
        }

        public DisplayBlock post(DisplayBlock data)
        {
            CAUTblDisplayBlock newRegister = new CAUTblDisplayBlock();
            newRegister.name = data.name;
            newRegister.description = data.description;
            newRegister.startFech = data.startFech;
            newRegister.endFech = data.endFech;
            newRegister.active = data.active;

            dbo.cau_display_block.Add(newRegister);
            int i = dbo.SaveChanges();
            // validación de haberse registrado bien
            if (i > 0)
            {
                return newRegister;
            }
            return null;
        }
        public DisplayBlock put(DisplayBlock register)
        {
            CAUTblDisplayBlock existingRegister = dbo.cau_display_block.FirstOrDefault(f => f.id == register.id);
            if (existingRegister == null) return null;

            existingRegister.id = register.id;
            existingRegister.name = register.name;
            existingRegister.description = register.description;
            existingRegister.startFech = register.startFech;
            existingRegister.endFech = register.endFech;
            existingRegister.active = register.active;

            int r = dbo.SaveChanges();
            if (r > 0)
            {
                return existingRegister;
            }
            else
            {
                return null;
            }
        }

        public static implicit operator DisplayBlock(CAUTblDisplayBlock query)
        {
            if (query == null) return null;

            NEXOContext nexoContext = new NEXOContext();
            DisplayBlock register = new DisplayBlock()
            {
                id = query.id,
                name = query.name,
                description = query.description,
                startFech = query.startFech,
                endFech = query.endFech,
                active = query.active
            };
            return register;
        }
    }
}