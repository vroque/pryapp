﻿using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.NEXO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Extra
{
    public class UniversityCard
    {
        private readonly NEXOContext _nexoContext = new NEXOContext();
        #region Metodos
        /// <summary>
        /// Retornar Datos de carnet del ultimo periodo
        /// </summary>
        /// <param name="pidm">Pidm de estudiante</param>
        /// <param name="department">Modalidad</param>
        /// <param name="campus">Sede</param>
        /// <param name="program">Escuela</param>
        /// <returns></returns>
        public dynamic get( decimal pidm, string department, string campus, string program  )
        {
            ACTerm term = new ACTerm().getCurrent(campus, department);
            if (term != null)
            {
                var data = _nexoContext.tcu_tbl_carnet.Where(f =>
                               f.pidm == pidm
                               && f.divs == "UCCI"
                               && f.campus == campus
                               && f.program == program
                               && f.idYear == term.term.Substring(0, 4))
                                .ToList().OrderByDescending(f => f.term).FirstOrDefault();
                return data;
            }
            else return null;
        }
        #endregion Metodos
    }
}
