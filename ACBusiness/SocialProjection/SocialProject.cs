﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.SocialProjection
{
    public class SocialProject
    {
        #region Propiedades PRStblProyecto
        public int IDProyecto { get; set; }
        public string NombreProyecto { get; set; }
        public string Asesor { get; set; }
        public string Descripcion { get; set; }
        public int IDEstado { get; set; }
        public string IDPerAcad { get; set; }
        public DateTime FechaCreacion { get; set; }
        #endregion

        #region Propiedades PRStblIntegranteProyecto
        public int IDIntegrante { get; set; }
        public string IDAlumno { get; set; }
        public int IDEstadoIntegrante { get; set; }
        public string Responsable { get; set; }
        public DateTime FechaRegistro { get; set; }
        #endregion

        #region Extras
        public int pidm { get; set; }
        DBOContext bducci = new DBOContext();
        #endregion

        #region Metodos
        public List<SocialProject> getSocialProjectionSocialByStudent(int Pidm) {
            List<SocialProject> list = bducci.prs_tbl_proyecto
                .Join(
                    bducci.prs_tbl_integrante_proyecto,
                    tp => new { IDProyecto = tp.IDProyecto },
                    ti => new { IDProyecto = ti.IDProyecto },
                    (tp, ti) => new { tp, ti }
                )
                .Join(
                    bducci.view_alumno_basico,
                    tp => new { IDAlumno = tp.ti.IDAlumno },
                    da => new { IDAlumno = da.IDAlumno },
                    (tp, da) => new { tp.tp, tp.ti, da }
                )
                .Where(
                    result =>
                        result.tp.IDEstado == 3  //completado
                        && result.ti.IDEstado != 5 // no retirado
                        && result.ti.IDEstado != 7 // no permiso
                        && result.da.IDPersonaN == Pidm
                ).ToList()
                .Select(
                    result => new SocialProject
                    {
                        IDProyecto = result.tp.IDProyecto,
                        NombreProyecto = result.tp.NombreProyecto,
                        Asesor = result.tp.Asesor,
                        Descripcion = result.tp.Descripcion,
                        IDEstado = result.tp.IDEstado,
                        IDPerAcad = result.tp.IDPerAcad,
                        FechaCreacion = result.tp.FechaCreacion,

                        IDIntegrante = result.ti.IDIntegrante,
                        IDAlumno = result.ti.IDAlumno,
                        IDEstadoIntegrante = result.ti.IDEstado,
                        Responsable = result.ti.Responsable,
                        FechaRegistro = result.ti.FechaRegistro,

                        pidm = result.da.IDPersonaN,
                    }
                ).ToList();
            return list;
        }
        #endregion
    }
}
