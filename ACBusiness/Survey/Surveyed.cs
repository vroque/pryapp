﻿namespace ACBusiness.Survey
{
    public class Surveyed
    {
        #region Properties

        public int id { get; set; }
        public decimal pidm { get; set; }
        public bool completed { get; set; }

        #endregion Properties
    }
}