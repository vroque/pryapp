﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Survey
{
    /// <summary>
    /// Encuestas
    /// </summary>
    public class Survey
    {
        #region Properties

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public DateTime begin_date { get; set; }
        public DateTime end_date { get; set; }
        public bool active { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public Survey()
        {
        }

        public Survey(Survey survey)
        {
            id = survey.id;
            name = survey.name;
            description = survey.description;
            url = survey.url;
            begin_date = survey.begin_date;
            end_date = survey.end_date;
            active = survey.active;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Survey list
        /// </summary>
        /// <returns>Survey list</returns>
        public async Task<List<Survey>> GetSurveyListAsync()
        {
            var surveyList = await _nexoContext.cau_survey
                .OrderByDescending(o => o.BeginDate).ThenByDescending(o => o.Id).ToListAsync();

            return surveyList.Select<CAUSurvey, Survey>(s => s).ToList();
        }

        /// <summary>
        /// Create new Survey
        /// </summary>
        /// <returns></returns>
        public async Task<Survey> CreateAsync()
        {
            var survey = new CAUSurvey();
            Compose(ref survey);
            if (survey == null) return null;

            _nexoContext.cau_survey.Add(survey);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? survey : null;
        }

        /// <summary>
        /// Update Survey
        /// </summary>
        /// <returns></returns>
        public async Task<Survey> UpdateAsync()
        {
            var survey = await _nexoContext.cau_survey.FirstOrDefaultAsync(w => w.Id == id);

            if (survey == null) return null;

            Compose(ref survey);
            var i = await _nexoContext.SaveChangesAsync();

            return i == 1 ? survey : null;
        }

        /// <summary>
        /// Delete Survey
        /// </summary>
        /// <param name="surveyId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int surveyId)
        {
            var findSurvey = await _nexoContext.cau_survey.FirstOrDefaultAsync(w => w.Id == surveyId);
            if (findSurvey == null) return false;

            _nexoContext.cau_survey.Remove(findSurvey);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1;
        }

        private void Compose(ref CAUSurvey cauSurvey)
        {
            if (cauSurvey == null) return;

            cauSurvey.Id = id;
            cauSurvey.Name = name;
            cauSurvey.Description = description;
            cauSurvey.Url = url;
            cauSurvey.BeginDate = begin_date;
            cauSurvey.EndDate = end_date;
            cauSurvey.Active = active;
        }

        #endregion Methods

        #region Implicit Operator

        public static implicit operator Survey(CAUSurvey cauSurvey)
        {
            if (cauSurvey == null) return null;

            var survey = new Survey
            {
                id = cauSurvey.Id,
                name = cauSurvey.Name,
                description = cauSurvey.Description,
                url = cauSurvey.Url,
                begin_date = cauSurvey.BeginDate,
                end_date = cauSurvey.EndDate,
                active = cauSurvey.Active
            };

            return survey;
        }

        #endregion Implicit Operator
    }
}