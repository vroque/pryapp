﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACBusiness.Academic;
using ACPermanence.Contexts.BDUCCI;
using ACBusiness.Institutional;

namespace ACBusiness.Admission
{
    /// <summary>
    /// Tipos de Admisión
    /// </summary>
    public class AdmissionType
    {
        #region Properties

        /// <summary>
        /// Modalidad de Postulación (IDModalidadPostu)
        /// </summary>
        public decimal TypeOfIncomeId { get; set; }

        /// <summary>
        /// Modalidad de Postulación (Transformación Recruiter) usado en BANNER
        /// </summary>
        public string TypeOfIncomeRecruiter { get; set; }

        /// <summary>
        /// Nombre Modalidad de Postulación (IDModalidadPostu)
        /// </summary>
        public string TypeOfIncomeName { get; set; }

        /// <summary>
        /// Modalidad (IDEscuelaADM)
        /// </summary>
        public string ModalityId { get; set; }

        /// <summary>
        /// Nombre Modalidad (Nombre de IDEscuelaADM)
        /// </summary>
        public string ModalityName { get; set; }

        private readonly DBOContext _dboContext = new DBOContext();

        #endregion Properties

        #region Methods

        /// <summary>
        /// Verifica si un estudiante UC pertenece a la modalidad PRONABEC.
        /// </summary>
        /// <param name="studentId">Código de estudiante</param>
        /// <param name="schoolId">Código de escuela académica</param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public bool IsPronabec(string studentId, string schoolId)
        {
            List<string> studentIdList = Student.GetAllStudentId(studentId);
            var ctaCorriente = _dboContext.tbl_cta_corriente
                .Where(w => w.IDEscuela == schoolId && studentIdList.Contains(w.IDAlumno))
                .OrderByDescending(o => o.FecInic).FirstOrDefault();

            if (ctaCorriente == null)
            {
                throw new NullReferenceException($"No se encontró información de {studentId} en la escuela {schoolId}");
            }

            return ctaCorriente.IDSeccionC.Substring(3, 2) == "20";
        }

        /// <summary>
        /// Verifica si un estudiante UC pertenece a la modalidad PRONABEC. en un periodo
        /// </summary>
        /// <param name="studentId">Código de estudiante</param>
        /// <param name="schoolId">Código de escuela académica</param>
        /// <param name="term">Periodo Académico</param>
        /// <returns></returns>
        public bool IsPronabecByTerm(string studentId, string schoolId, string term)
        {
            term = Term.toAPEC(term);
            List<string> studentIdList = Student.GetAllStudentId(studentId);
            var ctaCorriente = _dboContext.tbl_cta_corriente
                .Where(w => w.IDEscuela == schoolId && w.IDPerAcad == term && studentIdList.Contains(w.IDAlumno))
                .OrderByDescending(o => o.FecInic).FirstOrDefault();

            if (ctaCorriente == null)
            {
                throw new NullReferenceException(
                    $"No se encontró información de {studentId} en la escuela {schoolId} en el periodo {term}.");
            }

            return ctaCorriente.IDSeccionC.Substring(3, 2) == "20";
        }

        #endregion Methods
    }
}