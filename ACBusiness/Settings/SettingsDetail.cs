using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Settings
{
    public class SettingsDetail
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Order { get; set; }
        public string Type { get; set; }
        public string StrValue { get; set; }
        public decimal? NumberValue { get; set; }
        public DateTime? DateValue { get; set; }
        public int SettingsId { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();
        private const int SettingIdForTermsForEnrollment = 4;
        private const int SettingIdForTermsForExt = 6; // Constante para actividad extracurricular

        #endregion Properties

        #region Constructors

        public SettingsDetail()
        {
        }

        public SettingsDetail(SettingsDetail settingsDetail)
        {
            Id = settingsDetail.Id;
            Name = settingsDetail.Name;
            Description = settingsDetail.Description;
            Order = settingsDetail.Order;
            Type = settingsDetail.Type;
            StrValue = settingsDetail.StrValue;
            NumberValue = settingsDetail.NumberValue;
            DateValue = settingsDetail.DateValue;
            SettingsId = settingsDetail.SettingsId;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all settings details list
        /// </summary>
        /// <returns></returns>
        public async Task<List<SettingsDetail>> GetAsync()
        {
            List<CAUSettingsDetail> settingsDetailList = await _nexoContext.cau_settings_detail.ToListAsync();

            return settingsDetailList.Select(s => (SettingsDetail) s).ToList();
        }

        /// <summary>
        /// Get settings details by Id
        /// </summary>
        /// <param name="settingsDetailId">Settings detail id</param>
        /// <returns>Setting detail</returns>
        public async Task<SettingsDetail> GetByIdAsync(int settingsDetailId)
        {
            var settingsDetail =
                await _nexoContext.cau_settings_detail.FirstOrDefaultAsync(w => w.Id == settingsDetailId);

            return (SettingsDetail) settingsDetail;
        }

        /// <summary>
        /// Get settings detail for terms for enrollment
        /// </summary>
        /// <returns></returns>
        public async Task<SettingsDetail> GetTermsForEnrollment()
        {
            List<CAUSettingsDetail> termsForEnrollment = await _nexoContext.cau_settings_detail
                .Where(w => w.SettingsId == SettingIdForTermsForEnrollment).ToListAsync();

            var lastTermsForEnrollment = termsForEnrollment.OrderByDescending(o => o.Id).FirstOrDefault();

            return (SettingsDetail) lastTermsForEnrollment;
        }

        /// <summary>
        /// Obtiene el detalle para la actividad Extracurricular
        /// </summary>
        /// <returns></returns>
        public async Task<SettingsDetail> GetTermsForExtracurricular()
        {
            List<CAUSettingsDetail> termsForEnrollment = await _nexoContext.cau_settings_detail
                .Where(w => w.SettingsId == SettingIdForTermsForExt).ToListAsync();

            var lastTermsForEnrollment = termsForEnrollment.OrderByDescending(o => o.Id).FirstOrDefault();

            return (SettingsDetail)lastTermsForEnrollment;
        }

        /// <summary>
        /// Create new settings detail
        /// </summary>
        /// <returns>Setting detail saved</returns>
        public async Task<SettingsDetail> CreateAsync()
        {
            var settingsDetail = new CAUSettingsDetail();
            Compose(ref settingsDetail);
            if (settingsDetail == null) return null;

            _nexoContext.cau_settings_detail.Add(settingsDetail);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (SettingsDetail) settingsDetail : null;
        }

        /// <summary>
        /// Update setting detail
        /// </summary>
        /// <returns>Setting detail updated</returns>
        public async Task<SettingsDetail> UpdateAsync()
        {
            var settingsDetail = await _nexoContext.cau_settings_detail.FirstOrDefaultAsync(w => w.Id == Id);

            if (settingsDetail == null) return null;

            Compose(ref settingsDetail);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (SettingsDetail) settingsDetail : null;
        }

        /// <summary>
        /// Delete setting detail
        /// </summary>
        /// <param name="settingsDetailId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int settingsDetailId)
        {
            var settingsDetail =
                await _nexoContext.cau_settings_detail.FirstOrDefaultAsync(w => w.Id == settingsDetailId);

            if (settingsDetail == null) return false;

            _nexoContext.cau_settings_detail.Remove(settingsDetail);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1;
        }

        private void Compose(ref CAUSettingsDetail cauSettingsDetail)
        {
            if (cauSettingsDetail == null) return;

            cauSettingsDetail.Id = Id;
            cauSettingsDetail.Name = Name;
            cauSettingsDetail.Description = Description;
            cauSettingsDetail.Order = Order;
            cauSettingsDetail.Type = Type;
            cauSettingsDetail.StrValue = StrValue;
            cauSettingsDetail.NumberValue = NumberValue;
            cauSettingsDetail.DateValue = DateValue;
            cauSettingsDetail.SettingsId = SettingsId;
        }

        #endregion Methods

        #region Operators

        public static explicit operator SettingsDetail(CAUSettingsDetail detail)
        {
            if (detail == null) return null;

            var classDetail = new SettingsDetail
            {
                Id = detail.Id,
                Name = detail.Name,
                Description = detail.Description,
                Order = detail.Order,
                Type = detail.Type,
                StrValue = detail.StrValue,
                NumberValue = detail.NumberValue,
                DateValue = detail.DateValue,
                SettingsId = detail.SettingsId
            };

            return classDetail;
        }

        #endregion Operators
    }
}