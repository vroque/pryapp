﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Settings
{
    public class SettingsType
    {
        #region Properties

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime update_date { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public SettingsType()
        {
        }

        public SettingsType(SettingsType settingsType)
        {
            id = settingsType.id;
            name = settingsType.name;
            description = settingsType.description;
            update_date = DateTime.Now;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all settings type list
        /// </summary>
        /// <returns></returns>
        public async Task<List<SettingsType>> GetSettingsTypeListAsync()
        {
            var settingsTypeList = await _nexoContext.cau_settings_type.ToListAsync();

            return settingsTypeList.Select<CAUSettingsType, SettingsType>(s => s).ToList();
        }

        /// <summary>
        /// Get setting type by id
        /// </summary>
        /// <param name="settingTypeId">Setting type id</param>
        /// <returns>Setting type</returns>
        public SettingsType GetById(int settingTypeId)
        {
            var settingsType = _nexoContext.cau_settings_type.FirstOrDefault(w => w.Id == settingTypeId);

            return settingsType;
        }

        /// <summary>
        /// Create new setting type
        /// </summary>
        /// <returns>Setting type saved</returns>
        public async Task<SettingsType> CreateAsync()
        {
            var cauSettingsType = new CAUSettingsType();
            Compose(ref cauSettingsType);
            if (cauSettingsType == null) return null;

            _nexoContext.cau_settings_type.Add(cauSettingsType);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? cauSettingsType : null;
        }

        /// <summary>
        /// Update setting type
        /// </summary>
        /// <returns>Setting type updated</returns>
        public async Task<SettingsType> UpdateAsync()
        {
            var settingsType = await _nexoContext.cau_settings_type.FirstOrDefaultAsync(w => w.Id == id);

            if (settingsType == null) return null;

            Compose(ref settingsType);
            var i = await _nexoContext.SaveChangesAsync();

            return i == 1 ? settingsType : null;
        }

        /// <summary>
        /// Delete setting type
        /// </summary>
        /// <param name="settingTypeId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int settingTypeId)
        {
            var settingsType = await _nexoContext.cau_settings_type.FirstOrDefaultAsync(w => w.Id == settingTypeId);
            if (settingsType == null) return false;

            _nexoContext.cau_settings_type.Remove(settingsType);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1;
        }

        private void Compose(ref CAUSettingsType cauSettingsType)
        {
            if (cauSettingsType == null) return;

            cauSettingsType.Id = id;
            cauSettingsType.Name = name;
            cauSettingsType.Description = description;
            cauSettingsType.UpdateDate = update_date;
        }

        #endregion Methods

        #region Implicit Operators

        public static implicit operator SettingsType(CAUSettingsType cauSettingsType)
        {
            if (cauSettingsType == null) return null;

            var settingsType = new SettingsType
            {
                id = cauSettingsType.Id,
                name = cauSettingsType.Name,
                description = cauSettingsType.Description,
                update_date = cauSettingsType.UpdateDate
            };

            return settingsType;
        }

        #endregion Implicit Operators
    }
}