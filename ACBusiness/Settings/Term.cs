﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Settings
{
    public class Term
    {
        #region Properties

        public string term { get; set; }
        public string term_description { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public DateTime update_date { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public Term()
        {
        }

        public Term(Term t)
        {
            term = t.term;
            term_description = t.term_description;
            start_date = t.start_date;
            end_date = t.end_date;
            update_date = DateTime.Now;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all terms
        /// </summary>
        /// <returns></returns>
        public async Task<List<Term>> GetAllTermListAsync()
        {
            var termList = await _nexoContext.cau_term
                .OrderByDescending(o => o.StartDate).ThenByDescending(o => o.Term).ToListAsync();

            return termList.Select<CAUTerm, Term>(s => s).ToList();
        }

        /// <summary>
        /// Get term by term
        /// </summary>
        /// <param name="t">term</param>
        /// <returns>Term</returns>
        public async Task<Term> GetByTerm(string t)
        {
            var result = await _nexoContext.cau_term.FirstOrDefaultAsync(w => w.Term == t);

            return result;
        }

        /// <summary>
        /// Create new term
        /// </summary>
        /// <returns>Term saved</returns>
        public async Task<Term> CreateAsync()
        {
            var t = new CAUTerm();
            Compose(ref t);
            if (t == null) return null;

            _nexoContext.cau_term.Add(t);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? t : null;
        }

        /// <summary>
        /// Update term
        /// </summary>
        /// <returns>Term updated</returns>
        public async Task<Term> UpdateAsync()
        {
            var t = await _nexoContext.cau_term.FirstOrDefaultAsync(w => w.Term == term);

            if (t == null) return null;

            Compose(ref t);

            var i = await _nexoContext.SaveChangesAsync();

            return i == 1 ? t : null;
        }

        /// <summary>
        /// Delete term
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(string t)
        {
            var findTerm = await _nexoContext.cau_term.FirstOrDefaultAsync(w => w.Term == t);
            if (findTerm == null) return false;

            _nexoContext.cau_term.Remove(findTerm);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1;
        }

        private void Compose(ref CAUTerm cauTerm)
        {
            if (cauTerm == null) return;

            cauTerm.Term = term;
            cauTerm.TermDescription = term_description;
            cauTerm.StartDate = start_date;
            cauTerm.EndDate = end_date;
            cauTerm.UpdateDate = update_date;
        }

        #endregion Methods

        #region Implicit Operators

        public static implicit operator Term(CAUTerm cauTerm)
        {
            if (cauTerm == null) return null;

            var term = new Term
            {
                term = cauTerm.Term,
                term_description = cauTerm.TermDescription,
                start_date = cauTerm.StartDate,
                end_date = cauTerm.EndDate,
                update_date = cauTerm.UpdateDate
            };

            return term;
        }

        #endregion Implicit Operators
    }
}