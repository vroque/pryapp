﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Settings
{
    public class Settings
    {
        #region Properties

        public int Id { get; set; }
        public string Abbr { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public SettingsType SettingsType { get; set; }
        public bool Active { get; set; }
        public bool HasDetail { get; set; }
        public DateTime UpdateDate { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public Settings()
        {
        }

        public Settings(Settings settings)
        {
            Id = settings.Id;
            Abbr = settings.Abbr;
            Name = settings.Name;
            Description = settings.Description;
            SettingsType = settings.SettingsType;
            Active = settings.Active;
            HasDetail = settings.HasDetail;
            UpdateDate = DateTime.Now;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all settings list
        /// </summary>
        /// <returns></returns>
        public async Task<List<Settings>> GetSettingsListAsync()
        {
            List<CAUSettings> settingsList = await _nexoContext.cau_settings.ToListAsync();

            return settingsList.Select(s => (Settings) s).ToList();
        }

        /// <summary>
        /// Get setting by id
        /// </summary>
        /// <param name="settingId">Setting id</param>
        /// <returns>Setting</returns>
        public Settings GetById(int settingId)
        {
            var setting = _nexoContext.cau_settings.FirstOrDefault(w => w.Id == settingId);

            return (Settings) setting;
        }

        /// <summary>
        /// Get active settings by abbreviation and setting type id
        /// </summary>
        /// <param name="abbr">abbreviation</param>
        /// <param name="settingTypeId">setting type</param>
        /// <returns>Settings</returns>
        public static async Task<List<Settings>> GetActiveSettingsByAbbrAndBySettingTypeId(string abbr,
            int settingTypeId)
        {
            var nexoContext = new NEXOContext();
            List<CAUSettings> settings = await nexoContext.cau_settings
                .Where(w => w.Abbr == abbr && w.Active && w.SettingsTypeId == settingTypeId).ToListAsync();
            List<Settings> settingList = settings.Select(s => (Settings) s).ToList();
            return settingList;
        }

        /// <summary>
        /// Create new setting
        /// </summary>
        /// <returns>Setting saved</returns>
        public async Task<Settings> CreateAsync()
        {
            var cauSettings = new CAUSettings();
            Compose(ref cauSettings);
            if (cauSettings == null) return null;

            _nexoContext.cau_settings.Add(cauSettings);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (Settings) cauSettings : null;
        }

        /// <summary>
        /// Update setting
        /// </summary>
        /// <returns>Setting updated</returns>
        public async Task<Settings> UpdateAsync()
        {
            var setting = await _nexoContext.cau_settings.FirstOrDefaultAsync(w => w.Id == Id);

            if (setting == null) return null;

            Compose(ref setting);
            var i = await _nexoContext.SaveChangesAsync();

            return i == 1 ? (Settings) setting : null;
        }

        /// <summary>
        /// Delete setting type
        /// </summary>
        /// <param name="settingId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int settingId)
        {
            var setting = await _nexoContext.cau_settings.FirstOrDefaultAsync(w => w.Id == settingId);
            if (setting == null) return false;

            _nexoContext.cau_settings.Remove(setting);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1;
        }

        private void Compose(ref CAUSettings cauSettings)
        {
            if (cauSettings == null) return;

            cauSettings.Id = Id;
            cauSettings.Abbr = Abbr;
            cauSettings.Name = Name;
            cauSettings.Description = Description;
            cauSettings.SettingsTypeId = SettingsType.id;
            cauSettings.Active = Active;
            cauSettings.HasDetail = HasDetail;
            cauSettings.UpdateDate = UpdateDate;
        }

        #endregion Methods

        #region Operators

        public static explicit operator Settings(CAUSettings cauSettings)
        {
            if (cauSettings == null) return null;

            var settings = new Settings
            {
                Id = cauSettings.Id,
                Abbr = cauSettings.Abbr,
                Name = cauSettings.Name,
                Description = cauSettings.Description,
                SettingsType = new SettingsType().GetById(cauSettings.SettingsTypeId),
                Active = cauSettings.Active,
                HasDetail = cauSettings.HasDetail,
                UpdateDate = cauSettings.UpdateDate
            };

            return settings;
        }

        #endregion Operators
    }
}