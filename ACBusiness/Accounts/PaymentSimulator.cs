﻿using ACBusiness.Academic;
using ACBusiness.Economic;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BDUCCI;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using ACPermanence.DataBases.DBUCCI.DBO;

namespace ACBusiness.Accounts
{
    public class PaymentSimulator
    {
        #region properties

        public string department { get; set; }
        public string scala { get; set; }
        public string term { get; set; }
        public double credit_cost { get; set; }
        public double enrollment_cost { get; set; }
        public double student_card_cost { get; set; }
        public double health_insurance_cost { get; set; }
        public int StudyPlan { get; set; }
        // Informacion de c01 c02 c03 c04 c05
        public List<EconomicStatus> DataCuotas { get; set; }
        private readonly DBOContext _apec = new DBOContext();

        private readonly Dictionary<string, Dictionary<string, string>>
            _pensionCatSuffix = new Dictionary<string, Dictionary<string, string>>
            {
                {
                    "UREG", new Dictionary<string, string>
                    {
                        {"HYO", "01"},
                        {"ARQ", "16"},
                    }
                },
                {
                    "UPGT", new Dictionary<string, string>
                    {
                        {"HYO", "03"},
                        {"ARQ", "10"},
                        {"CUZ", "14"},
                    }
                },
                {
                    "UVIR", new Dictionary<string, string>
                    {
                        {"VIR", "08"},
                    }
                },
            };

        private readonly string[] _aditionalConceptList = {"CAR", "SME", "C00", "C01", "C02", "C03", "C04", "C05" };
        private readonly DBOContext _dboContext = new DBOContext();

        #endregion properties

        #region methods

        /// <summary>
        /// necesitamos obtener:
        ///     Escala
        ///     Costo de Escala
        ///     Costo de matricula
        ///     costo de carne de estudiante
        ///     costo de seguro medico
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        public PaymentSimulator Get(Student student)
        {
            // Scala
            var studentValues = Scala.GetScalaAndTermAndCronogramaByStudent(student.id);
            string studentScala = studentValues.Scala;
            string termApec = Term.toAPEC(studentValues.Term, student.profile.department);
            string termBanner = Term.toBanner(studentValues.Term, student.profile.department);
            var suffix = studentValues.Chronogram;
            var path = $"___{suffix}";
            var seccion = _apec.tbl_seccion_c
                .FirstOrDefault(f => f.IDDependencia == student.profile.div &&
                                     f.IDsede == student.profile.campus.code &&
                                     f.IDPerAcad == termApec &&
                                     f.IDSeccionC.Length == 5 &&
                                     SqlFunctions.PatIndex(path, f.IDSeccionC) > 0 &&
                                     f.IDEscuela == student.profile.program.id);

            if (seccion == null)
            {
                throw new NullReferenceException(
                    $"No existe registro el tblSeccionC para {student.profile.div} en {student.profile.campus.code}, periodo {termApec}, programa {student.profile.program.id}");
            }

            // costo por credito
            var categoriaPens = _apec.tbl_categoria_pension.FirstOrDefault(f =>
                f.IDDependencia == student.profile.div
                && f.IDSede == student.profile.campus.code // codigo apec
                && f.IDEscuela == student.profile.program.id
                && f.IDCategoriaPens == seccion.IDCategoriaPens
                && f.IDEscala == studentScala
            );

            if (categoriaPens == null)
            {
                throw new NullReferenceException(
                    $"No existe registro de categoría de pensión para {seccion.IDCategoriaPens} en la escala {studentScala}");
            }

            var studentCreditCost = categoriaPens.PensCred;

            if (student.profile.department == "UREG") studentCreditCost = studentCreditCost * 5;
            else studentCreditCost = studentCreditCost * 4;

            //COSTOS ADICIONALES
            List<DBOTblCtaCorriente> conceptos = _apec.tbl_cta_corriente
                .Where(f => 
                            f.IDDependencia == student.profile.div &&
                            f.IDSede == student.profile.campus.code &&
                            f.IDEscuela == student.profile.program.id &&
                            f.IDPerAcad == termApec &&
                            _aditionalConceptList.Contains(f.IDConcepto) &&
                            f.IDAlumno == student.id)
                .ToList();

            var conceptoMatricula = conceptos.FirstOrDefault(f => f.IDConcepto == "C00");
            var conceptoSeguroMedico = conceptos.FirstOrDefault(f => f.IDConcepto == "SME");
            var dataCuotas = conceptos.Where(f => (new string[] { "C01", "C02", "C03", "C04", "C05" }).Contains(f.IDConcepto)).ToList()
                .Select(
                    result => new EconomicStatus
                    {
                        div = result.IDDependencia,
                        term = result.IDPerAcad,
                        campus = result.IDSede,
                        student_id = result.IDAlumno,
                        concept_id = result.IDConcepto,
                        ammount = result.Cargo,
                        discount = result.Descuento,
                        interest = result.DescMora,
                        deposit = result.Abono,
                        total = result.Deuda,
                        paymen_date = result.FecCargo,
                        extension_date = result.FecProrroga,
                    }
                ).ToList();

            var studentEnrollmentCost = conceptoMatricula?.Cargo ?? 0;
            var studentHealthCost = conceptoSeguroMedico?.Cargo ?? 0;
            var ps = new PaymentSimulator
            {
                credit_cost = studentCreditCost,
                department = student.profile.department,
                enrollment_cost = studentEnrollmentCost,
                health_insurance_cost = studentHealthCost,
                term = termBanner,
                scala = studentScala,
                StudyPlan = int.Parse(student.profile.term_catalg),
                DataCuotas = dataCuotas
            };

            return ps;
        }

        /// <summary>
        /// Código de cronograma para construir cronograma de pago de un estudiante UC
        /// </summary>
        /// <param name="student">Datos del estudiante</param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        private string GetCronograma(Student student)
        {
            var nuevoPlan = int.Parse(student.profile.term_catalg) >= 201810 ? "1" : "0";
            var pronabec = new Admission.AdmissionType().IsPronabec(student.id, student.profile.program.id)
                ? 1
                : 0;
            var cronograma = _dboContext.tbl_cronogramas
                .FirstOrDefault(w => w.IDDepartamento == student.profile.department &&
                                     w.IDCampus == student.profile.campus.id &&
                                     w.Pronabec == pronabec &&
                                     w.Activo == 1 &&
                                     w.NuevoPlan == nuevoPlan);
            if (cronograma == null)
            {
                throw new NullReferenceException($"No se encontró cronograma de estudiante {student.id}.");
            }

            return cronograma.Cronograma;
        }

        #endregion methods
    }
}