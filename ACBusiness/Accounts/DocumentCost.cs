﻿using ACBusiness.Academic;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System.Linq;

namespace ACBusiness.Accounts
{
    /// <summary>
    /// Tabla de costos de los documentos
    /// </summary>
    public class DocumentCost
    {
        #region properties

        public int id { get; set; }
        protected string divs { get; set; }
        protected string department { get; set; }
        protected string program { get; set; }
        public double amount { get; set; }
        public string account { get; set; }
        public string costcenter { get; set; }
        public string service { get; set; }
        public double processcost { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion properties

        #region methods

        /// <summary>
        /// Costo del documento
        /// </summary>
        /// <param name="id"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        public static DocumentCost Get(int id, AcademicProfile profile)
        {
            var nexoContext = new NEXOContext();
            DocumentCost dc = nexoContext.cau_document_costs
                .FirstOrDefault(x => x.documentID == id &&
                                     x.divs == profile.div &&
                                     x.department == profile.department &&
                                     x.program == profile.program.id);
            return dc;
        }

        /// <summary>
        /// Costo de documento CIC
        /// </summary>
        /// <param name="documentId">Tipo de documento</param>
        /// <returns></returns>
        public DocumentCost GetCicDocumentCost(int documentId)
        {
            var documentCost = _nexoContext.cau_document_costs.FirstOrDefault(w => w.documentID == documentId);
            return documentCost;
        }

        #endregion methods

        #region implicit operators

        public static implicit operator DocumentCost(CAUTblDdocumentCost obj)
        {
            if (obj == null) return null;
            var dc = new DocumentCost
            {
                id = obj.documentID,
                divs = obj.divs,
                department = obj.department,
                program = obj.program,
                amount = obj.cost,
                account = obj.account,
                costcenter = obj.costcenter,
                service = obj.service,
                processcost = obj.processcost
            };
            return dc;
        }

        #endregion implicit operators
    }
}