﻿using ACPermanence.DataBases.BANNER.TZKCDAA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.ATE;
using ACBusiness.AtentionCenter;
using ACBusiness.Institutional;
using _corp = ACTools.Constants.InstitucionalConstants;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACBusiness.Accounts
{
    /// <author> Arturo Bolaños</author>
    /// <date>18/09/2016</date>
    /// <summary>
    /// Clase para Deudas Economicas
    /// </summary>
    public class Debt
    {
        #region properties
        //pidm
        public decimal person_id { get; set; }
        public string year { get; set; }
        //part term
        public string part { get; set; }
        public string concept_id { get; set; }
        public string description { get; set; }
        //documento number of the person
        public string document_number { get; set; }
        //Amount to be paid
        public decimal ammount { get; set; }
        //amount already paid
        public decimal deposit { get; set; }
        //total of the debt
        public decimal total { get; set; }
        //mora
        public decimal interest { get; set; }
        //multa
        public decimal penalty { get; set; }
        public decimal discount { get; set; }

        private const string UC = "UCCI";
        private const string BACHILLER = "BA";
        private const string BACHILLER_CONCEPT_ID = "C07";
        private const string GRADOS_Y_TITULOS = "GYT";

        #endregion properties
        #region methods

        /// <summary>
        /// Lista de deudas economicas de un alumno
        /// segun su codigo de persona (PIDM)
        /// </summary>
        /// <param name="person_id">Codigo de Persona</param>
        /// <returns>Lista de deudas</returns>
        public async Task<List<Debt>> query(decimal person_id)
        {
            // Student student = new Student(person_id);
            // for DBUCCI
            List<Debt> debts = new List<Debt>();
            DBOContext context = new DBOContext();
            await Task.Run(() =>
            {
                Personal.Person objPerson = new Personal.Person();
                List<SPListDebt> result = context.sp_ListDebt(_acad.DEFAULT_DIV, "", Student.getStudentCodeFromPidm(person_id))                
                    .ToList();
                debts = result
                    .Select<SPListDebt, Debt>(f => f)
                    .ToList();
            });
            return debts;

        }
        
        /// <summary>
        /// insert la deuda de una solicitud (por ahora en DBUCCI)
        /// segun un objeto ACrequest
        /// </summary>
        /// <param name="request">objeto ACrequest</param>
        /// <returns>Lista de deudas(List<sp_InsertDebt>) segun DBUCCI (se tiene que cambiar para que devuelva una List<Debt> )</returns>
        public static async Task<List<sp_InsertDebt>> insert_request_debt( ACRequest request) {
            //for DBUCCCI
            AcademicProfile profile = new AcademicProfile();
            profile.div = request.divs;
            profile.department = request.department;
            profile.program = new Program();
            profile.program.id = request.program;

            DocumentCost cd = DocumentCost.Get(request.document_id, profile);

            ACDocument doc = ACDocument.get(request.document_id);
            string campus_dbucci=Campus.get(request.campus).code;
            DBOContext context = new DBOContext();

            List<sp_InsertDebt> result = context.sp_InsertDebt(request.student_id, profile.program.id,
                Institutional.Term.toAPEC(request.term), request.id, cd.account, cd.costcenter, cd.service,
                request.document_id, _corp.UC, campus_dbucci, request.cost, doc.name, cd.processcost).ToList();
            // update tblrequest.concepid with sp_insertDebt.IDConcepto
            request.concept_id = result.First().IDConcepto;
            int update = await request.update();
            if (update == 0) {
                throw new Exception("No se guardo el idconcepto de la deuda en la solicitud");
            }
            return result;
            //for banner
            /*
             * 
             * 
             */
        }

        /// <summary>
        /// Verifica si un estudiante ya realizó el pago por trámite de bachiller
        /// </summary>
        /// <param name="student_id">Código de estudiante</param>
        /// <param name="school_id">Escuela académica</param>
        /// <returns>Estado de pago por trámite de bachiller</returns>
        public static bool isBachillerAccountPaid(string student_id, string school_id)
        {
            DBOContext dbo_context = new DBOContext();
            List<string> student_id_list = Student.GetAllStudentId(student_id);
            var dbo_tbl_cta_corriente =
                dbo_context.tbl_cta_corriente.Where(w => student_id_list.Contains(w.IDAlumno) &&
                                                         w.IDDependencia == UC &&
                                                         w.IDConcepto == BACHILLER_CONCEPT_ID &&
                                                         w.IDEscuela == GRADOS_Y_TITULOS &&
                                                         w.IDSeccionC.Substring(0, 2) == BACHILLER && w.Abono > 0 &&
                                                         w.IDSeccionC.Substring(w.IDSeccionC.Length - 3) == school_id)
                    .OrderByDescending(o => o.FecInic).ThenByDescending(o => o.FechaActualizacion).ToList();
            return dbo_tbl_cta_corriente.Any();
        }

        /// <summary>
        /// Verifica si un estudiante tiene restricción por deuda
        /// </summary>
        /// <param name="student">Objeto estudiante</param>
        /// <param name="term">Periodo Academico</param>
        /// <returns>False = no tiene restricción; True= Tiene restricción</returns>
        public async Task<bool> restrictionByDebt(Student student, string term) {
            ACTerm ac_term = new ACTerm();
            ACTerm object_term = new ACTerm().getCurrent(student.profile.campus.id, student.profile.department);

            bool validate = false;
            string currentTerm = await ac_term.getCurrentAsync(student.profile.campus.id, student.profile.department);

            if (currentTerm == term)
            {
                List<Debt> debts = await query(student.person_id);
                if (debts.Count > 0)
                {
                    validate = true;
                    List<string> conceptsID = debts.Select(f => f.concept_id).Distinct().ToList();
                    bool pronabec = new Admission.AdmissionType().IsPronabecByTerm(student.id, student.profile.program.id, term);
                    if (pronabec && !conceptsID.Contains("C06") && !conceptsID.Contains("C07")
                        && !conceptsID.Contains("C08") && !conceptsID.Contains("C09") && !conceptsID.Contains("C10"))
                    {
                        validate = false;
                    }
                }
            }                   
            return validate;
        }

        #endregion methods
       
        #region implicite operators
        public static implicit operator Debt(PCALCDEUDAALUMNO obj)
        {
            Debt debt = new Debt();
            debt.person_id = obj.PIDM;
            debt.year = obj.Anio;
            debt.part = obj.Periodo;
            debt.concept_id = obj.IDConcepto;
            debt.description = obj.Descripcion;
            debt.document_number = obj.NumDocumento;
            debt.ammount = obj.Deuda;
            debt.interest = obj.Mora;
            debt.penalty = obj.Multa;
            return debt;
        }

        public static implicit operator Debt(SPListDebt obj)
        {
            Debt debt = new Debt();
            debt.year = obj.FecCargo.Year.ToString();           
            debt.concept_id = obj.IDConcepto;
            debt.description = obj.Descripcion;
            debt.document_number = obj.IDAlumno;
            debt.total = Convert.ToDecimal(obj.Cargo);
            debt.ammount = Convert.ToDecimal(obj.Deuda);
            debt.interest = Convert.ToDecimal(obj.DescMora);
            debt.discount = Convert.ToDecimal(obj.Descuento);

            return debt;
        }
        #endregion implicite operators
    }
}

