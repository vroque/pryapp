﻿using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BDUCCI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ACBusiness.Accounts
{
    public class EconomicStatus
    {
        #region properties

        public string div { get; set; }
        public string campus { get; set; }
        public string term { get; set; }
        /// <summary>
        /// código persona
        /// </summary>
        public string concept_id { get; set; }
        public string description { get; set; }
        //documento number of the person
        public string student_id { get; set; }
        /// <summary>
        /// código banco
        /// </summary>
        public decimal code_banck { get; set; }
        //Amount to be paid
        public Double ammount { get; set; }
        //amount already paid
        public Double deposit { get; set; }
        //total of the debt
        public Double total { get; set; }
        //mora
        public Double interest { get; set; }
        //Fecha Cargo
        public DateTime? paymen_date { get; set; }
        //Fecha Prorroga
        public DateTime? extension_date { get; set; }
        public Double discount { get; set; }
        public bool DocumentReceivable { get; set; }
        public string IDSeccionC { get; set; }
        public DateTime? FecInic { get; set; }
        /// <summary>
        /// Id de pago online
        /// </summary>
        public string payment_online_id { get; set; }

        public List<EconomicStatus> details_grouped_gebts { get; set; }

        DBOContext _db = new DBOContext();
        #endregion properties

        #region Metodos

        public async Task<List<EconomicStatus>> getGroupedDebts(string div, string term, int pidm) {
            List<EconomicStatus> list_grouped_debts = _db.get_grouped_debts(div, term, pidm)
                .ToList()
                .Select(
                    result => new EconomicStatus
                    {
                        div = result.IDDependencia,
                        term = result.IDPerAcad,
                        campus = result.IDSede,
                        student_id = result.IDAlumno,
                        concept_id = result.IDGrupoConcepto,
                        code_banck = result.CodigoBanco,
                        description = result.Descripcion,
                        ammount = result.Cargo,
                        discount = result.Descuento,
                        interest = result.DescMora,
                        deposit = result.Abono,
                        total = result.Deuda,
                        paymen_date = result.FecCargo,
                        extension_date = result.FecProrroga,
                        IDSeccionC = result.IDSeccionC,
                        FecInic = result.FecInic,
                        DocumentReceivable = result.DocPorCobrar
                    }
                ).ToList();


            if (list_grouped_debts != null && list_grouped_debts.Count > 0)
            {
                List<decimal> code_banck_debt = list_grouped_debts.Select(f => f.code_banck).Distinct().ToList();

                List<EconomicStatus> details_debts = _db.tbl_cta_corriente
                    .Join(
                        _db.view_alumno_basico,
                        cta => new { cta.IDAlumno },
                        vab => new { vab.IDAlumno },
                        (cta, vab) => new { cta, vab }
                    ).Join(
                        _db.tbl_conceptos,
                        cta => new { cta.cta.IDDependencia, cta.cta.IDSede, cta.cta.IDEscuela, cta.cta.IDPerAcad, cta.cta.IDSeccionC, cta.cta.FecInic, cta.cta.IDConcepto },
                        tc => new { tc.IDDependencia, tc.IDSede, tc.IDEscuela, tc.IDPerAcad, tc.IDSeccionC, tc.FecInic, tc.IDConcepto },
                        (cta, tc) => new { cta.cta, cta.vab, tc }
                    ).Join(
                        _db.tbl_grupo_conceptos,
                        tc => new { tc.tc.IDDependencia, tc.tc.IDSede, tc.tc.IDPerAcad, tc.tc.IDSeccionC, tc.tc.FecInic, tc.tc.IDGrupoConcepto },
                        tgc => new { tgc.IDDependencia, tgc.IDSede, tgc.IDPerAcad, tgc.IDSeccionC, tgc.FecInic, tgc.IDGrupoConcepto },
                        (tc, tgc) => new { tc.cta, tc.vab, tc.tc, tgc }
                    ).Where(
                        result =>
                            code_banck_debt.Contains(result.tgc.CodigoBanco)
                            && result.vab.IDPersonaN == pidm
                            && result.cta.Deuda > 0
                    ).ToList().Select(
                        result => new EconomicStatus
                        {
                            div = result.cta.IDDependencia,
                            term = result.cta.IDPerAcad,
                            campus = result.cta.IDSede,
                            student_id = result.cta.IDAlumno,
                            concept_id = result.cta.IDConcepto,
                            code_banck = result.tgc.CodigoBanco,
                            description = result.tc.Descripcion,
                            ammount = result.cta.Cargo,
                            discount = result.cta.Descuento,
                            interest = result.cta.DescMora,
                            deposit = result.cta.Abono,
                            total = result.cta.Deuda,
                            paymen_date = result.cta.FecCargo,
                            extension_date = result.cta.FecProrroga,
                        }
                    ).ToList();

                //list_grouped_debts.ForEach(item => item.details_grouped_gebts = details_debts.FirstOrDefault(item2 => item2.code_banck == item.code_banck));
                
                foreach (EconomicStatus item in list_grouped_debts)
                {
                    item.details_grouped_gebts = new List<EconomicStatus>();
                    foreach (EconomicStatus item2 in details_debts)
                    {
                        if (item.code_banck == item2.code_banck)
                        {
                            item.details_grouped_gebts.Add(item2);
                        }

                    }

                }
            }


            return list_grouped_debts;
        }

        public async Task<List<EconomicStatus>> getPaymets(string div, string term, int pidm)
        {
            List<EconomicStatus> payments_list = _db.tbl_cta_corriente.Join(
                   _db.view_alumno_basico,
                   cta => new { cta.IDAlumno },
                   vab => new { vab.IDAlumno },
                   (cta, vab) => new { cta, vab }
                ).Join(
                    _db.tbl_conceptos,
                    union => new { union.cta.IDDependencia, union.cta.IDSede, union.cta.IDPerAcad, union.cta.IDSeccionC, union.cta.FecInic, union.cta.IDConcepto },
                    con => new { con.IDDependencia, con.IDSede, con.IDPerAcad, con.IDSeccionC, con.FecInic, con.IDConcepto },
                    (union, con) => new { union.cta, con, union.vab }
                ).Where(result =>
                   result.cta.IDDependencia == div &&
                   result.cta.IDPerAcad == term &&
                   result.vab.IDPersonaN == pidm &&
                   result.cta.Deuda <= 0 &&
                   result.cta.Abono > 0
                ).ToList()
                .OrderBy(result =>
                    result.cta.FecCargo
                ).ThenBy(result =>
                   result.con.Orden
                ).Select(result => new EconomicStatus
                {
                    student_id = result.cta.IDAlumno,
                    concept_id = result.cta.IDConcepto,
                    description = result.con.Descripcion,//(result.con.DescripcionPreM == null) ? result.con.Descripcion : result.con.DescripcionPreM,
                    ammount = result.cta.Cargo,
                    discount = result.cta.Descuento,
                    interest = result.cta.DescMora,
                    deposit = result.cta.Abono,
                    total = result.cta.Deuda,
                    paymen_date = result.cta.FecCargo,
                    extension_date = result.cta.FecProrroga,
                    div = result.cta.IDDependencia,
                    campus = result.cta.IDSede,
                    IDSeccionC = result.cta.IDSeccionC,
                    FecInic = result.cta.FecInic,
                    term = result.cta.IDPerAcad,
                    //person_id = result.vab.IDPersonaN
                }).ToList();
            return payments_list;

        }


        public async Task<EconomicStatus> getPaymentOnlineID(EconomicStatus debt) {
            // tipo 10 es agrupado
            EconomicStatus data = _db.sp_crear_id_pagoonline(debt.div, debt.campus, debt.term, debt.student_id, debt.concept_id, "10", debt.total, debt.code_banck.ToString())
                .ToList()
                .Select(
                    result => new EconomicStatus
                    {
                        payment_online_id = result.LastIDPago,
                    }
                ).FirstOrDefault();
            return data;
        }


        public async Task<List<string>> getTermsEconomicStatus(string div, int pidm) {
            List<string> terms = _db.tbl_cta_corriente.Join(
                   _db.view_alumno_basico,
                   cta => new { cta.IDAlumno },
                   vab => new { vab.IDAlumno },
                   (cta, vab) => new { cta, vab }
                ).Where(result =>
                   result.cta.IDDependencia == div &&
                   result.vab.IDPersonaN == pidm &&
                   result.cta.Cargo > 0
                ).Select(result => result.cta.IDPerAcad)
                .ToList()
                .Distinct()
                .Select(result => Term.toBanner(result))
                .OrderByDescending(result => result).ToList();

            return terms;
        }
        #endregion
    }
}
