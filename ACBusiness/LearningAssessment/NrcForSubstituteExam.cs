﻿using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.BANNER;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.OEA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _learning = ACTools.Constants.LearningAssessmentConstants;
using _acad = ACTools.Constants.AcademicConstants;
namespace ACBusiness.LearningAssessment
{
    public class NrcForSubstituteExam
    {
        #region Propiedades
        public long Id { get; set; }
        public string Department { get; set; }
        public string Campus { get; set; }
        public string Term { get; set; }
        public string SubjetID { get; set; }
        public string Credits { get; set; }
        public string Section { get; set; }
        public string Nrc { get; set; }
        public string PartTerm { get; set; }
        public string Module { get; set; }
        public decimal? TeacherPidm { get; set; }
        public string SubjetName { get; set; }
        public bool Active { get; set; }
        public bool HasSusti { get; set; }
        public string Author { get; set; }
        public DateTime DateCreated { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string Comment { get; set; }
        public List<NrcProgrammned> Scheduled { get; set; }
        public Absence AbsenceNrc { get; set; }
        public NrcProgrammned ScheduledSelected { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();
        private readonly BNContext _bnContext = new BNContext();
        #endregion Propiedades

        #region Metodos

        /// <summary>
        /// Obtener detalles de nrcs de tablas de bdintbanner
        /// </summary>
        /// <param name="department">Modalidad</param>
        /// <param name="term">Periodo</param>
        /// <param name="listNrcs">Lista de Nrcs</param>
        /// <returns></returns>
        public async Task<List<NrcForSubstituteExam>> GetListNrcsOfSaved(string department, string term, List<string> listNrcs, bool active) {
            List<NrcForSubstituteExam> data = _nexoContext.oea_Nrc_for_substitute_exam
                            .Where(
                                item =>
                                    item.Term == term
                                    && listNrcs.Contains(item.Nrc)
                                    && ((!active && item.Department == department) || item.Active == active)
                            ).ToList().OrderBy(order => order.SubjetName)
                            .Select(
                                result => (NrcForSubstituteExam)(result)).ToList();
            return data;
        }
        /// <summary>
        /// Obtener detalles de nrcs de vista de banner
        /// </summary>
        /// <param name="department">Modalidad</param>
        /// <param name="listNrcs">Lista de Nrcs</param>
        /// <returns></returns>
        public async Task<List<NrcForSubstituteExam>> GetListNrcsOfViewsBanner(string department, string term, List<string> listNrcs)
        {
            List<NrcForSubstituteExam> data = _bnContext.view_SZVNPES
                .Where(
                    item =>
                        item.IDDEPENDENCIA == _acad.DEFAULT_DIV
                        && item.MODALIDAD != null
                        && item.PERIODO != null
                        && item.PARTEPERIODO != null
                        && item.MODULO != null
                        && item.MODALIDAD == department
                        && item.PERIODO == term
                        && listNrcs.Contains(item.NRC)

                ).ToList().Select(
                    result => new NrcForSubstituteExam
                    {
                        Department = result.MODALIDAD,
                        Campus = result.IDSEDE,
                        Term = result.PERIODO,
                        SubjetID = result.IDASIGNATURA,
                        Credits = result.CREDITOS,
                        Section = result.SECCION,
                        Nrc = result.NRC,
                        PartTerm = result.PARTEPERIODO,
                        Module = result.MODULO,
                        TeacherPidm = result.PIDM_DOCENTE,
                        SubjetName = result.DESCASIGNATURA
                    }
                ).ToList();
            return data;
        }

        /// <summary>
        /// Obtener Lista de Nrc Programados con sus fechas por Periodo y lista de Nrc
        /// </summary>
        /// <param name="term">Periodo</param>
        /// <param name="Nrcs">Lista de Nrcs</param>
        /// <param name="isPublic"> Validar si es publico?(fechas de NRC)</param>
        /// <param name="active">Validar activos?</param>
        /// <returns></returns>
        public async Task<List<NrcForSubstituteExam>> GetListNrcForProgrammnedByTermAndNrcs(string department, string term, List<string> nrcs, bool isPublic, bool active) {
            List<NrcForSubstituteExam> data = (await GetListNrcsOfSaved(department, term, nrcs, active));
            if (data != null  && data.Count > 0)
            {
                NrcProgrammned objDates = new NrcProgrammned();
                List<long> listIds = data.Select(f => f.Id).Distinct().ToList();
                List<NrcProgrammned> listDates = await objDates.GetProgrammedByIds(listIds, isPublic, active);
                data.ForEach(
                    item =>
                        item.Scheduled = listDates
                            .Where(f => f.NrcForSubstituteExamId == item.Id)
                            .Select(s => s).ToList()
                    );
                data = data.Where(f => f.Scheduled != null && f.Scheduled.Count() > 0).ToList();
            }
            return data;
        }

        /// <summary>
        /// Listar Nrcs programados por periodo actual
        /// </summary>
        /// <param name="profile">Perfil de estudiante</param>
        /// <param name="isPublic"> Validar si es publico?(fechas de NRC)</param>
        /// <param name="active"> Validar si es activo?(fechas de NRC)(Nrc)</param>
        /// <returns></returns>
        public async Task<List<NrcForSubstituteExam>> GetListsubjectsbyStudentAndByCurrentPeriod(AcademicProfile profile, bool isPublic, bool active)
        {
            ACTerm currentTerm = new ACTerm().getCurrentSusbtitute(profile.campus.id, profile.department);
            return (await GetListSubjectsbyStudentAndTerm(profile, currentTerm.term, isPublic, active));
        }

        /// <summary>
        /// Listar Nrcs programados por periodo
        /// </summary>
        /// <param name="profile">Perfil estudiante</param>
        /// <param name="term">Periodo</param>
        /// <param name="isPublic">Validar si es publico?(fechas de NRC)</param>
        /// <param name="active">Validar si es activo?(fechas de NRC y Nrc)</param>
        /// <returns></returns>
        public async Task<List<NrcForSubstituteExam>> GetListSubjectsbyStudentAndTerm(AcademicProfile profile, string term, bool isPublic, bool active)
        {
            Course objCourse = new Course();
            List<Course> listCourses = objCourse.enrollmentsByTerm(profile, term);
            List<NrcForSubstituteExam> listsubstitute = new List<NrcForSubstituteExam>();
            if (listCourses != null && listCourses.Count > 0)
            {
                NrcForSubstituteExam objNrcForSubstituteExam = new NrcForSubstituteExam();
                SubstituteExamProgrammed objSubstituteExamProgrammed = new SubstituteExamProgrammed();
                List<string> listNrcs = listCourses.Select(f => f.nrc).Distinct().ToList();
                listsubstitute = await objNrcForSubstituteExam.GetListNrcForProgrammnedByTermAndNrcs(profile.department, term, listNrcs, isPublic, active);
                List<SubstituteExamProgrammed> listExamProgrammed = await objSubstituteExamProgrammed.GetSubstituteExamProgrammedbyStudentAndTerm(profile.person_id, term);
                List<string> NrcsProg = listExamProgrammed.Select(f => f.SubstituteExam.Nrc).Distinct().ToList();
                listsubstitute = listsubstitute.Where(item => !NrcsProg.Contains(item.Nrc)).ToList();
                if (listsubstitute != null && listsubstitute.Count > 0)
                {
                    Absence objAbsence = new Absence();
                    List<Absence> absenceNrcs = objAbsence.getByStudentAndTerm(profile, term);
                    if (absenceNrcs != null && absenceNrcs.Count > 0)
                    {
                        listsubstitute.ForEach(
                            item =>
                                item.AbsenceNrc = absenceNrcs
                                    .Where(
                                        f => f.nrc == item.Nrc)
                                    .Select(s => s).OrderByDescending(order => order.percentage)
                                    .FirstOrDefault()
                        );
                    }
                    
                }
            }
            return listsubstitute;
        }

        /// <summary>
        /// Obtener NRcs por departamento y periodo
        /// </summary>
        /// <param name="department">Modalidad</param>
        /// <param name="term">Periodo</param>
        /// <param name="nrcs">Lista de Nrcs a buscar</param>
        /// <returns></returns>
        public async Task<List<NrcForSubstituteExam>> GetListNrcsByTermAndDepartment(string department, string term, List<string> listNrcs) {
            //Obtener nrcs que ya estan registrados
            List<NrcForSubstituteExam> data = await GetListNrcForProgrammnedByTermAndNrcs(department, term, listNrcs, false, false);
            // los nrc que no se encontró
            listNrcs = listNrcs.Where(f => !data.Any(x => x.Nrc == f)).ToList();
            // buscar nrcs no encontrados en vistas de banner
            if (listNrcs != null && listNrcs.Count() > 0)
            {
                data.AddRange( await GetListNrcsOfViewsBanner(department, term, listNrcs));
            }
            return data;
        }

        /// <summary>
        /// obtener detalles de un Nrc
        /// </summary>
        /// <param name="department"></param>
        /// <param name="term"></param>
        /// <param name="nrc"></param>
        /// <returns></returns>
        public async Task<NrcForSubstituteExam> GetDetailsbyDepartmentAndTermAndNrc(string department, string term, string nrc) {
            List<string> listNrc = new List<string>();
            listNrc.Add(nrc);
            List<NrcForSubstituteExam> detailNrc = await GetListNrcsByTermAndDepartment(department, term, listNrc);
            return detailNrc.FirstOrDefault();
        }

        /// <summary>
        /// Guardar o actualizar NrcForSubstituteExam
        /// </summary>
        /// <param name="nrc">Nrc a guardar</param>
        /// <param name="user">Quien lo esta haciendo</param>
        /// <returns></returns>
        public async Task<NrcForSubstituteExam> SaveNrcForSubstituteExam(NrcForSubstituteExam nrc, string user, bool save) {
            OEANrcForSubstituteExam saveNrc = new OEANrcForSubstituteExam();
            // validar si existe
            List<string> listExistNrc = new List<string>();
            listExistNrc.Add(nrc.Nrc);
            NrcForSubstituteExam existNrc = (await GetListNrcsOfSaved(nrc.Department, nrc.Term, listExistNrc, false)).FirstOrDefault();
            // si existe modificar
            if (existNrc != null)
            {
                saveNrc = _nexoContext.oea_Nrc_for_substitute_exam.Find(existNrc.Id);
                // Validar si existe cambios
                if ((saveNrc.Active != nrc.Active || saveNrc.HasSusti != nrc.HasSusti) & save)
                {
                    saveNrc.ModifiedBy = user;
                    saveNrc.Active = nrc.Active;
                    saveNrc.HasSusti = nrc.HasSusti;
                    saveNrc.DateModified = DateTime.Now;
                    saveNrc.Comment = nrc.Comment;
                }
            }
            // si no asignar autor y fecha creación
            else
            {
                NrcForSubstituteExam notExistNrc = (await GetListNrcsOfViewsBanner(nrc.Department, nrc.Term, listExistNrc)).FirstOrDefault();
                saveNrc.Department = notExistNrc.Department;
                saveNrc.Campus = notExistNrc.Campus;
                saveNrc.SubjetID = notExistNrc.SubjetID;
                saveNrc.Credits = notExistNrc.Credits;
                saveNrc.Section = notExistNrc.Section;
                saveNrc.Term = notExistNrc.Term;
                saveNrc.Nrc = notExistNrc.Nrc;
                saveNrc.PartTerm = notExistNrc.PartTerm;
                saveNrc.Module = notExistNrc.Module;
                saveNrc.SubjetName = notExistNrc.SubjetName;
                saveNrc.Author = user;
                saveNrc.DateCreated = DateTime.Now;
                saveNrc.Active = nrc.Active;
                saveNrc.HasSusti = nrc.HasSusti;
                saveNrc.Comment = nrc.Comment;
                _nexoContext.oea_Nrc_for_substitute_exam.Add(saveNrc);
            }
            _nexoContext.SaveChanges();
            NrcForSubstituteExam savedNrc = (NrcForSubstituteExam)(saveNrc);
            savedNrc.Scheduled = nrc.Scheduled;
            savedNrc.Comment = nrc.Comment;
            // Guardar la programación de 
            NrcProgrammned objNrcProgrammned = new NrcProgrammned();
            if (savedNrc.Scheduled != null && savedNrc.Scheduled.Count > 0)
            {
                foreach (NrcProgrammned item in savedNrc.Scheduled)
                {
                    await objNrcProgrammned.SaveProgrammned(savedNrc.Id, item, savedNrc.Comment, user);
                }
            }
            
            return savedNrc;
        }
        
        
        
        #endregion Metodos

        #region Operadores explicitos
        public static explicit operator NrcForSubstituteExam(OEANrcForSubstituteExam obj) {
            NrcForSubstituteExam data = new NrcForSubstituteExam()
            {
                Id = obj.Id,
                Department = obj.Department,
                Campus = obj.Campus,
                Term = obj.Term,
                SubjetID = obj.SubjetID,
                Credits = obj.Credits,
                Section = obj.Section,
                Nrc = obj.Nrc,
                PartTerm = obj.PartTerm,
                Module = obj.Module,
                SubjetName = obj.SubjetName,
                Active = obj.Active,
                HasSusti = obj.HasSusti,
                Author = obj.Author,
                DateCreated = obj.DateCreated,
                ModifiedBy = obj.ModifiedBy,
                DateModified = obj.DateModified,
                Comment = obj.Comment
            };
            return data;
        }
        #endregion Operadores explicito
    }
}
