﻿using ACBusiness.Academic;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.OEA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;
using _oea = ACTools.Constants.LearningAssessmentConstants;
using _inst = ACTools.Constants.InstitucionalConstants;
using ACBusiness.Personal;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.Contexts.BDUCCI;
using ACTools.Mail;

namespace ACBusiness.LearningAssessment
{
    public class NoteClaim
    {
        #region Propiedades
        public long Id { get; set; }
        public int Year { get; set; }
        public int NoteClaimNumber { get; set; }
        public long SubjectId { get; set; }
        public string Type { get; set; }
        public long Status { get; set; }
        public decimal PidmStudent { get; set; }
        public decimal PidmTeacher { get; set; }
        public string ProgramId { get; set; }
        public int Component { get; set; }
        public int? SubComponent { get; set; }
        public string ScoreClaim { get; set; }
        public string Reason { get; set; }
        public bool HasPhysicalEvidence { get; set; }
        public long OfficeId { get; set; }
        public bool Active { get; set; }
        public decimal Author { get; set; }
        public DateTime DateCreated { get; set; }
        public decimal? ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string Comment { get; set; }
        public bool IsValid { get; set; }
        public string MsgValid { get; set; }
        public NrcForSubstituteExam Subject { get; set; }
        public Component ComponentData { get; set; } // Todos los datos del componente
        public SubComponent SubComponentData { get; set; } // Todos los datos del Subcomponente
        public Person StudentData { get; set; }
        public Person TeacherData { get; set; }
        public List<FileNoteClaim> Files { get; set; }
        public ReportNoteClaim reportTeacher { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();
        private readonly DBOContext _bducciContext = new DBOContext();
        #endregion Propiedades

        #region Metodos

        /// <summary>
        /// Obtener Cursos matriculados de un periodo
        /// </summary>
        /// <param name="claim">Filtros</param>
        /// <returns></returns>
        public Course GetEnrollmetByPidmAndTerm(NoteClaim claim)
        {
            Student student = new Student(claim.PidmStudent);
            student.profile = student.profiles.FirstOrDefault(f => f.program.id == claim.ProgramId);
            Course objCourse = new Course();
            Course matSubject = objCourse.reportCardtByTermBANNER(student.profile, claim.Subject.Term)
                .FirstOrDefault(f => f.term == claim.Subject.Term && f.nrc == claim.Subject.Nrc);

            return matSubject;
        }

        /// <summary>
        /// Obtener información basica de un reclamo por el ID
        /// </summary>
        /// <param name="id">Id del reclamo</param>
        /// <returns></returns>
        public NoteClaim GetSimpleDataById(long id)
        {
            var dataPre = _nexoContext.OEA_Note_Claim
               .Join(_nexoContext.oea_Nrc_for_substitute_exam,
                   ns => new { SubjectId = ns.SubjectId },
                   s => new { SubjectId = s.Id },
                   (ns, s) => new { ns, s })
                .Where(row => row.ns.Id == id)
                .FirstOrDefault();

            if (dataPre == null) return null;

            NoteClaim data = (NoteClaim)(dataPre.ns);
            data.Subject = (NrcForSubstituteExam)(dataPre.s);

            return data;
        }
        /// <summary>
        /// Obtener toda la información de un reclamo por el ID
        /// </summary>
        /// <param name="id">Id del reclamo</param>
        /// <returns></returns>
        public NoteClaim GetAllDataById(long id)
        {
            NoteClaim data = GetSimpleDataById(id);
            if (data == null) return null;

            FileNoteClaim objfile = new FileNoteClaim();
            data.StudentData = new Person(data.PidmStudent);
            data.TeacherData = new Person(data.PidmTeacher);
            data.Files = objfile.getFilesByClaimIdAndReportId(data.Id, null);
            Student student = new Student(data.PidmStudent);
            student.profile = student.profiles.FirstOrDefault(f => f.program.id == data.ProgramId);
            Course matSubject = GetEnrollmetByPidmAndTerm(data);
            data.ComponentData = matSubject.components.FirstOrDefault(f => f.id == data.Component);
            if (data.SubComponent != null)
            {
                data.SubComponentData = data.ComponentData.sub.FirstOrDefault(f => f.gcomId == data.Component && f.id == data.SubComponent);
            }
            // Reporte de docente
            ReportNoteClaim rptTeacher = new ReportNoteClaim();
            data.reportTeacher = rptTeacher.GetReportActiveByClaim(data.Id);

            return data;
        }

        /// <summary>
        /// Validar si la información que se guardara es valida
        /// </summary>
        /// <param name="profile">Perfil de estudiante</param>
        /// <param name="nrc">Nrc que reclama</param>
        /// <param name="term">Periodo</param>
        /// <returns>True -> si esta todo bien; False -> si hay algo mal </returns>
        public Tuple<bool, string> Validate(NoteClaim claim, bool save)
        {
            // validar datos que envian
            if (claim == null || claim.Subject == null)
            {
                return Tuple.Create(false, "Dato del estudiante o asignatura incorrecto.");
            }

            // validar matricula en periodo - nrc indicado
            Course matSubject = GetEnrollmetByPidmAndTerm(claim);
            if (matSubject == null)
            {
                return Tuple.Create(false, "No estás matriculado en la asignatura: " + claim.Subject.SubjetName);
            }

            // Validar que componente y subcomponente existen
            Component component = matSubject.components.FirstOrDefault(f => f.id == claim.Component);
            if (component == null)
            {
                return Tuple.Create(false, "El componente seleccionado no existe.");
            }
            SubComponent subComponent = component.sub.FirstOrDefault(f => f.id == claim.SubComponent);
            if (claim.SubComponent != null && subComponent == null)
            {
                return Tuple.Create(false, "El subcomponente seleccionado no existe.");
            }

            // Validar que si no selecciono subComponente el componente seleccionado no cuente con subcomponentes
            if (claim.SubComponent == null && component.sub != null && component.sub.Count > 0)
            {
                return Tuple.Create(false, "El componente seleccionado cuenta con subcomponentes.");
            }

            // Validar que existe nota en componente - subcomponente
            if (claim.SubComponent == null && (component.score == null
                || component.score.Trim() == "" || component.score.Trim() == "-"))
            {
                return Tuple.Create(false, "No tiene nota en el componente reclamado.");
            }
            if (claim.SubComponent != null && (subComponent.score == null
                || subComponent.score.Trim() == "" || component.score.Trim() == "-"))
            {
                return Tuple.Create(false, "No tiene nota en el subcomponente reclamado");
            }

            // Validar que reclamo este dentro de las fechas establecidas
            // Asignaturas presenciales
            if (matSubject.department == _acad.DEPA_REG && subComponent != null 
                && subComponent.daysUploadNote > 1)
            {
                return Tuple.Create(false, "Solo tienes un día para reclamar asignaturas regulares.");
            }
            if (matSubject.department == _acad.DEPA_REG && subComponent == null
                && component.daysUploadNote > 1)
            {
                return Tuple.Create(false, "Solo tienes un día para reclamar asignaturas regulares.");
            }
            // Asignaturas NO presenciales
            if (matSubject.department != _acad.DEPA_REG && subComponent != null
                && subComponent.daysUploadNote > 5)
            {
                return Tuple.Create(false, "Solo tienes 5 días para reclamar asignaturas semipresenciales.");
            }
            if (matSubject.department != _acad.DEPA_REG && subComponent == null
                && component.daysUploadNote > 5)
            {
                return Tuple.Create(false, "Solo tienes 5 días para reclamar asignaturas semipresenciales.");
            }

            // Validar que el estudiante ya no haya reclamado en esa asignatura en el componente - subcomponente seleccionado
            if ((GetByStudentAndSubject(claim.PidmStudent, claim.Subject, claim.Component, claim.SubComponent)) != null)
            {
                return Tuple.Create(false, "Ya tienes un reclamo en la nota seleccionada de la asignatura: " +
                                              claim.Subject.SubjetName + " - " +
                                              component.name +
                                              (claim.SubComponent != null ? (" - " + subComponent.name) : ""));
            }

            if (save)
            {
                if (claim.Type == null || claim.Type == null
                    || _oea.OEA_TYPES_NOTE_CLAIM.Where(f => f.Key == claim.Type).ToList().Count <= 0)
                {
                    return Tuple.Create(false, "El tipo de reclamo es invalido");
                }
                if (claim.Reason == null || claim.Reason.Trim() == "")
                {
                    return Tuple.Create(false, "El motivo no puede estar vacío.");
                }
            }

            return Tuple.Create(true, "");
        }

        /// <summary>
        /// Obtener información posible reclamo
        /// </summary>
        /// <param name="student">Estudiante</param>
        /// <param name="department">Modalidad</param>
        /// <param name="term">Periodo</param>
        /// <param name="nrc">Nrc</param>
        /// <param name="componentId">Id de componente</param>
        /// <param name="subComponentId">Id se subcomponente</param>
        /// <returns></returns>
        public async Task<NoteClaim> GetDataNrcByStudent(Student student, string department, string term, string nrc, int componentId, int? subComponentId)
        {
            NrcForSubstituteExam objNrc = new NrcForSubstituteExam();
            List<string> listExistNrc = new List<string>();
            listExistNrc.Add(nrc);
            // obtener y asignar data de asignatura
            NoteClaim data = new NoteClaim();
            NrcForSubstituteExam subject = (await objNrc.GetListNrcsOfViewsBanner(department, term, listExistNrc)).FirstOrDefault();
            data.PidmStudent = student.person_id;

            if (subject != null)
            {
                // Si asignatura no tiene docente asignado, asignar reclamo a CCERRON
                data.PidmTeacher = subject.TeacherPidm ?? 46484;
                data.Subject = subject;
            }

            data.ProgramId = student.profile.program.id;
            data.Component = componentId;
            data.SubComponent = subComponentId;
            Tuple<bool, string> validate = Validate(data, false);
            data.IsValid = validate.Item1;
            data.MsgValid = validate.Item2;

            if (data.IsValid)
            {
                Course objCourse = new Course();
                Course matSubject = objCourse.reportCardtByTermBANNER(student.profile, data.Subject.Term)
                                             .FirstOrDefault(f => f.term == data.Subject.Term && f.nrc == data.Subject.Nrc);
                data.ComponentData = matSubject.components.FirstOrDefault(f => f.id == componentId);
                data.ScoreClaim = data.ComponentData.score;
                if (data.SubComponent != null)
                {
                    data.SubComponentData = data.ComponentData.sub.FirstOrDefault(f => f.gcomId == componentId && f.id == subComponentId);
                    data.ScoreClaim = data.SubComponentData.score;
                }
            }

            return data;
        }

        /// <summary>
        /// Retornar reclamo de un estudiante por asignatura
        /// </summary>
        /// <param name="pidmStudent">Pidm de Estudiante</param>
        /// <param name="department">Modalidad de asignatura</param>
        /// <param name="term">Periodo de asignatura</param>
        /// <param name="nrc">Nrc de asignatura</param>
        /// <returns></returns>
        public NoteClaim GetByStudentAndSubject(decimal pidmStudent, NrcForSubstituteExam subject, int componentId, int? subComponentId)
        {
            var dataPre = _nexoContext.OEA_Note_Claim
               .Join(_nexoContext.oea_Nrc_for_substitute_exam,
                   ns => new { SubjectId = ns.SubjectId },
                   s => new { SubjectId = s.Id },
                   (ns, s) => new { ns, s })
               .Where(row =>
                       row.s.Department == subject.Department
                       && row.s.Term == subject.Term
                       && row.s.Nrc == subject.Nrc
                       && row.ns.PidmStudent == pidmStudent
                       && row.ns.Component == componentId
                       && row.ns.SubComponent == subComponentId)
               .FirstOrDefault();

            if (dataPre == null) return null;
            
            NoteClaim data = (NoteClaim)(dataPre.ns);
            data.Subject = (NrcForSubstituteExam)(dataPre.s);
            return data;
        }

        /// <summary>
        /// Obtener proximo numero para reclamo
        /// </summary>
        /// <param name="year">Año de reclamo</param>
        /// <returns></returns>
        public int GetNumerationByYear(int year)
        {
            int numeration = _nexoContext.OEA_Note_Claim.Where(result => result.Year == year)
                .Select(result => result.NoteClaimNumber).DefaultIfEmpty(0).Max();
            return numeration + 1;
        }

        /// <summary>
        /// Obtener officina por reclamo
        /// </summary>
        /// <param name="claim"></param>
        /// <returns></returns>
        public int GetOfficeByClaim(NoteClaim claim)
        {
            // Si es rectificación oficina asignada es Registros academicos
            if (claim.Type == _oea.OEA_TYPE_NOTE_CLAIM_RECTIFICATION)
            {
                return _inst.OFFICE_RA;
            }
            // Si es recalificación y modalidad virtual y no es examen final gestion docente
            if (claim.Type == _oea.OEA_TYPE_NOTE_CLAIM_REQUALIFICATION && claim.Subject.Department == _acad.DEPA_VIR
                && claim.ComponentData.code.Trim() != "5-EF")
            {
                return _inst.OFFICE_OFG;
            }
            // Si no, oficia de evaluaciones
            return _inst.OFFICE_OEA;
        }

        /// <summary>
        /// Obtener estado siguiente de reclamo
        /// </summary>
        /// <param name="claim"></param>
        /// <returns></returns>
        public long GetNextStatusByClaim(OEANoteClaim claim)
        {
            switch (claim.Status)
            {
                case 0:
                case _oea.OEA_STATUS_NOTE_CLAIM_REGISTERED:
                    return _oea.OEA_STATUS_NOTE_CLAIM_CAS;
                case _oea.OEA_STATUS_NOTE_CLAIM_CAS:
                    return _oea.OEA_STATUS_NOTE_CLAIM_TEACHER;
                case _oea.OEA_STATUS_NOTE_CLAIM_TEACHER:
                    return _oea.OEA_STATUS_NOTE_CLAIM_INVOLVED_AREA;
                case _oea.OEA_STATUS_NOTE_CLAIM_INVOLVED_AREA:
                    return _oea.OEA_STATUS_NOTE_CLAIM_RAU;
                case _oea.OEA_STATUS_NOTE_CLAIM_RAU:
                    return _oea.OEA_STATUS_NOTE_CLAIM_FINALIZED;
                default:
                    throw new Exception("No existe estado siguiente");
            }
        }

        /// <summary>
        /// Obtener reclamos por estudiante y periodo
        /// </summary>
        /// <param name="pidm">Pidm estudiante</param>
        /// <param name="term">Periodo</param>
        /// <returns></returns>
        public List<NoteClaim> GetClaimsByStudentAndTerm(decimal pidm, string term)
        {
            var dataPre = _nexoContext.OEA_Note_Claim
               .Join(_nexoContext.oea_Nrc_for_substitute_exam,
                   ns => new { SubjectId = ns.SubjectId },
                   s => new { SubjectId = s.Id },
                   (ns, s) => new { ns, s })
               .Where(row => row.ns.PidmStudent == pidm)
               .ToList();
            if (dataPre.Any())
            {
                List<NoteClaim> data = new List<NoteClaim>();
                foreach (var d in dataPre)
                {
                    NoteClaim dataAdd = (NoteClaim)(d.ns);
                    dataAdd.Subject = (NrcForSubstituteExam)(d.s);
                    data.Add(dataAdd);
                };
                return data;
            }

            return null;
        }
        /// <summary>
        /// Validar si usuario tiene accesos al modulo de la oficina
        /// </summary>
        /// <param name="officeId">código de oficina</param>
        /// <param name="moduleListUser">Lista de modulos del usuario</param>
        public void validateAccessOffice(int officeId, List<Module> moduleListUser)
        {
            string moduleName = _inst.OFFICES_NAMES_MODULE[officeId];
            Module accessModule = moduleListUser.FirstOrDefault(f => f.name == moduleName);

            if (moduleName.Trim().Count() <= 0 || accessModule == null)
            {
                throw new Exception("No tiene accesos.");
            }
        }
        /// <summary>
        /// Retornar lista de reclamos por estado y oficina
        /// </summary>
        /// <param name="status">Estado a filtrar</param>
        /// <param name="officeId">ID de officina</param>
        /// <returns></returns>
        public List<NoteClaim> GetListNotesClaimByStatusAndOffice(int status, int officeId, List<Module> moduleListUser)
        {
            // Validar que la persona que tenga accesos al modulo de la oficina que indica
            validateAccessOffice(officeId, moduleListUser);
            // Obtener data
            var dataPre = _nexoContext.OEA_Note_Claim
               .Join(_nexoContext.oea_Nrc_for_substitute_exam,
                   ns => new { SubjectId = ns.SubjectId },
                   s => new { SubjectId = s.Id },
                   (ns, s) => new { ns, s })
               .Where(
                   row =>
                       row.ns.Status == status
                       && ((status == _oea.OEA_STATUS_NOTE_CLAIM_RAU && officeId == _inst.OFFICE_RA) 
                           || row.ns.OfficeId == officeId))
               .ToList();

            if (dataPre.Any())
            {
                List<NoteClaim> notesClaim = new List<NoteClaim>();
                List<decimal> pidmStudents = dataPre.Select(f => f.ns.PidmStudent).ToList();
                List<DBOTblPersona> students = _bducciContext.tbl_persona.Where(result =>
                    pidmStudents.Contains(result.IDPersonaN)).ToList();

                foreach (var data in dataPre)
                {
                    NoteClaim dataAdd = (NoteClaim)(data.ns);
                    dataAdd.Subject = (NrcForSubstituteExam)(data.s);
                    dataAdd.StudentData = students.FirstOrDefault(f => f.IDPersonaN == dataAdd.PidmStudent);
                    notesClaim.Add(dataAdd);
                };
                
                return notesClaim;
            }

            return null;
        }

        /// <summary>
        /// Obtener reclamos de un docente
        /// </summary>
        /// <param name="pidmTeacher">Pidm de docente</param>
        /// <returns></returns>
        public List<NoteClaim> GetListNotesClaimByTeacher(decimal pidmTeacher)
        {
            var dataPre = _nexoContext.OEA_Note_Claim
               .Join(_nexoContext.oea_Nrc_for_substitute_exam,
                   ns => new { SubjectId = ns.SubjectId },
                   s => new { SubjectId = s.Id },
                   (ns, s) => new { ns, s })
               .Where(
                   row =>
                       row.ns.Status == _oea.OEA_STATUS_NOTE_CLAIM_TEACHER
                       && row.ns.PidmTeacher == pidmTeacher)
               .ToList();

            if (dataPre.Any())
            {
                List<NoteClaim> notesClaim = new List<NoteClaim>();
                List<decimal> pidmStudents = dataPre.Select(f => f.ns.PidmStudent).ToList();
                List<DBOTblPersona> students = _bducciContext.tbl_persona.Where(result =>
                    pidmStudents.Contains(result.IDPersonaN)).ToList();

                foreach (var data in dataPre)
                {
                    NoteClaim dataAdd = (NoteClaim)(data.ns);
                    dataAdd.Subject = (NrcForSubstituteExam)(data.s);
                    dataAdd.StudentData = students.FirstOrDefault(f => f.IDPersonaN == dataAdd.PidmStudent);
                    notesClaim.Add(dataAdd);
                };

                return notesClaim;
            }

            return null;
        }


        /// <summary>
        /// Guardar reclamo
        /// </summary>
        /// <param name="student"></param>
        /// <param name="claim"></param>
        /// <returns></returns>
        public async Task<NoteClaim> Save(Student student, NoteClaim claim, decimal pidm)
        {
            Tuple<bool, string> validateSave = Validate(claim, true);

            if (!validateSave.Item1)
            {
                throw new Exception(validateSave.Item2);
            }

            NoteClaim claimData = await GetDataNrcByStudent(
                student, 
                claim.Subject.Department,
                claim.Subject.Term,
                claim.Subject.Nrc,
                claim.Component,
                claim.SubComponent == null ? null : (int?)claim.SubComponent);
            claimData.Type = claim.Type;
            claimData.Reason = claim.Reason;
            claimData.HasPhysicalEvidence = claim.HasPhysicalEvidence;
            claimData.DateCreated = DateTime.Now;
            claimData.Year = claimData.DateCreated.Year;
            claimData.Status = _oea.OEA_STATUS_NOTE_CLAIM_CAS;
            claimData.Active = true;
            claimData.Author = pidm;
            claimData.NoteClaimNumber = GetNumerationByYear(claimData.Year);
            NrcForSubstituteExam objSubstitute = new NrcForSubstituteExam();
            NrcForSubstituteExam savedNrc = await objSubstitute.SaveNrcForSubstituteExam(claimData.Subject, "RECLAMONOTAS", false);
            claimData.SubjectId = savedNrc.Id;
            claimData.OfficeId = _inst.OFFICE_CAU;

            OEANoteClaim savedNoteClaim = new OEANoteClaim();
            savedNoteClaim = (OEANoteClaim)claimData;
            _nexoContext.OEA_Note_Claim.Add(savedNoteClaim);
            _nexoContext.SaveChanges();
            claim = claimData;
            claimData = (NoteClaim)(savedNoteClaim);
            claimData.ComponentData = claim.ComponentData;
            claimData.SubComponentData = claim.SubComponentData;

            return claimData;
        }

        /// <summary>
        /// Actualizar tipo de reclamo y derivar a docente
        /// </summary>
        /// <param name="claim"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool SaveDerivateTeacher(NoteClaim claim, decimal user)
        {

            if (claim == null || claim.Comment == null || claim.Comment.Trim() == "" || claim.Type == null 
                || claim.Type == null || !_oea.OEA_TYPES_NOTE_CLAIM.Any(f => f.Key == claim.Type))
            {
                throw new Exception("Valores Invalidos");
            }

            OEANoteClaim saveClaim = _nexoContext.OEA_Note_Claim.Find(claim.Id);
            NoteClaim validateClaim = GetAllDataById(claim.Id);

            if (validateClaim.Status != _oea.OEA_STATUS_NOTE_CLAIM_CAS || validateClaim.OfficeId != _inst.OFFICE_CAU)
            {
                throw new Exception("El reclamo tiene que estar en el CAU");
            }

            validateClaim.Type = claim.Type;
            saveClaim.Type = claim.Type;
            saveClaim.Comment = claim.Comment;
            saveClaim.ModifiedBy = user;
            saveClaim.DateModified = DateTime.Now;
            saveClaim.OfficeId = GetOfficeByClaim(validateClaim);
            saveClaim.Status = GetNextStatusByClaim(saveClaim);
            _nexoContext.SaveChanges();
            // Enviar Correo
            MailSender mail = new MailSender();
            mail.compose("oea_note_claim_derivate_teacher",
                new
                {
                    name = validateClaim.TeacherData.first_name,
                    numeration = validateClaim.NoteClaimNumber,
                    username = validateClaim.TeacherData.userName.ToLower(),
                    year = validateClaim.Year,
                    dateCreation = validateClaim.DateCreated,
                    nameStudent = validateClaim.StudentData.full_name,
                    identificationStudent = validateClaim.StudentData.document_number,
                    subjectName = validateClaim.Subject.SubjetName
                })
                .destination($"{validateClaim.TeacherData.userName}@continental.edu.pe", "Reclamo de nota")
                .send();

            return true;
        }

        /// <summary>
        /// Finalizar Reclamo
        /// </summary>
        /// <param name="claim">Reclamo</param>
        /// <param name="user">Usuario</param>
        /// <param name="moduleListUser">Modulos de usuario</param>
        /// <returns></returns>
        public bool FinalizeClaim(NoteClaim claim, decimal user, List<Module> moduleListUser)
        {

            if (claim == null || claim.Comment == null || claim.Comment.Trim() == "")
            {
                throw new Exception("Valores Invalidos");
            }
            // Validar que la persona que tenga accesos al modulo de RAU
            validateAccessOffice(_inst.OFFICE_RA, moduleListUser);
            // Validar estado de solicitud
            NoteClaim validateClaim = GetAllDataById(claim.Id);

            if (validateClaim.Status != _oea.OEA_STATUS_NOTE_CLAIM_RAU || validateClaim.reportTeacher == null
                || validateClaim.reportTeacher.Status != _oea.OEA_STATUS_NOTE_CLAIM_REPORT_APPROVED)
            {
                throw new Exception("El reclamo tiene que esta pendiente de RAU y el reporte aprobado.");
            }

            OEANoteClaim saveClaim = _nexoContext.OEA_Note_Claim.Find(claim.Id);
            SaveNextStatusClaim(validateClaim.Id, user);
            // Enviar Correo
            MailSender mail = new MailSender();
            mail.compose("oea_note_claim_answer_student",
                new
                {
                    names = validateClaim.StudentData.first_name,
                    year = validateClaim.Year,
                    numeration = validateClaim.NoteClaimNumber,
                    dateCreation = validateClaim.DateCreated,
                    subjectName = validateClaim.Subject.SubjetName,
                    component = validateClaim.ComponentData.name + 
                                (validateClaim.SubComponentData != null? (" - " + validateClaim.SubComponentData.name) : ""),
                    procced = (validateClaim.reportTeacher.Proceeds ? "Si" : "No"),
                    reason = claim.Comment,

                })
                .destination($"{validateClaim.StudentData.document_number}@continental.edu.pe", "Reclamo de nota")
                .send();

            return true;
        }
        /// <summary>
        /// Asignar siguiente estado a reclamo
        /// </summary>
        /// <param name="id">Id de reclamo</param>
        /// <param name="user">Usuario que hace cambio</param>
        /// <returns></returns>
        public bool SaveNextStatusClaim(long id, decimal user)
        {
            OEANoteClaim updateClaim = _nexoContext.OEA_Note_Claim.Find(id);

            if (updateClaim == null)
            {
                throw new Exception("No existe reclamo.");
            }

            updateClaim.Status = GetNextStatusByClaim(updateClaim);
            updateClaim.ModifiedBy = user;
            updateClaim.DateModified = DateTime.Now;
            _nexoContext.SaveChanges();

            return true;
        }
     
        #endregion Metodos

        #region Operadores explicitos
        public static explicit operator NoteClaim(OEANoteClaim obj)
        {
            NoteClaim data = new NoteClaim()
            {
                Id = obj.Id,
                Year = obj.Year,
                NoteClaimNumber = obj.NoteClaimNumber,
                SubjectId = obj.SubjectId,
                Type = obj.Type,
                Status = obj.Status,
                PidmStudent = obj.PidmStudent,
                PidmTeacher = obj.PidmTeacher,
                ProgramId = obj.ProgramId,
                Component = obj.Component,
                SubComponent = obj.SubComponent,
                ScoreClaim = obj.ScoreClaim,
                Reason = obj.Reason,
                HasPhysicalEvidence = obj.HasPhysicalEvidence,
                OfficeId = obj.OfficeId,
                Active = obj.Active,
                Author = obj.Author,
                DateCreated = obj.DateCreated,
                ModifiedBy = obj.ModifiedBy,
                DateModified = obj.DateModified,
                Comment = obj.Comment
            };

            return data;
        }

        public static explicit operator OEANoteClaim(NoteClaim obj)
        {
            OEANoteClaim data = new OEANoteClaim()
            {
                Year = obj.Year,
                NoteClaimNumber = obj.NoteClaimNumber,
                SubjectId = obj.SubjectId,
                Type = obj.Type,
                Status = obj.Status,
                PidmStudent = obj.PidmStudent,
                PidmTeacher = obj.PidmTeacher,
                ProgramId = obj.ProgramId,
                Component = obj.Component,
                SubComponent = obj.SubComponent,
                ScoreClaim = obj.ScoreClaim,
                Reason = obj.Reason,
                HasPhysicalEvidence = obj.HasPhysicalEvidence,
                OfficeId = obj.OfficeId,
                Active = obj.Active,
                Author = obj.Author,
                DateCreated = obj.DateCreated,
                ModifiedBy = obj.ModifiedBy,
                DateModified = obj.DateModified,
                Comment = obj.Comment
            };
            return data;
        }

        #endregion Operadores explicitos

        public class Module
        {
            public int id { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public string campus { get; set; }
        }
    }
}