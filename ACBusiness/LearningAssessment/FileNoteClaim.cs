﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.OEA;
using ACTools.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ACBusiness.LearningAssessment
{
    public class FileNoteClaim
    {
        #region Propiedades
        public long Id { get; set; }
        public long NoteClaimId { get; set; }
        public long? ReportNoteClaimId { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();
        #endregion Propiedades

        #region Metodos

        /// <summary>
        /// Obtener archivos por reclamo y reporte
        /// </summary>
        /// <param name="claimId">Id de reclamo</param>
        /// <param name="reportID">Id de reporte</param>
        /// <returns></returns>
        public List<FileNoteClaim> getFilesByClaimIdAndReportId(long claimId, long? reportID)
        {
            List<FileNoteClaim> files = _nexoContext.OEA_File_Note_Claim
                .Where(row => row.NoteClaimId == claimId && row.ReportNoteClaimId == reportID)
                .ToList().Select(rows =>(FileNoteClaim)(rows))
                .ToList();
            return files;
        }

        /// <summary>
        /// Obtener archivo por id
        /// </summary>
        /// <param name="fileId">Id de archivo</param>
        /// <returns></returns>
        public Tuple<string, string> GetFileDataById(long fileId)
        {
            OEAFileNoteClaim file = _nexoContext.OEA_File_Note_Claim.Find(fileId);
            return file == null 
                   ? null 
                   : Tuple.Create($"{AppSettings.reports["output-NoteClaim"]}{file.Path}", file.ContentType);
        }

        /// <summary>
        /// Guardar y subir archivo
        /// </summary>
        /// <param name="noteClaimId">Id de reclamo</param>
        /// <param name="reportNoteClaimId">Id de reporte</param>
        /// <param name="file">archivo</param>
        /// <returns></returns>
        public bool SaveFile(int noteClaimId, int? reportNoteClaimId, HttpPostedFile file)
        {
            string _path = AppSettings.reports["output-NoteClaim"];
            OEAFileNoteClaim saveFile = new OEAFileNoteClaim();
            saveFile.NoteClaimId = noteClaimId;
            saveFile.ReportNoteClaimId = reportNoteClaimId;
            saveFile.Path = file.FileName;
            saveFile.Name = file.FileName;
            saveFile.ContentType = file.ContentType;
            _nexoContext.OEA_File_Note_Claim.Add(saveFile);
            // Validar que guarda registro en bd
            if (_nexoContext.SaveChanges() <= 0)
            {
                throw new Exception("Error al registrar archivo.");
            }

            try
            {
                saveFile.Path = $"{saveFile.Id}{System.IO.Path.GetExtension(saveFile.Path)}";
                // valida que actualiza registro en bd
                if (_nexoContext.SaveChanges() <= 0)
                {
                    throw new Exception("No se pudo actualizar path.");
                }

                // Crea directorio si no existe
                if (!Directory.Exists(saveFile.Path))
                {
                    Directory.CreateDirectory(_path);
                }

                var filePath = $"{_path}/{saveFile.Path}";
                file.SaveAs(filePath); // Guardar archivo
            }
            catch (Exception e)
            {
                _nexoContext.OEA_File_Note_Claim.Remove(saveFile);
                _nexoContext.SaveChanges();
                return false;
            }

            return true;
        }
        #endregion Metodos

        #region Operadores Explicitos
        public static explicit operator FileNoteClaim(OEAFileNoteClaim obj)
        {
            FileNoteClaim data = new FileNoteClaim
            {
                Id = obj.Id,
                NoteClaimId = obj.NoteClaimId,
                ReportNoteClaimId = obj.ReportNoteClaimId,
                Name = obj.Name,
                Path = obj.Path,
                ContentType = obj.ContentType,
            };

            return data;
        }
        #endregion Operadores Explicitos
    }
}
