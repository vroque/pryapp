﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.Contexts.BANNER;
using ACBusiness.Academic;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACTools.Mail;

namespace ACBusiness.LearningAssessment
{
    public class Reports
    {
        #region Propiedades
        #region inputs report
        public string input_div { get; set; }
        public string input_term { get; set; }
        public string input_group { get; set; }
        public string input_modality { get; set; }
        public string InputCampus { get; set; }
        public string inputType { get; set; }
        public long? inputStatus { get; set; }
        public string inputComponent { get; set; }
        public string inputStudenId { get; set; }
        public int NumberReport { get; set; }
        public string emailSubject { get; set; }
        public string emailBody { get; set; }
        public List<NoteClaim> lstNotesClaim { get; set; }
        public string user { get; set; }

        #endregion
        public string r1_student_id { get; set; }
        public string r1_student_name { get; set; }
        public string r1_name_faculty { get; set; }
        public string r1_school_name { get; set; }
        public string r1_nrc { get; set; }
        public string r1_part_period { get; set; }
        public string r1_modality { get; set; }
        public string r1_campus_name { get; set; }
        public string r1_course_id { get; set; }
        public string r1_course_name { get; set; }
        public string r1_score_c1 { get; set; }
        public string r1_score_ep { get; set; }
        public string r1_score_c2 { get; set; }
        public string r1_score_ef { get; set; }
        public string r1_ammount { get; set; }
        public string r1_debt { get; set; }
        public string r1_deposit { get; set; }
        public string r1_section_id { get; set; }
        public string r1_group { get; set; }
        public DateTime? r1_request_date { get; set; }
        public string r1_proof { get; set; }
        public string R1ClassRoom { get; set; }
        public DateTime R1ExamDate { get; set; }

        public NrcForSubstituteExam NrcForSubstituteExam { get; set; }
        public NrcProgrammned NrcProgrammned { get; set; }

        private readonly DBOContext _dboContext = new DBOContext();
        private readonly NEXOContext _nexoContext = new NEXOContext();
        private readonly BNContext _bnContext = new BNContext();
        #endregion

        #region Metodos
        public async Task<List<string>> getTerms()
        {
            List<string> terms = _dboContext.tbl_examen_sustitutorio_banner
                .Select(result => result.IDPerAcad)
                .ToList().Distinct().OrderByDescending(result => result).ToList();
            return terms;
        }

        public async Task<List<Reports>> ReportSusbstituteProgrammed(string div, string term, string group, string modality)
        {
            List<Reports> report = _dboContext.OEA_sp_report_substitute_exam(div, term, group, modality)
                .Select(result => new Reports
                {
                    r1_student_id = result.IDAlumno,
                    r1_student_name = result.NomCompleto,
                    r1_name_faculty = result.NomFacultad,
                    r1_school_name = result.NomEsc,
                    r1_nrc = result.NRC,
                    r1_part_period = result.PartePeriodo,
                    r1_modality = result.Modalidad,
                    r1_campus_name = result.NombreSede,
                    r1_course_id = result.idasignatura,
                    r1_course_name = result.nomasignatura,
                    r1_score_c1 = result.C1,
                    r1_score_ep = result.EP,
                    r1_score_c2 = result.C2,
                    r1_score_ef = result.EF,
                    r1_ammount = result.Cargo,
                    r1_debt = result.Deuda,
                    r1_deposit = result.Abono,
                    r1_section_id = result.IDSeccion,
                    r1_group = result.Grupo,
                    r1_request_date = result.Fecha_Solicitud,
                    r1_proof = result.Just,
                    R1ClassRoom = result.ClassRoom,
                    R1ExamDate = result.ExamDate
                }).ToList();
            return report;

        }

        /// <summary>
        /// Reporte de Nrcs Programados
        /// </summary>
        /// <param name="departament">Modalidad</param>
        /// <param name="campus">Sede</param>
        /// <param name="term">Periodo</param>
        /// <param name="module">Grupo</param>
        /// <returns></returns>
        public async Task<List<Reports>> ReportOfNrcsProgrammed(string departament, string campus, string term, string module) {
            List<Reports> data = _nexoContext.oea_Nrc_for_substitute_exam
                .Join(
                    _nexoContext.oea_nrc_programmned,
                    nf => new { idNrc = nf.Id },
                    np => new { idNrc = np.NrcForSubstituteExamId },
                    (nf, np) => new { nf, np }
                ).Where(
                    item =>
                        (departament == null || item.nf.Department == departament)
                        && (campus == null || item.nf.Campus == campus)
                        && item.nf.Term == term
                        && (module == null || item.nf.Module == module)
                ).ToList().Select(result => new Reports
                {
                    NrcForSubstituteExam = ((NrcForSubstituteExam)(result.nf)),
                    NrcProgrammned = (NrcProgrammned)(result.np)

                }).ToList();
            return data;
        }

        /// <summary>
        /// Reporte general de reclamo de notas
        /// </summary>
        /// <param name="departament">Modalidad</param>
        /// <param name="campus">Sede</param>
        /// <param name="term">Periodo</param>
        /// <param name="type">Tipo</param>
        /// <param name="status">Estado</param>
        /// <param name="component">Componente</param>
        /// <param name="studentCode">Codigo de estudiante</param>
        /// <returns></returns>
        public async Task<List<NoteClaim>> ReportGeneralNoteClaim(string departament, string campus, string term, string type, long? status, string component, string studentCode)
        {
            var student = _dboContext.view_alumno_basico.Where(f => f.IDAlumno == studentCode).FirstOrDefault();
            decimal? studentPidm = (student == null ? (decimal?)null: student.IDPersonaN);

            var dataPre = _nexoContext.OEA_Note_Claim
               .Join(
                   _nexoContext.oea_Nrc_for_substitute_exam,
                   ns => new { SubjectId = ns.SubjectId },
                   s => new { SubjectId = s.Id },
                   (ns, s) => new { ns, s }
                ).Where(
                   row =>
                    row.s.Term == term
                    && ( departament == null || row.s.Department == departament )
                    && ( status == null || row.ns.Status == status )
                    && ( campus == null || row.s.Campus == campus )
                    && ( type == null || row.ns.Type == type )
                    && ( studentPidm == null || row.ns.PidmStudent == studentPidm )
                ).ToList();

            if (dataPre.Any())
            {
                // Obtener componentes
                List<string>listNrcs = dataPre.Select(f =>f.s.Nrc).Distinct().ToList();
                List<int?> listComponents= dataPre.Select(f => (int?)f.ns.Component).Distinct().ToList();
                var dataComponent = _bnContext.tbl_SHRGCOM
                    .Where( f =>
                        f.SHRGCOM_TERM_CODE == term
                        && listNrcs.Contains(f.SHRGCOM_CRN)
                        && listComponents.Contains(f.SHRGCOM_ID)).ToList();
                // Obtener data de subcomponente
                List<int?> listDataSC = dataPre.Where(f => f.ns.SubComponent != null)
                    .Select(f => f.ns.SubComponent).Distinct().ToList();
                var dataSubComponent = _bnContext.tbl_SHRSCOM
                    .Where(f =>
                        f.SHRSCOM_TERM_CODE == term
                        && listNrcs.Contains(f.SHRSCOM_CRN)
                        && listDataSC.Contains(f.SHRSCOM_ID)).ToList();

                List<decimal> pidmPersons = dataPre.Select(f => f.ns.PidmStudent).Distinct().ToList();
                pidmPersons.AddRange(dataPre.Select(f => f.ns.PidmTeacher).Distinct().ToList());
                List<DBOTblPersona> persons = _dboContext.tbl_persona.Where(result =>
                    pidmPersons.Contains(result.IDPersonaN)
                ).ToList();

                List<NoteClaim> notesClaim = new List<NoteClaim>();

                foreach (var data in dataPre)
                {
                    NoteClaim dataAdd = (NoteClaim)data.ns;
                    dataAdd.Subject = (NrcForSubstituteExam)data.s;
                    dataAdd.StudentData = persons.FirstOrDefault(f => f.IDPersonaN == dataAdd.PidmStudent);
                    dataAdd.TeacherData = persons.FirstOrDefault(f => f.IDPersonaN == dataAdd.PidmTeacher);
                    dataAdd.ComponentData = dataComponent
                        .Where(f =>
                           f.SHRGCOM_TERM_CODE == dataAdd.Subject.Term
                           && f.SHRGCOM_CRN == dataAdd.Subject.Nrc
                           && f.SHRGCOM_ID == dataAdd.Component)
                        .Select(row => new Component
                            {
                                name = row.SHRGCOM_NAME,
                                description = row.SHRGCOM_DESCRIPTION
                            })
                        .FirstOrDefault();
                    dataAdd.SubComponentData = dataSubComponent
                        .Where(f =>
                            f.SHRSCOM_TERM_CODE == dataAdd.Subject.Term
                            && f.SHRSCOM_CRN == dataAdd.Subject.Nrc
                            && f.SHRSCOM_ID == dataAdd.SubComponent)
                        .Select(row => new SubComponent
                            {
                                name = row.SHRSCOM_DESCRIPTION
                            })
                        .FirstOrDefault();
                    
                    notesClaim.Add(dataAdd);
                };

                notesClaim = notesClaim.Where(f => component == null || f.ComponentData.name == component).ToList();
                return notesClaim;
            }

            return null;
        }

        /// <summary>
        /// Enviar correo a docentes
        /// </summary>
        /// <param name="emailSubject">Asunto del correo</param>
        /// <param name="emailBody">Cuerpo del correo</param>
        /// <param name="data">a quienes</param>
        /// <param name="user">USuarioque manda</param>
        /// <returns></returns>
        public bool SendMailReportNoteClaim(string emailSubject, string emailBody, List<NoteClaim> data, string user)
        {
            List<string> emails = data.Select(f => $"{f.TeacherData.userName}@continental.edu.pe").Distinct().ToList();

            if (emails.Any())
            {
                emails.Add($"{user}@continental.edu.pe");
                // Enviar Correo
                MailSender mail = new MailSender();
                mail.compose("oea_note_claim_email_teacher",
                    new
                    {
                        bodyMsg = emailBody
                    })
                    .destinationCCO(emails, $"Reclamo de nota - {emailSubject}")
                    .send();

            }

            return true;
        }
        #endregion
    }
}
