﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.OEA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACBusiness.LearningAssessment
{
    public class NrcProgrammned
    {
        #region Propiedades
        public long Id { get; set; }
        public long NrcForSubstituteExamId { get; set; }
        public DateTime InicialRegistrationDate { get; set; }
        public DateTime FinalRegistrationDate { get; set; }
        public DateTime ExamDate { get; set; }
        public bool Active { get; set; }
        public string ClassRoom { get; set; }
        public bool IsPublic { get; set; }
        public string Author { get; set; }
        public DateTime DateCreated { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string Comment { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();
        #endregion Propiedades

        #region Metodos
        /// <summary>
        /// Obtener programaciones de Nrc por lista de NrcForSubstituteExamId
        /// </summary>
        /// <param name="ids"> lista de NrcForSubstituteExamId</param>}
        /// <param name="isPublic"> Validar si es publico?</param>}
        /// <param name="active">Validar si es activo?</param>
        /// <returns></returns>
        public async Task<List<NrcProgrammned>> GetProgrammedByIds(List<long>ids, bool isPublic, bool active) {
            List<NrcProgrammned> data = _nexoContext.oea_nrc_programmned
                .Where(
                    item =>
                        ids.Contains(item.NrcForSubstituteExamId)
                        && (!active || item.Active == active)
                        && (!isPublic || (item.IsPublic == isPublic 
                                                 && item.InicialRegistrationDate <= DateTime.Now && item.FinalRegistrationDate >= DateTime.Now))
                ).ToList().OrderBy(order => order.ExamDate)
                .Select(
                    result => (NrcProgrammned)(result)).ToList();
            return data;
        }

        /// <summary>
        /// Obtener programacion por fecha inicia, fecha final, fecha examen, Aula y Id de nrc
        /// </summary>
        /// <param name="nrcId">ID de nrc</param>
        /// <param name="inicialDate">Fecha inicial</param>
        /// <param name="finalDate">Fecha final</param>
        /// <param name="examDate">Fecha de examen</param>
        /// <param name="classRoom">Aula</param>
        /// <returns></returns>
        public async Task<NrcProgrammned> GetProgrammedByDatesAndClassRoomAndNrcId(long nrcId, DateTime inicialDate, DateTime finalDate, DateTime examDate, string classRoom)
        {
            NrcProgrammned data = _nexoContext.oea_nrc_programmned
                .Where(
                    item =>
                        item.NrcForSubstituteExamId == nrcId
                        && item.InicialRegistrationDate == inicialDate
                        && item.FinalRegistrationDate == finalDate
                        && item.ExamDate == examDate
                        && item.ClassRoom == classRoom
                ).ToList().OrderBy(order => order.ExamDate)
                .Select(
                    result => (NrcProgrammned)(result)).ToList().FirstOrDefault();
            return data;
        }

        /// <summary>
        /// Guardar o actualizar programación de Nrc
        /// </summary>
        /// <param name="idNrcProgrammnedId">Código de nrc al cual pertenece la programación</param>
        /// <param name="data">programación a guardar</param>
        /// <param name="comment">Comentario</param>
        /// <param name="user">Usuario que ejecuta acción</param>
        /// <returns></returns>
        public async Task<bool> SaveProgrammned(long idNrcProgrammnedId, NrcProgrammned data, string comment, string user) {
            if (idNrcProgrammnedId <= 0) throw new Exception("idNrcProgrammnedId no nulo.");
            if (data.InicialRegistrationDate == null || data.FinalRegistrationDate == null || data.ExamDate == null 
                || data.ClassRoom == null || data.ClassRoom.Trim().Length <= 0) throw new Exception("No se aceptan valores nulos");

            data.InicialRegistrationDate = new DateTime(data.InicialRegistrationDate.Year, data.InicialRegistrationDate.Month, data.InicialRegistrationDate.Day,
                                                        data.InicialRegistrationDate.Hour, data.InicialRegistrationDate.Minute, 0);

            data.FinalRegistrationDate = new DateTime(data.FinalRegistrationDate.Year, data.FinalRegistrationDate.Month, data.FinalRegistrationDate.Day,
                                                        data.FinalRegistrationDate.Hour, data.FinalRegistrationDate.Minute, 0);

            data.ExamDate = new DateTime(data.ExamDate.Year, data.ExamDate.Month, data.ExamDate.Day, data.ExamDate.Hour, data.ExamDate.Minute, 0);

            OEANrcProgrammned saveNrcProgrammned = new OEANrcProgrammned();
            NrcProgrammned existProgrammed = await GetProgrammedByDatesAndClassRoomAndNrcId(idNrcProgrammnedId, 
                data.InicialRegistrationDate, data.FinalRegistrationDate, data.ExamDate, data.ClassRoom);
            // existe programación
            if (existProgrammed != null || data.Id > 0)
            {
                saveNrcProgrammned = _nexoContext.oea_nrc_programmned.Find(data.Id);
                if (saveNrcProgrammned == null) saveNrcProgrammned = _nexoContext.oea_nrc_programmned.Find(existProgrammed.Id);
                if (saveNrcProgrammned == null) throw new Exception("No se encontro programación.");
                // validar que alguna fecha se cambio
                if (saveNrcProgrammned.InicialRegistrationDate != data.InicialRegistrationDate ||
                    saveNrcProgrammned.FinalRegistrationDate != data.FinalRegistrationDate ||
                    saveNrcProgrammned.ExamDate != data.ExamDate || saveNrcProgrammned.Active != data.Active ||
                    saveNrcProgrammned.IsPublic != data.IsPublic || saveNrcProgrammned.ClassRoom != data.ClassRoom)
                {
                    saveNrcProgrammned.InicialRegistrationDate = data.InicialRegistrationDate;
                    saveNrcProgrammned.FinalRegistrationDate = data.FinalRegistrationDate;
                    saveNrcProgrammned.ExamDate = data.ExamDate;
                    saveNrcProgrammned.Active = data.Active;
                    saveNrcProgrammned.IsPublic = data.IsPublic;
                    saveNrcProgrammned.ClassRoom = data.ClassRoom;
                    saveNrcProgrammned.ModifiedBy = user;
                    saveNrcProgrammned.DateModified = DateTime.Now;
                    saveNrcProgrammned.Comment = comment;

                }
            }
            // si no existe, registrar
            else {
                saveNrcProgrammned.NrcForSubstituteExamId = idNrcProgrammnedId;
                saveNrcProgrammned.InicialRegistrationDate = data.InicialRegistrationDate;
                saveNrcProgrammned.FinalRegistrationDate = data.FinalRegistrationDate;
                saveNrcProgrammned.ExamDate = data.ExamDate;
                saveNrcProgrammned.Active = data.Active;
                saveNrcProgrammned.IsPublic = data.IsPublic;
                saveNrcProgrammned.ClassRoom = data.ClassRoom;
                saveNrcProgrammned.Author = user;
                saveNrcProgrammned.DateCreated = DateTime.Now;
                saveNrcProgrammned.Comment = comment;
                _nexoContext.oea_nrc_programmned.Add(saveNrcProgrammned);
            }
            _nexoContext.SaveChanges();
            return true;
               
        }
        #endregion Metodos

        #region Operadores explicitos
        public static explicit operator NrcProgrammned(OEANrcProgrammned obj) {
            NrcProgrammned data = new NrcProgrammned()
            {
                Id = obj.Id,
                NrcForSubstituteExamId = obj.NrcForSubstituteExamId,
                InicialRegistrationDate = obj.InicialRegistrationDate,
                FinalRegistrationDate = obj.FinalRegistrationDate,
                ExamDate = obj.ExamDate,
                Active = obj.Active,
                ClassRoom = obj.ClassRoom,
                IsPublic = obj.IsPublic,
                Author = obj.Author,
                DateCreated = obj.DateCreated,
                ModifiedBy = obj.ModifiedBy,
                DateModified = obj.DateModified,
                Comment = obj.Comment
            };
            return data;
        }
        #endregion Operadores explicitos
    }
}
