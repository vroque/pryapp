﻿using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.OEA;
using ACTools.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACBusiness.LearningAssessment
{
    public class SubstituteExamProgrammed
    {
        #region Propiedades
        public long Id { get; set; }
        public long NrcProgrammnedId { get; set; }
        public decimal Pidm { get; set; }
        public int ProgramId { get; set; }
        public string Justification { get; set; }
        public bool Active { get; set; }
        public string Author { get; set; }
        public DateTime DateCreated { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string Comment { get; set; }
        public NrcProgrammned Scheduled { get; set; }
        public NrcForSubstituteExam SubstituteExam { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();
        #endregion Propiedades

        #region Metodos

        /// <summary>
        /// Retorna lista de NRCs que estudiante programo del periodo actual
        /// </summary>
        /// <param name="profile">Perfil de estudiante</param>
        /// <returns></returns>
        public async Task<List<SubstituteExamProgrammed>> GetSubstituteExamProgrammedbyStudentAndByCurrentPeriod(AcademicProfile profile) {
            ACTerm currentTerm = new ACTerm().getCurrentSusbtitute(profile.campus.id, profile.department);
            return ( await GetSubstituteExamProgrammedbyStudentAndTerm(profile.person_id, currentTerm.term));
        }

        /// <summary>
        /// Retorna lista de NRCs que estudiante programo por periodo
        /// </summary>
        /// <param name="pidm">PIDM estudiante</param>
        /// <param name="term">Periodo a Buscar</param>
        /// <returns></returns>
        public async Task<List<SubstituteExamProgrammed>> GetSubstituteExamProgrammedbyStudentAndTerm(decimal pidm, string term) {
            List<SubstituteExamProgrammed> data = _nexoContext.oea_substitute_exam_programmed
                .Join(
                    _nexoContext.oea_nrc_programmned,
                    sep => new { NrcProgrammnedId = sep.NrcProgrammnedId },
                    np => new { NrcProgrammnedId = np.Id },
                    (sep, np) => new { sep, np })
                .Join(
                    _nexoContext.oea_Nrc_for_substitute_exam,
                    np => new { NrcForSubstituteExamId = np.np.NrcForSubstituteExamId },
                    nfs => new { NrcForSubstituteExamId = nfs.Id },
                    (np, nfs) => new { np.sep, np.np, nfs })
                .Where(
                    item =>
                        item.sep.Pidm == pidm
                        && item.nfs.Term == term
                        && item.sep.Active == true).ToList().OrderBy(order => order.np.ExamDate)
                .Select(
                    result => new SubstituteExamProgrammed
                    {
                        Id = result.sep.Id,
                        NrcProgrammnedId = result.sep.NrcProgrammnedId,
                        Pidm = result.sep.Pidm,
                        ProgramId = result.sep.ProgramId,
                        Justification = result.sep.Justification,
                        Active = result.sep.Active,
                        Author = result.sep.Author,
                        DateCreated = result.sep.DateCreated,
                        ModifiedBy = result.sep.ModifiedBy,
                        DateModified = result.sep.DateModified,
                        Comment = result.sep.Comment,
                        Scheduled = (NrcProgrammned)(result.np),
                        SubstituteExam = (NrcForSubstituteExam)(result.nfs),
                    }
                ).ToList();
            return data;

        }

        /// <summary>
        /// Validar si la información que se guardara es valida
        /// </summary>
        /// <param name="profile">PErfil de estudiante</param>
        /// <param name="nrcProgram">Lista de NRCs programados</param>
        /// <param name="term">Periodo</param>
        /// <param name="validate">Validar inasistencias y numero de nrcs presenciales??</param>
        /// <returns>True -> si esta todo bien; False -> si hay algo mal </returns>
        public async Task<bool> ValidateSave(AcademicProfile profile, List<NrcForSubstituteExam> nrcProgram, string term, bool validate) {
            Course objCourse = new Course();
            term = term.Trim();
            // validar datos que envian
            if (profile == null || nrcProgram == null || nrcProgram.Count <= 0 || term == null || term.Trim().Length <= 0)
                throw new Exception("El perfil, nrcProgram o term  no pueden estar nulos o vacios.");
            // validar matricula en periodo indicado
            List<Course> listCourses = objCourse.enrollmentsByTerm(profile, term);
            if (listCourses == null || listCourses.Count <= 0) throw new Exception("El estudiante no tiene matrículas en el" + term);
            // validar que este matriculado en los Nrcs  que selecciono
            foreach (NrcForSubstituteExam program in nrcProgram){
                int validateMat = listCourses.Where(f => f.term == program.Term && f.nrc == program.Nrc).ToList().Count();
                if (validateMat <= 0)
                    throw new Exception("El estudiante no esta matriculado en la asignatura: " + program.SubjetName);
            }
            // Validar que el estudiante ya no haya solicitado examen en esa asignatura
            List<SubstituteExamProgrammed>listExamProgrammed = await GetSubstituteExamProgrammedbyStudentAndTerm(profile.person_id, term);
            foreach (NrcForSubstituteExam program in nrcProgram) {
                int validateProgrammed = listExamProgrammed
                    .Where(f => f.SubstituteExam.Term == program.Term && f.SubstituteExam.Nrc == program.Nrc).ToList().Count();
                if (validateProgrammed > 0) throw new Exception("El estudiante ya tiene programado la asignatura: " + program.SubjetName);
            }
            // validar porcentaje de inasistencia para asignaturas UREG y validateAbsence
            if (validate)
            {
                // validar solo dos asignaturas presenciales
                int NumUreg = nrcProgram.Where(f => f.Department == _acad.DEPA_REG).ToList().Count() 
                              + listExamProgrammed.Where(s => s.SubstituteExam.Department == _acad.DEPA_REG).ToList().Count();
                if (NumUreg > 2) throw new Exception("Se selecciono mas de dos asignaturas presenciales.");
                // validar porcentaje de inasistencia para asignaturas UREG 
                Absence objAbsence = new Absence();
                List<Absence> absenceNrcs = objAbsence.getByStudentAndTerm(profile, term);
                if (absenceNrcs != null && absenceNrcs.Count > 0)
                {
                    foreach (NrcForSubstituteExam program in nrcProgram)
                    {
                        int percentage = absenceNrcs
                            .Where(f =>
                                f.term == program.Term
                                && f.nrc == program.Nrc
                                && program.Department == _acad.DEPA_REG
                                ).Select(f => f.percentage).FirstOrDefault();
                        if (percentage >= 30) throw new Exception("El estudiante tiene " + percentage.ToString() + "% de inasistencia en la asignatura: " + program.SubjetName);
                    }
                }
                
            }
            return  true;
        }

        /// <summary>
        /// Guardar programación de examen x ultimo periodo
        /// </summary>
        /// <param name="profile">Perfil estudiante</param>
        /// <param name="user">Usuario que guarda</param>
        /// <param name="nrcProgram">Lista de asignaturas programadas</param>
        /// <param name="justification">Justificación</param>
        /// <param name="validate">Validar inasistencias y numero de nrcs presenciales??</param>
        /// <returns></returns>
        public async Task<bool> SaveSustiByStudentAndLastTerm(Student student, string user, List<NrcForSubstituteExam> nrcProgram, string justification, bool validate)
        {
            ACTerm currentTerm = new ACTerm().getCurrentSusbtitute(student.profile.campus.id, student.profile.department);
            return (await SaveSustiByStudentAndTerm(student, user, nrcProgram, justification, currentTerm.term, validate));
        }

        /// <summary>
        /// Guardar programación de examen x periodo
        /// </summary>
        /// <param name="profile">Perfil de estudiante</param>
        /// <param name="user">USuario que guarda</param>
        /// <param name="nrcProgram">Lista de asignaturas programadas</param>
        /// <param name="justification">Justificación</param>
        /// <param name="term">Periodo</param>
        /// <param name="validate">Validar inasistencias y numero de nrcs presenciales??</param>
        /// <returns></returns>
        public async Task<bool> SaveSustiByStudentAndTerm(Student student, string user, List<NrcForSubstituteExam> nrcProgram, string justification, string term, bool validate)
        {
            // validar
            bool validateSave = await ValidateSave(student.profile, nrcProgram, term, validate);
            if (!validateSave) throw new Exception("Datos no validos");
            List<OEASubstituteExamProgrammed> listDataSave = new List<OEASubstituteExamProgrammed>();
            foreach (NrcForSubstituteExam program in nrcProgram)
            {
                listDataSave.Add(
                    new OEASubstituteExamProgrammed()
                    {
                        NrcProgrammnedId = program.ScheduledSelected.Id,
                        Pidm = student.person_id,
                        ProgramId = int.Parse(student.profile.program.id),
                        Justification = justification,
                        Active = true,
                        Author = user,
                        DateCreated = DateTime.Now,
                    });
            }
            _nexoContext.oea_substitute_exam_programmed.AddRange(listDataSave);
            int save = _nexoContext.SaveChanges();
            if (save <= 0) throw new Exception("Error al guardar solicitud.");
            try
            {
                // Generar deuda por examen sustitutorio
                object generateDebt = _nexoContext.oea_spGenerateDebtSubstituteExamByStudent("UCCI", student.profile.campus.id, 
                                                                                             term, student.profile.program.id,
                                                                                             student.profile.person_id).ToList();
            }
            catch (Exception e)
            {
                // Desactivar Examenes solicitados
                listDataSave.ForEach(
                    item =>
                        {
                            item.Active = false;
                            item.DateModified = DateTime.Now;
                            item.ModifiedBy = user;
                            item.Comment = "Error al generar deuda.";
                        }
                    );
                _nexoContext.SaveChanges();
                throw new Exception(e.Message.ToString());
            }
            // Enviar Correo
            MailSender mail = new MailSender();
            string bodyMsg = "";
            nrcProgram.ForEach(
                    item => bodyMsg = $"{bodyMsg}<tr><td>{item.Nrc}</td><td>{item.SubjetName}</td><td>{item.ScheduledSelected.ExamDate}</td><td>{item.ScheduledSelected.ClassRoom}</td>"
                );
            mail.compose("oea_save_substitute_exam",
                new
                {
                    names = student.first_name,
                    subjets = bodyMsg,
                    justification = justification
                })
                .destination($"{student.id}@continental.edu.pe", "Tu examen sustitutorio fue programado")
                .send();
            return true;
        }

        /// <summary>
        /// Modificar un examen sustitutorio programado (Lista)
        /// </summary>
        /// <param name="user">Usuario de modificación</param>
        /// <param name="substituteExams">Sustitutorios que se modificara</param>
        /// <param name="comment">Comentario</param>
        /// <returns></returns>
        public async Task<bool> ModifySustiByStudent(Student student, string user, List<SubstituteExamProgrammed> substituteExams, string comment)
        {
            bool save;
            foreach (SubstituteExamProgrammed item in substituteExams)
            {
                save = await ModifySustiByStudent(student, user, item, comment);
            }
            return true;
        }

        /// <summary>
        /// Modificar un examen sustitutorio programado
        /// </summary>
        /// <param name="user">Usuario de modificación</param>
        /// <param name="substituteExam">Susti que se modificara</param>
        /// <param name="comment">Comentario</param>
        /// <returns></returns>
        public async Task<bool> ModifySustiByStudent(Student student, string user, SubstituteExamProgrammed substituteExam, string comment)
        {
            
            OEASubstituteExamProgrammed modifySubstituteExam = _nexoContext.oea_substitute_exam_programmed.Find(substituteExam.Id);
            if (student == null) throw new Exception("No existe estudiante");
            if (modifySubstituteExam.Active != substituteExam.Active)
            {
                modifySubstituteExam.Active = substituteExam.Active;
                modifySubstituteExam.ModifiedBy = user;
                modifySubstituteExam.DateModified = DateTime.Now;
                modifySubstituteExam.Comment = comment;
                _nexoContext.SaveChanges();
                object generateDebt = _nexoContext.oea_spGenerateDebtSubstituteExamByStudent("UCCI", student.profile.campus.id,
                                                                                             substituteExam.SubstituteExam.Term, student.profile.program.id,
                                                                                             student.profile.person_id).ToList();
            }
            return true;

        }
        #endregion Metodos
    }
}
