﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.OEA;
using ACTools.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using _oea = ACTools.Constants.LearningAssessmentConstants;

namespace ACBusiness.LearningAssessment
{
    public class ReportNoteClaim
    {
        #region Propiedades
        public long Id { get; set; }
        public long NoteClaimId { get; set; }
        public bool Proceeds { get; set; }
        public string ScoreCorrect { get; set; }
        public string Reason { get; set; }
        public DateTime? DateChange { get; set; }
        public long Status { get; set; }
        public bool Active { get; set; }
        public bool? Approved { get; set; }
        public decimal Author { get; set; }
        public DateTime DateCreated { get; set; }
        public decimal? ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string Comment { get; set; }
        public bool IsValid { get; set; }
        public string MsgValid { get; set; }
        public List<FileNoteClaim> Files { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();
        #endregion Propiedades

        #region Metodos
        /// <summary>
        /// Obtener Reporte por activo de reclamo
        /// </summary>
        /// <param name="claimId"></param>
        /// <returns></returns>
        public ReportNoteClaim GetReportActiveByClaim(long claimId)
        {

            ReportNoteClaim data = _nexoContext.OEA_Report_Note_Claim
                .Where(row =>
                        row.NoteClaimId == claimId
                        && row.Active == true
                        && (row.Status == _oea.OEA_STATUS_NOTE_CLAIM_REPORT_PENDING_REVISION || 
                            row.Status == _oea.OEA_STATUS_NOTE_CLAIM_REPORT_APPROVED))
                .ToList()
                .Select(result => (ReportNoteClaim)(result))
                .FirstOrDefault();

            if (data != null)
            {
                FileNoteClaim objfile = new FileNoteClaim();
                data.Files = objfile.getFilesByClaimIdAndReportId(data.NoteClaimId, data.Id);
            }

            return data;
        }

        /// <summary>
        /// Validar informe a guardar
        /// </summary>
        /// <param name="data">Informe que se quiere guardar</param>
        /// <returns></returns>
        public Tuple<bool, string> ValidateSave(OEAReportNoteClaim data)
        {
            // Razon o motivo no puede estar vacio
            if (data.Reason == null || data.Reason.Trim().Length <= 0)
            {
                return Tuple.Create(false, "La justificación no puede estar vacío.");
            }

            if (data.Proceeds)
            {
                // Validar que fecha de cambio de nota no sea nul
                if (data.DateChange == null)
                {
                    return Tuple.Create(false, "Fecha de cambio de nota no puede estar vacío.");
                }
                // validar que la fecha de cambiado de nota sea como 2 días mas de hoy
                if ( data.DateChange < (DateTime.Today).AddDays(2))
                {
                    return Tuple.Create(false, "La fecha para cambiar la nota debe ser como mínimo dos días más a hoy.");
                }
                decimal score;

                if (data.ScoreCorrect == null || data.ScoreCorrect.Trim().Length <= 0
                    || !decimal.TryParse(data.ScoreCorrect, out score))
                {
                    return Tuple.Create(false, "Nota correcta invalida.");
                }
                //Nota correcta entre 0 y 20
                if (score <= 0 || score > 20)
                {
                    return Tuple.Create(false, "La nota debe está en el rango de 0 - 20.");
                }
            }
            NoteClaim objNoteClaim = new NoteClaim();
            NoteClaim dataClaim = objNoteClaim.GetSimpleDataById(data.NoteClaimId);
            // Validar que reclamo exista
            if (dataClaim == null)
            {
                return Tuple.Create(false, "Reclamo invalido.");
            }
            // Validar que la persona que redacta este asignado al reclamo
            if (dataClaim.PidmTeacher != data.Author)
            {
                return Tuple.Create(false, "No está autorizado para responder el reclamo.");
            }

            if (dataClaim.Status != _oea.OEA_STATUS_NOTE_CLAIM_TEACHER)
            {
                return Tuple.Create(false, "El reclamo no esta en pendiente de informe.");
            }

            ReportNoteClaim rpt = GetReportActiveByClaim(data.NoteClaimId);

            if (rpt != null)
            {
                return Tuple.Create(false, "El reclamo ya tiene informe pendiente de verificación");
            }

            return Tuple.Create(true, "");
        }
        /// <summary>
        /// Guardar informe
        /// </summary>
        /// <param name="data">información del informe</param>
        /// <param name="user">Usuario que esta guardando informe</param>
        /// <returns></returns>
        public ReportNoteClaim Save(ReportNoteClaim data, decimal user)
        {
            OEAReportNoteClaim saveReport = new OEAReportNoteClaim();
            saveReport.NoteClaimId = data.NoteClaimId;
            saveReport.Author = user;
            saveReport.Proceeds = data.Proceeds;
            saveReport.Reason = data.Reason.Trim();
            saveReport.Active = true;
            saveReport.Status = _oea.OEA_STATUS_NOTE_CLAIM_REPORT_PENDING_REVISION;
            saveReport.DateCreated = DateTime.Now;

            if (saveReport.Proceeds)
            {
                saveReport.ScoreCorrect = data.ScoreCorrect;
                saveReport.DateChange = data.DateChange;
            }

            Tuple<bool, string> validate = ValidateSave(saveReport);
            data.IsValid = validate.Item1;
            data.MsgValid = validate.Item2;

            if (data.IsValid)
            {
                NoteClaim objClaim = new NoteClaim();
                _nexoContext.OEA_Report_Note_Claim.Add(saveReport);
                _nexoContext.SaveChanges();
                objClaim.SaveNextStatusClaim(saveReport.NoteClaimId, user);
                data = (ReportNoteClaim)(saveReport);
                data.IsValid = validate.Item1;
                data.MsgValid = validate.Item2;
            }

            return data;
        }

        public bool ProcessReport(ReportNoteClaim data, decimal user, List<NoteClaim.Module> moduleListUser)
        {
            // Validar Datos
            if (data == null || data.Comment == null || data.Comment.Trim() == "" || data.Approved == null)
            {
                throw new Exception("Valores Invalidos");
            }
            // Validar estado de reporte
            OEAReportNoteClaim saveReport = _nexoContext.OEA_Report_Note_Claim.Find(data.Id);

            if (saveReport == null || !saveReport.Active
                || (saveReport.Status != _oea.OEA_STATUS_NOTE_CLAIM_REPORT_PENDING_REVISION))
            {
                throw new Exception("Reporte en estado invalido.");
            }
            // Validar estado de reclamo
            NoteClaim objClaim = new NoteClaim();
            NoteClaim validateClaim = objClaim.GetAllDataById(saveReport.NoteClaimId);

            if (validateClaim == null || validateClaim.Status != _oea.OEA_STATUS_NOTE_CLAIM_INVOLVED_AREA)
            {
                throw new Exception("Reclamo invalido.");
            }
            // Validar que la persona que tenga accesos al modulo de la oficina que indica
            objClaim.validateAccessOffice((int)validateClaim.OfficeId, moduleListUser);
            // Guardar
            saveReport.Approved = data.Approved;
            saveReport.ModifiedBy = user;
            saveReport.DateModified = DateTime.Now;
            saveReport.Comment = data.Comment;

            if ((bool)saveReport.Approved)
            {
                saveReport.Status = _oea.OEA_STATUS_NOTE_CLAIM_REPORT_APPROVED;
                objClaim.SaveNextStatusClaim(saveReport.NoteClaimId, user);
            }
            else
            {
                saveReport.Active = false;
                saveReport.Status = _oea.OEA_STATUS_NOTE_CLAIM_REPORT_DISAPPROVED;
                // Actualizar reclamo
                OEANoteClaim updateClaim = _nexoContext.OEA_Note_Claim.Find(saveReport.NoteClaimId);
                if (updateClaim == null)
                {
                    throw new Exception("No existe reclamo.");
                }
                updateClaim.Status = _oea.OEA_STATUS_NOTE_CLAIM_TEACHER;
                updateClaim.ModifiedBy = user;
                updateClaim.DateModified = DateTime.Now;
            }
            _nexoContext.SaveChanges();
            // Enviar Correo
            MailSender mail = new MailSender();
            mail.compose("oea_note_claim_validate_report",
                new
                {
                    numeration = validateClaim.NoteClaimNumber,
                    year = validateClaim.Year,
                    name = validateClaim.TeacherData.first_name,
                    status = (bool)saveReport.Approved ? "Aprobado" : "Rechazado",
                    reason = saveReport.Comment,
                    msgAdd = (bool)saveReport.Approved ? "" : 
                             "Acceder al siguiente <a href='https://estudiantes.continental.edu.pe/backoffice/reclamo-notas-docente/reclamos'>enlace </a> para volver a registrar el informe.",
                })
                .destination($"{validateClaim.TeacherData.userName}@continental.edu.pe", "Reporte rechazado")
                .send();

            return true;
        }
        #endregion Metodos

        #region Operadores explicitos
        public static explicit operator ReportNoteClaim(OEAReportNoteClaim obj)
        {
            ReportNoteClaim data = new ReportNoteClaim()
            {
                Id = obj.Id,
                NoteClaimId = obj.NoteClaimId,
                Proceeds = obj.Proceeds,
                ScoreCorrect = obj.ScoreCorrect,
                Reason = obj.Reason,
                DateChange = obj.DateChange,
                Status = obj.Status,
                Active = obj.Active,
                Approved = obj.Approved,
                Author = obj.Author,
                DateCreated = obj.DateCreated,
                ModifiedBy = obj.ModifiedBy,
                DateModified = obj.DateModified,
                Comment = obj.Comment,
            };

            return data;
        }

        public static explicit operator OEAReportNoteClaim(ReportNoteClaim obj)
        {
            OEAReportNoteClaim data = new OEAReportNoteClaim()
            {
                Id = obj.Id,
                NoteClaimId = obj.NoteClaimId,
                Proceeds = obj.Proceeds,
                ScoreCorrect = obj.ScoreCorrect,
                Reason = obj.Reason,
                DateChange = obj.DateChange,
                Status = obj.Status,
                Active = obj.Active,
                Author = obj.Author,
                DateCreated = obj.DateCreated,
                ModifiedBy = obj.ModifiedBy,
                DateModified = obj.DateModified,
                Comment = obj.Comment,
            };

            return data;
        }

        #endregion Operadores explicitos
    }
}
