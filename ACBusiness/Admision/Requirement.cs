using ACBusiness.Academic;
using System.Collections.Generic;
using System.Linq;
using ACPermanence.Contexts.BANNER;

namespace ACBusiness.Admision
{
    /// <summary>
    /// Requisitos de admision por persona
    /// </summary>
    public class Requirement
    {
        #region Properties

        public string id { get; set; }
        public string name { get; set; }
        public decimal person_id { get; set; }
        public bool status { get; set; }
        public bool Required { get; set; }
        public bool Extended { get; set; }

        private readonly BNContext _bnContext = new BNContext();

        private const string True = "1";
        private const string False = "0";

        #endregion Properties

        #region Methods

        public List<Requirement> GetByProfile(AcademicProfile profile)
        {
            List<Requirement> list = GetPendingByStudent(profile.person_id, profile.department, profile.program.id);
            return list;
        }

        /// <summary>
        /// Documentos pendientes
        /// </summary>
        /// <param name="pidm"></param>
        /// <param name="department"></param>
        /// <param name="program"></param>
        /// <returns></returns>
        public List<Requirement> GetPendingByStudent(decimal pidm, string department, string program)
        {
            List<Requirement> list = _bnContext.p_requisitos_alumno(pidm)
                .Where(f => f.PROGRAM == program && f.DEPARTAMENT == department &&
                            f.DELIVERED == False && f.EXTENSION == False)
                .ToList()
                .Select(f => new Requirement
                {
                    id = f.CODEREQUERIMENT,
                    name = f.REQUERIMENTDESCRIPTION,
                    status = f.DELIVERED == True,
                    person_id = f.PIDM,
                    Required = f.REQUIRED == True,
                    Extended = f.EXTENSION == True
                }).ToList();

            return list;
        }

        /// <summary>
        /// Documentos obligatorios
        /// </summary>
        /// <param name="pidm"></param>
        /// <param name="department"></param>
        /// <param name="program"></param>
        /// <returns></returns>
        public List<Requirement> GetMandatoryDocumentsByStudent(decimal pidm, string department, string program)
        {
            List<Requirement> list = GetPendingByStudent(pidm, department, program);

            List<Requirement> blockingList = list.Where(w => w.Required && !w.Extended).ToList();

            return blockingList;
        }

        /// <summary>
        /// Documentos por estudiante
        /// </summary>
        /// <param name="pidm"></param>
        /// <param name="department"></param>
        /// <param name="program"></param>
        /// <returns></returns>
        public List<Requirement> GetAllByStudent(decimal pidm, string department, string program)
        {
            List<Requirement> list = _bnContext.p_requisitos_alumno(pidm)
                .Where(f => f.PROGRAM == program && f.DEPARTAMENT == department)
                .ToList()
                .Select(f => new Requirement
                {
                    id = f.CODEREQUERIMENT,
                    name = f.REQUERIMENTDESCRIPTION,
                    status = f.DELIVERED == True,
                    person_id = f.PIDM,
                    Required = f.REQUIRED == True,
                    Extended = f.EXTENSION == True
                }).ToList();

            return list;
        }

        #endregion Methods
    }
}