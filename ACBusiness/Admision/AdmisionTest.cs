﻿using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.ADM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Admision
{
    /// <summary>
    /// Codigo traido desde la version anterior
    /// </summary>
    public class AdmisionTest
    {
        public string IDAlumno { get; set; }
        public string IDDependencia { get; set; }
        public string IDSede { get; set; }
        public string IDPerAcad { get; set; }
        public string IDSeccionC { get; set; }
        public int c_IDTest { get; set; }
        public string c_NombreTest { get; set; }
        public int c_IDPreguntaByTest { get; set; }
        public string c_Pregunta { get; set; }
        public string c_alt1 { get; set; }
        public string c_alt2 { get; set; }
        public string c_alt3 { get; set; }
        public string c_alt4 { get; set; }
        public string c_alt5 { get; set; }
        public string c_UrlImagen { get; set; }
        public string result { get; set; }
        public string NombreEscuela { get; set; }
        public string Modalidad { get; set; }

        public DateTime IDExamen { get; set; }
        public string FechaExamen_str
        {
            get
            {
                return this.IDExamen.ToString("dd/MM/yyyy");
            }
        }
        public static List<AdmisionTest> collection(List<TestAdmisionsp_PreguntasByTest_CF> list)
        {
            List<AdmisionTest> lista_pregunta = new List<AdmisionTest>();
            foreach (TestAdmisionsp_PreguntasByTest_CF obj in list)
            {
                AdmisionTest _tmp_obj = new AdmisionTest();
                _tmp_obj.c_IDTest = obj.ID;
                _tmp_obj.c_NombreTest = obj.NombreTest;
                _tmp_obj.c_IDPreguntaByTest = obj.IDPreguntaByTest;
                _tmp_obj.c_Pregunta = obj.Pregunta;
                _tmp_obj.c_alt1 = obj.Alt1;
                _tmp_obj.c_alt2 = obj.Alt2;
                _tmp_obj.c_alt3 = obj.Alt3;
                _tmp_obj.c_alt4 = obj.Alt4;
                _tmp_obj.c_alt5 = obj.Alt5;
                if (obj.UrlImagen != null)
                {
                    _tmp_obj.c_UrlImagen = obj.UrlImagen;
                }
                else
                {
                    _tmp_obj.c_UrlImagen = "https://lh6.googleusercontent.com/-ttgCxJwaR_E/Vig06g0uwYI/AAAAAAAAASg/FdNWZtYTIcs/s10-no/_.png";
                }
                _tmp_obj.result = obj.Respuesta;
                lista_pregunta.Add(_tmp_obj);
            }
            return lista_pregunta;
        }
        public static List<AdmisionTest> Preguntas_By_IDTest(int @c_IDTest, string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @d_IDExamen, string @c_IDAlumno)
        {
           DBOContext context =new DBOContext();
            List<TestAdmisionsp_PreguntasByTest_CF> pila = context.TestAdmisionsp_PreguntasByTest_CF(@c_IDTest, @c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @d_IDExamen, @c_IDAlumno).ToList();
            List<AdmisionTest> a = collection(pila);
            return a;
        }
        public static List<AdmisionTest> collection(List<TestAdmisionsp_GetDatosAlumnoTestAdmision_CF> list)
        {
            List<AdmisionTest> datos_alumno_examen = new List<AdmisionTest>();
            foreach (TestAdmisionsp_GetDatosAlumnoTestAdmision_CF obj in list)
            {
                AdmisionTest _tmp_obj = new AdmisionTest();
                _tmp_obj.IDAlumno = obj.IDAlumno;
                _tmp_obj.IDDependencia = obj.IDDependencia;
                _tmp_obj.IDSede = obj.IDSede;
                _tmp_obj.IDExamen = obj.IDExamen;
                _tmp_obj.IDPerAcad = obj.IDPerAcad;
                _tmp_obj.IDSeccionC = obj.IDSeccionC;
                _tmp_obj.NombreEscuela = obj.NomEscuela;
                _tmp_obj.Modalidad = obj.Modalidad;
                datos_alumno_examen.Add(_tmp_obj);
            }
            return datos_alumno_examen;
        }
        public static List<AdmisionTest> Get_DatosAlumnoExamen(string IDAlumno)
        {
           DBOContext context =new DBOContext();
            List<TestAdmisionsp_GetDatosAlumnoTestAdmision_CF> pila = context.TestAdmisionsp_GetDatosAlumnoExamen_CF(IDAlumno).ToList();
            List<AdmisionTest> a = collection(pila);
            return a;
        }
        public static List<AdmisionTest> collection(List<TestAdmisionsp_Update_tblTestRespuesta_CF> list)
        {
            List<AdmisionTest> update_result = new List<AdmisionTest>();
            foreach (TestAdmisionsp_Update_tblTestRespuesta_CF obj in list)
            {
                AdmisionTest _tmp_obj = new AdmisionTest();
                _tmp_obj.result = obj.RESULT;
                update_result.Add(_tmp_obj);
            }
            return update_result;
        }
        public static List<AdmisionTest> Update_RespuestaTest(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @c_IDExamen, string @c_IDAlumno, int @c_IDTest, int @c_IDPreguntaByTest, string @c_Respuesta)
        {
           DBOContext context =new DBOContext();
            List<TestAdmisionsp_Update_tblTestRespuesta_CF> pila = context.TestAdmisionsp_Update_tblTestRespuesta_CF(@c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @c_IDExamen, @c_IDAlumno, @c_IDTest, @c_IDPreguntaByTest, @c_Respuesta).ToList();
            List<AdmisionTest> a = collection(pila);
            return a;
        }
        public static List<AdmisionTest> collection(List<TestAdmisionsp_CompletoTest_CF> list)
        {
            List<AdmisionTest> estadotest = new List<AdmisionTest>();
            foreach (TestAdmisionsp_CompletoTest_CF obj in list)
            {
                AdmisionTest _tmp_obj = new AdmisionTest();
                _tmp_obj.result = obj.RESULT;
                estadotest.Add(_tmp_obj);
            }
            return estadotest;
        }
        public static List<AdmisionTest> CompletoTest(string @c_IDDependencia, string @c_IDSede, string @c_IDSeccionC, string @c_IDPerAcad, DateTime @d_IDExamen, string @c_IDAlumno, int @c_IDTest)
        {
           DBOContext context =new DBOContext();
            List<TestAdmisionsp_CompletoTest_CF> pila = context.TestAdmisionCompletoTest_CF(@c_IDDependencia, @c_IDSede, @c_IDSeccionC, @c_IDPerAcad, @d_IDExamen, @c_IDAlumno, @c_IDTest).ToList();
            List<AdmisionTest> a = collection(pila);
            return a;
        }
    }
}
