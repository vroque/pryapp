﻿using System.Linq;
using _admission = ACTools.Constants.AdmissionConstant;
using ACPermanence.DataBases.BANNER.SATURN;
using ACPermanence.Contexts.BANNER;
using ACBusiness.Academic;
using System;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.Admision
{
    /// <summary>
    /// Tipos de Admisión
    /// </summary>
    public class AdmisionType
    {
        #region properties

        public string id { get; set; }
        public string name { get; set; }
        public bool is_pronabec { get; set; }

        #endregion properties

        #region statics methods

        /// <summary>
        /// obtiene los detalles de la modalidad de admision desde su codigo
        /// * adicionalmente busca modalidades adicionales por el perfil en el 
        /// * caso que sea la modalidad 99(por cambio de plan)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        public static AdmisionType get(string id, AcademicProfile profile = null, string source = "banner")
        {
            BNContext db = new BNContext();
            AdmisionType type;
            if (source != "banner") // se asume apec
            {
                var dboContext = new DBOContext();
                var equi = dboContext.tbl_equivalencia_modalidad_admision
                    .FirstOrDefault(f => f.IDModalidadPostu == id);
                if (equi == null)
                    throw new Exception($"la modalidad de admision {id} no tiene equivalente");
                id = equi.IDModRecruiter;
            }

            //verificar pustilaciones anteriores
            type = db.dim_admition_types.Find(id);

            if (type == null) throw new Exception($"no existe la modalidad de admisión {id}");

            return type;
        }


        /// <summary>
        /// Verifica si una modalidad de ingreso es PRONABEC o no.
        /// <para>
        /// NOTE: Dejar de usar este método cuando "algún día" DIM.tblAdmType.admPronabec tenga info verídica
        /// </para>
        /// </summary>
        /// <param name="adm_type">Código modalidad de postulación</param>
        /// <returns></returns>
        private static bool isPronabec(string adm_type)
        {
            return _admission.PRONABEC_LIST.Contains(adm_type);
        }

        #endregion statics methods

        #region implicite operators

        public static implicit operator AdmisionType(SZBADMT obj)
        {
            if (obj == null) return null;

            var type = new AdmisionType
            {
                id = obj.SZBADMT_CODE,
                name = obj.SZBADMT_DESCRIPTION,
                is_pronabec = isPronabec(obj.SZBADMT_CODE)
            };

            return type;
        }

        #endregion implicite operators
    }
}