﻿using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.ADM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Admision
{
    /// <summary>
    /// Codigo traido desde la version anterior
    /// </summary>
    public class AdmisionTestState
    {
        public int IDTest { get; set; }
        public string NombreTest_str { get; set; }
        public string Estado_str { get; set; }
        public string Orden_str { get; set; }
        public int Orden { get; set; }
        public string Indicador_str { get; set; }
        public string resultado_str { get; set; }
        public string Opcion_str { get; set; }
        public static List<AdmisionTestState> collection(List<TestAdmisionsp_ResultadoEscalaMotivacion_CF> list)
        {
            List<AdmisionTestState> ResultadoTest = new List<AdmisionTestState>();
            foreach (TestAdmisionsp_ResultadoEscalaMotivacion_CF obj in list)
            {
                AdmisionTestState _tmp_obj = new AdmisionTestState();
                _tmp_obj.Orden_str = obj.Orden.ToString();
                if (_tmp_obj.Orden_str == "1")
                {
                    _tmp_obj.resultado_str = obj.Resultado;
                }
                else
                {
                    _tmp_obj.Indicador_str = obj.Resultado;
                }


                ResultadoTest.Add(_tmp_obj);
            }
            return ResultadoTest;
        }
        public static List<AdmisionTestState> ResultadoTestMotivacion(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @d_IDExamen, string @c_IDAlumno)
        {
           DBOContext context =new DBOContext();
            List<TestAdmisionsp_ResultadoEscalaMotivacion_CF> pila = context.ResultadoTestmotivacion(@c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @d_IDExamen, @c_IDAlumno).ToList();
            List<AdmisionTestState> a = collection(pila);
            return a;
        }
        public static List<AdmisionTestState> collection(List<TestAdmisionsp_ResultadoInteresesOcu1_CF> list)
        {
            List<AdmisionTestState> ResultadosTest = new List<AdmisionTestState>();
            foreach (TestAdmisionsp_ResultadoInteresesOcu1_CF obj in list)
            {
                AdmisionTestState _tmp_obj = new AdmisionTestState();
                _tmp_obj.Indicador_str = obj.CAMPOS.ToString();
                _tmp_obj.resultado_str = obj.INTENSIDAD.ToString();
                ResultadosTest.Add(_tmp_obj);
            }
            return ResultadosTest;
        }
        public static List<AdmisionTestState> ResultadoTestOcupacional_1(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @c_IDExamen, string @c_IDAlumno)
        {
           DBOContext context =new DBOContext();
            List<TestAdmisionsp_ResultadoInteresesOcu1_CF> pila = context.ResultadoInteresesOcu1(@c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @c_IDExamen, @c_IDAlumno).ToList();
            List<AdmisionTestState> a = collection(pila);
            return a;
        }
        public static List<AdmisionTestState> collection(List<TestAdmisionsp_ResultadoInteresesOcu2_CF> list)
        {
            List<AdmisionTestState> ResultadosTest = new List<AdmisionTestState>();
            foreach (TestAdmisionsp_ResultadoInteresesOcu2_CF obj in list)
            {
                AdmisionTestState _tmp_obj = new AdmisionTestState();
                _tmp_obj.Indicador_str = obj.CAMPOS.ToString();
                _tmp_obj.resultado_str = obj.INTENSIDAD.ToString();
                ResultadosTest.Add(_tmp_obj);
            }
            return ResultadosTest;
        }
        public static List<AdmisionTestState> ResultadoTestOcupacional_2(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @c_IDExamen, string @c_IDAlumno)
        {
           DBOContext context =new DBOContext();
            List<TestAdmisionsp_ResultadoInteresesOcu2_CF> pila = context.ResultadoInteresesOcu2(@c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @c_IDExamen, @c_IDAlumno).ToList();
            List<AdmisionTestState> a = collection(pila);
            return a;
        }
        public static List<AdmisionTestState> collection(List<TestAdmisionsp_ResultadoInteresesOcu4_CF> list)
        {
            List<AdmisionTestState> ResultadosTest = new List<AdmisionTestState>();
            foreach (TestAdmisionsp_ResultadoInteresesOcu4_CF obj in list)
            {
                AdmisionTestState _tmp_obj = new AdmisionTestState();
                _tmp_obj.Opcion_str = obj.CAMPOS.ToString();
                _tmp_obj.resultado_str = obj.INTENSIDAD.ToString();
                ResultadosTest.Add(_tmp_obj);
            }
            return ResultadosTest;
        }
        public static List<AdmisionTestState> ResultadoTestOcupacional_4(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @c_IDExamen, string @c_IDAlumno)
        {
           DBOContext context =new DBOContext();
            List<TestAdmisionsp_ResultadoInteresesOcu4_CF> pila = context.ResultadoInteresesOcu4(@c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @c_IDExamen, @c_IDAlumno).ToList();
            List<AdmisionTestState> a = collection(pila);
            return a;
        }
        public static List<AdmisionTestState> collection(List<TestAdmisionsp_ResultadoEscalaHabilidades_CF> list)
        {
            List<AdmisionTestState> ResultadosTest = new List<AdmisionTestState>();
            foreach (TestAdmisionsp_ResultadoEscalaHabilidades_CF obj in list)
            {
                AdmisionTestState _tmp_obj = new AdmisionTestState();
                _tmp_obj.Orden_str = obj.Area;
                _tmp_obj.resultado_str = obj.Resultado;
                ResultadosTest.Add(_tmp_obj);
            }
            return ResultadosTest;
        }
        public static List<AdmisionTestState> ResultadoTestHabilidades(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @c_IDExamen, string @c_IDAlumno, string @c_IDTest)
        {
           DBOContext context =new DBOContext();
            List<TestAdmisionsp_ResultadoEscalaHabilidades_CF> pila = context.ResultadoTestHabilidades(@c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @c_IDExamen, @c_IDAlumno, @c_IDTest).ToList();
            List<AdmisionTestState> a = collection(pila);
            return a;
        }

    }
}
