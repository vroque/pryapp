﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.TermsAndConditions
{
    /// <summary>
    /// Términos y Condiciones
    /// </summary>
    public class TermsAndConditions
    {
        #region Properties

        public int Id { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public bool IsFile { get; set; }
        public string Detail { get; set; }
        public string Url { get; set; }
        public DateTime PublicationDate { get; set; }
        public bool Active { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        /// <summary>
        /// Tipo de términos y condiciones para módulos
        /// </summary>
        private const int ModuleTerms = 2;

        #endregion Properties

        #region Constructors

        public TermsAndConditions()
        {
        }

        public TermsAndConditions(TermsAndConditions termsAndConditions)
        {
            Id = termsAndConditions.Id;
            Type = termsAndConditions.Type;
            Name = termsAndConditions.Name;
            IsFile = termsAndConditions.IsFile;
            Detail = termsAndConditions.Detail;
            Url = termsAndConditions.Url;
            PublicationDate = termsAndConditions.PublicationDate;
            Active = termsAndConditions.Active;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Terms and conditions list
        /// </summary>
        /// <returns>Terms and conditions list</returns>
        public async Task<List<TermsAndConditions>> GetTermsListAsync()
        {
            List<CAUTermsAndConditions> termsList = await _nexoContext.cau_terms_conditions
                .OrderByDescending(o => o.PublicationDate).ThenByDescending(o => o.Id).ToListAsync();

            return termsList.Select(s => (TermsAndConditions) s).ToList();
        }

        /// <summary>
        /// Terms and conditions for modules
        /// </summary>
        /// <returns>Terms and conditions for modules list</returns>
        public async Task<List<TermsAndConditions>> GetModuleTermsAsync()
        {
            List<CAUTermsAndConditions> moduleTerms =
                await _nexoContext.cau_terms_conditions.Where(w => w.Type == ModuleTerms && w.Active).ToListAsync();

            return moduleTerms.Select(s => (TermsAndConditions) s).ToList();
        }

        /// <summary>
        /// Terms and conditions by Id
        /// </summary>
        /// <param name="termsAndConditionsId">Terms and conditions id</param>
        /// <returns>Terms and conditions</returns>
        public async Task<TermsAndConditions> GetByIdAsync(int termsAndConditionsId)
        {
            var termsAndConditions =
                await _nexoContext.cau_terms_conditions.FirstOrDefaultAsync(w => w.Id == termsAndConditionsId);

            return (TermsAndConditions) termsAndConditions;
        }

        /// <summary>
        /// Create new Terms and conditions
        /// </summary>
        /// <returns></returns>
        public async Task<TermsAndConditions> CreateAsync()
        {
            var terms = new CAUTermsAndConditions();
            Compose(ref terms);
            if (terms == null) return null;

            _nexoContext.cau_terms_conditions.Add(terms);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (TermsAndConditions) terms : null;
        }

        /// <summary>
        /// Update Terms and conditions
        /// </summary>
        /// <returns></returns>
        public async Task<TermsAndConditions> UpdateAsync()
        {
            var terms = await _nexoContext.cau_terms_conditions.FirstOrDefaultAsync(w => w.Id == Id);

            if (terms == null) return null;

            Compose(ref terms);
            var i = await _nexoContext.SaveChangesAsync();

            return i == 1 ? (TermsAndConditions) terms : null;
        }

        /// <summary>
        /// Delete Terms and conditions
        /// </summary>
        /// <param name="termsId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int termsId)
        {
            var findTerms = await _nexoContext.cau_terms_conditions.FirstOrDefaultAsync(w => w.Id == termsId);
            if (findTerms == null) return false;

            _nexoContext.cau_terms_conditions.Remove(findTerms);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1;
        }

        private void Compose(ref CAUTermsAndConditions cauTermsAndConditions)
        {
            if (cauTermsAndConditions == null) return;

            cauTermsAndConditions.Id = Id;
            cauTermsAndConditions.Type = Type;
            cauTermsAndConditions.Name = Name;
            cauTermsAndConditions.IsFile = IsFile;
            cauTermsAndConditions.Detail = Detail;
            cauTermsAndConditions.Url = Url;
            cauTermsAndConditions.PublicationDate = PublicationDate;
            cauTermsAndConditions.Active = Active;
        }

        #endregion Methods

        #region Operators

        public static explicit operator TermsAndConditions(CAUTermsAndConditions cauTerms)
        {
            if (cauTerms == null) return null;

            var terms = new TermsAndConditions
            {
                Id = cauTerms.Id,
                Type = cauTerms.Type,
                Name = cauTerms.Name,
                IsFile = cauTerms.IsFile,
                Detail = cauTerms.Detail,
                Url = cauTerms.Url,
                PublicationDate = cauTerms.PublicationDate,
                Active = cauTerms.Active
            };

            return terms;
        }

        #endregion Operators
    }
}