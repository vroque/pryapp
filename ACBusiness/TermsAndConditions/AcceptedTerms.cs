﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using ACBusiness.Settings;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.TermsAndConditions
{
    public class AcceptedTerms
    {
        #region Properties

        public int Id { get; set; }
        public decimal Pidm { get; set; }
        public int TermsId { get; set; }
        public bool Accepted { get; set; }
        public DateTime AcceptedDate { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public AcceptedTerms()
        {
        }

        public AcceptedTerms(AcceptedTerms acceptedTerms)
        {
            Id = acceptedTerms.Id;
            Pidm = acceptedTerms.Pidm;
            TermsId = acceptedTerms.TermsId;
            Accepted = acceptedTerms.Accepted;
            AcceptedDate = acceptedTerms.AcceptedDate;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Verifica que el estudiante haya aceptado un determinado terminos y condiciones
        /// </summary>
        /// <param name="pidm">pidm del estudiante</param>
        /// <returns></returns>
        public static async Task<AcceptedTerms> Get(decimal pidm)
        {
            using (var nexoContext = new NEXOContext())
            {
                var settingsDetailExt = await new SettingsDetail().GetTermsForExtracurricular();
                if (settingsDetailExt == null) return new AcceptedTerms();

                return (AcceptedTerms)(await nexoContext.cau_accepted_terms.FirstOrDefaultAsync(x => x.Pidm == pidm
                    && x.Accepted && x.TermsId == settingsDetailExt.NumberValue));
            }
        }

        /// <summary>
        /// Create new accepted terms
        /// </summary>
        /// <returns></returns>
        public async Task<AcceptedTerms> CreateAsync()
        {
            var terms = new CAUAcceptedTerms
            {
                Pidm = Pidm,
                TermsId = TermsId,
                Accepted = Accepted,
                AcceptedDate = DateTime.Now
            };
            
            _nexoContext.cau_accepted_terms.Add(terms);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (AcceptedTerms) terms : null;
        }

        #endregion Methods

        #region Operators

        public static explicit operator AcceptedTerms(CAUAcceptedTerms cauAcceptedTerms)
        {
            if (cauAcceptedTerms == null) return null;

            var acceptedTerms = new AcceptedTerms
            {
                Id = cauAcceptedTerms.Id,
                Pidm = cauAcceptedTerms.Pidm,
                TermsId = cauAcceptedTerms.TermsId,
                Accepted = cauAcceptedTerms.Accepted,
                AcceptedDate = cauAcceptedTerms.AcceptedDate
            };

            return acceptedTerms;
        }

        #endregion
    }
}