using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.TermsAndConditions
{
    public class TermsAndConditionsType
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public TermsAndConditionsType()
        {
        }

        public TermsAndConditionsType(int id)
        {
            var type = _nexoContext.cau_terms_and_conditions_type.FirstOrDefault(w => w.Id == id);
            if (type == null) return;

            Id = type.Id;
            Name = type.Name;
            Description = type.Description;
        }

        #endregion Constructors

        #region Methods

        public static async Task<List<TermsAndConditionsType>> GetAll()
        {
            var nexoContext = new NEXOContext();
            List<CAUTermsAndConditionsType> typeList = await nexoContext.cau_terms_and_conditions_type.ToListAsync();
            List<TermsAndConditionsType> classTypeList = typeList.Select(s => (TermsAndConditionsType) s).ToList();

            return classTypeList;
        }

        #endregion Methods

        #region Operators

        public static explicit operator TermsAndConditionsType(CAUTermsAndConditionsType type)
        {
            if (type == null) return null;

            var classType = new TermsAndConditionsType
            {
                Id = type.Id,
                Name = type.Name,
                Description = type.Description
            };

            return classType;
        }

        #endregion Operators
    }
}