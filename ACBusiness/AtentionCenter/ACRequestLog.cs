﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.AtentionCenter
{
    /// <summary>
    /// Historial de Pasos por los cuales transita 
    /// una solicitud desde su creación hasta el
    /// final de su ciclo
    /// </summary>
    public class ACRequestLog
    {
        #region properties
        public int id { get; set; }
        public int request_id { get; set; }
        public int old_state { get; set; }
        public int new_state { get; set; }
        public string observation { get; set; }
        public string user_id { get; set; }
        public int? office_user_id { get; set; }
        public int? office_id { get; set; }
        public string office_name { get; set; }
        public string approved { get; set; }
        public DateTime update_at{ get; set; }

        private NEXOContext _db = new NEXOContext();
        #endregion properties

        #region methods

        /// <summary>
        /// Lista los logs de una determinada solicitud
        /// </summary>
        /// <param name="request_id"></param>
        /// <returns></returns>
        public async Task<List<ACRequestLog>> listByRequest(int request_id)
        {
            List<ACRequestLog> logs;
            List<CAUTblRequestLog> list = await _db.cau_request_logs
                .Where(f => f.requestID == request_id)
                .OrderBy( f => f.updateDate)
                .ToListAsync();

                //.OrderByAsync(f => f.updateDate)
                
            logs = list.Select<CAUTblRequestLog, ACRequestLog>(f => f).ToList();

            return logs;
        }
        /// <summary>
        /// Rellena los datos de la solicitud en el log
        /// </summary>
        /// <param name="request"></param>
        public void syncByRequest(ACRequest request){
            this.request_id = request.id;
            this.old_state = request.state;
            this.new_state = request.state;
            this.office_user_id = request.office_user_id;
            this.office_id = request.office_id;
            this.update_at = DateTime.Now;
        }

        /// <summary>
        /// Crea el objeto y lo guarda en 
        /// la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<int> create()
        {
            CAUTblRequestLog log = new CAUTblRequestLog();
            this.compose(ref log);
            _db.cau_request_logs.Add(log);
            int i = await _db.SaveChangesAsync();
            return i;
        }

        /// <summary>
        /// Rellena los datos del log 
        /// en un objeto de base de datos
        /// </summary>
        /// <param name="log"></param>
        public void compose(ref CAUTblRequestLog log)
        {
            log.id = this.id;
            log.requestID = this.request_id;
            log.oldState = this.old_state;
            log.newState = this.new_state;
            log.observation = this.observation;
            log.userID = this.user_id;
            log.officeUserID = this.office_user_id;
            log.officeID = this.office_id;
            log.officeName = this.office_name;
            log.approved = this.approved;
            log.updateDate = this.update_at;
            
        }
        
        #endregion methods

        #region implicite operators
        public static implicit operator ACRequestLog(CAUTblRequestLog obj)
        {
            if (obj == null) return null;
            ACRequestLog log = new ACRequestLog();
            log.id = obj.id;
            log.request_id = obj.requestID;
            log.old_state = obj.oldState;
            log.new_state = obj.newState;
            log.observation = obj.observation;
            log.user_id = obj.userID;
            log.office_user_id = obj.officeUserID.GetValueOrDefault();
            log.office_id = obj.officeID.GetValueOrDefault();
            log.office_name = obj.officeName;
            log.approved = obj.approved;
            log.update_at = obj.updateDate;

            return log;
        }
        #endregion implicite operators
    }
}
