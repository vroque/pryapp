﻿using ACBusiness.Academic;
using ACBusiness.Accounts;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO.views;
using ACPermanence.DataBases.NEXO.CAU;
using ACTools.Configuration;
using ACTools.Mail;
using ACTools.PDF;
using ACTools.SuperString;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Personal;
using ACPermanence.DataBases.DBUCCI.ATE;
using Newtonsoft.Json;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _inst = ACTools.Constants.InstitucionalConstants;
using _acad = ACTools.Constants.AcademicConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _util = ACTools.Util;

namespace ACBusiness.AtentionCenter
{
    /// <summary>
    /// Solicitudes del Centro de Atención y Soluciones
    /// </summary>
    public class ACRequest
    {
        #region properties

        public int id { get; set; }
        public string divs { get; set; }
        public string campus { get; set; }
        public string program { get; set; }
        public decimal person_id { get; set; }
        public string student_id { get; set; }
        public int document_number { get; set; }
        public int document_id { get; set; }
        public int relationship_id { get; set; }
        public string student_name { get; set; }
        public int state { get; set; }
        public string description { get; set; }
        public double cost { get; set; }
        public string concept_id { get; set; }
        public int office_id { get; set; }
        public int office_user_id { get; set; }
        public string term { get; set; }
        public bool aproved { get; set; }

        public DateTime creation_date { get; set; }
        public DateTime delivery_date { get; set; }
        public DateTime update_date { get; set; }
        public string department { get; set; }
        public List<string> document_uri { get; set; }
        [JsonIgnore] public List<string> document_paths { get; set; }

        public string online_payment_id { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion properties

        #region methods

        /// <summary>
        /// Crea una nueva solicitud
        /// </summary>
        /// <returns></returns>
        public async Task<int> create(string user)
        {
            int current_state = this.state;
            int year = DateTime.Today.Year;

            // Último código de solicitud
            int last_request = _nexoContext.cau_requests.Where(f => f.requestID.ToString().StartsWith(year.ToString()))
                                   .Max(f => (int?) f.requestID) ?? 0;

            // Generación de nuevo código de solicitud
            if (last_request > 0)
                last_request += 1;
            else
            {
                var sufix = "1".completeZeros(6);
                last_request = int.Parse($"{year}{sufix}");
            }

            this.id = last_request;

            // guardar datos
            var req = new CAUTblRequest();

            // Siempre creamos la solicitud con el estado 'inicial'
            this.state = _ate.STATE_INITIAL;
            this.compose(ref req);
            _nexoContext.cau_requests.Add(req);
            var res = await _nexoContext.SaveChangesAsync();
            if (res == 1)
            {
                var ch_state = 0;
                switch (current_state)
                {
                    case _ate.STATE_ATENTION:
                        ch_state = await this.goAtention("SOLICITUD CREADA SIN DEUDA- PARA CENTRO DE ATENCION", user);
                        break;
                    case _ate.STATE_PAYMENT_PENDING:
                        //insertar deuda 
                        List<sp_InsertDebt> result = await Debt.insert_request_debt(this);
                        if (result.Count > 0)
                        {
                            this.cost = result.Sum(r => r.Cargo);
                            this.online_payment_id = result.FirstOrDefault().IDPagoOnline;
                            ch_state = await this.goPaymentPending("SOLICITUD PENDIENTE DE PAGO", user);
                        }
                        else
                            throw new Exception("no se pudo generar la deuda.");

                        // ch_state = await this.goPaymentPending("SOLICITUD PENDIENTE DE PAGO");
                        break;
                    case _ate.STATE_CANCEL:
                        ch_state = await this.goCancel("SOLICITUD CANCELADA EN LA CREACION", user);
                        break;
                    default:
                        ch_state = await this.goState(current_state, "INCIO EN ESTADO PERSONALIZADO SEGUN EL FLUJO",
                            user);
                        break;
                }

                if (ch_state == 0) throw new Exception("no se pudo cambiar de estado");

                //Enviar Correo
                var st = new Student(this.person_id);
                var obj = JObject.Parse(req.requestDescription);
                var descripcion = (string) obj["description"];

                var mail = new MailSender();
                mail.compose("request_set",
                        new
                        {
                            id = req.requestID,
                            student_id = st.id,
                            first_name = st.first_name,
                            full_name = st.full_name,
                            document_name = ACDocument.get(req.documentID).name,
                            description = descripcion,
                            amount = req.amount,
                            date = $"{req.requestDate:dd/MM/yyyy hh:mm tt}",
                            state_name = _ate.STATES_NAMES[req.requestStateID]
                        })
                    .destination($"{st.id}@continental.edu.pe", $"[CAU] Acuse de Solicitud #{req.requestID}").send();
            }

            return res;
        }


        /// <summary>
        /// <para>Creación de solicitudes para casos especiales.</para>
        /// <para> p.e.: Para generar constancia de ingreso como parte del trámite de bachiller se requiere generar una 
        /// solicitud que sirva de evidencia de que se generó una constancia de ingreso como parte de este trámite.
        /// Dicha solicitud se genera con estado "finalizado".</para>
        /// </summary>
        /// <param name="user">Nombre de usuario que crea la solicitud</param>
        /// <param name="debt">¿Generar deuda?</param>
        /// <param name="initialState">Estado inicial de la solicitud</param>
        /// <param name="conceptId">Concepto de pago</param>
        /// <returns></returns>
        /// TODO: Unir este método con el método 'Create'
        public async Task<int> createCustomRequest(string user, bool debt, int initialState, string conceptId = "")
        {
            var year = DateTime.Today.Year;

            // Último código de solicitud
            var lastRequest = _nexoContext.cau_requests.Where(f => f.requestID.ToString().StartsWith(year.ToString()))
                                  .Max(f => (int?) f.requestID) ?? 0;

            // Generación de nuevo código de solicitud
            if (lastRequest > 0) lastRequest += 1;
            else
            {
                var sufix = "1".completeZeros(6);
                lastRequest = int.Parse($"{year}{sufix}");
            }

            id = lastRequest;

            // guardar datos
            var cauTblRequest = new CAUTblRequest();

            // Siempre creamos la solicitud con el estado 'inicial'
            // A menos que se sea una solicitud para casos especiales y se desee crear la solicitud sin deuda y en un
            // determinado estado
            state = initialState != 1 && !debt ? initialState : _ate.STATE_INITIAL;

            // Para casos de generar solicitud sin deuda, tambien se puede indicar el concepto de 'pago' relacionado
            // al documento que se está solicitando
            if (conceptId != string.Empty)
            {
                concept_id = conceptId;
            }

            compose(ref cauTblRequest);
            _nexoContext.cau_requests.Add(cauTblRequest);
            var res = await _nexoContext.SaveChangesAsync();

            return res;
        }

        /// <summary>
        /// Creación de nueva solicitud.
        /// Incluye el proceso de creación de solicitud con estado 'inicial', cambio de estado y envío de acuse.
        /// </summary>
        /// <param name="user">Usuario que realiza la creación de solicitud</param>
        /// <param name="acuse">Enviar acuse de solicitud</param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        /// <exception cref="SystemException"></exception>
        public async Task<int> CreateAsync(string user, bool acuse = true)
        {
            var currentState = state;
            var newRequest = await SaveAsync();

            if (newRequest != 1) return newRequest;

            var i = await ChangeRequestStatusAsync(currentState, user);

            // Envío de e-mail luego de cambiar el estado de la solicitud
            if (i != 0 && acuse) SendAcknowledgmentOfRequest();

            return newRequest;
        }

        /// <summary>
        /// Creación de nueva solicitud
        /// </summary>
        /// <returns>Estado de creación de nueva solicitud</returns>
        private async Task<int> SaveAsync()
        {
            id = NewRequestNumber();

            var newRequest = new CAUTblRequest();

            // Toda solicitud debe ser creada con el estado 'inicial'
            state = _ate.STATE_INITIAL;

            compose(ref newRequest);
            _nexoContext.cau_requests.Add(newRequest);
            var saveStatus = await _nexoContext.SaveChangesAsync();

            return saveStatus;
        }

        /// <summary>
        /// Cambio de estado de solicitud
        /// </summary>
        /// <param name="requestStatus"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <exception cref="SystemException"></exception>
        private async Task<int> ChangeRequestStatusAsync(int requestStatus, string user)
        {
            var changedStatus = 0;
            switch (requestStatus)
            {
                case _ate.STATE_ATENTION:
                    changedStatus = await goAtention("SOLICITUD CREADA SIN DEUDA- PARA CENTRO DE ATENCION", user);
                    break;
                case _ate.STATE_PAYMENT_PENDING:
                    List<sp_InsertDebt> debts = await Debt.insert_request_debt(this);

                    if (debts == null) throw new NullReferenceException("No se pudo generar la deuda");

                    cost = debts.Sum(s => s.Cargo);
                    online_payment_id = debts.FirstOrDefault()?.IDPagoOnline;
                    changedStatus = await goPaymentPending("SOLICITUD PENDIENTE DE PAGO", user);
                    break;
                case _ate.STATE_CANCEL:
                    changedStatus = await goCancel("SOLICITUD CANCELADA EN LA CREACION", user);
                    break;
                default:
                    changedStatus = await goState(requestStatus, "INCIO EN ESTADO PERSONALIZADO SEGUN EL FLUJO", user);
                    break;
            }

            if (changedStatus == 0) throw new SystemException("No se pudo cambiar de estado");

            return changedStatus;
        }

        /// <summary>
        /// Envío de acuse de solicitud
        /// </summary>
        /// <exception cref="NullReferenceException"></exception>
        private void SendAcknowledgmentOfRequest()
        {
            var student = new Student(person_id);

            var requestCreated = _nexoContext.cau_requests.FirstOrDefault(w => w.requestID == id);

            if (requestCreated == null) throw new NullReferenceException($"La solicitud {id} no existe.");

            var obj = JObject.Parse(requestCreated.requestDescription);
            var requestDescription = (string) obj["description"];

            var mailSender = new MailSender();
            mailSender.compose("request_set", new
                {
                    id = requestCreated.requestID,
                    student_id = student.id,
                    first_name = student.first_name,
                    full_name = student.full_name,
                    document_name = ACDocument.get(requestCreated.documentID).name,
                    description = requestDescription,
                    amount = requestCreated.amount,
                    date = $"{requestCreated.requestDate:dd/MM/yyyy hh:mm tt}",
                    state_name = _ate.STATES_NAMES[requestCreated.requestStateID]
                }).destination($"{student.id}@continental.edu.pe",
                    $"[CAU] Acuse de Solicitud #{requestCreated.requestID}")
                .send();
        }

        /// <summary>
        /// Generación de nuevo código de solicitud
        /// </summary>
        /// <returns></returns>
        private int NewRequestNumber()
        {
            var year = DateTime.Today.Year;
            // Última solicitud generada
            var lastRequest = _nexoContext.cau_requests.Where(w => w.requestID.ToString().StartsWith(year.ToString()))
                                  .Max(m => (int?) m.requestID) ?? 0;

            if (lastRequest > 0) return lastRequest + 1;

            var suffix = "1".PadLeft(6, '0');
            return int.Parse($"{year}{suffix}");
        }


        /// <summary>
        /// Guarda cambios en la solicitud
        /// </summary>
        public async Task<int> update()
        {
            var req = await _nexoContext.cau_requests.FirstOrDefaultAsync(f => f.requestID == this.id);
            this.compose(ref req);

            return await _nexoContext.SaveChangesAsync();
        }

        /// <summary>
        /// Derivacion automatica segun la regla de flujo
        /// asignando la oficina a la cual se deriva
        /// </summary>
        /// <param name="message">Mensaje para quien se derive</param>
        /// <param name="office_id">id de oficina</param>
        /// <param name="user">usuario que deriva</param>
        /// <returns></returns>
        public async Task<int> derive(string message, string user)
        {
            //get state by document
            var ds = new ACDocumentState();
            List<ACDocumentState> flow = ds.flow(this.document_id);
            ACDocumentState current_state = flow.FirstOrDefault(f => f.state_id == this.state && f.order != -1);
            ACDocumentState next_state = flow.FirstOrDefault(f => f.state_id == current_state.next && f.order != -1);
            this.office_id = next_state.office_id;
            var i = await this.update();
            if (i == 0) return 0;

            return await this.goState(next_state.state_id, message, user);
        }

        public async Task<int> goAprove(string message, string user)
        {
            //update aprove
            this.aproved = true;
            //get state by document
            var ds = new ACDocumentState();
            List<ACDocumentState> flow = ds.flow(this.document_id);
            ACDocumentState current_state = flow.FirstOrDefault(f => f.state_id == this.state && f.order != -1);
            ACDocumentState next_state = flow.FirstOrDefault(f => f.state_id == current_state.next && f.order != -1);
            this.office_id = next_state.office_id;

            var i = await this.update();
            if (i == 0) return 0;
            var debt = 0;

            if (next_state.state_id == _ate.STATE_PAYMENT_PENDING) return await this.setDebt(message, user);

            return await this.goState(next_state.state_id, message, user);
        }

        public async Task<int> goCancel(string message, string user)
        {
            // enviar correo indicando cancelacion
            var mail = new MailSender();
            var st = new Student(this.person_id);
            mail.compose("request_refuse",
                    new
                    {
                        id = this.id,
                        student_id = st.id,
                        first_name = st.first_name,
                        full_name = st.full_name,
                        document_name = ACDocument.get(this.document_id).name,
                        description = this.description,
                        refuse_message = message,
                        date = $"{this.creation_date:dd/MM/yyyy hh:mm tt}",
                        state_name = _ate.STATES_NAMES[_ate.STATE_CANCEL]
                    })
                .destination($"{st.id}@continental.edu.pe", $"[CAU] Acuse de Solicitud #{this.id}").send();
            return await this.goState(_ate.STATE_CANCEL, message, user);
        }

        private async Task<int> goAtention(string message, string user)
        {
            return await this.goState(_ate.STATE_ATENTION, message, user);
            // generad deuda
        }

        private async Task<int> goPaymentPending(string message, string user)
        {
            return await this.goState(_ate.STATE_PAYMENT_PENDING, message, user);
        }

        public async Task<int> goDeliveryPending(string message, string user)
        {
            return await this.goState(_ate.STATE_DELIVERY_PENDING, message, user);
        }

        public async Task<int> goAprovalPending(int area, string message, string user)
        {
            this.office_id = area;
            var i = await this.update();
            if (i == 0) return 0;

            return await this.goState(_ate.STATE_APROVAL_PENDING, message, user);
        }

        public async Task<int> goProcessPending(string message, string user)
        {
            this.aproved = true;
            this.office_id = _inst.OFFICE_CAU;
            var i = await this.update();
            if (i == 0) return 0;

            return await this.goState(_ate.STATE_PROCESS_PENDING, message, user);
        }

        public async Task<int> goFinal(string message, string user)
        {
            return await this.goState(_ate.STATE_FINAL, message, user);
        }

        /// <summary>
        /// Cambia el estado de una solicitud por el estado
        /// recibido como parametro
        /// **Hace una verificacion para comprobar que el 
        /// nuevo estado sea uno valido segun su flujo.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="message"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<int> goState(int state, string message, string user)
        {
            var currentState = this.state;
            //get state by document
            var documentState = new ACDocumentState();
            var flow = documentState.flow(this.document_id);
            //validate if is a valid state
            var st = flow.FirstOrDefault(f => f.state_id == this.state && f.next == state);
            var nextState = flow.FirstOrDefault(f => f.state_id == state && f.order != -1);

            if (st == null) throw new Exception("proceso no permitido para esta solicitud");

            this.state = state;
            this.office_id = nextState?.office_id ?? this.office_id;
            //listar trigger
            //triggers antes del cambio
            //guardar 
            var res = await this.update();
            //triggers despues del cambio
            if (res == 1) res = await this.trace(currentState, state, message, user);

            return res;
        }

        /// <summary>
        /// genera deudas
        /// </summary>
        /// <param name="message"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<int> setDebt(string message, string user)
        {
            //validar que tenga un siguiente estado en debt
            var ds = new ACDocumentState();
            var flow = ds.flow(this.document_id);
            //validate if is a valid state
            var n = flow.Count(f => f.state_id == this.state && f.next == _ate.STATE_PAYMENT_PENDING);
            if (n < 1) throw new Exception("Estado no permitido para las solicitud");
            List<sp_InsertDebt> result = await Debt.insert_request_debt(this);
            if (result.Count > 0)
            {
                this.cost = result.Sum(f => f.Cargo);
                int r = await this.goState(_ate.STATE_PAYMENT_PENDING, message, user);
                return r;
            }

            throw new Exception("no se pudo crear la deuda");
        }

        /// <summary>
        /// Crear el log de trazabilidad de la solicitud
        /// </summary>
        /// <param name="old_state">Estado desde el que se cambio</param>
        /// <param name="new_state">Estado actual</param>
        /// <param name="message">Mensaje</param>
        /// <param name="user">Usuario actual</param>
        /// <returns></returns>
        private async Task<int> trace(int old_state, int new_state, string message, string user)
        {
            var log = new ACRequestLog();
            log.syncByRequest(this);
            log.old_state = old_state;
            log.new_state = new_state;
            log.observation = message;
            log.office_id = this.office_id;
            log.office_name = _inst.OFFICES_NAMES[this.office_id];
            log.user_id = user;

            return await log.create();
        }

        /// <summary>
        /// Guarda la solicitud en la base de datos
        /// (Actualiza o crea segun el objeto)
        /// </summary>
        /// <param name="req">
        /// Obj de Permanencia sobre el cual se guardaran los datos
        /// </param>
        /// <returns></returns>
        private bool compose(ref CAUTblRequest req)
        {
            if (req == null) return false;

            req.requestID = this.id;
            req.divs = this.divs;
            req.campus = this.campus;
            req.program = this.program;
            req.pidm = this.person_id;
            req.requestDate = this.creation_date;
            req.documentNumber = this.document_number;
            req.documentID = this.document_id;
            req.relationshipID = this.relationship_id;
            req.applicantName = this.student_name;
            req.deliveryDate = this.delivery_date;
            req.requestStateID = this.state;
            req.requestDescription = this.description;
            req.officeUserID = this.office_user_id;
            req.amount = this.cost;
            req.conceptID = this.concept_id;
            req.officeID = this.office_id;
            req.term = this.term;
            req.aproved = this.aproved;
            req.updateDate = DateTime.Now;
            req.onlinePaymentID = this.online_payment_id;

            return true;
        }

        private void verifyPaths()
        {
            if (this.document_uri == null) this.document_uri = new List<string>();

            if (this.document_paths == null) this.document_paths = new List<string>();
            //buscar todos los path
            //por ahora solo report card tiene varios documentos por solicitud 
            if (this.document_id == _ate.DOC_REPORT_CARD)
            {
                //buscar los periodos y listarlo
                var json_obj = JObject.Parse(this.description);
                var n = json_obj["term_list"].Count();
                for (var i = 1; i <= n; i++)
                {
                    var pdf = string.Format("{0}-{2}-{1}", this.id, this.student_id, i);

                    if (!PDFGenerator.exists(pdf)) continue;

                    this.document_uri.Add($"{AppSettings.reports["output-client"]}{pdf}/pdf/");
                    this.document_paths.Add($"{AppSettings.reports["output"]}{pdf}.pdf");
                }
            }
            else if (_cic.LanguagesCenterDocumentList.Contains(document_id))
            {
                var pdf = $"{id}-{document_number}-{student_id}";

                // TODO: Implementar obtención de uri y path para documentos CAIE

                if (!_util.LanguagesCenter.ExistDocument(pdf)) return;

                document_uri.Add($"{AppSettings.reports[_cic.URI_CIC_PDF]}{pdf}");
                document_paths.Add($"{AppSettings.reports[_cic.SRC_CIC_PDF]}{pdf}");
            }
            else
            {
                var pdf = string.Format("{0}-{2}-{1}",
                    this.id, this.student_id, this.document_number
                );

                if (!PDFGenerator.exists(pdf)) return;

                this.document_uri.Add($"{AppSettings.reports["output-client"]}{pdf}/pdf/");
                this.document_paths.Add($"{AppSettings.reports["output"]}{pdf}.pdf");
            }
        }

        #endregion methods

        #region static methods

        public static bool isNull(ACRequest req)
        {
            return req == null;
        }

        /// <summary>
        /// Obtiene una solicitude por su id
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public static async Task<ACRequest> get(int requestId)
        {
            var nexoContext = new NEXOContext();
            ACRequest req = await nexoContext.cau_requests.FirstOrDefaultAsync(r => r.requestID == requestId);
            switch (req.divs)
            {
                case _inst.UC:
                    var student = new Student(req.person_id);
                    student.syncProfile(req.divs, _acad.LEVL_PREGRADO, req.program); //hasta el momento tos son pregrado
                    req.student_id = student.id;
                    req.department = student.profile.department;
                    break;
                case _inst.CIC:
                    req.student_id = await Person.GetDocumentNumberAsync(req.person_id);
                    req.department = req.divs;
                    break;
                default:
                    throw new SystemException("Dependencia desconocida");
            }

            if (req.term == null)
                req.term = new ACTerm().getByDate(req.creation_date, req.campus, req.department).term;
            //student.syncProfile(req.divs,req.levl,req.program,req.person_id);
            // buscar si ya existe el documento pdf
            req.verifyPaths();
            //base.File(PDFGenerator.getRepoDocumentPath(pdf),"application/pdf");

            return req;
        }

        /// <summary>
        /// Obtenien una lista de las solicitus que van
        /// al centro de atención de una sede
        /// </summary>
        /// <returns></returns>
        public static async Task<List<ACRequest>> listInAtentionCampus(string campus)
        {
            var nexoContext = new NEXOContext();
            var dboContext = new DBOContext();
            List<CAUTblRequest> list_proto = await nexoContext.cau_requests
                .Where(f => (f.requestStateID == _ate.STATE_ATENTION ||
                             f.requestStateID == _ate.STATE_DELIVERY_PENDING ||
                             f.requestStateID == _ate.STATE_PROCESS_PENDING ||
                             f.requestStateID == _ate.STATE_DOCUMENT_RECEPTION) &&
                            f.campus == campus)
                .OrderByDescending(f => f.requestDate).ToListAsync();
            List<ACRequest> list = list_proto.Select<CAUTblRequest, ACRequest>(f => f).ToList();
            // lista de PIDM's
            List<int> pidm_list = list.Select(f => (int) f.person_id).Distinct().ToList();
            List<DBODATAlumnoBasico> student_list =
                dboContext.view_alumno_basico.Where(f => pidm_list.Contains(f.IDPersonaN)).ToList();
            foreach (var item in list)
            {
                var pidm = (int) item.person_id;
                item.student_id = student_list.FirstOrDefault(f => f.IDPersonaN == pidm)?.DNI;
            }

            return list;
        }

        /// <summary>
        /// Lista de todas las solicitudes de un determinado campus UC
        /// </summary>
        /// <param name="campus">Campus UC</param>
        /// <returns></returns>
        public static async Task<List<ACRequest>> RequestListByCampusAsync(string campus)
        {
            var nexoContext = new NEXOContext();
            var dboContext = new DBOContext();
            List<CAUTblRequest> listProto = await nexoContext.cau_requests.Where(w => w.campus == campus)
                .OrderByDescending(f => f.requestDate).ToListAsync();

            List<ACRequest> requestList = listProto.Select<CAUTblRequest, ACRequest>(f => f).ToList();
            // lista de PIDM's
            List<int> pidmList = requestList.Select(f => (int) f.person_id).Distinct().ToList();
            List<DBODATAlumnoBasico> studentList =
                dboContext.view_alumno_basico.Where(f => pidmList.Contains(f.IDPersonaN)).ToList();
            foreach (var request in requestList)
            {
                var pidm = (int) request.person_id;
                request.student_id = studentList.FirstOrDefault(f => f.IDPersonaN == pidm)?.DNI;
            }

            return requestList;
        }

        /// <summary>
        /// Lista de solicitudes generadas para el Centro de Idiomas
        /// </summary>
        /// <returns></returns>
        public static async Task<List<ACRequest>> LanguagesCenterRequestListAsync()
        {
            var nexoContext = new NEXOContext();
            var requests = await nexoContext.cau_requests.Where(w => w.divs == _inst.CIC)
                .OrderByDescending(o => o.requestDate).ToListAsync();
            var requestList = requests.Select<CAUTblRequest, ACRequest>(r => r).ToList();

            return requestList;
        }

        /// <summary>
        /// Listar solicitudes segun el area en que se encuentran
        /// </summary>
        /// <param name="campus"></param>
        /// <param name="area"></param>
        /// <returns></returns>
        public static async Task<List<ACRequest>> listByArea(string campus, int area)
        {
            var nexoContext = new NEXOContext();
            var dboContext = new DBOContext();

            List<CAUTblRequest> list_proto = await nexoContext.cau_requests
                .Where(f => f.campus == campus &&
                            f.officeID == area &&
                            f.requestStateID != _ate.STATE_CANCEL &&
                            f.requestStateID != _ate.STATE_INITIAL &&
                            f.requestStateID != _ate.STATE_PAYMENT_PENDING &&
                            f.requestStateID != _ate.STATE_FINAL)
                .ToListAsync();

            List<ACRequest> list = list_proto.Select<CAUTblRequest, ACRequest>(f => f).ToList();
            // lista de PIDM's
            List<int> pidm_list = list.Select(f => (int) f.person_id).Distinct().ToList();
            List<DBODATAlumnoBasico> student_list =
                dboContext.view_alumno_basico.Where(f => pidm_list.Contains(f.IDPersonaN)).ToList();
            foreach (var item in list)
            {
                int pidm = (int) item.person_id;
                item.student_id = student_list.FirstOrDefault(f => f.IDPersonaN == pidm).DNI;
            }

            return list;
        }

        /// <summary>
        /// Listar solicitudes segun el estado en que se encuentran
        /// </summary>
        /// <param name="campus">Sede o filial *__all__ para todas las sedes</param>
        /// <param name="state"></param>
        /// <returns></returns>
        public static async Task<List<ACRequest>> listByState(string campus, int state)
        {
            List<CAUTblRequest> list_proto;
            var nexoContext = new NEXOContext();
            if (campus == "__all__")
            {
                list_proto = await nexoContext.cau_requests
                    .Where(f => f.requestStateID == state &&
                                f.requestStateID != _ate.STATE_CANCEL &&
                                f.requestStateID != _ate.STATE_INITIAL &&
                                f.requestStateID != _ate.STATE_PAYMENT_PENDING &&
                                f.requestStateID != _ate.STATE_FINAL)
                    .ToListAsync();
            }
            else
            {
                list_proto = await nexoContext.cau_requests
                    .Where(f => f.campus == campus &&
                                f.requestStateID == state &&
                                f.requestStateID != _ate.STATE_CANCEL &&
                                f.requestStateID != _ate.STATE_INITIAL &&
                                f.requestStateID != _ate.STATE_PAYMENT_PENDING &&
                                f.requestStateID != _ate.STATE_FINAL)
                    .ToListAsync();
            }

            List<ACRequest> list = list_proto.Select<CAUTblRequest, ACRequest>(f => f).ToList();
            return list;
        }

        /// <summary>
        /// Lista solicitudes por codigo de persona
        /// </summary>
        /// <param name="person_id"></param>
        /// <returns></returns>
        public static async Task<List<ACRequest>> listByPerson(decimal person_id)
        {
            List<ACRequest> list;
            List<CAUTblRequest> list_proto;
            var nexoContext = new NEXOContext();
            list_proto = await nexoContext.cau_requests
                .Where(f => f.pidm == person_id &&
                            f.requestStateID != _ate.STATE_INITIAL &&
                            f.requestStateID != _ate.STATE_CANCEL)
                .ToListAsync();

            list = list_proto.Select<CAUTblRequest, ACRequest>(f => f).OrderByDescending(f => f.creation_date).ToList();
            return list;
        }

        public static async Task<List<ACRequest>> listResolutionsByPerson(decimal person_id)
        {
            List<ACRequest> list;
            List<CAUTblRequest> list_proto;
            var nexoContext = new NEXOContext();
            var dboContext = new DBOContext();
            list_proto = await nexoContext.cau_requests
                .Where(f => f.pidm == person_id &&
                            f.requestStateID != _ate.STATE_INITIAL &&
                            f.requestStateID != _ate.STATE_CANCEL &&
                            (f.documentID == _ate.DOC_CONVALIDATION ||
                             f.documentID == _ate.DOC_CONVALIDATION_EXTENSION))
                .ToListAsync();

            list = list_proto.Select<CAUTblRequest, ACRequest>(f => f).ToList();
            List<int> pidm_list = list.Select(f => (int) f.person_id).Distinct().ToList();
            List<DBODATAlumnoBasico> student_list = dboContext.view_alumno_basico
                .Where(f => pidm_list.Contains(f.IDPersonaN)).ToList();
            foreach (var item in list)
            {
                var p_id = (int) item.person_id;
                item.student_id = student_list.FirstOrDefault(f => f.IDPersonaN == p_id).DNI;
                item.verifyPaths();
            }

            return list;
        }

        /// <summary>
        /// Buscasegun los parametros de entrada
        /// </summary>
        /// <param name="id"></param>
        /// <param name="student_id"></param>
        /// <param name="document_id"></param>
        /// <param name="state"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static async Task<List<ACRequest>> search(
            int? id, string student_id, int? document_id, int? state, DateTime? date, string campus)
        {
            List<ACRequest> list;
            List<CAUTblRequest> list_proto;
            var nexoContext = new NEXOContext();
            var dboContext = new DBOContext();
            decimal pidm = 0;
            //get pidm from student code
            if (student_id != null)
                pidm = await Student.getPidmFromStudentID(student_id);
            //search
            list_proto = await nexoContext.cau_requests
                .Where(f => (id == null || f.requestID == id) &&
                            (student_id == null || f.pidm == pidm) &&
                            (document_id == null || f.documentID == document_id) &&
                            (state == null || f.requestStateID == state) &&
                            f.requestStateID != _ate.STATE_INITIAL
                            // && (date == null || 
                            //    (f.requestID == id )
                            // )
                            && f.campus == campus)
                .OrderByDescending(f => f.requestDate).ToListAsync();
            list = list_proto.Select<CAUTblRequest, ACRequest>(f => f).ToList();
            List<int> pidm_list = list.Select(f => (int) f.person_id).Distinct().ToList();
            List<DBODATAlumnoBasico> student_list =
                dboContext.view_alumno_basico.Where(f => pidm_list.Contains(f.IDPersonaN)).ToList();
            foreach (var item in list)
            {
                var p_id = (int) item.person_id;
                item.student_id = student_list.FirstOrDefault(f => f.IDPersonaN == p_id).DNI;
            }

            return list;
        }

        public static bool changeState(int id, int state)
        {
            var nexoContext = new NEXOContext();
            var req = nexoContext.cau_requests.FirstOrDefault(f => f.requestID == id);
            req.requestStateID = state;
            var i = nexoContext.SaveChanges();

            return i > 0;
        }


        public async Task<ACRequest> cancelRequest(ACRequest request)
        {
            ACRequest validateRequest = await get(request.id);
            if (validateRequest.state != _ate.STATE_PAYMENT_PENDING)
            {
                throw new Exception("La solicitud no esta como pendiente de pago.");
            }

            if (validateRequest.document_id == _ate.DOC_CONVALIDATION ||
                validateRequest.document_id == _ate.DOC_CONVALIDATION_EXTENSION)
            {
                throw new Exception(
                    "No se pueden cancelar solicitudes de convalidacion de asignaturas o Extension de asignaturas.");
            }

            var dboContext = new DBOContext();
            List<object> cancel = dboContext.sp_CancelRequest(request.id.ToString()).ToList();
            validateRequest.state = _ate.STATE_CANCEL;
            return validateRequest;
        }

        #endregion static methods

        #region implicit operators

        /// <summary>
        /// Conversión implicita del tipo CAUTblSolicitud de la capa permanencia
        /// al tipo ACRequest de la capa de negocio
        /// </summary>
        public static implicit operator ACRequest(CAUTblRequest obj)
        {
            ACRequest req = new ACRequest();
            req.id = obj.requestID;
            req.divs = obj.divs;
            req.campus = obj.campus;
            req.program = obj.program;
            req.person_id = obj.pidm;
            req.creation_date = obj.requestDate;
            req.document_number = obj.documentNumber;
            req.document_id = obj.documentID;
            req.relationship_id = obj.relationshipID;
            req.student_name = obj.applicantName;
            req.delivery_date = obj.deliveryDate;
            req.state = obj.requestStateID;
            req.description = obj.requestDescription;
            req.office_user_id = obj.officeUserID;
            req.cost = obj.amount;
            req.concept_id = obj.conceptID;
            req.office_id = obj.officeID;
            req.term = obj.term;
            req.aproved = obj.aproved;
            req.update_date = obj.updateDate;
            req.online_payment_id = obj.onlinePaymentID;

            return req;
        }

        #endregion implicit operators
    }
}