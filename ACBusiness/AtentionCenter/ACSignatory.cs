﻿using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using ACTools.PDF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.AtentionCenter
{
    /// <summary>
    /// Firmante
    /// Persona que Firma(digitalmente) un documento
    /// </summary>
    public class ACSignatory
    {
        #region properties
        public int id { get; set; }
        public decimal person_id { get; set; }
        public string name { get; set; }
        public byte[] signature { get; set; }
        public byte[] post_signature { get; set; }
        public byte[] seal { get; set; }
        public int order { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        private NEXOContext _nexo = new NEXOContext();
        private DBOContext _apec = new DBOContext();
        #endregion properties

        #region methods

        /// <summary>
        /// Lista los firmantes por tipo de documento y opcionalmente por college
        /// </summary>
        /// <param name="document_id"></param>
        /// <param name="college_tag"></param>
        /// <returns></returns>
        public List<ACSignatory> listByACDocument(int document_id, string college_tag) 
        {
            List<ACSignatory> list;
            list = _nexo.cau_signatories_documents
                .Join(
                    _nexo.cau_signatories,
                    sig_doc => sig_doc.signatoryID,
                    sign => sign.signatoryID,
                    (sig_doc, sign) => new { sig_doc, sign }
                ).Where(
                    f => f.sig_doc.documentID == document_id
                    && f.sign.startDate <= DateTime.Today
                    && f.sign.endDate >= DateTime.Today
                    && (
                        f.sig_doc.college == null
                        || f.sig_doc.college == college_tag 
                    )
                )
                .Select(
                    f => new ACSignatory
                    { // (Signatory)f.sign
                        id = f.sign.signatoryID,
                        person_id = f.sign.pidm,
                        name = f.sign.name,
                        signature = f.sign.signature,
                        post_signature = f.sign.postSignature,
                        seal = f.sign.seal,
                        order = f.sig_doc.order                        
                    }
                ).OrderBy( f => f.order)
                .ToList();
            return list;
        }


        /// <summary>
        /// Obtiene los nombres de los firmantes de un documento
        /// </summary>
        /// <param name="document_id"></param>
        /// <param name="college_tag"></param>
        /// <returns></returns>
        public List<string> listSignatoriesNames(int document_id, string college_tag)
        {
            List<string> signatories = new List<string>();
            var signers = _nexo.cau_signatories_documents
                .Join(
                    _nexo.cau_signatories,
                    sig_doc => sig_doc.signatoryID,
                    sign => sign.signatoryID,
                    (sig_doc, sign) => new { sig_doc, sign }
                ).Where(
                    f => f.sig_doc.documentID == document_id
                    && f.sign.startDate <= DateTime.Today
                    && f.sign.endDate >= DateTime.Today
                    && (
                        f.sig_doc.college == null
                        || f.sig_doc.college == college_tag
                    )
                ).ToList();
            List<int> pcodes = signers.Select(f => (int)f.sign.pidm).ToList();
            var persons = _apec.tbl_persona.Where(f => pcodes.Contains(f.IDPersonaN));

            foreach (var item in signers)
            {
                var per = persons.FirstOrDefault(f => f.IDPersonaN == (int)item.sign.pidm);
                signatories.Add(
                    String.Format("{3}: {0} {1}, {2}",
                        per.Appat,
                        per.ApMat,
                        per.Nombres,
                        item.sign.name
                    ));
            }
            return signatories;
        }

        public List<ACSignatory> getSignatureAtencionCenter(int document_id, string college_tag)
        {
            List<ACSignatory> list;
            list = _nexo.cau_signatories_documents
                .Join(
                    _nexo.cau_signatories,
                    sig_doc => sig_doc.signatoryID,
                    sign => sign.signatoryID,
                    (sig_doc, sign) => new { sig_doc, sign }
                ).Where(
                    f => f.sig_doc.documentID == document_id
                    && f.sign.startDate <= DateTime.Today
                    && f.sign.endDate >= DateTime.Today
                    && (
                        f.sig_doc.college == null
                        || f.sig_doc.college == college_tag
                    )
                )
                .Select(
                    f => new ACSignatory
                    { // (Signatory)f.sign
                        id = f.sign.signatoryID,
                        person_id = f.sign.pidm,
                        name = f.sign.name,
                        signature = f.sign.signature,
                        post_signature = f.sign.postSignature,
                        seal = f.sign.seal,
                        order = f.sig_doc.order
                    }
                ).OrderBy(f => f.order)
                .ToList();
            return list;
        }

        /// <summary>
        /// Convierte la el objeto de ACSignatory a PDFSign en lista
        /// para evitar la ruptura de capas con ACTools
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public List<PDFSign> listPDFSign(List<ACSignatory> list){
            List<PDFSign> signs = new List<PDFSign>();
            foreach (ACSignatory sign in list)
            {
                PDFSign pdf_sign = new PDFSign();
                pdf_sign.sign = sign.signature;
                pdf_sign.post_sign = sign.post_signature;
                pdf_sign.seal = sign.seal;
                signs.Add(pdf_sign);
            }
            return signs;
        }
        #endregion methods

        #region implicite operators
        //public static implicit operator Signatory(CAUTblSignatory obj)
        //{
        //    if (obj == null) return null;
        //    Signatory signatory = new Signatory();

        //    return signatory;
        //}
        #endregion implicite operators
    }
}
