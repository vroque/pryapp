﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.AtentionCenter
{
    class ACDocumentSignatory
    {
        #region properties
        public int id { get; set; }
        public int document_id { get; set; }
        public string rol { get; set; }
        public string college { get; set; }
        public int order { get; set; }
        #endregion properties
    }
}
