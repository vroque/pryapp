﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Admision;
using ACBusiness.Institutional;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACTools.PDF.Documents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _academic = ACTools.Constants.AcademicConstants;
using _corp = ACTools.Constants.InstitucionalConstants;
using _admission = ACTools.Constants.AdmissionConstant;

namespace ACBusiness.AtentionCenter.Documents
{
    /// <inheritdoc />
    /// <summary>
    /// Certificado de Estudio Promocional
    /// </summary>
    public class PromotionalStudyCertificate : ACDocument
    {
        #region Properties

        private const int DOCUMENT_ID = _ate.DOC_PROMOTIONAL_STUDIES_CERTIFICATE;
        private const int REQUEST_FOR_ACADEMIC_RECORDS = _ate.STATE_APROVAL_PENDING;
        private const int REQUEST_FINALIZED = _ate.STATE_FINAL;
        private const int REQUEST_CANCELLED = _ate.STATE_CANCEL;
        private const string BACHILLER_CONCEPT_ID = _ate.BACHILLER_CONCEPT_ID;
        private const string NO_SCHOOL = "0";
        private const int STUDENT_GRADUATED = _academic.STATE_GRADUATE;

        #endregion Properties

        #region Methods

        /// <summary>
        /// Generación de Solicitud para Certificado de Estudios Promocional
        /// </summary>
        /// <param name="students">Egresados UC</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<List<Graduated>> GenerateRequestAsync(Graduated[] students, string user)
        {
            DBOContext dbo_context = new DBOContext();
            NEXOContext nexo_context = new NEXOContext();

            // Solo procesamos los códigos que existan en la tabla de egresados
            foreach (var student in students)
            {
                if (student.school_id == NO_SCHOOL || !student.is_selected ||
                    student.academic_status != STUDENT_GRADUATED || !student.Processing) continue;

                // Extraemos más info sobre el egresado
                var moreDataForGraduated = await dbo_context.tbl_alumno_egresado.FirstOrDefaultAsync(w =>
                    w.IDAlumno == student.student_id && w.IDEscuela == student.school_id);

                var academicProfile = new AcademicProfile
                {
                    person_id = Student.getPidmFromStudentId(student.student_id),
                    div = moreDataForGraduated.IDDependencia,
                    campus = Campus.getByAPECCode(moreDataForGraduated.IDSede),
                    department = Department.toBanner(student.modality_id),
                    subject = moreDataForGraduated.IDPlanEstudio,
                    college = Program.getCollege(student.school_id),
                    program = Program.get(student.school_id),
                    adm_type = AdmisionType.get(Entrant.GetCurrentModalityPostulationBySchoolIdAndDepartment(Student.getPidmFromStudentId(student.student_id), student.school_id, Department.toBanner(student.modality_id)), null, "banner")
                };


                var estudiante = new Student(student.student_id);

                ACRequest acRequest;

                // Se busca si existe solicitud asociada a la creación de certificado de estudios que no esté finalizada
                var requestExist =
                    await nexo_context.cau_requests.Where(w => w.documentID == DOCUMENT_ID &&
                                                               w.conceptID == BACHILLER_CONCEPT_ID &&
                                                               w.pidm == academicProfile.person_id &&
                                                               w.requestStateID != REQUEST_FINALIZED &&
                                                               w.requestStateID != REQUEST_CANCELLED)
                        .OrderByDescending(o => o.requestID).FirstOrDefaultAsync();
                // Si existe solicitud asociada, mostramos la información de la solicitud
                if (requestExist != null)
                {
                    acRequest = await ACRequest.get(requestExist.requestID);
                    // guardamos el ID de la solicitud en "doc_url" para mostrar en frontend
                    student.doc_url = acRequest.id.ToString();
                    student.Processing = false;
                }
                else
                {
                    acRequest = await CreateRequestForGraduationProcessAsync(estudiante, academicProfile, user);
                    if (ACRequest.isNull(acRequest))
                    {
                        throw new Exception("Error al generar la solicitud");
                    }

                    // guardamos el ID de la solicitud en "doc_url" para mostrar en frontend
                    student.doc_url = acRequest.id.ToString();
                    student.Processing = false;
                }
            }

            return students.OrderBy(o => o.student_name).ToList();
        }

        /// <summary>
        /// Generación de Certificado de Estudio Promocional
        /// </summary>
        /// <param name="request">Datos de la solicitud</param>
        /// <param name="student">Datos del estudiante</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public static async Task<string> GenerateAsync(ACRequest request, Student student, string user)
        {
            var acSignatory = new ACSignatory();
            var course = new Course();
            var signs = acSignatory.listByACDocument(DOCUMENT_ID, student.profile.college.id);

            var data = new PromotionalStudyCertificatePdf.Data
            {
                name_file = $"{request.id}-{request.document_number}-{student.id}",
                user = user,
                signs = acSignatory.listPDFSign(signs),
                student_id = student.id,
                student_names = student.full_name,
                student_gender = student.gender,
                college_name = student.profile.college.name,
                program_name = student.profile.program.name,
                request_id = request.id,
                document_number = request.document_number,
                photo_bytes = new Person().getPhoto(student.person_id),
                GenderData = Student.getGenderData(student.id)
            };

            var json = JObject.Parse(request.description);
            var periodList = json["period_list"];
            var periods = periodList.Select(period => period.ToString()).ToList();

            var courses = course.listByStudentPeriods(student.profile, periods).OrderBy(o => o.name).ToList();

            foreach (var curso in courses)
            {
                var courseDetail = new PromotionalStudyCertificatePdf.CourseDetail
                {
                    Cycle = curso.cycle,
                    Type = curso.type,
                    Id = curso.subject + curso.id,
                    Name = curso.name,
                    University = string.Empty,
                    Country = string.Empty,
                    Resolution = string.Empty,
                    Observation = (curso.nrc == "CONV") ? "Convalidado" : string.Empty,
                    Score = int.Parse(curso.score),
                    Credits = curso.credits,
                    Date = curso.date,
                    IsAproved = curso.is_aproved
                };
                data.Courses.Add(courseDetail);
            }

            var outputPath = PromotionalStudyCertificatePdf.Generate(data);

            if (outputPath == null) return null;

            // Actualizamos la solicitud
            var message = $"Solicitud generada el {DateTime.Now}, por el usuario {user}";
            await request.goDeliveryPending(message, user);
            return outputPath;
        }

        /// <summary>
        /// Creación de Solicitud como parte del proceso de graduación
        /// </summary>
        /// <param name="student">Estudiante UC</param>
        /// <param name="academicProfile">Perfil académico del estudiante UC</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        private async Task<ACRequest> CreateRequestForGraduationProcessAsync(Student student,
            AcademicProfile academicProfile, string user)
        {
            // TODO: Generar periodos automático según carrera
            string json;

            switch (academicProfile.program.id)
            {
                case _corp.PROGRAM_MEDICINA_HUMANA:
                    json = JsonConvert.SerializeObject(new
                    {
                        description =
                        "Solicitud 'Certificado de Estudios Promocional' generada como parte del proceso de trámite de bachiller",
                        period_list =
                        new[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14"}
                    });
                    break;
                case _corp.PROGRAM_DERECHO:
                    json = JsonConvert.SerializeObject(new
                    {
                        description =
                        "Solicitud 'Certificado de Estudios Promocional' generada como parte del proceso de trámite de bachiller",
                        period_list =
                        new[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}
                    });
                    break;
                default:
                    json = JsonConvert.SerializeObject(new
                    {
                        description =
                        "Solicitud 'Certificado de Estudios Promocional' generada como parte del proceso de trámite de bachiller",
                        period_list =
                        new[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}
                    });
                    break;
            }

            var acRequest = await composeRequest(student, academicProfile, true, false);

            // Datos de la solicitud
            acRequest.document_id = DOCUMENT_ID;
            acRequest.description = json;
            acRequest.state = REQUEST_FOR_ACADEMIC_RECORDS;
            acRequest.concept_id = BACHILLER_CONCEPT_ID;
            acRequest.office_id = _corp.OFFICE_RA;

            var i =
                await acRequest.createCustomRequest(user, false, REQUEST_FOR_ACADEMIC_RECORDS, BACHILLER_CONCEPT_ID);

            return i == 1 ? acRequest : null;
        }

        /// <summary>
        /// Lista de solicitudes de "Certificado de Estudios Promocional" filtrado por campus
        /// </summary>
        /// <param name="campus">Campus UC</param>
        /// <returns>Lista de solicitudes de "Certificado de Estudios Promocional" filtrado por campus</returns>
        public static async Task<List<ACRequest>> RequestListByCampusAsync(string campus)
        {
            var list = await ACRequest.RequestListByCampusAsync(campus);
            list = list.Where(w => w.document_id == DOCUMENT_ID).ToList();
            return list;
        }

        #endregion Methods
    }
}