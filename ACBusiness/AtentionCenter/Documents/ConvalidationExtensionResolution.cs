﻿using ACBusiness.Academic;
using ACBusiness.Convalidations;
using ACTools.PDF.Documents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BDUCCI;
using _acad = ACTools.Constants.AcademicConstants;
using _ate = ACTools.Constants.AtentionCenterConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    /// <summary>
    /// Solicitud de convalidación
    /// </summary>
    public class ConvalidationExtensionResolution : ACDocument
    {
        #region properties
        private const int document_id = _ate.DOC_CONVALIDATION_EXTENSION;
        private const bool aproved = false;
        public List<TranferAbstact> transfers { get; set; }
        #endregion properties

        #region constructors
        public ConvalidationExtensionResolution(AcademicProfile profile)
        {
            base.sync(document_id, profile);
            //public List<Transfer> list(decimal person_id, bool with_coures = false
            this.transfers = new Transfer().list(profile.person_id, with_coures: false)
                    .Select(f => new TranferAbstact
                    {
                        term = f.term,
                        institution_name = f.institution_name,
                        seq = f.seq,
                        tseq = f.tseq
                    }
                    ).ToList();
            this.transfers = this.transfers == null ?
                new List<TranferAbstact>():
                this.transfers;
        }
        #endregion constructors

        /// <summary>
        /// Sobreescribe el metodo de validacion
        /// ejecuta la validacion del padre y luego
        /// ejecuta la validacion de este documetno
        /// </summary>
        /// <returns></returns>
        public new bool validate()
        {
            bool valid = true;
            //valida si el estudiante es postulantes o mas
            if (this.profile.status <= _acad.STATE_POSTULANT)
            {
                valid = false;
                stack_error.Add("NO ESTA MATRICULADO.");
            }
            this.is_valid = valid;
            return valid;
        }

        /// <summary>
        /// Genera el objeto solicitud
        /// * Peculiaridades con respecto a otras solicitudes
        ///   - App -> Solicita convalidacion
        ///   - CAJA -> paga
        ///   - CAU -> recoge documentación
        ///   - COU-asis -> Verifica infromacion y crea resumen
        ///   - COU-dir -> covalida en banner
        ///   - COU-asis -> Verifica conva
        ///   - RAU -> Cierra (pasa cap en banner)
        /// </summary>
        /// <param name="student"></param>
        /// <param name="profile"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public async Task<ACRequest> createRequest(
            Student student, AcademicProfile profile, string description, TranferAbstact transfer, List<object> courses, string user)
        {
            string json = JsonConvert.SerializeObject(
                new { description = description,
                    transfer = transfer,
                    courses = courses
                }
                );
            //valida matricula
            bool valid = this.validate();
            if (!valid) return null;
            //implementar su ID y su DescSolicitud
            ACRequest request = await base.composeRequest(student, profile, with_validation:false);
            //aumenta propias de esta solicitud
            request.document_id = document_id;
            request.cost = cost.amount;
            request.description = json; // JSON
            request.aproved = aproved;
            bool base_validation = await base.validate();
            //if(base_validation)
            if (request.state != _ate.STATE_CANCEL)
                request.state = _ate.STATE_DOCUMENT_VERIFICATION;
            int i = await request.create(user);
            if (i == 1)
                return request;
            return null;
        }

        /// <summary>
        /// Generar el archivo pdf correspondiente a las solicitud
        /// </summary>
        /// <param name="request">obj Solicitud</param>
        /// <returns>string URL Path del documento</returns>
        public async Task<string> generate(ACRequest request, Student student, string user)
        {
            ACSignatory signatory_obj = new ACSignatory();
            ConvalidationPDF.Data data = new ConvalidationPDF.Data();
            List<ACSignatory> signs = signatory_obj.listByACDocument(document_id, student.profile.college.id);

            List<string> studentIdList = Student.GetAllStudentId(student.id);
            var dboContext = new DBOContext();
            var entrantInfo = dboContext.tbl_postulante
                .Where(w => studentIdList.Contains(w.IDAlumno) && w.IDEscuela == request.program)
                .OrderByDescending(o => o.IDPerAcad).ThenByDescending(o => o.IDExamen).FirstOrDefault();

            data.name_file = string.Format("{0}-{2}-{1}",request.id, student.id, request.document_number);
            //get courses from transfer
            JObject json_obj = JObject.Parse(request.description);
            int seq = int.Parse(json_obj["seq"].ToString());
            int tseq = int.Parse(json_obj["tseq"].ToString());
            Transfer transfer = new Transfer().get(request.person_id, request.term, seq: seq, tseq: tseq, with_coures:true); // FIXME forzamos 201710 pra salvar el proceso pasado

            data.user = user;
            data.signs = signatory_obj.listPDFSign(signs);
            data.student_id = student.id;
            data.student_names = student.full_name;
            data.student_gender = student.gender;
            data.college_name = profile.college.name;
            data.program_name = profile.program.name;
            data.adm_type_name = profile.adm_type.name;
            data.request_id = request.id;
            data.document_number = request.document_number;
            data.proc_institution = transfer.institution_name; 
            // get resolution code from abstract
            
            data.resolution_code = json_obj["conva_resolution"].ToString() ;
            if (string.IsNullOrEmpty(data.resolution_code))
                throw new Exception("Es obligatorio el numero de resolución para este documento");
            data.resolution_date = DateTime.Now;
            data.term = entrantInfo != null ? Term.toBanner(entrantInfo.IDPerAcad) : request.term;
            data.college_code = _acad.HEAD_FACULTAD_BY_CODE[profile.college.id];
            data.resolution_year = DateTime.Today.Year.ToString();
            //data.proc_program = transfer.program;
            data.courses = new List<ConvalidationPDF.Course>();
            foreach(var item in transfer.courses)
            {
                ConvalidationPDF.Course course = new ConvalidationPDF.Course();
                course.proc_course_name = item.proc_course_name;
                // si es menos uno es porque no tiene creditos asignado en banner
                course.proc_course_credits = (item.proc_course_credits == -1) ? "-": item.proc_course_credits.ToString();
                course.proc_course_score = item.proc_course_score;
                course.uc_course_name = item.uc_course_name;
                course.uc_course_credits = item.uc_course_credits;
                course.uc_course_score = item.uc_course_score;
                course.uc_rowspan = item.uc_repeat;
                data.courses.Add(course);
            }
            
            var output_path = ConvalidationPDF.generate(data);
            return output_path;
        }

        public class TranferAbstact
        {
            public string institution_name { get; set; }
            public string term { get; set; }
            public int seq { get; set; }
            public int tseq { get; set; }
        }

    }
}
