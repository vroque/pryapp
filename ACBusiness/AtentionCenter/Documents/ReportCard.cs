﻿using ACBusiness.Academic;
using ACTools.PDF.Documents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;
using _ate = ACTools.Constants.AtentionCenterConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    /// <summary>
    /// Boleta de Notas
    /// </summary>
    public class ReportCard : ACDocument
    {
        #region properties

        private const int document_id = _ate.DOC_REPORT_CARD;
        public List<string> term_list { get; set; }

        #endregion properties

        #region constructors

        public ReportCard(AcademicProfile profile)
        {
            base.sync(document_id, profile);
            this.syncTakenTerms();
        }

        #endregion constructors


        #region methods

        /// <summary>
        /// Sobreescribe el metodo de validacion
        /// ejecuta la validacion del padre y luego
        /// ejecuta la validacion de este documetno
        /// </summary>
        /// <returns></returns>
        public new async Task<bool> validate()
        {
            var valid = true;
            //valida si el estudiante es matriculado o mas
            if (this.profile.status < _acad.STATE_STUDENT)
            {
                valid = false;
                base.stack_error.Add("No registra matrícula.");
            }

            this.is_valid = valid;
            return valid;
        }


        /// <summary>
        /// Llena la lista de periodos(TERM) en los cuales el 
        /// estudiante registra una matricula exceptiuando el actual
        /// </summary>
        private void syncTakenTerms()
        {
            var enrObj = new Enrollment();
            this.term_list = enrObj.getOldsTermsEnrollment(this.profile);
        }


        /// <summary>
        /// sobreescribe el metodo que crea
        /// la solicitud dandole paramentos
        /// especificos para este documento
        /// </summary>
        /// <param name="student">objecto estudiante</param>
        /// <param name="profile">objeto perfil</param>
        /// <param name="description">cadena de descripción</param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ACRequest> createRequest(Student student, AcademicProfile profile, List<string> term_list,
            string description, string user)
        {
            var json = JsonConvert.SerializeObject(new {description, term_list});
            var valid = await this.validate();

            if (!valid) return null;

            var request = await base.composeRequest(student, profile);
            //aumenta propias de esta solicitud
            request.document_id = document_id;
            // Cálculo de costo a pagar según el número de periodos seleccionados
            if (term_list.Count > 1)
            {
                request.cost = cost.amount * term_list.Count;
            }
            else
            {
                request.cost = cost.amount;
            }

            request.description = json; // JSON
            if (request.state != _ate.STATE_CANCEL)
            {
                request.state = _ate.STATE_PAYMENT_PENDING; // pendiente de pago
            }

            // TODO: request.concept_id = amount.id; //FALTA IMPLEMENTAR
            var i = await request.create(user);

            return i == 1 ? request : null;
        }

        /// <summary>
        /// Generar el archivo pdf correspondiente a las solicitud
        /// </summary>
        /// <param name="request">obj Solicitud</param>
        /// <param name="student"></param>
        /// <param name="user"></param>
        /// <param name="term"></param>
        /// <param name="index"></param>
        /// <returns>string URL Path del documento</returns>
        public async Task<string> generate(ACRequest request, Student student, string user, string term, int index)
        {
            var pdf = new ReportCardPDF();
            var signatoryObj = new ACSignatory();
            var signatories = signatoryObj.listByACDocument(document_id, student.profile.college.id);

            pdf.data.name_file = $"{request.id}-{index}-{student.id}";
            pdf.data.user = user;
            pdf.data.signs = signatoryObj.listPDFSign(signatories);
            pdf.data.student_id = student.id;
            pdf.data.student_names = student.full_name;
            pdf.data.student_gender = student.gender;
            pdf.data.college_name = profile.college.name;
            pdf.data.program_name = profile.program.name;
            pdf.data.request_id = request.id;
            pdf.data.document_number = index;
            pdf.data.term = term;
            pdf.data.index = index;
            // TRAER NOTAS //falta implementar
            pdf.data.scores = new List<ReportCardPDF.Score>();
            var courseObj = new Course();
            string outputPath;
            var courses = courseObj.reportCardtByTerm(profile, term);
            if (term.CompareTo("201710") >= 0)
            {
                foreach (var course in courses)
                {
                    if (course.nrc != "CONV")
                    {
                        var score = new ReportCardPDF.Score();
                        score.average = double.Parse(course.score);
                        score.cl1 = course.score_detail.c1 ?? getScore("2-C1", course.components);
                        score.cl2 = course.score_detail.c2 ?? getScore("4-C2", course.components);
                        score.ep = course.score_detail.ep ?? getScore("3-EP", course.components);
                        score.ef = course.score_detail.ef ?? getScore("5-EF", course.components);
                        score.course_id = course.id;
                        score.course_name = course.name;
                        score.credits = course.credits;
                        score.section = course.nrc;
                        pdf.data.scores.Add(score);
                    }
                }

                outputPath = pdf.generate();
            }
            else if (term.CompareTo("201510") >= 0)
            {
                // 4 componentes
                foreach (var course in courses)
                {
                    var score = new ReportCardPDF.Score();
                    score.average = double.Parse(course.score);
                    score.cl1 = course.score_detail.c1;
                    score.cl2 = course.score_detail.c2;
                    score.ep = course.score_detail.ep;
                    score.ef = course.score_detail.ef;
                    score.course_id = course.id;
                    score.course_name = course.name;
                    score.credits = course.credits;
                    score.section = course.subject;
                    pdf.data.scores.Add(score);
                }

                outputPath = pdf.generate();
            }

            else // anteriores al 201710
            {
                var pdf_apec = new ReportCardAPECPDF(pdf.data);
                foreach (var course in courses)
                {
                    var score = new ReportCardPDF.Score();
                    score.average = double.Parse(course.score);
                    score.cl1 = course.score_detail.c1;
                    score.cl2 = course.score_detail.c2;
                    score.ta1 = course.score_detail.ta1;
                    score.ta2 = course.score_detail.ta2;
                    score.ep = course.score_detail.ep;
                    score.ef = course.score_detail.ef;
                    score.course_id = course.id;
                    score.course_name = course.name;
                    score.credits = course.credits;
                    score.section = course.subject;
                    pdf_apec.data.scores.Add(score);
                }

                outputPath = pdf_apec.generate();
            }

            if (outputPath == null) return null;

            // Actualizar solicitud
            var message = $"Solicitud generada el {DateTime.Now}, por el usuario: {user}";
            await request.goDeliveryPending(message, user);

            return outputPath;
        }

        private string getScore(string component, List<Component> components)
        {
            var c = components.FirstOrDefault(f => f.code == component);

            return c?.score.Substring(0, 4) ?? "-";
        }

        #endregion methods
    }
}