﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Admision;
using ACBusiness.Institutional;
using ACBusiness.Personal;
using ACPermanence.Contexts.BANNER;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.CAU;
using ACTools.PDF;
using ACTools.PDF.Documents;
using ACTools.SuperString;
using Newtonsoft.Json;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _academic = ACTools.Constants.AcademicConstants;
using _admission = ACTools.Constants.AdmissionConstant;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    /// <inheritdoc />
    /// <summary>
    /// Constancia de Matrícula Promocional
    /// </summary>
    public class PromotionalProofOfEnrollment : ACDocument
    {
        #region Properties

        private const int DOCUMENT_ID = _ate.DOC_PROOF_OF_ENROLLMENT_PROMOTIONAL;
        private const int REQUEST_FINALIZED = _ate.STATE_FINAL;
        private const string BACHILLER_CONCEPT_ID = _ate.BACHILLER_CONCEPT_ID;
        private const string NO_SCHOOL = "0";

        /// <summary>
        /// Sin info sobre estado académico (matriculado, estudiante, no egresado, egresado, etc)
        /// </summary>
        private const int NO_ACADEMIC_STATUS = 0;

        private const decimal NO_ID_MODALIDAD_POSTU = _admission.NO_ID_MODALIDAD_POSTU;
        private const string ING_SISTEMAS = _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA;
        private const string ING_SISTEMAS_OLD = _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA_OLD;

        private readonly BNContext _bnContext = new BNContext();

        #endregion Properties

        #region Methods

        /// <summary>
        /// Genera Lista de Estudiantes con Constancia de Matrícula Promocional listos para descargar/imprimir
        /// </summary>
        /// <param name="students">Estudiantes para los que se procesará el documento</param>
        /// <param name="academicPeriod">Período académico para el documento</param>
        /// <param name="user">Usuario que genera el documento</param>
        /// <returns>Lista de Estudiantes con la ruta de la constancia de matrícula promocional</returns>
        public async Task<List<Graduated>> GenerateAsync(Graduated[] students, string academicPeriod, string user)
        {
            var enrolled = new List<Graduated>();
            var noEnrolled = new List<Graduated>();

            // Separamos únicamente estudiantes seleccionados y que hayan ingresado a alguna carrera
            foreach (var student in students)
            {
                // TODO: Validar si estudiante realizó pago por tramite de bachiller
                if (student.school_id != NO_SCHOOL && student.is_selected &&
                    student.academic_status != NO_ACADEMIC_STATUS && student.Processing)
                {
                    var perfil = new AcademicProfile
                    {
                        person_id = Student.getPidmFromStudentId(student.student_id),
                        department = Department.toBanner(student.modality_id),
                        program = Program.get(student.school_id)
                    };
                    // Período académico notación BANNER
                    var academicPeriodBanner = Term.toBanner(academicPeriod, perfil.department);

                    // Todos los períodos matriculados del estudiante
                    var allEnrolledPeriods = new Enrollment().GetAllTermsEnrolled(perfil);

                    // buscamos si existe matrícula en el período seleccionado
                    var haveEnrolledSelectedPeriod =
                        allEnrolledPeriods.Values.Where(w => w.Contains(academicPeriodBanner)).ToList();

                    if (haveEnrolledSelectedPeriod.Any())
                    {
                        /*Obtener el ultimo movimiento*/
                        var movEstudiante = Student.GetLastMovimientoEstudianteBySchoolId(Student.getPidmFromStudentId(student.student_id),
                           student.school_id);
                        if (movEstudiante == null)
                        {
                            throw new NullReferenceException(
                                $"No se encontró registro de postulante para {student.student_id} en {student.school_id}");
                        }

                        student.modality_id = movEstudiante.CODIGO_DEPARTAMENTO;
                        student.campusId = movEstudiante.CODIGO_CAMPUS;
                        student.school_id = movEstudiante.CODIGO_PROGRAMA;
                        student.school_name = Program.get(movEstudiante.CODIGO_PROGRAMA).name;
                        student.Catalogue = movEstudiante.CATALOGO_REAL;

                        // existe matricula en el periodo del input
                        // generar constancia matricula
                        var dictPath = await GetDocumentPathAsync(student, user, academicPeriod);
                        student.doc_url = dictPath["uri"];
                        student.save_as = dictPath["save_as"];
                        if (!string.IsNullOrEmpty(student.doc_url))
                        {
                            student.download = true;
                        }

                        if (string.IsNullOrEmpty(student.doc_url))
                        {
                            student.ProcessingError = true;
                        }

                        student.Processing = false;

                        enrolled.Add(student);
                        continue;
                    }
                    else
                    {
                        // no existe matricula en el periodo del input

                        // buscar si existe matricula en el periodo anterior o posterior
                        var previousPeriodEnrolled = new List<string>();
                        var nextPeriodEnrolled = new List<string>();
                        foreach (var period in allEnrolledPeriods)
                        {
                            if (int.Parse(period.Value) < int.Parse(academicPeriodBanner))
                            {
                                previousPeriodEnrolled.Add(period.Key);
                            }

                            if (int.Parse(period.Value) > int.Parse(academicPeriodBanner))
                            {
                                nextPeriodEnrolled.Add(period.Key);
                            }
                        }

                        if (previousPeriodEnrolled.Any())
                        {
                            var periodo = previousPeriodEnrolled.OrderByDescending(o => o).FirstOrDefault();

                            // generar constancia
                            /*
                            var dict_path = await getProofOfEnrollmentPromotionalPath(student, user, periodo);
                            student.doc_url = dict_path["uri"];
                            student.save_as = dict_path["save_as"];
                            if (!string.IsNullOrEmpty(student.doc_url))
                            {
                                student.download = true;
                            }
                            */
                            student.other_period = periodo;
                            student.other_period_description = "prev";
                            student.Processing = false;

                            enrolled.Add(student);
                            continue;
                        }

                        if (nextPeriodEnrolled.Any())
                        {
                            var periodo = nextPeriodEnrolled.OrderBy(o => o).FirstOrDefault();

                            // generar constancia
                            /*var dict_path = await getProofOfEnrollmentPromotionalPath(student, user, periodo);
                            student.doc_url = dict_path["uri"];
                            student.save_as = dict_path["save_as"];
                            if (!string.IsNullOrEmpty(student.doc_url))
                            {
                                student.download = true;
                            }
                            */
                            student.other_period = periodo;
                            student.other_period_description = "next";
                            student.Processing = false;

                            enrolled.Add(student);
                            continue;
                        }

                        // aqui armar un error para ingresar en el api
                        student.doc_url = string.Empty;
                        student.ProcessingError = true;
                    }

                    enrolled.Add(student);
                }
                else
                {
                    noEnrolled.Add(student);
                }
            }

            return enrolled.Union(noEnrolled).OrderBy(o => o.student_name).ToList();
        }

        /// <summary>
        /// Genera Lista de Estudiantes con Constancia de Primera Matrícula Promocional listos para descargar/imprimir.
        /// La primera matrícula hace mención a la primera matrícula en la que el estudiante inició estudios en la UC.
        /// </summary>
        /// <param name="students">Estudiantes para los que se procesará el documento</param>
        /// <param name="user">Usuario ue realia la operación</param>
        /// <returns></returns>
        public async Task<List<Graduated>> Generate1stEnrollmentAsync(Graduated[] students, string user)
        {
            var enrolled = new List<Graduated>();
            var noEnrolled = new List<Graduated>();
            // var academicPeriod = "201810";

            // Separamos únicamente estudiantes seleccionados y que hayan ingresado a alguna carrera
            foreach (var student in students)
            {
                // TODO: Validar si estudiante realizó pago por tramite de bachiller
                if (student.school_id != NO_SCHOOL && student.is_selected &&
                    student.academic_status != NO_ACADEMIC_STATUS && student.Processing)
                {
                    var perfil = new AcademicProfile
                    {
                        person_id = Student.getPidmFromStudentId(student.student_id),
                        // department = Department.toBanner(student.modality_id),
                        program = Program.get(student.school_id)
                    };
                    
                    // buscamos si existe 1ra matrícula del estudiante
                    var primeraMatricula = _bnContext.p_ciclo_primera_matricula(perfil.person_id).FirstOrDefault();

                    if (primeraMatricula != null)
                    {
                        perfil.department = Department.toBanner(primeraMatricula.DEPARTAMENT);
                        student.modality_id = primeraMatricula.DEPARTAMENT;
                        student.school_id = primeraMatricula.PROGRAM;
                        student.school_name = Program.get(primeraMatricula.PROGRAM).name;
                        student.campusId = primeraMatricula.CAMPUS;
                        // generar constancia primera matricula
                        var dictPath = await GetDocumentPathAsync(student, user, primeraMatricula.TERM, true);
                        student.doc_url = dictPath["uri"];
                        student.save_as = dictPath["save_as"];
                        
                        if (!string.IsNullOrEmpty(student.doc_url)) student.download = true;

                        if (string.IsNullOrEmpty(student.doc_url)) student.ProcessingError = true;

                        student.Processing = false;

                        enrolled.Add(student);
                        continue;
                    }

                    student.Processing = false;
                    student.ProcessingError = true;
                    enrolled.Add(student);
                }
                else
                {
                    noEnrolled.Add(student);
                }
            }

            return enrolled.Union(noEnrolled).OrderBy(o => o.student_name).ToList();
        }

        /// <summary>
        /// Genera URL para descargar la Constancia de Matrícula Promocional
        /// </summary>
        /// <param name="student"></param>
        /// <param name="user"></param>
        /// <param name="academicPeriod"></param>
        /// <param name="firstEnrollment"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private async Task<Dictionary<string, string>> GetDocumentPathAsync(Graduated student, string user,
            string academicPeriod, bool firstEnrollment = false)
        {
            var dboContext = new DBOContext();
            var nexoContext = _db;
            var studentDepartment = Department.toBanner(student.modality_id);

            var primeraMatricula = _bnContext
                .p_ciclo_primera_matricula(Student.getPidmFromStudentId(student.student_id)).FirstOrDefault();

            if (primeraMatricula == null) return null;

            // Periodo académico notación APEC
            var academicPeriodApec = Term.toAPEC(academicPeriod, studentDepartment);
            // Período académico notación BANNER 
            var academicPeriodBanner = Term.toBanner(academicPeriod, studentDepartment);

            var college = Program.getCollege(student.school_id);

            var acSignatory = new ACSignatory();
            var signs = acSignatory.listByACDocument(DOCUMENT_ID, college.id);

            var estudiante = new Student(student.student_id);

            var postulante = new List<DBOTblPostulante>();

            var escuelaList = new List<string> { student.school_id };
            

            if (student.school_id == ING_SISTEMAS) escuelaList.Add(ING_SISTEMAS_OLD);

            var studentIdList = Student.GetAllStudentId(student.student_id);

            var academicProfile = new AcademicProfile
            {
                person_id = Student.getPidmFromStudentId(student.student_id),
                div = _academic.DEFAULT_DIV,
                campus = Campus.get(student.campusId),
                department = Department.toBanner(student.modality_id),
                subject = student.Catalogue,
                college = Program.getCollege(student.school_id),
                program = Program.get(student.school_id),
                // adm_type = AdmisionType.get(Entrant.GetCurrentModalityPostulationBySchoolIdAndDepartment(Student.getPidmFromStudentId(student.student_id), student.school_id, Department.toBanner(student.modality_id)), null, "banner")
            };

            /**
             * Generación de Solicitud para guardar evidencia de generación del documento
             */
            ACRequest acRequest;

            // Se busca si existe solicitud asociada a la creación de constancia de matricula promocional
            var requestExists =
                nexoContext.cau_requests.Where(w => w.documentID == DOCUMENT_ID &&
                                                    w.conceptID == BACHILLER_CONCEPT_ID &&
                                                    academicPeriod.Contains(w.requestDescription) &&
                                                    w.pidm == estudiante.person_id)
                    .OrderByDescending(o => o.requestID).FirstOrDefault();
            // Si existe solicitud asociada, utilizamos los datos de éste para la generación de la constancia
            if (requestExists != null)
            {
                acRequest = await ACRequest.get(requestExists.requestID);
            }
            // sino, generamos una nueva solicitud
            else
            {
                acRequest =
                    await CreateRequestForGraduationProcessAsync(academicPeriod, estudiante, academicProfile, user);
                if (ACRequest.isNull(acRequest))
                {
                    return null;
                }
            }

            var acTermInfo = await ACTerm.GetByAcademicPeriodCampusAndDepartament(academicPeriodBanner,
                academicProfile);

            // Data para el PDF
            var data = new PromotionalProofOfEnrollmentPDF.Data
            {
                name_file = $"{acRequest.id}-{acRequest.document_number}-{Student.getDNI(student.student_id)}",
                user = user,
                signs = acSignatory.listPDFSign(signs),
                student_id = student.student_id,
                student_names = student.student_name,
                student_gender = string.Empty,
                college_name = college.name,
                program_name = student.school_name,
                request_id = acRequest.id,
                document_number = acRequest.document_number,
                photo_bytes = new Person().getPhoto(Student.getPidmFromStudentId(student.student_id)),
                Term = ACTerm.cleanForDocuments(academicPeriodApec),
                Period = firstEnrollment
                    ? primeraMatricula.NOMINALCYCLE.numberToRoman()
                    : Student.cycleByAcademicPeriod(academicPeriodBanner, academicProfile).numberToRoman(),
                DateTermEnrolled = acTermInfo.start_classes_date,
                StudentDni = Student.getDNI(student.student_id),
                StudyMode = _admission.STUDY_MODE[student.modality_id],
                GenderData = Student.getGenderData(student.student_id)
            };

            var proofOfEnrollment = PDFActions.exists(data.name_file)
                ? new Dictionary<string, string>
                {
                    {"uri", PDFActions.getRepoDocumentPath(data.name_file)},
                    {"save_as", data.name_file}
                }
                : new Dictionary<string, string>
                {
                    {"uri", PromotionalProofOfEnrollmentPDF.Generate(data)},
                    {"save_as", data.name_file}
                };

            // Eliminar la solicitud cuando no se haya generado el documento
            if (proofOfEnrollment["uri"].Any())
            {
                return proofOfEnrollment;
            }

            var req = new CAUTblRequest {requestID = acRequest.id};
            nexoContext.cau_requests.Remove(req);
            var res = await nexoContext.SaveChangesAsync();

            return proofOfEnrollment;
        }

        /// <summary>
        /// <para>
        /// Creación de solicitud para generar constancias relacionadas al proceso de trámite de bachiller.
        /// </para>
        /// Esta solicitud y constancia estan relacionados unicamente al proceso de trámite de bachiller,
        /// se crea con costo = 0 y el estado de solicitud = "finalizado"
        /// </summary>
        /// <param name="academicPeriod">Período académico</param>
        /// <param name="student">Datos del estudiante</param>
        /// <param name="academicProfile">Perfil académico del estudiante</param>
        /// <param name="user">Usuario que genera la solicitud</param>
        /// <returns>Solicitud de creación de constancia de matrícula asociada al proceso de trámite de bachiller</returns>
        private async Task<ACRequest> CreateRequestForGraduationProcessAsync(string academicPeriod, Student student,
            AcademicProfile academicProfile, string user)
        {
            var json = JsonConvert.SerializeObject(new
            {
                description = "Solicitud generada como parte del proceso de trámite por bachiller",
                term = academicPeriod
            });

            var acRequest = await composeRequest(student, academicProfile, true, false);

            // Datos de la solicitud
            acRequest.document_id = DOCUMENT_ID;
            acRequest.description = json;
            acRequest.state = REQUEST_FINALIZED;
            acRequest.concept_id = BACHILLER_CONCEPT_ID;

            var i = await acRequest.createCustomRequest(user, false, REQUEST_FINALIZED, BACHILLER_CONCEPT_ID);

            return i == 1 ? acRequest : null;
        }

        #endregion Methods
    }
}