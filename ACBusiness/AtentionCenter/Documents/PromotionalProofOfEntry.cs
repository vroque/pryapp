using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Accounts;
using ACBusiness.Admision;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACTools.PDF;
using ACTools.PDF.Documents;
using Newtonsoft.Json;
using _acad = ACTools.Constants.AcademicConstants;
using _admission = ACTools.Constants.AdmissionConstant;
using _ate = ACTools.Constants.AtentionCenterConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    /// <inheritdoc />
    /// <summary>
    /// Constancia de Ingreso Promocional
    /// </summary>
    public class PromotionalProofOfEntry : ACDocument
    {
        #region Properties

        public string student_name { get; set; }
        public string student_id { get; set; }

        /// <summary>
        /// Modalidad de Postulación (IDModalidadPostu)
        /// </summary>
        public decimal type_of_income_id { get; set; }

        /// <summary>
        /// Nombre de la Modalidad de Postulación (Nombre de IDModalidadPostu)
        /// </summary>
        public string type_of_income_name { get; set; }

        public string entry_period { get; set; }
        public string graduation_period { get; set; }
        public string school_id { get; set; }
        public string school_name { get; set; }

        /// <summary>
        /// Modalidad (IDEscuelaADM)
        /// </summary>
        public string modality_id { get; set; }

        public string modality_name { get; set; }
        public bool is_selected { get; set; }
        public int payment_status { get; set; }
        public string doc_url { get; set; }
        public bool download { get; set; }
        public string save_as { get; set; }
        public bool Processing { get; set; }
        public bool ProcessingError { get; set; }

        private const int DODUMENT_ID = _ate.DOC_ENTRY_PROOF;
        private const int REQUEST_FINALIZED = _ate.STATE_FINAL;
        private const string BACHILLER_CONCEPT_ID = _ate.BACHILLER_CONCEPT_ID;
        private const string NO_SCHOOL = "0";
        private const int NOT_PAID = 0; // Sin pagar
        private const int IS_PAID = 1; // Pagado
        private const int SELECTED_BUT_NOT_PAID = 2; // Seleccionado pero Estudiante no pagó por el proceso
        private const int SELECTED_BUT_NOT_GRADUATE = 3; // Seleccionado pero no es egresado

        #endregion Properties

        #region Methods

        /// <summary>
        /// Genera Lista de Estudiantes con Constancias de Ingreso listos para descargar/imprimir
        /// </summary>
        /// <param name="students">Estudiantes para los que se generarán constancias de ingreso</param>
        /// <param name="user">Usuario que genera el documento</param>
        /// <returns>Lista de Estudiantes con la ruta de la constancia de ingreso</returns>
        public static async Task<List<PromotionalProofOfEntry>> GenerateProofOfEntry(PromotionalProofOfEntry[] students,
            string user)
        {
            var dboContext = new DBOContext();
            var graduateIdList = new List<string>();
            var noGraduateIdList = new List<string>();

            // Separamos códigos de estudiantes que aún no hayan egresado para mostrar texto de no egresado
            foreach (var student in students)
            {
                var graduate = dboContext.tbl_alumno_egresado.FirstOrDefault(w => w.IDAlumno == student.student_id &&
                                                                                  w.IDEscuela == student.school_id);

                if (graduate != null)
                {
                    graduateIdList.Add(student.student_id);
                }
                else
                {
                    noGraduateIdList.Add(student.student_id);
                }
            }

            var graduates = students.Where(w => graduateIdList.Contains(w.student_id)).ToList();
            var noGraduates = students.Where(w => noGraduateIdList.Contains(w.student_id)).ToList();

            // De los que egresaron, verificamos que hayan pagado por el trámite de bachiller y proseguir con el flujo

            foreach (var student in graduates)
            {
                if (!student.is_selected || student.school_id == NO_SCHOOL || !student.Processing) continue;

                if (!Debt.isBachillerAccountPaid(student.student_id, student.school_id))
                {
                    student.payment_status = SELECTED_BUT_NOT_PAID;
                    student.doc_url = string.Empty;
                    continue;
                }

                student.payment_status = IS_PAID;

                // Obtenemos datos de ingreso
                var postulante = dboContext.tbl_postulante.Where(w =>
                        w.IDAlumno == student.student_id && w.IDEscuela == student.school_id && w.Ingresante == "1")
                    .OrderByDescending(o => o.IDPerAcad).FirstOrDefault();

                if (postulante != null)
                {
                    student.modality_id = postulante.IDEscuelaADM;
                    student.modality_name = _admission.MODALITY_OF_ADMISSION[student.modality_id];
                    student.type_of_income_id = postulante.IDModalidadPostu;
                    student.type_of_income_name = Academic.Admission.getTypeOfIncomeName(student.type_of_income_id);
                    student.entry_period = postulante.IDPerAcad;
                }

                var dictPath = await GetProofOfEntryPath(student, user);
                student.doc_url = dictPath["uri"];
                student.save_as = dictPath["save_as"];

                if (!string.IsNullOrEmpty(student.doc_url)) student.download = true;

                if (string.IsNullOrEmpty(student.doc_url)) student.ProcessingError = true;

                student.Processing = false;
            }

            foreach (var student in noGraduates)
            {
                if (!student.is_selected || student.school_id == NO_SCHOOL) continue;
                student.payment_status = SELECTED_BUT_NOT_GRADUATE;
                student.Processing = false;
            }

            return graduates.Union(noGraduates).OrderBy(o => o.student_name).ToList();
        }

        /// <summary>
        /// Genera la URL para descargar la constancia de ingreso
        /// </summary>
        /// <param name="student">Código de estudiante</param>
        /// <param name="user">Usuario que genera el documento</param>
        /// <returns>URL de una determinada constancia de ingreso</returns>
        private static async Task<Dictionary<string, string>> GetProofOfEntryPath(PromotionalProofOfEntry student,
            string user)
        {
            var dboContext = new DBOContext();
            var nexoContext = new NEXOContext();
            var college = Program.getCollege(student.school_id);

            var acSignatory = new ACSignatory();
            var signs = acSignatory.listByACDocument(DODUMENT_ID, college.id);

            var estudiante = new Student(student.student_id);
            var admissionTypeId = student.type_of_income_id.ToString(CultureInfo.InvariantCulture);

            /**
             * Para las modalidades de postulación (IDModalidadPostu) que tengan un solo caracter se antepone un 0 
             * para que se pueda comparar con los valores de DIM.tblAdmType
             * p.e.:
             * En BDUCCI.dbo.tblPostulante, existe valores IDModalidadPostu = 6 
             * sin embargo en BDINTBANNER.DIM.tblAdmType el id de este tipo de modalidad = "06"
             */
            if (student.type_of_income_id < 10 && student.type_of_income_id > 0)
            {
                admissionTypeId = "0" + student.type_of_income_id.ToString(CultureInfo.InvariantCulture);
            }

            var alumnoEgresado = dboContext.tbl_alumno_egresado.FirstOrDefault(w =>
                w.IDAlumno == student.student_id && w.IDEscuela == student.school_id);

            var academicProfile = new AcademicProfile();

            if (alumnoEgresado != null)
            {
                academicProfile = new AcademicProfile
                {
                    person_id = Student.getPidmFromStudentId(student.student_id),
                    div = alumnoEgresado.IDDependencia,
                    campus = Campus.getByAPECCode(alumnoEgresado.IDSede),
                    department = Department.toBanner(student.modality_id),
                    subject = alumnoEgresado.IDPlanEstudio,
                    college = Program.getCollege(student.school_id),
                    program = Program.get(student.school_id),
                    levl = _acad.LEVL_PREGRADO,
                    adm_type = AdmisionType.get(admissionTypeId, student.profile),
                    status = _acad.STATE_ENROLMENT,
                    status_name = _acad.STUDENT_STATES[_acad.STATE_ENROLMENT],
                    terms = null // IDPerAcad
                };
            }

            ACRequest acRequest; // Solicitud

            // Se busca si existe solicitud asociada a la creación de la constancia de ingreso
            // relacionada al proceso de trámite de bachiller
            var requestExists = nexoContext.cau_requests.Where(w => w.documentID == DODUMENT_ID &&
                                                                    w.conceptID == BACHILLER_CONCEPT_ID &&
                                                                    w.pidm == estudiante.person_id)
                .OrderByDescending(o => o.requestID).FirstOrDefault();
            // Si existe solicitud asociada, utilizamos los datos de éste para la generación de la constancia
            if (requestExists != null)
            {
                acRequest = await ACRequest.get(requestExists.requestID);
            }
            // Sino, generamos una nueva solicitud
            else
            {
                acRequest = await student.CreateRequestForGraduateProccess(estudiante, academicProfile, user);
                if (ACRequest.isNull(acRequest))
                {
                    return null;
                }
            }

            var data = new ProofOfEntryPDF.Data
            {
                name_file = $"{acRequest.id}-{acRequest.document_number}-{Student.getDNI(student.student_id)}",
                user = user,
                signs = acSignatory.listPDFSign(signs),
                student_id = student.student_id,
                student_names = student.student_name,
                student_gender = estudiante.gender,
                student_dni = Student.getDNI(student.student_id),
                college_name = college.name,
                program_name = student.school_name,
                request_id = acRequest.id,
                document_number = acRequest.document_number,
                adm_term = student.entry_period,
                adm_type_name = student.type_of_income_name,
                study_mode = _admission.STUDY_MODE[student.modality_id],
                gender_civil_status = Student.getGenderData(student.student_id)["civil_status"],
                gender_concerned = Student.getGenderData(student.student_id)["concerned"],
                gender_identified = Student.getGenderData(student.student_id)["identified"],
                date_of_admission = Entrant.GetDateOfAdmission(student.student_id, student.entry_period,
                    student.school_id, student.modality_id)
            };

            return PDFActions.exists(data.name_file)
                ? new Dictionary<string, string>
                {
                    {"uri", PDFActions.getRepoDocumentPath(data.name_file)},
                    {"save_as", data.name_file}
                }
                : new Dictionary<string, string>
                {
                    {"uri", ProofOfEntryPDF.generate(data)},
                    {"save_as", data.name_file}
                };
        }


        /// <summary>
        /// <para>
        /// Creación de solicitud para generar constancia de ingreso (Relacionada al proceso de trámite de bachiller).
        /// </para>
        /// Esta solicitud y constancia de ingreso estan relacionados unicamente al proceso de trámite de bachiller,
        /// se crea con costo = 0 (sin embargo se realiza una validación de que el estudiante haya realizado el pago
        /// por el trámite de bachiller) y el estado de solicitud = "finalizado"
        /// </summary>
        /// <param name="student">Datos del Estudiante</param>
        /// <param name="academicProfile">Perfil académico del estudiante</param>
        /// <param name="user">Usuario que genera la solicitud</param>
        /// <returns>Solicitud de creación de constancia de ingreso asociada al proceso de trámite de bachiller</returns>
        private async Task<ACRequest> CreateRequestForGraduateProccess(Student student,
            AcademicProfile academicProfile, string user)
        {
            var json = JsonConvert.SerializeObject(new
            {
                description = "Solicitud generada como parte del proceso de trámite por bachiller"
            });

            var acRequest = await composeRequest(student, academicProfile, true, false);

            // Datos de la solicitud
            acRequest.document_id = DODUMENT_ID;
            acRequest.description = json;
            acRequest.state = REQUEST_FINALIZED;
            acRequest.concept_id = BACHILLER_CONCEPT_ID;

            var i = await acRequest.createCustomRequest(user, false, REQUEST_FINALIZED, BACHILLER_CONCEPT_ID);

            return i == 1 ? acRequest : null;
        }

        #endregion Methods

        #region Implicit Operators

        /// <summary>
        /// Operador implícito para convertir data de tipo 'Entrants' (ingresantes a la UC) a 'ProofOfEntry'
        /// </summary>
        /// <param name="entrants">Objeto de tipo Entrants</param>
        /// <returns>Objeto de tipo ProofOfEntry</returns>
        public static implicit operator PromotionalProofOfEntry(Entrant entrants)
        {
            var proofOfEntry = new PromotionalProofOfEntry
            {
                student_id = entrants.student_id,
                student_name = entrants.student_name,
                type_of_income_id = entrants.type_of_income_id,
                type_of_income_name = entrants.type_of_income_name,
                entry_period = entrants.entry_period,
                graduation_period = string.Empty,
                school_id = entrants.school_id,
                school_name = entrants.school_name,
                modality_id = entrants.modality_id,
                modality_name = entrants.modality_name,
                is_selected = false,
                payment_status = NOT_PAID,
                doc_url = string.Empty,
                download = false
            };
            return proofOfEntry;
        }

        #endregion Implicit Operators
    }
}