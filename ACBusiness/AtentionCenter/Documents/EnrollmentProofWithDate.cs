﻿using ACBusiness.Academic;
using ACTools.PDF.Documents;
using ACTools.SuperString;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;
using _ate = ACTools.Constants.AtentionCenterConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    /// <summary>
    /// Constancia de Matrícula
    /// 
    /// Acerca de "Proof Of Enrollment":
    /// https://uwaterloo.ca/registrar/current-students/proof-or-verification-enrolment
    /// </summary>
    public class EnrollmentProofWithDate : ACDocument
    {
        #region properties
        private static int document_id = _ate.DOC_ENROLLMENT_PROOF_WITH_DATE; // Constancia de Matrícula
        public List<string> period_list { get; set; }
        public bool is_first_week { get; set; }  // Sólo se emite constancia hasta la primera semana después de la matricula
        #endregion properties

        #region constructors
        public EnrollmentProofWithDate(AcademicProfile profile)
        {
            base.sync(document_id, profile);
            this.isFirstWeek();
            this.syncTakenPeriods();
        }
        #endregion constructors

        #region methods
        /// <summary>
        /// Sobreescribe el metodo de validacion
        /// ejecuta la validacion del padre y luego
        /// ejecuta la validacion de este documento
        /// </summary>
        /// <returns></returns>
        public new async Task<bool> validate()
        {
            bool is_enrollment = true;
            Enrollment enr_obj = new Enrollment();
            // Valida si el estudiante es matriculado o más
            if (this.profile.status < _acad.STATE_STUDENT)
            {
                is_enrollment = false;
                base.stack_error.Add("No registra matrícula.");
            }

            bool has_enrollment = enr_obj.hasEnrollmentInThisTerm(profile);
            if (!has_enrollment)
            {
                this.stack_error.Add("No se encuentra estudiando actualmente.");
                is_enrollment = false;
            }

            bool has_enrollmentRangeInitClassEndTerm = enr_obj.hasEnrollmentInTermRangeInitClassEndTerm(profile, 0, 0);

            if (!has_enrollmentRangeInitClassEndTerm)
            {
                this.stack_error.Add("No se puede generar el documento en estas fechas.");
                has_enrollmentRangeInitClassEndTerm = false;
            }

            bool is_first_week = true;
            // Valida si se encuentra dentro de la primera semana posterior a la matrícula
            if (this.profile.status == _acad.STATE_STUDENT && !this.is_first_week)
            {
                is_first_week = false;
                stack_error.Add("Solo se puede solicitar la Constancia de Matrícula durante los primeros 7 días posteriores a la matrícula.");
            }

            this.is_valid = is_enrollment && is_first_week && has_enrollmentRangeInitClassEndTerm;
            return this.is_valid;
        }


        /// <summary>
        /// Solo se emiten Constancias de Matricula hasta la primera semana después de realizada la matrícula
        /// </summary>
        public void isFirstWeek()
        {
            //DateTime today = DateTime.Now;
            //DateTime sevenDaysAgo = today.AddDays(-7);
            //int count_period = _db.dim_student_enrollments
            //    .Join(_db.dim_terms,
            //          student => student.term,
            //          term => term.term,
            //          (student, term) => new { student, term })
            //    .Where(f =>
            //        f.student.pidm == person_id
            //        && f.student.divs == profile.div
            //        && f.student.levl == profile.levl
            //        && f.student.program == profile.program.id
            //        && (f.term.startDate >= sevenDaysAgo && f.term.startDate <= today)
            //        && f.term.endDate > DateTime.Today
            //    ).Select(f => f.term.term)
            //    .ToList().Count();
            //this.is_first_week = count_period == 0 ? false : true;
            this.is_first_week = true;
        }


        /// <summary>
        /// Llena la lista de periodos en los cuales el 
        /// estudiante registra una matricula con antiguedad NO mayor a 7 días
        /// </summary>
        private void syncTakenPeriods()
        {
            string term = new ACTerm().getCurrent(this.profile.campus.id, this.profile.department).term;
            // solo el periodo actual y solo si esta matriculado
            this.period_list = new List<string>();
            bool has_enrollment = new Enrollment().hasEnrollmentInThisTerm(profile);
            if (has_enrollment)
                this.period_list.Add(term);
        }


        /// <summary>
        /// Sobreescribe el método que crea
        /// la solicitud dándole parámentos
        /// específicos para este documento
        /// </summary>
        /// <param name="student">objecto estudiante</param>
        /// <param name="profile">objeto perfil</param>
        /// <param name="description">cadena de descripción</param>
        /// <returns></returns>
        public async Task<ACRequest> createRequest(
            Student student, AcademicProfile profile, List<string> period_list, string description, string user)
        {
            string json = JsonConvert.SerializeObject(new { description = description, period_list = period_list });
            bool valid = await this.validate();
            if (!valid)
            {
                return null;
            }
            ACRequest request = await base.composeRequest(student, profile);
            // Aumenta propias de esta solicitud
            request.document_id = document_id;
            // Cálculo de costo a pagar según el número de periodos seleccionados
            if (period_list.Count > 1)
            {
                request.cost = cost.amount * period_list.Count;
            }
            else
            {
                request.cost = cost.amount;
            }

            request.description = json; // JSON
            if (request.state != _ate.STATE_CANCEL)
            {
                request.state = _ate.STATE_PAYMENT_PENDING; // Pendiente de pago
            }
            //request.concept_id = amount.id; //FALTA IMPLEMENTAR
            int i = await request.create(user);
            if (i == 1)
            {
                return request;
            }
            return null;
        }

        /// <summary>
        /// Generar PDF
        /// </summary>
        /// <param name="request"></param>
        /// <param name="student"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<string> generate(ACRequest request, Student student, string user)
        {
            ACSignatory signatory_obj = new ACSignatory();
            ACTerm term_obj = new ACTerm();
            EnrollmentProofWithDatesPDF.Data data = new EnrollmentProofWithDatesPDF.Data();
            List<ACSignatory> signs = signatory_obj.listByACDocument(
                document_id, student.profile.college.id);

            data.name_file = string.Format(
                "{0}-{2}-{1}",
                request.id, student.id, request.document_number);
            data.user = user;
            data.signs = signatory_obj.listPDFSign(signs);
            data.student_id = student.id;
            data.student_names = student.full_name;
            data.student_gender = student.gender;
            data.college_name = profile.college.name;
            data.program_name = profile.program.name;
            data.request_id = request.id;
            data.document_number = request.document_number;
            data.term = ACTerm.cleanForDocuments(period_list.FirstOrDefault());
            ACTerm term__obj = new ACTerm();
            ACTerm term = term_obj.getCurrent(this.profile.campus.id, this.profile.department);

            data.start_date = term.start_classes_date;
            data.end_date = term.end_classes_date;
            int cycle = this.profile.actualCycle();
            data.period = cycle.numberToRoman();

            string output_path = EnrollmentProofWithDatesPDF.generate(data);
            /// Actualizar solicitud
            if (output_path != null)
            {
                string message = string.Format(
                    "Solicitud generada el {0}, por el usuario: {1}"
                    , DateTime.Now, user);
                await request.goDeliveryPending(message, user);
            }
            return output_path;
        }
        #endregion methods
    }
}
