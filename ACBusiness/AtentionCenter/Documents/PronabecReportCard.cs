﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Admision;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using ACTools.PDF.Documents;
using Newtonsoft.Json;
using _admission = ACTools.Constants.AdmissionConstant;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    public class PronabecReportCard : ACDocument
    {
        private const int DOCUMENT_ID = _ate.DOC_REPORT_CARD;
        private const int REQUEST_FINALIZED = _ate.STATE_FINAL;
        private const string NO_SCHOOL = "0";
        private const string ING_SISTEMAS = _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA;
        private const string ING_SISTEMAS_OLD = _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA_OLD;

        private readonly DBOContext _dboContext = new DBOContext();
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #region Methods

        /// <summary>
        /// Genera Boleta de Notas PRONABEC solo de estudiantes seleccionados.
        /// Retorna la lista original con boleta de notas solo para estudiantes pronabec seleccionados
        /// </summary>
        /// <param name="scholarshipStudents">Estudiantes PRONABEC</param>
        /// <param name="pronabecType">Tipo de PRONABEC</param>
        /// <param name="schoolId">Escuela académica</param>
        /// <param name="academicPeriod">Período académico</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns>Lista original con boleta de notas solo para estudiantes pronabec seleccionados</returns>
        public async Task<List<ScholarshipStudent>> GenerateAsync(IEnumerable<ScholarshipStudent> scholarshipStudents,
            string pronabecType, string schoolId, string academicPeriod, string user)
        {
            var scholarshipStudentList = new List<ScholarshipStudent>();
            var noSelected = new List<ScholarshipStudent>();
            
            // Separamos únicamente los estudiantes seleccionados
            foreach (var student in scholarshipStudents)
            {
                if (student.SchoolId != NO_SCHOOL && student.IsSelected && student.Processing)
                {
                    var perfil = new AcademicProfile
                    {
                        person_id = student.StudentPidm,
                        department = Department.toBanner(student.ModalityId),
                        program = Program.get(student.SchoolId)
                    };
                    // Período académico notación BANNER
                    var academicPeriodBanner = Term.toBanner(academicPeriod, perfil.department);

                    var dictPath = await GetDocumentPathAsync(student, user, academicPeriod);
                    student.DocUrl = dictPath["url"];
                    student.SaveAs = dictPath["saveAs"];
                    
                    if (!string.IsNullOrEmpty(student.DocUrl))
                    {
                        student.Download = true;
                    }

                    if (string.IsNullOrEmpty(student.DocUrl))
                    {
                        student.ProcessingError = true;
                    }

                    student.Processing = false;
                    scholarshipStudentList.Add(student);
                }
                else
                {
                    noSelected.Add(student);
                }
            }

            return scholarshipStudentList.Union(noSelected).OrderBy(o => o.StudentName).ToList();
        }

        /// <summary>
        /// Genera URL para descargar la Boleta de Notas PRONABEC
        /// </summary>
        /// <param name="student">Estudiante PRONABEC</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <param name="academicPeriod">Período académico</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private async Task<Dictionary<string, string>> GetDocumentPathAsync(ScholarshipStudent student, string user,
            string academicPeriod)
        {
            var studentDepartment = Department.toBanner(student.ModalityId);

            // Período académico notación BANNER 
            var academicPeriodBanner = Term.toBanner(academicPeriod, studentDepartment);

            var college = Program.getCollege(student.SchoolId);

            var acSignatory = new ACSignatory();
            var signs = acSignatory.listByACDocument(DOCUMENT_ID, college.id);

            var estudiante = new Student(student.StudentId);

            var studentIdList = Student.GetAllStudentId(student.StudentId);
            var escuelaList = new List<string> {student.SchoolId};
            if (student.SchoolId == ING_SISTEMAS)
            {
                escuelaList.Add(ING_SISTEMAS_OLD);
            }

            var postulante = _dboContext.tbl_postulante.Where(w => studentIdList.Contains(w.IDAlumno) &&
                                                                  escuelaList.Contains(w.IDEscuela) &&
                                                                  w.Ingresante == _admission.INGRESANTE &&
                                                                  w.Renuncia == _admission.NO_RENUNCIA_CARRERA &&
                                                                  w.IDModalidadPostu == student.TypeOfIncomeId &&
                                                                  w.IDDependencia == _corp.UC)
                .OrderByDescending(o => o.IDExamen).FirstOrDefault();

            if (postulante == null)
            {
                throw new Exception("No se encontró registro de postulación para el estudiante");
            }

            var newPeriodApec = postulante.IDPerAcad;
            var idDependencia = postulante.IDDependencia;
            var idPlanEstudio = postulante.IDPlanestudio;
            var alumnoEstado = _dboContext.tbl_alumno_estado.Where(w => w.IDAlumno == student.StudentId &&
                                                                       w.IDPerAcad == newPeriodApec)
                .OrderByDescending(o => o.FecMatricula).FirstOrDefault();
            var idSede = alumnoEstado != null ? alumnoEstado.IDSede : string.Empty;

            var academicProfile = new AcademicProfile
            {
                person_id = student.StudentPidm,
                div = idDependencia ?? _corp.UC,
                campus = Campus.getByAPECCode(idSede),
                department = Department.toBanner(student.ModalityId),
                subject = idPlanEstudio,
                college = Program.getCollege(student.SchoolId),
                program = Program.get(student.SchoolId),
                adm_type = AdmisionType.get(Entrant.GetEntrantInfo(student.StudentId, student.SchoolId)
                    .type_of_income_id.ToString(CultureInfo.InvariantCulture), null, "apec"),
            };

            /**
             * Generación de la solicitud para guardar evidencia de generación del documento
             */
            var acRequest = await CreateRequestForPronabecAsync(academicPeriod, estudiante, academicProfile, user);
            if (ACRequest.isNull(acRequest)) return null;

            var acTermInfo =
                await ACTerm.GetByAcademicPeriodCampusAndDepartament(academicPeriodBanner, academicProfile);

            // Data para el PDF
            var dataPdf = new ReportCardPDF
            {
                data =
                {
                    name_file = $"{acRequest.id}-{acRequest.document_number}-{Student.getDNI(student.StudentId)}",
                    user = user,
                    signs = acSignatory.listPDFSign(signs),
                    student_id = Student.getDNI(student.StudentId),
                    student_names = student.StudentName,
                    student_gender = string.Empty, // TODO: Calcular género
                    college_name = college.name,
                    program_name = student.SchoolName,
                    request_id = acRequest.id,
                    document_number = acRequest.document_number,
                    term = academicPeriod,
                    index = acRequest.document_number, // TODO: WTF is index?
                }
            };

            dataPdf.data.scores = new List<ReportCardPDF.Score>();
            var course = new Course();
            string outputPath;

            var courses = course.reportCardtByTerm(academicProfile, academicPeriod);
            if (academicPeriodBanner.CompareTo("201710") >= 0)
            {
                foreach (var c in courses)
                {
                    if (c.nrc != "CONV")
                    {
                        var score = new ReportCardPDF.Score
                        {
                            average = double.Parse(c.score),
                            cl1 = c.score_detail.c1 ?? GetScore("2-C1", c.components),
                            cl2 = c.score_detail.c2 ?? GetScore("4-C2", c.components),
                            ep = c.score_detail.ep ?? GetScore("3-EP", c.components),
                            ef = c.score_detail.ef ?? GetScore("5-EF", c.components),
                            course_id = c.id,
                            course_name = c.name,
                            credits = c.credits,
                            section = c.nrc
                        };
                        dataPdf.data.scores.Add(score);
                    }
                }

                outputPath = dataPdf.generate();
            }
            else if (academicPeriodBanner.CompareTo("201510") >= 0)
            {
                // 4 componentes
                foreach (var c in courses)
                {
                    var score = new ReportCardPDF.Score
                    {
                        average = double.Parse(c.score),
                        cl1 = c.score_detail.c1,
                        cl2 = c.score_detail.c2,
                        ep = c.score_detail.ep,
                        ef = c.score_detail.ef,
                        course_id = c.id,
                        course_name = c.name,
                        credits = c.credits,
                        section = c.subject
                    };
                    dataPdf.data.scores.Add(score);
                }

                outputPath = dataPdf.generate();
            }
            else // anteriores al 201710
            {
                var pdfApec = new ReportCardAPECPDF(dataPdf.data);
                foreach (var c in courses)
                {
                    var score = new ReportCardPDF.Score
                    {
                        average = double.Parse(c.score),
                        cl1 = c.score_detail.c1,
                        cl2 = c.score_detail.c2,
                        ta1 = c.score_detail.ta1,
                        ta2 = c.score_detail.ta2,
                        ep = c.score_detail.ep,
                        ef = c.score_detail.ef,
                        course_id = c.id,
                        course_name = c.name,
                        credits = c.credits,
                        section = c.subject
                    };
                    pdfApec.data.scores.Add(score);
                }

                outputPath = pdfApec.generate();
            }

            var reportCard = new Dictionary<string, string>
                {
                    {"url", outputPath},
                    {"saveAs", student.StudentName}
                };

            if (outputPath.Any()) return reportCard;

            // Eliminar la solicitud cuando no se haya generado el documento
            var req = new CAUTblRequest {requestID = acRequest.id};
            _nexoContext.cau_requests.Remove(req);
            var res = await _nexoContext.SaveChangesAsync();
            return reportCard;
        }

        private static string GetScore(string component, IEnumerable<Component> components)
        {
            var c = components.FirstOrDefault(s => s.code == component);
            return c?.score.Substring(0, 4) ?? "-";
        }

        /// <summary>
        /// <para>Creación de solicitud para generar boleta de notas PRONABEC.</para>
        /// Se crea con costo = 0 y estado de solicitud = "finalizado".
        /// </summary>
        /// <param name="academicPeriod">Período académico</param>
        /// <param name="student">Datos del estudiante</param>
        /// <param name="academicProfile">Perfil académico del estudiante</param>
        /// <param name="user">Usuario que genera la solicitud</param>
        /// <returns>Solicitud para generar boleta de notas PRONABEC</returns>
        private async Task<ACRequest> CreateRequestForPronabecAsync(string academicPeriod, Student student,
            AcademicProfile academicProfile, string user)
        {
            var json = JsonConvert.SerializeObject(new
            {
                description = "Solicitud generada automaticamente para la boleta de notas PRONABEC",
                term = academicPeriod
            });

            var acRequest = await composeRequest(student, academicProfile, true, false);

            // Datos de la solicitud
            acRequest.document_id = DOCUMENT_ID;
            acRequest.description = json;
            acRequest.state = REQUEST_FINALIZED;
            acRequest.concept_id = string.Empty;

            var i = await acRequest.createCustomRequest(user, false, REQUEST_FINALIZED, string.Empty);

            return i == 1 ? acRequest : null;
        }

        #endregion Methods
    }
}
