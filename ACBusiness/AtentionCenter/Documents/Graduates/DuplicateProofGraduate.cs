﻿using ACBusiness.Academic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _acad = ACTools.Constants.AcademicConstants;
using Newtonsoft.Json;
using ACTools.PDF.Documents;
using ACBusiness.Personal;
using _admission = ACTools.Constants.AdmissionConstant;
using ACBusiness.Institutional;

namespace ACBusiness.AtentionCenter.Documents.Graduates
{
    /// <summary>
    /// Duplicado de constancia de egresado
    /// </summary>
    public class DuplicateProofGraduate: ACDocument
    {
        #region Propiedades
        private int documentID = _ate.DOC_DUPLICATE_PROOF_GRADUATE;
        #endregion Propiedades

        #region Constructor
        public DuplicateProofGraduate(AcademicProfile profile) {
            this.is_valid = true;
            base.sync(documentID, profile);
            this.validateDuplicate();
        }
        #endregion Constructor

        #region Metodos
        /// <summary>
        /// Validaciones extra para el documento
        /// </summary>
        /// <returns></returns>
        public bool validateDuplicate()
        {
            if (this.profile.status < _acad.STATE_GRADUATE)
            {
                this.stack_error.Add($"No ha culminado en la carrera {this.profile.program.name}.");
                this.is_valid = false;
            }

            var egresado = Graduated.GetGraduationByStudentAndSchool(profile.person_id, profile.program.id);
            if (egresado == null || egresado.FechaEgresoUC == null || egresado.FechaEgresoUC > DateTime.Now)
            {
                this.stack_error.Add("No se asignó fecha de egresado o es inválido.");
                this.is_valid = false;
            }
            return this.is_valid;
        }

        /// <summary>
        /// Crear solicitud
        /// </summary>
        /// <param name="student"></param>
        /// <param name="profile"></param>
        /// <param name="description"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ACRequest> createRequest(Student student, AcademicProfile profile, string description, string user)
        {
            string json = JsonConvert.SerializeObject(new { description = description });
            // Validar
            bool valid = this.validateDuplicate();
            if (!valid) return null;

            //implementar su ID y su DescSolicitud
            ACRequest request = await base.composeRequest(student, profile);

            request.document_id = this.documentID;
            request.cost = cost.amount;
            request.description = json; // JSON
            bool baseValidation = await base.validate();
            if (request.state != _ate.STATE_CANCEL)
                request.state = _ate.STATE_PAYMENT_PENDING; // pendiente de pago
            int i = await request.create(user);
            if (i == 1)
                return request;
            return null;
        }

        /// <summary>
        /// Generar el documento correspondiente a la solicitud
        /// </summary>
        /// <param name="request"></param>
        /// <param name="student"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<string> generate(ACRequest request, Student student, string user) {
            if (!this.validateDuplicate()) throw new Exception("No se puede generar el documento.");
            var acSignatory = new ACSignatory();
            var egresado = Graduated.GetGraduationByStudentAndSchool(student.person_id, request.program);
            var academicProfile = new AcademicProfile
            {
                person_id = student.person_id,
                div = egresado.IDDependencia,
                department = Department.toBanner(egresado.ModalidadEgreso),
                college = Program.getCollege(egresado.IDEscuela),
                program = Program.get(egresado.IDEscuela),
            };
            var signs = acSignatory.listByACDocument(documentID, academicProfile.college.id);
            var genderData = Student.getGenderData(student.id);
            // Data para el PDF
            var data = new ProofOfGraduationPDF.Data
            {
                name_file = $"{request.id}-{request.document_number}-{Student.getDNI(student.id)}",
                user = user,
                signs = acSignatory.listPDFSign(signs),
                student_id = student.id,
                student_names = (student.last_name + " " + student.first_name).ToUpper(),
                student_dni = Student.getDNI(student.id),
                student_gender = string.Empty,
                college_name = academicProfile.college.name,
                program_name = academicProfile.program.name,
                request_id = request.id,
                document_number = request.document_number,
                study_mode = _admission.STUDY_MODE[egresado.ModalidadEgreso],
                graduation_date = egresado.FechaEgresoUC ?? DateTime.Now,
                credits_o = egresado.CreditosO,
                credits_e = egresado.CreditosE,
                gender_civil_status = genderData["civil_status"],
                gender_concerned = genderData["concerned"],
                gender_indicated = genderData["indicated"],
                gender_graduated = genderData["graduated"],
                gender_identified = genderData["identified"],
                photo_bytes = new Person().getPhoto(student.person_id)
            };

            string outputPath = ProofOfGraduationPDF.Generate(data);
            // Actualizar solicitud
            if (outputPath != null)
            {
                string message = $"Solicitud generada el {DateTime.Now}, por el usuario: {user}";
                await request.goDeliveryPending(message, user);
            }
            return outputPath;
        }
        #endregion Metodos
    }
}
