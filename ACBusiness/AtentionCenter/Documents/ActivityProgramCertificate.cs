﻿using ACBusiness.Academic;
using ACPermanence.Contexts.NEXO;
using ACTools.PDF;
using ACTools.PDF.Documents;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using _ate = ACTools.Constants.AtentionCenterConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    public class ActivityProgramCertificate : ACDocument
    {
        #region Properties
        private static int IdDocument = _ate.DOC_UNIVERSITY_LIFE_CERTIFICATE;
        private const bool Approved = true;
        #endregion

        #region Constructors
        public ActivityProgramCertificate()
        {
            this.is_valid = true;
        }
        public ActivityProgramCertificate(AcademicProfile profile)
        {
            this.is_valid = true;
            base.sync(IdDocument, profile);
        }
        #endregion

        #region methods   
        /// <summary>
        /// Valida que el estudiante haya aprobado la actividad que se le envie
        /// </summary>
        /// <param name="idProgrammingActivity">Id de la programación de la actividad</param>
        /// <param name="pidm">Pidm del estudiante</param>
        /// <returns></returns>
        public async Task<bool> Verify(int idProgrammingActivity, decimal pidm)
        {
            this.is_valid = true;

            using (var nexoContext = new NEXOContext())
            {
                var programming = await nexoContext.vuc_programming_activity.FirstOrDefaultAsync(act => act.Id == idProgrammingActivity);
                var activity = await nexoContext.vuc_activities.FirstOrDefaultAsync(act => act.Id == programming.IdActivity);

                if (activity?.IdActivityType == 1)
                {
                    var extracurricular = await nexoContext.vuc_tracing_program
                                            .Where(tracing => tracing.IdProgrammingActivity == idProgrammingActivity
                                                && tracing.PidmStudent == pidm && tracing.Active)
                                            .FirstOrDefaultAsync();

                    if (extracurricular?.Score < 14)
                    {
                        stack_error.Add("El estudiante no aprobó la actividad");
                        this.is_valid = false;
                    }
                }
            }
            return this.is_valid;
        }

        /// <summary>
        /// Genera el objeto solicitud
        /// * Peculiaridades con respecto a otras solicitudes
        ///   - Necesita comprobar que el alumno este estudiando actualmente
        /// </summary>
        /// <param name="student">Estudiante</param>
        /// <param name="profile">Profile del estudiante</param>
        /// <param name="idProgramActivity">Id de la programación de la actividad</param>
        /// <param name="description">Descripción del request</param>
        /// <param name="user">Usuario del backoffice</param>
        /// <returns></returns>
        public async Task<ACRequest> CreateRequest(Student student, AcademicProfile profile, int idProgramActivity,
            string description, string user)
        {
            var isValid = await Verify(idProgramActivity, student.person_id);

            if (!isValid)
            {
                return null;
            }

            ACRequest request = await base.composeRequest(student, profile, this.is_valid, false);
            string json = JsonConvert.SerializeObject(new
            {
                description = description,
                program = profile.program.id
            });

            request.document_id = IdDocument;
            request.description = json;
            request.aproved = Approved;
            if (request.state == _ate.STATE_INITIAL)
                request.state = _ate.STATE_FINAL;
            int i = await request.create(user);
            return i == 1 ? request : null;
        }

        /// <summary>
        /// Generar el archivo pdf correspondiente a la solicitud
        /// </summary>
        /// <param name="request">Request generado</param>
        /// <param name="student">Estudiante</param>
        /// <param name="user">Usuario</param>
        /// <param name="activityName">Nombre de la actividad</param>
        /// <param name="activityTerm">Periodo de la actividad</param>
        /// <param name="activityType">Id del tipo de la actividad</param>
        /// <param name="credits">Créditos de la actividad</param>
        /// <returns></returns>
        public string Generate(ACRequest request, Student student, string user, string activityName, 
            string activityTerm, int activityType, byte credits)
        {
            ACSignatory signatoryObj = new ACSignatory();
            List<ACSignatory> signs = signatoryObj.listByACDocument(IdDocument, student.profiles.LastOrDefault()?.college.id);

            string outputPath = null;
            PDFData data = new PDFData
            {
                name_file = $"{request.id}-{student.id}",
                user = user,
                student_id = student.id,
                student_names = student.full_name,
                student_gender = student.gender,
                college_name = profile.college.name,
                program_name = profile.program.name,
                request_id = request.id,
                document_number = request.document_number,
                signs = signatoryObj.listPDFSign(signs)
            };

            ActivityProgramPDF.Data activityData = new ActivityProgramPDF.Data(data);
            activityData.NameActivity = activityName;
            activityData.Term = activityTerm;
            activityData.ActivityType = activityType;
            activityData.Credit = credits;

            outputPath = ActivityProgramPDF.Generate(activityData, activityType);
            return outputPath;
        }
        #endregion methods
    }
}
