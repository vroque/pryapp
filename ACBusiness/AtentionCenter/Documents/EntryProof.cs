﻿using ACBusiness.Academic;
using ACBusiness.Admision;
using ACPermanence.Contexts.NEXO;
using ACTools.PDF.Documents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _inst = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    /// <summary>
    /// Constancia de Ingreso
    /// </summary>
    public class EntryProof : ACDocument
    {
        #region properties

        private const int document_id = _ate.DOC_ENTRY_PROOF;
        private bool first_request { get; set; }

        #endregion properties

        #region constructors

        public EntryProof(AcademicProfile profile)
        {
            base.sync(document_id, profile);
            this.isFirst();
        }

        #endregion constructors

        #region methods

        /// <summary>
        /// Comprueba que sea la primera solicitud
        /// </summary>
        /// <returns></returns>
        private bool isFirst()
        {
            var nexoContext = new NEXOContext();
            first_request = false;
            int n = nexoContext.cau_requests.Count(f =>
                f.documentID == this.id && f.pidm == this.person_id && f.requestStateID != 0);
            
            first_request = n == 0;

            return first_request;
        }

        /// <summary>
        /// Sobreescribe el metodo de validacion
        /// ejecuta la validacion del padre y luego
        /// ejecuta la validacion de este documetno
        /// </summary>
        /// <returns></returns>
        public new async Task<bool> validate()
        {
            bool valid = true;
            //valida si el estudiante es ingresante o mas
            if (this.profile.status <= _acad.STATE_POSTULANT)
            {
                valid = false;
                stack_error.Add("NO ESTA MATRICULADO.");
            }

            this.is_valid = valid;
            return valid;
        }

        /// <summary>
        /// Genera el objeto solicitud
        /// * Peculiaridades con respecto a otras solicitudes
        ///   - Necesita aprovacion por RA asi que approved inicia en false
        /// </summary>
        /// <param name="student"></param>
        /// <param name="profile"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public async Task<ACRequest> createRequest(
            Student student, AcademicProfile profile, string description, string user)
        {
            string json = JsonConvert.SerializeObject(new {description = description});
            //valida matricula
            bool valid = await this.validate();
            if (!valid) return null;
            //implementar su ID y su DescSolicitud
            ACRequest request = await base.composeRequest(student, profile);
            //aumenta propias de esta solicitud
            request.document_id = document_id;
            request.cost = (this.first_request) ? 0 : cost.amount;
            request.description = json; // JSON
            bool base_validation = await base.validate();
            if (request.state != _ate.STATE_CANCEL && this.first_request)
                request.state = _ate.STATE_ATENTION; // EN CENTRO DE ATENCION (no paga)
            else if (request.state != _ate.STATE_CANCEL)
                request.state = _ate.STATE_PAYMENT_PENDING;
            int i = await request.create(user);
            return i == 1 ? request : null;
        }

        /// <summary>
        /// Generar el archivo pdf correspondiente a las solicitud
        /// </summary>
        /// <param name="request">obj Solicitud</param>
        /// <returns>string URL Path del documento</returns>
        public async Task<string> generate(ACRequest request, Student student, string user)
        {
            ProofOfEntryPDF pdf = new ProofOfEntryPDF();
            ACSignatory signatory_obj = new ACSignatory();
            ACTerm term_obj = new ACTerm();
            ProofOfEntryPDF.Data data = new ProofOfEntryPDF.Data();
            List<ACSignatory> signs = signatory_obj.listByACDocument(document_id, student.profile.college.id);

            data.name_file = $"{request.id}-{request.document_number}-{student.id}";
            data.user = user;
            data.signs = signatory_obj.listPDFSign(signs);
            data.student_id = student.id;
            data.student_names = student.full_name;
            data.student_gender = student.gender;
            data.college_name = profile.college.name;
            data.program_name = profile.program.name;
            data.request_id = request.id;
            data.document_number = request.document_number;
            //get postulat period

            // Adminsion
            Postulation postulation = new Postulation().get(student.profile);
            if (postulation == null) throw new Exception("No se encuentra matrícula");
            data.adm_term = ACTerm.cleanForDocuments(postulation.term);

            data.adm_date = postulation.date;
            AdmisionType adm_type = profile.adm_type;

            data.adm_type_name = adm_type.name;

            data.study_mode = _inst.DEPARTMENT_DOCS_NAMES[postulation.department];
            data.date_of_admission =
                $"{postulation.date:dd} de {postulation.date.ToString("MMMM", new System.Globalization.CultureInfo("es-ES"))} de {postulation.date:yyyy}";

            string output_path = ProofOfEntryPDF.generate(data);
            // Actualizar solicitud
            if (output_path != null)
            {
                string message = $"Solicitud generada el {DateTime.Now}, por el usuario: {user}";
                await request.goDeliveryPending(message, user);
            }

            return output_path;
        }

        #endregion methods
    }
}