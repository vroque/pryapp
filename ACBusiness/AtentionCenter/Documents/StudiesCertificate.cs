﻿using ACBusiness.Academic;
using ACBusiness.Accounts;
using ACBusiness.Personal;
using ACTools.PDF.Documents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;
using _ate = ACTools.Constants.AtentionCenterConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    public class StudiesCertificate : ACDocument
    {
        #region properties
        private const int document_id = 1;
        public const int promocional_id = 23;
        protected const bool aproved = false;
        public bool is_graduate { get; set; }
        public string type { get; set; }
        public List<string> period_list { get; set; }
        public DocumentCost promocional_cost { get; set; }

        #endregion properties

        #region constructors
        public StudiesCertificate(AcademicProfile profile)
        {
            base.sync(document_id, profile);
            // obtener lista de periodos
            this.syncTakenPeriods();
            this.promocional_cost = DocumentCost.Get(promocional_id, base.profile);
            if (profile.status == _acad.STATE_GRADUATE)
                this.is_graduate = true;
        }
        #endregion constructors

        #region methods

        /// <summary>
        /// Sobreescribe el metodo de validacion
        /// ejecuta la validacion del padre y luego
        /// ejecuta la validacion de este documetno
        /// </summary>
        /// <returns></returns>
        public new async Task<bool> validate(){
            bool valid = true;
            //valida si el estudiante es matriculado o mas
            if (this.profile.status < _acad.STATE_STUDENT ) 
            {
                valid = false;
                stack_error.Add("No registra matrícula.");
            }
            this.is_valid = valid; 
            return valid;
        }

        /// <summary>
        /// llena la lista de periodos en los cual el 
        /// estudiante registra una matricula
        /// </summary>
        private void syncTakenPeriods() 
        {
            Enrollment enr_obj = new Enrollment();
            this.period_list = enr_obj.getAprovedPeriods(this.profile);
        }

        /// <summary>
        /// sobreescribe el metodo que crea
        /// la solicitud dandole paramentos
        /// especificos para este documento
        /// </summary>
        /// <param name="student">objecto estudiante</param>
        /// <param name="profile">objeto perfil</param>
        /// <param name="description">cadena de descripción</param>
        /// <returns></returns>
        public async Task<ACRequest> createRequest(
            Student student, AcademicProfile profile, List<string> period_list, string description, string user)
        {
            string json = JsonConvert.SerializeObject(
                new { description= description, period_list= period_list }
                );
            bool valid = await this.validate();
            if (!valid) return null;
            ACRequest request = await base.composeRequest(student, profile);
            //aumenta propias de esta solicitud
            request.document_id = document_id;
            int cant = period_list.Count();
            if (cant < 1) throw new Exception("Seleccionar por lo menos un periodo");
            request.cost = cost.amount * cant;
            request.aproved = aproved;
            request.description = json; // JSON
            if (request.state != _ate.STATE_CANCEL)
                request.state = _ate.STATE_PAYMENT_PENDING; // pendiente de pago
            //request.concept_id = amount.id; //FALTA IMPLEMENTAR
            int i = await request.create(user);
            if(i == 1)
                return request;
            return null;
        }
        
        /// <summary>
        /// Generar el archivo pdf correspondiente a las solicitud
        /// </summary>
        /// <param name="request">obj Solicitud</param>
        /// <returns>string URL Path del documento</returns>
        public async Task<string> generate(ACRequest request, Student student, string user)
        {
            StudiesCetificatePDF pdf = new StudiesCetificatePDF();
            ACSignatory signatory_obj = new ACSignatory();
            ACTerm term_obj = new ACTerm();
            Course course_obj = new Course();
            StudiesCetificatePDF.Data data = new StudiesCetificatePDF.Data();
            List<ACSignatory> signs = signatory_obj.listByACDocument(
                document_id, student.profile.college.id);
            // List<PDFSign> sign_list = new List<PDFSign>();

            data.name_file = string.Format(
                "{0}-{2}-{1}",
                request.id, student.id, request.document_number);
            data.user = user;
            data.signs = signatory_obj.listPDFSign(signs);
            data.student_id = student.id;
            data.student_names = student.full_name;
            data.student_gender = student.gender;
            data.college_name = profile.college.name;
            data.program_name = profile.program.name;
            data.request_id = request.id;
            data.document_number = request.document_number;
            //photo
            Person p = new Person();
            byte[] photo = p.getPhoto(profile.person_id);
            if (photo != null)
                data.photo_bytes = photo;
            else throw new Exception("el alumno no tiene foto");

            List<string> periods = new List<string>();

            // obtener los periodo que ah seleccionado JArray term_list
            JObject json_obj = JObject.Parse(request.description);
            //List<string> terms_list = json_obj["period_list"].ToList<string>();
            var period_list = json_obj["period_list"];
            foreach(var item in period_list)
            {
                periods.Add(item.ToString());
            }

            // obtener los cursos
            List<Course> courses = course_obj.listByStudentPeriods(profile, periods)
                .OrderBy(f => f.name).ToList(); ;
            //filtrar por estos peridos

            foreach (Course course in courses)
            {
                StudiesCetificatePDF.CourseDetail detail = new StudiesCetificatePDF.CourseDetail();
                detail.cycle = course.cycle;
                detail.type = course.type;
                detail.id = course.subject + course.id;
                detail.name = course.name;
                detail.university = "";
                detail.country = "";
                detail.resolution = "";
                detail.observation = (course.nrc == "CONV")?"Convalidado": "";
                detail.score =int.Parse(course.score);
                detail.credits = course.credits;
                detail.date = course.date;
                detail.is_aproved = course.is_aproved;
                data.courses.Add(detail);
            }
            string output_path = StudiesCetificatePDF.generate(data);
            /// Actualizar solicitud
            if (output_path != null)
            {
                string message = string.Format(
                    "Solicitud generada el {0}, por el usuario: {1}"
                    , DateTime.Now, user);
                await request.goDeliveryPending(message, user);
            }
            return output_path;
        }
        #endregion methods
    }
}
