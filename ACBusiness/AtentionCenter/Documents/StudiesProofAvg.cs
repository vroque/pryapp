﻿using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACTools.PDF.Documents;
using ACTools.SuperString;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _ate = ACTools.Constants.AtentionCenterConstants;

namespace CABusiness.AtentionCenter.Documents
{
    /// <summary>
    /// Constancia de estudios
    /// </summary>
    public class StudiesProofAvg: ACDocument
    {
        #region properties
        private int document_id = _ate.DOC_AVERAGE_STUDIES_PROOF;
        private const bool aproved = true;        
        public List<string> period_list { get; set; }
        public int type { get; set; }

        #endregion properties

        #region constructors
        public StudiesProofAvg(AcademicProfile profile)
        {
            this.is_valid = true;                 
            base.sync(document_id, profile);
        }
        #endregion constructors

        #region methods   
        //the validate() and createRequest() methods for this request dont exist
        //because we use CABusiness.AtentionCenter.Document.StudiesProof methods instead


        /// <summary>
        /// Generar el archivo pdf correspondiente a las solicitud
        /// </summary>
        /// <param name="request">obj Solicitud</param>
        /// <returns>string URL Path del documento</returns>
        public async Task<string> generate(ACRequest request, Student student, string user)
        {
            //same data for all pdfs
            StudiesProofAvgPDF pdf = new StudiesProofAvgPDF();
            ACSignatory signatory_obj = new ACSignatory();
            ACTerm term_obj = new ACTerm();
            StudiesProofAvgPDF.Data data = new StudiesProofAvgPDF.Data();
            List<ACSignatory> signs = signatory_obj.listByACDocument(
                document_id, student.profile.college.id);
            data.name_file = string.Format(
                "{0}-{2}-{1}",
                request.id, student.id, request.document_number);
            data.user = user;
            data.signs = signatory_obj.listPDFSign(signs);
            data.student_id = student.id;
            data.student_names = student.full_name;
            data.student_gender = student.gender;
            data.college_name = profile.college.name;
            data.program_name = profile.program.name;
            data.request_id = request.id;
            data.document_number = request.document_number;
            //our own data
            JObject request_description = JObject.Parse(request.description);
            AcademicTerm last_term_obj = AcademicTerm.getLastTerm(student.profile);
            int period = this.profile.cycleByTerm(last_term_obj.id);
            data.last_term = ACTerm.cleanForDocuments(last_term_obj.id);
            ACTerm term_dates = new ACTerm(data.last_term, student.profile.campus.id, student.profile.department);            
            data.last_term_end_date = string.Format(
                    "{0} de {1} de {2}",
                    term_dates.end_date.ToString("dd"),
                    term_dates.end_date.ToString("MMMM", new System.Globalization.CultureInfo("es-ES")),
                    term_dates.end_date.ToString("yyyy"));
            data.last_term_period = SuperString.NUMBER_TO_ROMAN[period];
            data.avg = student.profile.getGPA();  
            //generate
            string output_path = StudiesProofAvgPDF.generate(data);
            /// Actualizar solicitud
            if (output_path != null)
            {
                string message = string.Format(
                    "Solicitud generada el {0}, por el usuario: {1}"
                    , DateTime.Now, user);
                await request.goDeliveryPending(message, user);
            }
            return output_path;
        }

        #endregion methods
    }
}
