﻿using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACTools.PDF.Documents;
using ACTools.SuperString;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _acad = ACTools.Constants.AcademicConstants;
using Newtonsoft.Json;

namespace CABusiness.AtentionCenter.Documents
{
    /// <summary>
    /// Constancia de estudios
    /// </summary>
    public class GraduationProof: ACDocument
    {
        #region properties
        private int document_id = _ate.DOC_CULM_CAREER_STUDIES_PROOF;
        private const bool aproved = true;        
        public List<string> period_list { get; set; }
        public int type { get; set; }

        #endregion properties

        #region constructors
        public GraduationProof(AcademicProfile profile)
        {
  
            base.sync(document_id, profile);
        }
        #endregion constructors

        #region methods   
        //the validate() and createRequest() methods for this request dont exist
        //because we use CABusiness.AtentionCenter.Document.StudiesProof methods instead
        
        /// <summary>
        /// Valida si el alumno tiene un egreso con la carrera obtenida
        /// </summary>
        /// <returns></returns>
        public new async Task<bool> validate()
        {
            if (this.profile.status >= _acad.STATE_GRADUATE)
                this.is_valid = true;
            else
            {
                this.is_valid = false;
                this.stack_error.Add($"Usted no registra como egresado a la carrera de {this.profile.program.name}");
            }
            return is_valid;
        }

        /// <summary>
        /// Crea la solicitud apartir de los datos
        /// </summary>
        /// <param name="student"></param>
        /// <param name="profile"></param>
        /// <param name="description"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ACRequest> createRequest(
            Student student, AcademicProfile profile, string description, string user)
        {
            string json = JsonConvert.SerializeObject(
                   new { description = description }
                   );
            //valida matricula
            bool valid = await this.validate();
            if (!valid) return null;
            //implementar su ID y su DescSolicitud
            ACRequest request = await base.composeRequest(student, profile);
            //aumenta propias de esta solicitud
            request.document_id = document_id;
            request.cost = cost.amount;
            request.description = json; // JSON
            bool base_validation = await base.validate();
            if (request.state != _ate.STATE_CANCEL)
                request.state = _ate.STATE_PAYMENT_PENDING;
            int i = await request.create(user);
            if (i == 1)
                return request;
            return null;
        }

        /// <summary>
        /// Generar el archivo pdf correspondiente a las solicitud
        /// </summary>
        /// <param name="request">obj Solicitud</param>
        /// <returns>string URL Path del documento</returns>
        public async Task<string> generate(ACRequest request, Student student, string user)
        {
            //same data for all pdfs
            GraduationProofPDF pdf = new GraduationProofPDF();
            ACSignatory signatory_obj = new ACSignatory();
            GraduationProofPDF.Data data = new GraduationProofPDF.Data();
            

            List<ACSignatory> signs = signatory_obj.listByACDocument(
                document_id, student.profile.college.id);
            data.name_file = string.Format(
                "{0}-{2}-{1}",
                request.id, student.id, request.document_number);
            data.user = user;
            data.signs = signatory_obj.listPDFSign(signs);
            data.student_id = student.id;
            data.student_names = student.full_name;
            data.student_gender = student.gender;
            data.college_name = profile.college.name;
            data.program_name = profile.program.name;
            data.request_id = request.id;
            data.document_number = request.document_number;
            //our own data
            Graduate graduate = Graduate.get(student.profile);

            data.last_term = ACTerm.cleanForDocuments(graduate.last_semester);
            data.last_term_credits =graduate.credits_o + graduate.credits_e;
            data.last_term_period = SuperString.NUMBER_TO_ROMAN[graduate.last_period];
            //generate
            string output_path = GraduationProofPDF.generate(data);
            /// Actualizar solicitud
            if (output_path != null)
            {
                string message = string.Format(
                    "Solicitud generada el {0}, por el usuario: {1}"
                    , DateTime.Now, user);
                await request.goDeliveryPending(message, user);
            }
            return output_path;
        }

        #endregion methods
    }
}
