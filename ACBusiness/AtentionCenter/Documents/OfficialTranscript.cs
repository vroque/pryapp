﻿using ACBusiness.Academic;
using ACBusiness.Admision;
using ACBusiness.Institutional;
using ACBusiness.Personal;
using ACTools.PDF.Documents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    /// <summary>
    /// Historial Académico (A.K.A Record Académico)
    /// Acerca de "Official Transcript": 
    /// https://www.registrar.psu.edu/transcripts/transcripts.cfm
    /// http://forum.wordreference.com/threads/historial-académico-oficial.876027/?hl=es
    /// </summary>
    public class OfficialTranscript : ACDocument
    {
        #region properties
        private static int document_id = 3; // Historial Académico
        private const string NO_SCHOOL = "0";
        private const int NO_ACADEMIC_STATUS = 0;
        public List<string> period_list { get; set; }
        #endregion properties

        #region constructors

        public OfficialTranscript()
        { }
        public OfficialTranscript(AcademicProfile profile)
        {
            base.sync(document_id, profile);
            this.syncTakenPeriods();
        }
        #endregion constructors


        #region methods
        /// <summary>
        /// Sobreescribe el metodo de validacion
        /// ejecuta la validacion del padre y luego
        /// ejecuta la validacion de este documetno
        /// </summary>
        /// <returns></returns>
        public new async Task<bool> validate()
        {
            bool valid = true;
            // valida si el estudiante es matriculado o más
            if (this.profile.status < _acad.STATE_STUDENT)
            {
                valid = false;
                base.stack_error.Add("No registra matrícula.");
            }
            this.is_valid = valid;
            return valid;
        }


        /// <summary>
        /// Llena la lista de periodos en los cuales el 
        /// estudiante registra una matricula
        /// </summary>
        private void syncTakenPeriods()
        {
            Enrollment enr_obj = new Enrollment();
            this.period_list = enr_obj.getAprovedPeriods(this.profile);
        }


        /// <summary>
        /// sobreescribe el metodo que crea
        /// la solicitud dandole paramentos
        /// especificos para este documento
        /// </summary>
        /// <param name="student">objecto estudiante</param>
        /// <param name="profile">objeto perfil</param>
        /// <param name="description">cadena de descripción</param>
        /// <returns></returns>
        public async Task<ACRequest> createRequest(
            Student student, AcademicProfile profile, string description, string user)
        {
            string json = JsonConvert.SerializeObject(
                new { description = description, period_list = period_list } );
            bool valid = await this.validate();
            if (!valid)
                return null;
            ACRequest request = await base.composeRequest(student, profile);
            //aumenta propias de esta solicitud
            request.document_id = document_id;
            
            request.cost = cost.amount;
            
            request.description = json; // JSON
            if (request.state != _ate.STATE_CANCEL)
                request.state = _ate.STATE_PAYMENT_PENDING; // pendiente de pago
            // TODO: request.concept_id = amount.id; //FALTA IMPLEMENTAR
            int i = await request.create(user);
            if (i == 1)
                return request;
            return null;
        }


        /// <summary>
        /// Generar el archivo pdf correspondiente a las solicitud
        /// </summary>
        /// <param name="request">obj Solicitud</param>
        /// <param name="validateflow">VALIDAR FLUJO?</param>
        /// <returns>string URL Path del documento</returns>
        public async Task<string> generate(ACRequest request, Student student, string user, bool validateflow)
        {
            OficialTranscriptPDF pdf = new OficialTranscriptPDF();
            ACSignatory signatory_obj = new ACSignatory();
            ACTerm term_obj = new ACTerm();
            Course course_obj = new Course();
            OficialTranscriptPDF.Data data = new OficialTranscriptPDF.Data();
            List<ACSignatory> signatories = signatory_obj.listByACDocument(document_id, student.profile.college.id);
            Person p = new Person();
            decimal person_id = p.getPersonIDfromUser(user);
            //signatories.Add(signatory_obj.listByACDocument(
            //    document_id, student.profile.college.id)
            //        .FirstOrDefault(f => f.person_id == person_id)
            //);

            data.name_file = string.Format(
                "{0}-{2}-{1}",
                request.id, student.id, request.document_number);
            data.user = user;
            data.signs = signatory_obj.listPDFSign(signatories);
            data.student_id = student.id;
            data.student_names = student.full_name;
            data.student_gender = student.gender;
            data.college_name = profile.college.name;
            data.program_name = profile.program.name;
            data.request_id = request.id;
            data.document_number = request.document_number;

            List<Course> courses = course_obj.listAllByStudent(profile)
                .OrderBy( f=> f.date)
                .ThenBy(f => f.name)
                .ToList();
            foreach (Course course in courses)
            {
                OficialTranscriptPDF.CourseDetail detail = new OficialTranscriptPDF.CourseDetail();
                detail.cycle = course.cycle;
                detail.type = course.type;
                detail.id = course.subject + course.id; ;
                detail.name = course.name;
                detail.observation = (course.nrc == "CONV")? "Asignatura convalidada" : "";
                // detail.condition = "";
                detail.state = (course.is_aproved)? "Aprobado" : "Desaprobado";
                // detail.section = course.subject + course.nrc;
                detail.term = course.term;
                detail.score = int.Parse(course.score);
                detail.credits = course.credits;
                detail.is_active = course.is_active; // designa si la asignatura es activa o no
                // detail.date = course.date;
                detail.is_aproved = course.is_aproved;
                data.courses.Add(detail);
            }
            string output_path = OficialTranscriptPDF.generate(data);
            /// Actualizar solicitud
            if (output_path != null)
            {
                string message = string.Format(
                    "Solicitud generada el {0}, por el usuario: {1}"
                    , DateTime.Now, user);
                if (validateflow) await request.goDeliveryPending(message, user);
            }
            return output_path;
        }

        /// <summary>
        /// Generar solicitud sin deuda
        /// </summary>
        /// <param name="student">Estudiante</param>
        /// <param name="profile">Perfil</param>
        /// <param name="user">Usuario que crea</param>
        /// <returns></returns>
        public async Task<ACRequest> createRequestBatch(Student student, AcademicProfile profile, string user)
        {
            Enrollment enr_obj = new Enrollment();
            List<string> periodListStudent = enr_obj.getAprovedPeriods(profile);
            if (periodListStudent == null || periodListStudent.Count <= 0) throw new Exception("No tiene periodos aprobados");

            string json = JsonConvert.SerializeObject(new { description = $"Solicitud por Batch: {user}", period_list = periodListStudent });
            ACRequest request = await base.composeRequest(student, profile, true, false);
            request.document_id = document_id;
            request.description = json;
            request.state = _ate.STATE_FINAL;
            int save = await request.createCustomRequest(user, false, _ate.STATE_FINAL);
            if (save <= 0)
            {
                throw new Exception("Error al guardar la solicitud.");
            }
            return save == 1 ? request : null;
        }

        /// <summary>
        /// Generar Documento en batch
        /// </summary>
        /// <param name="student">Estudiante</param>
        /// <param name="user">Usuario que crea</param>
        /// <returns></returns>
        public async Task<string> GetDocumentPathBatch(Graduated student, string user)
        {
            var studentDepartment = Department.toBanner(student.modality_id);
            var college = Program.getCollege(student.school_id);
            var estudiante = new Student(student.student_id);
            var escuelaList = new List<string> { student.school_id };
            if (student.school_id == _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA) escuelaList.Add(_corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA_OLD);
            
            /*Obtener el ultimo movimiento*/
            var movEstudiante = Student.GetLastMovimientoEstudianteBySchoolId(Student.getPidmFromStudentId(student.student_id), student.school_id);
            if (movEstudiante == null)
            {
                throw new NullReferenceException(
                   $"No se encontró registro de postulante para {student.student_id} en {student.school_id}");
            }

            AcademicProfile academicProfile = new AcademicProfile
            {
                person_id = Student.getPidmFromStudentId(student.student_id),
                div = _acad.DEFAULT_DIV,
                campus = Campus.get(movEstudiante.CODIGO_CAMPUS),
                department = Department.toBanner(student.modality_id),
                subject = movEstudiante.CATALOGO_REAL,
                college = Program.getCollege(student.school_id),
                program = Program.get(student.school_id),
                adm_type = AdmisionType.get(Entrant.GetCurrentModalityPostulationBySchoolIdAndDepartment(Student.getPidmFromStudentId(student.student_id), student.school_id, Department.toBanner(student.modality_id)), null, "banner")
            };

            ACRequest acRequest = await createRequestBatch(estudiante, academicProfile, user);
            ACDocument doc_object = new ACDocument();

            string docPath = await doc_object.generateDocument(acRequest.id, user, "", 1, false);
            return docPath;
        }

        /// <summary>
        /// Iniciar creacion de solicitud y documento
        /// </summary>
        /// <param name="student">Estudiante</param>
        /// <param name="user">Usuario que crea</param>
        /// <returns></returns>
        public async Task<Graduated> GenerateBatch(Graduated student, string user)
        {
            if (student.school_id == NO_SCHOOL || !student.is_selected ||
                    student.academic_status == NO_ACADEMIC_STATUS || !student.Processing)
            {
                throw new Exception("No se puede generar el documento.");
            }

            var estudiante = new Student(student.student_id);
            var perfil = new AcademicProfile
            {
                person_id = Student.getPidmFromStudentId(student.student_id),
                department = Department.toBanner(student.modality_id),
                program = Program.get(student.school_id)
            };

            string dictPath = await GetDocumentPathBatch(student, user);
            student.doc_url = dictPath;
            if (!string.IsNullOrEmpty(student.doc_url))
            {
                student.download = true;
                student.Processing = false;
            }

            if (string.IsNullOrEmpty(student.doc_url))
            {
                student.ProcessingError = true;
            }

            student.Processing = false;
            return student;
        }



        #endregion methods
    }
}
