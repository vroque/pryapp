﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Admision;
using ACBusiness.AtentionCenter;
using ACBusiness.Institutional;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACTools.PDF;
using ACTools.PDF.Documents;
using Newtonsoft.Json;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _admission = ACTools.Constants.AdmissionConstant;

namespace ACBusiness.Academic
{
    /// <summary>
    /// Graduados UC
    /// </summary>
    public class ProofOfGraduation : ACDocument
    {
        #region Properties

        public string student_id { get; set; }
        public string student_name { get; set; }
        public string graduation_period { get; set; }
        public string school_id { get; set; }
        public string school_name { get; set; }
        public string modality_id { get; set; }
        public string modality_name { get; set; }
        public int credits_o { get; set; }
        public int credits_e { get; set; }
        public string volunteering { get; set; }
        public string foreign_language { get; set; }
        public string internship { get; set; }
        public DateTime? graduation_date { get; set; }
        public bool is_selected { get; set; }
        public int payment_status { get; set; }
        public string doc_url { get; set; }
        public bool download { get; set; }
        public string save_as { get; set; }
        public bool Processing { get; set; }
        public bool ProcessingError { get; set; }

        private const int DOCUMENT_ID = _ate.DOC_GRADUATION_PROOF;
        private const int REQUEST_FINALIZED = _ate.STATE_FINAL;
        private const string BACHILLER_CONCEPT_ID = _ate.BACHILLER_CONCEPT_ID;
        private const string NO_SCHOOL = "0";

        #endregion Properties

        #region Implicit Operators

        #endregion Implicit Operators

        #region Methods

        /// <summary>
        /// Genera Lista de Estudiantes con Constancia de Egresado listos para descargar/imprimir
        /// </summary>
        /// <param name="students">Estudiantes para los que se procesará el documento</param>
        /// <param name="date">Fecha de egreso UC</param>
        /// <param name="user">Usuario que genera el documento</param>
        /// <returns>Listra de Estudiantes con la ruta de la constancia de egresado</returns>
        public static async Task<List<ProofOfGraduation>> GetProofOfGraduation(ProofOfGraduation[] students,
            DateTime date, string user)
        {
            var dboContext = new DBOContext();

            var graduates = new List<ProofOfGraduation>();
            var noGraduates = new List<ProofOfGraduation>();

            // Separamos únicamente egresados seleccionados que hayan cumplido los requisitos de
            // (proyección, idioma extranjero y prácticas pre-profesionales) del resto
            foreach (var student in students)
            {
                // NOTE: Deshabilitamos temporalmente la verificación si se cumplió con los requisitos de
                // proyección social, idioma extranjero, y prácticas pre profesionales
                // TODO: Incluir validación de pago por trámite de bachiller cuando se vuelva a activar las otras validaciones
                if (student.school_id != NO_SCHOOL && student.is_selected /*&& student.volunteering == OK &&
                    student.foreign_language == OK && student.internship == OK*/ && student.Processing)
                {
                    // Si no tiene fecha de egresado insertamos la fecha 'date'
                    if (student.graduation_date == null)
                    {
                        // Registramos de la fecha en la tabla de egresados
                        var egresado = await dboContext.tbl_alumno_egresado
                            .FirstOrDefaultAsync(w => w.IDAlumno == student.student_id &&
                                                      w.IDEscuela == student.school_id);
                        egresado.FechaEgresoUC = date;
                        student.graduation_date = date;
                        var saved = await dboContext.SaveChangesAsync();
                        if (saved == 0)
                        {
                            throw new Exception("No se pudo actualizar la fecha de egresado UC");
                        }
                    }

                    graduates.Add(student);
                } /*else if (student.school_id != NO_SCHOOL && student.is_selected && (student.volunteering != OK ||
                           student.foreign_language != OK || student.internship != OK))
                {
                    // No cumple requisito proyección, idioma extranjero y prácticas pre-profesionales
                    student.payment_status = 3;  // Para mostrar etiqueta de que le falta requisitos
                    no_graduates.Add(student);
                }*/
                else
                {
                    noGraduates.Add(student);
                }
            }

            // Generación de Constancia de Egresado
            foreach (var graduated in graduates)
            {
                var dictPath = await GetProofOfGraduationPath(graduated, user);
                graduated.doc_url = dictPath["uri"];
                graduated.save_as = dictPath["save_as"];

                if (!string.IsNullOrEmpty(graduated.doc_url))
                {
                    graduated.download = true;
                }

                if (string.IsNullOrEmpty(graduated.doc_url))
                {
                    graduated.ProcessingError = true;
                }

                graduated.Processing = false;
            }

            return graduates.Union(noGraduates).OrderBy(o => o.student_name).ToList();
        }

        /// <summary>
        /// Genera la URL para descargar de la Constancia de Egresado
        /// </summary>
        /// <param name="student">Datos del estudiante</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        private static async Task<Dictionary<string, string>> GetProofOfGraduationPath(ProofOfGraduation student,
            string user)
        {
            var dboContext = new DBOContext();
            var nexoContext = new NEXOContext();
            var college = Program.getCollege(student.school_id);

            var acSignatory = new ACSignatory();
            var signs = acSignatory.listByACDocument(DOCUMENT_ID, college.id);

            var estudiante = new Student(student.student_id);

            // Extraemos más info sobre el egresado
            var moreDataForGraduated = dboContext.tbl_alumno_egresado.FirstOrDefault(w =>
                w.IDAlumno == student.student_id && w.IDEscuela == student.school_id);

            if (moreDataForGraduated == null)
            {
                throw new NullReferenceException(
                    $"No existe registro de {student.student_id} como graduado en escuela {student.school_id}");
            }

            var movEstudiante = Student.GetLastMovimientoEstudianteBySchoolId(Student.getPidmFromStudentId(student.student_id), student.school_id);

            var campus = (movEstudiante != null)
                ? Campus.get(movEstudiante.CODIGO_CAMPUS)
                : Campus.getByAPECCode(moreDataForGraduated.IDSede);

            if (movEstudiante == null)
            {
                throw new NullReferenceException(
                    $"No se encontró movimientos de estudiante {student.student_id} en escuela {student.school_id}");
            }

            var academicProfile = new AcademicProfile
            {
                person_id = Student.getPidmFromStudentId(student.student_id),
                div = moreDataForGraduated.IDDependencia,
                campus = campus,
                department = Department.toBanner(student.modality_id),
                subject = moreDataForGraduated.IDPlanEstudio,
                college = Program.getCollege(student.school_id),
                program = Program.get(student.school_id),
                adm_type = AdmisionType.get(movEstudiante.CODIGO_MOD_ADM, null)
            };

            /**
             * Generación de Solicitud para guardar evidencia de generación del documento
             */
            ACRequest acRequest;

            // Se busca si existe solicitud asociada a la creación de constancia de egresado
            var requestExists = nexoContext.cau_requests.Where(w => w.documentID == DOCUMENT_ID &&
                                                                    w.conceptID == BACHILLER_CONCEPT_ID &&
                                                                    w.pidm == estudiante.person_id)
                .OrderByDescending(o => o.requestID).FirstOrDefault();
            // Si existe solicitud asociada, utilizamos los datos de éste para la generación de la constancia
            if (requestExists != null)
            {
                acRequest = await ACRequest.get(requestExists.requestID);
            }
            // sino, generamos una nueva solicitud
            else
            {
                acRequest = await student.CreateRequestForGraduationProcess(estudiante, academicProfile, user);
                if (ACRequest.isNull(acRequest))
                {
                    return null;
                }
            }

            // Data para el PDF
            var data = new ProofOfGraduationPDF.Data
            {
                name_file = $"{acRequest.id}-{acRequest.document_number}-{Student.getDNI(student.student_id)}",
                user = user,
                signs = acSignatory.listPDFSign(signs),
                student_id = student.student_id,
                student_names = student.student_name,
                student_dni = Student.getDNI(student.student_id),
                student_gender = string.Empty,
                college_name = college.name,
                program_name = student.school_name,
                request_id = acRequest.id,
                document_number = acRequest.document_number,
                study_mode = _admission.STUDY_MODE[student.modality_id],
                graduation_date = student.graduation_date ?? DateTime.Now,
                credits_o = student.credits_o,
                credits_e = student.credits_e,
                gender_civil_status = Student.getGenderData(student.student_id)["civil_status"],
                gender_concerned = Student.getGenderData(student.student_id)["concerned"],
                gender_indicated = Student.getGenderData(student.student_id)["indicated"],
                gender_graduated = Student.getGenderData(student.student_id)["graduated"],
                gender_identified = Student.getGenderData(student.student_id)["identified"],
                photo_bytes = new Person().getPhoto(Student.getPidmFromStudentId(student.student_id))
            };

            return PDFActions.exists(data.name_file)
                ? new Dictionary<string, string>
                {
                    {"uri", PDFActions.getRepoDocumentPath(data.name_file)},
                    {"save_as", data.name_file}
                }
                : new Dictionary<string, string>
                {
                    {"uri", ProofOfGraduationPDF.Generate(data)},
                    {"save_as", data.name_file}
                };
        }

        /// <summary>
        /// <para>
        /// Creación de solicitud para generar constancias relacionadas al proceso de trámite de bachiller.
        /// </para>
        /// Esta solicitud y constancia estan relacionados unicamente al proceso de trámite de bachiller,
        /// se crea con costo = 0 y el estado de solicitud = "finalizado"
        /// </summary>
        /// <param name="student">Datos del Estudiante</param>
        /// <param name="academicProfile">Perfil académico del estudiante</param>
        /// <param name="user">Usuario que genera la solicitud</param>
        /// <returns>Solicitud de creación de constancia de ingreso asociada al proceso de trámite de bachiller</returns>
        private async Task<ACRequest> CreateRequestForGraduationProcess(Student student,
            AcademicProfile academicProfile, string user)
        {
            var json = JsonConvert.SerializeObject(new
            {
                description = "Solicitud generada como parte del proceso de trámite por bachiller"
            });

            var acRequest = await composeRequest(student, academicProfile, true, false);

            // Datos de la solicitud
            acRequest.document_id = DOCUMENT_ID;
            acRequest.description = json;
            acRequest.state = REQUEST_FINALIZED;
            acRequest.concept_id = BACHILLER_CONCEPT_ID;

            var i = await acRequest.createCustomRequest(user, false, REQUEST_FINALIZED, BACHILLER_CONCEPT_ID);

            return i == 1 ? acRequest : null;
        }

        #endregion Methods
    }
}