﻿using ACBusiness.Academic;
using ACBusiness.Accounts;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using _ate = ACTools.Constants.AtentionCenterConstants;


namespace ACBusiness.AtentionCenter.Documents
{
    public class  StudentCardDuplicate : ACDocument
    {
        #region properties
        private static int document_id = 14;
        public bool has_photo {get;set; }
        #endregion properties

        #region constructors
        public StudentCardDuplicate(AcademicProfile profile)
        {
            base.sync(document_id, profile);
            this.hasPhoto();
        }
        #endregion constructors

        #region methods

        public new async Task<bool> validate()
        {
            //validar deuda
            this.is_valid = true;
            stack_error = new List<string>();
            Debt debt_object = new Debt();
            var debts = await debt_object.query(profile.person_id);
          

            if (debts.Count() > 0)
            {
                foreach (Debt debt in debts)
                {
                    stack_error.Add(string.Format("Deuda: {0} [S/. {1}]",
                        debt.description,
                        debt.ammount
                        ));
                }
            }
            if (debts.Count > 0 )
            {
                this.is_valid = false;
            }

            //validar foto
            bool valid = true;
            if (!this.has_photo) {
                valid = false;
                stack_error.Add("No tiene foto.");
            }
            this.is_valid = this.is_valid && valid;
            return this.is_valid;
        }

        public async Task<ACRequest> createRequest(
            Student student, string description, string user)
        {
            string json = JsonConvert.SerializeObject(
                new { description = description }
                );
            bool valid = await this.validate();
            //if (!valid) return null;
            ACRequest request = await base.composeRequest(student, profile);
           
            request.document_id = document_id;
            request.cost = cost.amount;
            request.description = json; 
            if (request.state != _ate.STATE_CANCEL)
                request.state = _ate.STATE_PAYMENT_PENDING; 
            int i = await request.create(user);
            if (i == 1)
                return request;
            return null;
        }


        public void hasPhoto() {
            DBOContext db = new DBOContext();
            DBOTblPersona person = db.tbl_persona.FirstOrDefault(p => p.IDPersonaN == person_id);
            if (person.Foto == null && person.FotoRuta != null) {
                Byte[] imageData = null;
                try
                {
                    string photo_route = person.FotoRuta;
                    FileInfo fileInfo = new FileInfo(photo_route);
                    long imageFileLength = fileInfo.Length;
                    FileStream fs = new FileStream(photo_route, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    imageData = br.ReadBytes((int)imageFileLength);

                }
                catch (System.IO.FileNotFoundException error) { }
                finally { }
                person.Foto=imageData;
                db.SaveChanges();
            }
            DBOTblPersona person2 = db.tbl_persona.FirstOrDefault(p => p.IDPersonaN == person_id);           
             this.has_photo = person2.Foto==null ? false: true;
        }



        #endregion methods

    }
}
