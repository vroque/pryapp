﻿using ACBusiness.Academic;
using ACBusiness.Admision;
using ACBusiness.Institutional;
using ACTools.PDF.Documents;
using ACTools.SuperString;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    /// <summary>
    /// Constancia de 3ro, 5to y 10mo Superior Promocional
    /// </summary>
    public class ProofOf3510Promotional : ACDocument
    {
        #region properties
        private static int document_id = 22; // Constancia de 3ro, 5to y 10mo Superior Promocional
        private const string NO_SCHOOL = "0";
        /// <summary>
        /// Sin info sobre estado académico (matriculado, estudiante, no egresado, egresado, etc)
        /// </summary>
        private const int NO_ACADEMIC_STATUS = 0;
        public ThirdFifthTenth promo { get; set; }
        public bool has_3510 { get; set; }
        #endregion properties

        #region constructors
        public ProofOf3510Promotional()
        { }
        public ProofOf3510Promotional(AcademicProfile profile)
        {
            base.sync(document_id, profile);
            this.syncTakenPeriods();
            this.sync3510();
                
        }

        #endregion constructors


        #region methods
        /// <summary>
        /// Sobreescribe el metodo de validacion
        /// ejecuta la validacion del padre y luego
        /// ejecuta la validacion de este documento
        /// </summary>
        /// <returns></returns>
        public new async Task<bool> validate()
        {
            bool valid = true;
            // Valida si el estudiante es matriculado o más
            if (this.profile.status < _acad.STATE_STUDENT)
            {
                valid = false;
                base.stack_error.Add("No registra matrícula.");
            }
            this.is_valid = valid;
            return valid;
        }


        /// <summary>
        /// Verifica si un estudiante tiene algun puesto en 3, 5 y 10 superior promocional
        /// </summary>
        private void sync3510()
        {
            this.promo = new ThirdFifthTenth().getPromotional(this.profile);
            if (this.promo == null)
                this.has_3510 = false;
            else this.has_3510 = true;
        }


        /// <summary>
        /// Llena la lista de periodos en los cuales el 
        /// estudiante obtuvo 3ro, 5to o 10mo superior
        /// </summary>
        private void syncTakenPeriods()
        {
            // TODO: Reemplazar codigo en duro por la variable de pidm cuando se tenga datos en DB de integración
            //this.period_list = ThirdFifthTenth.getPromotional(461004/*Decimal.ToInt32(person_id)*/);
        }

        
        /// <summary>
        /// Sobreescribe el método que crea
        /// la solicitud dándole parámentos
        /// específicos para este documento
        /// </summary>
        /// <param name="student">objecto estudiante</param>
        /// <param name="profile">objeto perfil</param>
        /// <param name="description">cadena de descripción</param>
        /// <returns></returns>
        public async Task<ACRequest> createRequest(
            Student student, AcademicProfile profile,  string description, string user)
        {
            string json = JsonConvert.SerializeObject(new { description = description });
            bool valid = await this.validate();
            if (!valid)
            {
                return null;
            }
            ACRequest request = await base.composeRequest(student, profile);
            // Aumenta propias de esta solicitud
            request.document_id = document_id;

            request.cost = cost.amount;

            request.description = json; // JSON
            if (request.state != _ate.STATE_CANCEL)
            {
                request.state = _ate.STATE_PAYMENT_PENDING; // Pendiente de pago
            }
            // TODO: request.concept_id = amount.id; //FALTA IMPLEMENTAR
            int i = await request.create(user);
            if (i == 1)
            {
                return request;
            }
            return null;
        }
        public async Task<string> generate(ACRequest request, Student student, string user, string term, int index, bool validateflow)
        {
            string output_path = null;
            Proof3510GraduatePDF pdf = new Proof3510GraduatePDF();
            ACSignatory signatory_obj = new ACSignatory();
            ACTerm term_obj = new ACTerm();
            Proof3510GraduatePDF.Data data = new Proof3510GraduatePDF.Data();
            List<ACSignatory> signs = signatory_obj.listByACDocument(
                document_id, student.profile.college.id);
            //same data for all pdfs
            data.name_file = string.Format(
                "{0}-{2}-{1}",
                request.id, student.id, index);
            data.user = user;
            data.signs = signatory_obj.listPDFSign(signs);
            data.student_id = student.id;
            data.student_names = student.full_name;
            data.student_gender = student.gender;
            data.college_name = profile.college.name;
            data.program_name = profile.program.name;
            data.request_id = request.id;
            data.document_number = request.document_number;
            //our own data
            AcademicTerm last_term_obj = AcademicTerm.getLastTerm(
                student.profile                
            );
            
            data.last_term = ACTerm.cleanForDocuments(last_term_obj.id);
            int period = this.profile.cycleByTerm(last_term_obj.id);
            data.last_term_period = SuperString.NUMBER_TO_ROMAN[period];
            

            ThirdFifthTenth promo = new ThirdFifthTenth().getPromotional(profile);
            data.last_term_avg = promo.score.ToString();

            data.prom_3510 = new List<String>();
            if (promo.order == 10)
                data.prom_3510.Add(SuperString.NUMBER_TO_ORDINALS[10]);
            if (promo.order >= 5)
                data.prom_3510.Add(SuperString.NUMBER_TO_ORDINALS[5]);
            if (promo.order >= 3)
                data.prom_3510.Add("TERCIO"); 

            //generate
            output_path = Proof3510GraduatePDF.generate(data);
            if (output_path != null)
            {
                string message = string.Format(
                    "Solicitud generada el {0}, por el usuario: {1}"
                    , DateTime.Now, user);
                if (validateflow) await request.goDeliveryPending(message, user);
            }
            return output_path;
        }

        /// <summary>
        /// Crear solicitud sin deuda
        /// </summary>
        /// <param name="student">Estudiante</param>
        /// <param name="profile">Perfil de estudiante</param>
        /// <param name="user">Usuario que crea</param>
        /// <returns></returns>
        public async Task<ACRequest> createRequestBatch(Student student , AcademicProfile profile, string user) {
            string json = JsonConvert.SerializeObject(new { description = $"Solicitud por Batch: {user}" });
            ACRequest request = await base.composeRequest(student, profile, true, false);
            request.document_id = document_id;
            request.description = json;
            request.state = _ate.STATE_FINAL;
            int save = await request.createCustomRequest(user, false, _ate.STATE_FINAL);
            if (save <= 0)
            {
                throw new Exception("Error al guardar la solicitud.");
            }
            return save == 1 ? request : null;
        }

        /// <summary>
        /// Genera documento en bacth
        /// </summary>
        /// <param name="student">Estudiante</param>
        /// <param name="user">Usuario que crea</param>
        /// <returns></returns>
        public async Task<string> GetDocumentPathBatch(Graduated student, string user) {
            var studentDepartment = Department.toBanner(student.modality_id);
            var college = Program.getCollege(student.school_id);
            var estudiante = new Student(student.student_id);
            var escuelaList = new List<string> { student.school_id };
            if (student.school_id == _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA) escuelaList.Add(_corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA_OLD);

            /*Obtener el ultimo movimiento*/
            var movEstudiante = Student.GetLastMovimientoEstudianteBySchoolId(Student.getPidmFromStudentId(student.student_id), student.school_id);
            if (movEstudiante == null)
            {
                throw new NullReferenceException(
                   $"No se encontró registro de postulante para {student.student_id} en {student.school_id}");
            }

            AcademicProfile academicProfile = new AcademicProfile
            {
                person_id = Student.getPidmFromStudentId(student.student_id),
                div = _acad.DEFAULT_DIV,
                campus = Campus.get(movEstudiante.CODIGO_CAMPUS),
                department = Department.toBanner(student.modality_id),
                subject = movEstudiante.CATALOGO_REAL,
                college = Program.getCollege(student.school_id),
                program = Program.get(student.school_id),
                adm_type = AdmisionType.get(Entrant.GetCurrentModalityPostulationBySchoolIdAndDepartment(Student.getPidmFromStudentId(student.student_id), student.school_id, Department.toBanner(student.modality_id)), null, "banner")
            };

            ACRequest acRequest = await createRequestBatch(estudiante, academicProfile, user);
            ACDocument docObject = new ACDocument();

            string docPath = await docObject.generateDocument(acRequest.id, user, "", 1, false);
            return docPath;
        }

        /// <summary>
        /// Inicia creación de solicitud y documento sin deuda
        /// </summary>
        /// <param name="student">Estudiante</param>
        /// <param name="user">Usuario que crea</param>
        /// <returns></returns>
        public async Task<Graduated> GenerateBatch(Graduated student, string user) {
            if (student.school_id == NO_SCHOOL || !student.is_selected ||
                    student.academic_status == NO_ACADEMIC_STATUS || !student.Processing)
            {
                throw new Exception("No se puede generar el documento.");
            }
            
            var estudiante = new Student(student.student_id);
            var perfil = new AcademicProfile
            {
                person_id = Student.getPidmFromStudentId(student.student_id),
                department = Department.toBanner(student.modality_id),
                program = Program.get(student.school_id)
            };
            ThirdFifthTenth promo = new ThirdFifthTenth().getPromotional(perfil);
            if (promo == null)
            {
                throw new Exception("El estudiante no pertenece al tercio quinto promocional.");
            }

            string dictPath = await GetDocumentPathBatch(student, user);
            student.doc_url = dictPath;
            if (!string.IsNullOrEmpty(student.doc_url))
            {
                student.download = true;
                student.Processing = false;
            }

            if (string.IsNullOrEmpty(student.doc_url))
            {
                student.ProcessingError = true;
            }

            student.Processing = false;
            return student;
        }

        #endregion methods
    }
}
