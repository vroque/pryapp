﻿using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACTools.PDF;
using ACTools.PDF.Documents;
using ACTools.SuperString;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _inst = ACTools.Constants.InstitucionalConstants;


namespace CABusiness.AtentionCenter.Documents
{
    /// <summary>
    /// Constancia de estudios
    /// </summary>
    public class StudiesProof: ACDocument
    {
        #region properties
        private int document_id = _ate.DOC_STUDIES_PROOF;
        private const bool aproved = true;
        /// <summary>
        /// term list debido a que es una lista de periodo academicos
        /// </summary>
        public List<string> term_list { get; set; }

        public int type { get; set; }

        #endregion properties

        #region constructors
        public StudiesProof(AcademicProfile profile,int type)
        {            
            this.type = type;
            this.is_valid = true;

            //we change document_id for this types because they are different 
            //documents(different cost and id but similar pdf content)
            switch (this.type) {
                case 3:
                    //promedio acumulado
                    this.document_id = 24;
                    break;
                case 4:
                    //por carrera culminada
                    this.document_id = 26;
                    break;
                case 5:
                    //personalizada
                    this.document_id = 25;
                    break;
                case 6:
                    //con fecha de periodo
                    this.document_id = 52;
                    break;
            }          
            base.sync(document_id, profile);
        }
        public StudiesProof(AcademicProfile profile)
        {
            this.type = type;
            this.is_valid = true;
            base.sync(document_id, profile);
        }
        #endregion constructors

        #region methods   
        /// <summary>
        /// Sobreescribe el metodo de validacion
        /// ejecuta la validacion del padre y luego
        /// ejecuta la validacion de este documetno
        /// </summary>
        /// <returns></returns>
        public new async Task<bool> validate(){

            switch (this.type)
            {
                case 0: //constancia de estudios: General (Constancia del periodo actual)
                case 6: //constancia de estudios: (Constancia del periodo actual con fechas)
                    //comprobamos que el alumno este estudiando actualmente
                    Enrollment enr_obj = new Enrollment();
                    bool has_enrollment = enr_obj.hasEnrollmentInThisTerm(profile);
                    
                    if (!has_enrollment) {
                        this.stack_error.Add("No se encuentra estudiando actualmente.");
                        this.is_valid = false;                    
                    }

                    bool has_enrollmentRangeInitClassEndTerm = enr_obj.hasEnrollmentInTermRangeInitClassEndTerm(profile, 15, 0);
                    if (!has_enrollmentRangeInitClassEndTerm)
                    {
                        this.stack_error.Add("No se puede generar el documento en estas fechas.");
                        this.is_valid = false;
                    }

                    break;
                case 1:
                    //Histórico (Constancia hasta último periodo culminado)
                    //comprobamos que el alumno este  matriculado
                    if (this.profile.status < _acad.STATE_STUDENT)
                    {
                        this.is_valid = false;
                        stack_error.Add("No registra matrícula.");
                    }
                     break;
                case 2:
                    //Por Periodo
                    //comprobamos que el alumno este  matriculado
                    if (this.profile.status < _acad.STATE_STUDENT)
                    {
                        this.is_valid = false;
                        stack_error.Add("No registra matrícula.");
                    }
                    List<string> terms = await getTermList();
                    if (terms == null || terms.Count <= 0)
                    {
                        this.is_valid = false;
                        stack_error.Add("No registra matrícula en periodos anteriores.");
                    }
                    break;
                case 3:
                    //Promedio acumulado
                    //comprobamos que el alumno este  matriculado
                    if (this.profile.status < _acad.STATE_STUDENT)
                    {
                        this.is_valid = false;
                        stack_error.Add("No registra matrícula.");
                    }
                    break;
                case 4:
                    //Por Carrera Culminada
                    //comprobamos que el alumno este egresado
                    if (this.profile.status < _acad.STATE_GRADUATE)
                    {
                        this.is_valid = false;
                        stack_error.Add("No ha culminado ninguna carrera.");
                    }
                     break;
                case 5:
                    //Personalizada
                    //comprobamos que el alumno este  matriculado
                    if (this.profile.status < _acad.STATE_STUDENT)
                    {
                        this.is_valid = false;
                        stack_error.Add("No registra matrícula.");
                    }
                    break;
                default:
                    stack_error.Add("No se selecciono el tipo de certificado.");
                    this.is_valid = false;
                    break;
            }
            return this.is_valid;
        }
        /// <summary>
        /// this is only used by type 2(by_term)
        /// </summary>
        /// <returns>the list of terms by profile </returns>
        public async Task<List<string>> getTermList()
        {
            Enrollment enr_obj = new Enrollment();
 
            this.term_list = enr_obj.getOldsTermsEnrollment(this.profile);
            return this.term_list;

        }
        /// <summary>
        /// Genera el objeto solicitud
        /// * Peculiaridades con respecto a otras solicitudes
        ///   - Necesita comporbar que el alumno este estudiando actualmente
        /// </summary>
        /// <param name="student"></param>
        /// <param name="profile"></param>
        /// <param name="description"></param>
        /// <param name="type"></param>
        /// <param name="term_list">lista de peridos(201710, 201720 etc)</param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ACRequest> createRequest(
            Student student, AcademicProfile profile, string description
            ,int type, List<string> term_list, string user)
        {
            this.type = type;
            //implementar su ID y su DescSolicitud
            await this.validate();        
            ACRequest request = await base.composeRequest(
                student, profile,this.is_valid);
            string json;
            //cost and description
            if (type == 5)
            {
                request.aproved = false;
                json = JsonConvert.SerializeObject(new
                {
                    description = description,
                    type = type
                });
                request.document_id = document_id;
                request.description = json;
                request.aproved = aproved;
                request.cost = cost.amount;
                request.state = _ate.STATE_APROVAL_PENDING;
                request.office_id = _inst.OFFICE_RA;
                int j = await request.create(user);
                if (j == 1)
                    return request;
                return null;
            }
            if (type != 2)
            {
                //type 2: by_term

                //for test
                //List<string> last_term =new List<string> { "201620" };     
                ACTerm term_obj = new ACTerm();
                string last_term = await term_obj.getCurrentAsync(
                    profile.campus.id, profile.department);
                List<string> list_term =new List<string> {last_term };
                json = JsonConvert.SerializeObject(
                    new {
                        description = description,
                        type = type,
                        program = profile.program.id,
                        term = list_term
                    }
                );

                request.cost = cost.amount;
            }
            else
            {
                json = JsonConvert.SerializeObject(
                    new {
                        description = description,
                        type = type,
                        program = profile.program.id,
                        term_list = term_list}
                    );
                //cost based on period_list(terms selected in UI)
                if (term_list.Count > 1)
                {
                    request.cost = cost.amount * term_list.Count;
                }
                else
                {
                    request.cost = cost.amount;
                }
            }
            request.document_id = document_id;
            request.description = json; 
            request.aproved = aproved; 
            if (request.state == _ate.STATE_INITIAL)
                request.state = _ate.STATE_PAYMENT_PENDING; 
            int i = await request.create(user);
            if(i == 1)
                return request;
            return null;
        }

        /// <summary>
        /// Generar el archivo pdf correspondiente a las solicitud
        /// </summary>
        /// <param name="request">obj Solicitud</param>
        /// <param name="student"></param>
        /// <param name="user"></param>
        /// <param name="term"></param>
        /// <param name="index"></param>
        /// <returns>string URL Path del documento</returns>
        public async Task<string> generate(ACRequest request, Student student, string user, string term, int index)
        {
            JObject request_description = JObject.Parse(request.description);
            string output_path = null;
            ACSignatory signatory_obj = new ACSignatory();
            ACTerm term_obj = new ACTerm();
            List<ACSignatory> signs = signatory_obj.listByACDocument(document_id, student.profile.college.id);
            PDFData data = new PDFData
            {
                name_file = $"{request.id}-{index}-{student.id}",
                user = user,
                signs = signatory_obj.listPDFSign(signs),
                student_id = student.id,
                student_names = student.full_name,
                student_gender = student.gender,
                college_name = profile.college.name,
                program_name = profile.program.name,
                request_id = request.id,
                document_number = index
            };
            /// studies proof type={
            /// 0:"general",
            /// 1:"history(last-term)",
            /// 2:"by_term"
            /// 6:"with dates"
            /// }
            switch (request_description["type"].ToString())
            {
                case "0":
                {
                    //Solo para el periodo actual
                    StudiesProofPDF pdf = new StudiesProofPDF();
                    StudiesProofPDF.Data edata = new StudiesProofPDF.Data(data);
                    //Periodo ACTUAL
                    ACTerm ac_term = term_obj.getCurrent(
                        profile.campus.id, profile.department);
                    edata.last_term = ACTerm.cleanForDocuments(ac_term.term);

                    int cycle = profile.actualCycle();
                    edata.last_term_period = cycle.numberToRoman();
                    //generate
                    output_path = StudiesProofPDF.generate(edata);
                    break;
                }
                case "6":
                {
                    //Solo para el periodo actual con fecha
                    StudiesProofWithDatesPDF pdf = new StudiesProofWithDatesPDF();
                    StudiesProofWithDatesPDF.Data edata = new StudiesProofWithDatesPDF.Data(data);
                    //Periodo ACTUAL
                    ACTerm ac_term = term_obj.getCurrent(
                        profile.campus.id, profile.department);
                    edata.last_term = ACTerm.cleanForDocuments(ac_term.term);
                    edata.last_term_end_date = ac_term.end_classes_date;
                    edata.last_term_start_date = ac_term.start_classes_date;
                    int cycle = profile.actualCycle();
                    edata.last_term_period = cycle.numberToRoman();
                    //generate
                    output_path = StudiesProofWithDatesPDF.generate(edata);
                    break;
                }
                default:
                {
                    //studies proof (history(last-term),by_term)
                    StudiesProofPDFByTerm pdf = new StudiesProofPDFByTerm();
                    StudiesProofPDFByTerm.Data edata = new StudiesProofPDFByTerm.Data(data);
                    //same data for all pdfs
                    ACTerm ac_term;
                    if (request_description["type"].ToString() == "1")
                    { // last term
                        Enrollment enr_obj = new Enrollment();
                        ac_term = enr_obj.getLastTermEnrollment(profile);
                        //ac_term = term_obj.get(profile.campus.id, profile.department);
                    }
                    else // custom term
                        ac_term = term_obj.get(term, profile.campus.id, profile.department);

                    edata.last_term = ACTerm.cleanForDocuments(ac_term.term);
                    // ACTerm term_dates = new ACTerm(data.last_term, student.profile.campus.id);
                    //if (term_dates.end_date == null) throw new Exception("no dates for this term");

                    //cycle in determinate term asn program
                    int cycle = profile.cycleByTerm(ac_term.term);
                    edata.last_term_period = cycle.numberToRoman();
                    output_path = StudiesProofPDFByTerm.generate(edata);
                    break;
                }
            }

            if (output_path != null)
            {
                string message = string.Format(
                    "Solicitud generada el {0}, por el usuario: {1}"
                    , DateTime.Now, user);
                await request.goDeliveryPending(message, user);
            }
            return output_path;
        }

        #endregion methods
    }
}
