﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Admision;
using ACBusiness.Institutional;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.CAU;
using ACTools.PDF;
using ACTools.PDF.Documents;
using ACTools.SuperString;
using Newtonsoft.Json;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _admission = ACTools.Constants.AdmissionConstant;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    /// <inheritdoc />
    /// <summary>
    /// Constancia de Matrícula PRONABEC
    /// </summary>
    public class PronabecProofOfEnrollment : ACDocument
    {
        #region Properties

        private const int DOCUMENT_ID = _ate.DOC_PROOF_OF_ENROLLMENT_PRONABEC;
        private const int REQUEST_FINALIZED = _ate.STATE_FINAL;
        private const string POSTULANT_ENTRANT = _admission.INGRESANTE;
        private const string NO_SCHOOL = "0";
        private const decimal NO_ID_MODALIDAD_POSTU = _admission.NO_ID_MODALIDAD_POSTU;
        private const string ING_SISTEMAS = _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA;
        private const string ING_SISTEMAS_OLD = _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA_OLD;

        private readonly DBOContext DboContext = new DBOContext();
        private readonly NEXOContext NexoContext = new NEXOContext();

        #endregion Properties

        #region Methods

        /// <summary>
        /// <para>Genera Constancia de Matrícula PRONABEC solo para estudiantes seleccionados.</para>
        /// Retorna la lista original con constancia de matrícula solo para estudiantes pronabec seleccionados
        /// </summary>
        /// <param name="scholarshipStudents">Estudiantes PRONABEC</param>
        /// <param name="pronabecType">Tipo de PRONABEC</param>
        /// <param name="schoolId">Escuela académica</param>
        /// <param name="academicPeriod">Período académico</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns>Lista original con constancia de matrícula solo para estudiantes pronabec seleccionados</returns>
        public async Task<List<ScholarshipStudent>> GenerateAsync(IEnumerable<ScholarshipStudent> scholarshipStudents,
            string pronabecType, string schoolId, string academicPeriod, string user)
        {
            var enrolled = new List<ScholarshipStudent>();
            var noEnrolled = new List<ScholarshipStudent>();

            // Separamos únicamente los estudiantes seleccionados
            foreach (var student in scholarshipStudents)
            {
                if (student.SchoolId != NO_SCHOOL && student.IsSelected && student.Processing && student.IsPronabec)
                {
                    var perfil = new AcademicProfile
                    {
                        person_id = student.StudentPidm,
                        department = Department.toBanner(student.ModalityId),
                        program = Program.get(student.SchoolId)
                    };
                    // Período académico notación BANNER
                    var academicPeriodBanner = Term.toBanner(academicPeriod, perfil.department);

                    // Todos los períodos matriculados del estudiante
                    var allEnrolledPeriods = new Enrollment().GetAllTermsEnrolled(perfil);

                    // Buscamos si existe matrícula en el período seleccionado
                    var haveEnrolledSelectedPeriod =
                        allEnrolledPeriods.Values.Where(w => w.Contains(academicPeriodBanner)).ToList();

                    if (haveEnrolledSelectedPeriod.Any())
                    {
                        // Genera constancia matrícula
                        var dictPath = await GetDocumentPathAsync(student, user, academicPeriod);
                        student.DocUrl = dictPath["url"];
                        student.SaveAs = dictPath["saveAs"];
                        if (!string.IsNullOrEmpty(student.DocUrl))
                        {
                            student.Download = true;
                        }

                        if (string.IsNullOrEmpty(student.DocUrl))
                        {
                            student.ProcessingError = true;
                        }

                        student.Processing = false;

                        enrolled.Add(student);
                        continue;
                    }

                    // No Existe matrícula

                    // Buscar si existe matrícula en el período anterior o pesterior
                    var previousPeriodEnrolled = new List<string>();
                    var nextPeriodEnrolled = new List<string>();

                    foreach (var enrolledPeriod in allEnrolledPeriods)
                    {
                        if (int.Parse(enrolledPeriod.Value) < int.Parse(academicPeriodBanner))
                        {
                            previousPeriodEnrolled.Add(enrolledPeriod.Key);
                        }

                        if (int.Parse(enrolledPeriod.Value) > int.Parse(academicPeriodBanner))
                        {
                            nextPeriodEnrolled.Add(enrolledPeriod.Key);
                        }
                    }

                    if (previousPeriodEnrolled.Any())
                    {
                        var periodo = previousPeriodEnrolled.OrderByDescending(o => o).FirstOrDefault();
                        student.OtherPeriod = periodo;
                        student.OtherPeriodDescription = "prev";
                        student.Processing = false;
                        enrolled.Add(student);
                        continue;
                    }

                    if (nextPeriodEnrolled.Any())
                    {
                        var periodo = nextPeriodEnrolled.OrderBy(o => o).FirstOrDefault();
                        student.OtherPeriod = periodo;
                        student.OtherPeriodDescription = "next";
                        student.Processing = false;
                        enrolled.Add(student);
                        continue;
                    }

                    // aqui armar un error para ingresar en el api
                    student.DocUrl = string.Empty;
                    student.ProcessingError = true;

                    enrolled.Add(student);
                }
                else
                {
                    noEnrolled.Add(student);
                }
            }

            return enrolled.Union(noEnrolled).OrderBy(o => o.StudentName).ToList();
        }

        /// <summary>
        /// Genera URL para descargar la Constancia de Matrícula PRONABEC
        /// </summary>
        /// <param name="scholarshipStudent"></param>
        /// <param name="user"></param>
        /// <param name="academicPeriod"></param>
        /// <returns></returns>
        private async Task<Dictionary<string, string>> GetDocumentPathAsync(ScholarshipStudent scholarshipStudent,
            string user, string academicPeriod)
        {
            var academicPeriodApec = Term.toAPEC(academicPeriod);
            var academicPeriodBanner = Term.toBanner(academicPeriod);

            var college = Program.getCollege(scholarshipStudent.SchoolId);

            var acSignatory = new ACSignatory();
            var signsList = acSignatory.listByACDocument(DOCUMENT_ID, college.id);

            var estudiante = new Student(scholarshipStudent.StudentId);

            var postulante = new List<DBOTblPostulante>();

            var escuelaList = new List<string> {scholarshipStudent.SchoolId};
            if (scholarshipStudent.SchoolId == ING_SISTEMAS)
            {
                escuelaList.Add(ING_SISTEMAS_OLD);
            }

            // Buscamos si existe registro en tblPostulante para el período del argumento para obtener datos de ingreso
            var hasRegPeriod =
                await DboContext.tbl_postulante.Where(w => w.IDAlumno == scholarshipStudent.StudentId &&
                                                           escuelaList.Contains(w.IDEscuela) &&
                                                           w.Ingresante == _admission.INGRESANTE &&
                                                           w.Renuncia == _admission.NO_RENUNCIA_CARRERA &&
                                                           w.IDModalidadPostu != NO_ID_MODALIDAD_POSTU &&
                                                           w.IDPerAcad == academicPeriodApec &&
                                                           w.IDDependencia == _corp.UC).ToListAsync();

            if (hasRegPeriod.Any())
            {
                postulante.Add(hasRegPeriod.OrderByDescending(o => o.IDExamen).FirstOrDefault());
            }
            else
            {
                // Saca todos los cambios de tblPostulante para luego filtrar el período del argumento
                var hasOtherRegPeriod =
                    await DboContext.tbl_postulante.Where(w => w.IDAlumno == scholarshipStudent.StudentId &&
                                                               escuelaList.Contains(w.IDEscuela) &&
                                                               w.Ingresante == POSTULANT_ENTRANT &&
                                                               w.Renuncia == _admission.NO_RENUNCIA_CARRERA &&
                                                               w.IDModalidadPostu != NO_ID_MODALIDAD_POSTU &&
                                                               w.IDDependencia == _corp.UC).ToListAsync();

                // Filtro de info anterior a período del argumento para sacar datos para ser usados en la constancia
                postulante.AddRange(hasOtherRegPeriod.Where(w =>
                    int.Parse(Term.toBanner(w.IDPerAcad)) < int.Parse(academicPeriodBanner)));
            }

            var newRegPostulante = postulante.OrderByDescending(o => o.IDPerAcad).ThenByDescending(o => o.IDExamen)
                .FirstOrDefault();

            string newPeriodApec;
            string idDependencia;
            string idSede;
            string idPlanEstudio;
            if (newRegPostulante != null)
            {
                newPeriodApec = newRegPostulante.IDPerAcad;
                idDependencia = newRegPostulante.IDDependencia;
                idPlanEstudio = newRegPostulante.IDPlanestudio;
                // TODO: ¿Debería de verificar solo carreras UC?
                var alumnoEstado = await DboContext.tbl_alumno_estado.Where(w =>
                        w.IDAlumno == scholarshipStudent.StudentId &&
                        w.IDPerAcad == newPeriodApec)
                    .OrderByDescending(o => o.FecMatricula).FirstOrDefaultAsync();
                idSede = alumnoEstado != null ? alumnoEstado.IDSede : string.Empty;
            }
            else
            {
                throw new Exception("No se encontró registro de postulante");
            }

            var academicProfile = new AcademicProfile
            {
                person_id = Student.getPidmFromStudentId(scholarshipStudent.StudentId),
                div = idDependencia,
                campus = Campus.getByAPECCode(idSede),
                department = Department.toBanner(scholarshipStudent.ModalityId),
                subject = idPlanEstudio,
                college = Program.getCollege(scholarshipStudent.SchoolId),
                program = Program.get(scholarshipStudent.SchoolId),
                adm_type = AdmisionType.get(
                    Entrant.GetEntrantInfo(scholarshipStudent.StudentId, scholarshipStudent.SchoolId).type_of_income_id
                        .ToString(CultureInfo.InvariantCulture), null, "apec")
            };

            /**
             * Generación de Solicitud para guardar evidencia de generación del documento
             */
            ACRequest acRequest;

            // Se busca si existe solicitud asociada a la creación de constancia de matrícula PRONABEC
            var requestExist =
                await NexoContext.cau_requests.Where(w => w.documentID == DOCUMENT_ID &&
                                                          // buscar concepto
                                                          academicPeriod.Contains(w.requestDescription) &&
                                                          w.pidm == estudiante.person_id)
                    .OrderByDescending(o => o.requestID).FirstOrDefaultAsync();
            // Si existe solicitud asociada, utilizamos los datos de éste para la generación de la constancia
            if (requestExist != null)
            {
                acRequest = await ACRequest.get(requestExist.requestID);
            }
            // sino, generamos nueva solicitud
            else
            {
                acRequest =
                    await CreateRequestAsync(academicPeriod, estudiante, academicProfile, user);

                if (ACRequest.isNull(acRequest)) return null;
            }

            var acTermInfo = await ACTerm.GetByAcademicPeriodCampusAndDepartament(academicPeriod, academicProfile);

            // Data para el PDF
            var data = new PronabecProofOfEnrollmentPdf.Data
            {
                name_file =
                    $"{acRequest.id}-{acRequest.document_number}-{Student.getDNI(scholarshipStudent.StudentId)}",
                user = user,
                signs = acSignatory.listPDFSign(signsList),
                student_id = Student.getDNI(scholarshipStudent.StudentId),
                student_names = scholarshipStudent.StudentName,
                student_gender = string.Empty,
                college_name = college.name,
                program_name = scholarshipStudent.SchoolName,
                request_id = acRequest.id,
                document_number = acRequest.document_number,
                photo_bytes = new Person().getPhoto(Student.getPidmFromStudentId(scholarshipStudent.StudentId)),
                Term = ACTerm.cleanForDocuments(academicPeriod),
                Period = Student.cycleByAcademicPeriod(academicPeriodBanner, academicProfile).numberToRoman(),
                Courses = new Course().reportCardtByTermBANNER(academicProfile, academicPeriodBanner)
                    .Select(s => new PronabecProofOfEnrollmentPdf.Course
                    {
                        Code = s.id,
                        Name = s.name,
                        Type = s.type,
                        Ht = Convert.ToInt32(s.TheoreticalHours),
                        Hp = Convert.ToInt32(s.PracticalHours),
                        Credits = s.credits
                    }).ToList()
            };

            var prooOfEnrollment = PDFActions.exists(data.name_file)
                ? new Dictionary<string, string>
                {
                    {"url", PDFActions.getRepoDocumentPath(data.name_file)},
                    {"saveAs", data.student_names}
                }
                : new Dictionary<string, string>
                {
                    {"url", PronabecProofOfEnrollmentPdf.Generate(data)},
                    {"saveAs", data.student_names}
                };

            if (prooOfEnrollment["url"].Any()) return prooOfEnrollment;

            // Eliminar la solicitud cuando no se haya generado el documento
            var req = new CAUTblRequest {requestID = acRequest.id};
            NexoContext.cau_requests.Remove(req);
            var res = await NexoContext.SaveChangesAsync();

            return prooOfEnrollment;
        }

        /// <summary>
        /// <para>Creación de solicitud ara generar Constancia de Matrícula PRONABEC.</para>
        /// Se crea con costo = 0 y estado de solicitud = "finalizado"
        /// </summary>
        /// <param name="academicPeriod">Período académico</param>
        /// <param name="student">Datos del estudiante</param>
        /// <param name="academicProfile">Perfil académico</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        private async Task<ACRequest> CreateRequestAsync(string academicPeriod, Student student,
            AcademicProfile academicProfile, string user)
        {
            var json = JsonConvert.SerializeObject(new
            {
                description = "Solicitud generada automáticamente para Constancia de Matrícula PRONABEC",
                term = academicPeriod
            });

            var acRequest = await composeRequest(student, academicProfile, true, false);

            // Datos de la solicitud
            acRequest.document_id = DOCUMENT_ID;
            acRequest.description = json;
            acRequest.state = REQUEST_FINALIZED;
            var i = await acRequest.createCustomRequest(user, false, REQUEST_FINALIZED, string.Empty);

            return i == 1 ? acRequest : null;
        }

        #endregion Methods
    }
}