﻿using ACBusiness.Academic;
using ACBusiness.Admision;
using ACBusiness.Institutional;
using ACTools.PDF.Documents;
using ACTools.SuperString;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.AtentionCenter.Documents
{
    /// <summary>
    /// Constancia de 3ro, 5to y 10mo Superior por Periodo
    /// </summary>
    public class ProofOf3510PerPeriod : ACDocument
    {
        #region properties
        private static int document_id = 9; // Constancia de 3ro, 5to y 10mo Superior por Periodo
        private const string NO_SCHOOL = "0";

        /// <summary>
        /// Sin info sobre estado académico (matriculado, estudiante, no egresado, egresado, etc)
        /// </summary>
        private const int NO_ACADEMIC_STATUS = 0;
        public List<ThirdFifthTenth> term_list { get; set; }
        public bool has_3510 { get; set; }
        #endregion properties

        #region constructors
        public ProofOf3510PerPeriod(AcademicProfile profile = null)
        {
            if (profile!= null)
            {
                base.sync(document_id, profile);
                this.syncTakenPeriods();
                this.has3510();
            }
            
        }
        #endregion constructors


        #region methods
        /// <summary>
        /// Sobreescribe el metodo de validacion
        /// ejecuta la validacion del padre y luego
        /// ejecuta la validacion de este documento
        /// </summary>
        /// <returns></returns>
        public new async Task<bool> validate()
        {
            bool valid = true;
            // Valida si el estudiante es matriculado o más
            if (this.profile.status < _acad.STATE_STUDENT)
            {
                valid = false;
                base.stack_error.Add("No registra matrícula.");
            }
            this.is_valid = valid;
            return valid;
        }


        /// <summary>
        /// Verifica si un estudiante tiene algun puesto en 3, 5 y 10 superior
        /// </summary>
        private void has3510()
        {
            // FIXME
            int count_3510 = _db.dim_student_enrollments.Where(x => x.pidm == this.person_id && x.thirdFithTenthTop != "0").Count();
            this.has_3510 = count_3510 == 0 ? false : true;
        }


        /// <summary>
        /// Llena la lista de periodos en los cuales el 
        /// estudiante obtuvo 3ro, 5to o 10mo superior
        /// </summary>
        private void syncTakenPeriods()
        {
            // TODO: Reemplazar codigo en duro por la variable de pidm cuando se tenga datos en DB de integración
            this.term_list = new ThirdFifthTenth().list(this.profile);

        }


        /// <summary>
        /// Sobreescribe el método que crea
        /// la solicitud dándole parámentos
        /// específicos para este documento
        /// </summary>
        /// <param name="student">objecto estudiante</param>
        /// <param name="profile">objeto perfil</param>
        /// <param name="description">cadena de descripción</param>
        /// <returns></returns>
        public async Task<ACRequest> createRequest(
            Student student, AcademicProfile profile, List<string> term_list, string description, string user)
        {
            string json = JsonConvert.SerializeObject(new { description = description, term_list = term_list });
            bool valid = await this.validate();
            if (!valid)
                return null;
            ACRequest request = await base.composeRequest(student, profile);
            // Aumenta propias de esta solicitud
            request.document_id = document_id;
            // Cálculo de costo a pagar según el número de periodos seleccionados
            if (term_list.Count > 1)
                request.cost = cost.amount * term_list.Count;
            else
                request.cost = cost.amount;
            bool base_validation = await base.validate();
            request.description = json; // JSON
            if (request.state != _ate.STATE_CANCEL)
            {
                request.state = _ate.STATE_PAYMENT_PENDING; // Pendiente de pago
            }
            // TODO: request.concept_id = amount.id; //FALTA IMPLEMENTAR
            int i = await request.create(user);
            if (i == 1)
            {
                return request;
            }
            return null;
        }
        public async Task<string> generate(ACRequest request, Student student, string user, string term, int index, bool validateflow)
        {
            string output_path = null;
            Proof3510ByTermPDF pdf = new Proof3510ByTermPDF();
            ACSignatory signatory_obj = new ACSignatory();
            ACTerm term_obj = new ACTerm();
            Proof3510ByTermPDF.Data data = new Proof3510ByTermPDF.Data();
            List<ACSignatory> signs = signatory_obj.listByACDocument(
                document_id, student.profile.college.id);
            //same data for all pdfs
            data.name_file = string.Format(
                "{0}-{2}-{1}",
                request.id, student.id, index);
            data.user = user;
            data.signs = signatory_obj.listPDFSign(signs);
            data.student_id = student.id;
            data.student_names = student.full_name;
            data.student_gender = student.gender;
            data.college_name = profile.college.name;
            data.program_name = profile.program.name;
            data.request_id = request.id;
            data.document_number = index;
            //our own data
            ThirdFifthTenth s35 = new ThirdFifthTenth().get(this.profile, term);
            if (s35 == null)
                throw new Exception($"No pertenece a un grupo superior en el periodo {term}");
            
            data.term = ACTerm.cleanForDocuments(s35.term);
            data.term_period = SuperString.NUMBER_TO_ROMAN[int.Parse(s35.period)];
            data.term_avg = s35.score.ToString("0.00");
            data.term_3510 = new List<String>();
            if (s35.order == 10)
                data.term_3510.Add(SuperString.NUMBER_TO_ORDINALS[10]);
            if (s35.order >= 5)
                data.term_3510.Add(SuperString.NUMBER_TO_ORDINALS[5]);
            if (s35.order >= 3)
                data.term_3510.Add("TERCIO");
            //generate
            output_path = Proof3510ByTermPDF.generate(data);
            if (output_path != null)
            {
                string message = string.Format(
                    "Solicitud generada el {0}, por el usuario: {1}"
                    , DateTime.Now, user);
                if (validateflow)
                {
                    await request.goDeliveryPending(message, user);
                }
                
            }
            return output_path;
        }
        #endregion methods

        private async Task<ACRequest> createRequestBatch(string term, Student student, AcademicProfile profile, string user)
        {
            List<string> term_list = new List<string>();
            term = Term.toBanner(term);
            term_list.Add(term);
            var json = JsonConvert.SerializeObject(new
            {
                description = "Solicitud sin deuda generada por Registros.",
                term_list = term_list
            });
            var acRequest = await composeRequest(student, profile, true, false);
            acRequest.document_id = document_id;
            acRequest.description = json;
            acRequest.state = _ate.STATE_FINAL;
            //acRequest.concept_id = BACHILLER_CONCEPT_ID;
            int save = await acRequest.createCustomRequest(user, false, _ate.STATE_FINAL);
            if (save <= 0)
            {
                throw new Exception("Error al guardar la solicitud.");
            }

            return save == 1 ? acRequest : null;
        }

        public async Task<string> GetDocumentPathBatch(Graduated student, string user, string term) {
            //var dboContext = new DBOContext();
            var nexoContext = _db;
            var studentDepartment = Department.toBanner(student.modality_id);
            term = Term.toAPEC(term, studentDepartment);
            var college = Program.getCollege(student.school_id);

            var acSignatory = new ACSignatory();
            var signs = acSignatory.listByACDocument(document_id, college.id);

            var estudiante = new Student(student.student_id);

            //var postulante = new List<DBOTblPostulante>();

            var escuelaList = new List<string> { student.school_id };


            if (student.school_id == _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA) escuelaList.Add(_corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA_OLD);


            /*Obtener el ultimo movimiento*/
            var movEstudiante = Student.GetMovimientosEstudiante(Student.getPidmFromStudentId(student.student_id))
                .ToList().OrderBy(r => r.NUMERO)
                .FirstOrDefault(w => w.CODIGO_PROGRAMA == student.school_id);

            if (movEstudiante == null)
            {
                throw new NullReferenceException(
                   $"No se encontró registro de postulante para {student.student_id} en {student.school_id}");
            }
            string newPeriodApec = Term.toAPEC(movEstudiante.PERIODO_INICIO);
            string idDependencia = _acad.DEFAULT_DIV; // UCCI
            string idPlanEstudio = movEstudiante.CATALOGO_REAL;
            string modalityId = Department.toBanner(movEstudiante.CODIGO_DEPARTAMENTO);
            string idSede = movEstudiante.CODIGO_CAMPUS;

            AcademicProfile academicProfile = new AcademicProfile
            {
                person_id = Student.getPidmFromStudentId(student.student_id),
                div = idDependencia,
                campus = Campus.get(idSede),
                department = Department.toBanner(student.modality_id),
                subject = idPlanEstudio,
                college = Program.getCollege(student.school_id),
                program = Program.get(student.school_id),
                adm_type = AdmisionType.get(Entrant.GetCurrentModalityPostulationBySchoolIdAndDepartment(Student.getPidmFromStudentId(student.student_id), student.school_id, Department.toBanner(student.modality_id)), null, "banner")
            };

            ACRequest acRequest = await createRequestBatch(term, estudiante, academicProfile, user);

            ACDocument doc_object = new ACDocument();

            string doc_path = await doc_object.generateDocument(acRequest.id, user, term, 1, false);
            return doc_path;



        }
        public async Task<Graduated> generateBatch(Graduated student, string term, string user) {
            if (student.school_id == NO_SCHOOL || !student.is_selected ||
                    student.academic_status == NO_ACADEMIC_STATUS || !student.Processing) {
                throw new Exception("No se puede generar el documento.");
            }
            var estudiante = new Student(student.student_id);
            var perfil = new AcademicProfile
            {
                person_id = Student.getPidmFromStudentId(student.student_id),
                department = Department.toBanner(student.modality_id),
                program = Program.get(student.school_id)
            };

            term = Term.toBanner(term, perfil.department);

            ProofOf3510PerPeriod listTerms = new ProofOf3510PerPeriod(perfil);
            await listTerms.validate();
            List<string> TermsHave3510 = listTerms.term_list.Select(f => f.term).ToList().ToList();

            //var have3510Term = listTerms.term_list.Where(f => f.term == termBanner).FirstOrDefault();
            if (TermsHave3510.Contains(term))
            {
                string dictPath = await GetDocumentPathBatch(student, user, term);
                student.doc_url = dictPath;
                //student.save_as = dictPath["save_as"];
                if (!string.IsNullOrEmpty(student.doc_url))
                {
                    student.download = true;
                    student.Processing = false;
                }

                if (string.IsNullOrEmpty(student.doc_url))
                {
                    student.ProcessingError = true;
                }
            }
            else
            {
                if (TermsHave3510.Count > 0)
                {
                    List<int> LstTErmsI = listTerms.term_list.Select(f => int.Parse(f.term)).ToList().ToList();
                    int termi = int.Parse(term);
                    student.other_period = (LstTErmsI.Aggregate((x, y) => Math.Abs(x - termi) < Math.Abs(y - termi) ? x : y)).ToString();
                    
                }
                else
                {
                    student.ProcessingError = true;
                }
                student.download = false;
            }
            student.Processing = false;
            return student;

        }



    }
}
