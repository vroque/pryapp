﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.AtentionCenter
{
    public class ACRequestAttachments
    {
        #region properties
        // public int id { get; set; }
        public int request_id { get; set; }
        public int requeriment_id { get; set; }
        public string requeriment_name { get; set; }
        public bool delivered { get; set; }
        public List<ACAttachmentDocument> documents { get; set; }
        #endregion properties

        #region methods
        public async static Task<List<ACRequestAttachments>> list(int request_id)
        {
            List<ACRequestAttachments> list = new List<ACRequestAttachments>();
            NEXOContext db = new NEXOContext();
            ACRequest req = await ACRequest.get(request_id);
            List<CAUtblDocumentRequirement> reqs = db.cau_document_requirements.Where(f =>
                f.documentID == req.document_id
                && f.requirementCategory == "DOCU")
                .ToList();
            List<CAUTblRequestAttachment> docs = db.cau_request_attachments.Where(f =>
               f.requestID == request_id
            ).ToList();
            foreach(CAUtblDocumentRequirement item in reqs)
            {
                ACRequestAttachments r = new ACRequestAttachments();
                r.request_id = request_id;
                r.requeriment_id = item.id;
                r.requeriment_name = item.requirementDescription;
                r.documents = docs.Where(f => f.requerimentID == item.id)
                    .Select(f => new ACAttachmentDocument {
                        name = f.name,
                        number = f.number,
                        url = f.url
                    }).ToList();
                if (r.documents == null || r.documents.Count == 0)
                    r.delivered = false;
                else r.delivered = true;
                list.Add(r);
            }
            return list;
        }
        #endregion methods
    }

    public class ACAttachmentDocument
    {
        public int number { get; set; }
        public string name { get; set; }
        public string url { get; set; }
    }
}
