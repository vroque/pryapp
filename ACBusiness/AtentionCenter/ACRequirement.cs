﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ACBusiness.AtentionCenter
{
    /// <summary>
    /// Requerimientos de archivos para solicitudes(Documentos)
    /// </summary>
    public class ACRequirement
    {
        #region properties
        public int id { get; set; }
        public int document_id { get; set; }
        public int category_id { get; set; }
        public string description { get; set; }
        public int order { get; set; }
        public int min_files { get; set; }
        public int max_files { get; set; }
        public List<ACRequirementFile> files { get; set; }
        private NEXOContext _nx = new NEXOContext();
        #endregion properties

        #region methods
        public async Task<List<ACRequirement>> getByDocument(int document_id)
        {
            List<CAUTblRequirement> list = await _nx.cau_requirements
               .Where(f => f.documentID == document_id)
               .ToListAsync();

            List<ACRequirement> reqs = list
                .Select<CAUTblRequirement, ACRequirement>(f => f)
                .ToList();
            
            return reqs;
        }

        public async Task<List<ACRequirement>> getByRequest(int request_id, int document_id)
        {
            List<ACRequirement> reqs = await this.getByDocument(document_id);
            var files = await _nx.cau_requirement_files.Where(f =>
                f.requestID == request_id
            ).ToListAsync();

            foreach(var item in reqs)
            {
                item.files = new List<ACRequirementFile>();
                item.files = files.Where(f => f.requirementID == item.id)
                    .ToList()
                    .Select<CAUTblRequirementFiles, ACRequirementFile>(f => f)
                    .ToList();
            }
            return reqs;
        }
        #endregion methods

        #region implicit operator
        public static implicit operator ACRequirement(CAUTblRequirement tmp)
        {
            if (tmp == null) return null;
            ACRequirement r = new ACRequirement();
            r.id = tmp.id;
            r.document_id = tmp.documentID;
            r.category_id = tmp.categoryID;
            r.description = tmp.description;
            r.order = tmp.order;
            r.min_files = tmp.minFiles;
            r.max_files = tmp.maxFiles;
            r.files = new List<ACRequirementFile>();
            return r;
        }
        #endregion implicit operator
    }

    /// <summary>
    /// Registro de archivos entregados de una solicitud
    /// </summary>
    public class ACRequirementFile
    {
        #region properties
        public int id { get; set; }
        public int requirement_id { get; set; }
        public int request_id { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        private NEXOContext _nx = new NEXOContext();
        #endregion properties

        #region methods 

        /// <summary>
        /// Crear el objeto en la base de datos
        /// </summary>
        /// <param name="request_id"></param>
        /// <returns></returns>
        public async Task<ACRequirementFile> create(int request_id)
        {
            this.request_id = request_id;
           
            CAUTblRequirementFiles rf = compose(this);
            rf.createDate = DateTime.Now;
            rf.updateDate = DateTime.Now;
            _nx.cau_requirement_files.Add(rf);

            int r = await _nx.SaveChangesAsync();
            if (r > 0) return rf;
            return null;
        }

        public async Task<bool> delete(int file_id) {
            CAUTblRequirementFiles fileDelete = _nx.cau_requirement_files.Find(file_id);
            _nx.cau_requirement_files.Remove(fileDelete);
            int rowDelete = await _nx.SaveChangesAsync();
            return (rowDelete > 0);
        }


        /// <summary>
        /// Convierte el objeto de negocio en objeto de permanencia
        /// </summary>
        /// <param name="tmp"></param>
        /// <returns></returns>
        private CAUTblRequirementFiles compose(ACRequirementFile tmp)
        {
            if (tmp == null) return null;
            CAUTblRequirementFiles r = new CAUTblRequirementFiles();
            r.id = tmp.id;
            r.requirementID= tmp.requirement_id;
            r.requestID = tmp.request_id;
            r.description = tmp.description;
            r.url = tmp.url;
            return r;
        }
        #endregion methods

        #region implicit operator
        public static implicit operator ACRequirementFile(CAUTblRequirementFiles tmp)
        {
            if (tmp == null) return null;
            ACRequirementFile r = new ACRequirementFile();
            r.id = tmp.id;
            r.description = tmp.description;
            r.requirement_id = tmp.requirementID;
            r.request_id = tmp.requestID;
            r.url = tmp.url;
            return r;
        }
        #endregion implicit operator
    }
}
