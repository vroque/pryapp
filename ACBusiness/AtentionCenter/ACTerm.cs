﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BANNER;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.AtentionCenter
{
    /// <summary>
    /// Rango de fechas en las que se puede generar constancias,
    /// en relacion a un determinado TERM(201710)
    /// * se agrego fechas de matriculas
    /// </summary>
    public class ACTerm
    {
        #region properties

        public string term { get; set; }
        public string part_term { get; set; }
        public string campus_id { get; set; }
        public string department { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public DateTime start_classes_date { get; set; }
        public DateTime end_classes_date { get; set; }
        public DateTime start_enroll_date { get; set; }
        public DateTime end_enroll_date { get; set; }
        public DateTime? start_substitute_exam_date { get; set; }
        public DateTime? end_substitute_exam_date { get; set; }

        private NEXOContext _db = new NEXOContext();

        #endregion properties

        #region constructor

        public ACTerm(string term, string campus_id, string department_id)
        {
            CAUTblTermDocument tmp = _db.cau_tem_documents.FirstOrDefault(
                f =>
                    f.campus == campus_id
                    && f.department == department_id
                    && f.term == term
            );
            this.term = term;
            this.campus_id = campus_id;
            if (tmp != null)
            {
                end_date = tmp.endDate;
                start_date = tmp.startDate;
            }
        }

        public ACTerm()
        {
            //contruct create for old instances
        }

        #endregion construct

        #region methods

        /// <summary>
        /// Crea un nuevo peridodo
        /// </summary>
        /// <returns></returns>
        public async Task<bool> create()
        {
            var term = new CAUTblTermDocument
            {
                term = this.term,
                partTerm = "1",
                campus = campus_id,
                department = department,
                startDate = new DateTime(start_date.Year, start_date.Month, start_date.Day, 0, 0, 0),
                startClassesDate = new DateTime(start_date.Year, start_date.Month, start_date.Day, 0, 0, 0),
                endDate = new DateTime(end_date.Year, end_date.Month, end_date.Day, 23, 59, 59),
                endClassesDate = new DateTime(end_classes_date.Year, end_classes_date.Month, end_classes_date.Day, 23,
                    59, 59),
                startEnrollDate = new DateTime(start_enroll_date.Year, start_enroll_date.Month, start_enroll_date.Day, 0, 0, 0),
                endEnrollDate = new DateTime(end_enroll_date.Year, end_enroll_date.Month, end_enroll_date.Day, 23, 59, 59),
                updateDate = DateTime.Now
            };
            _db.cau_tem_documents.Add(term);
            var i = await _db.SaveChangesAsync();
            return i > 0;

        }

        /// <summary>
        /// Actualiza el periodo
        /// </summary>
        /// <returns></returns>
        public async Task<bool> update()
        {
            CAUTblTermDocument term = _db.cau_tem_documents.FirstOrDefault(f => f.campus == campus_id &&
                                                                                f.department == department &&
                                                                                f.term == this.term);

            term.startDate = new DateTime(start_date.Year, start_date.Month, start_date.Day, 0, 0, 0);
            term.startClassesDate = new DateTime(start_classes_date.Year, start_classes_date.Month,
                start_classes_date.Day, 0, 0, 0);

            term.endDate = new DateTime(end_date.Year, end_date.Month, end_date.Day, 23, 59, 59);
            term.endClassesDate = new DateTime(end_classes_date.Year, end_classes_date.Month, end_classes_date.Day, 23,
                59, 59);
            term.startEnrollDate = new DateTime(start_enroll_date.Year, start_enroll_date.Month, start_enroll_date.Day, 0, 0, 0);
            term.endEnrollDate = new DateTime(end_enroll_date.Year, end_enroll_date.Month, end_enroll_date.Day, 23, 59, 59);
            if (start_substitute_exam_date != null)
            {
                term.startSubstituteExamDate = new DateTime(start_substitute_exam_date.Value.Year, start_substitute_exam_date.Value.Month, start_substitute_exam_date.Value.Day, 0, 0, 0);
            }
            if (end_substitute_exam_date != null)
            {
                term.endSubstituteExamDate = new DateTime(end_substitute_exam_date.Value.Year, end_substitute_exam_date.Value.Month, end_substitute_exam_date.Value.Day, 23, 59, 59);
            }

            var i = await _db.SaveChangesAsync();
            return i > 0;

        }

        /// <summary>
        /// Periodo actual de un determinado campus
        /// * Metodo Asincrono
        /// </summary>
        /// <param name="campus_id"></param>
        /// <returns></returns>
        public async Task<string> getCurrentAsync(string campus_id, string department_id)
        {
            ACTerm term = await _db.cau_tem_documents.Where(f => f.campus == campus_id &&
                                                                 f.department == department_id &&
                                                                 f.startDate <= DateTime.Today &&
                                                                 f.endDate >= DateTime.Today).FirstOrDefaultAsync();
            if (term == null)
            {
                throw new Exception(
                    $"No hay Periodo en curso para el campus: {campus_id} y la modalidad: {department_id}");
            }

            return term.term;
        }

        /// <summary>
        /// Periodo actual de un determinado campus/department
        /// </summary>
        /// <param name="campus_id"></param>
        /// <returns></returns>
        public ACTerm getCurrent(string campus_id, string department_id)
        {
            ACTerm term = _db.cau_tem_documents.FirstOrDefault(f => f.campus == campus_id &&
                                                                    f.department == department_id &&
                                                                    f.startDate <= DateTime.Today &&
                                                                    f.endDate >= DateTime.Today);
            if (term == null)
            {
                throw new Exception(
                    $"No hay Periodo en curso para el campus: {campus_id} y la modalidad: {department_id}");
            }
            return term;
        }

        /// <summary>
        /// Período actual para mostrar horario de clases
        /// </summary>
        /// <param name="campusId">Código de sede/filial</param>
        /// <param name="departmentId">Modalidad</param>
        /// <returns></returns>
        public ACTerm GetCurrenTermForSchedule(string campusId, string departmentId)
        {
            var today = DateTime.Today;
            var tomorrow = today.AddDays(1);
            var after10Days = today.AddDays(10);
            var currentTerm = _db.cau_tem_documents
                .Where(w => w.campus == campusId && w.department == departmentId &&
                            w.startDate <= after10Days && tomorrow <= w.endDate)
                .OrderByDescending(o => o.startDate).FirstOrDefault();

            return currentTerm;
        }

        /// <summary>
        /// Periodo anterior actual de un determinado campus/department
        /// </summary>
        /// <param name="campus_id"></param>
        /// <returns></returns>
        public ACTerm getLast(string campus_id, string department_id)
        {
            ACTerm current_term = getCurrent(campus_id, department_id);
            ACTerm term = _db.cau_tem_documents.Where(f =>
                    f.campus == campus_id
                    && f.department == department_id
                    && f.term != current_term.term
                    && f.endDate <= current_term.start_date
                ).OrderByDescending(f => f.startDate)
                .FirstOrDefault();
            if (term == null)
                throw new Exception($"no hay ultimo perido para el campus {campus_id} en {department_id}");
            return term;
        }

        /// <summary>
        /// Periodo de un determinado campus/department
        /// segun el codigo
        /// </summary>
        /// <param name="campus_id"></param>
        /// <returns></returns>
        public ACTerm get(string term_id, string campus_id, string department_id)
        {
            ACTerm term = _db.cau_tem_documents.FirstOrDefault(f => f.campus == campus_id &&
                                                                    f.department == department_id &&
                                                                    f.term == term_id);
            if (term == null)
                throw new Exception($"no hay periodo {term_id} para el campus {campus_id} en {department_id}");
            return term;
        }

        /// <summary>
        /// Obtiene el periodo segun una fecha sede y modalidad
        /// </summary>
        /// <param name="date"></param>
        /// <param name="campus_id"></param>
        /// <param name="department_id"></param>
        /// <returns></returns>
        public ACTerm getByDate(DateTime date, string campus_id, string department_id)
        {
            ACTerm term = _db.cau_tem_documents.FirstOrDefault(f => f.campus == campus_id &&
                                                                    f.department == department_id &&
                                                                    f.startDate <= date &&
                                                                    f.endDate >= date);
            if (term == null)
            {
                throw new Exception(
                    $"no hay perido para la fecha {date: dd/MM/yyy} en el campus {campus_id} de {department_id}");
            }

            return term;
        }

        /// <summary>
        /// Info de un período académico según campus (IDSede) y department (IDEscuelaADM) 
        /// </summary>
        /// <param name="academicPeriod">Período académico (notación BANNER)</param>
        /// <param name="academicProfile">perfil académico del estudiante</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static async Task<ACTerm> GetByAcademicPeriodCampusAndDepartament(string academicPeriod,
            AcademicProfile academicProfile)
        {
            var bnContext = new BNContext();
            var dboContext = new DBOContext();

            var academicPeriodBanner = academicPeriod;
            var academicPeriodApec = Term.toAPEC(academicPeriod, academicProfile.department);

            ACTerm acTerm;

            if (int.Parse(academicPeriod) >= 201710)
            {
                // banner
                acTerm = await bnContext.tbl_SFRSTCR.Where(w =>
                        w.SFRSTCR_PIDM == academicProfile.person_id && w.SFRSTCR_TERM_CODE == academicPeriodBanner)
                    .Select(s => new ACTerm
                    {
                        term = s.SFRSTCR_TERM_CODE,
                        start_classes_date = s.SFRSTCR_ADD_DATE
                    }).OrderBy(o => o.start_classes_date).FirstOrDefaultAsync();
            }
            else
            {
                // apec
                var studentCode = await dboContext.view_alumno_basico
                    .Where(w => w.IDPersonaN == academicProfile.person_id)
                    .Select(s => s.IDAlumno).Distinct().ToListAsync();

                acTerm = await dboContext.tbl_matricula_doc.Where(w => studentCode.Contains(w.IDAlumno) &&
                                                                       w.IDPerAcad == academicPeriodApec &&
                                                                       w.IDtipoMatricula == "MATRI")
                    .Select(s =>
                        new ACTerm
                        {
                            term = s.IDPerAcad,
                            campus_id = s.IDSede,
                            start_classes_date = s.fecha ?? DateTime.Now
                        }).FirstOrDefaultAsync();
            }

            if (acTerm == null)
            {
                throw new Exception(
                    $"No hay info para el periodo {academicPeriod} en el campus {academicProfile.campus.id} " +
                    $"de {academicProfile.department}");
            }

            return acTerm;
        }

        /// <summary>
        /// Retorna el periodo actual segun campus y departamento
        /// comparando su inicio y fin de clases
        /// retornara NULL si no hay periodo
        /// </summary>
        /// <param name="campusId"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        public ACTerm getCurrentEnrollment(string campusId, string department)
        {
            ACTerm term = _db.cau_tem_documents.Where(f => 
                    f.campus == campusId &&
                    f.department == department &&
                    f.startClassesDate <= DateTime.Today &&
                    f.endClassesDate >= DateTime.Today
                ).FirstOrDefault();
            return term;
        }

        /// <summary>
        /// Retorna el periodo actual segun campus y departamento
        /// comparando su inicio y fin de matricula
        /// retornara NULL si no hay periodo
        /// </summary>
        /// <param name="campusId"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        public ACTerm getCurrentEnrollmentByFechEnroll(string campusId, string department)
        {
            ACTerm term = _db.cau_tem_documents.Where(f =>
                    f.campus == campusId &&
                    f.department == department &&
                    f.startEnrollDate <= DateTime.Today &&
                    f.endEnrollDate >= DateTime.Today
                ).FirstOrDefault();
            if (term == null)
                throw new Exception("Las matrículas para el periodo actual ya finalizaron");
            return term;
        }


        /// <summary>
        /// Periodo actual de examen sustitutorio de un determinado campus
        /// </summary>
        /// <param name="campusId">Sede</param>
        /// <param name="departmentId">Modalidad</param>
        /// <returns></returns>
        public ACTerm getCurrentSusbtitute(string campusId, string departmentId)
        {
            ACTerm term = _db.cau_tem_documents.FirstOrDefault(f => f.campus == campusId &&
                                                                    f.department == departmentId &&
                                                                    f.startSubstituteExamDate <= DateTime.Today &&
                                                                    f.endSubstituteExamDate >= DateTime.Today);
            if (term == null)
            {
                throw new Exception(
                    $"No hay Periodo en curso para examen sustitutorio para el campus: {campusId} y la modalidad: {departmentId}");
            }
            return term;
            
        }
        #endregion methods

        #region static methods

        /// <summary>
        /// Lista los periodos de constancias
        /// </summary>
        /// <returns>term list</returns>
        public static async Task<List<ACTerm>> query()
        {
            NEXOContext db = new NEXOContext();

            List<CAUTblTermDocument> terms = await db.cau_tem_documents.OrderByDescending(f => f.term).ThenBy(f => f.campus)
                .ThenBy(f => f.startDate).ToListAsync();
            List<ACTerm> document_terms = terms.Select<CAUTblTermDocument, ACTerm>(f => f).ToList();

            return document_terms;
        }

        /// <summary>
        /// Obtiene los períodos disponibles según su fecha de matrícula disponible,
        /// modalidad y su campus
        /// </summary>
        /// <returns></returns>
        public static List<ACTerm> getTermByFech(string department, string campus)
        {
            NEXOContext db = new NEXOContext();
            DateTime today = DateTime.Now;
            List<ACTerm> terms = db.cau_tem_documents.Where(f => 
                                    f.department == department
                                    && f.campus == campus
                                    && f.startEnrollDate <= today
                                    && f.endEnrollDate >= today
                                    ).OrderBy(f => f.term).ToList()
                                .Select<CAUTblTermDocument, ACTerm>(f => f).ToList();

            return terms;
        }

        /// <summary>
        /// Cambia los periodos verano YYYY00 y virtual YYYY30 (notación BANNER)
        /// o períodos verano YYYY-0 y virtual YYYY-3 (notación APEC)
        /// por periodos regulares para documentos oficiales
        /// </summary>
        /// <param name="term">Período académico</param>
        /// <returns>Clean term</returns>
        public static string cleanForDocuments(string term)
        {
            var academicPeriodBanner = Term.toBanner(term);
            string termForDocs;
            switch (academicPeriodBanner.Substring(4, 2))
            {
                case "00":
                    termForDocs = $"{academicPeriodBanner.Substring(0, 4)}10";
                    break;
                case "30":
                    termForDocs = $"{academicPeriodBanner.Substring(0, 4)}20";
                    break;
                default:
                    termForDocs = academicPeriodBanner;
                    break;
            }

            return termForDocs;
        }

        #endregion static methods

        #region implicite operators

        public static implicit operator ACTerm(CAUTblTermDocument obj)
        {
            if (obj == null) return null;
            var term = new ACTerm
            {
                term = obj.term,
                part_term = obj.partTerm,
                campus_id = obj.campus,
                department = obj.department,
                start_date = obj.startDate,
                end_date = obj.endDate,
                start_classes_date = obj.startClassesDate,
                end_classes_date = obj.endClassesDate,
                start_enroll_date = obj.startEnrollDate,
                end_enroll_date = obj.endEnrollDate,
                start_substitute_exam_date = obj.startSubstituteExamDate,
                end_substitute_exam_date = obj.endSubstituteExamDate
            };
            return term;
        }

        #endregion implicite operators
    }
}