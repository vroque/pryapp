﻿using ACBusiness.Academic;
using ACBusiness.Accounts;
using ACBusiness.AtentionCenter.Documents;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using ACTools.Configuration;
using ACTools.PDF;
using CABusiness.AtentionCenter.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Threading.Tasks;
using ACBusiness.LanguagesCenter.Documents;
using StudiesCertificate = ACBusiness.AtentionCenter.Documents.StudiesCertificate;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _inst = ACTools.Constants.InstitucionalConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _util = ACTools.Util;
using ACBusiness.AtentionCenter.Documents.Graduates;

namespace ACBusiness.AtentionCenter
{
    /// <summary>
    /// Documento
    /// Documento generado mediante solicitudes del 
    /// centro de antención al usuario
    /// </summary>
    public class ACDocument
    {
        #region properties

        public int id { get; set; }

        public string name { get; set; }

        // Properties for common use      
        protected decimal person_id { get; set; }
        public bool is_valid { get; set; }
        public DocumentCost cost { get; set; }
        protected AcademicProfile profile { get; set; }
        public List<string> stack_error { get; set; }
        public List<ACDocumentState> flow { get; set; }

        protected NEXOContext _db = new NEXOContext();

        #endregion properties

        #region methods

        /// <summary>
        /// Carga los datos generales de el documento
        /// </summary>
        /// <param name="id">ID del Documento</param>
        /// <param name="profile">
        /// Perfil del alumno que pide el documento
        /// </param>
        protected void sync(int id, AcademicProfile profile)
        {
            ACDocument doc = ACDocument.get(id);
            this.id = doc.id;
            this.name = doc.name;
            this.person_id = profile.person_id;
            this.profile = profile;
            this.cost = this.getCost();
            this.flow = this.getFlow();
            this.stack_error = new List<string>();
        }

        /// <summary>
        /// Hace las comprovaciones para definir si el usaurio
        /// puede procesar cualquier documento
        /// ** si el usaurio no cumple con la validación
        /// igualmente se genera la solicitus pero esta se 
        /// cancela automaticamente.
        /// (por temas legales no se le puede impedir realizar una solicitud)
        /// </summary>
        /// <returns></returns>
        public async Task<bool> validate()
        {
            this.is_valid = true;
            stack_error = new List<string>();
            Debt debt_object = new Debt();
            var debts = await debt_object.query(profile.person_id);
            var reqs = profile.requeriments();

            if (debts.Count() > 0)
            {
                foreach (Debt debt in debts)
                {
                    stack_error.Add(string.Format("Deuda: {0} [S/. {1}]",
                        debt.description,
                        debt.ammount
                    ));
                }
            }

            if (reqs.Count() > 0)
            {
                foreach (var req in reqs)
                {
                    stack_error.Add(string.Format("Requisito: {0}",
                        req.name
                    ));
                }
            }

            if (debts.Count > 0 || reqs.Count > 0)
            {
                this.is_valid = false;
            }

            return this.is_valid;
        }

        /// <summary>
        /// <para>Crea la solicitud para este documento</para>
        /// <para>Uso General:</para>
        /// * Los documentos se inician:
        ///   -> En el estado INICIO si cumplen la validacion
        ///   -> Aprobado es verdadero
        ///   -> Fecha de entrega es 3 días después
        ///   -> Documento número es igual a 1
        ///   -> Oficina es el Centro de Atención
        /// de lo contrario de inician en estado CANCELADO
        /// <para>Uso Especial:</para>
        /// Para casos en los que se requiera que no se realice la validación de requisitos withValidation = false.
        /// p.e.: Constancias de Ingreso relacionados al proceso de trámite de bachiller (se requiere de una 
        /// solicitud para tener un registro de la generación de esta constancia como parte del trámite de bachiller)
        /// </summary>
        /// <param name="student">Datos básicos del estudiante</param>
        /// <param name="profile">Perfil académico del estudiante</param>
        /// <param name="child_is_valid"></param>
        /// <param name="with_validation">¿Se debe de realizar una validación de requisitos?</param>
        /// <returns>Objeto de solicitud</returns>
        public virtual async Task<ACRequest> composeRequest(Student student, AcademicProfile profile,
            bool child_is_valid = true, bool with_validation = true)
        {
            ACTerm ac_term = new ACTerm();
            ACRequest request = new ACRequest
            {
                //id= "";
                divs = profile.div,
                campus = profile.campus != null ? profile.campus.id : string.Empty,
                program = profile.program.id,
                person_id = student.person_id,
                student_id = student.id,
                document_number = 1,
                relationship_id = 9, // Estático hasta implementarlo o eliminarlo
                student_name = $"{student.last_name}, {student.first_name}",
                delivery_date = DateTime.Now.AddDays(3), // estático hasta implementarlo
                office_user_id = 0,
                office_id = _inst.OFFICE_CAU, // Centro de Atención
                term = profile.campus != null
                    ? await ac_term.getCurrentAsync(profile.campus.id, profile.department)
                    : string.Empty,
                aproved = true,
                update_date = DateTime.Now,
                creation_date = DateTime.Now,
                department = profile.department
            };

            bool is_valid;
            if (with_validation)
            {
                is_valid = await this.validate();
            }
            else
            {
                is_valid = true;
            }

            /**
             * NOTE: No se debe de restringir el trámite documentario por deuda ni documentos a estudiantes PRONABEC
             * Lo ideal sería comparar profile.adm_type.is_pronabec pero todos los registros de 
             * DIM.tblAdmType.admPronabec tienen valor 0 (incluidos los de PRONABEC)
             */

            if ((is_valid && child_is_valid) || profile.adm_type.is_pronabec)
                request.state = _ate.STATE_INITIAL; // ESTADO INICIAL
            else
                request.state = _ate.STATE_CANCEL; // CANCELADO
            return request;
        }

        /// <summary>
        /// Calcula el monto de la solicitud para este documento
        /// </summary>
        /// <returns></returns>
        private DocumentCost getCost()
        {
            //parametros y modo de calcular el monto por definir 
            DocumentCost dc = DocumentCost.Get(this.id, this.profile);
            return dc;
        }

        /// <summary>
        /// Obtiene el flujo para este documento
        /// </summary>
        /// <returns></returns>
        private List<ACDocumentState> getFlow()
        {
            ACDocumentState state = new ACDocumentState();
            return state.flow(this.id);
        }

        /// <summary>
        /// Generar Documento:
        /// Sirve de punto de entrada para redirigir a los metodos propios
        /// para general el documento especificado segun su "id"
        /// y de punto de salida del path del documento en pdf 
        /// </summary>
        /// <param name="requestId"> id de la solicitud</param>
        /// <param name="user"></param>
        /// <param name="term"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public async Task<string> generateDocument(int requestId, string user, string term, int index, bool validateFlow = true)
        {
            var request = await ACRequest.get(requestId);
            //validar que no este en un estado en el que no se pueda generar
            if (request.state == _ate.STATE_CANCEL || request.state == _ate.STATE_INITIAL ||
                request.state == _ate.STATE_PAYMENT_PENDING)
            {
                throw new Exception("El estado de la solicitud no permite generar el documento.");
            }

            var documentId = request.document_id;
            //Validar existencia de un PDF
            var pdf = $"{request.id}-{index}-{request.student_id}";

            if (request.divs == _inst.CIC)
            {
                if (_util.LanguagesCenter.ExistDocument(pdf))
                {
                    var url = $"{AppSettings.reports[_cic.SRC_CIC_PDF]}{pdf}";
                    return url;
                }

                if (_util.LanguagesCenter.ExistCaieDocument(pdf))
                {
                    var url = $"{AppSettings.reports[_cic.SRC_CAIE_PDF]}{pdf}";
                    return url;
                }
            }

            if (PDFGenerator.exists(pdf))
            {
                var uri = $"{AppSettings.reports["output-client"]}{pdf}/pdf/";
                return uri;
            }

            var student = new Student(request.person_id);

            //refactorizar 
            var profile = student.profiles
                .FirstOrDefault(f => f.program.id == request.program &&
                                     f.campus.id == request.campus && // FIXME change for program
                                     f.div == request.divs);
            student.profile = profile;
            string documentPath = null;
            // Router -> Rutea los documentos por sus id y trae el str del pdf
            switch (documentId)
            {
                case _ate.DOC_STUDIES_PROOF_WITH_DATE:
                case _ate.DOC_STUDIES_PROOF:
                    var studiesProof = new StudiesProof(profile);
                    documentPath = await studiesProof.generate(request, student, user, term, index);
                    break;
                case _ate.DOC_AVERAGE_STUDIES_PROOF:
                    var studiesProofAvg = new StudiesProofAvg(profile);
                    documentPath = await studiesProofAvg.generate(request, student, user);
                    break;
                case _ate.DOC_CUSTOM_STUDIES_PROOF:
                    /*
                    StudiesProofCustom studies_proof_custom = new StudiesProofCustom(profile);
                    document_path = await studies_proof_custom.generate(request, student, user);                    
                    */
                    throw new Exception("Registros academicos se encarga de generar este documento");
                case _ate.DOC_CULM_CAREER_STUDIES_PROOF:
                    var studiesProofCareer = new GraduationProof(profile);
                    documentPath = await studiesProofCareer.generate(request, student, user);
                    break;
                case _ate.DOC_ENTRY_PROOF:
                    var entryProof = new EntryProof(profile);
                    documentPath = await entryProof.generate(request, student, user);
                    break;
                case _ate.DOC_STUDIES_CERTIFICATE:
                    var studiesCertificate = new StudiesCertificate(profile);
                    documentPath = await studiesCertificate.generate(request, student, user);
                    break;
                case _ate.DOC_PROMOTIONAL_STUDIES_CERTIFICATE:
                    documentPath = await PromotionalStudyCertificate.GenerateAsync(request, student, user);
                    break;
                case _ate.DOC_REPORT_CARD:
                    //verificar periodo e indice
                    var reportCard = new ReportCard(profile);
                    documentPath = await reportCard.generate(request, student, user, term, index);
                    break;
                case _ate.DOC_3510SUPERIOR_PROOF:
                    var proof3510ByPeriod = new ProofOf3510PerPeriod(profile);
                    documentPath = await proof3510ByPeriod.generate(request, student, user, term, index, validateFlow);
                    break;
                case _ate.DOC_PROMOTIONAL_3510SUPERIOR_PROOF:
                    var proof3510Prom = new ProofOf3510Promotional(profile);
                    documentPath = await proof3510Prom.generate(request, student, user, term, index, validateFlow);
                    break;
                case _ate.DOC_ACADEMIC_HISTORY:
                    var officialTranscript = new OfficialTranscript(profile);
                    documentPath = await officialTranscript.generate(request, student, user, validateFlow);
                    break;
                case _ate.DOC_ENROLLMENT_PROOF:
                    var enrollmentProof = new EnrollmentProof(profile);
                    documentPath = await enrollmentProof.generate(request, student, user);
                    break;
                case _ate.DOC_ENROLLMENT_PROOF_WITH_DATE:
                    var enrollmentProofDate = new EnrollmentProofWithDate(profile);
                    documentPath = await enrollmentProofDate.generate(request, student, user);
                    break;
                case _ate.DOC_CONVALIDATION:
                    var convalidation = new ConvalidationResolution(profile);
                    documentPath = await convalidation.generate(request, student, user);
                    break;
                case _ate.DOC_CONVALIDATION_EXTENSION:
                    var convalidationEx = new ConvalidationExtensionResolution(profile);
                    documentPath = await convalidationEx.generate(request, student, user);
                    break;
                case _ate.DOC_CIC_PROOF_OF_ENROLLMENT:
                    var cicProofOfEnrollment = new ProofOfEnrollment();
                    documentPath = await cicProofOfEnrollment.GenerateDocumentAsync(request, student, user);
                    break;
                case _ate.DOC_CIC_STUDIES_CERTIFICATE:
                    var cicStudiesCertificate = new LanguagesCenter.Documents.StudiesCertificate();
                    documentPath = await cicStudiesCertificate.GenerateDocumentAsync(request, student, user);
                    break;
                case _ate.DOC_CIC_DIPLOMA:
                    var cicDiploma = new Diploma();
                    documentPath = await cicDiploma.GenerateDocumentAsync(request, student, user);
                    break;
                case _ate.DOC_CIC_CAIE_EXT:
                    var caie = new ProofOfForeignLanguage();
                    documentPath = await caie.GenerateDocumentAsync(request, student, user);
                    break;
                case _ate.DOC_DUPLICATE_PROOF_GRADUATE:
                    var duplicateProofGraduate = new DuplicateProofGraduate(profile);
                    documentPath = await duplicateProofGraduate.generate(request, student, user);
                    break;
                default:
                    throw new ServerException("Tipo de documento desconocido");
            }

            if (documentPath == null)
                throw new Exception("No se pudo Generar el archivo PDF");
            return documentPath;
        }

        #endregion methods

        #region static methods

        public static ACDocument get(int id)
        {
            NEXOContext db = new NEXOContext();
            ACDocument doc = db.cau_documents.FirstOrDefault(f => f.document == id);
            return doc;
        }

        #endregion static methods

        #region implicit operators

        public static implicit operator ACDocument(CAUTblDocument obj)
        {
            ACDocument doc = new ACDocument();
            doc.id = obj.document;
            doc.name = obj.documentName;
            return doc;
        }

        #endregion implicit operators
    }
}