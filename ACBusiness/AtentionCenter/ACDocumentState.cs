﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace ACBusiness.AtentionCenter
{
    /// <summary>
    /// Estados por los cuales para un documento
    /// determinado
    /// </summary>
    public class ACDocumentState
    {
        #region properties
        private int document_id { get; set; }
        public int state_id { get; set; }
        public int order { get; set; }
        public int next { get; set; }
        public int office_id { get; set; }
        private NEXOContext _db = new NEXOContext();
        #endregion properties

        #region methods

        /// <summary>
        /// Flujo de estados validos para un documento
        /// </summary>
        /// <param name="document_id">ID del Documento</param>
        /// <returns></returns>
        public List<ACDocumentState> flow(int document_id)
        {
            _db = new NEXOContext();
            List<CAUTblDocumentState> flow_tbl = _db.cau_document_states
                .Where(f => f.documentID == document_id).ToList();
                //.Where(f => f.IDDocumento == document_id).ToList();//&& f.order != null

            List<ACDocumentState> flow = flow_tbl
                .Select<CAUTblDocumentState, ACDocumentState>(f => f)
                .OrderBy(f => f.order)
                .ToList();
            //List<ACDocumentState> flow = new List<ACDocumentState>();
            return flow;
        }
        #endregion methods

        #region implicite operators
        public static implicit operator ACDocumentState(CAUTblDocumentState obj)
        {
            ACDocumentState docest = new ACDocumentState();
            docest.document_id = obj.documentID;
            docest.state_id = obj.state;
            docest.next = obj.nextState;
            docest.order = obj.order;
            docest.office_id = obj.officeID;
            return docest;
        }
        #endregion implicite operators
    }
}
