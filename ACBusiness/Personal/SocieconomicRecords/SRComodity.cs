﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Personal.SocieconomicRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 18/06/2016</date>
    /// <summary>
    /// Modulo de comodidades del hogar para la Ficha Socieconomica
    /// </summary>
    public class SRComodity
    {
        #region properties
        public List<string> appliances { get; set; }
        public List<string> services { get; set; }
        #endregion

        #region functions
        public SRComodity() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static SRComodity get(decimal person_id, string version)
        {
            SRProto<SRComodity> soc = new SRProto<SRComodity>();
            SRComodity comodity = soc.get(new SRComodity(), "comodity", person_id, version);
            return comodity;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public int save(decimal person_id, string version)
        {
            SRProto<SRComodity> soc = new SRProto<SRComodity>();
            int r = soc.save(this, "comodity", person_id, version);
            return r;
        }
        #endregion
    }
}
