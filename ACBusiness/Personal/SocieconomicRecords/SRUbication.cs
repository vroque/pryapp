﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;

namespace ACBusiness.Personal.SocieconomicRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 14/06/2016</date>
    /// <summary>
    /// Modulo de Ubicación para la Ficha Socieconomica
    /// </summary>
    public class SRUbication
    {

        #region Properties

        public string origin_country { get; set; }
        public string origin_ubigeo { get; set; }
        public string origin_address { get; set; }
        public string referency { get; set; }
        public string reference_phone { get; set; }
        public string reference_person { get; set; }
        public string local_phone { get; set; }
        public string celphone { get; set; }
        public string celphone2 { get; set; }
        public string celphone3 { get; set; }
        public string email { get; set; }
        public string email2 { get; set; }
        public string email3 { get; set; }
        public string has_recurrent { get; set; }
        public string recurrent_country { get; set; }
        public string recurrent_ubigeo { get; set; }
        public string recurrent_address { get; set; }
        public string recurrent_referency { get; set; }
        #endregion

        #region functions
        public SRUbication() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static SRUbication get(decimal person_id, string version)
        {
            SRProto<SRUbication> soc = new SRProto<SRUbication>();
            SRUbication ubication = soc.get(new SRUbication(), "ubication", person_id, version);
            return ubication;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public int save(decimal person_id, string version)
        {
            SRProto<SRUbication> soc = new SRProto<SRUbication>();
            int r = soc.save(this, "ubication", person_id, version);
            return r;
        }
        #endregion
    }


}
