﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Personal.SocieconomicRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 18/06/2016</date>
    /// <summary>
    /// Modulo laboral para la Ficha Socieconomica
    /// </summary>
    public class SRHealth
    {
        #region properties
        public string hospital { get; set; }
        public string other { get; set; }
        public string disability { get; set; }
        public string disability_type { get; set; }
        public string has_disease { get; set; }
        public string disease { get; set; }
        public string has_familiar_disease { get; set; }
        public List<FamiliarDisease> familiar_disease { get; set; }
        #endregion
        #region functions
        public SRHealth() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static SRHealth get(decimal person_id, string version)
        {
            SRProto<SRHealth> soc = new SRProto<SRHealth>();
            SRHealth health = soc.get(new SRHealth(), "health", person_id, version);
            return health;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public int save(decimal person_id, string version)
        {
            SRProto<SRHealth> soc = new SRProto<SRHealth>();
            int r = soc.save(this, "health", person_id, version);
            return r;
        }
        #endregion
    }

    public struct FamiliarDisease
    {
        public string person_id { get; set; }
        public string disease { get; set; }

        public static implicit operator String(FamiliarDisease obj)
        {
            obj.disease = obj.disease.Replace(':', ' ').Replace(',',' ');
            return string.Format("{0}:{1}", obj.person_id, obj.disease);
        }
    }
}
