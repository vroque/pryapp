﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Personal.SocieconomicRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 18/06/2016</date>
    /// <summary>
    /// Modulo Academico para la Ficha Socieconomica
    /// </summary>
    public class SRFamiliar
    {
        #region properties
        public string father { get; set; }
        public string mother { get; set; }
        public string parent_status { get; set; }
        public string head_family { get; set; }
        public string head_family_other { get; set; }
        public List<string> lives_with { get; set; }
        public string economic_status { get; set; }
        public string dependes_on { get; set; }
        public string independet_support { get; set; }
        public string dependent_support { get; set; }
        public string economic_providers { get; set; }
        public string economic_providers_brothers { get; set; }
        public string economic_depends { get; set; }
        public string economic_depends_brothers { get; set; }

        #endregion properties

        #region functions
        public SRFamiliar() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static SRFamiliar get(decimal person_id, string version)
        {
            SRProto<SRFamiliar> soc = new SRProto<SRFamiliar>();
            SRFamiliar familiar = soc.get(new SRFamiliar(), "familiar", person_id, version);
            return familiar;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public int save(decimal person_id, string version)
        {
            SRProto<SRFamiliar> soc = new SRProto<SRFamiliar>();
            int r = soc.save(this, "familiar", person_id, version);
            return r;
        }
        #endregion
    }
}
