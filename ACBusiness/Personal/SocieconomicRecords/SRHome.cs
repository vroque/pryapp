﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Personal.SocieconomicRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 21/06/2016</date>
    /// <summary>
    /// Modulo Vivienda para la Ficha Socieconomica
    /// </summary>
    public class SRHome
    {
        #region properties
        public string home_type { get; set; }
        public string home_status { get; set; }
        public string wall_material { get; set; }
        public string roof_material { get; set; }
        public string floor_material { get; set; }

        #endregion
        #region functions
        public SRHome() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static SRHome get(decimal person_id, string version)
        {
            SRProto<SRHome> soc = new SRProto<SRHome>();
            SRHome home = soc.get(new SRHome(), "home", person_id, version);
            return home;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public int save(decimal person_id, string version)
        {
            SRProto<SRHome> soc = new SRProto<SRHome>();
            int r = soc.save(this, "home", person_id, version);
            return r;
        }
        #endregion
    }
}
