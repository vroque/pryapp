﻿using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.Contexts.BDUCCI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACTools;
using ACTools.SuperString;

namespace ACBusiness.Personal.SocieconomicRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 30/06/2016</date>
    /// <summary>
    /// Relaciones familiares para la Ficha Socieconomica
    /// </summary>
    public class SRFamiliarRelation
    {
        #region properties
        public decimal person_id { get; set; }
        public string economic_type { get; set; }
        public string relation { get; set; }
        public string other_relation { get; set; }
        // Personal details
        public string document_type { get; set; }
        public string document_number { get; set; }
        public string father_name { get; set; }
        public string mother_name { get; set; }
        public string first_name { get; set; }
        //conit
        public string conti_student { get; set; }
        public string conti_pension { get; set; }
        // Academic details
        public string college_type { get; set; }
        public string last_grade { get; set; }
        // Ubication details
        public string birth_country { get; set; }
        public string ubigeo { get; set; }
        public string origin_address { get; set; }
        public string referency { get; set; }
        // Contact details
        public string local_phone { get; set; }
        public string reference_phone { get; set; }
        public string reference_person { get; set; }
        public string celphone { get; set; }
        public string celphone2 { get; set; }
        public string celphone3 { get; set; }
        public string email { get; set; }
        public string email2 { get; set; }
        // Work details
        public string has_work { get; set; }
        public string category_dep { get; set; }
        public string category_ind { get; set; }
        public string category_cj { get; set; }
        // depend
        public string dep_company { get; set; }
        public string dep_type { get; set; }
        public string dep_activity { get; set; }
        public string dep_position { get; set; }
        public int dep_service_time { get; set; }
        // indpepend
        public string ind_actual_work { get; set; }
        public string ind_activity { get; set; }
        public string ind_has_ruc { get; set; }
        public string ind_ruc { get; set; }
        public int ind_workers { get; set; }
        // CJ
        public string cj_cesante { get; set; }
        public string cj_company { get; set; }
        public int cj_service_time { get; set; }
        // Complementary work
        public string has_work_complementary { get; set; }
        public string com_name { get; set; }
        

        #endregion properties

        #region functions
        public SRFamiliarRelation() { }
        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static SRFamiliarRelation get(decimal person_id, string version)
        {
            SRProto<SRFamiliarRelation> soc = new SRProto<SRFamiliarRelation>();
            SRFamiliarRelation familiar = soc.get(new SRFamiliarRelation(), "relations", person_id, version);
            return familiar;
        }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static List<SRFamiliarRelation> list(decimal person_id, string version)
        {
            // cargamos las relaciones del alumno
            DBOContext db = new DBOContext();
            List<DBOTblRelacionFamiliar> relations = db.tbl_relacion_familiar
                .Where(f => f.persona == person_id.ToString()).ToList();
            // cargamos a la familia segun las relaciones
            List<SRFamiliarRelation> familiars = new List<SRFamiliarRelation>();
            SRProto<SRFamiliarRelation> soc = new SRProto<SRFamiliarRelation>();

            foreach (DBOTblRelacionFamiliar fr in relations)
            {
                decimal family_id = decimal.Parse(
                    fr.familiar.Substring(1, fr.familiar.Length -1)
                );
                SRFamiliarRelation relation = soc.get(new SRFamiliarRelation(), "relations", family_id, version);
                familiars.Add(relation);
            }
            
            return familiars;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public decimal save(decimal person_id, string version)
        {
            this.father_name = this.father_name.ToUpper();
            this.mother_name = this.mother_name.ToUpper();
            this.first_name = this.first_name.ToUpper();
            //verificar si la persona existe
            if (this.person_id == 0)
            {
                //si la persona no existe creamos la relación
                DBOContext db = new DBOContext();
                DBOTblRelacionFamiliar relation = new DBOTblRelacionFamiliar
                {
                    persona = person_id.ToString(),
                    familiar = "",
                    relacion = this.relation
                };
                db.tbl_relacion_familiar.Add(relation);
                db.SaveChanges();
                relation.familiar = "p" + relation.id.ToString().completeZeros(7);
                db.SaveChanges();
                this.person_id = relation.id; // decimal.Parse(relation.familiar);
            }
            SRProto<SRFamiliarRelation> soc = new SRProto<SRFamiliarRelation>();
            int r = soc.save(this, "relations", this.person_id, version);
            return this.person_id;
        }
        /// <summary>
        /// Eliminar la relación Familiar
        /// **no eliminar los datos del familiar creado
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="familiar_id"></param>
        /// <returns></returns>
        public static decimal remove(decimal person_id, decimal familiar_id)
        {
            DBOContext db = new DBOContext();
            string family_id = "p" + familiar_id.ToString().completeZeros(7);
            DBOTblRelacionFamiliar relation = db.tbl_relacion_familiar.FirstOrDefault(
                f => f.persona == person_id.ToString()
                    && f.familiar == family_id
                );
            if (relation != null)
            {
                db.tbl_relacion_familiar.Remove(relation);
                db.SaveChanges();
                return 0;
            }

            return person_id;
        }
        #endregion
    }
}
