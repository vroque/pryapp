﻿using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.Contexts.BDUCCI;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System;
using ACTools.SuperString;

namespace ACBusiness.Personal.SocieconomicRecords
{
    /// <summary>
    /// Base para la interaccion  con la base de datos
    /// con las clases de la ficha socieconomica
    /// </summary>
    /// <typeparam name="T">SR Class </typeparam>
    public class SRProto<T>
    {
        
        public string type { get; set; }

        /// <summary>
        /// Obtiene los campos de la ficha segun
        /// el tipo de objeto enviado
        /// </summary>
        /// <param name="obj">SR Class </param>
        /// <param name="type"> Modulo de la ficha </param>
        /// <param name="pidm"> ID de persona</param>
        /// <param name="version"> Version de la ficha</param>
        /// <returns></returns>
        public T get(T obj, string type, decimal pidm, string version)
        {
            DBOContext db = new DBOContext();
            string person_id = pidm.ToString();
            //para familiares tomar en cuenta la P (como diferenciador del codigo de personas)
            if (obj.GetType().Name == "SRFamiliarRelation")
                person_id = "p" + pidm.ToString().completeZeros(7);
            // Conversion implicita
            List<DBOTblFichaSocEco> list = db.tbl_ficha_socioeconomica.Where(
                f => f.nivel == type
                  && f.persona == person_id
                  && f.version == version
                ).ToList();
            SRProto<T>.asingProperties(ref obj, list);

            return obj;
        }

        /// <summary>
        /// Guarda los datos de T en 
        /// la base de datos
        /// </summary>
        /// <param name="pidm">codigo de persona</param>
        /// <param name="version">version de la ficha</param>
        /// <returns>numero de objetos salvados en la BD</returns>
        public int save(T obj, string type, decimal pidm, string version)
        {
            DBOContext db = new DBOContext();
            string person_id = pidm.ToString();
            //agregar P solo en familiares
            if(obj.GetType().Name == "SRFamiliarRelation")
                person_id = "p" + pidm.ToString().completeZeros(7);

            // obtenemos los tados
            List<DBOTblFichaSocEco> lista = db.tbl_ficha_socioeconomica.Where(
                f => f.nivel == type
                  && f.persona == person_id
                  && f.version == version
                ).ToList();
            // buscar en la propiedades
            foreach (var prop in obj.GetType().GetProperties())
            {
                // buscamos la propiedad en la lista de propiedades, si existe la modificamos
                DBOTblFichaSocEco tmp = lista.FirstOrDefault(f => f.clave == prop.Name);
                string final_val = "";
                //para listas 
                if (prop.PropertyType.Name == "List`1") {
                    // lista de FamiliarDisease
                    if( prop.PropertyType.FullName.Contains("ACBusiness.Personal.SocieconomicRecords.FamiliarDisease") )
                    {
                        List<FamiliarDisease> list = (List<FamiliarDisease>)Convert.ChangeType(prop.GetValue(obj), prop.PropertyType);
                        if (list != null)
                        {
                            //convertimos las listas de tipo FamiliarDisease a cadena concatenando us propiedades
                            List<string> list2 = list.Select<FamiliarDisease, String>(x => x).ToList();
                            final_val = string.Join(",", list2);
                        }
                    }
                    else{
                        List<string> list = (List<string>)Convert.ChangeType(prop.GetValue(obj), prop.PropertyType);
                        if(list != null)
                            final_val = string.Join(",", list);
                    }
                }

                else {
                    final_val = (prop.GetValue(obj) == null) ? "" : prop.GetValue(obj).ToString();
                }
                // si existe, actualiza
                if (tmp != null)
                {
                    tmp.fecha = DateTime.Now;
                    tmp.valor = final_val;
                }
                else // sino, crea
                {
                    DBOTblFichaSocEco n = new DBOTblFichaSocEco();
                    n.nivel = type;
                    n.persona = person_id;
                    n.clave = prop.Name;
                    n.valor = final_val;
                    n.version = version;
                    n.fecha = DateTime.Now;
                    db.tbl_ficha_socioeconomica.Add(n);
                }
            }
            int r = db.SaveChanges();
            return r;
        }
        
        /// <summary>
        /// Asigna los datos de la base de datos
        /// al objeto
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="lista"></param>
        /// <returns></returns>
        public static void asingProperties(ref T obj, List<DBOTblFichaSocEco> lista)
        {
            foreach (var prop in obj.GetType().GetProperties())
            {
                //buscamos la propiedad en la lista de propiedades, si existe la anexsamos
                DBOTblFichaSocEco tmp = lista.FirstOrDefault(f => f.clave == prop.Name);
                if (tmp != null)
                {
                    if (tmp.valor != null && tmp.valor != "")
                    {
                        //para listas 
                        if (prop.PropertyType.Name == "List`1")
                        {
                            if (prop.PropertyType.FullName.Contains("ACBusiness.Personal.SocieconomicRecords.FamiliarDisease"))
                            {
                                List<String> list = tmp.valor.Split(',').ToList<string>();
                                List<FamiliarDisease> list2 = list.Select<String, FamiliarDisease>(
                                    x => new FamiliarDisease
                                    {
                                        person_id = x.Split(':')[0],
                                        disease = x.Split(':')[1]
                                    }
                                   ).ToList();
                                prop.SetValue(obj, Convert.ChangeType(list2, prop.PropertyType), null);
                            }
                            else
                            {
                                List<string> list = tmp.valor.Split(',').ToList<string>();
                                prop.SetValue(obj, Convert.ChangeType(list, prop.PropertyType), null);
                            }
                        }
                        else
                        {
                            if(tmp.nivel == "relations" && tmp.clave == "person_id" && tmp.valor.StartsWith("p"))
                            {
                                tmp.valor = tmp.valor.Substring(1, tmp.valor.Length - 1);
                            }
                            prop.SetValue(obj, Convert.ChangeType(tmp.valor, prop.PropertyType), null);
                        }

                    }
                }
            }
            //return true;
        }
    }
}
