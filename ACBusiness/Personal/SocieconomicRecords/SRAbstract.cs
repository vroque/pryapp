﻿using ACPermanence.Contexts.BDUCCI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Personal.SocieconomicRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 15/08/2016</date>
    /// <summary>
    /// Resumen del avance
    /// </summary>
    public class SRAbstract
    {
        #region Properties
        public int total_percent { get; set; }
        public string completes_module { get; set; }
        public string missing_modules { get; set; }
        #endregion
    
        #region functions
        public SRAbstract() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static SRAbstract get(decimal person_id, string version)
        {
            SRProto<SRAbstract> soc = new SRProto<SRAbstract>();
            SRAbstract abs= soc.get(new SRAbstract(), "abstract", person_id, version);
            //si no existe colegio copiarlo desde tbl persona
            
            return abs;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public int save(decimal person_id, string version)
        {
            SRProto<SRAbstract> soc = new SRProto<SRAbstract>();
            int r = soc.save(this, "abstract", person_id, version);
            return r;
        }
        #endregion
    }
}
