﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Personal.SocieconomicRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 20/06/2016</date>
    /// <summary>
    /// Modulo de ingresos para la Ficha Socieconomica
    /// </summary>
    public class SRIncome
    {
        #region properties
        public int student { get; set; }
        public int father { get; set; }
        public int mother { get; set; }
        public int spouse { get; set; }
        public int other_fam { get; set; }
        public int cj_father { get; set; }
        public int cj_mother { get; set; }
        public int widowhood { get; set; }
        public int food { get; set; }
        public int house_rent { get; set; }
        public int car_rent { get; set; }
        public int other { get; set; }
        #endregion

        #region functions
        public SRIncome() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static SRIncome get(decimal person_id, string version)
        {
            SRProto<SRIncome> soc = new SRProto<SRIncome>();
            SRIncome income = soc.get(new SRIncome(), "income", person_id, version);
            return income;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public int save(decimal person_id, string version)
        {
            SRProto<SRIncome> soc = new SRProto<SRIncome>();
            int r = soc.save(this, "income", person_id, version);
            return r;
        }
        #endregion
    }
}
