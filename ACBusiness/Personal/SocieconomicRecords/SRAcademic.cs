﻿using ACPermanence.Contexts.BDUCCI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Personal.SocieconomicRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 14/06/2016</date>
    /// <summary>
    /// Modulo Academico para la Ficha Socieconomica
    /// </summary>
    public class SRAcademic
    {
        #region Properties
        public int end_year { get; set; }
        public string last_grade { get; set; }
        public string school_name { get; set; }
        public string school_ubigeo { get; set; }
        public string school_type { get; set; }
        public int school_pension { get; set; }
        public string college_type { get; set; }
        public string college_mode { get; set; }
        public int college_pension { get; set; }
        public string college_name { get; set; }
        public string college_career { get; set; }
        public int college_duration { get; set; }
        public string first_in_college { get; set; }
        #endregion
    
        #region functions
        public SRAcademic() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static SRAcademic get(decimal person_id, string version)
        {
            SRProto<SRAcademic> soc = new SRProto<SRAcademic>();
            SRAcademic academic = soc.get(new SRAcademic(), "academic", person_id, version);

            DBOContext db = new DBOContext();
            var school_id = db.tbl_persona.FirstOrDefault(f => f.IDPersonaN == person_id).IDColegio;
            var school = db.tbl_colegio.FirstOrDefault(f => f.IDColegio == school_id);
            if (school != null)
            {
                academic.school_name = school.NomColegio;
                academic.school_type = (school.TipoColegio == "Particular") ? "Privado" : "Público";
                academic.school_ubigeo = school.IDDistrito;
            }
            return academic;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public int save(decimal person_id, string version)
        {
            SRProto<SRAcademic> soc = new SRProto<SRAcademic>();
            int r = soc.save(this, "academic", person_id, version);
            return r;
        }
        #endregion
    }
}
