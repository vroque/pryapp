﻿using ACBusiness.Personal.SocieconomicRecords;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Personal.SocieconomicRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 21/06/2016</date>
    /// <summary>
    /// Modulo gastos para la Ficha Socieconomica
    /// </summary>
    public class SRExpenses
    {
        #region properties
        public int food { get; set; }
        public int home { get; set; }
        public int services { get; set; }
        public int gas { get; set; }
        public int conti { get; set; }
        public int brothers { get; set; }
        public int sons { get; set; }
        public int materials { get; set; }
        public int housekeeper { get; set; }
        public int movility { get; set; }
        public int clothing { get; set; }
        public int salud { get; set; }
        public int bank { get; set; }
        public int other { get; set; }
        #endregion properties
        #region functions
        public SRExpenses() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static SRExpenses get(decimal person_id, string version)
        {
            SRProto<SRExpenses> soc = new SRProto<SRExpenses>();
            SRExpenses expenses = soc.get(new SRExpenses(), "expenses", person_id, version);
            return expenses;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public int save(decimal person_id, string version)
        {
            SRProto<SRExpenses> soc = new SRProto<SRExpenses>();
            int r = soc.save(this, "expenses", person_id, version);
            return r;
        }
        #endregion
    }
}
