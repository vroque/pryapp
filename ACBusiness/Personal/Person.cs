﻿using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACTools.SuperString;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ACPermanence.DataBases.DBUCCI.DBO.views;
using ACTools.strings;

namespace ACBusiness.Personal
{
    /// <author>Arturo Bolaños</author>
    /// <date>08/06/2016</date>
    /// <summary>
    /// Clase de Persona para bindear 
    /// con la base de datos
    /// </summary>
    /// <note>
    /// FIXME
    /// Se esta jalando la información desde BDUCCI para conpatibilidad de datos
    /// con la ficha socioeconomica
    /// </note>
    public class Person
    {
        #region properties

        public string document_type { get; set; }
        public string document_number { get; set; }
        public string first_name { get; set; }

        /// <summary>
        /// Apellido Paterno
        /// </summary>
        public string fathers_last_name { get; set; }

        /// <summary>
        /// Apellido Materno
        /// </summary>
        public string mothers_last_name { get; set; }

        /// <summary>
        /// Apellidos
        /// </summary>
        public string last_name { get; set; }

        public string full_name => fathers_last_name != null
            ? $"{fathers_last_name} {mothers_last_name}, {first_name}".toEachUpperClean()
            : null;

        public string address { get; set; }
        public string gender { get; set; }
        public DateTime birthdate { get; set; }
        public string country { get; set; }
        public string ubigeo { get; set; }
        public string marital_status { get; set; }

        /// <summary>
        /// PIDM
        /// </summary>
        public decimal id { get; set; }

        /// <summary>
        /// IDPersona
        /// </summary>
        public string IdPersona { get; set; }

        public string cellPhone { get; set; }
        public string rpm { get; set; }
        public string email1 { get; set; }
        public string email2 { get; set; }
        public string userName { get; set; }

        private readonly DBOContext _dboContext = new DBOContext();
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion properties

        #region Constructors

        public Person()
        {
        }

        public Person(decimal pidm)
        {
            SyncPersona(pidm);
        }

        #endregion Constructors

        #region methods

        /// <summary>
        /// Obtener Persona por codigo
        /// </summary>
        /// <param name="personId">Codigo de Persona (PIDM)</param>
        /// <returns>Person: objeto persona</returns>
        [Obsolete("Use 'new Person(pidm)' instead", true)]
        public Person get(decimal personId)
        {
            int pidm = decimal.ToInt32(personId);
            // conversion implicita
            Person person = _dboContext.tbl_persona.FirstOrDefault(f => f.IDPersonaN == pidm);
            return person;
        }

        /// <summary>
        /// Obtener Persona por Codigo de estudiante
        /// </summary>
        /// <param name="studentId">Codigo de estudiante</param>
        /// <returns>Person: objeto persona</returns>
        public Person getByStudentCode(string studentId)
        {
            DBODATAlumnoBasico student = _dboContext.view_alumno_basico.FirstOrDefault(w => w.IDAlumno == studentId);
            if (student == null) return null;

            Person person = _dboContext.tbl_persona.FirstOrDefault(w => w.DNI == student.DNI);
            return person;
        }

        public Person getByDNI(string dni)
        {
            Person person = _dboContext.tbl_persona.FirstOrDefault(w => w.DNI == dni);
            return person;
        }

        public decimal getPersonIDfromUser(string userId)
        {
            var user = _nexoContext.access_users.FirstOrDefault(f => f.userID == userId);
            return user.pidm;
        }

        public static string GetFullName(decimal pidm)
        {
            using (var dboContext = new DBOContext())
            {
                var person = dboContext.tbl_persona.AsNoTracking().FirstOrDefault(w => w.IDPersonaN == pidm);
                if (person == null) return null;
                return $"{person.Nombres} {person.Appat} {person.ApMat}";
            }
        }

        /// <summary>
        /// Creación de objeto persona
        /// </summary>
        /// <param name="pidm">PIDM</param>
        private void SyncPersona(decimal pidm)
        {
            var persona = _dboContext.tbl_persona.FirstOrDefault(w => w.IDPersonaN == pidm);

            if (persona == null) return;

            id = persona.IDPersonaN;
            address = persona.Direccion;
            birthdate = persona.FecNac ?? new DateTime(1900, 01, 01);
            country = persona.IDPais;
            ubigeo = persona.IDDistritoNac;
            marital_status = persona.EstCivil;
            fathers_last_name = persona.Appat.toEachCapitalize();
            mothers_last_name = persona.ApMat.toEachCapitalize();
            first_name = persona.Nombres.toEachCapitalize();
            last_name = fathers_last_name != null
                ? $"{fathers_last_name} {mothers_last_name}".toEachCapitalize()
                : string.Empty;
            gender = persona.Sexo;
            cellPhone = persona.Celular;
            rpm = persona.Rpm;
            email1 = persona.Email1;
            email2 = persona.Email2;
            userName = persona.IDUsuario;
            if (persona.CE != null)
            {
                document_type = "CE";
                document_number = persona.CE;
            }
            else
            {
                document_type = "DNI";
                document_number = persona.DNI;
            }
        }

        /// <summary>
        /// Creación de IDPersona de BDUCCI.dbo.tblPersona
        /// </summary>
        /// <returns></returns>
        private async Task<string> CreateIdPersona()
        {
            var regex = new Regex(@"^[a-zA-Z]$");

            var firstName = regex.IsMatch(first_name.Substring(0, 1)) ? first_name.Substring(0, 1) : "X";
            var fathersLastName = regex.IsMatch(fathers_last_name.Substring(0, 1))
                ? fathers_last_name.Substring(0, 1)
                : "X";
            var mothersLastName = regex.IsMatch(mothers_last_name.Substring(0, 1))
                ? mothers_last_name.Substring(0, 1)
                : "X";

            var initials = (firstName + fathersLastName + mothersLastName).toEachUpperClean();

            var lastRecord = await _dboContext.tbl_persona.Where(w => w.IDPersona.Substring(0, 3) == initials)
                .OrderByDescending(o => o.FechaCreacion).FirstOrDefaultAsync();

            var digit = 0;

            if (lastRecord != null)
            {
                digit = int.Parse(lastRecord.IDPersona.Substring(3, 7));
            }

            digit += 1;

            return initials + digit.ToString().PadLeft(7, '0');
        }

        /// <summary>
        /// Creación de Persona en BDUCCI.bdo.tblPersona
        /// </summary>
        /// <returns></returns>
        private async Task<int> Create(string user)
        {
            var persona = new DBOTblPersona
            {
                IDPersona = await CreateIdPersona(),
                Appat = fathers_last_name.ToUpperInvariant(),
                ApMat = mothers_last_name.ToUpperInvariant(),
                Nombres = first_name.ToUpperInvariant(),
                Sexo = gender,
                FecNac = birthdate,
                DNI = document_type == "DNI" ? document_number : "0", //string.Empty,
                CE = document_type == "CE" ? document_number : "0", //string.Empty
            };
            // TODO: FIXME:
            //            _dboContext.tbl_persona.Add(persona);
            //            return await _dboContext.SaveChangesAsync();
            // var fecha0 = persona.FecNac ?? DateTime.Now;
            var fecha = (persona.FecNac ?? DateTime.Now).ToString("yyyyMMdd");

            var registeredPerson = _dboContext.set_persona(persona.IDPersona, persona.Appat, persona.ApMat,
                persona.Nombres, persona.Sexo, fecha, persona.DNI, persona.CE).ToList();

            return registeredPerson.Any() ? 1 : 0;
        }

        /// <summary>
        /// Registro de nueva persona
        /// </summary>
        /// <param name="person">Persona a registrar</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static async Task<Person> NewAsync(Person person, string user)
        {
            var rpta = await person.Create(user);

            if (rpta <= 0) throw new Exception($"Error al guardar persona {person.document_number}");

            var dni = new[] {person.document_number};
            var persona = await SearchByDocumentNumberAsync(dni);

            return persona.FirstOrDefault();
        }


        /// <summary>
        /// Actualiza datos de una persona
        /// </summary>
        /// <returns></returns>
        public int Update()
        {
            int pidm = decimal.ToInt32(this.id);
            DBOTblPersona person = _dboContext.tbl_persona.FirstOrDefault(f => f.IDPersonaN == pidm);

            if (person == null) return 0;

            person.Direccion = this.address;
            person.FecNac = this.birthdate;
            person.IDPais = this.country;
            person.IDDistritoNac = this.ubigeo;
            person.EstCivil = this.marital_status;
            person.Appat = this.fathers_last_name;
            person.ApMat = this.mothers_last_name;
            person.Nombres = this.first_name;
            person.Sexo = this.gender;
            return _dboContext.SaveChanges();
        }

        /// <summary>
        /// Foto de persona
        /// </summary>
        /// <param name="personId">PIDM</param>
        /// <returns></returns>
        public Byte[] getPhoto(decimal personId)
        {
            DBOContext db = new DBOContext();
            DBOTblPersona person = db.tbl_persona.FirstOrDefault(p => p.IDPersonaN == personId);
            if (person == null) return null;
            if (person.Foto != null)
            {
                return person.Foto;
            }

            byte[] imageData = null;
            try
            {
                string photo_route = person.FotoRuta;
                FileInfo fileInfo = new FileInfo(photo_route);
                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(photo_route, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                imageData = br.ReadBytes((int) imageFileLength);
            }
            catch (FileNotFoundException ex)
            {
            }
            catch (IOException ex)
            {
            }
            catch (ArgumentNullException ex)
            {
            }

            return imageData;
        }

        /// <summary>
        /// Busca personas a partir de un arreglo de DNI o CE
        /// </summary>
        /// <param name="dni">DNI de personas a buscar</param>
        /// <returns></returns>
        public static async Task<List<Person>> SearchByDocumentNumberAsync(string[] dni)
        {
            var dboContext = new DBOContext();

            // Busca personas
            var basicData = await dboContext.tbl_persona.Where(w => dni.Contains(w.DNI) ||
                                                                    dni.Contains(w.CE)).ToListAsync();
            var peopleFound = basicData.Select<DBOTblPersona, Person>(s => s).ToList();

            // Personas con encontradas
            dni = basicData.Aggregate(dni, (current, tblPersona) => current.Where(w => w != tblPersona.DNI &&
                                                                                       w != tblPersona.CE).ToArray());
            var peopleNotFound = dni.Select(s => new Person
            {
                document_number = s,
            }).ToList();

            return peopleFound.Union(peopleNotFound).OrderBy(o => o.last_name).ThenBy(o => o.first_name)
                .ThenBy(o => o.document_number).ToList();
        }

        /// <summary>
        /// Obtiene el celular de una persona mediante su dni
        /// </summary>
        /// <param name="dni">Dni de la persona</param>
        /// <returns>Número de celular</returns>
        public static string GetPhone(string dni)
        {
            DBOContext dboContext = new DBOContext();
            var person = dboContext.tbl_persona.FirstOrDefault(x => x.DNI == dni);
            if (person == null)
                throw new ArgumentNullException(Errors.NO_PERSON);
            return person.Celular;
        }

        /// <summary>
        /// Número de documento de identificación (DNI o CE) de una persona
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public static async Task<string> GetDocumentNumberAsync(decimal pidm)
        {
            var dboContext = new DBOContext();
            var basicData = await dboContext.tbl_persona.Where(w => w.IDPersonaN == pidm).ToListAsync();
            return basicData.FirstOrDefault()?.DNI ?? basicData.FirstOrDefault()?.CE;
            ;
        }

        /// <summary>
        /// Número de documento de identificación (DNI o CE) de una persona
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public static string GetDocumentNumber(decimal pidm)
        {
            var dboContext = new DBOContext();
            var basicData = dboContext.tbl_persona.FirstOrDefault(w => w.IDPersonaN == pidm);
            return basicData?.DNI ?? basicData?.CE;
        }

        #endregion methods

        #region implicit operators

        public static implicit operator Person(DBOTblPersona obj)
        {
            if (obj == null) return null;
            Person person = new Person
            {
                id = (decimal) obj.IDPersonaN,
                address = obj.Direccion,
                birthdate = obj.FecNac ?? new DateTime(1900, 01, 01),
                country = obj.IDPais,
                ubigeo = obj.IDDistritoNac,
                marital_status = obj.EstCivil,
                fathers_last_name = obj.Appat.toEachCapitalize(),
                mothers_last_name = obj.ApMat.toEachCapitalize(),
                first_name = obj.Nombres.toEachCapitalize(),
                userName = obj.IDUsuario.toEachCapitalize()
            };
            person.last_name = person.fathers_last_name != null
                ? $"{person.fathers_last_name} {person.mothers_last_name}".toEachCapitalize()
                : string.Empty;
            person.gender = obj.Sexo;
            person.cellPhone = obj.Celular;
            person.rpm = obj.Rpm;
            person.email1 = obj.Email1;
            person.email2 = obj.Email2;
            person.userName = obj.IDUsuario;

            if (obj.CE != null)
            {
                person.document_type = "CE";
                person.document_number = obj.CE;
            }
            else
            {
                person.document_type = "DNI";
                person.document_number = obj.DNI;
            }

            return person;
        }

        #endregion implicit operators
    }
}