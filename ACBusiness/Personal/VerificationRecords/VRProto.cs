﻿using ACBusiness.Personal.SocieconomicRecords;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACTools.SuperString;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using _per = ACTools.Constants.PersonalConstants;

namespace ACBusiness.Personal.VerificationRecords
{
    /// <summary>
    /// Base para la interaccion  con la base de datos
    /// con las clases de la ficha socieconomica
    /// </summary>
    /// <typeparam name="T">SR Class </typeparam>
    public class VRProto<T>
    {
        /// <summary>
        /// Obtiene los campos de la ficha segun
        /// el tipo de objeto enviado
        /// </summary>
        /// <param name="obj">VR Class </param>
        /// <param name="type"> Modulo de la ficha </param>
        /// <param name="person_id"> ID de persona del Alumno</param>
        /// <param name="user"> Usuario administrativo logeando</param>
        /// <returns></returns>
        public async Task<T> get(T obj, string type, string person_id, string user)
        {
            int version = _per.SOC_REC_VERSION;
            string vs_student = string.Format("{0}.0", version);
            string vs_verification = string.Format("{0}.1", version);
            string user_json = "{\"user\": \"" + user.ToLower() +"\"}";
            bool is_new = false;
            DBOContext db = new DBOContext();
            // Conversion implicita
            List<DBOTblFichaSocEco> list = await db.tbl_ficha_socioeconomica.Where(
                f => f.nivel == type
                  && f.persona == person_id
                  && f.version == vs_verification
                ).ToListAsync();
            if (list.Count == 0)
            {
                is_new = true;
                //buscar en la ficha del estudiante
               
                list = await db.tbl_ficha_socioeconomica.Where(
                f => f.nivel == type
                  && f.persona == person_id
                  && f.version == vs_student//student version
                ).ToListAsync();
            }
            VRProto<T>.asingProperties(ref obj, list);
            if(is_new)
                await this.save(obj, type, person_id.ToString(), user );
            return obj;
        }

        /// <summary>
        /// Guarda los datos de T en 
        /// la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task<int> save(T obj, string type, string person_id, string user)
        {
            int version = _per.SOC_REC_VERSION;
            string vs_student = string.Format("{0}.0", version);
            string vs_verification = string.Format("{0}.1", version);
            string user_json = "{\"user\": \"" + user.ToLower() + "\"}";
            DBOContext db = new DBOContext();
            // obtenemos los tados
            List<DBOTblFichaSocEco> lista = await db.tbl_ficha_socioeconomica.Where(
                f => f.nivel == type
                  && f.persona == person_id
                  && f.version == vs_verification
                ).ToListAsync();
            // buscar en la propiedades
            foreach (var prop in obj.GetType().GetProperties())
            {
                // buscamos la propiedad en la lista de propiedades, si existe la modificamos
                DBOTblFichaSocEco tmp = lista.FirstOrDefault(f => f.clave == prop.Name);
                string final_val = "";
                //para listas 
                if (prop.PropertyType.Name == "List`1") {
                    // lista de FamiliarDisease
                    if( prop.PropertyType.FullName.Contains("ACBusiness.Personal.SocieconomicRecords.FamiliarDisease") )
                    {
                        List<FamiliarDisease> list = (List<FamiliarDisease>)Convert.ChangeType(prop.GetValue(obj), prop.PropertyType);
                        if (list != null)
                        {
                            //convertimos las listas de tipo FamiliarDisease a cadena concatenando us propiedades
                            List<string> list2 = list.Select<FamiliarDisease, String>(x => x).ToList();
                            final_val = string.Join(",", list2);
                        }
                    }
                    else{
                        List<string> list = (List<string>)Convert.ChangeType(prop.GetValue(obj), prop.PropertyType);
                        if(list != null)
                            final_val = string.Join(",", list);
                    }
                }

                else {
                    final_val = (prop.GetValue(obj) == null) ? "" : prop.GetValue(obj).ToString();
                }
                // si existe, actualiza
                if (tmp != null)
                {
                    tmp.fecha = DateTime.Now;
                    tmp.valor = final_val;
                }
                else // sino, crea
                {
                    DBOTblFichaSocEco n = new DBOTblFichaSocEco();
                    n.nivel = type;
                    n.persona = person_id;
                    n.clave = prop.Name;
                    n.valor = final_val;
                    n.tag = user_json;
                    n.version = vs_verification;
                    n.fecha = DateTime.Now;
                    db.tbl_ficha_socioeconomica.Add(n);
                }
            }
            int r = await db.SaveChangesAsync();
            return r;
        }

        /// <summary>
        /// Asigna los datos de la base de datos
        /// al objeto
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="lista"></param>
        /// <returns></returns>
        public static void asingProperties(ref T obj, List<DBOTblFichaSocEco> lista)
        {
            foreach (var prop in obj.GetType().GetProperties())
            {
                //buscamos la propiedad en la lista de propiedades, si existe la anexsamos
                DBOTblFichaSocEco tmp = lista.FirstOrDefault(f => f.clave == prop.Name);
                if (tmp != null)
                {
                    if (tmp.valor != null && tmp.valor != "")
                    {
                        //para listas 
                        if (prop.PropertyType.Name == "List`1")
                        {
                            if (prop.PropertyType.FullName.Contains("ACBusiness.Personal.SocieconomicRecords.FamiliarDisease"))
                            {
                                List<String> list = tmp.valor.Split(',').ToList<string>();
                                List<FamiliarDisease> list2 = list.Select<String, FamiliarDisease>(
                                    x => new FamiliarDisease
                                    {
                                        person_id = x.Split(':')[0],
                                        disease = x.Split(':')[1]
                                    }
                                   ).ToList();
                                prop.SetValue(obj, Convert.ChangeType(list2, prop.PropertyType), null);
                            }
                            else
                            {
                                List<string> list = tmp.valor.Split(',').ToList<string>();
                                prop.SetValue(obj, Convert.ChangeType(list, prop.PropertyType), null);
                            }
                        }
                        else
                        {
                            prop.SetValue(obj, Convert.ChangeType(tmp.valor, prop.PropertyType), null);
                        }

                    }
                }
                else
                {
                    //si no existe lo buscamos en la ficha del estudiante

                }
            }
            //return true;
        }
    }
}
