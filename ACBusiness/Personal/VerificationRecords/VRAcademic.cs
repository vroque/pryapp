﻿using ACPermanence.Contexts.BDUCCI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ACBusiness.Personal.VerificationRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 14/06/2016</date>
    /// <summary>
    /// Modulo Academico para la Ficha Socieconomica
    /// </summary>
    public class VRAcademic
    {
        #region Properties
        public int end_year { get; set; }
        public string school_name { get; set; }
        public string school_ubigeo { get; set; }
        public string school_type { get; set; }
        public int school_pension { get; set; }
        #endregion
    
        #region functions
        public VRAcademic() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static async Task<VRAcademic> get(decimal person_id, string version)
        {
            VRProto<VRAcademic> soc = new VRProto<VRAcademic>();
            VRAcademic academic = await soc.get(new VRAcademic(), "academic", person_id.ToString(), version);

            //if (academic.school_name == null)
            //{
            DBOContext db = new DBOContext();
            int pidm = Decimal.ToInt32(person_id);
            var person = await db.tbl_persona.FirstOrDefaultAsync(f => f.IDPersonaN == pidm);
            decimal school_id = person.IDColegio;
            var school = await db.tbl_colegio.FirstOrDefaultAsync(f => f.IDColegio == school_id);
            if(school != null){
                academic.school_name = school.NomColegio;
                academic.school_type = (school.TipoColegio == "Particular") ? "Privado" : "Público";
                academic.school_ubigeo = school.IDDistrito;
            }
            //}
            return academic;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task<int> save(string person_id, string version)
        {
            VRProto<VRAcademic> soc = new VRProto<VRAcademic>();
            int r = await soc.save(this, "academic", person_id, version);
            return r;
        }
        #endregion
    }
}
