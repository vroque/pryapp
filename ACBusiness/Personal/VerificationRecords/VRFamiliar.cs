﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ACBusiness.Personal.VerificationRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 18/06/2016</date>
    /// <summary>
    /// Modulo Academico para la Ficha Socieconomica
    /// </summary>
    public class VRFamiliar
    {
        #region properties
        public string father { get; set; }
        public string mother { get; set; }
        public string parent_status { get; set; }
        /// <summary>
        /// 1 padre
        /// 2 madre
        /// 3 el estudiante
        /// 4 conyuge
        /// 5 hermano
        /// 6 otro
        /// </summary>
        public string head_family { get; set; }
        public string head_family_other { get; set; }
        public List<string> lives_with { get; set; }

        public string economic_status { get; set; }

        #endregion properties

        #region functions
        public VRFamiliar() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static async Task<VRFamiliar> get(decimal person_id, string version)
        {
            VRProto<VRFamiliar> soc = new VRProto<VRFamiliar>();
            VRFamiliar familiar = await  soc.get(new VRFamiliar(), "familiar", person_id.ToString(), version);
            return familiar;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task<int> save(string person_id, string version)
        {
            VRProto<VRFamiliar> soc = new VRProto<VRFamiliar>();
            int r = await soc.save(this, "familiar", person_id, version);
            return r;
        }
        #endregion
    }
}
