﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ACBusiness.Personal.VerificationRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 18/06/2016</date>
    /// <summary>
    /// Modulo laboral para la Ficha Socieconomica
    /// </summary>
    public class VRWorking
    {
        #region properties
        public string has { get; set; }
        public string category_dep { get; set; }
        public string category_ind { get; set; }
        public string category_cj { get; set; }
        public string dep_company { get; set; }
        public string dep_type { get; set; }
        public string dep_activity { get; set; }
        public int dep_service_time { get; set; }
        public string dep_position { get; set; }
        public string ind_actual_work { get; set; }
        public string ind_activity { get; set; }
        public string ind_has_ruc { get; set; }
        public string ind_ruc { get; set; }
        public int ind_workers { get; set; }
        public string cj_cesante { get; set; }
        public string cj_company { get; set; }
        public int cj_service_time { get; set; }
        public string has_complementary { get; set; }
        public string com_name { get; set; }
        #endregion


        #region functions
        public VRWorking() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static async Task<VRWorking> get(decimal person_id, string version)
        {
            VRProto<VRWorking> soc = new VRProto<VRWorking>();
            VRWorking working = await soc.get(new VRWorking(), "working", person_id.ToString(), version);
            return working;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task<int> save(string person_id, string version)
        {
            VRProto<VRWorking> soc = new VRProto<VRWorking>();
            int r = await soc.save(this, "working", person_id, version);
            return r;
        }
        #endregion
    }
}
