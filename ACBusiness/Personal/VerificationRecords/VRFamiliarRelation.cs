﻿using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.Contexts.BDUCCI;
using ACTools.SuperString;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Personal.VerificationRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 30/06/2016</date>
    /// <summary>
    /// Relaciones familiares para la Ficha Socieconomica
    /// </summary>
    public class VRFamiliarRelation
    {
        #region properties
        public string person_id { get; set; }
        /// <summary>
        /// D dependiente
        /// A aportante
        /// </summary>
        public string economic_type { get; set; }
        /// <summary>
        /// P padre
        /// M madre
        /// C conyuge
        /// H hermano
        /// I hijo
        /// O otros
        /// </summary>
        public string relation { get; set; }
        public string other_relation { get; set; }
        // Personal details
        public string document_type { get; set; }
        public string document_number { get; set; }
        public string father_name { get; set; }
        public string mother_name { get; set; }
        public string first_name { get; set; }
        //conit
        public string conti_pension { get; set; }
        // Academic details
        public string college_type { get; set; }
        /// <summary>
        /// Sin estudios
        /// Sin estudios
        /// Educación inicial
        /// Educación primaria
        /// Educación secundaria
        /// Educación técnica (instituto superior, etc)
        /// Educación superior pedagógica
        /// Educación universitaria
        /// Educación universitaria de postgrado
        /// Fuerzas armadas y policiales
        /// </summary>
        public string last_grade { get; set; }
        // Ubication details
        public string birth_country { get; set; }
        public string ubigeo { get; set; }
        public string origin_address { get; set; }
        public string referency { get; set; }
        // Contact details
        public string local_phone { get; set; }
        public string reference_phone { get; set; }
        public string reference_person { get; set; }
        public string celphone { get; set; }
        public string celphone2 { get; set; }
        public string celphone3 { get; set; }
        public string email { get; set; }
        public string email2 { get; set; }
        // Work details
        public string has_work { get; set; }
        public string category_dep { get; set; }
        public string category_ind { get; set; }
        public string category_cj { get; set; }
        // depend
        public string dep_company { get; set; }
        public string dep_type { get; set; }
        public string dep_activity { get; set; }
        public string dep_position { get; set; }
        public int dep_service_time { get; set; }
        // indpepend
        public string ind_actual_work { get; set; }
        public string ind_activity { get; set; }
        public string ind_has_ruc { get; set; }
        public string ind_ruc { get; set; }
        public int ind_workers { get; set; }
        // CJ
        public string cj_cesante { get; set; }
        public string cj_company { get; set; }
        public int cj_service_time { get; set; }
        // Complementary work
        public string has_work_complementary { get; set; }
        public string com_name { get; set; }
        

        #endregion properties

        #region functions
        public VRFamiliarRelation() { }
        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static async Task<VRFamiliarRelation> get(decimal person_id, string version)
        {
            VRProto<VRFamiliarRelation> soc = new VRProto<VRFamiliarRelation>();
            VRFamiliarRelation familiar = await soc.get(new VRFamiliarRelation(), "relations", person_id.ToString(), version);
            return familiar;
        }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static async Task<List<VRFamiliarRelation>> list(decimal person_id, string version)
        {
            // cargamos las relaciones del alumno
            DBOContext db = new DBOContext();
            List<DBOTblRelacionFamiliar> relations = await db.tbl_relacion_familiar
                .Where(f => f.persona == person_id.ToString()).ToListAsync();
            // cargamos a la familia segun las relaciones
            List<VRFamiliarRelation> familiars = new List<VRFamiliarRelation>();
            VRProto<VRFamiliarRelation> soc = new VRProto<VRFamiliarRelation>();

            foreach (DBOTblRelacionFamiliar fr in relations)
            {
                //quitar la p del codigo de familiar
                VRFamiliarRelation relation = await soc.get(new VRFamiliarRelation(), "relations", fr.familiar, version);
                familiars.Add(relation);
            }
            
            return familiars;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task<string> save(string person_id, string version)
        {
            this.father_name = this.father_name.ToUpper();
            this.mother_name = this.mother_name.ToUpper();
            this.first_name = this.first_name.ToUpper();
            //verificar si la persona existe
            if (this.person_id == null)
            {
                //si la persona no existe creamos la relación
                DBOContext db = new DBOContext();
                DBOTblRelacionFamiliar relation = new DBOTblRelacionFamiliar
                {
                    persona = person_id,
                    familiar = "",
                    relacion = this.relation
                };
                db.tbl_relacion_familiar.Add(relation);
                db.SaveChanges();
                relation.familiar = "p"+ relation.id.ToString().completeZeros(7);
                db.SaveChanges();
                this.person_id = relation.familiar;
            }
            VRProto<VRFamiliarRelation> soc = new VRProto<VRFamiliarRelation>();
            int r = await soc.save(this, "relations", this.person_id, version);
            return this.person_id;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="familiar_id"></param>
        /// <returns></returns>
        public static decimal remove(decimal person_id, string familiar_id)
        {
            DBOContext db = new DBOContext();
            DBOTblRelacionFamiliar relation = db.tbl_relacion_familiar.FirstOrDefault(
                f => f.persona == person_id.ToString()
                    && f.familiar == familiar_id
                );
            if (relation != null)
            {
                db.tbl_relacion_familiar.Remove(relation);
                db.SaveChanges();
                return 0;
            }

            return person_id;
        }
        #endregion
    }
}
