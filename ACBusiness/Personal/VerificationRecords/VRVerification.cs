﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Personal.VerificationRecords
{
    public class VRVerification
    {
        #region properties
        public string essalud_father {get; set;}
        public string essalud_mother { get; set; }
        public string essalud_other { get; set; }
        public string essalud_student { get; set; }
        public string sunat_father { get; set; }
        public string sunat_mother { get; set; }
        public string sunat_other { get; set; }
        public string sunat_student { get; set; }
        
        #endregion properties
    }
}
