﻿using ACBusiness.Personal.SocieconomicRecords;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ACBusiness.Personal.VerificationRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 18/06/2016</date>
    /// <summary>
    /// Modulo laboral para la Ficha Socieconomica
    /// </summary>
    public class VRHealth
    {
        #region properties
        public string hospital { get; set; }
        public string other { get; set; }
        public string disability { get; set; }
        public string disability_type { get; set; }
        public string has_disease { get; set; }
        public string disease { get; set; }
        public string has_familiar_disease { get; set; }
        public List<FamiliarDisease> familiar_disease { get; set; }
        #endregion
        #region functions
        public VRHealth() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static async Task<VRHealth> get(decimal person_id, string version)
        {
            VRProto<VRHealth> soc = new VRProto<VRHealth>();
            VRHealth health = await soc.get(new VRHealth(), "health", person_id.ToString(), version);
            return health;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task<int> save(string person_id, string version)
        {
            VRProto<VRHealth> soc = new VRProto<VRHealth>();
            int r = await soc.save(this, "health", person_id, version);
            return r;
        }
        #endregion
    }

    public struct VFamiliarDisease
    {
        public string person_id { get; set; }
        public string disease { get; set; }

        public static implicit operator String(VFamiliarDisease obj)
        {
            obj.disease = obj.disease.Replace(':', ' ').Replace(',',' ');
            return string.Format("{0}:{1}", obj.person_id, obj.disease);
        }
    }
}
