﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ACBusiness.Personal.VerificationRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 20/06/2016</date>
    /// <summary>
    /// Modulo de ingresos para la Ficha Socieconomica
    /// </summary>
    public class VRIncome
    {
        #region properties
        public int student { get; set; }
        public int father { get; set; }
        public int mother { get; set; }
        public int spouse { get; set; }
        public int other_fam { get; set; }
        public int cj_father { get; set; }
        public int cj_mother { get; set; }
        public int widowhood { get; set; }
        public int food { get; set; }
        public int house_rent { get; set; }
        public int car_rent { get; set; }
        public int other { get; set; }
        #endregion

        #region functions
        public VRIncome() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static async Task<VRIncome> get(decimal person_id, string version)
        {
            VRProto<VRIncome> soc = new VRProto<VRIncome>();
            VRIncome income = await soc.get(new VRIncome(), "income", person_id.ToString(), version);
            return income;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task<int> save(string person_id, string version)
        {
            VRProto<VRIncome> soc = new VRProto<VRIncome>();
            int r = await soc.save(this, "income", person_id, version);
            return r;
        }

        /// <summary>
        /// Total de ingresos
        /// </summary>
        /// <returns></returns>
        public int total()
        {
            int total = 0;
            total += this.student;
            total += this.father;
            total += this.mother;
            total += this.spouse;
            total += this.other_fam;
            total += this.cj_father;
            total += this.cj_mother;
            total += this.widowhood;
            total += this.food;
            total += this.house_rent;
            total += this.car_rent;
            total += this.other;
            return total;
        }
        #endregion
    }
}
