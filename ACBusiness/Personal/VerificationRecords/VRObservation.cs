﻿using ACTools.Constants;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.Contexts.BDUCCI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using _per = ACTools.Constants.PersonalConstants;


namespace ACBusiness.Personal.VerificationRecords
{
    /// <summary>
    /// Observaciones para la ficha de verificación
    /// </summary>
    public class VRObservation
    {
        #region properties
        public decimal person_id { get; set; }
        public string name { get; set; }
        public string observation { get; set; }
        public DateTime date { get; set; }
        #endregion properties

        #region functions
        public VRObservation() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static async Task<List<VRObservation>> query(decimal person_id)
        {
            DBOContext db = new DBOContext();
            string version = string.Format("{0}.1", _per.SOC_REC_VERSION);
            List<DBOTblFichaSocEco> list = await db.tbl_ficha_socioeconomica.Where(
                f => f.nivel == "observation"
                  && f.persona == person_id.ToString()
                  && f.version == version
                ).ToListAsync();

            List<VRObservation> observations = list.Select<DBOTblFichaSocEco, VRObservation>(x => x).ToList();

            return observations;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task<int> save(string user_id)
        {
            DBOContext db = new DBOContext();
            DBOTblFichaSocEco tmp = new DBOTblFichaSocEco();
            string version = string.Format("{0}.1", _per.SOC_REC_VERSION);
            tmp.nivel = "observation";
            tmp.persona = this.person_id.ToString();
            tmp.tag = "{\"user\": \"" + user_id + "\"}";
            tmp.clave = this.name;
            tmp.valor = this.observation;
            tmp.version = version;
            tmp.fecha = this.date;
            db.tbl_ficha_socioeconomica.Add(tmp);
           
            int r = await db.SaveChangesAsync();
            return r;
        }

        public static bool is_open(decimal person_id)
        {
            List<string> list = new List<string> 
                { "REVISION DE FICHA CERRADA.", "REAPERTURA DE LA FICHA."};

            DBOContext db = new DBOContext();
            string version = string.Format("{0}.1", _per.SOC_REC_VERSION);
            List<DBOTblFichaSocEco> rows = db.tbl_ficha_socioeconomica.Where(
                f => f.nivel == "observation"
                  && f.persona == person_id.ToString()
                  && f.version == version
                  && list.Contains(f.valor)
                ).OrderByDescending(f => f.fecha)
                .ToList();
            if(rows == null || rows.Count == 0)
                return true;

            DBOTblFichaSocEco closed = rows.Where(
                        f => f.valor == "REVISION DE FICHA CERRADA."
                    ).OrderBy(f => f.fecha).LastOrDefault();
            if(closed == null) return true;

            DBOTblFichaSocEco reopen = rows.Where(
                        f => f.valor == "REAPERTURA DE LA FICHA."
                    ).OrderBy(f => f.fecha).LastOrDefault();
            if (reopen != null)
            {
                if (reopen.fecha > closed.fecha)
                    return true;
            }

            return false;
        }
        #endregion functions

        #region implite opeartors
        public static implicit operator VRObservation(DBOTblFichaSocEco obj)
        {
            VRObservation obs = new VRObservation();
            obs.name = obj.clave;
            obs.observation = obj.valor;
            obs.date = (obj.fecha ==null)? DateTime.Now : (DateTime)obj.fecha;

            return obs;
        }

        #endregion implite opeartors

    }
}
