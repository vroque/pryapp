﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ACBusiness.Personal.VerificationRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 18/09/2016</date>
    /// <summary>
    /// Modulo de esalud y sunat
    /// </summary>
    public class VREssu
    {
         #region properties
        public string essalud_father { get; set; }
        public string essalud_mother { get; set; }
        public string essalud_other { get; set; }
        public string essalud_student { get; set; }

        public string has_sunat_father { get; set; }
        public string has_sunat_mother { get; set; }
        public string has_sunat_other { get; set; }
        public string has_sunat_student { get; set; }
        
        public string sunat_father { get; set; }
        public string sunat_mother { get; set; }
        public string sunat_other { get; set; }
        public string sunat_student { get; set; }


        #endregion

        #region functions
        public VREssu() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static async Task<VREssu> get(decimal person_id, string version)
        {
            VRProto<VREssu> soc = new VRProto<VREssu>();
            VREssu comodity = await soc.get(new VREssu(), "essu", person_id.ToString(), version);
            return comodity;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task<int> save(string person_id, string version)
        {
            VRProto<VREssu> soc = new VRProto<VREssu>();
            int r = await soc.save(this, "essu", person_id, version);
            return r;
        }
        #endregion
    }
}
