﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ACBusiness.Personal.VerificationRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 18/06/2016</date>
    /// <summary>
    /// Modulo de comodidades del hogar para la Ficha Socieconomica
    /// </summary>
    public class VRComodity
    {
        #region properties
        public List<string> appliances { get; set; }
        public List<string> services { get; set; }
        #endregion

        #region functions
        public VRComodity() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static async Task<VRComodity> get(decimal person_id, string version)
        {
            VRProto<VRComodity> soc = new VRProto<VRComodity>();
            VRComodity comodity = await soc.get(new VRComodity(), "comodity", person_id.ToString(), version);
            return comodity;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task<int> save(string person_id, string version)
        {
            VRProto<VRComodity> soc = new VRProto<VRComodity>();
            int r = await soc.save(this, "comodity", person_id, version);
            return r;
        }
        #endregion
    }
}
