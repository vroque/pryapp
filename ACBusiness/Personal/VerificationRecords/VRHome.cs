﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ACBusiness.Personal.VerificationRecords
{
    /// <author> Arturo Bolaños</author>
    /// <date> 21/06/2016</date>
    /// <summary>
    /// Modulo Vivienda para la Ficha Socieconomica
    /// </summary>
    public class VRHome
    {
        #region properties
        public string home_type { get; set; }
        public string home_status { get; set; }
        public string wall_material { get; set; }
        #endregion
        #region functions
        public VRHome() { }

        /// <summary>
        /// Binding a los metodos de SRProto para 
        /// obtener datos desde la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static async Task<VRHome> get(decimal person_id, string version)
        {
            VRProto<VRHome> soc = new VRProto<VRHome>();
            VRHome home = await soc.get(new VRHome(), "home", person_id.ToString(), version);
            return home;
        }

        /// <summary>
        /// Binding a SRProto para 
        /// obtener guardar los datos en el
        /// objeto actual a la base de datos
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task<int> save(string person_id, string version)
        {
            VRProto<VRHome> soc = new VRProto<VRHome>();
            int r = await soc.save(this, "home", person_id, version);
            return r;
        }
        #endregion
    }
}
