﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;

namespace ACBusiness.Personal
{
    /// <summary>
    /// Parentesco
    /// </summary>
    public class Kinship
    {
        #region Properties

        public int KinshipId { get; set; }
        public string KinshipName { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Lista de Parentesco
        /// </summary>
        /// <returns>Lista de Parentesco</returns>
        public static async Task<List<Kinship>> GetTask()
        {
            var nexoContext = new NEXOContext();
            var kinship = await nexoContext.cau_kinship
                .Select(s => new Kinship {KinshipId = s.KinshipID, KinshipName = s.KinshipName})
                .OrderBy(o => o.KinshipName).ToListAsync();
            return kinship;
        }

        #endregion Methods
    }
}