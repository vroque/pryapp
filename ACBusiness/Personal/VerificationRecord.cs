﻿using ACBusiness.Economic;
using ACBusiness.Personal.VerificationRecords;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using _per = ACTools.Constants.PersonalConstants;

namespace ACBusiness.Personal
{
    /// <summary>
    /// Ficha SOcieconomica de Verificacion
    /// </summary>
    public class VerificationRecord
    {
        #region properties
        public decimal person_id { get; set; }
        public string version { get; set; }
        public string type { get; set; }
        public string mode { get; set; }
        public string scala { get; set; }
        public bool status { get; set; }
        public Person personal { get; set; }
        public VRAcademic academic { get; set; }
        public VRWorking working { get; set; }
        public VRFamiliar familiar { get; set; }
        public List<VRFamiliarRelation> relations { get; set; }
        public VRIncome income { get; set; }
        public VRExpenses expenses { get; set; }
        public VRHome home { get; set; }
        public VRComodity comodity { get; set; }
        public VRHealth health { get; set; }
        public VREssu essu { get; set; }
        public List<VRObservation> observations { get; set; }
        #endregion properties

        #region functions

        /// <summary>
        /// Obtener ficha Socoeconomica por el codigo de estudiante
        /// </summary>
        /// <param name="student_id"></param>
        /// <returns></returns>
        public static async Task<VerificationRecord> get(decimal person_id, string user_id)
        {
            int version = _per.SOC_REC_VERSION;
            VerificationRecord socrec = new VerificationRecord();
            // obtener datos de persona
            Person person = new Person(person_id);
            socrec.personal = person; // sync 
            // datos academicos
            socrec.academic = await VRAcademic.get(socrec.personal.id, user_id); // async
            // datos laborales
            socrec.working = await VRWorking.get(socrec.personal.id, user_id);
            // datos familiares
            socrec.familiar = await VRFamiliar.get(socrec.personal.id, user_id);
            // datos de Dependientes y aportantes
            socrec.relations = await VRFamiliarRelation.list(socrec.personal.id, user_id);
            // datos ingresos familiares
            socrec.income = await VRIncome.get(socrec.personal.id, user_id);
            // datos egresos familiares
            socrec.expenses = await VRExpenses.get(socrec.personal.id, user_id);
            // datos de vivienda
            socrec.home = await VRHome.get(socrec.personal.id, user_id);
            // otras comodidades del hogar
            socrec.comodity = await VRComodity.get(socrec.personal.id, user_id);
            // datos de salud
            socrec.health = await VRHealth.get(socrec.personal.id, user_id);
            // confirmacion essalud sunat
            socrec.essu = await VREssu.get(socrec.personal.id, user_id);
            //Observaciones
            socrec.observations = await VRObservation.query(socrec.personal.id);
            socrec.person_id = socrec.personal.id;
            //generic data
            socrec.version = string.Format("{0}.1", version);
            socrec.type = "verification";

            socrec.scala = Scala.GetByStudent(person.document_number);

            //verificar primera accion
            if (socrec.observations.Count == 0)
            {
                VRObservation obs = new VRObservation();
                obs.person_id = socrec.personal.id;
                obs.date = DateTime.Now;
                obs.observation = "APERTURA DE LA FICHA DE REVISION POR EL USUARIO (" + user_id + ")";
                obs.name = user_id;
                int r = await obs.save(user_id);
                if (r == 0)
                    throw new Exception("no se pudo guardar la revision de la ficha");
                socrec.observations.Add(obs);
                socrec.status = true;
            }
            else
            {
                socrec.status = VRObservation.is_open(socrec.personal.id);
            }
            return socrec;
        }

        /// <summary>
        /// Obtener ficha Socoeconomica por el codigo de estudiante
        /// </summary>
        /// <param name="student_id"></param>
        /// <returns></returns>
        public static async Task<VerificationRecord> getByStudentCode(string student_id, string user_id) 
        {
            int version = _per.SOC_REC_VERSION;
            VerificationRecord socrec = new VerificationRecord();
            // obtener datos de persona
            Person person = new Person();
            socrec.personal =  person.getByStudentCode(student_id); // sync 
            // datos academicos
            socrec.academic = await VRAcademic.get(socrec.personal.id, user_id); // async
            // datos laborales
            socrec.working = await VRWorking.get(socrec.personal.id, user_id);
            // datos familiares
            socrec.familiar = await VRFamiliar.get(socrec.personal.id, user_id);
            // datos de Dependientes y aportantes
            socrec.relations = await VRFamiliarRelation.list(socrec.personal.id, user_id);
            // datos ingresos familiares
            socrec.income = await VRIncome.get(socrec.personal.id, user_id);
            // datos egresos familiares
            socrec.expenses = await VRExpenses.get(socrec.personal.id, user_id);
            // datos de vivienda
            socrec.home = await VRHome.get(socrec.personal.id, user_id);
            // otras comodidades del hogar
            socrec.comodity = await VRComodity.get(socrec.personal.id, user_id);
            // datos de salud
            socrec.health = await VRHealth.get(socrec.personal.id, user_id);
            // confirmacion essalud sunat
            socrec.essu = await VREssu.get(socrec.personal.id, user_id);
            //Observaciones
            socrec.observations = await VRObservation.query(socrec.personal.id);
            socrec.person_id = socrec.personal.id;
            //generic data
            socrec.version = string.Format("{0}.1", version);
            socrec.type = "verification";

            socrec.scala = Scala.GetByStudent(student_id);

            //verificar primera accion
            if (socrec.observations.Count == 0)
            {
                VRObservation obs = new VRObservation();
                obs.person_id = socrec.personal.id;
                obs.date = DateTime.Now;
                obs.observation = "APERTURA DE LA FICHA DE REVISION POR EL USUARIO (" + user_id + ")";
                obs.name = user_id;
                int r = await obs.save(user_id);
                if (r == 0)
                    throw new Exception("no se pudo guardar los datos");
                socrec.observations.Add(obs);
                socrec.status = true;
            }
            else{
                socrec.status = VRObservation.is_open(socrec.personal.id);
            }
            return socrec;
        }

        public async Task<bool> save_single(string person_id, string level, string key, string value)
        {
            DBOContext db = new DBOContext();
            string version = string.Format("{0}.1", _per.SOC_REC_VERSION);
            
            DBOTblFichaSocEco item = await db.tbl_ficha_socioeconomica.FirstOrDefaultAsync(
                f => f.nivel == level
                  && f.clave == key
                  && f.persona == person_id
                  && f.version == version
                );
            if (item != null)
            {
                item.valor = value;
                item.fecha = DateTime.Now;
                int i = db.SaveChanges();
                if (i > 0) return true;
            }
            return false;
        }


        public static async Task<bool> isClosed(string student_id)
        {
            int version = _per.SOC_REC_VERSION;
            VerificationRecord socrec = new VerificationRecord();
            // obtener datos de persona
            Person person = new Person();
            socrec.personal = person.getByStudentCode(student_id); // sync 
            //get observations 
            socrec.observations = await VRObservation.query(socrec.personal.id);

            if (socrec.observations.Count > 0)
            {
                VRObservation obs = socrec.observations
                    .OrderByDescending(f => f.date)
                    .FirstOrDefault();
                if (obs == null)
                    return false;
                else if (obs.observation == "REVISION DE FICHA CERRADA.")
                    return true;
            }

            return false;
        }
       
        #endregion functions
    }
}
