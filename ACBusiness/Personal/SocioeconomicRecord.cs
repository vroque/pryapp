﻿using ACBusiness.Personal.SocieconomicRecords;
using System;
using System.Collections.Generic;

namespace ACBusiness.Personal
{
    /// <summary>
    /// Ficha SOcieconomica Completa
    /// </summary>
    public class SocioeconomicRecord
    {
        #region properties
        public string person_id { get; set; }
        public string version { get; set; }
        public string type { get; set; }
        public Person personal { get; set; }
        public SRUbication ubication { get; set; }
        public SRAcademic academic { get; set; }
        public SRWorking working { get; set; }
        public SRFamiliar familiar { get; set; }
        public List<SRFamiliarRelation> relations { get; set; }
        public SRIncome income { get; set; }
        public SRExpenses expenses { get; set; }
        public SRHome home { get; set; }
        public SRComodity comodity { get; set; }
        public SRHealth health { get; set; }
        #endregion properties

        #region functions
        /// <summary>
        /// Obtener ficha Socoeconomica por el codigo de estudiante
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static SocioeconomicRecord getByStudentCode(string student_id) 
        {
            string version = "1.0";
            SocioeconomicRecord socrec = new SocioeconomicRecord();
            // obtener datos de persona
            Person person = new Person();
            socrec.personal = person.getByStudentCode(student_id);
            // datos de ubicación
            socrec.ubication = SRUbication.get(socrec.personal.id, version);
            // datos academicos
            socrec.academic = SRAcademic.get(socrec.personal.id, version);
            // datos laborales
            socrec.working = SRWorking.get(socrec.personal.id, version);
            // datos familiares
            socrec.familiar = SRFamiliar.get(socrec.personal.id, version);
            // datos de Dependientes y aportantes
            socrec.relations = SRFamiliarRelation.list(socrec.personal.id, version);
            // datos ingresos familiares
            socrec.income = SRIncome.get(socrec.personal.id, version);
            // datos egresos familiares
            socrec.expenses = SRExpenses.get(socrec.personal.id, version);
            // datos de vivienda
            socrec.home = SRHome.get(socrec.personal.id, version);
            // otras comodidades del hogar
            socrec.comodity = SRComodity.get(socrec.personal.id, version);
            // datos de salud
            socrec.health = SRHealth.get(socrec.personal.id, version);
            return socrec;
        }
        #endregion functions
    }
}
