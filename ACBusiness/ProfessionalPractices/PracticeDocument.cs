﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
namespace ACBusiness.ProfessionalPractices
{
    public class PracticeDocument
    {
        #region Propiedades
        public string IDDependencia { get; set; }
        public string IDSede { get; set; }
        public int IDPracticaDocumento { get; set; }
        public string IDPractica { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string IDPersonaRegistro { get; set; }
        public string IDEscuela { get; set; }
        public string IDAlumno { get; set; }

        DBOContext _db = new DBOContext();
        #endregion

        #region Propiedades Extras
        public int pidm { get; set; }
        #endregion

        #region Metodos
        public List<PracticeDocument> getPracticeDocumentPIDM(int Pidm, string idescuela)
        {
            List<PracticeDocument> list = _db.opp_tbl_practica_documento
                .Join(
                    _db.view_alumno_basico,
                    td => new { IDAlumno = td.IDAlumno },
                    da => new { IDAlumno = da.IDAlumno },
                    (td, da) => new { td, da}
                )
                .Where(
                    result =>
                        result.da.IDPersonaN == Pidm
                        && result.td.IDEscuela == idescuela
                )
                .ToList()
                .Select(
                    result => new PracticeDocument
                    {
                        IDDependencia = result.td.IDDependencia,
                        IDSede = result.td.IDSede,
                        IDPracticaDocumento = result.td.IDPracticaDocumento,
                        IDPractica = result.td.IDPractica,
                        FechaCreacion = result.td.FechaCreacion,
                        IDPersonaRegistro = result.td.IDPersonaRegistro,
                        IDEscuela = result.td.IDEscuela,
                        IDAlumno = result.td.IDAlumno,
                        pidm = result.da.IDPersonaN,
                    }
                ).ToList();
            return list;
        }
        #endregion
    }
}
