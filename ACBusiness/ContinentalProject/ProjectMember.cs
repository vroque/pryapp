﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Institutional;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.PRY;
using _projConst = ACTools.Constants.ProjectUCConstants;
using _instConst = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.ContinentalProject
{
    public class ProjectMember
    {
        #region Properties

        public int Id { get; set; }
        public string Div { get; set; }
        public string Campus { get; set; }
        public string CampusName { get; set; }
        public string Program { get; set; }
        public string ProgramName { get; set; }
        public string Department { get; set; }
        public string DepartmentName { get; set; }
        public decimal Pidm { get; set; }
        public bool Approved { get; set; }
        public int IdMemberType { get; set; }
        public MemberType MemberType { get; set; }
        public int IdProfileProject { get; set; }
        public ProfileProject ProfileProject { get; set; }
        public string Remarks { get; set; }
        public DateTime CreatedAt { get; set; }
        public string FullName { get; set; }
        public DateTime? ChangeStatusDate { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Registra un integrante a un proyecto
        /// </summary>
        /// <param name="projectMember"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static async Task<ProjectMember> Create(ProjectMember projectMember, string user)
        {
            var projMember = new PRYProjectMember
            {
                Active = true,
                Approved = false,
                Campus = projectMember.Campus,
                Department = projectMember.Department,
                Div = projectMember.Div,
                IdMemberType = projectMember.IdMemberType,
                IdProfileProject = projectMember.IdProfileProject,
                Pidm = projectMember.Pidm,
                Program = projectMember.Program
            };
            projMember.CreatedBy = projMember.UpdatedBy = user;
            projMember.CreatedAt = projMember.UpdatedAt = DateTime.Now;

            using (var dboContext = new DBOContext())
            {
                dboContext.pry_project_member.Add(projMember);
                return await dboContext.SaveChangesAsync() > 0 ? projectMember : null;
            }
        }

        public static async Task<List<ProjectMember>> GetByIdProject(int idProject)
        {
            using (var dboContext = new DBOContext())
            {
                var members = await dboContext.pry_project_member.AsNoTracking().Where(x =>
                    x.Active && x.IdProject == idProject && x.IdMemberType == _projConst.MemberTypeMember)
                    .Select(x => new ProjectMember
                    {
                        Approved = x.Approved,
                        Campus = x.Campus,
                        CreatedAt = x.CreatedAt,
                        Department = x.Department,
                        Div = x.Div,
                        IdMemberType = x.IdMemberType,
                        IdProfileProject = x.IdProfileProject,
                        Pidm = x.Pidm,
                        Program = x.Program,
                        ChangeStatusDate = x.ChangeStatusDate
                    }).ToListAsync();

                members.ForEach(x =>
                {
                    x.CampusName = string.IsNullOrEmpty(x.Campus) ? null :_instConst.CAMPUS_NAME[x.Campus];
                    x.DepartmentName = string.IsNullOrEmpty(x.Department) ? "No aplica" : _instConst.DEPARTMENT_DOCS_NAMES[x.Department];
                    x.ProgramName = string.IsNullOrEmpty(x.Program) ? "No aplica" :_instConst.PROGRAMS[x.Program];
                    x.FullName = Person.GetFullName(x.Pidm);
                    x.MemberType = new MemberType { Id = x.IdMemberType, Name = _projConst.MemberTypesName[x.IdMemberType] };
                    x.ProfileProject = ContinentalProject.ProfileProject.GetById(x.IdProfileProject);
                });

                return members;
            }
        }

        #endregion
    }
}
