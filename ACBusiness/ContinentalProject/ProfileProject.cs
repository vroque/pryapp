﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.PRY;

namespace ACBusiness.ContinentalProject
{
    public class ProfileProject
    {
        #region Properties

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int IdProject { get; set; }
        public int NumberVacancies { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Obtiene el perfil en base a su id
        /// </summary>
        /// <param name="id">Id del perfil</param>
        /// <returns></returns>
        public static ProfileProject GetById(int id)
        {
            using (var dboContext = new DBOContext())
            {
                return dboContext.pry_profile_project.AsNoTracking().Where(x => x.Active && x.Id == id)
                    .Select(x => new ProfileProject
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Description = x.Description
                    }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Registra un perfil para un proeycto
        /// </summary>
        /// <param name="profileProject">Perfil del proyecto</param>
        /// <param name="userId">Nombre del usuario que lo registra</param>
        /// <returns></returns>
        public static async Task<ProfileProject> Create(ProfileProject profileProject, string userId)
        {
            var profProject = new PRYProfileProject { Active = true };
            profProject.CreatedAt = profProject.UpdatedAt = DateTime.Now;
            profProject.CreatedBy = profProject.UpdatedBy = userId;
            profProject.Description = profileProject.Description;
            profProject.IdProject = profileProject.IdProject;
            profProject.NumberVacancies = profileProject.NumberVacancies;
            profProject.Title = profileProject.Title;

            using (var dboContext = new DBOContext())
            {
                dboContext.pry_profile_project.Add(profProject);
                return await dboContext.SaveChangesAsync() > 0 ? profileProject : null;
            }
        }

        /// <summary>
        /// Registra un perfil para un proeycto
        /// </summary>
        /// <param name="profileProject">Perfil del proyecto</param>
        /// <param name="idProject">Id del proyecto</param>
        /// <param name="userId">Nombre del usuario que lo registra</param>
        /// <param name="dboContext">Contexto</param>
        /// <returns></returns>
        public static void AddToContext(ProfileProject profileProject, int idProject, string userId, DBOContext dboContext)
        {
            var profProject = new PRYProfileProject { Active = true };
            profProject.CreatedAt = profProject.UpdatedAt = DateTime.Now;
            profProject.CreatedBy = profProject.UpdatedBy = userId;
            profProject.Description = profileProject.Description;
            profProject.IdProject = idProject;
            profProject.NumberVacancies = profileProject.NumberVacancies;
            profProject.Title = profileProject.Title;
            dboContext.pry_profile_project.Add(profProject);

        }

        /// <summary>
        /// Actualiza un perfil de un proyecto
        /// </summary>
        /// <param name="profileProject">Modelo del perfil del proyecto solicitado</param>
        /// <param name="userId">Nombre del usuario que actualiza</param>
        /// <returns></returns>
        public static async Task<bool> Update(ProfileProject profileProject, string userId)
        {
            using (var dboContext = new DBOContext())
            {
                var profProject = await dboContext.pry_profile_project.FirstOrDefaultAsync(x => x.Active &&
                    x.IdProject == profileProject.IdProject && x.Id == profileProject.Id);

                if (profProject == null) return false;

                dboContext.Entry(profProject).Property(x => x.CreatedAt).IsModified = false;
                dboContext.Entry(profProject).Property(x => x.CreatedBy).IsModified = false;
                dboContext.Entry(profProject).Property(x => x.IdProject).IsModified = false;

                profProject.UpdatedAt = DateTime.Now;
                profProject.UpdatedBy = userId;
                profProject.Description = profileProject.Description;
                profProject.NumberVacancies = profileProject.NumberVacancies;
                profProject.Title = profileProject.Title;

                return await dboContext.SaveChangesAsync() > 0;
            }
        }

        /// <summary>
        /// Actualiza un perfil de un proyecto
        /// </summary>
        /// <param name="idProfileProject">Id del perfil del proyecto</param>
        /// <param name="userId">Nom del usuario que elimina</param>
        /// <returns></returns>
        public static async Task<bool> Delete(int idProfileProject, string userId)
        {
            using (var dboContext = new DBOContext())
            {
                var profProject = await dboContext.pry_profile_project.FirstOrDefaultAsync(x => x.Active && x.Id == idProfileProject);

                if (profProject == null) return false;

                dboContext.Entry(profProject).Property(x => x.Active).IsModified = true;
                dboContext.Entry(profProject).Property(x => x.UpdatedAt).IsModified = true;
                dboContext.Entry(profProject).Property(x => x.UpdatedBy).IsModified = true;

                profProject.UpdatedAt = DateTime.Now;
                profProject.UpdatedBy = userId;
                profProject.Active = false;

                return await dboContext.SaveChangesAsync() > 0;
            }
        }

        #endregion
    }
}
