﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using _acadCons = ACTools.Constants.AcademicConstants;

namespace ACBusiness.ContinentalProject
{
    public class ProjectType
    {
        #region Properties

        public int Id { get; set; }
        public string Div { get; set; }
        public string Name { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Retorna todos los tipos de proyectos activos
        /// </summary>
        /// <param name="div">Dependencia</param>
        /// <returns></returns>
        public static async Task<List<Category>> GetAll(string div = _acadCons.DEFAULT_DIV)
        {
            using (var dboContext = new DBOContext())
            {
                return await dboContext.pry_project_type.AsNoTracking().Where(x => x.Active && x.Div == div)
                    .Select(x => new Category { Id = x.Id, Name = x.Name, Div = x.Div }).ToListAsync();
            }
        }

        #endregion
    }
}
