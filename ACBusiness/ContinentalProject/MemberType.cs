﻿namespace ACBusiness.ContinentalProject
{
    public class MemberType
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }

        #endregion
    }
}
