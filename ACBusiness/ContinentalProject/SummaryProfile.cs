﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using _projConst = ACTools.Constants.ProjectUCConstants;
namespace ACBusiness.ContinentalProject
{
    public class SummaryProfile
    {
        #region Properties

        public int IdProfileProject { get; set; }
        public int IdProject { get; set; }
        public string ProfileName { get; set; }
        public int QuantityVacancy { get; set; }
        public int BusyQuantityVacancy { get; set; }

        #endregion

        #region Methods

        public static async Task<List<SummaryProfile>> GetByIdProjet(int idProject)
        {
            using (var dboContext = new DBOContext())
            {
                var profiles = await dboContext.pry_profile_project.Where(x => x.Active && x.IdProject == idProject).ToListAsync();

                var summary = new List<SummaryProfile>();

                foreach (var profile in profiles)
                {
                    summary.Add(new SummaryProfile
                    {
                        IdProject = idProject,
                        IdProfileProject = profile.Id,
                        ProfileName = profile.Title,
                        QuantityVacancy = profile.NumberVacancies,
                        BusyQuantityVacancy = dboContext.pry_project_member.Count(x => x.Active && x.Approved
                            && x.ChangeStatusDate != null && x.IdMemberType == _projConst.MemberTypeMember
                            && x.IdProfileProject == profile.Id && x.IdProject == profile.IdProject)
                    });
                }
                    
                return summary;
            }
        }

        #endregion
    }
}
