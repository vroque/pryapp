﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Configuration;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.PRY;
using ACTools.Mail;
using ACTools.SuperString;
using _acadConst = ACTools.Constants.AcademicConstants;
using _projConst = ACTools.Constants.ProjectUCConstants;

namespace ACBusiness.ContinentalProject
{
    public class Project
    {
        #region Properties

        public int Id { get; set; }
        public string Div { get; set; }
        public string Campus { get; set; }
        public string Title { get; set; }
        public string Objective { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string IdMicrosoftTeams { get; set; }
        public bool Approved { get; set; }
        public int IdCategory { get; set; }
        public Category Category { get; set; }
        public int IdProjectType { get; set; }
        public ProjectType ProjectType { get; set; }
        public List<ProfileProject> ProfileProjects { get; set; }
        public List<ProjectMember> ProjectMembers { get; set; }
        public DateTime? ChangeStatusDate { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Obtiene todos los proyectos activos
        /// /// * Incluye:
        ///     - Categoria
        ///     - Tipo de proyecto
        /// </summary>
        /// <returns></returns>
        public static async Task<List<Project>> GetAll()
        {
            using (var dboContext = new DBOContext())
            {
                var projects = await dboContext.pry_project.AsNoTracking().Where(x => x.Active && x.Id != 0)
                    .Join(dboContext.pry_category, project => project.IdCategory, category => category.Id,
                        (project, category) => new { project, category })
                    .Join(dboContext.pry_project_type, firstUnion => firstUnion.project.IdProjectType, projectType => projectType.Id,
                        (firstUnion, projectType) => new { firstUnion, projectType })
                    .ToListAsync();
                return projects.Select(x => new Project
                {
                    Id = x.firstUnion.project.Id,
                    Title = x.firstUnion.project.Title,
                    Campus = x.firstUnion.project.Campus,
                    Objective = x.firstUnion.project.Objective,
                    Description = x.firstUnion.project.Description,
                    CreatedAt = x.firstUnion.project.CreatedAt,
                    Approved = x.firstUnion.project.Approved,
                    ChangeStatusDate = x.firstUnion.project.ChangeStatusDate,
                    ProjectType = new ProjectType { Id = x.projectType.Id, Name = x.projectType.Name },
                    Category = new Category { Id = x.firstUnion.category.Id, Name = x.firstUnion.category.Name }
                }).ToList();
            }
        }

        /// <summary>
        /// Obtiene el proyecto mediante su id
        /// * Incluye:
        ///     - Categoria
        ///     - Tipo de proyecto
        ///     - Perfiles del proyecto
        /// </summary>
        /// <param name="id">id del proyecto</param>
        /// <returns></returns>
        public static async Task<Project> GetById(int id)
        {
            using (var dboContext = new DBOContext())
            {
                var pryProject = await dboContext.pry_project.AsNoTracking().Where(x => x.Active && x.Id == id)
                    .Join(dboContext.pry_category, proj => proj.IdCategory, category => category.Id,
                        (proj, category) => new { proj, category })
                    .Join(dboContext.pry_project_type, firstUnion => firstUnion.proj.IdProjectType, projectType => projectType.Id,
                        (firstUnion, projectType) => new { firstUnion, projectType })
                    .FirstOrDefaultAsync();

                if (pryProject == null) return null;

                var project = new Project
                {
                    Id = pryProject.firstUnion.proj.Id,
                    Title = pryProject.firstUnion.proj.Title,
                    Campus = pryProject.firstUnion.proj.Campus,
                    Objective = pryProject.firstUnion.proj.Objective,
                    Description = pryProject.firstUnion.proj.Description,
                    CreatedAt = pryProject.firstUnion.proj.CreatedAt,
                    Approved = pryProject.firstUnion.proj.Approved,
                    Keywords = pryProject.firstUnion.proj.Keywords,
                    IdCategory = pryProject.firstUnion.proj.IdCategory,
                    IdProjectType = pryProject.firstUnion.proj.IdProjectType,
                    ChangeStatusDate = pryProject.firstUnion.proj.ChangeStatusDate,
                    ProjectType = new ProjectType { Id = pryProject.projectType.Id, Name = pryProject.projectType.Name },
                    Category = new Category { Id = pryProject.firstUnion.category.Id, Name = pryProject.firstUnion.category.Name },
                    ProfileProjects = dboContext.pry_profile_project.Where(x => x.Active && x.IdProject == pryProject.firstUnion.proj.Id)
                        .Select(x => new ProfileProject { Id = x.Id, Description = x.Description, Title = x.Title, NumberVacancies = x.NumberVacancies })
                        .ToList(),
                    ProjectMembers = dboContext.pry_project_member.Where(x => x.Active && x.IdProject == pryProject.firstUnion.proj.Id
                        && (x.IdMemberType == _projConst.MemberTypeOwner || x.IdMemberType == _projConst.MemberTypeLeader))
                        .Select(x => new ProjectMember { Pidm = x.Pidm, IdMemberType = x.IdMemberType }).ToList()
                };

                project.ProfileProjects.ForEach(x =>
                {
                    x.NumberVacancies -= (dboContext.pry_project_member.Count(y => y.Active && y.Approved
                        && y.IdProfileProject == x.Id && y.IdMemberType == _projConst.MemberTypeMember));
                });

                project.ProjectMembers.ForEach(x =>
                {
                    //var fullName = Person.GetFullName(x.Pidm);
                    x.FullName = Person.GetFullName(x.Pidm);
                });

                return project;
            }
        }

        /// <summary>
        /// Obtiene el proyecto mediante su id
        /// * Incluye:
        ///     - Tipo de proyecto
        /// </summary>
        /// <param name="idCategory">id de la categoria</param>
        /// <returns></returns>
        public static async Task<List<Project>> GetByCategoryId(int idCategory)
        {
            using (var dboContext = new DBOContext())
            {
                return await dboContext.pry_project.AsNoTracking().Where(x => x.Active && x.IdCategory == idCategory
                        && x.Id != 0 && x.Approved)
                    .Join(dboContext.pry_category, project => project.IdCategory, category => category.Id,
                        (project, category) => new { project, category })
                    .Join(dboContext.pry_project_type, firstUnion => firstUnion.project.IdProjectType, projectType => projectType.Id,
                        (firstUnion, projectType) => new { firstUnion, projectType })
                    .Select(x => new Project
                    {
                        Id = x.firstUnion.project.Id,
                        Title = x.firstUnion.project.Title,
                        Campus = x.firstUnion.project.Campus,
                        Objective = x.firstUnion.project.Objective,
                        Description = x.firstUnion.project.Description,
                        CreatedAt = x.firstUnion.project.CreatedAt,
                        ProjectType = new ProjectType { Id = x.projectType.Id, Name = x.projectType.Name },
                        Category = new Category { Id = x.firstUnion.category.Id, Name = x.firstUnion.category.Name }
                    })
                    .ToListAsync();
            }
        }

        /// <summary>
        /// Obtiene todos los proyectos activos de un owner de proyecto
        /// /// * Incluye:
        ///     - Categoria
        ///     - Tipo de proyecto
        /// </summary>
        /// /// <param name="pidm">Pidm de la persona</param>
        /// <returns></returns>
        public static async Task<List<Project>> GetByPidm(decimal pidm)
        {
            using (var dboContext = new DBOContext())
            {
                var projects = await dboContext.pry_project.AsNoTracking().Where(x => x.Active && x.OwnerPidm == pidm)
                    .Join(dboContext.pry_category, project => project.IdCategory, category => category.Id,
                        (project, category) => new { project, category })
                    .Join(dboContext.pry_project_type, firstUnion => firstUnion.project.IdProjectType, projectType => projectType.Id,
                        (firstUnion, projectType) => new { firstUnion, projectType })
                    .ToListAsync();
                return projects.Select(x => new Project
                {
                    Id = x.firstUnion.project.Id,
                    Title = x.firstUnion.project.Title,
                    Campus = x.firstUnion.project.Campus,
                    Objective = x.firstUnion.project.Objective,
                    Description = x.firstUnion.project.Description,
                    CreatedAt = x.firstUnion.project.CreatedAt,
                    Approved = x.firstUnion.project.Approved,
                    ChangeStatusDate = x.firstUnion.project.ChangeStatusDate,
                    ProjectType = new ProjectType { Id = x.projectType.Id, Name = x.projectType.Name },
                    Category = new Category { Id = x.firstUnion.category.Id, Name = x.firstUnion.category.Name }
                }).ToList();
            }
        }

        /// <summary>
        /// Registra un nuevo proyecto
        /// </summary>
        /// <param name="newProject">Modelo del proyecto</param>
        /// <param name="campus">Campus</param>
        /// <param name="pidm">Pidm de la persona que registra</param>
        /// <param name="userId">Nombre de usuario del que registra</param>
        /// <returns></returns>
        public static async Task<Project> Create(Project newProject, string campus, decimal pidm, string userId)
        {
            var project = new PRYProject
            {
                Title = newProject.Title,
                Campus = newProject.Campus,
                Description = newProject.Description,
                Active = true,
                IdCategory = newProject.IdCategory,
                IdProjectType = newProject.IdProjectType,
                Objective = newProject.Objective,
                Div = _acadConst.DEFAULT_DIV,
                Approved = false,
                Keywords = newProject.Keywords,
                OwnerPidm = pidm
            };
            project.CreatedAt = project.UpdatedAt = DateTime.Now;
            project.CreatedBy = project.UpdatedBy = userId;

            using (var dboContext = new DBOContext())
            {
                using (var transaction = dboContext.Database.BeginTransaction())
                {
                    var createdProject = dboContext.pry_project.Add(project);

                    if ((await dboContext.SaveChangesAsync()) <= 0)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    // Agregamos al dueño del proyecto
                    dboContext.pry_project_member.Add(new PRYProjectMember
                    {
                        Active = true,
                        Approved = true,
                        Campus = campus,
                        IdMemberType = _projConst.MemberTypeOwner,
                        Pidm = pidm,
                        IdProject = createdProject.Id,
                        Div = _acadConst.DEFAULT_DIV,
                        CreatedAt = DateTime.Now,
                        CreatedBy = userId,
                        UpdatedAt = DateTime.Now,
                        UpdatedBy = userId
                    });

                    // Agregamos a los lideres
                    foreach (var projectMember in newProject.ProjectMembers)
                    {
                        dboContext.pry_project_member.Add(new PRYProjectMember
                        {
                            Active = true,
                            Approved = true,
                            Campus = campus,
                            IdMemberType = _projConst.MemberTypeLeader,
                            IdProject = createdProject.Id,
                            Pidm = projectMember.Pidm,
                            Div = _acadConst.DEFAULT_DIV,
                            CreatedAt = DateTime.Now,
                            CreatedBy = userId,
                            UpdatedAt = DateTime.Now,
                            UpdatedBy = userId
                        });
                    }

                    // Agregamos los perfiles
                    foreach (var profileProject in newProject.ProfileProjects)
                    {
                        ProfileProject.AddToContext(profileProject, createdProject.Id, userId, dboContext);
                    }

                    if ((await dboContext.SaveChangesAsync()) <= 0)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    try
                    {
                        var mail = new MailSender();
                        var person = new Person(project.OwnerPidm);
                        mail.compose("pry_generic_template",
                                new { first_name = person.full_name, message = $"Tu proyecto \"{project.Title}\" será evaluado, toda información se enviará por este medio." })
                            .destination($"{person.userName}@continental.edu.pe", "Tu proyecto fue subido con éxito").send();
                    }
                    catch { }

                    transaction.Commit();
                    return newProject;
                }
            }
        }

        /// <summary>
        /// Registra un nuevo proyecto
        /// </summary>
        /// <param name="newProject">Modelo del proyecto</param>
        /// <param name="userId">Nombre de usuario del que registra</param>
        /// <returns></returns>
        public static async Task<Project> Update(Project newProject, string userId)
        {
            using (var dboContext = new DBOContext())
            {
                using (var transaction = dboContext.Database.BeginTransaction())
                {
                    var project = await dboContext.pry_project.FirstOrDefaultAsync(x => x.Active && x.Id == newProject.Id);
                    if (project == null)
                    {
                        transaction.Dispose();
                        return null;
                    }

                    // Obtener todos los lideres
                    var members = await dboContext.pry_project_member.Where(x => x.Active && x.IdProject == project.Id
                        && x.IdMemberType == _projConst.MemberTypeLeader).ToListAsync();

                    // Insertar los nuevos
                    foreach (var member in newProject.ProjectMembers)
                    {
                        if (!members.Any(x => x.Pidm == member.Pidm))
                        {
                            dboContext.pry_project_member.Add(new PRYProjectMember
                            {
                                Active = true,
                                Approved = true,
                                Campus = project.Campus,
                                IdMemberType = _projConst.MemberTypeLeader,
                                Pidm = member.Pidm,
                                IdProject = project.Id,
                                Div = _acadConst.DEFAULT_DIV,
                                CreatedAt = DateTime.Now,
                                CreatedBy = userId,
                                UpdatedAt = DateTime.Now,
                                UpdatedBy = userId
                            });
                            if ((await dboContext.SaveChangesAsync()) <= 0)
                            {
                                transaction.Rollback();
                                return null;
                            }
                        }
                    }

                    // Eliminar
                    foreach (var member in members)
                    {
                        if (!newProject.ProjectMembers.Any(x => x.Pidm == member.Pidm))
                        {
                            var oldMember = await dboContext.pry_project_member.FirstOrDefaultAsync(x => x.Active
                                && x.Pidm == member.Pidm && x.IdProject == project.Id && x.IdProfileProject == member.IdProfileProject);

                            if (oldMember == null) continue;

                            dboContext.Entry(oldMember).Property(x => x.Active).IsModified = true;
                            dboContext.Entry(oldMember).Property(x => x.UpdatedAt).IsModified = true;
                            dboContext.Entry(oldMember).Property(x => x.UpdatedBy).IsModified = true;

                            oldMember.Active = false;
                            oldMember.UpdatedAt = DateTime.Now;
                            oldMember.UpdatedBy = userId;

                            if ((await dboContext.SaveChangesAsync()) <= 0)
                            {
                                transaction.Rollback();
                                return null;
                            }
                        }
                    }

                    // Obtener todos los perfiles
                    var profiles = await dboContext.pry_profile_project.Where(x => x.Active && x.IdProject == project.Id)
                        .ToListAsync();

                    // Insertar los que son nuevos
                    foreach (var profile in newProject.ProfileProjects)
                    {
                        if (!profiles.Any(x => x.Title == profile.Title && x.Description == profile.Description))
                        {
                            ProfileProject.AddToContext(profile, project.Id, userId, dboContext);
                            if ((await dboContext.SaveChangesAsync()) <= 0)
                            {
                                transaction.Rollback();
                                return null;
                            }
                        }
                    }

                    // Desactivar los que ya no se enviaron
                    foreach (var profile in profiles)
                    {
                        if (!newProject.ProfileProjects.Any(x => x.Title == profile.Title && x.Description == profile.Description))
                        {
                            var oldProfile = await dboContext.pry_profile_project.FirstOrDefaultAsync(x => x.Active
                                && x.Title == profile.Title && x.Description == profile.Description && x.IdProject == profile.IdProject);

                            if (oldProfile == null) continue;

                            dboContext.Entry(oldProfile).Property(x => x.Active).IsModified = true;
                            dboContext.Entry(oldProfile).Property(x => x.UpdatedAt).IsModified = true;
                            dboContext.Entry(oldProfile).Property(x => x.UpdatedBy).IsModified = true;

                            oldProfile.Active = false;
                            oldProfile.UpdatedAt = DateTime.Now;
                            oldProfile.UpdatedBy = userId;

                            if ((await dboContext.SaveChangesAsync()) <= 0)
                            {
                                transaction.Rollback();
                                return null;
                            }
                        }
                    }

                    transaction.Commit();
                    return newProject;
                }
            }
        }

        /// <summary>
        /// Aprueba el proyecto
        /// </summary>
        /// <param name="enrollProject">Modelo del cambio de estado</param>
        /// <param name="userId">Nombre del usuario que aprueba</param>
        /// <returns></returns>
        public static async Task<bool> Approve(EnrollProject enrollProject, string userId)
        {
            using (var dboContext = new DBOContext())
            {
                var project = await dboContext.pry_project.FirstOrDefaultAsync(x => x.Active && x.Id == enrollProject.IdProject);

                if (project == null || project.ChangeStatusDate != null) return false;

                dboContext.Entry(project).Property(x => x.Approved).IsModified = true;
                dboContext.Entry(project).Property(x => x.UpdatedAt).IsModified = true;
                dboContext.Entry(project).Property(x => x.UpdatedBy).IsModified = true;
                dboContext.Entry(project).Property(x => x.IdMicrosoftTeams).IsModified = true;
                dboContext.Entry(project).Property(x => x.ChangeStatusDate).IsModified = true;

                project.UpdatedAt = DateTime.Now;
                project.ChangeStatusDate = DateTime.Now;
                project.UpdatedBy = userId;
                project.Approved = enrollProject.Status;

                if (!enrollProject.Status)
                {
                    if ((await dboContext.SaveChangesAsync()) > 0)
                    {
                        try
                        {
                            var mail = new MailSender();
                            var person = new Person(project.OwnerPidm);
                            mail.compose("pry_generic_template",
                                    new
                                    {
                                        first_name = person.full_name,
                                        message = $"Tu proyecto \"{project.Title}\" fue rechazado."
                                    })
                                .destination($"{person.userName}@continental.edu.pe", "Tu proyecto fue rechazado")
                                .send();
                            return true;
                        }
                        catch
                        {
                            return false;
                        }
                    }
                }

                var principalOwner = await ACTools.MicrosoftGraphRest.User.GetUriUser(WebConfigurationManager.AppSettings["mg-principalowner"]);
                var secondOwner = await ACTools.MicrosoftGraphRest.User.GetUriUser(userId.ToEmailUc());

                var members = await dboContext.pry_project_member.Where(x => x.Active && x.IdMemberType == _projConst.MemberTypeLeader
                    && x.IdProject == enrollProject.IdProject).Select(x => x.Pidm).ToListAsync();

                var leaders = new string[members.Count+1];
                for (var i = 0; i < leaders.Length; i++)
                {
                    if (i >= leaders.Length - 1)
                    {
                        continue;
                    }

                    var user = new Person(members[i]);
                    var userName = string.IsNullOrEmpty(user.userName) ? user.document_number : user.userName;
                    leaders[i] = await ACTools.MicrosoftGraphRest.User.GetUriUser(userName.ToEmailUc());
                }
                leaders[members.Count] = secondOwner;

                var groupId = await ACTools.MicrosoftGraphRest.Group.Create(project.Title, project.Id, project.Description,
                    new[] { principalOwner }, leaders);
                var teamsId = await ACTools.MicrosoftGraphRest.Teams.Create(groupId);
                //var planId = await ACTools.MicrosoftGraphRest.Planner.Create(groupId, project.Title);

                project.IdMicrosoftTeams = teamsId;
                project.IdMicrosoftGroup = groupId;

                if ((await dboContext.SaveChangesAsync()) > 0)
                {
                    try
                    {
                        var mail = new MailSender();
                        var person = new Person(project.OwnerPidm);
                        mail.compose("pry_generic_template",
                                new
                                {
                                    first_name = person.full_name,
                                    message = $"Tu proyecto \"{project.Title}\" fue aprobado."
                                })
                            .destination($"{person.userName}@continental.edu.pe", "Tu proyecto fue aprobado").send();
                    }
                    catch
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        #endregion
    }
}
