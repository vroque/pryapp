﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.PRY;
using ACTools.Mail;
using ACTools.SuperString;
using _projConst = ACTools.Constants.ProjectUCConstants;
using _acadConst = ACTools.Constants.AcademicConstants;

namespace ACBusiness.ContinentalProject
{
    public class EnrollProject
    {
        #region Properties
        public int IdProject { get; set; }
        public int IdProfileProject { get; set; }
        public decimal Pidm { get; set; }
        public bool Status { get; set; }

        #endregion
        #region Methods

        /// <summary>
        /// Verifica si una persona esta inscrita en un proyecto
        /// </summary>
        /// <param name="pidm">¨Pidm de la persona</param>
        /// <param name="idProject">Id del proyecto</param>
        /// <returns></returns>
        public static async Task<bool> GetEnrollByIdProject(decimal pidm, int idProject)
        {
            using (var dboContext = new DBOContext())
            {
                return await dboContext.pry_project_member.AnyAsync(x => x.Active && x.IdProject == idProject && x.Pidm == pidm);
            }
        }

        // TODO: Enviar una respuesta segun lo ocurrido
        /// <summary>
        /// Inscribe a un estudiante en un proyecto
        /// </summary>
        /// <param name="idProfileProject">Id del perfil del proyecto al cual se inscribe</param>
        /// <param name="pidm">Pidm del que se registra</param>
        /// <param name="profile">Perfil academico</param>
        /// <param name="documentNumber">Dni del estudiante</param>
        /// <param name="campus">Campus</param>
        /// <returns></returns>
        public static async Task<bool> Enroll(int idProfileProject, string documentNumber, string campus, decimal pidm, AcademicProfile profile)
        {
            using (var dboContext = new DBOContext())
            {
                var profileProject = await dboContext.pry_profile_project.FirstOrDefaultAsync(x => x.Active
                    && x.Id == idProfileProject);

                if (profileProject == null) return false;

                if (await dboContext.pry_project_member.AnyAsync(x => x.Active && x.IdProfileProject == profileProject.Id
                    && x.IdProject == profileProject.IdProject && x.Pidm == pidm)) return false;

                var projectMember = new PRYProjectMember
                {
                    Active = true,
                    Approved = false,
                    Campus = campus,
                    CreatedAt = DateTime.Now,
                    CreatedBy = documentNumber,
                    Department = profile?.department,
                    Div = profile == null ? _acadConst.DEFAULT_DIV : profile.div,
                    IdProject = profileProject.IdProject,
                    IdProfileProject = idProfileProject,
                    IdMemberType = _projConst.MemberTypeMember,
                    Pidm = pidm,
                    Program = profile?.program.id,
                    UpdatedAt = DateTime.Now,
                    UpdatedBy = documentNumber
                };

                dboContext.pry_project_member.Add(projectMember);

                if ((await dboContext.SaveChangesAsync()) > 0)
                {
                    try
                    {
                        var mail = new MailSender();
                        var person = new Person(projectMember.Pidm);
                        var emailUc = string.IsNullOrEmpty(person.userName) ? person.document_number.ToEmailUc() : person.userName.ToEmailUc();
                        mail.compose("pry_generic_template",
                                new { first_name = person.full_name, message = "Estas en la etapa de seleccion, te matendremos informad@." })
                            .destination(emailUc, $"[ Inscripción ] {profileProject.Title.ToUpper()}").send();
                        return true;
                    }
                    catch { }
                }

                return false;
            }
        }

        /// <summary>
        /// Aprueba una inscripcion a un proyecto
        /// </summary>
        /// <param name="enrollProject">Modelo de los datos de la inscripcion</param>
        /// <param name="userId">Usuario que registra</param>
        /// <returns></returns>
        public static async Task<bool> Approve(EnrollProject enrollProject, string userId)
        {
            using (var dboContext = new DBOContext())
            {
                var projectMember = await dboContext.pry_project_member.FirstOrDefaultAsync(x => x.Active
                    && x.IdProfileProject == enrollProject.IdProfileProject && x.Pidm == enrollProject.Pidm);

                if (projectMember == null) return false;

                var user = new Person(enrollProject.Pidm);
                var groupId = await dboContext.pry_project.Where(x => x.Active && x.Approved && x.Id == projectMember.IdProject)
                    .Select(x => x.IdMicrosoftGroup).FirstOrDefaultAsync();

                if (enrollProject.Status)
                {
                    var person = new Person(projectMember.Pidm);
                    var idPersonToTeams = string.IsNullOrEmpty(person.userName) ? person.document_number : person.userName;
                    var res = await ACTools.MicrosoftGraphRest.User.AddUser(idPersonToTeams, groupId);
                    if (res)
                    {
                        try
                        {
                            var project = await dboContext.pry_project.AsNoTracking().FirstOrDefaultAsync(x => x.Active
                                && x.Id == projectMember.IdProject);
                            if (project == null) return false;
                            var mail = new MailSender();
                            mail.compose("pry_generic_template",
                                    new { first_name = person.full_name, message = $"Bienvenido, ya eres parte del proyecto {project.Title}." })
                                .destination(idPersonToTeams.ToEmailUc(), $"Banco de proyectos - {project.Title.ToUpper()}").send();
                            return true;
                        }
                        catch { }
                    }
                    else
                    {
                        return false;
                    }
                }

                dboContext.Entry(projectMember).Property(x => x.Approved).IsModified = true;
                dboContext.Entry(projectMember).Property(x => x.UpdatedAt).IsModified = true;
                dboContext.Entry(projectMember).Property(x => x.UpdatedBy).IsModified = true;
                dboContext.Entry(projectMember).Property(x => x.ChangeStatusDate).IsModified = true;

                projectMember.UpdatedAt = DateTime.Now;
                projectMember.UpdatedBy = userId;
                projectMember.Approved = enrollProject.Status;
                projectMember.ChangeStatusDate = DateTime.Now;

                return await dboContext.SaveChangesAsync() > 0;
            }
        }

        #endregion
    }
}
