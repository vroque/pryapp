﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.UBIGEO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Ubigeo
{
    /// <summary>
    /// Departementos de Perú
    /// </summary>
    public class UbigeoDepartment
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        private NEXOContext _db = new NEXOContext();

        /// <summary>
        /// Lista todos los departamentos
        /// </summary>
        /// <returns></returns>
        public List<UbigeoDepartment> query()
        {
            List<UbigeoDepartment> list = _db.ubigeo_departament
                .ToList()
                .Select<UBIGEOTblDepartamento, UbigeoDepartment>(f => f)
                .ToList();
            return list;
        }

        public static implicit operator UbigeoDepartment(UBIGEOTblDepartamento obj)
        {
            if (obj == null) return null;
            UbigeoDepartment dep = new UbigeoDepartment();
            dep.id = obj.idDepartament;
            dep.name = obj.departament;
            return dep;
        }
    }
}
