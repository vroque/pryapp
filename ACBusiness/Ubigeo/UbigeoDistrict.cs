﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.UBIGEO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Ubigeo
{
    /// <summary>
    /// Distritps del Perú
    /// </summary>
    public class UbigeoDistrict
    {
        public string id { get; set; }
        public string name { get; set; }
        public string province { get; set; }
        public string department { get; set; }
        private NEXOContext _db = new NEXOContext();

        /// <summary>
        /// Lista todos los distritos
        /// </summary>
        /// <returns></returns>
        public List<UbigeoDistrict> query()
        {
            List<UbigeoDistrict> list = _db.ubigeo_district
                .ToList()
                .Select<UBIGEOTblDistrito, UbigeoDistrict>(f => f)
                .ToList();
            return list;
        }

        /// <summary>
        /// Filtra los distritos por su codigo de provincia
        /// </summary>
        /// <param name="department_id"></param>
        /// <returns></returns>
        public List<UbigeoDistrict> filter(string province_id)
        {
            List<UbigeoDistrict> list = _db.ubigeo_district
                .Where(f => f.idProvince == province_id)
                .ToList()
                .Select<UBIGEOTblDistrito, UbigeoDistrict>(f => f)
                .ToList();
            return list;
        }

        public static implicit operator UbigeoDistrict(UBIGEOTblDistrito obj)
        {
            if (obj == null) return null;
            UbigeoDistrict dist = new UbigeoDistrict();
            dist.id = obj.idDistrict;
            dist.name = obj.district;
            dist.province = obj.idProvince;
            dist.department = obj.idDepartament;
            return dist;
        }
    }
}
