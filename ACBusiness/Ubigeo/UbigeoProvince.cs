﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.UBIGEO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Ubigeo
{
    /// <summary>
    /// Provinvias del Perú
    /// </summary>
    public class UbigeoProvince
    {
        public string id { get; set; }
        public string name { get; set; }
        public string department { get; set; }
        private NEXOContext _db = new NEXOContext();

        /// <summary>
        /// Lista todas las provincias
        /// </summary>
        /// <returns></returns>
        public List<UbigeoProvince> query()
        {
            List<UbigeoProvince> list = _db.ubigeo_province
                .ToList()
                .Select<UBIGEOTblProvincia, UbigeoProvince>(f => f)
                .ToList();
            return list;
        }

        /// <summary>
        /// Filtra las provincias por su codigo de departemento
        /// </summary>
        /// <param name="department_id"></param>
        /// <returns></returns>
        public List<UbigeoProvince> filter(string department_id)
        {
            List<UbigeoProvince> list = _db.ubigeo_province
                .Where( f => f.idDepartament == department_id)
                .ToList()
                .Select<UBIGEOTblProvincia, UbigeoProvince>(f => f)
                .ToList();
            return list;
        }

        public static implicit operator UbigeoProvince(UBIGEOTblProvincia obj)
        {
            if (obj == null) return null;
            UbigeoProvince prov = new UbigeoProvince();
            prov.id = obj.idProvince;
            prov.name = obj.province;
            prov.department = obj.idDepartament;
            return prov;
        }
    }
}
