﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.UBIGEO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Ubigeo
{
    /// <summary>
    /// Paises
    /// </summary>
    public class UbigeoCountry
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        private NEXOContext _db = new NEXOContext();

        /// <summary>
        /// Lista todos los paises
        /// </summary>
        /// <returns></returns>
        public List<UbigeoCountry> query()
        {
            List<UbigeoCountry> list = _db.ubigeo_country
                .ToList()
                .Select<UBIGEOTblCountry, UbigeoCountry>( f => f)
                .ToList();
            return list;
        }

        public static implicit operator UbigeoCountry(UBIGEOTblCountry obj)
        {
            if (obj == null) return null;
            UbigeoCountry country = new UbigeoCountry();
            country.id = obj.idCountry;
            country.name = obj.country;
            country.code = obj.codeCountry;
            return country;
        }
    }
}
