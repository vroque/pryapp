﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.Academic
{
    public class Subject
    {
        public string id { get; set; }
        public string name { get; set; }


        public static Subject getById(string id, string type_study_plan)
        {
            DBOContext db = new DBOContext();
            Subject result = db.tbl_plan_estudio.Where(x => x.IDPlanEstudio == id && x.IDTipoPlanEstudio == type_study_plan).FirstOrDefault();
            return result;
        }
        #region implicits
        public static implicit operator Subject(DBOTblPlanEstudio obj)
        {
            Subject study_plan = new Subject();
            study_plan.id = obj.IDPlanEstudio;
            study_plan.name = obj.Desripcion;
            return study_plan;
        }
        #endregion
    }
}
