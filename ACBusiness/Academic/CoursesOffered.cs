﻿using System;
using ACPermanence.Contexts.BANNER;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.DataBases.BANNER.SATURN;
using Newtonsoft.Json;
using ACTools.SuperString;
using ACPermanence.DataBases.BANNER.BANINST1;
using ACBusiness.AtentionCenter;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using ACBusiness.Extra;
using ACPermanence.Contexts.NEXO;

namespace ACBusiness.Academic
{
    public class CoursesOffered
    {
        public System.DateTime startFech { get; set; }
        public System.DateTime endFech { get; set; }
        public bool active { get; set; }
        public string term { get; set; }
        public List<Offertacademic> offertAcademic { get; set; }

        readonly BNContext _banner = new BNContext();
        readonly NEXOContext dbo = new NEXOContext();

        public List<CoursesOffered> getCourses(AcademicProfile profile)
        {
            ACTerm termObj = new ACTerm().getCurrentEnrollment(profile.campus.id, profile.department);

            List<CoursesOffered> listSubjects = new List<CoursesOffered>();

            List<ACTerm> terms = new List<ACTerm>(ACTerm.getTermByFech(profile.department, profile.campus.id));
            if (terms != null && terms.Count() > 0)
            {
                DisplayBlock display = dbo.cau_display_block.FirstOrDefault(f => f.id == 1);
                foreach (ACTerm item in terms)
                {
                    CoursesOffered subjects = new CoursesOffered();
                    subjects.active = display.active;
                    subjects.startFech = display.startFech;
                    subjects.endFech = display.endFech;
                    subjects.term = item.term;
                    subjects.offertAcademic = _banner.p_asignaturasproy(profile.person_id, profile.program.id, item.term).ToList()
                                              .Select<P_ASIGNATURASPROY, Offertacademic>(f => f).ToList();

                    listSubjects.Add(subjects);
                }
            }
            
            return listSubjects;
        }

        public List<CoursesOffered> getCoursesByRestriction(AcademicProfile profile)
        {
            // asignaturas disponibles id=1
            DisplayBlock display = dbo.cau_display_block.FirstOrDefault(f => f.id == 1);

            List<CoursesOffered> listSubjects = new List<CoursesOffered>();

            if (display != null)
            {
                DateTime today = DateTime.Now;

                if (today >= display.startFech && today <= display.endFech)
                {
                    if (today.TimeOfDay >= display.startFech.TimeOfDay && today.TimeOfDay <= display.endFech.TimeOfDay)
                    {
                        List<ACTerm> terms = new List<ACTerm>(ACTerm.getTermByFech(profile.department, profile.campus.id));
                        if (terms != null && terms.Count() > 0)
                        {
                            foreach (ACTerm item in terms)
                            {
                                CoursesOffered subjects = new CoursesOffered();
                                subjects.active = display.active;
                                subjects.startFech = display.startFech;
                                subjects.endFech = display.endFech;
                                subjects.term = item.term;
                                subjects.offertAcademic = _banner.p_asignaturasproy(profile.person_id, profile.program.id, item.term).ToList()
                                                          .Select<P_ASIGNATURASPROY, Offertacademic>(f => f).ToList();

                                if (display.active)
                                {
                                    listSubjects.Add(subjects);
                                } else
                                {
                                    subjects.offertAcademic= null;
                                    listSubjects.Add(subjects);
                                }
                            }
                        }
                    }
                } 
            }

            return listSubjects;            
        }

        public class Offertacademic
        {
            public int pidm { get; set; }
            public string term { get; set; }
            public string program { get; set; }
            public string departament { get; set; }
            public string campus { get; set; }
            public string cycle { get; set; }
            public string subject { get; set; }
            public string course { get; set; }
            public string nameCourse { get; set; }
            public int? credits { get; set; }
            public string recommended { get; set; }
            public string elective { get; set; }

            #region implicite operators
            public static implicit operator Offertacademic(P_ASIGNATURASPROY query)
            {
                Offertacademic list = new Offertacademic();
                list.pidm = query.pidm;
                list.term = query.term;
                list.course = query.course;
                list.program = query.program;
                list.departament = query.departament;
                list.campus = query.campus;
                list.nameCourse = query.nameCourse;
                list.credits = query.credits;
                list.subject = query.subject;
                list.cycle = query.cycle;
                list.recommended = query.MOST_PROB;
                list.elective = query.ELECTIVE;
                return list;
            }
            #endregion implicite operators
        }
    }
}
