﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.DBUCCI.DBO.views;
using _academic = ACTools.Constants.AcademicConstants;
using _ate = ACTools.Constants.AtentionCenterConstants;
using _admission = ACTools.Constants.AdmissionConstant;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.Academic
{
    /// <summary>
    /// Graduados UC
    /// </summary>
    public class Graduated
    {
        #region Properties

        public int student_pidm { get; set; }
        public string student_id { get; set; }
        public string student_name { get; set; }
        public string entry_period { get; set; }
        public string graduation_period { get; set; }
        public string other_period { get; set; }
        public string other_period_description { get; set; }
        public string school_id { get; set; }
        public string school_name { get; set; }
        public string modality_id { get; set; }
        public string campusId { get; set; }
        public string Catalogue { get; set; }
        public string modality_name { get; set; }
        public int credits_o { get; set; }
        public int credits_e { get; set; }
        public bool volunteering { get; set; }
        public bool foreign_language { get; set; }
        public bool internship { get; set; }
        public bool bachiller { get; set; }
        public bool titulo { get; set; }
        public DateTime? graduation_date { get; set; }
        public bool is_selected { get; set; }
        public int payment_status { get; set; }
        public int academic_status { get; set; }
        public string doc_url { get; set; }
        public bool download { get; set; }
        public string save_as { get; set; }
        public bool Processing { get; set; }
        public bool ProcessingError { get; set; }

        private const string NO_SCHOOL = "0";

        /// <summary>
        /// Estudiante NO Encontrado
        /// </summary>
        const int STUDENT_NOT_FOUND = 0;

        /// <summary>
        /// Estudiante Egresado
        /// </summary>
        private const int STUDENT_GRADUATED = _academic.STATE_GRADUATE;

        /// <summary>
        /// Estudiante NO Egresado
        /// </summary>
        const int STUDENT_NOT_GRADUATED = -_academic.STATE_GRADUATE;

        /// <summary>
        /// Modalidad de postulación "Sin Descripción"
        /// </summary>
        private const decimal NO_ID_MODALIDAD_POSTU = -1;

        #endregion Properties

        #region Methods

        /// <summary>
        /// Lista de Egresados UC según Código de Estudiante
        /// </summary>
        /// <param name="graduates">Arreglo de código de estudiantes</param>
        /// <returns>Lista de Egresados UC</returns>
        public static async Task<List<Graduated>> GetByStudentIdAsync(string[] graduates)
        {
            var dboContext = new DBOContext();

            List<DBODATAlumnoBasico> datosAlumos = await dboContext.view_alumno_basico
                .Where(f => graduates.Contains(f.IDAlumno))
                .ToListAsync();
            List<int> listaPidms = datosAlumos.Select(f => f.IDPersonaN).Distinct().ToList();

            /**
             * Códigos No Encontrados
             */

            string[] noEncontrados = graduates;

            foreach (var alumno in datosAlumos)
            {
                noEncontrados = noEncontrados.Where(w => w != alumno.IDAlumno).ToArray();
            }

            var notFound = new List<Graduated>();
            foreach (var noEncontrado in noEncontrados)
            {
                var graduated = new Graduated
                {
                    student_pidm = 0,
                    student_id = noEncontrado,
                    student_name = "Código No Encontrado",
                    school_id = NO_SCHOOL,
                    academic_status = STUDENT_NOT_FOUND
                };
                notFound.Add(graduated);
            }

            /**
             * Egresados
             */

            var graduados =
                await dboContext.tbl_alumno_egresado
                    .Join(
                        dboContext.view_alumno_basico,
                        tae => new {IDAlumno = tae.IDAlumno},
                        dab => new {IDAlumno = dab.IDAlumno},
                        (tae, dab) => new {tae, dab}
                    ).Where(w => listaPidms.Contains(w.dab.IDPersonaN)).ToListAsync();
            List<Graduated> egresados = graduados
                .Select(s => new Graduated
                {
                    student_pidm = s.dab.IDPersonaN,
                    student_id = s.tae.IDAlumno,
                    student_name = Student.getStudentName(s.tae.IDAlumno),
                    graduation_period = s.tae.IDPerAcad,
                    school_id = s.tae.IDEscuela,
                    school_name = Program.get(s.tae.IDEscuela).name,
                    modality_id = Student.GetCurrentModalityIdBySchoolId(s.tae.IDAlumno, s.tae.IDEscuela),
                    modality_name = Student.GetCurrentModalityNameBySchoolId(s.tae.IDAlumno, s.tae.IDEscuela),
                    campusId = s.tae.IDSede,
                    credits_o = s.tae.CreditosO,
                    credits_e = s.tae.CreditosE,
                    volunteering = s.tae.ProySocial == "1",
                    foreign_language = s.tae.Idioma == "1",
                    internship = s.tae.PracPreProfecio == "1",
                    bachiller = s.tae.Bachiller == "1",
                    titulo = s.tae.Titulado == "1",
                    graduation_date = s.tae.FechaEgresoUC,
                    academic_status = STUDENT_GRADUATED
                })
                .ToList();

            /**
             * No Egresados
             */

            foreach (var graduado in egresados)
            {
                datosAlumos = datosAlumos.Where(f => f.IDPersonaN != graduado.student_pidm).ToList();
            }

            var noEgresados = new List<Graduated>();
            foreach (var alumno in datosAlumos)
            {
                var graduated = new Graduated
                {
                    student_pidm = alumno.IDPersonaN,
                    student_id = alumno.IDAlumno,
                    student_name = alumno.NomCompleto,
                    school_id = NO_SCHOOL,
                    academic_status = STUDENT_NOT_GRADUATED
                };
                noEgresados.Add(graduated);
            }

            return egresados.Union(noEgresados).Union(notFound).OrderBy(o => o.student_name).ToList();
        }

        /// <summary>
        /// Lista de Egresados UC según el código UC.
        /// Si el código ingresado no tiene ninguna carrera terminada, se añade datos con información de postulante
        /// ingresante a la carrera (esta info puede ser diferente a los datos con el cual estudiante se encuentre en
        /// la actualidad. p.e. Cambio en plan de estudios, modalidad, etc)
        /// </summary>
        /// <param name="graduates">Arreglo de código de estudiantes</param>
        /// <returns></returns>
        public static async Task<List<Graduated>> GetByStudentIdIncludedAllEntrySchoolAsync(string[] graduates)
        {
            List<Graduated> graduatedList = await GetByStudentIdAsync(graduates);

            List<Graduated> noGraduated = graduatedList.Where(w => w.academic_status == STUDENT_NOT_GRADUATED).ToList();
            List<Graduated> graduatedAndNotFound =
                graduatedList.Where(w => w.academic_status != STUDENT_NOT_GRADUATED).ToList();

            List<int> listaPidms = noGraduated.Select(f => f.student_pidm).Distinct().ToList();

            var noEgresados = new List<Graduated>();
            foreach (int pidm in listaPidms)
            {
                List<P_MOVIMIENTOSESTUDIANTE> entrants = Student.GetMovimientosEstudiante(pidm).ToList();
                foreach (var entrant in entrants)
                {
                    var graduated = new Graduated
                    {
                        student_id = entrant.CODIGO,
                        student_name = Student.getStudentName(entrant.CODIGO),
                        school_id = entrant.CODIGO_PROGRAMA,
                        school_name = Program.get(entrant.CODIGO_PROGRAMA).name,
                        modality_id = entrant.CODIGO_DEPARTAMENTO,
                        modality_name = entrant.DESCRIPCION_DEPARTAMENTO,
                        entry_period = null,
                        academic_status = STUDENT_NOT_GRADUATED
                    };
                    noEgresados.Add(graduated);
                }
            }

            //captura de códigos que ni estan en egresados ni en postulantes

            foreach (var gNf in graduatedAndNotFound)
            {
                graduates = graduates.Where(w => w != gNf.student_id).ToArray();
            }

            foreach (var entrant in noGraduated)
                //foreach (var entrant in entrants)
            {
                graduates = graduates.Where(w => w != entrant.student_id).ToArray();
            }

            var orphans = new List<Graduated>();
            foreach (var orphan in graduates)
            {
                var graduated = new Graduated
                {
                    student_id = orphan,
                    student_name = Student.getStudentName(orphan),
                    school_id = NO_SCHOOL,
                    academic_status = STUDENT_NOT_GRADUATED
                };
                orphans.Add(graduated);
            }

            return graduatedList.Where(w => w.academic_status != STUDENT_NOT_GRADUATED).Union(noEgresados)
                .Union(orphans).OrderBy(o => o.student_name).ToList();
        }

        /// <summary>
        /// Get all graduates
        /// </summary>
        /// <returns></returns>
        public static async Task<List<Graduated>> GetAllGraduatesAsync()
        {
            var dboContext = new DBOContext();
            var dbGraduates = await dboContext.tbl_alumno_egresado
                .Join(
                    dboContext.view_alumno_basico,
                    ae => new {IDAlumno = ae.IDAlumno},
                    ab => new {IDAlumno = ab.IDAlumno},
                    (ae, ab) => new {ae, ab}).ToListAsync();
            List<Graduated> graduates = dbGraduates
                .Select(s => new Graduated
                {
                    student_id = s.ae.IDAlumno,
                    student_name = s.ab.Nombres,
                    graduation_period = s.ae.IDPerAcad,
                    school_id = s.ae.IDEscuela,
                    campusId = s.ae.IDSede,
                    credits_o = s.ae.CreditosO,
                    credits_e = s.ae.CreditosE,
                    volunteering = s.ae.ProySocial == "1",
                    foreign_language = s.ae.Idioma == "1",
                    internship = s.ae.PracPreProfecio == "1",
                    bachiller = s.ae.Bachiller == "1",
                    titulo = s.ae.Titulado == "1",
                    graduation_date = s.ae.FechaEgresoUC
                })
                .OrderByDescending(o => o.graduation_period)
                .ThenBy(o => o.student_name)
                .ToList();

            foreach (var graduate in graduates)
            {
                graduate.student_name = Student.getStudentName(graduate.student_id);
            }

            return graduates;
        }


        /// <summary>
        /// Get graduates by term
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        public static async Task<List<Graduated>> GetByGraduationTermAsync(string term)
        {
            var dboContext = new DBOContext();
            var apecTerm = Term.toAPEC(term);
            List<DBOTblAlumnoEgresado> dbGraduates = await dboContext.tbl_alumno_egresado
                .Where(w => w.IDPerAcad == apecTerm).ToListAsync();

            List<Graduated> graduates = dbGraduates.Select(s => (Graduated) s).ToList();

            foreach (var graduate in graduates)
            {
                graduate.student_name = Student.getStudentName(graduate.student_id);
            }

            return graduates.OrderBy(o => o.student_name).ToList();
        }

        /// <summary>
        /// Lista de egresados UC de acuerdo a una escuela académica 'schoolId' y período de egreso 'graduationPeriod'
        /// </summary>
        /// <param name="schoolId">Código de la Escuela Académica</param>
        /// <param name="graduationPeriod">Período de Egreso UC</param>
        /// <returns>Lista de egresados UC</returns>
        public static async Task<List<Graduated>> GetBySchoolAndGraduationPeriodAsync(string schoolId,
            string graduationPeriod)
        {
            var dboContext = new DBOContext();
            List<DBOTblAlumnoEgresado> egresados = await dboContext.tbl_alumno_egresado
                .Where(w => w.IDEscuela == schoolId && w.IDPerAcad == graduationPeriod && w.IDDependencia == _corp.UC)
                .ToListAsync();
            List<Graduated> graduates =
                egresados.Select(s => new Graduated
                {
                    student_id = s.IDAlumno,
                    student_name = Student.getStudentName(s.IDAlumno),
                    graduation_period = s.IDPerAcad,
                    school_id = s.IDEscuela,
                    school_name = Program.get(s.IDEscuela).name,
                    academic_status = STUDENT_GRADUATED
                }).OrderBy(o => o.student_name).ToList();
            return graduates;
        }

        /// <summary>
        /// Diccionario con información sobre el último período y última carrera egresada de un estudiante
        /// </summary>
        /// <param name="pidm">PIDM de estudiante</param>
        /// <returns></returns>
        public static DBOTblAlumnoEgresado GetLastGraduationInfo(decimal pidm)
        {
            var dboContext = new DBOContext();
            List<string> studentIdList = Student.GetAllStudentId(pidm);
            var graduated = dboContext.tbl_alumno_egresado.Where(w => studentIdList.Contains(w.IDAlumno))
                .OrderBy(o => o.FechaRegistro).FirstOrDefault();

            return graduated;
        }

        /// <summary>
        /// Update graduate grade
        /// </summary>
        /// <param name="graduates"></param>
        /// <param name="bachelorResolutionDate"></param>
        /// <param name="titleResolutionDate"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static async Task<List<Graduated>> UpdateGraduatesAsync(Graduated[] graduates,
            DateTime? bachelorResolutionDate, DateTime? titleResolutionDate, string user)
        {
            var dboContext = new DBOContext();

            foreach (var graduate in graduates)
            {
                if (!graduate.is_selected || graduate.school_id == NO_SCHOOL || !graduate.Processing) continue;

                var graduateData = await dboContext.tbl_alumno_egresado
                    .FirstOrDefaultAsync(w => w.IDAlumno == graduate.student_id &&
                                              w.IDEscuela == graduate.school_id);
                if (graduateData == null) continue;

                // only update if data is different of database data
                if (graduate.bachiller != (graduateData.Bachiller == "1"))
                {
                    graduateData.Bachiller = graduate.bachiller ? "1" : "0";
                    if (bachelorResolutionDate.HasValue)
                    {
                        graduateData.BAFechaResolucion = bachelorResolutionDate;
                        graduateData.IDPerAcadBA = GetTermForGrade(bachelorResolutionDate ?? DateTime.Now);
                    }

                    if (!graduate.bachiller)
                    {
                        graduateData.BAFechaResolucion = null;
                        graduateData.IDPerAcadBA = null;
                    }
                }

                // only update if data is different of database data
                if (graduate.titulo != (graduateData.Titulado == "1"))
                {
                    graduateData.Titulado = graduate.titulo ? "1" : "0";
                    if (titleResolutionDate.HasValue)
                    {
                        graduateData.TIFechaResolucion = titleResolutionDate;
                        graduateData.IDPerAcadTI = GetTermForGrade(titleResolutionDate ?? DateTime.Now);
                    }

                    if (!graduate.titulo)
                    {
                        graduateData.TIFechaResolucion = null;
                        graduateData.IDPerAcadTI = null;
                    }
                }

                var i = await dboContext.SaveChangesAsync();
                graduate.is_selected = false;
                graduate.Processing = false;
            }

            return graduates.ToList();
        }

        /// <summary>
        /// Get term for bachelor term or title grade term from an parameter
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string GetTermForGrade(DateTime dateTime)
        {
            if (dateTime.Month <= 2)
            {
                return $"{dateTime.Year}-0";
            }

            if (dateTime.Month >= 3 && dateTime.Month <= 7)
            {
                return $"{dateTime.Year}-1";
            }

            return $"{dateTime.Year}-2";
        }


        /// <summary>
        /// Obtener registro de egreso de un estudiante por escuela
        /// </summary>
        /// <param name="pidm">PIDM de estudiante</param>
        /// <param name="schoolId">Codigo de carrera</param>
        /// <returns></returns>
        public static DBOTblAlumnoEgresado GetGraduationByStudentAndSchool(decimal pidm, string schoolId)
        {
            var dboContext = new DBOContext();
            List<string> studentIdList = Student.GetAllStudentId(pidm);
            var graduated = dboContext.tbl_alumno_egresado
                .Where(w => studentIdList.Contains(w.IDAlumno) && schoolId == w.IDEscuela)
                .OrderBy(o => o.FechaRegistro).FirstOrDefault();
            return graduated;
        }

        #endregion Methods

        #region Operators

        public static explicit operator Graduated(DBOTblAlumnoEgresado dbGraduated)
        {
            if (dbGraduated == null) return null;
            var graduated = new Graduated
            {
//                student_pidm = 0,
                student_id = dbGraduated.IDAlumno,
//                student_name = string.Empty,
//                entry_period = string.Empty, 
                graduation_period = dbGraduated.IDPerAcad,
//                other_period = string.Empty,,
//                other_period_description = string.Empty,
                school_id = dbGraduated.IDEscuela,
//                school_name = string.Empty,
                modality_id = dbGraduated.ModalidadEgreso,
                campusId = dbGraduated.IDSede,
//                Catalogue = string.Empty,
//                modality_name = string.Empty,
                credits_o = dbGraduated.CreditosO,
                credits_e = dbGraduated.CreditosE,
                volunteering = dbGraduated.ProySocial == "1",
                foreign_language = dbGraduated.Idioma == "1",
                internship = dbGraduated.PracPreProfecio == "1",
                bachiller = dbGraduated.Bachiller == "1",
                titulo = dbGraduated.Titulado == "1",
                graduation_date = dbGraduated.FechaEgresoUC,
//                is_selected = false,
//                academic_status = 0,
//                doc_url = string.Empty,
//                download = false,
//                save_as = string.Empty,
//                Processing = false,
//                ProcessingError = false
            };

            return graduated;
        }

        #endregion #Operators
    }
}