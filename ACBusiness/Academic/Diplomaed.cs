﻿using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.BANINST1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Academic
{
    /// <summary>
    /// 
    /// </summary>
    public class Diplomaed
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<Course> courses { get; set; }
        readonly BNContext _bn = new BNContext();

        public List<Diplomaed> get(string department, string program, string campus, string term_catalog) {
            List<Diplomaed> diplomaeds = new List<Diplomaed>();

            // eliminamos la consulta para sustituirlka por un procedimiento
            List<SZRCDAB> list = _bn.tbl_SZRCDAB.Where(f =>
                f.SZRCDAB_DEPT_CODE == department
                && f.SZRCDAB_MARJ_CODE == program
                && f.SZRCDAB_CAMP_CODE == campus
                && f.SZRCDAB_TERM_CODE_EFF == term_catalog
                && f.SZRCDAB_COLL_CODE != "AC" // actividades
                && f.SZRCDAB_COLL_CODE != "CN" // ciclo nivelacion
                                               // creo que refiere a cursos de especialidad
                && f.SZRCDAB_AGEN_AREA.Contains("D")
                && f.SZRCDAB_AGEN_REQ_CREDITS != null
            ).ToList();

            List<string> course_areas = list
                .Select(f => f.SZRCDAB_PAAP_AREA)
                .Distinct().ToList();
            //get diplomates codes
            var n = _bn.tbl_SMRALIB.Where(f => course_areas.Contains(f.SMRALIB_AREA))
                .Select(f => new { id= f.SMRALIB_AREA, name= f.SMRALIB_AREA_DESC }).ToList();

            foreach (string dip in course_areas)
            {
                Diplomaed d = new Diplomaed();
                d.id = dip.Substring(3, 3);
                var dp = n.FirstOrDefault(f => f.id == dip);
                d.name = dp == null ? dip : dp.name;
                d.courses = new List<Course>();
                foreach (var item in list.Where(f => f.SZRCDAB_PAAP_AREA.Substring(3, 3) == d.id) ) {
                    Course c = new Course {
                        id = $"{item.SZRCDAB_SUBJ_CODE}{item.SZRCDAB_CRSE_NUMB}",
                        name = item.SZRCDAB_TITLE,
                        subject = item.SZRCDAB_SUBJ_CODE,
                        type = "E",
                        cycle = 0,
                        credits = item.SZRCDAB_CREDIT_HR_HIGH == null ? -1 : (int)item.SZRCDAB_CREDIT_HR_HIGH,
                        is_active = "1"
                    };
                    d.courses.Add(c);
                }
                diplomaeds.Add(d);
            }
            

            return diplomaeds;

        }
        
    }
}
