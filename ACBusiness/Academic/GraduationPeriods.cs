﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.Academic
{
    public class GraduationPeriods
    {
        #region Properties

        public string PeriodId { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Lista de Períodos Académicos de la UC en los que existen graduados
        /// </summary>
        /// <returns>Períodos Académicos de la UC en los que existen graduados</returns>
        public static async Task<IEnumerable<GraduationPeriods>> GetAllAsync()
        {
            var dboContext = new DBOContext();
            List<GraduationPeriods> periods = await dboContext.tbl_alumno_egresado
                .Where(w => w.IDDependencia == _corp.UC)
                .Select(s => new GraduationPeriods {PeriodId = s.IDPerAcad})
                .Distinct().OrderByDescending(o => o.PeriodId).ToListAsync();
            return periods;
        }

        #endregion Methods
    }
}