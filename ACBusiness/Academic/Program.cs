﻿using System.Linq;
using ACPermanence.Contexts.NEXO;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.SATURN;

namespace ACBusiness.Academic
{
    /// <summary>
    /// Escuela Academica ej. 103 Ing Sistemas.
    /// </summary>
    public class Program
    {
        #region Properties

        public string id { get; set; }
        public string name { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Información Básica de la Escuela Académica:
        /// - Id: Código de la Escuela Académica
        /// - Name: Nombre de la Escuela Académica
        /// </summary>
        /// <param name="programId">Código de la Escuela Académica</param>
        /// <returns></returns>
        public static Program get(string programId)
        {
            var bnContext = new BNContext();
            var school = bnContext.tbl_SZVMAJR.FirstOrDefault(w => w.SZVMAJR_CODE == programId);
            return school;
        }

        /// <summary>
        /// Get all UC programs
        /// </summary>
        /// <returns></returns>
        public static async Task<List<Program>> GetAllAsync()
        {
            var bnContext = new BNContext();
            List<SZVMAJR> majors = await bnContext.tbl_SZVMAJR.ToListAsync();

            return majors.Select<SZVMAJR, Program>(s => s).ToList();
        }

        public static List<Program> getByList(List<string> list)
        {
            var bnContext = new BNContext();
            List<Program> schools = bnContext.tbl_SZVMAJR
                .Where(f => list.Contains(f.SZVMAJR_CODE)).ToList()
                .Select<SZVMAJR, Program>(f => f).ToList();
            return schools;
        }

        /// <summary>
        /// Facultad (College) a la que pertenece una Escuela Académica (Program)
        /// </summary>
        /// <param name="programId">Código de Escuela Académica</param>
        /// <returns>Datos de la Facultad a la que pertenece una Escuela Académica</returns>
        public static College getCollege(string programId)
        {
            var nexoContext = new NEXOContext();
            var dimTblCollegeProgram = nexoContext.dim_colleges_programs.FirstOrDefault(w => w.program == programId);

            if (dimTblCollegeProgram == null) return new College {id = string.Empty, name = string.Empty};

            return nexoContext.dim_colleges.Where(w => w.college == dimTblCollegeProgram.college)
                .Select(s => new College
                {
                    id = s.college,
                    name = s.collegeDescription
                })
                .FirstOrDefault();
        }

        #endregion Methods

        #region Operator

        public static implicit operator Program(SZVMAJR obj)
        {
            if (obj == null) return null;
            var academicSchool = new Program
            {
                id = obj.SZVMAJR_CODE,
                name = obj.SZVMAJR_DESCRIPTION
            };
            return academicSchool;
        }

        #endregion Operator
    }
}