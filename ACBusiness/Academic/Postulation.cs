﻿using ACBusiness.AtentionCenter;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BANNER;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.DIM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;
using System.Data.Entity;


namespace ACBusiness.Academic
{
    public class Postulation
    {
        #region properties
        public string div { get; set; }
        public string campus_id { get; set; }
        public string term { get; set; }
        public string levl { get; set; }
        public string college_id { get; set; }
        public string program_id { get; set; }
        public string department { get; set; }
        public string adm_type_id { get; set; }
        public decimal pidm { get; set; }
        public int state { get; set; }
        public DateTime date { get; set; }
        
        readonly DBOContext _apec = new DBOContext();
        #endregion properties

        #region methods

        /// <summary>
        /// Traemos las postulaciones desde APEC
        /// ** debido a que las posutlaciones no estan en banner
        /// *** modificar una vez la postulaciones esten en BANNER 
        /// *** aprox 2019
        /// </summary>
        /// <param name="student_id">DNI o codigo del estudiante</param>
        /// <returns></returns>
        public List<Postulation> list(string student_id)
        {
            List<Postulation> postulations = new List<Postulation>();
            var pos = _apec.tbl_postulante.Join(
                    _apec.tbl_alumno_estado,
                    p => new { st = p.IDAlumno, term = p.IDPerAcad, p = p.IDEscuelaADM, div = p.IDDependencia, f = p.IDExamen },
                    e => new { st = e.IDAlumno, term = e.IDPerAcad, p = e.IDEscuela, div = e.IDDependencia, f = e.FecInic },
                    (p, e) => new { p, e }
                ).Join(
                _apec.view_alumno_basico,
                    t => new { id = t.p.IDAlumno },
                    a => new { id = a.IDAlumno },
                    (t, a) => new { t.p, t.e, a }
                ).Where(f =>
                    f.a.DNI == student_id
                    && (
                        f.p.IDEscuelaADM == _acad.DEPA_PGT_APEC ||
                        f.p.IDEscuelaADM == _acad.DEPA_REG_APEC ||
                        f.p.IDEscuelaADM == _acad.DEPA_VIR_APEC
                    )
                    && f.p.Renuncia == "0"
                    && f.p.IDDependencia == _acad.DEFAULT_DIV
                ).ToList().OrderByDescending(f => f.p.Ingresante).ToList();

            // cambiar el tipo de modalidad de admision Term.toBanner()
            string last_term = pos
                .Select(f => f.p.IDPerAcad)
                .OrderByDescending(f => f)
                .FirstOrDefault();
            // todos las postulaciones del ultimo periodo
            pos = pos.Where(f => f.p.IDPerAcad == last_term).ToList();

            //buscamos si alguna de estas postulaciones estan en el periodo actual o superior
            foreach (var item in pos) {
                //si lo se esto es estupido T_T
                
                string department_obj = Department.toBanner(item.p.IDEscuelaADM);
                string term_postulation = Term.toBanner(item.p.IDPerAcad);
                if (item.p.IDEscuelaADM == "ADV")
                    item.e.IDSede = "VIR";
                Campus campus = Campus.getByAPECCode(item.e.IDSede);

                ACTerm term_current = new ACTerm().getCurrent(campus.id, department_obj);
                if(term_postulation.CompareTo(term_current.term) >= 0)
                {
                    Postulation postulation = new Postulation();
                    postulation.div = item.p.IDDependencia;
                    postulation.campus_id = campus.id;
                    postulation.term = term_postulation;
                    postulation.levl = _acad.DEFAULT_LEVL;
                    postulation.college_id = College.getByProgramID(item.p.IDEscuela).id;
                    postulation.program_id = item.p.IDEscuela;
                    postulation.department = department_obj;
                    postulation.adm_type_id = item.p.IDModalidadPostu.ToString();
                    postulation.state = item.p.Ingresante == "1"?_acad.STATE_ENROLMENT: _acad.STATE_POSTULANT;
                    postulation.date = item.p.FechaInscripcion;
                    postulations.Add(postulation);
                }
            }
            return postulations;
        }

        /// <summary>
        /// Obtiene la ulta postulacion a la que ingreso segun la carrera y la 
        /// modalidad
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        public Postulation get(AcademicProfile profile)
        {
            List<string> student_id = Student.ListAPECStudentCodesFromPidm(profile.person_id);
            string department_apec = Department.toAPEC(profile.department);

            // validar si su postulacion es
            var pos = _apec.tbl_postulante.Join(
                    _apec.tbl_alumno_estado,
                    p => new { term = p.IDPerAcad, p = p.IDEscuelaADM, div = p.IDDependencia },
                    e => new { term = e.IDPerAcad, p = e.IDEscuela, div = e.IDDependencia },
                    (p, e) => new { p, e }
                ).Join(
                    _apec.tbl_equivalencia_modalidad_admision,
                    pe => new { adm = pe.p.IDModalidadPostu.ToString(), dep = pe.p.IDEscuelaADM },
                    eq => new { adm = eq.IDModalidadPostu, dep = eq.departament },
                    (pe, eq) => new { pe.p, pe.e, eq }
                ).Where(f =>
                    student_id.Contains(f.p.IDAlumno)
                    && f.p.IDEscuela == profile.program.id
                    && f.p.IDEscuelaADM == department_apec
                    && f.p.Ingresante == "1"
                    && f.p.Renuncia == "0"
                    && f.p.IDDependencia == _acad.DEFAULT_DIV
                ).OrderByDescending(f => f.p.IDPerAcad)
                .ThenByDescending(f => f.p.IDExamen)
                .FirstOrDefault();
            // si no hay postulacion validar el tipo de admision maximos
            // ... si esto esta terriblemente mal
            if(pos == null)
            {
                pos = _apec.tbl_postulante.Join(
                   _apec.tbl_alumno_estado,
                   p => new { term = p.IDPerAcad, p = p.IDEscuelaADM, div = p.IDDependencia },
                   e => new { term = e.IDPerAcad, p = e.IDEscuela, div = e.IDDependencia },
                   (p, e) => new { p, e }
               ).Join(
                   _apec.tbl_equivalencia_modalidad_admision,
                   pe => new { adm = pe.p.IDModalidadPostu.ToString(), dep = pe.p.IDEscuelaADM },
                   eq => new { adm = eq.IDModalidadPostu, dep = eq.departament },
                   (pe, eq) => new { pe.p, pe.e, eq }
               ).Where(f =>
                   student_id.Contains(f.p.IDAlumno)
                   //&& f.p.IDEscuela == profile.program.id
                   && f.eq.IDModRecruiter == profile.adm_type.id
                   && f.p.IDEscuelaADM == department_apec
                   && f.p.Ingresante == "1"
                   //&& f.p.Renuncia == "0"
                   && f.p.IDDependencia == _acad.DEFAULT_DIV
               ).OrderByDescending(f => f.p.IDPerAcad)
               .ThenByDescending(f => f.p.IDExamen)
               .FirstOrDefault();
            }

            Postulation postulation = new Postulation();
            postulation.div = pos.p.IDDependencia;
            postulation.campus_id = profile.campus.id;
            postulation.term = Term.toBanner(pos.p.IDPerAcad);
            postulation.levl = _acad.DEFAULT_LEVL;
            postulation.college_id = profile.college.id;
            postulation.program_id = profile.program.id;
            postulation.department = profile.department;
            // se ignora la postulacio de apec y se usa la de recruiter/banner
            postulation.adm_type_id = pos.eq.IDModRecruiter;
            postulation.state = pos.p.Ingresante == "1" ? _acad.STATE_ENROLMENT : _acad.STATE_POSTULANT;
            postulation.date = pos.p.IDExamen;
                
            return postulation;
        }
        #endregion methods

        #region implicite operatos
        public static implicit operator Postulation(PPostulacionesXAlumno obj)
        {
            if (obj == null) return null;
            Postulation p = new Postulation();
            p.div = obj.DIVS;
            p.campus_id = obj.CAMPUS;
            p.term = obj.TERM;
            p.levl = obj.LEVL;
            p.college_id = obj.COLLEGE;
            p.program_id = obj.PROGRAM;
            p.department = obj.DEPARTMENT;
            p.adm_type_id = obj.ADMTYPE;
            p.pidm = obj.PIDM;
            p.date = obj.REGISTRATIONDATE;

            return p;
        }
        public static implicit operator Postulation(DIMTblPostulant obj)
        {
            if (obj == null) return null;
            Postulation p = new Postulation();
            p.div = obj.divs;
            p.campus_id = obj.campus;
            p.term = obj.term;
            p.levl = obj.levl;
            p.college_id = obj.college;
            p.program_id = obj.program;
            p.department = obj.departament;
            p.adm_type_id = obj.admType;
            p.pidm = obj.pidm;
            p.date = obj.registrationDate;

            return p;
        }
        #endregion implicite operatos
    }
}
