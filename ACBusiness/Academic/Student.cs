﻿using ACBusiness.Accounts;
using ACBusiness.Personal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.BANNER;
using ACTools.SuperString;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.DBUCCI.DBO.views;
using _admission = ACTools.Constants.AdmissionConstant;
using _acad = ACTools.Constants.AcademicConstants;
using _s = ACTools.strings.Errors;

namespace ACBusiness.Academic
{
    /// <author>Arturo Bolaños</author>
    /// <date>16/09/2016</date>
    /// <summary>
    /// Clase de Estudiante
    /// Esta clase contine la informacion del estudiante
    /// </summary>
    public class Student
    {
        #region properties

        /// <summary>
        /// DNI del estudiante
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Pidm del estudiante
        /// </summary>
        public decimal person_id { get; set; }

        /// <summary>
        /// Nombres del estudiante
        /// </summary>
        public string first_name { get; set; }

        /// <summary>
        /// Apellidos (paterno + materno)
        /// </summary>
        public string last_name { get; set; }

        /// <summary>
        /// Nombre completo (apellidos + nombre(s))
        /// </summary>
        public string full_name => $"{first_name} {last_name}".toEachUpperClean();

        /// <summary>
        /// Id del alumno en la vista de estudiantes (BDUCCI)
        /// </summary>
        public string IdAlumno { get; set; }

        public string gender { get; set; }
        public string scala { get; set; }

        public List<AcademicProfile> profiles { get; set; }
        public AcademicProfile profile { get; set; }

        private readonly DBOContext _dboContext = new DBOContext();

        #endregion properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public Student()
        {
        }

        /// <summary>
        /// Carga la infromacion academica de un estudiante mediante
        /// su codigo(DNI)
        /// </summary>
        /// <param name="student_id">Codigo de Alumno</param>
        public Student(string student_id)
        {
            this.syncByStudentCode(student_id);
        }

        /// <summary>
        /// Carga la infromacion academica de un estudiante mediante
        /// su pidm (codigo de persona)
        /// </summary>
        /// <param name="pidm">Código de persona</param>
        public Student(decimal pidm)
        {
            this.sync(pidm);
        }

        #endregion

        #region methods

        /// <summary>
        /// Busca los datos Personales basados en su codigo de persona (PIDM)
        /// y todas las carreras que estudia o estudio.
        /// </summary>
        /// <param name="person_id"></param>
        public void sync(decimal person_id)
        {
            Person person = new Person(person_id);
            this.id = person.document_number;
            // se busca en la tabla de alumnos debiado a que el caso de los extranjeros
            // su codigo de estudiantes no es su codigo de carné de extranjeria 
            if (person.document_type != "DNI")
            {
                int pidm = (int) person_id;
                DBOContext dbo = new DBOContext();
                DBODATAlumnoBasico student = dbo.view_alumno_basico
                    .FirstOrDefault(f => f.IDPersonaN == pidm);
                this.id = student.IDAlumno;
            }

            this.person_id = person.id;
            this.first_name = person.first_name;
            this.last_name = person.last_name;
            this.gender = person.gender;

            this.profiles = AcademicProfile.listProfiles(person_id);
        }

        /// <summary>
        /// Busca los datos Personales basados en su codigo de estudiante
        /// y todas las carreras que estudia o estudio.
        /// </summary>
        /// <param name="student_id">Codigo de Alumno</param>
        public void syncByStudentCode(string student_id)
        {
            Person person_obj = new Person();
            Person person = person_obj.getByStudentCode(student_id);
            // se concidera el codigo de estudiantes debido a que en el caso de los extranjeros
            // su codigo de estudiantes esta en el campo DNI 
            // y su codigo de Carné de extranjeria esta en el campo CE
            this.id = student_id;
            this.person_id = person.id;
            this.first_name = person.first_name;
            this.last_name = person.last_name;
            this.gender = person.gender;
            //buscamos sus posibles postulaciones
            this.profiles = AcademicProfile.listProfiles(person_id);
        }

        /// <summary>
        /// Establece el perfil por defecto en base a los parametros
        /// </summary>
        /// <param name="div">Dependencia</param>
        /// <param name="levl">Nivel: Preprago, postgrado</param>
        /// <param name="program">Carrera</param>
        public void syncProfile(string div, string levl, string program)
        {
            AcademicProfile prof = this.profiles.FirstOrDefault
            (f =>
                f.div == div
                && f.levl == levl
                && f.program.id == program
            );
            this.profile = prof;
        }

        public void syncProfile(string program)
        {
            string div = _acad.DEFAULT_DIV;
            string levl = _acad.DEFAULT_LEVL;
            AcademicProfile prof = this.profiles.FirstOrDefault
            (f =>
                f.div == div
                && f.levl == levl
                && f.program.id == program
            );
            this.profile = prof;
        }

        public async Task<List<Debt>> debts()
        {
            Debt debt = new Debt();
            return await debt.query(this.person_id);
        }

        public List<Student> searchStudents(string[] students)
        {
            string fullName = string.Join(" ", students);
            List<DBODATAlumnoBasico> test = _dboContext.view_alumno_basico.Where(f =>
                    f.NomCompleto.Contains(fullName)
                    && f.DNI != null
                    && f.DNI == f.IDAlumno // estudiantes con codigo nuevo (DNI == codigo de estudiante)
            ).ToList();
            var listDni = test.Select(f => f.DNI).Distinct().ToArray().Take(10);
            List<Student> listStudents = new List<Student>();

            foreach (string item in listDni)
            {
                Student student = new Student();
                var person = test.FirstOrDefault(f => f.DNI == item);
                student.person_id = person.IDPersonaN;
                student.id = person.DNI;
                student.first_name = person.Nombres;
                student.last_name = $"{person.ApPat} {person.ApMat}";
                listStudents.Add(student);
            }

            return listStudents;
        }

        public Boolean validateDNI(string dni)
        {
            Boolean valid = false;
            DBODATAlumnoBasico test = _dboContext.view_alumno_basico.FirstOrDefault(f =>
                    f.DNI != null
                    && f.DNI == dni // estudiantes con codigo nuevo (DNI == codigo de estudiante)
            );
            if (test != null)
            {
                valid = true;
            }

            return valid;
        }

        #endregion

        #region static methods   

        /// <summary>
        /// Verifica si un código de estudiante existe
        /// </summary>
        /// <param name="studentId">Código de estudiante</param>
        /// <returns></returns>
        public static bool ExistStudent(string studentId)
        {
            var dboContext = new DBOContext();
            return dboContext.view_alumno_basico.Any(w => w.IDAlumno == studentId);
        }

        public static async Task<decimal> getPidmFromStudentID(string student_id)
        {
            DBOContext db = new DBOContext();
            DBOTblPersona person = await db.tbl_persona.FirstOrDefaultAsync(
                f => f.DNI == student_id);
            if (person == null)
                throw new ArgumentNullException(_s.NO_PERSON);

            return (decimal) person.IDPersonaN;
        }

        /// <summary>
        /// Obtiene el PIDM(person_id) desde el codigo de estudiante
        /// </summary>
        /// <param name="studentId">Código de Estudiante</param>
        /// <returns>PIDM</returns>
        public static decimal getPidmFromStudentId(string studentId)
        {
            var dboContext = new DBOContext();
            DBODATAlumnoBasico person = dboContext.view_alumno_basico.FirstOrDefault(f => f.IDAlumno == studentId);
            if (person == null) throw new ArgumentNullException(_s.NO_STUDENT);

            return person.IDPersonaN;
        }

        /// <summary>
        /// Obtiene el Pidm( person_id) desde el DNI (codigo de estudiante)
        /// </summary>
        /// <param name="student_id"></param>
        /// <returns></returns>
        public static decimal getPidmFromOldCode(string old_code)
        {
            DBOContext db = new DBOContext();
            DBODATAlumnoBasico person = db.view_alumno_basico.FirstOrDefault(
                f => f.IDAlumno == old_code);
            if (person == null)
                throw new ArgumentNullException(_s.NO_STUDENT);

            return (decimal) person.IDPersonaN;
        }

        /// <summary>
        /// Obtiene el DNI (codigo de estudiante) desde el pidm (person_id)
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns>Codigo de estudiante</returns>
        public static string getStudentCodeFromPidm(decimal pidm)
        {
            DBOContext db = new DBOContext();
            DBODATAlumnoBasico person = db.view_alumno_basico.FirstOrDefault(
                f => f.IDPersonaN == pidm);
            if (person == null)
                throw new ArgumentNullException(_s.NO_STUDENT);
            return person.DNI;
        }

        /// <summary>
        /// Obtiene la información básica del estudiante (Dni, apellidos, nombres y pidm)
        /// </summary>
        /// <param name="pidm">Pidm del estudiante</param>
        /// <returns></returns>
        public static Student GetStudent(decimal pidm)
        {
            using (var dboContext = new DBOContext())
            {
                // Por lo general el IDAlumno debería ser el DNI del estudiante, en los registros de la BD si el
                // estudiante tiene más de un IDAlumno el último es su DNI.
                var student = dboContext.view_alumno_basico.AsNoTracking().Where((x => x.IDPersonaN == pidm))
                    .ToList().LastOrDefault();

                if (student == null)
                    throw new ArgumentNullException(_s.NO_STUDENT);

                return new Student
                {
                    id = student.DNI,
                    first_name = student.Nombres,
                    last_name = $"{student.ApPat} {student.ApMat}",
                    person_id = student.IDPersonaN,
                    IdAlumno = student.IDAlumno
                };
            }
        }

        /// <summary>
        /// Obtiene el DNI (codigo de estudiante) desde el pidm (person_id)
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns>Codigo de estudiante</returns>
        public static List<string> ListAPECStudentCodesFromPidm(decimal pidm)
        {
            DBOContext db = new DBOContext();
            List<string> codes = db.view_alumno_basico
                .Where(
                    f => f.IDPersonaN == pidm)
                .Select(f => f.IDAlumno)
                .ToList();
            if (codes == null)
                throw new ArgumentNullException(_s.NO_STUDENT);
            return codes;
        }

        /// <summary>
        /// Último movimiento a partir de una lista de movimientos
        /// </summary>
        /// <param name="mov">Movimiento</param>
        /// <returns></returns>
        public static P_MOVIMIENTOSESTUDIANTE GetLastMovimientoEstudiante(List<P_MOVIMIENTOSESTUDIANTE> mov)
        {
            var lastMov = mov.FirstOrDefault(w => w.PERIODO_FIN == "ACTIVO");

            if (lastMov != null) return lastMov;

            lastMov = mov.OrderByDescending(o => o.PERIODO_INICIO).FirstOrDefault();

            return lastMov;
        }

        /// <summary>
        /// Info sobre movimientos del estudiante.
        /// Lista de carreras con info de sede, modalidad estudio, último código de estudiante asociado a cada carrera.
        /// El estado "ACTIVO" esta asociado al último movimiento realizado por el estudiante.
        /// Así, si el estudiante se encuentra estudiando una carrera adicional, el estado "ACTIVO" irá en referencia
        /// a esta última carrera.
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public static List<P_MOVIMIENTOSESTUDIANTE> GetMovimientosEstudiante(decimal pidm)
        {
            return new BNContext().p_movimientos_estudiante(pidm).ToList();
        }

        /// <summary>
        /// Info sobre último movimiento de estudiante
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public static P_MOVIMIENTOSESTUDIANTE GetLastMovimientoEstudiante(decimal pidm)
        {
            return GetMovimientosEstudiante(pidm).FirstOrDefault(w => w.PERIODO_FIN == "ACTIVO");
        }

        /// <summary>
        /// Info sobre movimientos del estudiante de acuerdo a una carrera
        /// </summary>
        /// <param name="pidm">PIDM de estudiante</param>
        /// <param name="schoolId">Código de escuela académica</param>
        /// <returns></returns>
        public static List<P_MOVIMIENTOSESTUDIANTE> GetMovimientosEstudianteBySchoolId(decimal pidm,
            string schoolId)
        {
            return GetMovimientosEstudiante(pidm).Where(w => w.CODIGO_PROGRAMA == schoolId).ToList();
        }

        /// <summary>
        /// Infor sobre el último movimiento del estudiante en relación a una carrera
        /// </summary>
        /// <param name="pidm"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public static P_MOVIMIENTOSESTUDIANTE GetLastMovimientoEstudianteBySchoolId(decimal pidm, string schoolId)
        {
            var allMovEstudiante = GetMovimientosEstudianteBySchoolId(pidm, schoolId);

            var movEstudiante = allMovEstudiante.FirstOrDefault(w => w.PERIODO_FIN == "ACTIVO");

            if (movEstudiante != null) return movEstudiante;

            /**
             * Debido a que el estado "ACTIVO" hace mención al último movimiento, para estudiantes egresados
             * que se encuentren estudiando una carrera adicional y se requiera saber el último estado de la carrera
             * egresada, se tiene que sacar el último movimiento relacionado a la carrera.
             */
            movEstudiante = allMovEstudiante.OrderByDescending(o => o.PERIODO_FIN)
                .FirstOrDefault();

            if (movEstudiante == null)
            {
                throw new NullReferenceException(
                    $"No se encontraron movimientos del estudiante {pidm} en la escuela {schoolId}");
            }

            return movEstudiante;
        }

        /// <summary>
        /// Último movimiento estudiante pro escuela y período académico
        /// </summary>
        /// <param name="pidm"></param>
        /// <param name="schoolId"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        public static P_MOVIMIENTOSESTUDIANTE GetMovimientoEstudianteBySchoolIdAndTerm(decimal pidm, string schoolId,
            int term)
        {
            List<P_MOVIMIENTOSESTUDIANTE> mov = GetMovimientosEstudianteBySchoolId(pidm, schoolId)
                .Where(w => int.Parse(w.PERIODO_INICIO) <= term).ToList();

            return GetLastMovimientoEstudiante(mov);
        }

        /// <summary>
        /// Modalidad de estudios (formerly IDEscuelaADM)
        /// </summary>
        /// <param name="studentId">Código de estudiante</param>
        /// <param name="schoolId">Código de escuela académica</param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public static string GetCurrentModalityIdBySchoolId(string studentId, string schoolId)
        {
            var pidm = getPidmFromStudentId(studentId);
            var movEstudiante = GetLastMovimientoEstudianteBySchoolId(pidm, schoolId);

            return movEstudiante.CODIGO_DEPARTAMENTO;
        }

        /// <summary>
        /// Nombre de modalidad de estudios (nombre de IDEscuelaADM)
        /// </summary>
        /// <param name="studentId">Código de estudiante</param>
        /// <param name="schoolId">Código de escuela académica</param>
        /// <returns></returns>
        public static string GetCurrentModalityNameBySchoolId(string studentId, string schoolId)
        {
            var modalityId = GetCurrentModalityIdBySchoolId(studentId, schoolId);
            return !string.IsNullOrEmpty(modalityId) ? _admission.MODALITY_OF_ADMISSION[modalityId] : string.Empty;
        }

        /// <summary>
        /// Información de Movimiento de Estudiante en un determinado período académico
        /// </summary>
        /// <param name="pidm"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        public static Movement GetMovementInfoByTerm(decimal pidm, string term)
        {
            var mov = GetMovimientosEstudiante(pidm).ToList();
            var t = int.Parse(term);
            var movement = mov
                .Select<P_MOVIMIENTOSESTUDIANTE, Movement>(s => s)
                .FirstOrDefault(w => w.BeginTerm <= t && w.EndTerm >= t);

            return movement;
        }

        /// <summary>
        /// Obtiene Nombre del estudiante a partir de un Código de estudiante
        /// </summary>
        /// <param name="student_id">Código de estudiante</param>
        /// <returns>Nombre completo del estudiante</returns>
        public static string getStudentName(string student_id)
        {
            DBOContext dbo_context = new DBOContext();
            DBODATAlumnoBasico dbo_dat_alumno_basico =
                dbo_context.view_alumno_basico.FirstOrDefault(w => w.IDAlumno == student_id);
            return dbo_dat_alumno_basico != null
                ? $"{dbo_dat_alumno_basico.ApPat} {dbo_dat_alumno_basico.ApMat} {dbo_dat_alumno_basico.Nombres}"
                : string.Empty;
        }

        /// <summary>
        /// Lista de todos los códigos de estudiantes asociados a una persona a partir de otro código de estudiante
        /// </summary>
        /// <param name="studentId">Código de estudiante</param>
        /// <returns>Lista de códigos de estudiante asociados a una persona</returns>
        public static List<string> GetAllStudentId(string studentId)
        {
            var dboContext = new DBOContext();
            var student = dboContext.view_alumno_basico.FirstOrDefault(w => w.IDAlumno == studentId);

            if (student == null) return new List<string> {studentId};

            return dboContext.view_alumno_basico.Where(w => w.IDPersonaN == student.IDPersonaN)
                .Select(s => s.IDAlumno).ToList();
        }

        /// <summary>
        /// Lista de todos los códigos de estudiantes asociados a una persona a partir de un código PIDM
        /// </summary>
        /// <param name="studentPidm">Código PIDM del estudiante</param>
        /// <returns></returns>
        public static List<string> GetAllStudentId(decimal studentPidm)
        {
            var dboContext = new DBOContext();
            var student = dboContext.view_alumno_basico.FirstOrDefault(w => w.IDPersonaN == studentPidm);

            if (student == null) return null;

            return dboContext.view_alumno_basico.Where(w => w.IDPersonaN == student.IDPersonaN)
                .Select(s => s.IDAlumno).ToList();
        }


        /// <summary>
        /// Lista de todos los códigos de estudiantes asociados a un estudiante-carrera
        /// </summary>
        /// <param name="pidm">Código de persona BANNER</param>
        /// <param name="schoolId">Código de Escuela Académica</param>
        /// <returns></returns>
        public static List<string> getAllStudentIdRelatedToSchoolId(decimal pidm, string schoolId)
        {
            int p_id = Convert.ToInt32(pidm);
            DBOContext dbo_context = new DBOContext();
            var student_id_list = dbo_context.view_alumno_basico.Where(w => w.IDPersonaN == p_id)
                .Select(s => s.IDAlumno).ToList();

            return student_id_list;
        }

        /// <summary>
        /// DNI de Estudiante
        /// </summary>
        /// <param name="student_id">Código de estudiante</param>
        /// <returns>DNI de estudiante</returns>
        public static string getDNI(string student_id)
        {
            DBOContext dbo_context = new DBOContext();
            var student = dbo_context.view_alumno_basico.FirstOrDefault(w => w.IDAlumno == student_id);
            return student?.DNI;
        }

        /// <summary>
        /// Información relacionadas al género del estudiante para ser usados en los documentos de la UC.
        /// p.e.: "el Sr.", "la Sra(ita).", "del interesado", "de la interesada"
        /// </summary>
        /// <param name="student_id">Código de estudiante</param>
        /// <returns></returns>
        public static Dictionary<string, string> getGenderData(string student_id)
        {
            DBOContext dbo_context = new DBOContext();
            var dbo_dat_alumno_basico = dbo_context.view_alumno_basico.FirstOrDefault(w => w.IDAlumno == student_id);

            if (dbo_dat_alumno_basico == null)
            {
                throw new ArgumentNullException(_s.NO_STUDENT);
            }

            string gender_id = dbo_dat_alumno_basico.Sexo;
            switch (gender_id)
            {
                case "0": // FEMENINO
                    return new Dictionary<string, string>
                    {
                        {"civil_status", "la Sra(ita)."},
                        {"identified", "identificada"},
                        {"enrolled", "matriculada"},
                        {"concerned", "de la interesada"},
                        {"indicated", "la indicada"},
                        {"graduated", "EGRESADA"}
                    };
                case "1": // MASCULINO
                    return new Dictionary<string, string>
                    {
                        {"civil_status", "el Sr."},
                        {"identified", "identificado"},
                        {"enrolled", "matriculado"},
                        {"concerned", "del interesado"},
                        {"indicated", "el indicado"},
                        {"graduated", "EGRESADO"}
                    };
                default:
                    return new Dictionary<string, string>
                    {
                        {"civil_status", "el Sr(a)."},
                        {"identified", "identificado(a)"},
                        {"enrolled", "matriculado(a)"},
                        {"concerned", "del interesado(a)"},
                        {"indicated", "el indicado(a)"},
                        {"graduated", "EGRESADO(A)"}
                    };
            }
        }

        /// <summary>
        /// Ciclo del estudiante en un determinado período académico.
        /// </summary>
        /// <param name="academicPeriod">Período académico</param>
        /// <param name="profile">Perfil del estudiante</param>
        /// <returns>Ciclo de un estudiante en un determinado período académico</returns>
        /// <exception cref="Exception"></exception>
        public static int cycleByAcademicPeriod(string academicPeriod, AcademicProfile profile)
        {
            BNContext bn_context = new BNContext();

            ACTerm term = new ACTerm().get(academicPeriod, profile.campus.id, profile.department);

            if (academicPeriod != term.term)
            {
                academicPeriod = $"{academicPeriod.Substring(0, 5)}5";
            }

            List<PCicloCreditosAlumno> creditos_alumnos =
                bn_context.p_creditos_alumno(profile.person_id, academicPeriod, profile.program.id).ToList();
            PCicloCreditosAlumno ciclo_creditos_alumno = creditos_alumnos.FirstOrDefault();
            if (ciclo_creditos_alumno == null)
            {
                throw new ArgumentNullException(
                    string.Format(_s.NO_INFO_IN_PERIOD, "ciclo", academicPeriod)
                );
            }

            int cycle = 0;
            int.TryParse(ciclo_creditos_alumno.NOMINALCYCLE.ToString(), out cycle);
            return cycle;
        }

        #endregion

        public class Movement
        {
            #region Properties

            public string StudentId { get; set; }
            public decimal Pidm { get; set; }
            public string CampusId { get; set; }
            public string CollegeId { get; set; }
            public string ProgramId { get; set; }
            public string DepartmentId { get; set; }
            public string Catalog { get; set; }
            public int BeginTerm { get; set; }
            public int EndTerm { get; set; }
            public string AdmissionModality { get; set; }

            #endregion Properties

            #region Implicit Operators

            public static implicit operator Movement(P_MOVIMIENTOSESTUDIANTE mov)
            {
                if (mov == null) return null;

                var movement = new Movement
                {
                    StudentId = mov.CODIGO,
                    Pidm = mov.PIDM,
                    CampusId = mov.CODIGO_CAMPUS,
                    CollegeId = mov.CODIGO_FACULTAD,
                    ProgramId = mov.CODIGO_PROGRAMA,
                    DepartmentId = mov.CODIGO_DEPARTAMENTO,
                    Catalog = mov.CATALOGO_REAL,
                    BeginTerm = int.Parse(mov.PERIODO_INICIO),
                    EndTerm = mov.PERIODO_FIN == "ACTIVO"
                        ? int.Parse($"{DateTime.Now.Year + 1}00")
                        : int.Parse(mov.PERIODO_FIN),
                    AdmissionModality = mov.CODIGO_MOD_ADM
                };

                return movement;
            }

            #endregion Implicit Operators
        }
    }
}