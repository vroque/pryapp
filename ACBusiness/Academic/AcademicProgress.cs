﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;

namespace ACBusiness.Academic
{
    public class AcademicProgress
    {
        public decimal student_id { get; set; }
        public string study_plan { get; set; }
        public List<Course> courses { get; set; }
        readonly BNContext _bn = new BNContext();

        public AcademicProgress get(AcademicProfile profile){
            AcademicProgress p = new AcademicProgress();

            List<PHistoriaXCarreraAlumno> list = _bn.p_historia_carrera_alumno(
                 profile.person_id, profile.program.id).ToList();

            p.student_id = profile.person_id;
            p.study_plan = profile.term_catalg;
            p.courses = new List<Course>();
            foreach(PHistoriaXCarreraAlumno item in list)
            {
                Course course = new Course();
                course.cycle = item.CYCLE== null? -1: int.Parse(item.CYCLE);
                course.credits = (int)item.CREDIT;
                course.date = item.DATEFINALSCORE;
                course.id = $"{item.SUBJECT}{item.COURSE}";
                course.subject = item.SUBJECT;
                course.is_active = "1";
                course.is_aproved = Convert.ToBoolean(Convert.ToInt32(item.APROVED));
                course.name = item.NAMECOURSE;
                course.nrc = item.NRC;
                course.score = item.SCORE;
                course.term = item.TERM;
                course.type = item.ELECTIVE == "1" ? "E" : "O";
                course.equivalent = item.RULE == null
                    ? $"{item.SUBJECT}{item.COURSE}"
                    : item.RULE.StartsWith("ELEC")
                        ? $"{item.SUBJECT}{item.COURSE}"
                        : item.RULE;
                p.courses.Add(course);
            }

            return p;
        }

    }
}
