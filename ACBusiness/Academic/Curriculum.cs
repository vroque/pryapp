﻿using System;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BANNER;
using System.Collections.Generic;
using System.Linq;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;

namespace ACBusiness.Academic
{
    /// <summary>
    /// Plan de Estudios
    /// </summary>
    public class Curriculum
    {
        #region properties

        public string id { get; set; }

        /// <summary>
        /// Nro creditos Totales del plan
        /// </summary>
        public int credits { get; set; }

        /// <summary>
        /// Nro Creditos Totales Obligatorios del plan 
        /// </summary>
        public int credit_mandatory { get; set; }

        /// <summary>
        /// Nro Creditos Totales Electivos del plan
        /// </summary>
        public int credit_elective { get; set; }

        /// <summary>
        /// Modalidad (Modalidad del pregrado)Ureg, uvir, upgt
        /// </summary>
        public string department { get; set; }

        /// <summary>
        /// Carrera o Escuela Academica
        /// </summary>
        public Program program { get; set; }

        /// <summary>
        /// Colegio de la carrera o facultad
        /// </summary>
        public College college { get; set; }

        /// <summary>
        /// Sede
        /// </summary>
        public Campus campus { get; set; }

        /// <summary>
        /// Lista de Ciclos del plan
        /// </summary>
        public List<Cycle> cycles { get; set; }

        public int? actual_cycle { get; internal set; }

        private readonly BNContext _bnContext = new BNContext();

        #endregion

        #region methods

        public Curriculum get(string department, string program, string campus, string term_catalog)
        {
            var curriculum = new Curriculum();
            //Traemos el periodo catalogo
            // eliminamos la consulta para sustituirla por un procedimiento
            var list = _bnContext.tbl_SZRCDAB.Where(f =>
                f.SZRCDAB_DEPT_CODE == department
                && f.SZRCDAB_MARJ_CODE == program
                && f.SZRCDAB_CAMP_CODE == campus
                && f.SZRCDAB_TERM_CODE_EFF == term_catalog
                && f.SZRCDAB_COLL_CODE != "AC" // actividades
                && f.SZRCDAB_COLL_CODE != "CN" // ciclo nivelacion
                // creo que refiere a cursos de especialidad
                && !f.SZRCDAB_AGEN_AREA.Contains("D")
                && f.SZRCDAB_AGEN_REQ_CREDITS != null
            ).ToList();

            //sustituimos la consulta por el procedimiento enviado

            var total_credits = list.FirstOrDefault()?.SZRCDAB_PGEN_REQ_CREDITS;
            curriculum.id = term_catalog;
            curriculum.credits = total_credits == null ? -1 : (int) total_credits;
            curriculum.department = department;
            curriculum.program = Program.get(program);
            curriculum.college = Program.getCollege(program);
            curriculum.campus = Campus.get(campus);
            var cycle_list = list
                .Select(f => int.Parse(f.SZRCDAB_AGEN_AREA.Substring(4, 2))).Distinct().OrderBy(f => f).ToList();

            curriculum.cycles = new List<Cycle>();
            foreach (var cycle_number in cycle_list)
            {
                var cycle = new Cycle();
                var cycle_str = $"{cycle_number:00}";
                var area = $"{program}P{cycle_str}{department}";
                cycle.id = cycle_number;
                var cycle_credits = list.FirstOrDefault(f => f.SZRCDAB_AGEN_AREA == area)?.SZRCDAB_AGEN_REQ_CREDITS;
                cycle.credits = cycle_credits == null ? -1 : (int) cycle_credits;

                cycle.courses = list.Where(f => f.SZRCDAB_AGEN_AREA == area)
                    .Select(f => new Course
                    {
                        id = $"{f.SZRCDAB_SUBJ_CODE}{f.SZRCDAB_CRSE_NUMB}",
                        name = f.SZRCDAB_TITLE,
                        subject = f.SZRCDAB_SUBJ_CODE,
                        type = (f.SZRCDAB_ELEC == "0") ? "O" : "E",
                        cycle = cycle_number,
                        credits = f.SZRCDAB_CREDIT_HR_HIGH == null ? -1 : (int) f.SZRCDAB_CREDIT_HR_HIGH,
                        is_active = "1"
                    }).OrderByDescending(f => f.type).ThenBy(f => f.name).ToList();

                curriculum.cycles.Add(cycle);
            }

            //Agregar actividad al ciclo 1 y 2
            Course activity = new Course();
            activity.id = "ACUC------";
            activity.name = "Actividad 1";
            activity.subject = "";
            activity.type = "O";
            activity.cycle = 1;
            activity.credits = 1;
            activity.is_active = "1";
            curriculum.cycles.FirstOrDefault(f => f.id == 1)?.courses.Add(activity);
            activity.cycle = 2;
            activity.name = "Actividad 2";
            curriculum.cycles.FirstOrDefault(f => f.id == 2)?.courses.Add(activity);
            var mandatoty = 0;
            mandatoty = list.Where(f => f.SZRCDAB_ELEC == "0")
                            .Sum(f => (int) f.SZRCDAB_CREDIT_HR_HIGH) + 2; //sumamos los electivos
            curriculum.credit_mandatory = mandatoty;
            curriculum.credit_elective = curriculum.credits - mandatoty;
            return curriculum;
        }


        public Curriculum getByStudent(AcademicProfile profile)
        {
            var curriculum = new Curriculum();
            // get las cycle for 

            var last_periods = _bnContext.tbl_SZRCDAB.Where(f =>
                f.SZRCDAB_DEPT_CODE == profile.department
                && f.SZRCDAB_MARJ_CODE == profile.program.id
                && f.SZRCDAB_CAMP_CODE == profile.campus.id
                && f.SZRCDAB_TERM_CODE_EFF == profile.term_catalg
                && f.SZRCDAB_COLL_CODE != "AC" // actividades
                && f.SZRCDAB_COLL_CODE != "CN" // ciclo nivelacion
                && !f.SZRCDAB_AGEN_AREA.Contains("D") // creo que refiere a cursos de especialidad
                && f.SZRCDAB_AGEN_REQ_CREDITS != null
            ).ToList();
            var last_period = last_periods.OrderByDescending(f => int.Parse(f.SZRCDAB_CICL)).FirstOrDefault();


            //sustituimos la consulta por el procedimiento enviado
            List<PCatalogoPersonzalizado> catalogoPersonzalizadoList =
                _bnContext.p_catalogo_por_alumno(profile.person_id, profile.program.id).ToList();
            var movEstudiante = Student.GetLastMovimientoEstudianteBySchoolId(profile.person_id, profile.program.id);
            List<PCatalogoPersonzalizado> list = catalogoPersonzalizadoList
                .Where(w => w.TERMCATALOG == movEstudiante.CATALOGO_REAL && w.DEPARTAMENT == profile.department)
                .ToList();

            curriculum.id = profile.term_catalg;
            curriculum.credits = last_period == null ? 10 : (int) last_period.SZRCDAB_PGEN_REQ_CREDITS;
            curriculum.department = profile.department;
            curriculum.program = profile.program;
            curriculum.college = profile.college;
            curriculum.campus = profile.campus;

            List<PCatalogoPersonzalizado> listCopy = list.Select(s => new PCatalogoPersonzalizado
            {
                ELECTIVE = s.ELECTIVE,
                RULE = s.RULE,
                SUBJECT = s.SUBJECT,
                COURSE = s.COURSE,
                NAMECOURSE = s.NAMECOURSE,
                CREDITS = s.CREDITS,
                CYCLE = s.CYCLE
            }).Distinct().ToList();
            List<PCatalogoPersonzalizado> listWithoutDuplicates = new List<PCatalogoPersonzalizado>();
            foreach (var item in listCopy)
            {
                if (!listWithoutDuplicates.Any(w => w.CYCLE == item.CYCLE && w.COURSE == item.COURSE &&
                                                    w.RULE == item.RULE))
                {
                    listWithoutDuplicates.Add(item);
                }
            }

            List<int> cycle_list = listWithoutDuplicates.Select(f => int.Parse(f.CYCLE)).Distinct().OrderBy(f => f)
                .ToList();

            curriculum.cycles = new List<Cycle>();
            foreach (var cycle_number in cycle_list)
            {
                var cycle = new Cycle();
                cycle.id = cycle_number;

                cycle.credits = listWithoutDuplicates.Where(f => int.Parse(f.CYCLE) == cycle_number)
                    .Sum(f => f.CREDITS ?? 0);

                cycle.courses = listWithoutDuplicates.Where(f => int.Parse(f.CYCLE) == cycle_number)
                    .Select(f => new Course
                    {
                        id = f.RULE == null
                            ? ""
                            : !f.RULE.StartsWith("ELEC")
                                ? f.RULE
                                : f.SUBJECT == "MANN"
                                    ? f.RULE
                                    : $"{f.SUBJECT}{f.COURSE}", // $"{f.SUBJECT}{f.COURSE}",
                        name = f.NAMECOURSE,
                        subject = f.SUBJECT,
                        type = (f.ELECTIVE != "1") ? "O" : "E",
                        cycle = cycle_number,
                        credits = f.CREDITS ?? 0,
                        equivalent = $"{f.SUBJECT}{f.COURSE}", //f.RULE??"",
                        is_active = "1",
                        creditsRequered = list.Where(w => int.Parse(w.CYCLE) == cycle_number && w.RULE == f.RULE &&
                                                            w.SUBJECT == f.SUBJECT && w.COURSE == f.COURSE).Select(s => s.PREREQCRED).FirstOrDefault(), //f.PREREQCRED, 
                        prerequisite_list = list.Where(w => int.Parse(w.CYCLE) == cycle_number && w.RULE == f.RULE &&
                                                            w.SUBJECT == f.SUBJECT && w.COURSE == f.COURSE)
                            .Select(s =>
                                new Prerequisite {subject = s.PREQSUBJECT, course = s.PREQCOURSE, name = s.PREQNAME})
                            .OrderBy(o => o.course).ToList()
                    }).OrderByDescending(f => f.type)
                    .ThenBy(f => f.name)
                    .ToList();

                curriculum.cycles.Add(cycle);
            }

            var mandatoty = 0;
            mandatoty = listWithoutDuplicates.Where(f => f.ELECTIVE == "0")
                .Sum(f => f.CREDITS ?? 0); //sumamos las actividades
            curriculum.credit_mandatory = mandatoty;
            curriculum.credit_elective = curriculum.credits - mandatoty;
            return curriculum;
        }

        #endregion methods
    }

    public class Cycle
    {
        public int id { get; set; }
        public int credits { get; set; }
        public List<Course> courses { get; set; }
    }
}