﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.Academic
{
    public class Period
    {
        public string id { get; set; }
        public static List<Period> getAll()
        {
            DBOContext db = new DBOContext();
            List<Period> result = db.tbl_per_acad.Where(x => x.IDDependencia == "UCCI").Select(x => new Period { id = x.IDPerAcad }).OrderByDescending(x => x.id).ToList();
            return result;
        }
    }
}
