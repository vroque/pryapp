﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Academic
{
    public class Modality
    {
        public string name {get; set; }

        #region statics
        public static string getByAcademicSchool(string escuela_id)
        {
            // ADV, PGT -> PGR
            string mod;
            switch (escuela_id)
            {
                case "ADG": mod = "PGT"; break;
                case "ADE": mod = "PGE"; break;
                default: mod = "PGR"; break;
            }
            return mod;
        }
        #endregion
    }
}
