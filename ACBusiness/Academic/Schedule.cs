﻿using ACBusiness.AtentionCenter;
using ACBusiness.Extra;
using ACPermanence.Contexts.BANNER;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using ACPermanence.DataBases.DBUCCI.DBO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ACBusiness.Academic
{
    public class Schedule
    {
        public string day { get; set; }
        public List<ScheduleCourse> courses { get; set; }

        private readonly BNContext _banner = new BNContext();
        readonly NEXOContext nexoContext = new NEXOContext();

        /// <summary>
        /// Student schedule
        /// </summary>
        /// <param name="profile">Student profile</param>
        /// <returns>Student schedule list</returns>
        public List<Schedule> GetByStudent(AcademicProfile profile)
        {
            var term = new ACTerm().GetCurrenTermForSchedule(profile.campus.id, profile.department);
            return GetStudentSchedule(profile.person_id, term.term);
        }
        /// <summary>
        /// Student schedule
        /// </summary>
        /// <param name="profile">Student profile</param>
        /// <param name="term">term</param>
        /// <returns></returns>
        public List<Schedule> GetByStudentAndTerm(AcademicProfile profile, string term)
        {
            return GetStudentSchedule(profile.person_id, term);
        }

        public List<Schedule> getScheduleByNrcTermByRestriction(string nrc, string term, string subject, string course)
        {
            // asignaturas disponibles id=1
            DisplayBlock display = nexoContext.cau_display_block.FirstOrDefault(f => f.id == 1);
            var schedule = new List<Schedule>();
            if (display != null)
            {
                if (display.active)
                {
                    DateTime today = DateTime.Now;
                    if (today >= display.startFech && today <= display.endFech)
                    {
                        if (today.TimeOfDay >= display.startFech.TimeOfDay && today.TimeOfDay <= display.endFech.TimeOfDay)
                        {
                            schedule = getScheduleByNrcTerm(nrc, term, subject, course);
                            return schedule;
                        }
                        else
                        {
                            return schedule;
                        }
                    }
                }
                else
                {
                    return schedule;
                }
            }
            return schedule;
        }

        public List<Schedule> getScheduleByNrcTerm(string nrc, string term, string subject, string course)
        {
            List<Schedule> schedule = new List<Schedule>();
            var list = _banner.ban_horarios
                    .Where(f =>
                    f.SSRMEET_CRN == nrc
                    && f.SSRMEET_TERM_CODE == term
                    && f.SSRMEET_BEGIN_TIME != null
                    && f.SSRMEET_BEGIN_TIME.Length > 0
                    && f.SSRMEET_END_TIME != null
                    && f.SSRMEET_END_TIME.Length > 0
                    )
                    .Select(f =>
                       new PHorarioXAlumno {
                           nrc = f.SSRMEET_CRN,
                           beginTime = f.SSRMEET_BEGIN_TIME,
                           endTime = f.SSRMEET_END_TIME,
                           module = "",
                           courseName = "",
                           dateStart = f.SSRMEET_START_DATE,
                           dateEnd = f.SSRMEET_END_DATE,
                           classroom = f.SSRMEET_ROOM_CODE??"Sin asignar",
                           sunday = f.SSRMEET_SUN_DAY==null? "0" : "1",
                           monday = f.SSRMEET_MON_DAY == null ? "0" : "1",
                           tuesday = f.SSRMEET_TUE_DAY == null ? "0" : "1",
                           wednesday = f.SSRMEET_WED_DAY == null ? "0" : "1",
                           thursday = f.SSRMEET_THU_DAY == null ? "0" : "1",
                           friday = f.SSRMEET_FRI_DAY == null ? "0" : "1",
                           saturday = f.SSRMEET_SAT_DAY == null ? "0" : "1",
                           type = f.SSRMEET_CATAGORY // its a trap
                       }
                    )
                    .OrderBy(f => f.beginTime).ToList();// ordenamos por hora de inicio para validar horario consecutivo

            List<string> categories = list.Select(f => f.type).Distinct().ToList();

            var docentes = _banner.ban_docentes.Join(
                    _banner.dim_personas,
                    cd => cd.SIRASGN_PIDM,
                    pe => pe.SPRIDEN_PIDM,
                    (cd, pe) => new { cd, pe}
                ).Where(f =>
                f.cd.SIRASGN_TERM_CODE == term
                && f.cd.SIRASGN_CRN == nrc
                && categories.Contains(f.cd.SIRASGN_CATEGORY)
            ).Select(f => new {
                pidm = f.cd.SIRASGN_PIDM,
                id =f.pe.SPRIDEN_ID,
                lastName = f.pe.SPRIDEN_LAST_NAME,
                firstName = f.pe.SPRIDEN_FIRST_NAME,
                category = f.cd.SIRASGN_CATEGORY
            }).ToList();

            //var curso = _banner.tbl_SCRSYLN.FirstOrDefault(f => f.SCRSYLN_CRSE_NUMB == subject && f.SCRSYLN_CRSE_NUMB == nrc);
            var curso = _banner.tbl_SCRSYLN.FirstOrDefault(f => f.SCRSYLN_SUBJ_CODE == subject && f.SCRSYLN_CRSE_NUMB == course);

            // fill docentes in lista de horarios
            foreach (var item in list)
            {
                var docente = docentes.FirstOrDefault(f => f.category == item.type);
                if (docente != null)
                {
                    item.nameTeacher = $"{docente.lastName} {docente.firstName}";
                    item.pidmTeacher = docente.pidm;
                }
                if(curso != null)
                {
                    item.courseName = curso.SCRSYLN_LONG_COURSE_TITLE;
                }
                item.beginTime = $"{item.beginTime.Substring(0,2)}:{item.beginTime.Substring(2, 2)}";
                item.endTime = $"{item.endTime.Substring(0, 2)}:{item.endTime.Substring(2, 2)}";
            }

            List<Schedule> data = new List<Schedule>();
            if (list != null && list.Count > 0)
            {
                data.Add(getScheduleByDay(list.Where(f => f.monday == "1").ToList(), "Lunes"));

                data.Add(getScheduleByDay(list.Where(f => f.tuesday == "1").ToList(), "Martes"));

                data.Add(getScheduleByDay(list.Where(f => f.wednesday == "1").ToList(), "Miércoles"));

                data.Add(getScheduleByDay(list.Where(f => f.thursday == "1").ToList(), "Jueves"));

                data.Add(getScheduleByDay(list.Where(f => f.friday == "1").ToList(), "Viernes"));

                data.Add(getScheduleByDay(list.Where(f => f.saturday == "1").ToList(), "Sábado"));

                data.Add(getScheduleByDay(list.Where(f => f.sunday == "1").ToList(), "Domingo"));

                return data;
            }
            else
            {
                return null;
            }

        }


        /// <summary>
        /// Horario de estudiante por período académico.
        /// </summary>
        /// <param name="studentPidm">PIDM del estudiante</param>
        /// <param name="term">Período académico</param>
        /// <returns></returns>
        public List<Schedule> GetStudentSchedule(decimal studentPidm, string term)
        {
            var data = new List<Schedule>();
            var list = _banner.p_horario_x_alumno(studentPidm, term).ToList();

            if (list == null || list.Count <= 0) return null;

            data.Add(getScheduleByDay(list.Where(f => f.monday == "1").ToList(), "Lunes"));
            data.Add(getScheduleByDay(list.Where(f => f.tuesday == "1").ToList(), "Martes"));
            data.Add(getScheduleByDay(list.Where(f => f.wednesday == "1").ToList(), "Miércoles"));
            data.Add(getScheduleByDay(list.Where(f => f.thursday == "1").ToList(), "Jueves"));
            data.Add(getScheduleByDay(list.Where(f => f.friday == "1").ToList(), "Viernes"));
            data.Add(getScheduleByDay(list.Where(f => f.saturday == "1").ToList(), "Sábado"));
            data.Add(getScheduleByDay(list.Where(f => f.sunday == "1").ToList(), "Domingo"));

            return data;
        }

        /// <summary>
        /// Horario de cursos iniciados (clases iniciadas) de un estudiante en un período académico.
        /// </summary>
        /// <param name="studentPidm"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        public List<Schedule> GetStudentScheduleByStartedCourses(decimal studentPidm, string term)
        {
            var data = new List<Schedule>();
            var threeDaysAgo = DateTime.Today.AddDays(-3);
            var list = _banner.p_horario_x_alumno(studentPidm, term)
                .Where(item => item.dateEnd >= DateTime.Today && item.dateStart <= DateTime.Today).OrderBy(f => f.beginTime).ToList();// ordenamos por hora de inicio para validar horario consecutivo

            if (list.Count <= 0) return null;

            data.Add(getScheduleByDay(list.Where(f => f.monday == "1").ToList(), "Lunes"));
            data.Add(getScheduleByDay(list.Where(f => f.tuesday == "1").ToList(), "Martes"));
            data.Add(getScheduleByDay(list.Where(f => f.wednesday == "1").ToList(), "Miércoles"));
            data.Add(getScheduleByDay(list.Where(f => f.thursday == "1").ToList(), "Jueves"));
            data.Add(getScheduleByDay(list.Where(f => f.friday == "1").ToList(), "Viernes"));
            data.Add(getScheduleByDay(list.Where(f => f.saturday == "1").ToList(), "Sábado"));
            data.Add(getScheduleByDay(list.Where(f => f.sunday == "1").ToList(), "Domingo"));

            return data;
        }

        protected bool compareTimes(string endTime, string startTime)
        {
            var h1 = int.Parse(endTime.Substring(0, 2));
            var m1 = int.Parse(endTime.Substring(3, 2));
            var h2 = int.Parse(startTime.Substring(0, 2));
            var m2 = int.Parse(startTime.Substring(3, 2));

            var t1 = h1 * 60 + m1 + 1;
            var t2 = h2 * 60 + m2;

            return t1 == t2;
        }

        protected int timeMinutes(string time)
        {
            var hours = int.Parse(time.Substring(0, 2));
            var minutes = int.Parse(time.Substring(3, 2));
            var newTime = hours * 60 + minutes;
            return newTime;
        }

        private Schedule getScheduleByDay(List<PHorarioXAlumno> listFilter, string day)
        {
            var schedule = new Schedule { day = day, courses = new List<ScheduleCourse>() };

            ScheduleCourse course = listFilter.FirstOrDefault();
            if (course == null) return schedule;

            for (var i = 1; i < listFilter.Count; i++)
            {
                if (compareTimes(course.end, listFilter[i].beginTime) && course.nrc == listFilter[i].nrc) // verifica si son continuos y del mismo curso
                {
                    course.end = listFilter[i].endTime;
                }
                else
                {
                    schedule.courses.Add(course);
                    course = listFilter[i];
                }
            }

            schedule.courses.Add(course);

            for (var x = 0; x < schedule.courses.Count; x++)
            {
                for (var y = x + 1; y < schedule.courses.Count; y++)
                {
                    if (timeMinutes(schedule.courses[x].start) > timeMinutes(schedule.courses[y].start))
                    {
                        ScheduleCourse a = schedule.courses[x];
                        schedule.courses[x] = schedule.courses[y];
                        schedule.courses[y] = a;
                    }
                }
            }

            return schedule;
        }
    }

    public class ScheduleCourse
    {
        public string id { get; set; }
        public string nrc { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string name { get; set; }
        public string module { get; set; }
        public DateTime dateStart { get; set; }
        public DateTime dateEnd { get; set; }
        public string teacher { get; set; }
        public string classroom { get; set; }


        public static implicit operator ScheduleCourse(PHorarioXAlumno item)
        {
            if (item == null) return null;
            var course = new ScheduleCourse();
            course.id = item.course;
            course.nrc = item.nrc;
            course.classroom = item.classroom;
            course.start = item.beginTime;
            course.end = item.endTime;
            course.dateStart = item.dateStart;
            course.dateEnd = item.dateEnd;
            course.name = item.courseName;
            course.module = item.module;
            course.teacher = item.nameTeacher;
            return course;
        }
        
    }
}
