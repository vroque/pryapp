﻿using System.Collections.Generic;
using System.Linq;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BDUCCI;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACBusiness.Academic
{
    public class ThirdFifthTenth
    {
        #region properties

        public string term { get; set; }
        public int order { get; set; }
        public string period { get; set; }
        public float score { get; set; }
        readonly DBOContext _apec = new DBOContext();

        #endregion properties

        /// <summary>
        /// lista todos las veces que un alumno alcanzo 3 5 o 10 superior
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        public List<ThirdFifthTenth> list(AcademicProfile profile)
        {
            List<ThirdFifthTenth> list = _apec.view_alumno_basico.Join(
                    _apec.rau_tbl_tercio_quinto,
                    alu => alu.IDAlumno,
                    s35 => s35.IDAlumno,
                    (alu, s35) => new {alu, s35}
                ).Where(f =>
                    f.alu.IDPersonaN == profile.person_id
                    && f.s35.IDDependencia == _acad.DEFAULT_DIV
                    && f.s35.IDEscuela == profile.program.id
                    && f.s35.Participa == "1"
                    && f.s35.Tercio == "1"
                ).ToList()
                .Select(f => new ThirdFifthTenth
                {
                    term = Term.toBanner(f.s35.IDPerAcad),
                    period = f.s35.Ciclo,
                    score = float.Parse(f.s35.PromedioPonderado.ToString()),
                    order = (f.s35.Decimo == "1" ? 10 : (f.s35.Quinto == "1" ? 5 : 3))
                }).ToList();

            return list;
        }

        /// <summary>
        /// Obtiene los datos si un alumno alcanzo 3 5 10 superitor
        /// en un determinado periodo
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        public ThirdFifthTenth get(AcademicProfile profile, string term)
        {
            var termApec = Term.toAPEC(term, profile.department);
            var s3510 = _apec.view_alumno_basico.Join(
                    _apec.rau_tbl_tercio_quinto,
                    alu => alu.IDAlumno,
                    s35 => s35.IDAlumno,
                    (alu, s35) => new {alu, s35}
                ).Where(f =>
                    f.alu.IDPersonaN == profile.person_id
                    && f.s35.IDDependencia == _acad.DEFAULT_DIV
                    && f.s35.IDEscuela == profile.program.id
                    && f.s35.Participa == "1"
                    && f.s35.Tercio == "1"
                    && f.s35.IDPerAcad == termApec
                ).ToList()
                .Select(f => new ThirdFifthTenth
                {
                    term = term,
                    period = f.s35.Ciclo,
                    score = float.Parse(f.s35.PromedioPonderado.ToString()),
                    order = (f.s35.Decimo == "1" ? 10 : (f.s35.Quinto == "1" ? 5 : 3))
                }).FirstOrDefault();
            return s3510;
        }

        public ThirdFifthTenth getPromotional(AcademicProfile profile)
        {
            var data = _apec.view_alumno_basico.Join(
                    _apec.rau_tbl_tercio_quinto_promo,
                    alu => alu.IDAlumno,
                    s35 => s35.IDAlumno,
                    (alu, s35) => new {alu, s35}
                ).Where(f =>
                    f.alu.IDPersonaN == profile.person_id
                    && f.s35.IDDependencia == _acad.DEFAULT_DIV
                    && f.s35.IDEscuela == profile.program.id
                    && f.s35.Valido == "1"
                    && f.s35.Tercio == "1"
                ).ToList()
                .Select(f => new ThirdFifthTenth
                {
                    term = Term.toBanner(f.s35.IDPerAcad),
                    score = float.Parse(f.s35.PromedioPonderado.ToString()),
                    order = (f.s35.Decimo == "1" ? 10 : (f.s35.Quinto == "1" ? 5 : 3))
                }).FirstOrDefault();
            return data;
        }
    }

    /// <summary>
    /// Estructura para almacenar datos sobre: Tercio, Quinto y Decimo Superior Promocional
    /// </summary>
    public class ThirdFifthTenthPromo
    {
        public string term { get; set; }
        public string third_fitfh_tenth_top { get; set; }
    }
}