﻿using System.Collections.Generic;
using System.Linq;
using ACPermanence.Contexts.NEXO;
using ACPermanence.Contexts.BDUCCI;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BANNER;

namespace ACBusiness.Academic
{
    ///<author>Wanshi reymundo</author>
    ///<date>13/01/2017</date>
    /// <summary>
    /// Perfil de los egresados
    /// </summary>
    public class Graduate
    {
        #region properties

        public decimal person_id { get; set; }
        public string divs { get; set; }
        public string program { get; set; }
        public string program_description { get; set; }
        public string dni { get; set; }
        public string student_name { get; set; }
        public string last_semester { get; set; }
        public string first_semester { get; set; }
        public bool projection { get; set; }
        public bool practice { get; set; }
        public bool language { get; set; }
        public bool bachelor { get; set; }
        public bool degree { get; set; }

        // for gradution only
        public int credits_o { get; set; }
        public int credits_e { get; set; }
        public int last_period { get; set; }
        public string term_catalog { get; set; }

        #endregion properties

        #region methods

        public static Graduate get(AcademicProfile profile)
        {
            var dboContext = new DBOContext();
            Curriculum curriculum = new Curriculum().get(
                profile.department, profile.program.id,
                profile.campus.id, profile.term_catalg);
            int last_find_period = curriculum.cycles.OrderByDescending(f => f.id).FirstOrDefault().id;

            Graduate g = dboContext.view_alumno_basico.Join(
                    dboContext.tbl_alumno_egresado,
                    alu => alu.IDAlumno,
                    egr => egr.IDAlumno,
                    (alu, egre) => new {alu, egre}
                ).Where(f =>
                    f.alu.IDPersonaN == profile.person_id
                    && f.egre.IDEscuela == profile.program.id
                ).ToList()
                .Select(f => new Graduate
                {
                    divs = f.egre.IDDependencia,
                    program = f.egre.IDEscuela,
                    credits_o = f.egre.CreditosO,
                    credits_e = f.egre.CreditosE,
                    last_semester = Term.toBanner(f.egre.IDPerAcad),
                    first_semester = Term.toBanner(f.egre.IDPeracadIni),
                    projection = f.egre.ProySocial == "1",
                    practice = f.egre.PracPreProfecio == "1",
                    language = f.egre.Idioma == "1",
                    bachelor = f.egre.Bachiller == "1",
                    degree = f.egre.Titulado == "1",
                    term_catalog = profile.term_catalg,
                    last_period = last_find_period
                })
                .FirstOrDefault();
            g.person_id = profile.person_id;

            return g;
        }

        #endregion methods
    }
}