﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BANNER;
using ACPermanence.Contexts.BDUCCI;
using _admission = ACTools.Constants.AdmissionConstant;
using _cau = ACTools.Constants.AtentionCenterConstants;

namespace ACBusiness.Academic
{
    /// <summary>
    /// Estudiantes Becados
    /// </summary>
    public class ScholarshipStudent
    {
        #region Properties

        public decimal StudentPidm { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string EntryPeriod { get; set; }
        public string OtherPeriod { get; set; }
        public string OtherPeriodDescription { get; set; }
        public string SchoolId { get; set; }
        public string SchoolName { get; set; }

        /// <summary>
        /// Modalidad de Postulación (IDModalidadPostu)
        /// </summary>
        public decimal TypeOfIncomeId { get; set; }
        
        /// <summary>
        /// Modalidad de Postulación (Transformación Recruiter) usado en BANNER
        /// </summary>
        public string TypeOfIncomeRecruiter { get; set; }

        /// <summary>
        /// Nombre de la Modalidad de Postulación (Nombre de IDModalidadPostu)
        /// </summary>
        public string TypeOfIncomeName { get; set; }

        /// <summary>
        /// Modalidad (IDEscuelaADM)
        /// </summary>
        public string ModalityId { get; set; }

        public string ModalityName { get; set; }
        public bool IsSelected { get; set; }
        public bool IsPronabec { get; set; }
        public int AcademicStatus { get; set; }
        public string DocUrl { get; set; }
        public bool Download { get; set; }
        public string SaveAs { get; set; }
        public bool Processing { get; set; }
        public bool ProcessingError { get; set; }
        
        private readonly BNContext _bnContext = new BNContext();
        private readonly DBOContext _dboContext = new DBOContext();

        #endregion Properties

        #region Methods

        /// <summary>
        /// Lista de Ingresantes a la UC
        /// </summary>
        /// <param name="applicants">Códigos de estudiantes</param>
        /// <returns>Lista de Ingresantes a la UC</returns>
        public async Task<List<ScholarshipStudent>> GetEntrantsAsync(string[] applicants)
        {
            var entrants = new List<ScholarshipStudent>();
            await Task.Run(() =>
            {
                entrants = Entrant.GetByStudentId(applicants).Select<Entrant, ScholarshipStudent>(s => s).ToList();
            });
            return entrants;
        }

        /// <summary>
        /// Lista de Ingresantes a la UC de una determinada modalidad PRONABEC
        /// que es encuentran matriculados en un determinado período académico
        /// </summary>
        /// <param name="pronabecType">Tipo de PRONABEC</param>
        /// <param name="schoolId">Escuela académica</param>
        /// <param name="enrolledPeriod">Período de matrícula</param>
        /// <returns></returns>
        public async Task<List<ScholarshipStudent>> GetEnrolledByPronabecTypeAndEnrolledPerdiodAsync(
            string pronabecType, string schoolId, string enrolledPeriod)
        {
            var academicPeriodBanner = Term.toBanner(enrolledPeriod);
            var academicPeriodApec = Term.toAPEC(enrolledPeriod);
            var pType = decimal.Parse(pronabecType);
            
            // Solo Ingresantes PRONABEC de determinado tipo PRONABEC en determinada escuela académica
            var pronabecEntrants = await Entrant.GetPronabecEntrantsByTypeAndSchoolAsync(pType, schoolId);
            var pronabecEntrantsByType = pronabecEntrants.Select<Entrant, ScholarshipStudent>(s => s).ToList();

            // dependiendo el periodo de matricula buscar en apec o banner
            if (int.Parse(academicPeriodBanner) >= _cau.PeriodOfDataMigrationToBanner)
            {
                var pronabecEntrantsByTypePidmList = pronabecEntrantsByType.Select(s => s.StudentPidm).ToList();
                var pronabecPidmList = await _bnContext.tbl_SZVCMPP
                    .Where(w => pronabecEntrantsByTypePidmList.Contains(w.SFRSTCR_PIDM) &&
                                w.SSBSECT_TERM_CODE == academicPeriodBanner)
                    .Select(s => s.SFRSTCR_PIDM).Distinct().ToListAsync();
                pronabecEntrantsByType =
                    pronabecEntrantsByType.Where(w => pronabecPidmList.Contains(w.StudentPidm)).ToList();
                return pronabecEntrantsByType;
            }

            var pronabecEntrantsByTypeStudentIdList = pronabecEntrantsByType.Select(s => s.StudentId).ToList();
            var pronabecStudentIdList = await _dboContext.tbl_matricula
                .Where(w => pronabecEntrantsByTypeStudentIdList.Contains(w.IDAlumno) &&
                            w.IDPerAcad == academicPeriodApec).Select(s => s.IDAlumno).ToListAsync();
            pronabecEntrantsByType =
                pronabecEntrantsByType.Where(w => pronabecStudentIdList.Contains(w.StudentId)).ToList();
            return pronabecEntrantsByType;
        }

        #endregion Methods

        #region Implicit Operators

        /// <summary>
        /// Conversión implicita de 'Entrant' a 'ScholarshipStudent'
        /// </summary>
        /// <param name="entrant">Objeto 'Entrant'</param>
        /// <returns>Objeto de tipo 'ScholarshipStudent'</returns>
        public static implicit operator ScholarshipStudent(Entrant entrant)
        {
            if (entrant == null) return null;
            
            var dboContext = new DBOContext();
            var mPostu = entrant.type_of_income_id.ToString(CultureInfo.InvariantCulture);
            var modalidadPostuRecruiter = dboContext.tbl_equivalencia_modalidad_admision.FirstOrDefault(w =>
                w.IDModalidadPostu == mPostu);

            var pronabecList = Scholarships.GetPronabecScholarships().Select(s => s.TypeOfIncomeId);

            var scholarshipStudent = new ScholarshipStudent
            {
                StudentPidm = entrant.school_id == "0" ? 0 : Student.getPidmFromStudentId(entrant.student_id),
                StudentId = entrant.student_id,
                StudentName = entrant.student_name,
                EntryPeriod = entrant.entry_period,
                //OtherPeriod = string.Empty,
                //OtherPeriodDescription = string.Empty,
                SchoolId = entrant.school_id,
                SchoolName = entrant.school_name,
                TypeOfIncomeId = entrant.type_of_income_id,
                TypeOfIncomeRecruiter = modalidadPostuRecruiter?.IDModRecruiter,
                TypeOfIncomeName = entrant.type_of_income_name,
                ModalityId = entrant.modality_id,
                ModalityName = entrant.modality_name,
                //IsSelected = false,
                IsPronabec = pronabecList.Contains(entrant.type_of_income_id),
                //AcademicStatus = 0,
                //DocUri = string.Empty,
                //Download = false,
                //SaveAs = string.Empty,
                //Processing = false,
                //ProcessingError = false
            };
            return scholarshipStudent;
        }

        #endregion Implicit Operators
    }
}