﻿using ACBusiness.Economic;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.BUW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _inst = ACTools.Constants.InstitucionalConstants;


namespace ACBusiness.Academic
{
    /// <summary>
    /// Freshman: alumnos de primer año
    /// alumnos que an postulado, ingresado y matriculado en el semestre actual
    /// </summary>
    public class Freshman: Student
    {

        /// <summary>
        /// Listar cachimbos por periodo carrera y sede
        /// </summary>
        /// <param name="term"></param>
        /// <param name="program"></param>
        /// <param name="campus"></param>
        /// <returns></returns>
        public static List<Freshman> listByTerm(string term, string program, string campus) {
            NEXOContext nx = new NEXOContext();
            DBOContext apec = new DBOContext();
            //buscamos los alumnos que cumplen los requisitos
            List<decimal> pidm_list = nx.dim_student_postulations
                .Join(
                    nx.dim_student_enrollments,
                    post => new { post.divs, post.levl, post.program, post.pidm, post.term },
                    prog => new { prog.divs, prog.levl, prog.program, prog.pidm, prog.term },
                    (post, prog) => new { post = post, prog = prog }
                ).Where(f =>
                   f.post.term == term
                   && f.post.status == "INGRESANTE"
                   && f.post.program == program
                   && f.post.campus == campus
                ).Select(f => f.post.pidm )
                .ToList();
            // traemos sus datos personales
            List<Freshman> freshmans = apec.tbl_persona.Where(f =>
               pidm_list.Contains(f.IDPersonaN)
            ).Select(f => new Freshman { 
                id = f.DNI,
                person_id = f.IDPersonaN,
                last_name = f.Appat + " " + f.ApMat,
                first_name = f.Nombres,
                gender = f.Sexo
            }).ToList();
            // Obtener las escalas en bloque
            List<string> student_list = freshmans.Select(f => f.id).ToList();
            // <student_id, scala>
            Dictionary<string,string> student_scalas = Scala.getByStudentList(student_list);
            freshmans.ForEach(f => f.scala = student_scalas[f.id]);

            return freshmans;
        }

        //public async Task<string> categorize(AcademicProfile profile, string user)
        //{
        //    Categorization categorization = new Categorization(profile, user);
        //    string category = await categorization.compute();
           
        //    return category;
        //}

        public static Freshman instanceParent(Student tmp)
        {
            if (tmp == null) return null;
            Freshman obj = new Freshman();
            obj.id = tmp.id;
            obj.person_id = tmp.person_id;
            obj.first_name = tmp.first_name;
            obj.last_name = tmp.last_name;
            obj.gender = tmp.gender;
            obj.scala = tmp.scala;
            obj.profiles = tmp.profiles;
            obj.profile = tmp.profile;

            return obj;
        }
    }
}
