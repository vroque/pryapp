﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Admission;
using ACPermanence.Contexts.BANNER;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;
using _admission = ACTools.Constants.AdmissionConstant;

namespace ACBusiness.Academic
{
    /// <inheritdoc />
    /// <summary>
    /// Becas UC
    /// </summary>
    public class Scholarships : AdmissionType
    {
        #region Properties

        public bool IsScholarship { get; set; }
        public bool IsPronabec { get; set; }

        private readonly DBOContext _dboContext = new DBOContext();

        #endregion Properties

        #region Methods

        /// <summary>
        /// Becas PRONABEC
        /// </summary>
        /// <returns>Becas PRONABEC</returns>
        public async Task<List<Scholarships>> GetPronabecScholarshipsAsync()
        {
            // PRONABEC en APEC
            var pronabecIdApecList = GetPronabecApecIds()
                .Select(s => s.ToString(CultureInfo.InvariantCulture)).ToList();

            // PRONABEC en BANNER
            var pronabecBanner = await _dboContext.tbl_equivalencia_modalidad_admision
                .Where(w => pronabecIdApecList.Contains(w.IDModalidadPostu)).ToListAsync();

            var pronabec = pronabecBanner.Select<DBOTblModalidadPostuRecruiter, Scholarships>(s => s);

            return pronabec.OrderBy(o => o.TypeOfIncomeName).ToList();
        }

        /// <summary>
        /// Becas PRONABEC
        /// </summary>
        /// <returns>Becas PRONABEC</returns>
        public static IEnumerable<Scholarships> GetPronabecScholarships()
        {
            var dboContext = new DBOContext();
            // PRONABEC en APEC
            var pronabecIdApecList = GetPronabecApecIds()
                .Select(s => s.ToString(CultureInfo.InvariantCulture)).ToList();

            // PRONABEC en BANNER
            var pronabecBanner = dboContext.tbl_equivalencia_modalidad_admision
                .Where(w => pronabecIdApecList.Contains(w.IDModalidadPostu)).ToList();

            var pronabec = pronabecBanner.Select<DBOTblModalidadPostuRecruiter, Scholarships>(s => s);

            return pronabec.OrderBy(o => o.TypeOfIncomeName).ToList();
        }

        /// <summary>
        /// Colección de códigos de Becas UC.
        /// Incluyen PRONABEC y otras becas
        /// </summary>
        /// <returns>Colección de códigos de Becas UC</returns>
        private static IEnumerable<decimal> GetScholarshipsIds()
        {
            var dboContext = new DBOContext();
            var scholarshipsApec = dboContext.tbl_modalidad_postu.Where(w => w.Descripcion.Contains("beca"))
                .Select(s => new {s.IDModalidadPostu, s.Descripcion, s.IDEscuelaADM}).Distinct().ToList();
            return scholarshipsApec.Select(s => s.IDModalidadPostu).ToList();
        }

        /// <summary>
        /// Colección de códigos PRONABEC en APEC
        /// </summary>
        /// <returns>Lista de códigos PRONABEC en APEC</returns>
        private static IEnumerable<decimal> GetPronabecApecIds()
        {
            var dboContext = new DBOContext();
            var pronabecApec = dboContext.tbl_modalidad_postu.Where(w => w.Descripcion.Contains("pronabec"))
                .Select(s => new {s.IDModalidadPostu, s.Descripcion, s.IDEscuelaADM}).Distinct().ToList();
            return pronabecApec.Select(s => s.IDModalidadPostu).ToList();
        }

        /// <summary>
        /// Colección de códigos PRONABEC en BANNER
        /// </summary>
        /// <returns>Lista de códigos PRONABEC en BANNER</returns>
        private static IEnumerable<string> GetPronabecBannerIds()
        {
            var dboContext = new DBOContext();
            var pronabecIdApecList = GetPronabecApecIds()
                .Select(s => s.ToString(CultureInfo.InvariantCulture)).ToList();
            var pronabecBanner = dboContext.tbl_equivalencia_modalidad_admision
                .Where(w => pronabecIdApecList.Contains(w.IDModalidadPostu)).ToList();

            return pronabecBanner.Select(s => s.IDModalidadPostu).ToList();
        }

        #endregion Methods

        #region ImplicitOperator

        /// <summary>
        /// Conversión implícita de 'DBOTblModalidadPostuRecruiter' a 'Scholarships'
        /// </summary>
        /// <param name="recruiter">Tabla dbo.TblModalidadPostuRecruiter</param>
        /// <returns>Tipo de dato 'Scholarships'</returns>
        /// <exception cref="Exception"></exception>
        public static implicit operator Scholarships(DBOTblModalidadPostuRecruiter recruiter)
        {
            if (recruiter == null) return null;

            var dboContext = new DBOContext();

            var scholarship = new Scholarships()
            {
                TypeOfIncomeId = decimal.Parse(recruiter.IDModalidadPostu),
                TypeOfIncomeRecruiter = recruiter.IDModRecruiter,
                ModalityId = recruiter.departament
            };

            var modPostu = decimal.Parse(recruiter.IDModalidadPostu);
            var modalidadPostu = dboContext.tbl_modalidad_postu.Where(w =>
                w.IDModalidadPostu == modPostu &&
                w.IDEscuelaADM == recruiter.departament).OrderByDescending(o => o.IDPerAcad).FirstOrDefault();

            if (modalidadPostu == null) throw new Exception("No se encontó modalidad de postulación");

            scholarship.TypeOfIncomeName = modalidadPostu.Descripcion;
            scholarship.ModalityName = _admission.MODALITY_OF_ADMISSION[modalidadPostu.IDEscuelaADM];

            var scholarships = GetScholarshipsIds();
            var scholarshipsBanner = GetPronabecBannerIds();

            scholarship.IsScholarship = scholarships.Contains(decimal.Parse(recruiter.IDModalidadPostu));
            scholarship.IsPronabec = scholarshipsBanner.Contains(recruiter.IDModalidadPostu);

            return scholarship;
        }

        #endregion ImplicitOperator
    }
}