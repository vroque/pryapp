﻿using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Academic
{
    public class Absence
    {
        public string coursename { get; set; }
        public string course { get; set; }
        public string nrc { get; set; }
        public int percentage { get; set; }
        public double pidm { get; set; }
        public string program { get; set; }
        public string term { get; set; }
        public List<AbsenceCourse> course_absences { get; set; }

        readonly BNContext _bn = new BNContext();

        public List<Absence> getByStudent(AcademicProfile profile)
        {
            ACTerm object_term = new ACTerm().getCurrent(profile.campus.id, profile.department);
            List<Absence> data = this.getByStudentAndTerm(profile, object_term.term);
            return data;
        }

        /// <summary>
        /// Obtener inasistencia por periodo
        /// </summary>
        /// <param name="profile">Perfil estudiante</param>
        /// <param name="term">Periodo</param>
        /// <returns></returns>
        public List<Absence> getByStudentAndTerm(AcademicProfile profile, string term)
        {
            List<P_INASISTENCIAXALUMNO> list = _bn.p_inasistencia_x_alumno(profile.person_id, term).ToList();
            Schedule schedule_obj = new Schedule();
            List<Schedule> schedule = schedule_obj.GetByStudentAndTerm(profile, term);
            if (list != null && list.Count > 0 && schedule != null)
            {
                string[] days = new string[] { "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" };
                List<Absence> data = list.Select(f => f.nrc).Distinct().Select(f => new Absence { nrc = f }).ToList();

                foreach (Absence item in data)
                {
                    var i = list.Where(f => f.nrc == item.nrc).FirstOrDefault();
                    item.coursename = i.namecourse;
                    item.course = i.course;
                    item.nrc = i.nrc;
                    item.percentage = Int32.Parse(i.percentageinasistence.Replace("%", ""));
                    item.pidm = i.pidm;
                    item.program = i.program;
                    item.term = i.term;
                    //inner
                    item.course_absences = list.Where(f => f.nrc == item.nrc).Select<P_INASISTENCIAXALUMNO, AbsenceCourse>(f => f).ToList();
                }

                foreach (Absence item in data)
                {
                    foreach (AbsenceCourse course in item.course_absences)
                    {
                        //string dia = course.date.ToString("dddd");
                        int dia = (int)course.date.DayOfWeek;

                        foreach (Schedule s in schedule)
                        {
                            if (s.day == days[dia])
                            {
                                foreach (ScheduleCourse c in s.courses)
                                {
                                    if (c.nrc == item.nrc)
                                    {
                                        course.start = c.start;
                                        course.end = c.end;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }

                return data;
            }
            else
            {
                return null;
            }
        }
    }
    public class AbsenceCourse
    {
        public DateTime date { get; set; }
        public string start { get; set; }
        public string end { get; set; }

        public static implicit operator AbsenceCourse(P_INASISTENCIAXALUMNO item)
        {
            if (item == null) return null;
            AbsenceCourse courseabsence = new AbsenceCourse();
            courseabsence.date = item.absencedate;
            return courseabsence;
        }
    }
}
