﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;
using _acad = ACTools.Constants.AcademicConstants;
using _admission = ACTools.Constants.AdmissionConstant;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.Academic
{
    /// <summary>
    /// Ingresantes a la UC
    /// </summary>
    public class Entrant
    {
        #region Properties

        public string student_name { get; set; }
        public string student_id { get; set; }

        /// <summary>
        /// Modalidad de Postulación (IDModalidadPostu)
        /// </summary>
        public decimal type_of_income_id { get; set; }

        /// <summary>
        /// Nombre de la Modalidad de Postulación (Nombre de IDModalidadPostu)
        /// </summary>
        public string type_of_income_name { get; set; }

        public string entry_period { get; set; }
        public string school_id { get; set; }
        public string school_name { get; set; }

        /// <summary>
        /// Modalidad (IDEscuelaADM)
        /// </summary>
        public string modality_id { get; set; }

        /// <summary>
        /// Nombre Modalidad (Nombre de IDEscuelaADM)
        /// </summary>
        public string modality_name { get; set; }

        /// <summary>
        /// Modalidad de postulación "Sin Descripción"
        /// </summary>
        private const decimal NO_ID_MODALIDAD_POSTU = _admission.NO_ID_MODALIDAD_POSTU;

        private const string NO_SCHOOL = "0";
        private const string ING_SISTEMAS = _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA;
        private const string ING_SISTEMAS_OLD = _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA_OLD;

        #endregion Properties

        #region Methods

        /// <summary>
        /// Lista de ingresantes a la UC para una determinada escuela académica 'schoolId' en un determinado período 
        /// académico 'entryPeriod'
        /// </summary>
        /// <param name="schoolId">Código de Escuela Académica</param>
        /// <param name="entryPeriod">Período de Ingreso a la UC</param>
        /// <returns>Lista de Ingresantes a la UC según escuela académica y período de ingreso</returns>
        public static List<Entrant> GetBySchoolAndEntryPeriod(string schoolId, string entryPeriod)
        {
            var dboContext = new DBOContext();
            var postulantes =
                dboContext.tbl_postulante.Where(w => w.IDEscuela == schoolId &&
                                                     w.IDPerAcad == entryPeriod &&
                                                     w.IDDependencia == _corp.UC &&
                                                     w.Ingresante == _admission.INGRESANTE &&
                                                     w.Renuncia == _admission.NO_RENUNCIA_CARRERA).ToList();
            var entrants = postulantes.Select<DBOTblPostulante, Entrant>(e => e).OrderBy(o => o.student_name).ToList();
            return entrants;
        }

        /// <summary>
        /// Lista de Ingresantes a la UC según Código de Estudiante ingresados
        /// </summary>
        /// <param name="applicants">Arreglo de código de estudiantes</param>
        /// <returns>Lista de Ingresantes a la UC según Códigos de Estudiantes ingresados</returns>
        public static List<Entrant> GetByStudentId(string[] applicants)
        {
            var dboContext = new DBOContext();

            // Ingresantes
            var postulantes = dboContext.tbl_postulante
                .Where(w => applicants.Contains(w.IDAlumno) &&
                            w.IDDependencia == _corp.UC &&
                            w.Ingresante == _admission.INGRESANTE &&
                            w.Renuncia == _admission.NO_RENUNCIA_CARRERA &&
                            w.IDModalidadPostu != _admission.NO_ID_MODALIDAD_POSTU)
                .ToList();
            var entrants = postulantes.Select<DBOTblPostulante, Entrant>(e => e).OrderBy(o => o.student_name).ToList();

            // No Ingresantes
            foreach (var e in entrants)
            {
                applicants = applicants.Where(w => w != e.student_id).ToArray();
            }

            var alumnoBasico = dboContext.view_alumno_basico.Where(w => applicants.Contains(w.IDAlumno)).ToList();
            var noEntrants = new List<Entrant>();
            foreach (var a in alumnoBasico)
            {
                var e = new Entrant
                {
                    student_id = a.IDAlumno,
                    student_name = a.NomCompleto,
                    school_id = NO_SCHOOL
                };
                noEntrants.Add(e);
            }

            // Códigos No Encontrados
            foreach (var a in alumnoBasico)
            {
                applicants = applicants.Where(w => w != a.IDAlumno).ToArray();
            }

            var applicantsNotFound = new List<Entrant>();
            foreach (var applicant in applicants)
            {
                var e = new Entrant
                {
                    student_id = applicant,
                    student_name = "Código No Encontrado",
                    school_id = NO_SCHOOL
                };
                applicantsNotFound.Add(e);
            }

            return entrants.Union(noEntrants).Union(applicantsNotFound).OrderBy(o => o.student_name).ToList();
        }

        /// <summary>
        /// Lista de ingresantes a la UC de la modalidad PRONABEC según tipo PRONABEC y escuela académica
        /// </summary>
        /// <param name="pronabecType">Tipo PRONABEC</param>
        /// <param name="schoolId">Escuela académica</param>
        /// <returns>Lista de ingresantes a la UC de la modalidad PRONABEC según tipo PRONABEC y escuela académica</returns>
        public static async Task<List<Entrant>> GetPronabecEntrantsByTypeAndSchoolAsync(decimal pronabecType,
            string schoolId)
        {
            var dboContext = new DBOContext();
            var postulantes = await dboContext.tbl_postulante
                .Where(w => w.IDDependencia == _corp.UC &&
                            w.Ingresante == _admission.INGRESANTE &&
                            w.Renuncia == _admission.NO_RENUNCIA_CARRERA &&
                            w.IDEscuela == schoolId &&
                            w.IDModalidadPostu == pronabecType).ToListAsync();

            var scholarships = new Scholarships();
            var pronabecIdList = (await scholarships.GetPronabecScholarshipsAsync()).Select(w => w.TypeOfIncomeId)
                .ToList();
            var pronabecEntrants = postulantes.Where(w => pronabecIdList.Contains(w.IDModalidadPostu))
                .Select<DBOTblPostulante, Entrant>(s => s).OrderBy(o => o.student_name).ToList();
            return pronabecEntrants;
        }

        /// <summary>
        /// Fecha de Admisión de un ingresante a la UC. 
        /// Retorna fecha con formato tipo: "13 de Mayo de 2017"
        /// </summary>
        /// <param name="studentId">Código de estudiante</param>
        /// <param name="entryPeriod">Período de ingreso a la UC</param>
        /// <param name="schoolId">Código de escuela académica</param>
        /// <param name="modalityId">Modalidad de estudios (admisión)</param>
        /// <returns>Fecha de admisión en la que ingresó a la UC</returns>
        public static string GetDateOfAdmission(string studentId, string entryPeriod, string schoolId,
            string modalityId)
        {
            var dboContext = new DBOContext();
            var dboTblPostulante = dboContext.tbl_postulante.FirstOrDefault(w => w.IDAlumno == studentId &&
                                                                                 w.IDPerAcad == entryPeriod &&
                                                                                 w.IDEscuela == schoolId &&
                                                                                 w.IDEscuelaADM == modalityId);
            return dboTblPostulante != null
                ? $"{dboTblPostulante.IDExamen:dd} de {dboTblPostulante.IDExamen.ToString("MMMM", new CultureInfo("es-ES"))} de {dboTblPostulante.IDExamen:yyyy}"
                : string.Empty;
        }

        /// <summary>
        /// Datos de ingresante de un Estudiante UC.
        /// Coge el registro más actual relacionado al estudiante-carrera
        /// </summary>
        /// <param name="studentId">Código de Estudiante</param>
        /// <param name="schoolId">Código de Escuela Académica</param>
        /// <returns></returns>
        public static Entrant GetEntrantInfo(string studentId, string schoolId)
        {
            var dboContext = new DBOContext();
            var pidm = Student.getPidmFromStudentId(studentId);
            var studentIdList = Student.getAllStudentIdRelatedToSchoolId(pidm, schoolId);
            var schoolIdList = new List<string> {schoolId};

            if (schoolId == ING_SISTEMAS) schoolIdList.Add(ING_SISTEMAS_OLD);

            var movEstudiante = Student.GetMovimientosEstudiante(Student.getPidmFromStudentId(studentId))
                .FirstOrDefault(w => w.CODIGO_PROGRAMA == schoolId);

            var entrant =
                dboContext.tbl_postulante.Where(w => studentIdList.Contains(w.IDAlumno) &&
                                                     schoolIdList.Contains(w.IDEscuela) &&
                                                     w.Ingresante == _admission.INGRESANTE &&
                                                     w.Renuncia == _admission.NO_RENUNCIA_CARRERA &&
                                                     w.IDModalidadPostu != NO_ID_MODALIDAD_POSTU)
                    .OrderByDescending(o => o.IDPerAcad)
                    .ThenByDescending(o => o.IDExamen)
                    .Select(s => new Entrant
                    {
                        student_id = s.IDAlumno,
                        student_name = string.Empty,
                        type_of_income_id = s.IDModalidadPostu,
                        type_of_income_name = string.Empty,
                        entry_period = s.IDPerAcad,
                        school_id = schoolId,
                        school_name = string.Empty,
                        modality_id = s.IDEscuelaADM,
                        modality_name = string.Empty,
                    })
                    .FirstOrDefault();

            if (entrant == null)
            {
                throw new NullReferenceException(
                    $"No se encontró información de ingreso de {studentId} para {schoolId}");
            }

            entrant.student_name = Student.getStudentName(entrant.student_id);
            entrant.type_of_income_name = Admission.getTypeOfIncomeName(entrant.type_of_income_id);
            entrant.school_name = Program.get(entrant.school_id).name;
            entrant.modality_name = _admission.MODALITY_OF_ADMISSION[entrant.modality_id];

            if (movEstudiante == null) return entrant;

            entrant.modality_id = movEstudiante.CODIGO_DEPARTAMENTO;
            entrant.modality_name = _acad.DEPARTMENT_NAMES_CAU[movEstudiante.CODIGO_DEPARTAMENTO];

            return entrant;
        }

        /// <summary>
        /// Datos de ingresante de un Estudiante UC.
        /// Coge el registro más actual relacionado al estudiante-carrera-periodo
        /// </summary>
        /// <param name="studentId">Código de Estudiante</param>
        /// <param name="schoolId">Código de Escuela Académica</param>
        /// <param name="academicPeriod">Período Académico</param>
        /// <returns>Datos de ingresante de un Estudiante UC</returns>
        public static Entrant GetEntrantInfo(string studentId, string schoolId, string academicPeriod)
        {
            var dboContext = new DBOContext();
            var pidm = Student.getPidmFromStudentId(studentId);
            var studentIdList = Student.getAllStudentIdRelatedToSchoolId(pidm, schoolId);
            var schoolIdList = new List<string> {schoolId};
            if (schoolId == ING_SISTEMAS)
            {
                schoolIdList.Add(ING_SISTEMAS_OLD);
            }

            var entrant =
                dboContext.tbl_postulante.Where(w => studentIdList.Contains(w.IDAlumno) &&
                                                     schoolIdList.Contains(w.IDEscuela) &&
                                                     w.IDPerAcad == academicPeriod &&
                                                     w.Ingresante == _admission.INGRESANTE &&
                                                     w.Renuncia == _admission.NO_RENUNCIA_CARRERA &&
                                                     w.IDModalidadPostu != NO_ID_MODALIDAD_POSTU)
                    .OrderByDescending(o => o.IDPerAcad)
                    .ThenByDescending(o => o.IDExamen)
                    .Select(s => new Entrant
                    {
                        student_id = s.IDAlumno,
                        student_name = string.Empty,
                        type_of_income_id = s.IDModalidadPostu,
                        type_of_income_name = string.Empty,
                        entry_period = s.IDPerAcad,
                        school_id = schoolId,
                        school_name = string.Empty,
                        modality_id = s.IDEscuelaADM,
                        modality_name = string.Empty,
                    })
                    .OrderByDescending(o => o.entry_period)
                    .FirstOrDefault();

            if (entrant == null)
            {
                return null;
            }

            entrant.student_name = Student.getStudentName(entrant.student_id);
            entrant.type_of_income_name = Admission.getTypeOfIncomeName(entrant.type_of_income_id);
            entrant.school_name = Program.get(entrant.school_id).name;
            entrant.modality_name = _admission.MODALITY_OF_ADMISSION[entrant.modality_id];

            return entrant;
        }


        public static string GetCurrentModalityPostulationBySchoolIdAndDepartment(decimal pidm, string schoolId, string department) {
            var modality = Student.GetMovimientosEstudianteBySchoolId(pidm, schoolId).
                        FirstOrDefault(f => f.CODIGO_DEPARTAMENTO == department).CODIGO_MOD_ADM.ToString(CultureInfo.InvariantCulture);

            if (modality == _admission.NO_ID_MODALIDAD_POSTU_BANNER && (schoolId == _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA_OLD || schoolId == _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA))
            {

                modality = Student.GetMovimientosEstudianteBySchoolId(pidm, _corp.PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA_OLD).
                   FirstOrDefault(f => f.CODIGO_DEPARTAMENTO == department).CODIGO_MOD_ADM.ToString(CultureInfo.InvariantCulture);
            }

            return modality;
        }
    #endregion Methods


    #region Implicit Operators

    /// <summary>
    /// Conversion implícita de 'DBOTblPostulante' a 'Entrant'
    /// </summary>
    /// <param name="dboTblPostulante">Tabla dbo.TblPostulante</param>
    /// <returns>Tipo de dato 'Entrant'</returns>
    public static implicit operator Entrant(DBOTblPostulante dboTblPostulante)
        {
            var entrant = new Entrant
            {
                student_name = Student.getStudentName(dboTblPostulante.IDAlumno),
                student_id = dboTblPostulante.IDAlumno,
                type_of_income_id = dboTblPostulante.IDModalidadPostu,
                type_of_income_name = Admission.getTypeOfIncomeName(dboTblPostulante.IDModalidadPostu),
                entry_period = dboTblPostulante.IDPerAcad,
                school_id = dboTblPostulante.IDEscuela,
                school_name = Program.get(dboTblPostulante.IDEscuela).name,
                modality_id = dboTblPostulante.IDEscuelaADM,
                modality_name = _admission.MODALITY_OF_ADMISSION[dboTblPostulante.IDEscuelaADM]
            };
            return entrant;
        }

        #endregion Implicit Operators
    }
}