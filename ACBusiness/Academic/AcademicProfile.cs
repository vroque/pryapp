﻿using ACBusiness.Admision;
using ACBusiness.AtentionCenter;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BANNER;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.BANNER.SATURN;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using ACPermanence.DataBases.NEXO.DIM;
using System;
using System.Collections.Generic;
using System.Linq;
using ACBusiness.Graduation;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACBusiness.Academic
{
    /// <author>Arturo Bolaños</author>
    /// <date>16/09/2016</date>
    /// <summary>
    /// Perfil de Estudiante
    /// </summary>
    public class AcademicProfile
    {
        #region properties

        public decimal person_id { get; set; }
        public string div { get; set; }
        public Campus campus { get; set; }
        public string department { get; set; }
        public string subject { get; set; }
        public College college { get; set; }
        public Program program { get; set; }
        public string levl { get; set; }
        public AdmisionType adm_type { get; set; }       
        public GraduateInformation GraduateInfo { get; set; }
        public int status { get; set; }
        public string status_name { get; set; }
        public string degree { get; set; }
        public string term_catalg { get; set; }
        public List<string> terms { get; set; }

        private readonly BNContext _bn = new BNContext();
        private readonly DBOContext _dboContext = new DBOContext();

        #endregion properties

        #region constructors

        #endregion

        #region methods

        public List<Requirement> requeriments()
        {
            var req = new Requirement();
            List<Requirement> list = req.GetByProfile(this);
            return list;
        }

        /// <summary>
        /// Obtener Periodo(ciclo) actual del alumno
        /// </summary>
        /// <returns></returns>
        public int actualCycle()
        {
            var term = new ACTerm().getCurrent(this.campus.id, this.department);
            var cycle = this.cycleByTerm(term.term);
            return cycle;
        }

        /// <summary>
        /// Obtener ciclo actual del alumno de un determinado periodo
        /// </summary>
        /// <returns></returns>
        public int cycleByTerm(string term)
        {
            var currentTerm = new ACTerm().getCurrent(this.campus.id, this.department);

            /**
             * Se modifica el período a consultar
             * (a un período no válido, mayor al período a consultar y menor al siguiente período válido)
             * para que sea considerado en la consulta en P_CICLO_CREDITOS_ALUMNO
             */
            if (term != currentTerm.term) term = $"{term.Substring(0, 5)}5";

            List<PCicloCreditosAlumno> ccas = _bn.p_creditos_alumno(this.person_id, term, this.program.id).ToList();
            var cca = ccas.FirstOrDefault();

            if (cca == null) throw new ArgumentNullException($"No hay datos de ciclo del alumno en el periodo: {term}");

            var cycle = 0;

            // si no es el ciclo actual restar creditos matriculdos
            int.TryParse(cca.NOMINALCYCLE.ToString(), out cycle);
            return cycle;
        }

        /// <summary>
        /// Obtiene el Promedio Global Acumulado
        /// de un alumno
        /// </summary>
        /// <returns></returns>
        public float getGPA()
        {
            float gpa = 0;
            float credits = 0;
            List<Course> courses = new Course().listAllByStudent(this);
            foreach (var course in courses)
            {
                float score = 0;
                float.TryParse(course.score, out score);
                gpa += course.credits * score;
                credits += course.credits;
            }

            if (credits > 0)
                gpa = gpa / credits;
            return gpa;
        }

        #endregion

        #region static methods

        /// <summary>
        /// Lista los perfiles academicos de un estudiantes
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public static List<AcademicProfile> listProfiles(decimal pidm)
        {
            List<AcademicProfile> profiles = new List<AcademicProfile>();
            //Traer postulaciones
            List<AcademicProfile> postulations = listPostulation(pidm);
            //Traer matriculas
            List<AcademicProfile> enrollments = listHistory(pidm);
            //Traer Egresos
            List<AcademicProfile> graduations = GraduationsList(pidm);
            //agegar egresos
            // si ya tienen egreso no se agrega matricula
            profiles.AddRange(enrollments);

            // si ya tiene egreso o matricula no se agrega postulacion
            foreach (AcademicProfile item in postulations)
            {
                int n = profiles.Count(f => f.div == item.div && f.program.id == item.program.id &&
                                            f.department == item.department);
                if (n == 0) profiles.Add(item);
            }

            //Buscamos egresos
            foreach (var item in profiles)
            {
                var graduate = graduations.FirstOrDefault(f =>
                    f.div == item.div
                    && f.program.id == item.program.id);
                if (graduate != null)
                {
                    item.GraduateInfo = graduate.GraduateInfo;
                    item.status = graduate.status;
                    item.status_name = _acad.STUDENT_STATES[graduate.status];
                }
            }

            return profiles;
        }

        /// <summary>
        /// Lista las postulaciones de una alumno y genera el 
        /// respectivo Perfil Academico para su posterios uso
        /// </summary>
        /// <param name="pidm">Identificador de Persona(Banner)</param>
        /// <returns></returns>
        private static List<AcademicProfile> listPostulation(decimal pidm)
        {
            var profiles = new List<AcademicProfile>();

            string studentId = Student.getStudentCodeFromPidm(pidm);
            List<Postulation> list = new Postulation().list(studentId);
            foreach (var item in list)
            {
                var profile = new AcademicProfile();
                profile.person_id = pidm;
                profile.div = item.div;
                profile.campus = Campus.get(item.campus_id);
                profile.program = Program.get(item.program_id);
                profile.department = item.department;
                profile.college = College.get(item.college_id);
                profile.levl = item.levl;
                profile.adm_type = AdmisionType.get(item.adm_type_id, profile: profile, source: "apec");
                profile.status = item.state;
                profile.status_name = _acad.STUDENT_STATES[item.state];
                profiles.Add(profile);
            }

            return profiles;
        }

        /// <summary>
        /// Lista las carreras(programs) que el alumno 
        /// tiene registradas en banner y genera el respectivo Perfil Academico 
        /// para su posterios uso
        /// </summary>
        /// <param name="pidm">Identificador de Persona(Banner)</param>
        /// <returns></returns>
        private static List<AcademicProfile> listHistory(decimal pidm)
        {
            var bnContext = new BNContext();

            var profiles = new List<AcademicProfile>();
           
            // obtener data anterior
            var data = bnContext.tbl_SORLCUR
                .Where(f => f.SORLCUR_PIDM == pidm
                        && f.SORLCUR_CACT_CODE == "ACTIVE"
                        && f.SORLCUR_ROLL_IND == "Y"
                        && f.SORLCUR_TERM_CODE_CTLG != "000000"
                        && f.SORLCUR_LEVL_CODE == "PG")
                .ToList()
                .Where(
                    f =>
                        string.Compare(f.SORLCUR_TERM_CODE, "201710") < 0
                        || (string.Compare(f.SORLCUR_TERM_CODE, "201710") >= 0
                            && f.SORLCUR_TERM_CODE_END == null)
                ).Select(
                    result => new {
                        KEY_SEQNO = result.SORLCUR_KEY_SEQNO,
                        PROGRAM = result.SORLCUR_PROGRAM,
                        COLL_CODE = result.SORLCUR_COLL_CODE,
                        LEVL_CODE = result.SORLCUR_LEVL_CODE,
                        TERM_CODE_ADMIT = result.SORLCUR_TERM_CODE_ADMIT,
                        CAMP_CODE = result.SORLCUR_CAMP_CODE,
                        DEGC_CODE = result.SORLCUR_DEGC_CODE
                    }
                )
                .ToList();

            // Obtener data despues de 201710
            var dataAfter =
                bnContext.tbl_SZVCMPP.Where(item => item.SFRSTCR_PIDM == pidm).ToList()
                .Where(
                        item =>
                            string.Compare(item.SSBSECT_TERM_CODE, "201710") >= 0
                            && item.SORLCUR_LEVL_CODE == "PG"
                    )
                .Select(
                        result => new
                        {
                            KEY_SEQNO = result.SORLCUR_KEY_SEQNO,
                            PROGRAM = result.SORLCUR_PROGRAM,
                            COLL_CODE = result.SORLCUR_COLL_CODE,
                            LEVL_CODE = result.SORLCUR_LEVL_CODE,
                            TERM_CODE_ADMIT = result.SORLCUR_TERM_CODE_ADMIT,
                            CAMP_CODE = result.SORLCUR_CAMP_CODE,
                            DEGC_CODE = result.SORLCUR_DEGC_CODE
                        }
                    ).ToList();

            // Obtener data de reincorporados despues de 201710
            var dataRereincorporated = bnContext.tbl_SGBSTDN
                .Where(
                    item =>
                        item.SGBSTDN_PIDM == pidm
                        && item.SGBSTDN_STST_CODE == "AS" // ACTIVO
                        && item.SGBSTDN_STYP_CODE == "R" // REINCORPORADO
                        && string.Compare(item.SGBSTDN_TERM_CODE_EFF, "201710") >= 0
                        && item.SGBSTDN_LEVL_CODE == "PG"
                ).ToList().OrderByDescending(order => order.SGBSTDN_TERM_CODE_EFF)
                .Select(
                        result => new
                        {
                            KEY_SEQNO = 5,
                            PROGRAM = result.SGBSTDN_PROGRAM_1,
                            COLL_CODE = result.SGBSTDN_COLL_CODE_1,
                            LEVL_CODE = result.SGBSTDN_LEVL_CODE,
                            TERM_CODE_ADMIT = result.SGBSTDN_TERM_CODE_ADMIT,
                            CAMP_CODE = result.SGBSTDN_CAMP_CODE,
                            DEGC_CODE = result.SGBSTDN_DEGC_CODE_1
                        }
                    ).FirstOrDefault();
        
            data.AddRange(dataAfter);
            if (dataRereincorporated != null) data.Add(dataRereincorporated);            

            // buscamos las postulaciones aparte
            List<SARADAP> postulations = bnContext.tbl_SARADAP
                .Where(f => f.SARADAP_PIDM == pidm && f.SARADAP_ADMT_CODE != "99" && f.SARADAP_LEVL_CODE == "PG").ToList();


            List<string> programs = data.Select(f => f.PROGRAM).Distinct().ToList();
            foreach (var item in programs)
            {
                var profile = new AcademicProfile();
                // siempre el ultimo
                var pro = data.OrderByDescending(f => f.KEY_SEQNO)
                    .FirstOrDefault(f => f.PROGRAM == item);
                var lastMov = Student.GetLastMovimientoEstudianteBySchoolId(pidm, pro.PROGRAM);
                profile.person_id = pidm;
                profile.div = _acad.DEFAULT_DIV;
                profile.campus = Campus.get(lastMov.CODIGO_CAMPUS);
                profile.program = Program.get(pro.PROGRAM);
                profile.department = lastMov.CODIGO_DEPARTAMENTO;
                profile.college = College.get(lastMov.CODIGO_FACULTAD);
                profile.levl = pro.LEVL_CODE;

                profile.degree = pro.DEGC_CODE;
                profile.term_catalg = getRigthTermCatalog(lastMov.CATALOGO_REAL, profile.department,
                    profile.program.id);
                profile.status = _acad.STATE_STUDENT;
                profile.status_name = _acad.STUDENT_STATES[_acad.STATE_STUDENT];
                //buscamos las postulaciones
                SARADAP postulation = postulations.FirstOrDefault(f =>
                    f.SARADAP_TERM_CODE_ENTRY == pro.TERM_CODE_ADMIT
                    && f.SARADAP_LEVL_CODE == pro.LEVL_CODE
                    && f.SARADAP_CAMP_CODE == pro.CAMP_CODE
                    && f.SARADAP_MAJR_CODE_1 == pro.PROGRAM
                );
                //si no existe de la carrera traemos la ultima
                //... es estupido lo se ...
                if (postulation == null)
                {
                    postulation = postulations.OrderByDescending(f => f.SARADAP_TERM_CODE_ENTRY).FirstOrDefault();
                }

                profile.adm_type = AdmisionType.get(postulation.SARADAP_ADMT_CODE, profile);
                profiles.Add(profile);
            }


            return profiles;
        }

        /// <summary>
        /// Lista los egresos (egresado, bachiller, titulado) de la persona
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public static List<AcademicProfile> GraduationsList(decimal pidm)
        {
            List<AcademicProfile> profiles;
            //pendiente hasta que haya egresados
            var dboContext = new DBOContext();
            profiles = dboContext.view_alumno_basico
                .Join(dboContext.tbl_alumno_egresado,
                    alu => alu.IDAlumno,
                    egr => egr.IDAlumno,
                    (alu, egre) => new {alu, egre})
                .Where(f => f.alu.IDPersonaN == pidm)
                .ToList()
                .Select(f => new AcademicProfile
                {
                    div = f.egre.IDDependencia,
                    program = Program.get(f.egre.IDEscuela),
                    GraduateInfo = new GraduateInformation(f.egre.ModalidadEgreso, f.egre.IDPerAcad),
                    status = f.egre.Titulado == "1"? 
                        _acad.STATE_DEGREE: f.egre.Bachiller == "1"? 
                            _acad.STATE_BACHELOR:_acad.STATE_GRADUATE
                })
                .ToList();
            return profiles;
        }

        /// <summary>
        /// Lista los periodos(term) del alumno
        /// </summary>
        /// <returns></returns>
        public List<string> getTerms()
        {
            var enrollment = new Enrollment();
            this.terms = enrollment.getAllTermsEnrollment(this);
            return this.terms;
        }

        /// <summary>
        /// Obtiene el codigo de alumno de APEC
        /// </summary>
        /// <returns></returns>
        public string getAPECCode()
        {
            var dboContext = new DBOContext();
            int id = (int) this.person_id;
            string char_department = Department.toAPEC(this.department);
            string studentId = dboContext.view_alumno_basico
                .Join(dboContext.tbl_postulante,
                    al => al.IDAlumno,
                    po => po.IDAlumno,
                    (al, po) => new {al, po})
                .Where(f => f.al.IDPersonaN == id
                            && f.po.IDEscuelaADM == char_department
                            && f.po.IDEscuela == this.program.id)
                .Select(f => f.po.IDAlumno)
                .FirstOrDefault();
            return studentId;
        }

        /// <summary>
        /// Códigos APEC relacionados a una carrera
        /// </summary>
        /// <returns>Lista de códigos APEC relacionados a una carrera</returns>
        public List<string> GetAPECStudentIdListByProgram()
        {
            var apecDepartmentName = Department.toAPEC(department);
            List<string> studentIdList = _dboContext.view_alumno_basico
                .Join(_dboContext.tbl_postulante,
                    ab => ab.IDAlumno,
                    p => p.IDAlumno,
                    (ab, p) => new {ab, p})
                .Where(w => w.ab.IDPersonaN == person_id
                            && w.p.IDEscuelaADM == apecDepartmentName
                            && w.p.IDEscuela == program.id)
                .Select(s => s.p.IDAlumno)
                .ToList();

            return studentIdList;
        }

        #endregion static methods

        private static string getRigthTermCatalog(string term, string department, string program)
        {
            var bnContext = new BNContext();
            TermCatalogObject to = bnContext.tbl_SZVCRED
                .FirstOrDefault(f => f.SZVCRED_DEPT_CODE == department
                                     && f.SZVCRED_PROGRAM == program
                                     && f.SZVCRED_TERM_CODE_CTLG.CompareTo(term) <= 0
                                     && f.SZVCRED_TERM_CODE_CTLG_END.CompareTo(term) >= 0);

            return to == null ? term : to.term_catalog;
        }

        #region implicite operators

        public static implicit operator AcademicProfile(DIMTblGraduate obj)
        {
            var profile = new AcademicProfile();
            profile.person_id = obj.pidm;
            profile.div = obj.divs;
            profile.campus = Campus.get(obj.campus);
            profile.program = Program.get(obj.program);
            profile.department = obj.departament;
            profile.college = College.get(obj.college);
            profile.levl = obj.levl;
            profile.adm_type = AdmisionType.get(obj.admType, profile);
            profile.status = _acad.STATE_GRADUATE;
            return profile;
        }

        #endregion implicite operators

        public class TermCatalogObject
        {
            public string term_catalog { get; set; }
            public int credits_mandatory { get; set; }
            public int credits_elective { get; set; }

            public static implicit operator TermCatalogObject(SZVCRED obj)
            {
                if (obj == null) return null;
                TermCatalogObject t = new TermCatalogObject();
                t.term_catalog = obj.SZVCRED_TERM_CODE_CTLG;
                t.credits_elective = obj.SZVCRED_CRED_E ?? 0;
                t.credits_mandatory = obj.SZVCRED_CRED_O ?? 0;
                return t;
            }
        }
    }
}