using System.Linq;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;

namespace ACBusiness.Academic
{
    public class Admission
    {
        protected Admission(){
        }
        /// <summary>
        /// Nombre de la modalidad de postulación
        /// </summary>
        /// <param name="type_of_income_id">Código de la modalidad de postulación</param>
        /// <returns>Nombre de la modalidad de postulación</returns>
        public static string getTypeOfIncomeName(decimal type_of_income_id)
        {
            DBOContext dbo_context = new DBOContext();
            DBOTblModalidadPostu dbo_tbl_modalidad_postu =
                dbo_context.tbl_modalidad_postu.FirstOrDefault(w => w.IDModalidadPostu == type_of_income_id);

            return dbo_tbl_modalidad_postu != null ? dbo_tbl_modalidad_postu.Descripcion : string.Empty;
        }
    }
}