﻿using System.Collections.Generic;
using System.Linq;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.Academic
{
    /// <summary>
    /// Períodos de Admisión UC
    /// </summary>
    public class AdmissionPeriods
    {
        #region Properties

        public string period_id { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Lista de Períodos Académicos de Admisión de la UC
        /// </summary>
        /// <returns></returns>
        public static List<AdmissionPeriods> getAll()
        {
            DBOContext dbo_context = new DBOContext();
            List<AdmissionPeriods> periods = dbo_context.tbl_postulante.Where(w => w.IDDependencia == "UCCI")
                .Select(s => new AdmissionPeriods {period_id = s.IDPerAcad})
                .Distinct()
                .OrderByDescending(o => o.period_id)
                .ToList();
            return periods;
        }

        #endregion Methods
    }
}