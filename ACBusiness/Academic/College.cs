﻿using System.Collections.Generic;
using System.Linq;
using ACPermanence.DataBases.NEXO.DIM;
using ACPermanence.Contexts.NEXO;

namespace ACBusiness.Academic
{
    public class College
    {
        #region Properties

        public string id { get; set; }
        public string name { get; set; }
        public List<Program> programs { get; set; }

        #endregion Properties

        #region static methods

        /// <summary>
        /// Obtiene el college(facultad) segun el id enviado
        /// </summary>
        /// <param name="collegeId"></param>
        /// <returns></returns>
        public static College get(string collegeId)
        {
            var nexoContext = new NEXOContext();
            College result = nexoContext.dim_colleges.FirstOrDefault(x => x.college == collegeId);
            return result;
        }

        /// <summary>
        /// get College from program ID
        /// </summary>
        /// <param name="programId">program id</param>
        /// <returns></returns>
        public static College getByProgramID(string programId)
        {
            var nexoContext = new NEXOContext();
            var collegeProgram = nexoContext.dim_colleges_programs.FirstOrDefault(f => f.program == programId);

            if (collegeProgram == null) return null;

            var college = get(collegeProgram.college);
            return college;
        }

        /// <summary>
        /// Lista todos los colleges de un lvl(nivel)
        /// </summary>
        /// <param name="levl">Nivel(pregrado, prosgrado, etc)</param>
        /// <returns>Lista de id y nombre del colegio</returns>
        public static List<College> listCollegeByLvl(string levl)
        {
            var nexoContext = new NEXOContext();
            List<College> list = nexoContext.dim_colleges
                .Join(
                    nexoContext.dim_colleges_programs,
                    col => col.college,
                    colpr => colpr.college,
                    (col, colpr) => new {col = col, colpr = colpr})
                .Where(f => f.colpr.active == "1" && f.colpr.levl == levl)
                .ToList()
                .Select(f => new College
                {
                    id = f.col.college,
                    name = f.col.collegeDescription
                })
                .ToList();

            return list;
        }

        /// <summary>
        /// Lista todos los colleges de un lvl(nivel)
        /// </summary>
        /// <param name="levl">Nivel(pregrado, prosgrado, etc)</param>
        /// <returns>Lista de id, nombre del colegio, su programa y su nombre del programa</returns>
        public static List<College> listCollegeByLvlWithPrograms(string levl)
        {
            var nexoContext = new NEXOContext();

            var colleges = nexoContext.dim_colleges
                .Join(
                    nexoContext.dim_colleges_programs,
                    col => col.college,
                    colpr => colpr.college,
                    (col, colpr) => new {col = col, colpr = colpr})
                .Where(w => w.colpr.active == "1" && w.colpr.levl == levl)
                .Select(f => new
                {
                    college_id = f.col.college,
                    college_name = f.col.collegeDescription,
                    program_id = f.colpr.program,
                    program_name = string.Empty,
                    levl = f.colpr.levl
                })
                .OrderBy(o => o.college_name)
                .ToList();

            //componer el objeto
            var list = new List<College>();
            foreach (var item in colleges)
            {
                College col;
                if (list.FirstOrDefault(f => f.id == item.college_id) == null)
                {
                    col = new College
                    {
                        id = item.college_id,
                        name = item.college_name,
                        programs = new List<Program>()
                    };
                    list.Add(col);
                }
                else
                    col = list.FirstOrDefault(f => f.id == item.college_id);

                var pr = Program.get(item.program_id);
                if (pr != null) col.programs.Add(pr);
            }

            return list;
        }

        #endregion static methods

        #region implicits

        public static implicit operator College(DIMTblCollege obj)
        {
            if (obj == null) return null;
            var faculty = new College {id = obj.college, name = obj.collegeDescription};
            return faculty;
        }

        #endregion
    }
}