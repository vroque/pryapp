﻿using ACBusiness.AtentionCenter;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BANNER;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.BANNER.SATURN;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using ACPermanence.DataBases.DBUCCI.DBO.procedures;
using ACPermanence.DataBases.NEXO.DIM;
using ACTools.SuperString;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;
using _inst = ACTools.Constants.InstitucionalConstants;
using _learningConst = ACTools.Constants.LearningAssessmentConstants;

namespace ACBusiness.Academic
{
    /// <summary>
    /// Asignaturas
    /// </summary>
    public class Course
    {
        #region properties
        public string id { get; set; }
        public string equivalent { get; set; }
        public string name { get; set; }
        public string subject { get; set; }
        public string nrc { get; set; }
        public string partTerm { get; set; }
        public string campus { get; set; }
        public string department { get; set; }
        /// <summary>
        /// O => obligatorio
        /// E => electivo
        /// </summary>
        public string type { get; set; }
        public int cycle { get; set; }
        public int credits { get; set; }
        public string score { get; set; }
        public string score_old { get; set; }
        public string is_active { get; set; }
        public string term { get; set; }
        public string observation { get; set; }
        public DateTime? date { get; set; }
        public ScoreDetail score_detail { get; set; }
        public List<Component> components { get; set; }
        public bool is_aproved { get; set; }
        
        /// <summary>
        /// Horas teóricas
        /// </summary>
        public decimal? TheoreticalHours { get; set; }
        
        /// <summary>
        /// Horas prácticas
        /// </summary>
        public decimal? PracticalHours { get; set; }

        public List<Prerequisite> prerequisite_list { get; set; }
        /// <summary>
        /// Creditos minimos requeridos
        /// </summary>
        public decimal? creditsRequered { get; set; }

        private readonly BNContext _bn = new BNContext();
        private readonly DBOContext _dboContext = new DBOContext();

        #endregion properties

        public Course() {
            this.score_detail = new ScoreDetail();
        }

        #region methods

        /// <summary>
        /// Course name
        /// </summary>
        /// <param name="subjectCode">SUBJ_CODE</param>
        /// <param name="courseNumber">CRSE_NUMB</param>
        /// <returns></returns>
        public static async Task<string> GetCourseNameAsync(string subjectCode, string courseNumber)
        {
            var bnContext = new BNContext();
            var courseName = await bnContext.tbl_SCRSYLN.FirstOrDefaultAsync(w =>
                w.SCRSYLN_SUBJ_CODE == subjectCode && w.SCRSYLN_CRSE_NUMB == courseNumber);

            return courseName?.SCRSYLN_LONG_COURSE_TITLE ?? string.Empty;
        }
        
        /// <summary>
        /// Listar todos los cursos llevados por un perfil de estudiante
        /// </summary>
        /// <param name="profile"></param>
        /// <returns> Lista de Cursos</returns>
        public List<Course> listAllByStudent(AcademicProfile profile, string type = "history")
        {
            List<Course> list;
            if (type == "history")
                list = _bn.p_historia_carrera_alumno(profile.person_id, profile.program.id)
                    .ToList()
                    .Select<PHistoriaXCarreraAlumno, Course>(f => f)
                    .ToList();
            else
                list = _bn.p_certificado_carrera_alumno(profile.person_id, profile.program.id)
                    .ToList()
                    .Select<PHistoriaXCarreraAlumno, Course>(f => f)
                    .ToList();
            return list;

        }

        /// <summary>
        /// Listar todos los cursos llevados por un perfil de
        /// estudiante de lo periodos(ciclos) determinados por la lista
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="terms"></param>
        /// <returns></returns>
        public List<Course> listByStudentPeriods(AcademicProfile profile, List<string> period_list)
        {
            period_list = period_list.Select(f => f.completeZeros(2)).ToList();
            List<Course> list = _bn.p_certificado_carrera_alumno(profile.person_id, profile.program.id)
                .Where(f => period_list.Contains(f.CYCLE))
                .ToList()
                .Select<PHistoriaXCarreraAlumno, Course>(f => f)
                .ToList();

            return list;
        }

        /// <summary>
        /// Listar todos los cursos llevados por un perfil de
        /// estudiante de lo periodos(ciclos) determinados por la lista
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="terms"></param>
        /// <returns></returns>
        public List<Course> listByStudentTerm(AcademicProfile profile, string term)
        {
            List<Course> list = _bn.p_historia_periodo_alumno(profile.person_id, profile.program.id, term)
                .ToList()
                .Select<PHistoriaXCarreraAlumno, Course>(f => f)
                .ToList();
            return list;
        }

        /// <summary>
        /// Obtener boleta de notas de banner
        /// desde 201710 en adelante BANNER
        /// del   201710  hacia atras APEC
        /// </summary>
        /// <param name="profile">perfil</param>
        /// <param name="termId">Periodo</param>
        /// <returns></returns>
        public List<Course> reportCardtByTerm(AcademicProfile profile, string termId)
        {
            var mov = Student.GetMovementInfoByTerm(profile.person_id, termId);
            if (mov!= null)
            {
                var campus = Campus.get(mov.CampusId);
                profile.campus = campus;
                profile.department = mov.DepartmentId;
            }
            return string.Compare(termId, "201710") >= 0
                ? reportCardtByTermBANNER(profile, termId)
                : reportCardByTermAPEC(profile, termId);
        }

        public List<Course> consolidatedByTerm(AcademicProfile profile, string term)
        {
            List<Course> list = this.reportCardtByTerm(profile, term);
            foreach(Course element in list)
            {
                SSBSECT time = _bn.tbl_SSBSECT.FirstOrDefault(f =>
                                                            f.SSBSECT_TERM_CODE == element.term
                                                            && f.SSBSECT_SUBJ_CODE == element.subject
                                                            && f.SSBSECT_CRN == element.nrc
                                                            && f.SSBSECT_CRSE_NUMB == element.id);
                if (time!=null)
                {
                    element.TheoreticalHours = time.SSBSECT_LEC_HR;
                    element.PracticalHours = time.SSBSECT_LAB_HR;
                }
            }

            return list;
        }

        /// <summary>
        /// Boleta de notas del alumno desde APEC
        /// para peridos inferiores al 201710
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        private List<Course> reportCardByTermAPEC(AcademicProfile profile, string term)
        {
            List<string> studentIdList = profile.GetAPECStudentIdListByProgram();
            var courses = new List<Course>();
            foreach (var studentId in studentIdList)
            {
                List<Course> coursesByStudentId = _dboContext.sp_boleta_nota(
                        _inst.UC, profile.campus.code, Term.toAPEC(term, profile.department), studentId)
                    .ToList()
                    .Select<DBOSPBoletaDeNotas, Course>(f => f)
                    .ToList();
                if (coursesByStudentId.Any())
                {
                    courses = courses.Union(coursesByStudentId).ToList();
                }
            }

            return courses;
        }

        /// <summary>
        /// Obtener boleta de notas de banner
        /// desde 201710 en adelante
        /// </summary>
        /// <param name="profile">perfil</param>
        /// <param name="term_id">Periodo</param>
        /// <returns></returns>
        public List<Course> reportCardtByTermBANNER(AcademicProfile profile, string term_id)
        {
            List<Course> list = this.enrollmentsByTerm(profile, term_id);
            // obtener notas

            var data_components = _bn.tbl_SHRMRKS.Join( // LEFT JOIN componente
                 _bn.tbl_SHRGCOM, //datos de componente
                    co => new { term = co.SHRMRKS_TERM_CODE, crn = co.SHRMRKS_CRN, pidm = co.SHRMRKS_GCOM_ID },
                    dco => new { term = dco.SHRGCOM_TERM_CODE, crn = dco.SHRGCOM_CRN, pidm = dco.SHRGCOM_ID },
                    (co, dco) => new { co, dco }
                ).Where(f =>
                   f.dco.SHRGCOM_WEIGHT > 0
                  && f.co.SHRMRKS_PIDM == profile.person_id
                  && f.co.SHRMRKS_TERM_CODE == term_id
                )
                .ToList()
                .Select(f => new { f.co, f.dco })
                .ToList();

            var subcomponents = _bn.tbl_SHRSCOM.Join( // datos de  SUBCOMPONENTE
                    _bn.tbl_SHRSMRK, // nota del subcomponente
                    sco => new { id = sco.SHRSCOM_ID, term = sco.SHRSCOM_TERM_CODE, crn = sco.SHRSCOM_CRN, gcom = sco.SHRSCOM_GCOM_ID }, //pidm = sco.SHRSCOM_PIDM,
                    nsc => new { id = nsc.SHRSMRK_SCOM_ID, term = nsc.SHRSMRK_TERM_CODE, crn = nsc.SHRSMRK_CRN, gcom = nsc.SHRSMRK_GCOM_ID },//, pidm = nsc.SHRSMRK_PIDM
                    (sco, nsc) => new { sco, nsc }
                ).Where(
                    f => f.nsc.SHRSMRK_PIDM == profile.person_id
                    && f.nsc.SHRSMRK_TERM_CODE == term_id
                ).ToList()
                .Select(f => new { f.sco, f.nsc })
                .ToList();

            List<Course> list_courses = new List<Course>();
            List<string> list_names = list.Select(f => f.name).Distinct().ToList();
            List<RecoveryTest> observations = recoveryTestsByTerm(profile.person_id, term_id);
            // Aramar el objeto a retornar
            foreach (string item in list_names)
            {
                //solo el mayor para ignorar los cursos ligados de 0 creditos
                Course course = list.OrderByDescending(f => f.credits).FirstOrDefault(f => f.name == item);
                // * puede tener mas de una nota de subsanación por curso
                List<RecoveryTest> tests = observations.Where(f => f.crn == course.nrc).OrderBy(f => f.date).ToList();
                if (tests == null || tests.Count == 0)
                    course.observation = "";
                else
                {
                    string obs = "";
                    tests.ForEach(f => obs += $"{f.description} ");
                    // sustituir promedio global
                    course.observation = obs;
                    course.score = String.Format("{0:00.00}", float.Parse(tests.Last().score_grade));
                    course.date = tests.Last().date;
                }
                course.components = new List<Component>();
                course.components = data_components.Where(f =>
                   f.co.SHRMRKS_CRN == course.nrc
                ).Select(f => new Component
                {
                    id = (f.co.SHRMRKS_GCOM_ID == null) ? 0 : (int)f.co.SHRMRKS_GCOM_ID,
                    name = f.dco.SHRGCOM_DESCRIPTION,
                    score = (f.co.SHRMRKS_SCORE == null) ? "-" : String.Format("{0:00.00}", f.co.SHRMRKS_SCORE),
                    weight = f.dco.SHRGCOM_WEIGHT.ToString(),
                    date = (f.co.SHRMRKS_SCORE == null) ? null : f.co.SHRMRKS_ACTIVITY_DATE,
                    daysUploadNote = (f.co.SHRMRKS_SCORE == null) ? null : (int?)SubstracDatesWithoutWeekends(DateTime.Now, Convert.ToDateTime(f.co.SHRMRKS_ACTIVITY_DATE)),
                    code = f.dco.SHRGCOM_NAME,
                    observation = ""
                }).ToList();
                // sub componentes
                foreach (Component comp in course.components)
                {
                    //por tests
                    if(tests != null)
                    {
                        foreach (var test in tests)
                        {
                            if (test != null && comp.code == _acad.BANNER_SCORE_CONCEPT_SUST[test.code])
                            {
                                comp.score_old = comp.score;
                                // el p#$%$ test.score es nulo
                                if (test.score == null)
                                    comp.score = String.Format("{0:00.00}", float.Parse(test.score_grade));
                                else
                                    comp.score = String.Format("{0:00.00}", float.Parse(test.score));
                            }
                        }
                    }

                    comp.sub = new List<SubComponent>();
                    comp.sub = subcomponents.Where(f =>
                        f.nsc.SHRSMRK_CRN == course.nrc
                        && f.nsc.SHRSMRK_GCOM_ID == comp.id
                    ).Select(f => new SubComponent
                    {
                        id = (f.sco.SHRSCOM_ID == null) ? 0 : (int)f.sco.SHRSCOM_ID,
                        gcomId = comp.id,
                        name = f.sco.SHRSCOM_DESCRIPTION,
                        weight = (f.sco.SHRSCOM_WEIGHT == null) ? "" : f.sco.SHRSCOM_WEIGHT.ToString(),
                        score = f.nsc.SHRSMRK_GRDE_CODE,
                        date = (f.nsc.SHRSMRK_GRDE_CODE == null) ? null : f.nsc.SHRSMRK_ACTIVITY_DATE,
                        daysUploadNote = (f.nsc.SHRSMRK_GRDE_CODE == null) ? null : (int?)SubstracDatesWithoutWeekends(DateTime.Now, Convert.ToDateTime(f.nsc.SHRSMRK_ACTIVITY_DATE)),
                        observation = ""
                    })
                    .ToList();
                }
                list_courses.Add(course);
            }

            return list_courses;

        }

        /// <summary>
        /// Informacion de los examenes sustitutorios por alumno y periodo
        /// </summary>
        /// <param name="pidm">codigo de persona (del alumno)</param>
        /// <param name="term">periodo de busqueda</param>
        /// <returns></returns>
        public List<RecoveryTest> recoveryTestsByTerm(decimal pidm, string term)
        {
            List<RecoveryTest> list = _bn.tbl_SHRTCKG.Join(
                    _bn.tbl_SHRTCKN,
                    t1 => new { pidm = t1.SHRTCKG_PIDM, term = t1.SHRTCKG_TERM_CODE, seq = t1.SHRTCKG_TCKN_SEQ_NO },
                    t2 => new { pidm = t2.SHRTCKN_PIDM, term = t2.SHRTCKN_TERM_CODE, seq = t2.SHRTCKN_SEQ_NO },
                    (t1, t2) => new { t1, t2 }
                ).Join(
                    _bn.tbl_STVGCMT,
                    ta => new { code = ta.t1.SHRTCKG_GCMT_CODE },
                    t3 => new { code = t3.STVGCMT_CODE },
                    (ta, t3) => new { ta.t1, ta.t2, t3 }
                ).Where(f =>
                    f.t1.SHRTCKG_PIDM == pidm
                    && f.t1.SHRTCKG_TERM_CODE == term
                ).Select(f => new RecoveryTest {
                    crn = f.t2.SHRTCKN_CRN,
                    code = f.t3.STVGCMT_CODE,
                    description = f.t3.STVGCMT_DESCRIPTION,
                    term = f.t1.SHRTCKG_TERM_CODE,
                    date = f.t1.SHRTCKG_ACTIVITY_DATE,
                    seq = f.t1.SHRTCKG_FINAL_GRDE_SEQ_NO,
                    score = f.t1.SHRTCKG_GRDE_CODE_INCMP_FINAL,
                    score_grade = f.t1.SHRTCKG_GRDE_CODE_FINAL
                }).ToList();
            list = list.Where(f => f.seq ==
               list.OrderByDescending(g => g.seq)
                   .FirstOrDefault(h => h.crn == f.crn).seq
                ).ToList();
            return list;
        }

        /// <summary>
        /// Listar todos los cursos llevados por un perfil de
        /// estudiante de lo periodos(TERM) determinados por la lista
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="terms"></param>
        /// <returns></returns>
        public List<Course> listByStudentTerms(AcademicProfile profile, List<string> terms)
        {
            List<Course> list = _bn.p_historia_carrera_alumno(profile.person_id, profile.program.id)
                .Where(f => terms.Contains(f.TERM))
                .OrderBy(f => f.NAMECOURSE)
                .Select<PHistoriaXCarreraAlumno, Course>(f => f)
                .ToList();

            return list;
        }

        public List<Course> listByCurrentPeriod(AcademicProfile profile) {
            ACTerm current_term = new ACTerm().getCurrent(profile.campus.id, profile.department);
            List<Course> list = enrollmentsByTerm(profile, current_term.term);
            return list == null ? new List<Course>() : list;
        }


        /// <summary>
        /// Cursos matriculados el periodo
        /// desde 201710 en adelante
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        public List<Course> enrollmentsByTerm(AcademicProfile profile, string term_id)
        {
            List<Course> list = _bn.tbl_SFRSTCR.Join(
                    _bn.tbl_SSBSECT,
                    mat => new { crn = mat.SFRSTCR_CRN, term = mat.SFRSTCR_TERM_CODE },
                    cur => new { crn = cur.SSBSECT_CRN, term = cur.SSBSECT_TERM_CODE },
                    (mat, cur) => new { mat = mat, cur = cur }
                ).Join(
                    _bn.tbl_SCRSYLN,
                    mc => new { subj = mc.cur.SSBSECT_SUBJ_CODE, numb = mc.cur.SSBSECT_CRSE_NUMB },
                    des => new { subj = des.SCRSYLN_SUBJ_CODE, numb = des.SCRSYLN_CRSE_NUMB },
                    (mc, des) => new { mat = mc.mat, cur = mc.cur, des = des }
                )
                .Join(
                    _bn.tbl_SORLCUR,
                    d => new { pidm = d.mat.SFRSTCR_PIDM },
                    r => new { pidm = (decimal)r.SORLCUR_PIDM },
                    (d, r) => new { mat = d.mat, cur = d.cur, des = d.des, r = r }
                ).Where(f =>
                    f.mat.SFRSTCR_PIDM == profile.person_id
                    && f.mat.SFRSTCR_TERM_CODE == term_id
                    && (f.mat.SFRSTCR_RSTS_CODE == "RW" || f.mat.SFRSTCR_RSTS_CODE == "RE")
                    && f.r.SORLCUR_TERM_CODE_END == null
                    && f.r.SORLCUR_LMOD_CODE == "LEARNER"
                    && (f.mat.SFRSTCR_CREDIT_HR != 0 || f.cur.SSBSECT_SUBJ_CODE == "CNUC")
                ).ToList()
                .Select(f => new Course
                {
                    id = $"{f.cur.SSBSECT_SUBJ_CODE}{f.cur.SSBSECT_CRSE_NUMB}",
                    name = f.des.SCRSYLN_LONG_COURSE_TITLE,
                    subject = f.cur.SSBSECT_SUBJ_CODE,
                    nrc = f.mat.SFRSTCR_CRN,
                    partTerm = f.cur.SSBSECT_PTRM_CODE,
                    campus = f.cur.SSBSECT_CAMP_CODE,
                    department = _learningConst.GetModalityByPartTerm(f.cur.SSBSECT_PTRM_CODE),
                    type = "o",
                    credits = (int)f.cur.SSBSECT_CREDIT_HRS,
                    score = f.mat.SFRSTCR_GRDE_CODE ?? "-",
                    is_active = "1",
                    term = f.mat.SFRSTCR_TERM_CODE,
                    TheoreticalHours = f.cur.SSBSECT_LEC_HR ?? 0,
                    PracticalHours = f.cur.SSBSECT_LAB_HR ?? 0
                }).ToList();
            return list;
        }

        /// <summary>
        /// Obtiene los cursos restantes del plan de alumno
        /// * adicionalemte puede ingorar los cursos jalados
        /// </summary>
        /// <param name="profile"> perfil del alumno</param>
        /// <param name="ignoreDisapproved">ignorar cursos jalados</param>
        /// <returns></returns>
        public List<Course> ListRemaining(AcademicProfile profile, bool ignoreDisapproved = false)
        {
            // remaining courses in plan
            List<Course> r_courses = _bn.p_catalogo_por_alumno(profile.person_id, profile.program.id)
                .Where(f => f.SUBJECT == "MANN" && f.COURSE == "CONN" && f.ELECTIVE == "0")
                .Select<PCatalogoPersonzalizado, Course>(f => f)
                .ToList();
            // diplomaeds
            List<Course> d_courses = new List<Course>();
            new Diplomaed().get(profile.department, profile.program.id,
                                profile.campus.id, profile.term_catalg)
                .ForEach(f => d_courses.AddRange(f.courses));
            // join courses list
            r_courses.AddRange(d_courses);
            //enrollments
            List<Course> e_courses = new Course().listByCurrentPeriod(profile);
            //remove enrollments
            List<Course> courses = new List<Course>();
            foreach (var item in r_courses)
            {
                int ec = e_courses.Count(f => f.id == item.id || f.equivalent == f.id);
                if (ec == 0) courses.Add(item);
            }
            //quitamos los cursos jalados
            if (ignoreDisapproved)
            {
                List<Course> t_courses = listAllByStudent(profile).Where(f => !f.is_aproved ).ToList();
                List<Course> n_courses = new List<Course>();
                foreach (var item in courses)
                {
                    int ec = t_courses.Count(f => $"{f.subject}{f.id}" == item.id || f.equivalent == f.id);
                    if (ec == 0) n_courses.Add(item);
                }
                return n_courses;
            }

            return courses;
        }
        
        /// <summary>
        /// Retar fechas sin fines de semana
        /// </summary>
        /// <param name="date1">Fecha 1</param>
        /// <param name="date2">Fecha 2</param>
        /// <returns></returns>
        public int SubstracDatesWithoutWeekends(DateTime dateEnd, DateTime dateStart)
        {
            return (Enumerable.Range(0, (dateEnd - dateStart).Days + 1)
                              .Select(d => dateStart.AddDays(d))
                              .Where(r => (int)r.DayOfWeek != 0 && (int)r.DayOfWeek != 6)
                              .Count());
        }

        #endregion methods

        #region implicite Operators
        public static implicit operator Course(DBOSPBoletaDeNotas obj)
        {
            if (obj == null) return null;
            Course course = new Course();
            course.id = obj.IDAsignatura;
            course.name = obj.NomAsignatura;
            course.subject = obj.IDSeccion;
            course.nrc = obj.IDAsignatura;
            course.credits = obj.Creditos;
            course.score = obj.Nota.ToString();
            course.term = Term.toBanner(obj.IDPerAcad);
            course.score_detail.c1 = obj.CL1.ToString();
            course.score_detail.c2 = obj.CL2.ToString();
            course.score_detail.ta1 = obj.TA1.ToString();
            course.score_detail.ta2 = obj.TA2.ToString();
            course.score_detail.ep = obj.EP.ToString();
            course.score_detail.ef = obj.EF.ToString();
            return course;
        }

        public static implicit operator Course(DIMTblAcademicProgress obj)
        {
            if (obj == null) return null;
            Course course = new Course();
            course.id = obj.course;
            course.name = obj.nameCourse;
            course.subject = obj.subject;
            course.nrc = obj.nrc;
            course.type = (obj.elective == "0") ? "O" : "E";
            int initial_cycle = 0;
            Int32.TryParse(obj.cycle, out initial_cycle);
            course.cycle = initial_cycle;
            course.credits = obj.credit;
            course.score = obj.score.ToString();
            course.term = obj.term;
            course.date = obj.dateFinalScore;
            course.is_aproved = Convert.ToBoolean(Convert.ToInt32(obj.aproved));
            return course;
        }

        public static implicit operator Course(PHistoriaXCarreraAlumno obj)
        {
            if (obj == null) return null;
            Course course = new Course();
            course.credits = (int)obj.CREDIT;
            course.cycle = int.Parse(obj.CYCLE ?? "-1");
            course.date = obj.DATEFINALSCORE;
            course.id = obj.COURSE; //$"{obj.SUBJECT}{obj.COURSE}"
            course.is_active = obj.ACTIVE;
            course.is_aproved = (obj.APROVED == "1");
            course.name = obj.NAMECOURSE;
            course.nrc = obj.NRC;
            course.score = obj.SCORE;
            course.subject = obj.SUBJECT;
            course.term = obj.TERM;
            course.type = (obj.ELECTIVE == "1")? "E":"O";
            course.equivalent = obj.RULE == null ? "" :
                obj.RULE.StartsWith("ELEC") ? $"{obj.SUBJECT}{obj.COURSE}" :
                obj.RULE;
            return course;
        }

        public static implicit operator Course(PCatalogoPersonzalizado obj)
        {
            if (obj == null) return null;
            string cycle_str = $"{obj.CYCLE:00}";
            int cycle_number = int.Parse(cycle_str);
            Course course = new Course
            {
                id = !obj.RULE.StartsWith("ELEC") ? obj.RULE : obj.SUBJECT == "MANN" ? obj.RULE : $"{obj.SUBJECT}{obj.COURSE}", // $"{f.SUBJECT}{f.COURSE}",
                name = obj.NAMECOURSE,
                subject = obj.SUBJECT,
                type = (obj.ELECTIVE != "1") ? "O" : "E",
                cycle = cycle_number,
                credits = obj.CREDITS == null ? 0 : (int)obj.CREDITS,
                is_active = "1",
            };
            return course;
        }
        #endregion implicite Operators
    }

    public class Prerequisite
    {
        public string subject { get; set; }
        public string course { get; set; }
        public string name { get; set; }
    }

    public class ScoreDetail
    {
        public string c1 { get; set; }
        public string c2 { get; set; }
        public string ta1 { get; set; }
        public string ta2 { get; set; }
        public string ep { get; set; }
        public string ef { get; set; }
    }

    public class Component
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string score { get; set; }
        public string score_old { get; set; }
        public string weight { get; set; }
        public DateTime? date { get; set; }
        public string code { get; set; }
        public string observation { get; set; }
        public List<SubComponent> sub { get; set; }
        public int? daysUploadNote { get; set; }
    }

    public class SubComponent
    {
        public int id { get; set; }
        public int gcomId { get; set; }
        public string name { get; set; }
        public string weight { get; set; }
        public string score { get; set; }
        public string observation { get; set; }
        public DateTime? date { get; set; }
        public int? daysUploadNote { get; set; }
    }

    public class RecoveryTest
    {
        public string crn { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public string term { get; set; }
        public DateTime date { get; set; }
        public string seq { get; set; }
        public string score { get; set; }
        public string score_grade { get; set; }
    }
}
