﻿using System;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.BANINST1;
using ACPermanence.DataBases.BANNER.SATURN;
using System.Collections.Generic;
using System.Linq;
using ACBusiness.AtentionCenter;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;

namespace ACBusiness.Academic
{
    public class AcademicSummary
    {
        #region Propiedades
        public int total_credits { get; set; }
        public int actual_credits { get; set; }
        public int total_cycles { get; set; }
        public int actual_cycle { get; set; }
        public float pga { get; set; }

        readonly BNContext banner = new BNContext();
        #endregion

        #region Metodos
        public AcademicSummary get(AcademicProfile profile) {
            
            AcademicSummary summary = new AcademicSummary();
            
            Curriculum curriculum = new Curriculum();
            Curriculum data_curriculum = curriculum.get(profile.department, profile.program.id, profile.campus.id,profile.term_catalg);
            ACTerm term = new ACTerm().getCurrent(profile.campus.id, profile.department);
            PCicloCreditosAlumno data_alumno;
            try {
                data_alumno = banner.p_creditos_alumno(profile.person_id, term.term, profile.program.id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                data_alumno = new PCicloCreditosAlumno { REALCREDITS = 0, ACTUALCYCLE=1 };
            }
            float pga_student = profile.getGPA();
            if (data_curriculum != null && data_alumno != null)
            {
                summary = new AcademicSummary
                {
                    total_credits = data_curriculum.credits,
                    actual_credits = data_alumno.REALCREDITS,
                    total_cycles = data_curriculum.cycles.Count,
                    actual_cycle = (int)data_alumno.ACTUALCYCLE,
                    pga = pga_student,
                 };
            }
            return summary;
        }

        #endregion
    }
}
