﻿using System.Collections.Generic;
using System.Linq;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.Academic
{
    /// <author>Arturo Bolaños</author>
    /// <date>05/07/2016</date>
    /// <summary>
    /// Datos de la Malla Curricular 
    /// segun un plan y una escuela
    /// </summary>
    public class CourseTrack
    {
        #region Properties
        public string study_plan { get; set; }
        public string academic_school { get; set; }
        public List<Course> courses { get; set; }
        #endregion Properties

        public static CourseTrack get(string study_plan, string academic_school) 
        { 
            DBOContext dbo = new DBOContext();
            List<Course> track_courses = dbo.tbl_plan_estudio_det
                .Join(
                    dbo.tbl_asignatura_acad,
                    plan => new { plan.IDDependencia, plan.IDEscuela, plan.IDEscuelaP, plan.IDPlanEstudio, plan.IDAsignatura, plan.Ciclo },
                    course => new { course.IDDependencia, course.IDEscuela, course.IDEscuelaP, course.IDPlanEstudio, course.IDAsignatura, course.Ciclo },
                    (plan, course) => new { plan, course }
                ).Where(f =>
                    f.plan.IDDependencia == "UCCI"
                    && f.course.Activo == "1"
                    && f.plan.IDEscuelaP == academic_school
                    && f.plan.IDPlanEstudio == study_plan
                ).Select(
                    f => new Course()
                    {
                        id = f.course.IDAsignatura,
                        name = f.course.NomAsignatura,
                        type = f.course.Tipo,
                        cycle = f.course.Ciclo,
                        is_active = f.course.Activo,
                        credits = f.course.Creditos,
                    }
                ).OrderBy(f => f.cycle)
                .ThenBy(f => f.name)
                .ToList<Course>();

            CourseTrack track = new CourseTrack();
            track.study_plan = study_plan;
            track.academic_school = academic_school;
            track.courses = track_courses;

            return track;
        }

    }
}
