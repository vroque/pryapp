﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using ACBusiness.AtentionCenter;
using ACPermanence.DataBases.NEXO.DIM;
using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using System.Data.Entity;

namespace ACBusiness.Academic
{
    /// <summary>
    /// Datos Academicos del alumno en un determinado periodo
    /// ** no confundir con TERM ojeto de informacion sobre periodo
    /// </summary>
    public class AcademicTerm
    {
        #region properties
        public string id { get; set; }
        public string div { get; set; }
        public string departament { get; set; }
        public string program { get; set; }
        public string levl { get; set; }
        public decimal pidm { get; set; }
        public string thirdFithTenthTop { get; set; }
        public int realCredits { get; set; }
        public decimal PGA { get; set; }
        public string termCatalog { get; set; }
        public string actualCycle { get; set; }
        #endregion properties

        #region methods
        /// <summary>
        ///  busca el ultimo periodo sin contar el periodo actual
        /// </summary>
        /// <param name="profile">Student Profile</param>
        /// <returns>return last term by not the actual term</returns>
        public static AcademicTerm getLastTerm(AcademicProfile profile)
        {
            BNContext _db = new BNContext();
            Enrollment enr_obj = new Enrollment();

            string last_term = enr_obj.getOldsTermsEnrollment(profile).OrderBy(f => f).LastOrDefault();
            AcademicTerm term = _db.p_creditos_alumno(profile.person_id, last_term, profile.program.id).FirstOrDefault();

            return term;

        }
        #endregion methods

        #region implicite converts
        public static implicit operator AcademicTerm(PCicloCreditosAlumno obj)
        {
            if (obj == null) return null;
            AcademicTerm term = new AcademicTerm();
            term.id = obj.TERM;
            term.departament = obj.DEPARTMENT;
            term.program = obj.PROGRAM;
            term.levl = obj.LEVL;
            term.pidm = obj.PIDM;
            term.realCredits = obj.REALCREDITS;
            term.termCatalog = obj.TERMCATALOG;
            term.actualCycle = obj.ACTUALCYCLE== null?"1": obj.ACTUALCYCLE.ToString();
            return term;
        }
        #endregion implicite converts


    }
}
