﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.Academic.tools
{
    /// <summary>
    /// Regla de Egresado 
    /// Numero de Creditos electivos y obligatorios para 
    /// conciderar a un alumno como egresado
    /// </summary>
    public class GraduateRule
    {
        #region atributes
        public int credits_o { get; set; }
        public int credits_e { get; set; }
        public string school_id { get; set; }
        public string study_plan_id { get; set; }
        public string academic_period_id { get; set; }
        #endregion

        /// <summary>
        /// Obtiene la regla de egresado para aplicarse segun
        /// el periodo de ingreso, la escuela academica y el plan de estudios
        /// </summary>
        /// <param name="periodo">Periodo en le que se matricula </param>
        /// <param name="school">Escuela Academica</param>
        /// <param name="subject">Plan de estudios</param>
        /// <param name="campus">Codigo de la Sede</param>
        /// <returns></returns>
        public static GraduateRule get(string periodo, string school, string study_plan, string sede_id)
        {
            DBOContext db = new DBOContext();
            GraduateRule regla =  db.tbl_reglas_egresado.FirstOrDefault(f => 
                f.IDPerAcad == periodo
                && f.IDEscuela == school
                && f.IDPlanEstudio == study_plan
                && f.IDSede == sede_id
                );
            return regla;

        }

        #region implicits
        public static implicit operator GraduateRule(DBOTblReglasEgresado obj)
        {
            if (obj == null) return null;
            GraduateRule regla = new GraduateRule();
            regla.credits_o = obj.CreditosO;
            regla.credits_e = obj.CreditosE;
            regla.school_id = obj.IDEscuela;
            regla.study_plan_id = obj.IDPlanEstudio;
            regla.academic_period_id = obj.IDPerAcad;
            return regla;
        }
        #endregion
    }
}
