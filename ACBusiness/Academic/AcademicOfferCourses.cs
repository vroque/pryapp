﻿using System;
using ACPermanence.Contexts.BANNER;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.DataBases.BANNER.SATURN;
using Newtonsoft.Json;
using ACBusiness.AtentionCenter;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using ACBusiness.Extra;
using ACPermanence.Contexts.NEXO;

namespace ACBusiness.Academic
{
    public class AcademicOfferCourses
    {
        public string term { get; set; }
        public string campus { get; set; }
        public string departament { get; set; }
        public string partterm { get; set; }
        public string program { get; set; }
        public string nrc { get; set; }
        public string subject { get; set; }
        public string course { get; set; }
        public int? credits { get; set; }
        public int nroligas { get; set; }
        public string nrcpadre { get; set; }
        public string codliga { get; set; }
        public int? maxvacancies { get; set; }
        public int? vacanciesavailable { get; set; }
        public string idseccion { get; set; }
        public string namecourse { get; set; }
        public int? pidmteacher { get; set; }
        public string idteacher { get; set; }
        public string teacher { get; set; }

        readonly BNContext _banner = new BNContext();
        readonly NEXOContext nexoContext = new NEXOContext();

        public List<AcademicOfferCourses> getOfferCourses(AcademicProfile profile, string term, string subject, string course)
        {
            //Ejecutamos el procedimiento que actualiza los cursos offertados para cada curso
            List<P_NRCSPADREPROY> nullabled = _banner.p_actualizaproy(profile.person_id, profile.program.id, term, subject, course, profile.campus.id, profile.department).ToList();

            List<AcademicOfferCourses> listSubjects = _banner.p_nrcspadreproy(profile.person_id, profile.program.id, term, subject, course)
                                                        .OrderByDescending(f=>f.vacanciesavailable).ToList()
                                                        .Select<P_NRCSPADREPROY, AcademicOfferCourses>(f => f).ToList();

            return listSubjects;
        }

        public List<AcademicOfferCourses> getOfferCoursesByRestriction(AcademicProfile profile, string term, string subject, string course)
        {
            // asignaturas disponibles id=1
            DisplayBlock display = nexoContext.cau_display_block.FirstOrDefault(f => f.id == 1);
            var listSubjects = new List<AcademicOfferCourses>();
            if (display != null)
            {
                if (display.active)
                {
                    DateTime today = DateTime.Now;
                    if (today >= display.startFech && today <= display.endFech)
                    {
                        if (today.TimeOfDay >= display.startFech.TimeOfDay && today.TimeOfDay <= display.endFech.TimeOfDay)
                        {
                            listSubjects = getOfferCourses(profile, term, subject, course);
                            return listSubjects;
                        }
                        else
                        {
                            return listSubjects;
                        }
                    }
                }
                else
                {
                    return listSubjects;
                }
            }
            return listSubjects;
        }


        public List<AcademicOfferCourses> getOfferCoursesChildren(AcademicProfile profile, string nrcpadre, string term, string subject, string course, string codliga)
        {
            List<AcademicOfferCourses> listSubjects = _banner.p_nrcshijoproy(profile.person_id, profile.program.id, term, subject, course, codliga)
                                                        .OrderByDescending(f => f.vacanciesavailable).ToList()
                                                        .Select<P_NRCSPADREPROY, AcademicOfferCourses>(f => f).ToList();
            return listSubjects;
        }

        public List<AcademicOfferCourses> getOfferCoursesChildrenByRestriction(AcademicProfile profile, string nrcpadre, string term, string subject, string course, string codliga)
        {
            // asignaturas disponibles id=1
            DisplayBlock display = nexoContext.cau_display_block.FirstOrDefault(f => f.id == 1);
            var listSubjects = new List<AcademicOfferCourses>();
            if (display != null)
            {
                if (display.active)
                {
                    DateTime today = DateTime.Now;
                    if (today >= display.startFech && today <= display.endFech)
                    {
                        if (today.TimeOfDay >= display.startFech.TimeOfDay && today.TimeOfDay <= display.endFech.TimeOfDay)
                        {
                            listSubjects = getOfferCoursesChildren(profile, nrcpadre, term, subject, course, codliga);
                            return listSubjects;
                        }
                        else
                        {
                            return listSubjects;
                        }
                    }
                }
                else
                {
                    return listSubjects;
                }
            }
            return listSubjects;
        }

        #region implicite operators
        public static implicit operator AcademicOfferCourses(P_NRCSPADREPROY query)
        {
            AcademicOfferCourses list = new AcademicOfferCourses();
            list.term = query.term;
            list.campus = query.campus;
            list.departament = query.departament;
            list.partterm = query.partterm;
            list.program = query.program;
            list.nrc = query.nrc;
            list.nroligas = query.nroligas;
            list.nrcpadre = query.nrcpadre;
            list.subject = query.subject;
            list.course= query.course;
            list.credits = query.credits;
            list.codliga = query.codliga;
            list.maxvacancies = query.maxvacancies;
            list.vacanciesavailable = query.vacanciesavailable;
            list.idseccion = query.idseccion;
            list.namecourse = query.namecourse;
            list.pidmteacher = query.pidmteacher;
            list.idteacher = query.idteacher;
            list.teacher = query.teacher;
            return list;
        }
        #endregion implicite operators
    }
}
