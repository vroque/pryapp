﻿using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.BANNER;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using System.Collections.Generic;
using System.Linq;
using ACPermanence.Contexts.BDUCCI;
using ACBusiness.Institutional;
using ACPermanence.DataBases.BANNER.BANINST1;
using System;
using System.Data.Entity;

namespace ACBusiness.Academic
{
    /// <summary>
    /// Matriculas
    /// objeto para el funcionamiento de matriculas
    /// </summary>
    public class Enrollment
    {
        #region properties
        public string divs { get; set; }
        public string campus { get; set; }
        public string levl { get; set; }
        public string program { get; set; }
        public string term { get; set; }
        public decimal pidm { get; set; }
        public string department { get; set; }
        readonly BNContext _bn = new BNContext();
        readonly DBOContext _apec = new DBOContext();
        readonly NEXOContext _nexo = new NEXOContext();
        #endregion properties

        #region methods

        /// <summary>
        /// Obtiene todos los períodos académicos UC en los que existieron matrícula
        /// </summary>
        /// <returns></returns>
        public List<string> getAllEnrollmentPeriods()
        {
            // Lista de períodos de matrícula BANNER
            var nexo_all_enrollment_periods = _bn.tbl_SZVCMPP
                .Select(s => s.SORLCUR_TERM_CODE_ADMIT)
                .Distinct()
                .ToList();

            // Lista de períodos de matrícula BDUCCI
            var bducci_all_enrollment_periods = _apec.tbl_matricula
                .Select(s => s.IDPerAcad)
                .Distinct()
                .ToList();

            List<string> nexo_enrollment_periods = new List<string>();
            List<string> bducci_enrollment_periods = new List<string>();

            foreach (var period in nexo_all_enrollment_periods)
            {
                if (int.Parse(period) >= 201710)
                {
                    nexo_enrollment_periods.Add(period);
                }
            }
            nexo_enrollment_periods = nexo_enrollment_periods
                .ToList();

            foreach (var period in bducci_all_enrollment_periods)
            {
                if (period == "201700" || int.Parse(period.Substring(0, 4)) <= 2016)
                {
                    bducci_enrollment_periods.Add(period);
                }
            }
            bducci_enrollment_periods = bducci_enrollment_periods
                .ToList();

            return nexo_enrollment_periods
                .Union(bducci_enrollment_periods)
                .OrderByDescending(f => f)
                .ToList();
        }
        
       
        /// <summary>
        /// Lista todos los periodos en los que se a matriculado el alumno
        /// </summary>
        /// <returns></returns>
        public List<string> getAllTermsEnrollment(AcademicProfile profile) {

            List<string> bn_enrollment = _bn.tbl_SZVCMPP.Where(f =>
                f.SFRSTCR_PIDM == profile.person_id
                && f.SORLCUR_PROGRAM == profile.program.id
            // ignoramos department
            ).Select(f => f.SSBSECT_TERM_CODE)
            .ToList();

            List<string> apec_enrollment = _apec.tbl_matricula_doc.Join( 
                    _apec.view_alumno_basico,
                    mat => mat.IDAlumno,
                    alu => alu.IDAlumno,
                    (mat, alu) => new { mat, alu}
                ).Where(f =>
                    f.alu.IDPersonaN == profile.person_id
                    && f.mat.IDEscuela == profile.program.id
                    // ignoramos department
                    && f.mat.IDtipoMatricula == "MATRI"
                ).Select(f =>f.mat.IDPerAcad)
                .ToList();

            return bn_enrollment
                .Union(apec_enrollment.Select(f => Term.toBanner(f, profile.department)))
                .OrderByDescending(f => f)
                .ToList();
        }

        /// <summary>
        /// <para>Períodos en los que un estudiante UC estuvo matriculado.</para>
        /// Retorna un diccionario de períodos con:
        /// clave = período en formato mixto (formato banner y formato apec) y
        /// valor = período en formato banner
        /// p.e.:
        /// <code>
        /// {
        ///   {"201710", "201710"},
        ///   {"2016-1", "201610"}
        /// }
        /// </code>
        /// </summary>
        /// <param name="academicProfile"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetAllTermsEnrolled(AcademicProfile academicProfile)
        {
            var enrolledBanner = _bn.tbl_SZVCMPP
                .Where(f => f.SFRSTCR_PIDM == academicProfile.person_id &&
                            // ignoramos department
                            f.SORLCUR_PROGRAM == academicProfile.program.id)
                .Select(f => f.SSBSECT_TERM_CODE).Distinct().ToList();

            var enrolledApec = _apec.tbl_matricula_doc
                .Join(_apec.view_alumno_basico, mat => mat.IDAlumno,
                    alu => alu.IDAlumno,
                    (mat, alu) => new {mat, alu}
                ).Where(f => f.alu.IDPersonaN == academicProfile.person_id &&
                             // ignoramos department
                             f.mat.IDEscuela == academicProfile.program.id &&
                             f.mat.IDtipoMatricula == "MATRI").Select(f => f.mat.IDPerAcad).Distinct().ToList();

            var termMix = enrolledBanner.Union(enrolledApec).OrderByDescending(o => o).ToList();
            var termBanner = enrolledBanner
                .Union(enrolledApec.Select(s => Term.toBanner(s, academicProfile.department)))
                .OrderByDescending(o => o).ToList();

            var enrolled = termMix.Zip(termBanner, (k, v) => new {k, v}).ToDictionary(x => x.k, x => x.v);

            return enrolled;
        }

        /// <summary>
        /// muestra todos los periodos concluidos que ah terminado el alumno
        /// </summary>
        /// <returns></returns>
        public List<string> getOldsTermsEnrollment(AcademicProfile profile)
        {
            ACTerm term_obj = new ACTerm();
            ACTerm term_current = term_obj.getCurrent(profile.campus.id, profile.department);

            List<string> old_terms = getAllTermsEnrollment(profile)
                .Where(f => f != term_current.term)
                .ToList();

            return old_terms;
        }

        /// <summary>
        /// El ultimo periodo matriculado sin contar el actual
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="term_limit"></param>
        /// <returns></returns>
        public ACTerm getLastTermEnrollment(AcademicProfile profile, string term_limit = null)
        {
            List<string> terms = this.getOldsTermsEnrollment(profile);
            string last_term = terms.OrderBy(f => f).LastOrDefault();
            ACTerm t = new ACTerm(last_term, profile.campus.id, profile.department);
            return t;
        }

        /// <summary>
        /// Busca matriculas en el periodo actual
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        public bool hasEnrollmentInThisTerm(AcademicProfile profile)
        {
            ACTerm term_current = new ACTerm().getCurrent(profile.campus.id, profile.department);
            SZVCMPP term_object = _bn.tbl_SZVCMPP
                .FirstOrDefault(f => 
                    f.SSBSECT_TERM_CODE == term_current.term
                    && f.SFRSTCR_PIDM == profile.person_id
                    && f.SORLCUR_PROGRAM == profile.program.id
                );
            if (term_object != null)
                return true;
            return false;
        }

        public bool hasEnrollmentInTermRangeInitClassEndTerm(AcademicProfile profile, int addDaysInit, int addDaysEnd) {
            ACTerm term_current = _nexo.cau_tem_documents.Where(f =>
                    f.campus == profile.campus.id &&
                    f.department == profile.department &&
                    DbFunctions.AddDays(f.startClassesDate, addDaysInit) <= DateTime.Today &&
                    DbFunctions.AddDays(f.endDate, addDaysEnd) >= DateTime.Today
                ).FirstOrDefault();

            if (term_current == null)
            {
                return false;
            }
            else {
                SZVCMPP term_object = _bn.tbl_SZVCMPP
                .FirstOrDefault(f =>
                    f.SSBSECT_TERM_CODE == term_current.term
                    && f.SFRSTCR_PIDM == profile.person_id
                    && f.SORLCUR_PROGRAM == profile.program.id
                );
                if (term_object != null)
                    return true;
            }

            
            return false;
        }

        /// <summary>
        /// Retorna una lista de los Periodos(Ciclos) en los que el estudiante
        /// haya aprovado por lo menos un curso
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        public List<string> getAprovedPeriods(AcademicProfile profile)
        {
            List<PHistoriaXCarreraAlumno> list = _bn.p_historia_carrera_alumno(
                profile.person_id, profile.program.id).ToList();

            List<string> period_taken = list
                .Where(f => f.APROVED == "1")
                .Select(f => int.Parse(f.CYCLE))
                .OrderBy(f => f)
                .Distinct()
                .Select(f => f.ToString())
                .ToList();
            return period_taken;
        }

        #endregion methods

        #region implicite operators
        public static implicit operator Enrollment(PHistoriaXCarreraAlumno temp)
        {
            if (temp == null) return null;
            Enrollment obj = new Enrollment();
            obj.campus = temp.CAMPUS;
            obj.department = temp.DEPARTAMENT;
            obj.divs = temp.DIVS;
            obj.levl = temp.LEVL;
            obj.pidm = temp.PIDM;
            obj.program = temp.PROGRAM;
            obj.term = temp.TERM;
            return obj;
        }
        #endregion implicite operators
    }
}

