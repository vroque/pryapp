﻿using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.SATURN;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ACBusiness.Academic
{
    public class CloseEnrollment
    {
        #region Properties
        public string term { get; set; }
        public int pidm { get; set; }
        public string rgrpCode { get; set; }
        public string user { get; set; }
        public DateTime activityDate { get; set; }
        public int surrogateId { get; set; }
        public int version { get; set; }
        public string userId { get; set; }
        public string dataOrigin { get; set; }
        public string vpdiCode { get; set; }
        #endregion Properties

        public CloseEnrollment closeEnrollmentByStudent(decimal pidm, string term)
        {
            BNContext bnContext = new BNContext();
            CloseEnrollment data = bnContext.tbl_SFBRGRP.FirstOrDefault(f => f.SFBRGRP_TERM_CODE == term && f.SFBRGRP_PIDM==pidm);

            return data;
        }

        #region implicite Operators
        public static implicit operator CloseEnrollment(SFBRGRP obj)
        {
            if (obj == null) return null;
            CloseEnrollment data = new CloseEnrollment();
            data.term = obj.SFBRGRP_TERM_CODE;
            data.pidm = obj.SFBRGRP_PIDM;
            data.rgrpCode = obj.SFBRGRP_RGRP_CODE;
            data.user = obj.SFBRGRP_USER;
            data.activityDate = obj.SFBRGRP_ACTIVITY_DATE;
            data.surrogateId = obj.SFBRGRP_SURROGATE_ID;
            data.version = obj.SFBRGRP_VERSION;
            data.userId = obj.SFBRGRP_USER_ID;
            data.dataOrigin = obj.SFBRGRP_DATA_ORIGIN;
            data.vpdiCode = obj.SFBRGRP_VPDI_CODE;
            return data;
        }
        #endregion implicite Operators
    }
}
