﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.DIM;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Institutional
{
    /// <summary>
    /// 
    /// </summary>
    public class Department
    {
        #region properties
        public string id { get; set; }
        public string name { get; set; }
        /// <summary>
        /// Indicador de vigencia del registro (1 y 0)
        /// </summary>
        public string campana { get; set; }
        #endregion properties

        #region methods
        /// <summary>
        /// Obtiene la lista de modalidades activas
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<Department>> GetAllAsync()
        {
            using (var nexoContext = new NEXOContext())
            {
                var data = await nexoContext.dim_departaments.Where(x => x.campana == "1").ToListAsync();
                return data.Select<DIMTblDepartament, Department>(x => x).ToList();
            }
        }

        /// <summary>
        /// Obtiene el department por su ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Department get(string id) {
            NEXOContext _db = new NEXOContext();

            Department d = _db.dim_departaments.FirstOrDefault(f =>
               f.departament == id
            );
            return d;
        }
        #endregion methods

        #region implicite operatos 
        public static implicit operator Department(DIMTblDepartament obj)
        {
            if (obj == null) return null;
            Department d = new Department();
            d.id = obj.departament;
            d.name = obj.departamentDescription;
            d.campana = obj.campana;
            return d;
        }

        public static string toAPEC(string id)
        {
            switch(id) {
                case "UREG":
                    return "ADM";
                case "UPGT":
                    return "ADG";
                case "UVIR":
                    return "ADV";
                default:
                    return id;
            }
        }

        public static string toBanner(string id)
        {
            switch (id)
            {
                case "ADM":
                    return "UREG";
                case "ADG":
                    return "UPGT";
                case "ADV":
                    return "UVIR";
                default:
                    return id;
            }
        }
        #endregion implicite operatos 
    }
}
