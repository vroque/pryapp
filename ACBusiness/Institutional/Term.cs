﻿using _acad = ACTools.Constants.AcademicConstants;

namespace ACBusiness.Institutional
{
    public class Term
    {
        /// <summary>
        /// Convierte período académico a notación utilizada en APEC
        /// p.e.: 201710 se convierte a 2017-1
        /// </summary>
        /// <param name="term"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        public static string toAPEC(string term, string department = null)
        {
            if (term.Length != 6) return null;

            if (term.Substring(4, 1) == "-") return term;
            
            var apec_term = term.Substring(0, 4) + "-" + term.Substring(4, 1);
            if ((department == _acad.DEPA_PGT || department == _acad.DEPA_VIR) && term.Substring(4, 2) == "00")
            {
                var year = int.Parse(term.Substring(0, 4)) - 1;
                apec_term = $"{year}-3";
            }
            return apec_term;
        }

        /// <summary>
        /// Convierte período académico a notación utilizada en BANNER
        /// p.e.: 2017-1 se convierte a 201710
        /// </summary>
        /// <param name="term"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        public static string toBanner(string term, string department = null)
        {
            if (term.Length != 6) return null;

            if (term.Substring(4, 1) != "-") return term;
            
            var banner_term = $"{term.Substring(0, 4)}{term.Substring(5, 1)}0";
            if ((department == _acad.DEPA_PGT || department == _acad.DEPA_VIR) && term.Substring(4, 2) == "-3")
            {
                var year = int.Parse(term.Substring(0, 4)) + 1;
                banner_term = $"{year}00";
            }
            return banner_term;
        }
    }
}