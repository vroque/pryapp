﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.DIM;
using System.Data.Entity;

namespace ACBusiness.Institutional
{
    public class Campus
    {
        #region properties
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        #endregion properties

        #region static methods
        public static Campus get(string id)
        {
            NEXOContext db = new NEXOContext();
            Campus campus = db.dim_campus.FirstOrDefault(f => f.campus == id);
            return campus;
        }

        public static Campus getByAPECCode(string id)
        {
            NEXOContext db = new NEXOContext();
            Campus campus = db.dim_campus.FirstOrDefault(f => f.codePlace == id);
            return campus;
        }

        /// <summary>
        /// Retorna la lista de campus
        /// </summary>
        /// <returns></returns>
        public static List<Campus> query()
        {
            NEXOContext db = new NEXOContext();
            List<Campus> campuses = db.dim_campus
                .Where(f => f.campus != "999")
                .ToList()
                .Select<DIMTblCampus, Campus>(f => f)
                .ToList();
            return campuses;
        }

        public static List<Campus> getByListID(List<string> list)
        {
            NEXOContext db = new NEXOContext();
            List<DIMTblCampus> l1 = db.dim_campus
                .Where( f => list.Contains(f.campus) ).ToList();

            List<Campus> campuses =  l1
                .Select<DIMTblCampus, Campus>(f => f)
                .ToList();
            return campuses;
        }

        #endregion static methods

        #region Implicit operator
        public static implicit operator Campus(DIMTblCampus model)
        {
            if (model == null) return null;
            return new Campus
            {
                id = model.campus,
                code = model.codePlace,
                name = model.campusDescription
            };
        }
        #endregion Implicit operator

    }
}

