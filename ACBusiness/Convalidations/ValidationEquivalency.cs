﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;

namespace ACBusiness.Convalidations
{
    public class ValidationEquivalency
    {
        #region properties
        // public string academic_school_id { get; set; }      
        // public string required { get; set; }
        public string school_id { get; set; }
        public string study_plan_id { get; set; }
        public string destination_study_plan_id { get; set; }
        //public Dictionary<string,string> courses { get; set; }
        public List<Equivalency> courses { get; set; }
        
        #endregion properties

        public static ValidationEquivalency get(string school_id, string study_plan_id, string destination_study_plan_id)
        {
            DBOContext dbo = new DBOContext();
            List<Equivalency> courses = dbo.tbl_asignatura_acad_u_conva
                .Where(f =>
                    f.IDPlanEstudio == study_plan_id
                    && f.IDPlanEstudioR == destination_study_plan_id
                    && f.IDEscuelaP == school_id
                    && f.IDEscuelaP2 == school_id
                    // && f.IDAsignatura != "NO_EXI"
                    // && f.IDAsignaturaR != "NO_EXI"
                ).Select(
                    x => new Equivalency { origin_course = x.IDAsignatura, destination_course = x.IDAsignaturaR }
                ).ToList();
                

            ValidationEquivalency table = new ValidationEquivalency();
            table.school_id = school_id;
            table.study_plan_id = study_plan_id;
            table.destination_study_plan_id = destination_study_plan_id;
            table.courses = courses;

            return table;
        }

    }
    public class Equivalency
    {
        public string  origin_course { get; set; }
        public string destination_course { get; set; }
    }
}
