﻿using ACPermanence.Contexts.BANNER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACBusiness.Academic;

namespace ACBusiness.Convalidations
{
    /// <summary>
    /// Convalidaciones
    /// </summary>
    public class Transfer
    {
        #region properties
        public decimal person_id { get; set; }
        public string levl { get; set; }
        public string term { get; set; }
        public int seq { get; set; }
        public int tseq { get; set; }
        public DateTime? date { get; set; }
        public string institution_id { get; set; }
        public string institution_name { get; set; }
        public string program { get; set; }
        public List<TranferCourse> courses { get; set; }

        private BNContext _bn = new BNContext();
        #endregion properties

        #region methods
        /// <summary>
        /// Obtener todas las convalidaciones de un alumno
        /// </summary>
        /// <param name="person_id">Codigo de persona</param>
        /// <param name="term">Periodo(TERM ) ex 201710</param>
        /// <param name="has_coures">[opcional] Incluir informacion de cursos</param>
        /// <returns>Lista de convalidaciones</returns>
        public List<Transfer> list(decimal person_id, bool with_coures = false)
        {
            List<Transfer> list = _bn.tbl_SHRTRAM.Join(
                    _bn.tbl_SHRTRIT,
                    conv => new { pidm = conv.SHRTRAM_PIDM, seq = conv.SHRTRAM_TRIT_SEQ_NO },
                    cce => new { pidm = cce.SHRTRIT_PIDM, seq = cce.SHRTRIT_SEQ_NO },
                    (conv, cce) => new { conv, cce }
                ).Join(
                    _bn.tbl_STVSBGI,
                    ccc => ccc.cce.SHRTRIT_SBGI_CODE,
                    cen => cen.STVSBGI_CODE,
                    (ccc, cen) => new { ccc.conv, cen }
                ).Where( f => 
                    f.conv.SHRTRAM_PIDM == person_id
                ).Select(f => new Transfer {
                    person_id = f.conv.SHRTRAM_PIDM,
                    levl = f.conv.SHRTRAM_LEVL_CODE,
                    term = f.conv.SHRTRAM_TERM_CODE_ENTERED,
                    seq = f.conv.SHRTRAM_SEQ_NO,
                    tseq = f.conv.SHRTRAM_TRIT_SEQ_NO,
                    date = f.conv.SHRTRAM_ACCEPTANCE_DATE,
                    institution_id = f.cen.STVSBGI_CODE,
                    institution_name = f.cen.STVSBGI_DESC
                }).ToList();

            if (with_coures)
                foreach (Transfer item in list)
                    item.courses = getTransferCourses(person_id, item.term);

            return list;
        }

        /// <summary>
        /// Obtener todas las convalidaciones de un alumno en un determinado periodo
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="term"></param>
        /// <param name="with_coures"></param>
        /// <returns></returns>
        public List<Transfer> listByTerm(decimal person_id, string term, bool with_coures = false)
        {
            term = fixTerm(term);
            List<Transfer> list = _bn.tbl_SHRTRAM.Join(
                    _bn.tbl_SHRTRIT,
                    conv => new { pidm = conv.SHRTRAM_PIDM, seq = conv.SHRTRAM_TRIT_SEQ_NO },
                    cce => new { pidm = cce.SHRTRIT_PIDM, seq = cce.SHRTRIT_SEQ_NO },
                    (conv, cce) => new { conv, cce }
                ).Join(
                    _bn.tbl_STVSBGI,
                    ccc => ccc.cce.SHRTRIT_SBGI_CODE,
                    cen => cen.STVSBGI_CODE,
                    (ccc, cen) => new { ccc.conv, cen }
                ).Where(f =>
                   f.conv.SHRTRAM_PIDM == person_id &&
                   f.conv.SHRTRAM_TERM_CODE_ENTERED == term
                ).Select(f => new Transfer
                {
                    person_id = f.conv.SHRTRAM_PIDM,
                    levl = f.conv.SHRTRAM_LEVL_CODE,
                    term = f.conv.SHRTRAM_TERM_CODE_ENTERED,
                    seq = f.conv.SHRTRAM_SEQ_NO,
                    tseq = f.conv.SHRTRAM_TRIT_SEQ_NO,
                    date = f.conv.SHRTRAM_ACCEPTANCE_DATE,
                    institution_id = f.cen.STVSBGI_CODE,
                    institution_name = f.cen.STVSBGI_DESC
                }).ToList();

            if (with_coures)
                foreach (Transfer item in list)
                    item.courses = getTransferCourses(person_id, item.term);

            return list;
        }
        /// <summary>
        /// Obtiene una convalidacion en base al periodo en el que realizo
        /// la convalidación
        /// </summary>
        /// <param name="pidm"></param>
        /// <param name="term"></param>
        /// <param name="term"></param>
        /// <param name="with_coures">[opcional] Incluir informacion de cursos</param>
        /// <returns></returns>
        public Transfer get(decimal person_id, string term, int tseq = 1, int seq = 1, bool with_coures = false)
        {
            term = fixTerm(term);
            Transfer transfer = _bn.tbl_SHRTRAM.Join(
                    _bn.tbl_SHRTRIT,
                    conv => new { pidm = conv.SHRTRAM_PIDM, tseq = conv.SHRTRAM_TRIT_SEQ_NO },
                    cce => new { pidm = cce.SHRTRIT_PIDM, tseq = cce.SHRTRIT_SEQ_NO },
                    (conv, cce) => new { conv, cce }
                ).Join(
                    _bn.tbl_STVSBGI,
                    ccc => ccc.cce.SHRTRIT_SBGI_CODE,
                    cen => cen.STVSBGI_CODE,
                    (ccc, cen) => new { ccc.conv, cen }
                ).Where(f =>
                   f.conv.SHRTRAM_PIDM == person_id
                   && f.conv.SHRTRAM_TERM_CODE_ENTERED == term
                   && f.conv.SHRTRAM_SEQ_NO == seq
                   && f.conv.SHRTRAM_TRIT_SEQ_NO == tseq
                ).Select(f => new Transfer
                {
                    person_id = f.conv.SHRTRAM_PIDM,
                    levl = f.conv.SHRTRAM_LEVL_CODE,
                    term = f.conv.SHRTRAM_TERM_CODE_ENTERED,
                    seq = f.conv.SHRTRAM_SEQ_NO,
                    tseq = f.conv.SHRTRAM_TRIT_SEQ_NO,
                    date = f.conv.SHRTRAM_ACCEPTANCE_DATE,
                    institution_id = f.cen.STVSBGI_CODE,
                    institution_name = f.cen.STVSBGI_DESC
                }).ToList()
                .FirstOrDefault();
            if (with_coures)
                transfer.courses = getTransferCourses(person_id, term, seq:seq, tseq:tseq);
            return transfer;
        }

        /// <summary>
        /// Obtiene la listas de cursos convalidados de una convalidacion
        /// </summary>
        /// <param name="person_id"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        private List<TranferCourse> getTransferCourses(decimal person_id, string term, int seq=1, int tseq=1)
        {
            //INNER JOIN SCRSYLN DES
            //    ON cur.SSBSECT_SUBJ_CODE = des.SCRSYLN_SUBJ_CODE
            //    AND cur.SSBSECT_CRSE_NUMB = des.SCRSYLN_CRSE_NUMB
            //SHRTRCE_TRIT_SEQ_NO, SHRTRCE_TRAM_SEQ_NO
            List<TranferCourse> list = _bn.tbl_SHRTRCR
                .Join(_bn.tbl_SHRTRCE,
                    proc => new
                    {
                        seq_tr = proc.SHRTRCR_TRIT_SEQ_NO, seq = proc.SHRTRCR_SEQ_NO, pidm = proc.SHRTRCR_PIDM
                    },
                    uc => new {seq_tr = uc.SHRTRCE_TRIT_SEQ_NO, seq = uc.SHRTRCE_TRCR_SEQ_NO, pidm = uc.SHRTRCE_PIDM},
                    (proc, uc) => new {proc, uc})
                .Where(f => f.uc.SHRTRCE_PIDM == person_id
                    && f.uc.SHRTRCE_TERM_CODE_EFF == term
                    && f.uc.SHRTRCE_TRIT_SEQ_NO == tseq
                    && f.proc.SHRTRCR_TRIT_SEQ_NO == tseq
                    && f.uc.SHRTRCE_TRAM_SEQ_NO == seq
                    && f.proc.SHRTRCR_TRAM_SEQ_NO == seq)
                .ToList()
                .Select(f => new TranferCourse
                {
                    proc_seq_no = f.proc.SHRTRCR_SEQ_NO,
                    proc_course_name = f.proc.SHRTRCR_TRANS_COURSE_NAME,
                    proc_course_credits = f.proc.SHRTRCR_TRANS_CREDIT_HOURS == null
                        ? -1
                        : f.proc.SHRTRCR_TRANS_CREDIT_HOURS ?? -1,
                    proc_course_score = f.proc.SHRTRCR_TRANS_GRADE,
                    uc_seq_no = f.uc.SHRTRCE_TRCR_SEQ_NO,
                    uc_course_subj = f.uc.SHRTRCE_SUBJ_CODE,
                    uc_course_number = f.uc.SHRTRCE_CRSE_NUMB,
                    uc_course_name = Course.GetCourseNameAsync(f.uc.SHRTRCE_SUBJ_CODE, f.uc.SHRTRCE_CRSE_NUMB).Result,
                    uc_course_credits = (int) f.uc.SHRTRCE_CREDIT_HOURS,
                    uc_course_score = f.uc.SHRTRCE_GRDE_CODE,
                    SHRTRCE_COUNT_IN_GPA_IND = f.uc.SHRTRCE_COUNT_IN_GPA_IND
                })
                .OrderBy(f => f.uc_seq_no)
                .ToList();
            // Contar repeticiones
            int sw = 0; // count switcher
            foreach (var item in list)
            {
                if (sw == 0)
                {
                    item.uc_repeat = list
                        .Count(f => f.uc_course_subj == item.uc_course_subj &&
                                    f.uc_course_number == item.uc_course_number);
                    sw = item.uc_repeat;
                }
                else
                {
                    item.uc_repeat = 0;
                }
                sw--;
            }

            /**
             * Generalmente, muchas asignaturas de la universidad origen convalidan con una asignatura de la UC,
             * cuando se encuentra este caso, el usuario marca, desde banner, una asignatura para que esta sea considerada
             * como la asignatura que convalida y así poder considerar la nota de dicha asignatura.
             *
             * Recorremos la lista de cursos para, en caso de haber varias asignaturas que convalidan a un curso UC,
             * seleccionar la nota del curso marcado SHRTRCE_COUNT_IN_GPA_IND="Y"
             * si no existe curso marcado, seleccionamos la mayor nota
             */
            foreach (var course in list)
            {

                // buscamos repeticiones
                List<TranferCourse> courseList = list
                    .Where(w => w.uc_course_subj == course.uc_course_subj &&
                                w.uc_course_number == course.uc_course_number)
                    .ToList();
                /**
                 * si hay repeticiones, seleccionamos la nota de SHRTRCE_COUNT_IN_GPA_IND == "Y"
                 * si no existe SHRTRCE_COUNT_IN_GPA_IND == "Y", seleccionamos la mayor nota
                 */
                if (courseList.Count <= 1) continue;

                var marked = courseList.FirstOrDefault(w => w.SHRTRCE_COUNT_IN_GPA_IND == "Y");
                if (marked == null)
                {
                    var maxScore = courseList.Select(s => s.proc_course_score).Max();
                    marked = courseList.FirstOrDefault(w => w.proc_course_score == maxScore);
                }

                course.uc_course_score = marked?.uc_course_score;
            }
            
            return list;
        }

        private string fixTerm(string term)
        {
            if (term.Substring(4, 2) == "00")
                term = $"{term.Substring(0,4)}10";
            return term;
        }
        #endregion methods
    }

    public class TranferCourse
    {
        public int proc_seq_no { get; set; }
        public string proc_course_name { get; set; }
        public float proc_course_credits { get; set; }
        public string proc_course_score { get; set; }

        public int uc_seq_no { get; set; }
        public string uc_course_subj { get; set; }
        public string uc_course_number { get; set; }
        public string uc_course_name { get; set; }
        public int uc_course_credits { get; set; }
        public string uc_course_score { get; set; }
        public string SHRTRCE_COUNT_IN_GPA_IND { get; set; }
        public int uc_repeat { get; set; }
    }
}
