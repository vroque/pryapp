﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.COU;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACBusiness.Academic.tools;

namespace ACBusiness.Convalidations
{
    /// <summary>
    /// Resumen de Creditos para validacion
    /// </summary>
    public class ValidationSummary
    {
        #region properties
        public int old_mandatory { get; set; }
        public int old_elective { get; set; }
        public int new_mandatory { get; set; }
        public int new_elective { get; set; }
        public int plan_mandatory { get; set; }
        public int plan_elective { get; set; }
        #endregion properties

        #region methods
        public static ValidationSummary get(string student_id, string sede_id, string period_id, string plan) {
            DBOContext db = new DBOContext();
            COUTblValidacionDocumento doc = db.cou_tbl_validacion_documento
                .FirstOrDefault(f => f.IDAlumno == student_id);
            List<COUTblValidacionAsignatura> asig = db.cou_tbl_validacion_asignatura
                .Where(f => f.IDDocumentoValidacion == doc.ID).ToList();
            List<COUTblValidacionMatricula> matr = db.cou_tbl_validacion_matricula
                .Where(f => f.IDDocumentoValidacion == doc.ID).ToList();
            
            //antes hay que obtener el plan y el periodo de su postulación
            DBOTblPostulante pos = db.tbl_postulante.Where(f =>
                    f.IDAlumno == student_id
                    && f.Ingresante == "1"
                    && f.Renuncia == "0")
                    .OrderByDescending(f => f.IDPerAcad)
                    .FirstOrDefault();//.ToList();
            ////obtener regla de egresado
            GraduateRule rule = GraduateRule.get(period_id, pos.IDEscuela, plan, sede_id);
            ValidationSummary sumary = new ValidationSummary();
            sumary.old_mandatory = 0;
            sumary.old_elective = 0;
            sumary.new_mandatory = 0;
            sumary.new_elective = 0;
            sumary.plan_mandatory = rule.credits_o;
            sumary.plan_elective = rule.credits_e;

            foreach (COUTblValidacionAsignatura item in asig)
            {
                if (item.Tipo == "O")
                    sumary.old_mandatory += item.Creditos;
                else if (item.Tipo == "E")
                    sumary.old_elective += item.Creditos;
            }
            //comporbar si ha llevado actividades en su plan
            string old_plan = (matr.FirstOrDefault() == null) ? "P000000002" : asig.FirstOrDefault().IDPlanEstudio;
            int activities = db.tbl_matricula.Where(f =>
                f.IDAlumno == student_id
                && f.IDEscuela == "D01"
                && f.Nota >= 11
                && f.IDPlanEstudio == old_plan
                ).Count();
            //si ha llevado 1 sumamos 1 si no ah llevado nada sumamos 2, si ah lllevado 2 no debe sumar
            if (activities == 1) sumary.new_mandatory += 1;
            if (activities == 0) sumary.new_mandatory += 2;
            //fin de comprobacion de actividades
            foreach (COUTblValidacionMatricula item in matr)
            {
                //Ignorar Actividades
                if (item.IDEscuela == "D01") continue;
                if (item.Tipo == "O")
                    sumary.new_mandatory += item.Creditos;
                else if (item.Tipo == "E")
                    sumary.new_elective += item.Creditos;
            }

            return sumary;
            
        }
        #endregion methods
    }
}
