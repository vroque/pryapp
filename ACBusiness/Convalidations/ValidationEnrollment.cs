﻿using ACPermanence.DataBases.DBUCCI.COU;
using ACPermanence.Contexts.BDUCCI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Convalidations
{
    public class ValidationEnrollment
    {
        #region properties
        public int id {get; set;}
        public string sede_id {get; set;}
        public int document_id {get; set;}
        public string course_id {get; set;}
        public string course_name {get; set;}
        public int credits {get; set;}
        public string type {get; set;}
        public int cicle {get; set;}
        public string study_plan {get; set;}

        private readonly DBOContext _apec = new DBOContext();
        #endregion properties

        #region methods
        public List<ValidationEnrollment> query(int document_id)
        {
            
            List<ValidationEnrollment> enrollments = _apec.cou_tbl_validacion_matricula
                .Where(f => f.IDDocumentoValidacion == document_id).ToList()
                // conversion implicita de la lista
                .Select<COUTblValidacionMatricula, ValidationEnrollment>(x => x)
                .ToList();
            return enrollments;
        }
        #endregion methods

        #region implicite operators
        public static implicit operator ValidationEnrollment(COUTblValidacionMatricula obj)
        {
            ValidationEnrollment ve = new ValidationEnrollment();
            ve.id = obj.ID;
            ve.sede_id = obj.IDSede;
            ve.document_id = obj.IDDocumentoValidacion;
            ve.course_id = obj.IDAsignatura;
            ve.course_name = obj.Nombre;
            ve.credits = obj.Creditos;
            ve.type = obj.Tipo;
            ve.cicle = obj.ciclo;
            ve.study_plan = obj.IDPlanEstudio;
            return ve;
        }
        #endregion implicite operators
    }
}
