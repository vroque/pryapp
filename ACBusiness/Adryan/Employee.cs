﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.ADRYAN.Synonyms;
using _adryan = ACTools.Constants.AdryanConstants;

namespace ACBusiness.Adryan
{
    public class Employee
    {
        #region Properties

        public string codigo_unico { get; set; }
        public string compania { get; set; }
        public string numero_documento { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }
        public string nombre { get; set; }
        public string iddependencia { get; set; }
        public string nombre_unidad_funcional { get; set; }
        public string descripcion_ubicacion_fisica { get; set; }
        public string unidad_funcional_organica { get; set; }
        public string puesto_organica { get; set; }
        public bool IsActive { get; set; }

        #region Propiedades Extras

        public decimal employee_pidm { get; set; }
        public string employee_dni { get; set; }
        public string employee_ce { get; set; }
        public string employee_position_company_description { get; set; }
        public string employee_user_id { get; set; }

        #endregion Propiedades Extras

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public Employee()
        {
        }

        public Employee(Employee employee)
        {
            codigo_unico = employee.codigo_unico;
            compania = employee.compania;
            numero_documento = employee.numero_documento;
            apellido_paterno = employee.apellido_paterno;
            apellido_materno = employee.apellido_materno;
            nombre = employee.nombre;
            iddependencia = employee.iddependencia;
            nombre_unidad_funcional = employee.nombre_unidad_funcional;
            descripcion_ubicacion_fisica = employee.descripcion_ubicacion_fisica;
            unidad_funcional_organica = employee.unidad_funcional_organica;
            puesto_organica = employee.puesto_organica;
            employee_pidm = employee.employee_pidm;
            employee_dni = employee.employee_dni;
            employee_ce = employee.employee_ce;
            employee_position_company_description = employee.employee_position_company_description;
            employee_user_id = employee.employee_user_id;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all profiles of a employee
        /// </summary>
        /// <param name="employeePidm">Employee id</param>
        /// <param name="companyId">Company id</param>
        /// <param name="status">status</param>
        /// <returns></returns>
        public static async Task<List<Employee>> GetByPidmAndCompanyIdAndStatusAsync(decimal employeePidm,
            string companyId, string status)
        {
            DBOTblPersona persona;
            using (var dboContext = new DBOContext())
            {
                persona = await dboContext.tbl_persona.FirstOrDefaultAsync(w => w.IDPersonaN == employeePidm);
                if (persona == null) return null;
            }

            var nexoContext = new NEXOContext();

            List<ADRYANvTrabajador> worker = await nexoContext.adryan_v_trabajador.Where(w =>
                w.numero_documento.Trim() == persona.DNI).ToListAsync();

            if (!worker.Any())
            {
                return new List<Employee>();
            }

            if (!companyId.Equals("all"))
            {
                worker = worker.Where(w => w.compania == companyId).ToList();
            }

            /**
             * Filter by status
             */
            worker = FilterWorkerByStatus(worker, status);

            return worker.Any() ? worker.Select(s => (Employee) s).ToList() : new List<Employee>();
        }

        /// <summary>
        /// Get employee info filtered by company.
        /// Default filter: Employee of UC company.
        /// If there is no employee record with active status, then search any record filtered by DNI
        /// </summary>
        /// <param name="employeePidm">Employee id</param>
        /// <param name="companyId">Company id, value by default is "01" (UNIVERSIDAD CONTINENTAL)</param>
        /// <returns></returns>
        public static async Task<Employee> GetByPidmAndCompanyIdAsync(decimal employeePidm,
            string companyId = _adryan.ContinentalUniversityCompany)
        {
            DBOTblPersona persona;
            using (var dboContext = new DBOContext())
            {
                persona = await dboContext.tbl_persona.FirstOrDefaultAsync(w => w.IDPersonaN == employeePidm);
                if (persona == null) return null;
            }

            var nexoContext = new NEXOContext();
            // Search active employee by DNI and company
            var worker = await nexoContext.adryan_v_trabajador.FirstOrDefaultAsync(w =>
                w.numero_documento.Trim() == persona.DNI && w.compania == companyId && w.fecha_retiro == null);

            if (worker != null)
            {
                return (Employee) worker;
            }

            // If there is no employee record with active status, then search any record filtered by DNI and company
            worker = await nexoContext.adryan_v_trabajador.FirstOrDefaultAsync(w =>
                w.numero_documento.Trim() == persona.DNI && w.compania == companyId);

            // If there is no employee record with active status and company, then search any record filtered only by DNI
            worker = await nexoContext.adryan_v_trabajador.FirstOrDefaultAsync(w =>
                w.numero_documento.Trim() == persona.DNI);

            if (worker == null) return null;

            return (Employee) worker;
        }

        /// <summary>
        /// Get all university employees filtered by functional unit and company.
        /// Default filter: UC company.
        /// </summary>
        /// <param name="functionalUnitId">Functional unit id</param>
        /// <param name="companyId">Company id, value by default is "01" (UNIVERSIDAD CONTINENTAL)</param>
        /// <returns></returns>
        public static async Task<List<Employee>> GetAllEmployeesByFunctionalUnitAndCompanyIdAsync(
            string functionalUnitId, string companyId = _adryan.ContinentalUniversityCompany)
        {
            using (var nexoContext = new NEXOContext())
            {
                List<ADRYANvTrabajador> employees = await nexoContext.adryan_v_trabajador
                    .Where(w => w.unidad_funcional_organica == functionalUnitId &&
                                w.compania == companyId &&
                                w.fecha_retiro == null)
                    .ToListAsync();

                return employees.Select(s => (Employee) s).OrderBy(o => o.apellido_paterno)
                    .ThenBy(o => o.apellido_materno).ToList();
            }
        }

        /// <summary>
        /// Get all employees filtered by functional unit and company.
        /// </summary>
        /// <param name="functionalUnitId">Functional unit id</param>
        /// <param name="companyId">Company id</param>
        /// <returns></returns>
        public static List<Employee> GetAllEmployeesByFunctionalUnit(string functionalUnitId, string companyId)
        {
            using (var nexoContext = new NEXOContext())
            {
                List<ADRYANvTrabajador> employees = nexoContext.adryan_v_trabajador
                    .Where(w => w.unidad_funcional_organica == functionalUnitId &&
                                w.compania == companyId &&
                                w.fecha_retiro == null)
                    .ToList();

                return employees.Select(s => (Employee) s).ToList();
            }
        }

        /// <summary>
        /// Search employee
        /// </summary>
        /// <param name="searchTerm">search term</param>
        /// <param name="type">type of value to search</param>
        /// <param name="companyId">UC or IC</param>
        /// <param name="status">active, inactive, or all</param>
        /// <returns></returns>
        public static async Task<List<Employee>> SearchAsync(string searchTerm, string type, string companyId,
            string status)
        {
            if (type.Equals("userId"))
            {
                var dboContext = new DBOContext();
                var nexoContext = new NEXOContext();

                List<DBOTblPersona> persons = await dboContext.tbl_persona
                    .Where(w => w.IDUsuario.ToLower().Contains(searchTerm.ToLower())).ToListAsync();

                List<string> personsDni = persons.Select(s => s.DNI).Distinct().ToList();

                if (persons.Any())
                {
                    List<ADRYANvTrabajador> employees = nexoContext.adryan_v_trabajador
                        .Where(w => personsDni.Contains(w.numero_documento)).ToList();

                    if (!employees.Any())
                    {
                        return new List<Employee>();
                    }

                    if (!companyId.Equals("all"))
                    {
                        employees = employees.Where(w => w.compania == companyId).ToList();
                    }

                    /**
                     * Filter by status
                     */
                    employees = FilterWorkerByStatus(employees, status);

                    return employees.Any() ? employees.Select(s => (Employee) s).ToList() : new List<Employee>();
                }

                return new List<Employee>();
            }

            return new List<Employee>();
        }

        /// <summary>
        /// Filter workers by status.
        /// Status can be: "active", "inactive", or "all"
        /// </summary>
        /// <param name="trabajadores"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static List<ADRYANvTrabajador> FilterWorkerByStatus(List<ADRYANvTrabajador> trabajadores, string status)
        {
            if (status.Equals("all"))
            {
                return trabajadores;
            }

            if (status.Equals("active"))
            {
                return trabajadores.Where(w => w.fecha_retiro == null).ToList();
            }

            return status.Equals("inactive")
                ? trabajadores.Where(w => w.fecha_retiro != null).ToList()
                : new List<ADRYANvTrabajador>();
        }

        /// <summary>
        /// Lista trabajadores de la compañia por nombre
        /// </summary>
        /// <param name="compania"> código de compañia</param>
        /// <param name="names">nombres</param>
        /// <returns>Lista trabajadores de la compañia por nombre</returns>
        public async Task<List<Employee>> listCollaborators(string compania, string names)
        {
            List<Employee> listCollaborators = _nexoContext.adryan_v_trabajador
                .Join(_nexoContext.adryan_tbl_puesto_compania,
                    avt => new {compania = avt.compania, position_company_description = avt.puesto_organica},
                    tpc => new {compania = tpc.compania, position_company_description = tpc.puesto},
                    (avt, tpc) => new {avt, tpc})
                .Where(result => result.avt.compania == compania &&
                                 (result.avt.nombre.Trim() + " " + result.avt.apellido_paterno.Trim() + " " +
                                  result.avt.apellido_materno.Trim()).Contains(names))
                .ToList()
                .OrderBy(result => result.avt.nombre)
                .ToList()
                .Select(result => new Employee
                {
                    codigo_unico = result.avt.codigo_unico.Trim(),
                    compania = result.avt.compania.Trim(),
                    numero_documento = result.avt.numero_documento.Trim(),
                    apellido_paterno = result.avt.apellido_paterno.Trim(),
                    apellido_materno = result.avt.apellido_materno.Trim(),
                    nombre = result.avt.nombre.Trim() + " " + result.avt.apellido_paterno.Trim() + " " +
                             result.avt.apellido_materno.Trim(),
                    iddependencia = result.avt.iddependencia.Trim(),
                    nombre_unidad_funcional = result.avt.nombre_unidad_funcional.Trim(),
                    descripcion_ubicacion_fisica = result.avt.descripcion_ubicacion_fisica.Trim(),
                    unidad_funcional_organica = result.avt.unidad_funcional_organica.Trim(),
                    puesto_organica = result.avt.puesto_organica.Trim(),
                    employee_position_company_description = result.tpc.descripcion_puesto.Trim()
                }).ToList();

            List<string> persons_DNI = listCollaborators.Select(f => f.numero_documento).Distinct().ToList();
            var bducci = new DBOContext();
            List<DBOTblPersona> persons = bducci.tbl_persona
                .Where(result => result.DNI.ToString() != null &&
                                 result.DNI.Trim().Length > 0 &&
                                 persons_DNI.Contains(result.DNI.ToString()))
                .ToList();
            foreach (var collaborator in listCollaborators)
            {
                var person = persons.FirstOrDefault(result => result.DNI.ToString() == collaborator.numero_documento);
                collaborator.employee_pidm = person?.IDPersonaN ?? 0;
                collaborator.employee_user_id = person?.IDUsuario;
            }

            return listCollaborators;
        }

        /// <summary>
        /// Lista trabajadores de la compañia por nombre y unidad funcional
        /// </summary>
        /// <param name="compania">código de compañia</param>
        /// <param name="funtional_unity_id">código de unidad funcional</param>
        /// <returns>Lista trabajadores de la compañia por nombre y unidad funcional</returns>
        public async Task<List<Employee>> listCollaboratorsFuntionalUnity(string compania,
            string funtional_unity_id)
        {
            List<Employee> listCollaborators = _nexoContext.adryan_v_trabajador
                .Join(_nexoContext.adryan_tbl_puesto_compania,
                    avt => new {compania = avt.compania, position_company_description = avt.puesto_organica},
                    tpc => new {compania = tpc.compania, position_company_description = tpc.puesto},
                    (avt, tpc) => new {avt, tpc})
                .Where(result => result.avt.compania == compania &&
                                 result.avt.unidad_funcional_organica == funtional_unity_id)
                .ToList()
                .OrderBy(result => result.avt.nombre)
                .ToList()
                .Select(result => new Employee
                {
                    codigo_unico = result.avt.codigo_unico.Trim(),
                    compania = result.avt.compania.Trim(),
                    numero_documento = result.avt.numero_documento.Trim(),
                    apellido_paterno = result.avt.apellido_paterno.Trim(),
                    apellido_materno = result.avt.apellido_materno.Trim(),
                    nombre = result.avt.nombre.Trim() + " " + result.avt.apellido_paterno.Trim() + " " +
                             result.avt.apellido_materno.Trim(),
                    iddependencia = result.avt.iddependencia.Trim(),
                    nombre_unidad_funcional = result.avt.nombre_unidad_funcional.Trim(),
                    descripcion_ubicacion_fisica = result.avt.descripcion_ubicacion_fisica.Trim(),
                    unidad_funcional_organica = result.avt.unidad_funcional_organica.Trim(),
                    puesto_organica = result.avt.puesto_organica.Trim(),
                    employee_position_company_description = result.tpc.descripcion_puesto.Trim()
                })
                .ToList();

            List<string> persons_DNI = listCollaborators.Select(f => f.numero_documento).Distinct().ToList();
            var bducci = new DBOContext();
            List<DBOTblPersona> persons = bducci.tbl_persona
                .Where(result => result.DNI.ToString() != null &&
                                 result.DNI.Trim().Length > 0 &&
                                 persons_DNI.Contains(result.DNI.ToString()))
                .ToList();
            foreach (var collaborator in listCollaborators)
            {
                var person = persons.FirstOrDefault(result => result.DNI.ToString() == collaborator.numero_documento);
                collaborator.employee_pidm = person?.IDPersonaN ?? 0;
                collaborator.employee_user_id = person?.IDUsuario;
            }

            return listCollaborators;
        }

        public async Task<Employee> getColaboratosByPidm(string compania, int pidm)
        {
            var bducci = new DBOContext();

            var collaborator = bducci.tbl_persona
                .Where(result => result.IDPersonaN == pidm)
                .Select(result => new Employee
                {
                    employee_pidm = result.IDPersonaN,
                    employee_dni = result.DNI,
                    employee_user_id = result.IDUsuario,
                    employee_ce = result.CE,
                })
                .FirstOrDefault();

            if (collaborator != null)
            {
                var collaborator2 = _nexoContext.adryan_v_trabajador
                    .Join(_nexoContext.adryan_tbl_puesto_compania,
                        sat => new {compania = sat.compania, position_company_description = sat.puesto_organica},
                        tpc => new {compania = tpc.compania, position_company_description = tpc.puesto},
                        (sat, tpc) => new {sat, tpc})
                    .Where(result => (collaborator.employee_ce == null &&
                                      result.sat.numero_documento == collaborator.employee_dni) ||
                                     (collaborator.employee_ce != null &&
                                      result.sat.numero_documento == collaborator.employee_ce))
                    .ToList()
                    .Select(result => new Employee
                    {
                        codigo_unico = result.sat.codigo_unico.Trim(),
                        compania = result.sat.compania.Trim(),
                        numero_documento = result.sat.numero_documento.Trim(),
                        apellido_paterno = result.sat.apellido_paterno.Trim(),
                        apellido_materno = result.sat.apellido_materno.Trim(),
                        nombre = result.sat.nombre.Trim() + " " + result.sat.apellido_paterno.Trim() + " " +
                                 result.sat.apellido_materno.Trim(),
                        iddependencia = result.sat.iddependencia.Trim(),
                        nombre_unidad_funcional = result.sat.nombre_unidad_funcional.Trim(),
                        descripcion_ubicacion_fisica = result.sat.descripcion_ubicacion_fisica.Trim(),
                        unidad_funcional_organica = result.sat.unidad_funcional_organica.Trim(),
                        puesto_organica = result.sat.puesto_organica.Trim(),
                        employee_position_company_description = result.tpc.descripcion_puesto.Trim()
                    })
                    .FirstOrDefault();

                if (collaborator2 != null)
                {
                    collaborator.codigo_unico = collaborator2.codigo_unico;
                    collaborator.compania = collaborator2.compania;
                    collaborator.numero_documento = collaborator2.numero_documento;
                    collaborator.apellido_paterno = collaborator2.apellido_paterno;
                    collaborator.apellido_materno = collaborator2.apellido_materno;
                    collaborator.nombre = collaborator2.nombre;
                    collaborator.iddependencia = collaborator2.iddependencia;
                    collaborator.nombre_unidad_funcional = collaborator2.nombre_unidad_funcional;
                    collaborator.descripcion_ubicacion_fisica = collaborator2.descripcion_ubicacion_fisica;
                    collaborator.unidad_funcional_organica = collaborator2.unidad_funcional_organica;
                    collaborator.puesto_organica = collaborator2.puesto_organica;
                    collaborator.employee_position_company_description =
                        collaborator2.employee_position_company_description;
                }
            }

            return collaborator;
        }

        #endregion Methods

        #region Operators

        public static explicit operator Employee(ADRYANvTrabajador trabajador)
        {
            if (trabajador == null) return null;

            var dboContext = new DBOContext();
            var persona =
                dboContext.tbl_persona.FirstOrDefaultAsync(w => w.DNI == trabajador.numero_documento).Result;

            var employee = new Employee
            {
                codigo_unico = trabajador.codigo_unico,
                compania = trabajador.compania,
                numero_documento = trabajador.numero_documento.Trim(),
                apellido_paterno = trabajador.apellido_paterno.Trim(),
                apellido_materno = trabajador.apellido_materno.Trim(),
                nombre = trabajador.nombre.Trim(),
                iddependencia = trabajador.iddependencia,
                nombre_unidad_funcional = trabajador.nombre_unidad_funcional.Trim(),
                descripcion_ubicacion_fisica = trabajador.descripcion_ubicacion_fisica.Trim(),
                unidad_funcional_organica = trabajador.unidad_funcional_organica,
                puesto_organica = trabajador.puesto_organica,
                IsActive = trabajador.fecha_retiro == null,
                employee_pidm = persona?.IDPersonaN ?? 0,
                employee_dni = persona?.DNI,
                employee_ce = persona?.CE,
                employee_user_id = persona?.IDUsuario
            };

            return employee;
        }

        #endregion Operators
    }
}
