﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.ADRYAN.Synonyms;
using _adryan = ACTools.Constants.AdryanConstants;

namespace ACBusiness.Adryan
{
    public class FunctionalUnit
    {
        #region Properties

        public string unidad_funcional { get; set; }
        public string compania { get; set; }
        public string codigo_sucursal { get; set; }
        public string centro_costo { get; set; }
        public string nombre_unidad_funcional { get; set; }
        public string unidad_funcional_superior { get; set; }
        public int members { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public FunctionalUnit()
        {
        }

        public FunctionalUnit(FunctionalUnit functionalUnit)
        {
            unidad_funcional = functionalUnit.unidad_funcional;
            compania = functionalUnit.compania;
            codigo_sucursal = functionalUnit.codigo_sucursal;
            centro_costo = functionalUnit.centro_costo;
            nombre_unidad_funcional = functionalUnit.nombre_unidad_funcional;
            unidad_funcional_superior = functionalUnit.unidad_funcional_superior;
            members = functionalUnit.members;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get functional info by id and company.
        /// Default filter: UC functional units.
        /// </summary>
        /// <param name="functionalUnitId">Functional unit id/code</param>
        /// <param name="companyId">Company id</param>
        /// <returns></returns>
        public static async Task<FunctionalUnit> GetByIdAndCompanyIdAsync(string functionalUnitId,
            string companyId = _adryan.ContinentalUniversityCompany)
        {
            using (var nexoContext = new NEXOContext())
            {
                var functionalUnit =
                    await nexoContext.adryan_tbl_unidad_funcional.FirstOrDefaultAsync(w =>
                        w.unidad_funcional == functionalUnitId && w.compania == companyId);

                return (FunctionalUnit) functionalUnit;
            }
        }

        /// <summary>
        /// Get all main functional units
        /// Default filter: UC functional units of UC company.
        /// </summary>
        /// <param name="functionalUnitId">Functional unit id, value by default is "00000005" (UC PRESIDENCIA)</param>
        /// <param name="companyId">Company id, value by default is "01" (UNIVERSIDAD CONTINENTAL)</param>
        /// <returns></returns>
        public static async Task<List<FunctionalUnit>> GetAllMainFunctionalUnitByFunctionalUnitIdAndCompanyIdAsync(
            string functionalUnitId = _adryan.ContinentalUniversityRootFunctionalUnit,
            string companyId = _adryan.ContinentalUniversityCompany)
        {
            using (var nexoContext = new NEXOContext())
            {
                List<ADRYANtbl_unidad_funcional> mainFunctionalUnits = await nexoContext.adryan_tbl_unidad_funcional
                    .Where(w => w.unidad_funcional_superior == functionalUnitId && w.compania == companyId)
                    .ToListAsync();

                return mainFunctionalUnits.Select(s => (FunctionalUnit) s).ToList();
            }
        }

        /// <summary>
        /// Get all child functional units of a UC functional unit.
        /// Default filter: UC child functional units of UC company.
        /// </summary>
        /// <returns></returns>
        public async Task<List<FunctionalUnit>> GetAllChildFunctionalUnitByCompanyIdAsync(
            string companyId = _adryan.ContinentalUniversityCompany)
        {
            List<ADRYANtbl_unidad_funcional> childFunctionalUnits = await _nexoContext.adryan_tbl_unidad_funcional
                .Where(w => w.unidad_funcional_superior == unidad_funcional &&
                            w.compania == companyId)
                .ToListAsync();

            return childFunctionalUnits.Select(s => (FunctionalUnit) s).ToList();
        }

        /// <summary>
        /// Functional unit by company id
        /// - 01 : UNIVERSIDAD CONTINENTAL
        /// - 02 : CORPORACION APEC
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static async Task<List<FunctionalUnit>> GetAllByCompanyIdAsync(string companyId)
        {
            var nexoContext = new NEXOContext();
            List<ADRYANtbl_unidad_funcional> functionalUnitList = await nexoContext.adryan_tbl_unidad_funcional
                .Where(w => w.compania == companyId)
                .ToListAsync();

            return functionalUnitList.Select(s => (FunctionalUnit) s).OrderBy(o => o.nombre_unidad_funcional).ToList();
        }

        /// <summary>
        /// Search functional units
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static async Task<List<FunctionalUnit>> SearchAsync(string searchTerm, string companyId)
        {
            using (var nexoContext = new NEXOContext())
            {
                List<ADRYANtbl_unidad_funcional> functionalUnits = await nexoContext.adryan_tbl_unidad_funcional
                    .Where(w => w.nombre_unidad_funcional.Contains(searchTerm)).ToListAsync();

                return companyId.Equals("all")
                    ? functionalUnits.Select(s => (FunctionalUnit) s).ToList()
                    : functionalUnits.Where(w => w.compania == companyId).ToList().Select(s => (FunctionalUnit) s)
                        .ToList();
            }
        }

        #endregion Methods

        #region Operators

        public static explicit operator FunctionalUnit(ADRYANtbl_unidad_funcional unidadFuncional)
        {
            if (unidadFuncional == null) return null;

            var functionalUnit = new FunctionalUnit
            {
                unidad_funcional = unidadFuncional.unidad_funcional,
                compania = unidadFuncional.compania,
                codigo_sucursal = unidadFuncional.codigo_sucursal,
                centro_costo = unidadFuncional.centro_costo,
                nombre_unidad_funcional = unidadFuncional.nombre_unidad_funcional.Trim(),
                unidad_funcional_superior = unidadFuncional.unidad_funcional_superior,
                members = Employee
                    .GetAllEmployeesByFunctionalUnit(unidadFuncional.unidad_funcional, unidadFuncional.compania).Count,
            };


            return functionalUnit;
        }

        #endregion Operators
    }
}
