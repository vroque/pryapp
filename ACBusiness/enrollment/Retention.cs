﻿using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ACBusiness.Accounts
{
    public class Retention
    {
        #region properties

        public int pidm { get; set; }
        public string hlddCode { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public string reason { get; set; }
        public string description { get; set; }
        public string descriptionComplete { get; set; }
        public int surrogateId { get; set; }

        readonly BNContext _banner = new BNContext();

        #endregion properties

        #region methods

        /// <summary>
        /// Devuelve todas las retenciones de un estudiante
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public List<Retention> getRetentionsByPidm(decimal pidm)
        {
            List<Retention> retentions = new List<Retention>();

            retentions = _banner.p_retencionesxalumnomatri123(pidm).ToList()
                            .Select<P_RETENCIONESXALUMNOMATRI123, Retention>(f => f).ToList();

            return retentions;
        }
        
        #endregion methods
       
        #region implicite operators
        public static implicit operator Retention(P_RETENCIONESXALUMNOMATRI123 obj)
        {
            Retention retention = new Retention();
            retention.pidm = obj.PIDM;
            retention.hlddCode = obj.HLDD_CODE;
            retention.fromDate = obj.FROM_DATE;
            retention.toDate = obj.TO_DATE;
            retention.reason = obj.REASON;
            retention.description = obj.DESCRIPTION;
            retention.descriptionComplete = obj.DESCRIPTION2;
            retention.surrogateId = obj.SURROGATE_ID;
            return retention;
        }
        #endregion implicite operators
    }
}

