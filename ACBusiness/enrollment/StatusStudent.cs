﻿using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using System.Linq;

namespace ACBusiness.Accounts
{
    public class StatusStudent
    {
        #region properties

        public int pidm { get; set; }
        public bool active { get; set; }
        public string term { get; set; }

        readonly BNContext _banner = new BNContext();

        #endregion properties

        #region methods

        /// <summary>
        /// Devuelve el ultimo estado del estudiante
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public StatusStudent getStatusByStudent(decimal pidm)
        {
            StatusStudent status = new StatusStudent();

            status = _banner.p_estadoxalumno(pidm).Select<P_ESTADOXALUMNO, StatusStudent>(f => f).FirstOrDefault();

            return status;
        }
        
        #endregion methods
       
        #region implicite operators
        public static implicit operator StatusStudent(P_ESTADOXALUMNO obj)
        {
            StatusStudent status = new StatusStudent();
            status.pidm = obj.sgbstdn_pidm;
            status.active = (obj.sgbstdn_stst_code == "AS") ? true : false;
            status.term = obj.sgbstdn_term_code_eff;
            return status;
        }
        #endregion implicite operators
    }
}

