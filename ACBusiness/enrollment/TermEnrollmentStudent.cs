﻿using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using System.Linq;

namespace ACBusiness.enrollment
{
    public class TermEnrollmentStudent
    {
        #region properties

        public int pidm { get; set; }
        public string termEnrollment { get; set; }
        public string term { get; set; }

        readonly BNContext _banner = new BNContext();

        #endregion properties

        #region methods

        /// <summary>
        /// Devuelve el periodo de matricula del estudiante
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public TermEnrollmentStudent getTermEnrollmentByStudent(decimal pidm)
        {
            TermEnrollmentStudent status = new TermEnrollmentStudent();

            status = _banner.p_periodomatriactivoxalum(pidm).Select<P_PERIODOMATRIACTIVOXALUM, TermEnrollmentStudent>(f => f).FirstOrDefault();

            return status;
        }

        #endregion methods

        #region implicite operators
        public static implicit operator TermEnrollmentStudent(P_PERIODOMATRIACTIVOXALUM obj)
        {
            TermEnrollmentStudent status = new TermEnrollmentStudent();
            status.pidm = obj.PIDM;
            status.termEnrollment = obj.TERM_ENROLLMENT;
            status.term = obj.TERM;
            return status;
        }
        #endregion implicite operators
    }
}
