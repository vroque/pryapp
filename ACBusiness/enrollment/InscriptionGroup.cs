﻿using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ACBusiness.Accounts
{
    public class InscriptionGroup
    {
        #region properties

        public int pidm { get; set; }
        public string term { get; set; }
        public string code { get; set; }
        public int priority { get; set; }
        public DateTime beginDate { get; set; }
        public DateTime endDate { get; set; }
        public bool enrollment { get; set; }

        readonly BNContext _banner = new BNContext();

        #endregion properties
        
        #region methods

        /// <summary>
        /// Devuelve el grupo de inscripción de un estudiante
        /// </summary>
        /// <param name="pidm">pidm del estudiante</param>
        /// <param name="term">periodo de la consulta</param>
        public InscriptionGroup getInscriptionGroup(decimal pidm, string term)
        {
            InscriptionGroup inscriptionGroup = _banner.p_grupoinsxalumno(pidm, term).Select<P_GRUPOINSXALUMNO, InscriptionGroup>(f => f).FirstOrDefault();

            return inscriptionGroup;
        }
        
        #endregion methods
       
        #region implicite operators

        public static implicit operator InscriptionGroup(P_GRUPOINSXALUMNO obj)
        {
            InscriptionGroup inscriptionGroup = new InscriptionGroup();
            inscriptionGroup.pidm = obj.SFBRGRP_PIDM;
            inscriptionGroup.term = obj.SFBRGRP_TERM_CODE;
            inscriptionGroup.code = obj.SFBRGRP_RGRP_CODE;
            inscriptionGroup.priority = obj.SFBWCTL_PRIORITY;
            inscriptionGroup.beginDate = obj.SFRWCTL_BEGIN_DATE;
            inscriptionGroup.endDate = obj.SFRWCTL_END_DATE;
            inscriptionGroup.enrollment = (obj.ENROLLMENT==1)? true:false;
            return inscriptionGroup;
        }
        #endregion implicite operators
    }
}

