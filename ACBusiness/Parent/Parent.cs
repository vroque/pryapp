﻿using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.CAU;
using ACTools.SuperString;
using System;
using System.IO;
using System.Linq;
using ACPermanence.DataBases.DBUCCI.DBO.views;
using System.Collections.Generic;
using ACBusiness.Personal;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ACBusiness.Parent
{
    /// <author>Julio Palacios</author>
    /// <date>06/11/2017</date>
    /// <summary>
    /// Clase de PersonaApoderado para bindear 
    /// con la base de datos
    /// </summary>
    /// <note>
    /// Se esta jalando la información desde DBINTBANNER
    /// </note>
    public class Relationship
    {
        #region properties
        //public decimal id { get; set; }
        //public decimal pidm { get; set; }
        //public string studentpidm { get; set; }
        //public string relationship { get; set; }
        //public bool active { get; set; }
        //public DateTime activationdate { get; set; }
        //public decimal activationuserpidm { get; set; }
        //public DateTime lastmodificationdate { get; set; }
        //public string lastmodificationcomment { get; set; }
        //public decimal lastmodificationuserpidm { get; set; }
        public int id { get; set; }
        public decimal parent_id { get; set; }
        public Person child { get; set; }
        public int kinship_id { get; set; }
        public bool active { get; set; }
        public DateTime? update_at { get; set; }
        public List<Academic.AcademicProfile> profile { get; set; }


        private DBOContext _db = new DBOContext();
        private NEXOContext _nx = new NEXOContext();
        #endregion properties

        #region functions
        /// <summary>
        /// Lista las personas que estan registradas como "hijos" de una persona
        /// por su pidm
        /// </summary>
        /// <param name="parent_id">PIDM del padre/apoderado</param>
        /// <returns></returns>
        public async Task<List<Relationship>> listChilds(decimal parent_id)
        {
            List<CAUPersonApoderado> parent_list = await _nx.cau_person_apoderado
                    .Where(item => item.PIDM == parent_id).ToListAsync();
            if (parent_list == null || parent_list.Count == 0)
            {
                return null;
            }
            //obtener los objetos de persona de los children
            List<int> student_ids = parent_list.Select(f =>
                (int)f.StudentPIDM).Distinct().ToList();
            List<DBOTblPersona> child_list = await _db.tbl_persona
                .Where(f => student_ids.Contains(f.IDPersonaN))
                .ToListAsync();

            List<Relationship> relations = parent_list.Select(f => new Relationship {
                id = f.ID,
                parent_id = parent_id,
                child = child_list.FirstOrDefault(g => (int)f.StudentPIDM == g.IDPersonaN),
                kinship_id = f.KinshipID,
                active = f.Active,
                update_at = f.LastModificationDate
            }).ToList();

            return relations;
        }


        //crud
        public async Task<Relationship> create(decimal parent_id, decimal child_id, string relation, decimal user_id, string comment = "")
        {
            CAUPersonApoderado obj = new CAUPersonApoderado();
            obj.LastModificationComment = comment;
            obj.LastModificationDate = DateTime.Now;
            obj.LastModificationUserPIDM = user_id;
            obj.PIDM = parent_id;
            //obj.relationship = relation;
            obj.StudentPIDM = child_id;
            obj.Active = true;
            //validacion de existencia
            List<Relationship> ps = await listChilds(parent_id);
            if (ps.Count(f => f.child.id == child_id) > 0)
            {
                throw new Exception("ya existe lo que quieres crear");
            }
            else
            {
                _nx.cau_person_apoderado.Add(obj);
                int r = await _nx.SaveChangesAsync();
                if (r > 0)
                    return obj;
                return null;
            }
        }

        public async Task<Relationship> update(string comment, decimal user_id)
        {
            CAUPersonApoderado obj = new CAUPersonApoderado();
            obj.LastModificationComment = comment;
            obj.LastModificationDate = DateTime.Now;
            obj.LastModificationUserPIDM = user_id;
            obj.PIDM = this.parent_id;
            obj.KinshipID = this.kinship_id;
            obj.StudentPIDM = this.child.id;
            obj.Active = this.active;
            _nx.cau_person_apoderado.Add(obj);

            int r = await _nx.SaveChangesAsync();
            if (r > 0)
                return obj;
            return null;
        }

        public bool parentExist(decimal parent_id)
        {
            CAUPersonApoderado parent = _nx.cau_person_apoderado.FirstOrDefault(item => item.PIDM == parent_id);
            if (parent != null)
            {
                return true;
            }

            return false;
        }

        public Person getPersonIfExist(decimal parent_id)
        {
            CAUPersonApoderado parent = _nx.cau_person_apoderado
                    .FirstOrDefault(item => item.PIDM == parent_id);
            if (parent != null)
            {
                Person person = _db.tbl_persona.FirstOrDefault(f => f.IDPersonaN == (int)parent_id);
                if(person != null)
                    return person;
            }
            
            return null;
        }

        //REMOVER
        //public static Person getByORacle()
        //{
        //    BNContext db = new BNContext();
        //    SPRIDEN sp = db.dim_personas.FirstOrDefault();
        //    Person p = new Person();
        //    p.id = sp.SPRIDEN_PIDM;
        //    p.last_name = sp.SPRIDEN_LAST_NAME;
        //    p.first_name = sp.SPRIDEN_FIRST_NAME;
        //     more arguments
        //    return p;
        //}
        #endregion functions

        #region inplicit operators
        public static implicit operator Relationship(CAUPersonApoderado tmp)
        {
            if (tmp == null) return null;
            Relationship p = new Relationship();

            return p;
        }
        #endregion
    }
}
