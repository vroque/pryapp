﻿using ACPermanence.Contexts.NEXO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACBusiness.Poll
{
    public class PollQuestion
    {
        #region Propiedades
        public long Id { get; set; }
        public long PollId { get; set; }
        public string QuestionTitle { get; set; }
        public short QuestionOrder { get; set; }
        public bool IsComment { get; set; }
        public List<PollChoice> Choices { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();
        #endregion Propiedades

        #region Metodos
        /// <summary>
        /// Obtener preguntas de una encuesta
        /// </summary>
        /// <param name="pollId">ID de encuesta</param>
        /// <returns></returns>
        public async Task<List<PollQuestion>> GetQuestionsByPoll(long pollId) {
            List<PollQuestion> data = _nexoContext.cau_poll_question
                .Where(conditional => conditional.PollId == pollId)
                .ToList()
                .OrderBy(order => order.QuestionOrder)
                .Select(
                    result => new PollQuestion
                    {
                        Id = result.Id,
                        PollId = result.PollId,
                        QuestionTitle = result.QuestionTitle,
                        QuestionOrder = result.QuestionOrder,
                        IsComment = result.IsComment
                    }
                ).ToList();
            PollChoice objChoise = new PollChoice();
            List<long> listQuestionsId = data.Select(f => f.Id).Distinct().ToList();
            List<PollChoice> choises = objChoise.GetChoicesByQuestions(listQuestionsId);
            data.ForEach(
                itemQuestion =>
                    itemQuestion.Choices = choises.Where(f => f.PollQuestionId == itemQuestion.Id).Select(s => s).ToList()
                );
            return data;
        }
        #endregion Metodos
    }
}
