﻿using ACPermanence.Contexts.NEXO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _poll_constants = ACTools.Constants.PollConstants;
using System.Data.Entity;
namespace ACBusiness.Poll
{
    public class Poll
    {
        #region Propiedades
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime PublicationDate { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsRequest { get; set; }
        public short? RequestState { get; set; }
        public bool Active { get; set; }
        public List<PollQuestion> Questions { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();
        #endregion Propiedades

        #region Metodos
        /// <summary>
        /// Obtener la encuesta por estado de solicitud
        /// </summary>
        /// <param name="requestState">estado de solicitud</param>
        /// <returns></returns>
        public async Task<Poll> GetPollByRequestState(int requestState) {
            Poll data = _nexoContext.cau_poll
                .Where(
                    conditional =>
                        conditional.IsRequest
                        && conditional.RequestState == requestState
                        && conditional.Active == _poll_constants.ACTIVE
                        && conditional.BeginDate <= DbFunctions.TruncateTime(DateTime.Now)
                        && conditional.EndDate >= DbFunctions.TruncateTime(DateTime.Now)
                ).ToList()
                .OrderBy(order => order.PublicationDate)
                .Select(
                    result => new Poll
                    {
                        Id = result.Id,
                        Name = result.Name,
                        Description = result.Description,
                        PublicationDate = result.PublicationDate,
                        BeginDate = result.BeginDate,
                        EndDate = result.EndDate,
                        IsRequest = result.IsRequest,
                        RequestState = result.RequestState,
                        Active = result.Active
                    }
                ).ToList().FirstOrDefault();
            if (data != null)
            {
                PollQuestion objPollQuestion = new PollQuestion();
                data.Questions = await objPollQuestion.GetQuestionsByPoll(data.Id);
            }
            
            return data;
        }
        #endregion Metodos

    }
}
