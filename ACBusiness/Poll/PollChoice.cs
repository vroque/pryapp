﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ACBusiness.Poll
{
    public class PollChoice
    {
        #region Propiedades
        public long Id { get; set; }
        public long PollQuestionId { get; set; }
        public short OrderChoice { get; set; }
        public string ChoiceText { get; set; }
        public int Votes { get; set; }
        public bool IsComment { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();
        #endregion Propiedades

        #region Metodos
        /// <summary>
        /// Retorna opciones de una pregunta (ordernado)
        /// </summary>
        /// <param name="questionID"></param>
        /// <returns></returns>
        public List<PollChoice> GetChoicesByQuestions(List<long> questionIDs)
        {
            List<PollChoice> data = _nexoContext.cau_poll_choice
                .Where(contional => questionIDs.Contains(contional.PollQuestionId))
                .ToList()
                .OrderBy(order => order.OrderChoice).ToList()
                .Select(result => (PollChoice)(result)).ToList();
            return data;

        }

        /// <summary>
        /// Obtener informacion de una opción por el id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        public PollChoice GetChoiceById(long id)
        {
            PollChoice data = _nexoContext.cau_poll_choice
                .Where(conditional => conditional.Id == id)
                .ToList()
                .Select(result => (PollChoice)(result))
                .FirstOrDefault();
            return data;
        }

        /// <summary>
        /// Agregar en uno la votación de una opción
        /// </summary>
        /// <param name="id">código de opción</param>
        /// <returns></returns>
        public PollChoice AddValuePollChoice(long id)
        {
            CAUPollChoice dataPollChoice = _nexoContext.cau_poll_choice.Find(id);
            if (dataPollChoice == null) throw new Exception("Error al obtener opción");
            dataPollChoice.Votes++;
            int save = _nexoContext.SaveChanges();
            if (save <= 0) throw new Exception("Error al guardar encuesta.");   
            PollChoice data = (PollChoice)(dataPollChoice);
            return data;

        }
        #endregion Metodos



        #region Operadores explicito
        public static explicit operator PollChoice(CAUPollChoice obj)
        {
            PollChoice pollChoice = new PollChoice() {
                Id = obj.Id,
                PollQuestionId = obj.PollQuestionId,
                OrderChoice = obj.OrderChoice,
                ChoiceText = obj.ChoiceText,
                Votes = obj.Votes,
                IsComment = obj.IsComment
            };
            return pollChoice;
        }
        #endregion Operadores explicito
    }
}
