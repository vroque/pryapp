﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System;

namespace ACBusiness.Poll
{
    public class PollChoiceDetail
    {
        #region Propiedades
        public long Id { get; set; }
        public long PollChoiceId { get; set; }
        public decimal Pidm { get; set; }
        public int RequestId { get; set; }
        public string Comment { get; set; }
        public DateTime VoteDate { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();
        #endregion Propiedades

        #region Metodos
        /// <summary>
        /// Guardar detalle de votación
        /// </summary>
        /// <param name="data">información a guardar</param>
        /// <returns></returns>

        public PollChoiceDetail SavePollChoiceDetail(PollChoiceDetail data)
        {
            CAUPollChoiceDetail dataSave = new CAUPollChoiceDetail()
            {
                PollChoiceId = data.PollChoiceId,
                Pidm = data.Pidm,
                RequestId = data.RequestId,
                Comment = data.Comment == null ? null : data.Comment.Trim(),
                VoteDate = DateTime.Now
            };
            //validar que si es comentario no guarde
            CAUPollChoice dataPollChoice = _nexoContext.cau_poll_choice.Find(data.PollChoiceId);
            if (dataPollChoice == null) throw new Exception("Error al obtener opción");
            if (dataPollChoice.IsComment && (dataSave.Comment == null || dataSave.Comment.Trim().Length <= 0))
                return data;
            //Guardar
            _nexoContext.cau_poll_choice_detail.Add(dataSave);
            int save = _nexoContext.SaveChanges();
            if (save <= 0)
                throw new Exception("Error al guardar detalle de votación de encuesta");
            data.Id = dataSave.Id;
            PollChoice choice = new PollChoice();
            choice.AddValuePollChoice(dataSave.PollChoiceId);
            return data;
        }

        #endregion Metodos
    }
}
