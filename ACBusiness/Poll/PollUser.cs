﻿using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ACBusiness.Poll
{
    public class PollUser
    {
        #region Propiedades
        public long Id { get; set; }
        public long PollId { get; set; }
        public decimal Pidm { get; set; }
        public int RequestId { get; set; }
        public short Order { get; set; }
        public DateTime PollDate { get; set; }
        public bool Omitted { get; set; }
        public bool Completed { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();
        #endregion Propiedades

        #region Metodos

        /// <summary>
        /// Obtiene encuestas realizadas por el alumno en un rango de dias (para el pasado)
        /// </summary>
        /// <param name="pidm">pidm estudiante</param>
        /// <param name="numberDays">numero de dias</param>
        /// <param name="isRequest">si es solicitud</param>
        /// <returns></returns>
        public async Task<List<PollUser>> GetPollUserByPidmAndDays(int pidm, int numberDays, bool isRequest = false)
        {
            DateTime dateInit = Convert.ToDateTime(DateTime.Now.ToShortDateString()).AddDays(-numberDays);
            DateTime dateEnd = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            List<PollUser> data = _nexoContext.cau_poll_user
                .Join(
                    _nexoContext.cau_poll,
                    cpu => new { pullID = cpu.PollId },
                    cp => new { pullID = cp.Id },
                    (cpu, cp) => new { cpu, cp }
                )
                .Where(
                    conditional =>
                        conditional.cpu.Pidm == pidm
                        && conditional.cp.IsRequest == isRequest
                        && DbFunctions.TruncateTime(conditional.cpu.PollDate) >= dateInit
                        && DbFunctions.TruncateTime(conditional.cpu.PollDate) <= dateEnd
                ).ToList()
                .Select(result => (PollUser)(result.cpu))
                .ToList();
            return data;
        }

        /// <summary>
        /// Onbiene encuestas realizas por el alumno 
        /// valida si es de tramite y si es omitido
        /// </summary>
        /// <param name="pidm">pidm estudiante</param>
        /// <param name="isRequest">si es de solicitud</param>
        /// <param name="requestId">codigo de solicitud</param>
        /// <param name="omitted">omitido?</param>
        /// <returns></returns>
        public async Task<List<PollUser>> GetPollUserByPidm(int pidm, bool isRequest = false, int? requestId = null, bool omitted = false)
        {
            List<PollUser> data = _nexoContext.cau_poll_user
                .Join(
                    _nexoContext.cau_poll,
                    cpu => new { pullID = cpu.PollId },
                    cp => new { pullID = cp.Id },
                    (cpu, cp) => new { cpu, cp }
                )
                .Where(
                    conditional =>
                        conditional.cpu.Pidm == pidm
                        && conditional.cp.IsRequest == isRequest
                        && (requestId == null || conditional.cpu.RequestId == requestId)
                        && conditional.cpu.Omitted == omitted
                ).ToList()
                .Select(result => (PollUser)(result.cpu))
                .ToList();
            return data;
        }
        /// <summary>
        /// valida si un estudiante debe tomar la encuesta de una solicitud
        /// </summary>
        /// <param name="request">solicitud</param>
        /// <returns></returns>
        public async Task<bool> ValidateMustPollRequest(int requestId) {
            ACRequest request = await ACRequest.get(requestId);
            if (request.id % 2 != 0)
            {
                return false;
            }
            List<PollUser> pollUserOmitted = await GetPollUserByPidm(int.Parse(request.person_id.ToString()), true, request.id, true);
            if (pollUserOmitted.Count > 0)
            {
                return false;
            }
            List<PollUser> pollUser15Days = await GetPollUserByPidmAndDays(int.Parse(request.person_id.ToString()), 15, true);
            if (pollUser15Days.Count > 0)
            {
                return false;
            }
            return true;

        }

        /// <summary>
        /// Guardar pull user
        /// </summary>
        /// <param name="data"> información que se guardara</param>
        /// <param name="pollChoiceID">Codigo de opción</param>
        /// <param name="comment">Comentario</param>
        /// <returns></returns>
        public async Task<PollUser> SavePollUser(PollUser data, List<PollChoiceDetail> dataPollChoiceDetail)
        {
            if (!data.Omitted && dataPollChoiceDetail.Count <= 0)
                throw new Exception("Detalle de voto en nulo.");
            CAUPollUser saveData = new CAUPollUser() {
                PollId = data.PollId,
                Pidm = data.Pidm,
                RequestId = data.RequestId,
                Order = 1,
                PollDate = DateTime.Now,
                Omitted = data.Omitted,
                Completed = data.Omitted ? false : true
            };
            _nexoContext.cau_poll_user.Add(saveData);
            int save = _nexoContext.SaveChanges();
            if (save <= 0) throw new Exception("Error al guardar en PollUSer");
            data = (PollUser)(saveData);
            if (!data.Omitted)
            {
                foreach (PollChoiceDetail pollChoiceDetail in dataPollChoiceDetail)
                {
                    pollChoiceDetail.SavePollChoiceDetail(pollChoiceDetail);
                }
            }
            return data;
        }
        
        #endregion Metodos

        #region Operadores explicitos

        /// <summary>
        /// Conversión implicita del tipo CAUPoolUser de la capa permanencia
        /// al tipo PollUser de la capa de negocio
        /// </summary>
        public static explicit operator PollUser(CAUPollUser obj)
        {
            PollUser pollUser = new PollUser() {
                Id = obj.Id,
                PollId = obj.PollId,
                Pidm = obj.Pidm,
                RequestId = obj.RequestId,
                Order = obj.Order,
                PollDate = obj.PollDate,
                Omitted = obj.Omitted,
                Completed = obj.Completed
            };
            return pollUser;
        }

        #endregion Operadores explicitos
    }
}
