﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.VUC;

namespace ACBusiness.ExtracurricularActivities
{
    public class ActivityType
    {
        #region Properties
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre de tipo de actividad
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Implicit operator
        public static implicit operator ActivityType(VUCActivityType model)
        {
            if (model == null) return null;

            return new ActivityType
            {
                Id = model.Id,
                Name = model.Name
            };
        }
        #endregion

        #region Methods
        /// <summary>
        /// Obtiene los tipos de actividades.
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<ActivityType>> GetAll()
        {
            using (var nexoContext = new NEXOContext())
            {
                return await nexoContext.vuc_activity_types.AsNoTracking().Where(act => act.Active)
                    .Select(act => new ActivityType { Id = act.Id, Name = act.Name}).ToListAsync();
            }
        }

        /// <summary>
        /// Obtiene el tipo de actividad mediante su ID.
        /// </summary>
        /// <param name="id">Id del tipo de actividad</param>
        /// <returns></returns>
        public static async Task<ActivityType> GetById(int id)
        {
            using (var nexoContext = new NEXOContext())
            {
                return await nexoContext.vuc_activity_types.AsNoTracking().FirstOrDefaultAsync(x => x.Active && x.Id == id);
            }
        }
        #endregion
    }
}