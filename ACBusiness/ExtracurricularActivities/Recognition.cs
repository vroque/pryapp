﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.VUC;
using ACTools.Mail;

namespace ACBusiness.ExtracurricularActivities
{
    public class Recognition
    {
        #region Properties
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Fecha del reconocimiento
        /// </summary>
        public DateTime? RecognitionDate { get; set; }

        /// <summary>
        /// Razón del reconocimiento
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Descripción del reconocimiento
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Evidencia del reconocimiento
        /// </summary>
        public string PathEvidency { get; set; }

        /// <summary>
        /// Pidm del estudiante
        /// </summary>
        public List<decimal> PidmStudents { get; set; }

        /// <summary>
        /// Identificador de la tabla tipo de convalidación
        /// </summary>
        public int IdConvalidationType { get; set; }

        /// <summary>
        /// Código de ruta de reconocimiento
        /// </summary>
        public string PathCode { get; set; }

        /// <summary>
        /// Cantidad de créditos de reconocimiento
        /// </summary>
        public byte Credit { get; set; }
        #endregion

        #region Implicit operator
        public static implicit operator VUCRecognition(Recognition model)
        {
            if (model == null) return null;
            return new VUCRecognition
            {
                Id = model.Id,
                RecognitionDate = model.RecognitionDate,
                Reason = model.Reason,
                Description = model.Description,
                PathEvidency = model.PathEvidency,
                IdConvalidationType = model.IdConvalidationType,
                Credit = model.Credit,
                CreatedAt = DateTime.Now,
                Active = true
            };
        }

        public static implicit operator Recognition(VUCRecognition model)
        {
            if (model == null) return null;
            return new Recognition
            {
                Id = model.Id,
                RecognitionDate = model.RecognitionDate,
                Reason = model.Reason,
                Description = model.Description,
                PathEvidency = model.PathEvidency,
                IdConvalidationType = model.IdConvalidationType,
                Credit = model.Credit
            };
        }
        #endregion

        #region Methods
        /// <summary>
        /// Registra un reconocimiento para uno o más estudiantes.
        /// </summary>
        /// <param name="model">Modelo del estudiante</param>
        /// <returns></returns>
        public static async Task<Recognition> Create(Recognition model)
        {
            // Validar que se envien todos los campos obligatorios, si no retorna null
            if (model.IdConvalidationType < 1 || string.IsNullOrEmpty(model.PathEvidency) 
                || model.PidmStudents.Count < 1 || string.IsNullOrEmpty(model.Reason) 
                || model.RecognitionDate == DateTime.MinValue)
            {
                return null;
            }

            // Validar que se envia créditos (1 o 2 como máximo a petición del área usuaria) si escogió tipo de 
            // acreditación extracurricular
            if (model.IdConvalidationType == 1 && model.Credit != 1 && model.Credit != 2)
            {
                return null;
            }

            using (var nexoContext = new NEXOContext())
            {
                using (var transaction = nexoContext.Database.BeginTransaction())
                {
                    // Si el tipo de acreditación es 2 (Proyección social) se ignora los créditos.
                    // La cantidad de créditos solo aplica a los extracurriculares
                    model.Credit = model.IdConvalidationType == 2 ? (byte)0 : model.Credit;

                    var recognition = nexoContext.vuc_recognition.Add(model);
                    var success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    foreach (var pidm in model.PidmStudents)
                    {
                        nexoContext.vuc_recognition_student.Add(new VUCRecognitionStudent
                        {
                            IdRecognition = recognition.Id,
                            PidmStudent = pidm,
                            Active = true,
                            CreatedAt = DateTime.Now,
                        });
                    }
                    success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    transaction.Commit();
                }
            }

            // Envio de correos para los estudiantes con reconocimientos
            foreach (var pidm in model.PidmStudents)
            {
                var student = Student.GetStudent(pidm);
                var mail = new MailSender();

                if (student != null)
                {
                    try
                    {
                        mail.compose("accredited", new
                        {
                            firstName = student.first_name,
                            isSocialProject = model.IdConvalidationType == 2,
                            activityTypeName = model.Reason,
                            activityName = model.Reason,
                            creditsNumber = model.Credit,
                            creditsDescription = model.Credit == 1 ? "crédito" : "créditos",
                            extraCurricularDescription = model.Credit == 1 ? "extra curricular" : "extra curriculares",
                        }).destination($"{student.id}@continental.edu.pe", "Tu participación fue acreditada").send();
                    }
                    catch { }
                }
            }

            return model;
        }
        #endregion
    }
}
