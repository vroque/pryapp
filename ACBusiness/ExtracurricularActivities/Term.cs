﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;

namespace ACBusiness.ExtracurricularActivities
{
    public class Term
    {
        #region Properties
        /// <summary>
        /// Valor del periodo academico (Ejem: 201910)
        /// </summary>
        public string TermDescription { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Obtiene los periodos académicos vigentes a la fecha
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<Term>> GetTerms()
        {
            using (var nexoContext = new NEXOContext())
            {
                return await nexoContext.cau_tem_documents.AsNoTracking()
                        .Where(x => DbFunctions.TruncateTime(x.startDate) <= DbFunctions.TruncateTime(DateTime.Now))
                                    .Select(x => new Term { TermDescription = x.term })
                                    .Distinct().OrderByDescending(x => x.TermDescription).ToListAsync();
            }
        }
        #endregion
    }
}
