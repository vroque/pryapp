﻿using System.Collections.Generic;
using System.Data.Entity;
using ACTools.Configuration;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using _vuc = ACTools.Constants.ExtracurricularActivitiesConstants;

namespace ACBusiness.ExtracurricularActivities
{
    public class StudentSearch
    {
        #region Properties
        /// <summary>
        /// IDAlumno
        /// </summary>
        public string IdAlumno { get; set; }

        /// <summary>
        /// Dni del estudiante
        /// </summary>
        public string Dni { get; set; }

        /// <summary>
        /// Nombre completo del estudiante
        /// </summary>
        public string NomCompleto { get; set; }

        /// <summary>
        /// IDPersonaN de BDUCCI
        /// </summary>
        public decimal IdPerson { get; set; }

        /// <summary>
        /// Lista de reconocimientos
        /// </summary>
        public List<Recognition> Recognitions { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Busca a estudiantes por DNIs separados por un espacio " "
        /// </summary>
        /// <param name="dnis">arreglo de dni's del estudiante</param>
        public static async Task<IEnumerable<StudentSearch>> GetStudentsByDni(string[] dnis)
        {
            List<string> checkedDnis = new List<string>();

            foreach (var dni in dnis)
            {
                if (dni.Length == 8) checkedDnis.Add(dni);
            }
            // hace un recorrido por cada dni
            List<StudentSearch> students = new List<StudentSearch>();

            using (var dboContext = new DBOContext())
            {
                foreach (var elemento in checkedDnis)
                {
                    var data = await dboContext.view_alumno_basico.FirstOrDefaultAsync(x => x.DNI == elemento);

                    if (data == null) continue;

                    students.Add(new StudentSearch
                    {
                        Dni = data.DNI,
                        IdAlumno = data.IDAlumno,
                        NomCompleto = data.NomCompleto,
                        IdPerson = data.IDPersonaN
                    });
                }
            }

            using (var nexoContext = new NEXOContext())
            {
                // Obtener los reconocimientos del estudiante
                for (int i = 0; i < students.Count; i++)
                {
                    decimal pidm = students[i].IdPerson;
                    students[i].Recognitions = nexoContext.vuc_recognition_student
                        .Join(nexoContext.vuc_recognition,
                             (rs) => rs.IdRecognition,
                             (r) => r.Id,
                             (rs, r) => new { rs, r })
                        .Where(x => x.rs.PidmStudent == pidm)
                        .Select(x => new Recognition
                        {
                            Id = x.r.Id,
                            Reason = x.r.Reason,
                            RecognitionDate = x.r.RecognitionDate,
                            Credit = x.r.Credit,
                            Description = x.r.Description,
                            IdConvalidationType = x.r.IdConvalidationType,
                            PathEvidency = x.r.PathEvidency
                        }).ToList();
                    students[i].Recognitions.ForEach(x =>
                    {
                        if (!string.IsNullOrEmpty(x.PathEvidency))
                        {
                            x.PathEvidency = $"{AppSettings.files[_vuc.URI_VUC_FILE]}{x.PathEvidency.Split('.')[0]}/";
                        }
                    });
                }
            }
            return students;
        }
        #endregion
    }
}