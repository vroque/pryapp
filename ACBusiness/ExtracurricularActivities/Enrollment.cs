﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.NEXO;

namespace ACBusiness.ExtracurricularActivities
{
    public class Enrollment
    {
        #region Methods
        /// <summary>
        /// Retorna la cantidad de inscritos en actividades extracurriculares en el periodo actual.
        /// </summary>
        /// <param name="profile">Perfíl del estudiante</param>
        /// <returns></returns>
        public static async Task<IEnumerable<EnrolledProgramActivity>> GetEnrollments(AcademicProfile profile)
        {
            var term = new ACTerm().getCurrent(profile.campus.id, profile.department);
            using (var nexoContext = new NEXOContext())
            {
                return await nexoContext.vuc_enrolled_activity.Where(enroll => enroll.Active 
                                        && enroll.Term == term.term && enroll.PidmStudent == profile.person_id)
                    .Select(enrolled => new EnrolledProgramActivity
                    {
                        Id = enrolled.Id,
                        IdProgramActivity = enrolled.IdProgramActivity,
                        Cycle = enrolled.Cycle,
                        Curriculum = enrolled.Curriculum,
                        IdCampus = enrolled.IdCampus,
                        IdModality = enrolled.IdModality,
                        DateEnrolled = enrolled.DateEnrolled,
                        Eap = enrolled.EAP,
                        Confirmed = enrolled.Confirmed,
                        Active = enrolled.Active,
                        CreatedAt = enrolled.CreatedAt,
                        UpdatedAt = enrolled.UpdatedAt,
                        PidmStudent = enrolled.PidmStudent,
                        DateConfirmed = enrolled.DateConfirmed,
                        IdConcept = enrolled.IDConcepto,
                        ProgrammingActivity = nexoContext.vuc_programming_activity
                    .Where(progr => progr.Id == enrolled.IdProgramActivity)
                    .Select(progr => new ProgrammingActivity
                    {
                        IdActivity = progr.IdActivity,
                        Activity = nexoContext.vuc_activities
                        .Where((act => act.Id == progr.IdActivity))
                        .Select(act => new Activity
                        {
                            Id = act.Id,
                            Name = act.Name,
                            ShortName = act.ShortName
                        }).FirstOrDefault()
                    }).FirstOrDefault()
                    }).ToListAsync();
            }
        }
        #endregion
    }
}
