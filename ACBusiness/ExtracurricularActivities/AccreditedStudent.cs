﻿using System.Collections.Generic;
using System.Linq;
using ACBusiness.Academic;
using ACPermanence.Contexts.BANNER;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;

namespace ACBusiness.ExtracurricularActivities
{
    public class AccreditedStudent
    {
        #region Properties      
        /// <summary>
        /// Pidm del estudiante
        /// </summary>
        public decimal PidmStudent { get; set; }

        /// <summary>
        /// Nombre de la actividad - taller
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// Nota final de la actividad - taller
        /// </summary>
        public string FinalScore { get; set; }

        /// <summary>
        /// Porcentaje de asistencia de la actividad - taller
        /// </summary>
        public string PercentageAssistance { get; set; }

        /// <summary>
        /// Nombre completo del estudiante
        /// </summary>
        public string FullNameStudent { get; set; }

        /// <summary>
        /// DNI del estudiante
        /// </summary>
        public string DocumentNumber { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Método para obtener la asistencia y nota final de un estudiante.
        /// </summary>
        /// <param name="pidm">pidm del estudiante</param>
        /// <param name="nrc">nrc del curso</param>
        /// <param name="term">periodo académico</param>
        /// <returns></returns>
        public static IEnumerable<AccreditedStudent> GetStudentAssistanceFinalScoreCourse(decimal pidm, string nrc,
            string term)
        {
            var student = Student.GetStudent(pidm);

            using (var bnContext = new BNContext())
            {
                List<AccreditedStudent> studentByAccredited = new List<AccreditedStudent>();

                // Lista de cursos matriculados segun el periodo
                List<Course> courses = EnrollmentsByTerm(pidm, term);

                courses = courses.Where(x => x.subject == "ASEX" && x.nrc == nrc).ToList();

                // llamamos al sp donde devuelve la inasistencia por alumno
                List<P_INASISTENCIAXALUMNO> absences = bnContext.p_inasistencia_x_alumno(pidm, term).ToList();

                // recorremos la lista de nombres de los cursos
                foreach (Course course in courses)
                {
                    studentByAccredited.Add(new AccreditedStudent
                    {
                        CourseName = course.name,
                        FullNameStudent = student?.full_name,
                        FinalScore = course.score,
                        PercentageAssistance = absences
                            .FirstOrDefault(x => x.nrc == course.nrc)?.percentageinasistence ?? "0",
                        PidmStudent = pidm,
                        DocumentNumber = student?.id
                    });
                }

                return studentByAccredited;
            }
        }

        /// <summary>
        /// Obtiene las matrículas de un estudiante mediante su pidm y el periodo 
        /// </summary>
        /// <param name="pidm">pidm del estudiante</param>
        /// <param name="term">pediodo académico</param>
        /// <returns></returns>
        public static List<Course> EnrollmentsByTerm(decimal pidm, string term)
        {
            using (var bnContext = new BNContext())
            {
                List<Course> list = bnContext.tbl_SFRSTCR.Join(
                            bnContext.tbl_SSBSECT,
                            mat => new { crn = mat.SFRSTCR_CRN, term = mat.SFRSTCR_TERM_CODE },
                            cur => new { crn = cur.SSBSECT_CRN, term = cur.SSBSECT_TERM_CODE },
                            (mat, cur) => new {mat, cur }
                        ).Join(
                            bnContext.tbl_SCRSYLN,
                            mc => new { subj = mc.cur.SSBSECT_SUBJ_CODE, numb = mc.cur.SSBSECT_CRSE_NUMB },
                            des => new { subj = des.SCRSYLN_SUBJ_CODE, numb = des.SCRSYLN_CRSE_NUMB },
                            (mc, des) => new { mat = mc.mat, cur = mc.cur, des = des }
                        )
                        .Join(
                            bnContext.tbl_SORLCUR,
                            d => new { pidm = d.mat.SFRSTCR_PIDM },
                            r => new { pidm = r.SORLCUR_PIDM },
                            (d, r) => new { mat = d.mat, cur = d.cur, des = d.des, r = r }
                        ).Where(f =>
                            f.mat.SFRSTCR_PIDM == pidm
                            && f.mat.SFRSTCR_TERM_CODE == term
                            && (f.mat.SFRSTCR_RSTS_CODE == "ASEX" || f.mat.SFRSTCR_RSTS_CODE == "RE")
                            && f.r.SORLCUR_TERM_CODE_END == null
                            && f.r.SORLCUR_LMOD_CODE == "LEARNER"
                        ).ToList()
                        .Select(f => new Course
                        {
                            id = $"{f.cur.SSBSECT_SUBJ_CODE}{f.cur.SSBSECT_CRSE_NUMB}",
                            name = f.des.SCRSYLN_LONG_COURSE_TITLE,
                            subject = f.cur.SSBSECT_SUBJ_CODE,
                            nrc = f.mat.SFRSTCR_CRN,
                            score = f.mat.SFRSTCR_GRDE_CODE ?? "-",
                        }).ToList();
                return list;
            }
        }
        #endregion methods
    }
}
