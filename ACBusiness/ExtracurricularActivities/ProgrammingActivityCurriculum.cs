﻿namespace ACBusiness.ExtracurricularActivities
{
    public class ProgrammingActivityCurriculum
    {
        #region Properties
        /// <summary>
        /// Id de la programación de una actividad
        /// </summary>
        public int IdProgrammingActivity { get; set; }

        /// <summary>
        /// Id del tipo de plan de estudios
        /// </summary>
        public string IdCurriculum { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }
        #endregion
    }
}
