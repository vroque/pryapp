﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.VUC;
using ACTools.Configuration;
using _vuc = ACTools.Constants.ExtracurricularActivitiesConstants;

namespace ACBusiness.ExtracurricularActivities
{
    public class Axi
    {
        #region Properties
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del eje
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripción del eje
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ruta de la imagen almacenada en el servidor de aplicaciones
        /// </summary>
        public string ImagePath { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }

        /// <summary>
        /// Imagen en base 64 que se envia desde el cliente
        /// </summary>
        public string ImageEncode { get; set; }

        /// <summary>
        /// Actividades del eje
        /// </summary>
        public IEnumerable<Activity> Activities { get; set; }
        #endregion

        #region Implicit operators
        public static implicit operator VUCAxi(Axi model)
        {
            if (model == null) return null;

            return new VUCAxi
            {
                Id = model.Id,
                Description = model.Description,
                Name = model.Name,
                ImagePath = model.ImagePath,
                Active = true,
                CreatedAt = DateTime.Now,
                UpdatedAt = null
            };
        }

        public static implicit operator Axi(VUCAxi model)
        {
            if (model == null) return null;

            return new Axi
            {
                Id = model.Id,
                Description = model.Description,
                Name = model.Name,
                ImagePath = model.ImagePath,
                Active = model.Active,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt,
                Activities = null
            };
        }
        #endregion

        #region Methods
        /// <summary>
        /// Obtiene los ejes con sus respectivas actividades.
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<Axi>> GetAll()
        {
            using (var nexoContext = new NEXOContext())
            {
                var axis = await nexoContext.vuc_axis.Where(axi => axi.Active)
                             .Select(axi => new Axi
                             {
                                Id = axi.Id,
                                Name = axi.Name,
                                Description = axi.Description,
                                ImagePath = axi.ImagePath,
                                Activities = nexoContext.vuc_activities
                                            .Where(activity => activity.Active && activity.IdAxi == axi.Id)
                                            .Select(activity => new Activity
                                            {
                                                Id = activity.Id,
                                                ShortName = activity.ShortName,
                                                Name = activity.Name,
                                                Visible = activity.Visible,
                                                IsReplicated = activity.IsReplicated,
                                                BannerCode = activity.BannerCode,
                                                IdActivityType = activity.IdActivityType,
                                                ActivityType = nexoContext.vuc_activity_types
                                                              .Where(actType => actType.Active 
                                                                     && actType.Id == activity.IdActivityType)
                                                              .Select(actType => new ActivityType
                                                              {
                                                                 Id = actType.Id,
                                                                 Name = actType.Name
                                                              }).FirstOrDefault()
                                            }).ToList()
                             }).ToListAsync();

                axis.ForEach(x =>
                {
                    if (!string.IsNullOrEmpty(x.ImagePath))
                    {
                        x.ImagePath = $"{AppSettings.images[_vuc.URI_FRONT_VUC_IMAGE_AXI]}{x.ImagePath.Split('.')[0]}/";
                    }
                });
                return axis;
            }
        }

        /// <summary>
        /// Obtiene un eje por su ID.
        /// </summary>
        /// <param name="id">Id del eje</param>
        /// <returns></returns>
        public static async Task<Axi> GetById(int id)
        {
            using (var nexoContext = new NEXOContext())
            {
                Axi axi = await nexoContext.vuc_axis.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id && x.Active);
                if (!string.IsNullOrEmpty(axi.ImagePath))
                {
                    axi.ImagePath = $"{AppSettings.images[_vuc.URI_VUC_IMAGE_AXI]}{axi.ImagePath.Split('.')[0]}/";
                }
                return axi;
            }
        }

        /// <summary>
        /// Obtiene un eje por su nombre (para realizar validaciones de no repetir un eje
        /// con el mismo nombre).
        /// </summary>
        /// <param name="name">Nombre del eje</param>
        /// <returns></returns>
        public static async Task<Axi> GetByName(string name)
        {
            using (var nexoContext = new NEXOContext())
            {
                return await nexoContext.vuc_axis.FirstOrDefaultAsync(activity => activity.Name == name && activity.Active);
            }
        }

        /// <summary>
        /// Registra un nuevo eje.
        /// </summary>
        /// <param name="axi">Model Axi (Eje)</param>
        /// <returns></returns>
        public static async Task<Axi> Create(Axi axi)
        {
            using (var nexoContext = new NEXOContext())
            {
                nexoContext.vuc_axis.Add(axi);
                var success = (await nexoContext.SaveChangesAsync()) > 0;
                return success ? axi : null;
            }
        }

        /// <summary>
        /// Actualiza un eje.
        /// </summary>
        /// <param name="model">Modelo del eje (Axi)</param>
        /// <returns></returns>
        public static async Task<Axi> Update(Axi model)
        {
            using (var nexoContext = new NEXOContext())
            {
                var axi = await nexoContext.vuc_axis.FindAsync(model.Id);
                if (axi == null) return null;
               
                axi.Name = model.Name;
                axi.Description = model.Description;
                axi.ImagePath = string.IsNullOrEmpty(model.ImagePath) ? axi.ImagePath : model.ImagePath;
                axi.UpdatedAt = DateTime.Now;
                nexoContext.Entry(axi).Property(a => a.CreatedAt).IsModified = false;

                var success = (await nexoContext.SaveChangesAsync()) > 0;
                return success ? model : null;
            }
        }

        /// <summary>
        /// Realiza un eliminado lógico (cambia su estado Active a 0).
        /// </summary>
        /// <param name="id">Id del eje</param>
        /// <returns></returns>
        public static async Task<Axi> Delete(int id)
        {
            using (var nexoContext = new NEXOContext())
            {
                var axi = await nexoContext.vuc_axis.FindAsync(id);
                if (axi == null) return null;

                axi.UpdatedAt = DateTime.Now;
                axi.Active = false;
                nexoContext.Entry(axi).Property(a => a.UpdatedAt).IsModified = true;
                nexoContext.Entry(axi).Property(a => a.Active).IsModified = true;

                var success = (await nexoContext.SaveChangesAsync()) > 0;
                return success ? axi : null;
            }
        }
        #endregion
    }
}
