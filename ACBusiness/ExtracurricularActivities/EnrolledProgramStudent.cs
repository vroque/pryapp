﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Personal;
using ACPermanence.Contexts.NEXO;

namespace ACBusiness.ExtracurricularActivities
{
    public class EnrolledProgramStudent
    {
        #region properties
        /// <summary>
        /// Tipo de actividad
        /// </summary>
        public string ActivityType { get; set; }

        /// <summary>
        /// Nombre de la actividad
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Fecha de inicio de la actividad
        /// </summary>
        public DateTime StartDateActivity { get; set; }

        /// <summary>
        /// Fecha de fin de la actividad
        /// </summary>
        public DateTime EndDateActivity { get; set; }

        /// <summary>
        /// Identificador de la tabla campus
        /// </summary>
        public string IdCampus { get; set; }

        /// <summary>
        /// DNI del estudiante
        /// </summary>
        public string DocumentNumber { get; set; }

        /// <summary>
        /// Nombre completo del estudiante
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Carrera del estudiante
        /// </summary>
        public string Eap { get; set; }

        /// <summary>
        /// Ciclo del estudiante
        /// </summary>
        public int Cycle { get; set; }

        /// <summary>
        /// Plan de estudios
        /// </summary>
        public string Curriculum { get; set; }

        /// <summary>
        /// Estado de confirmación
        /// </summary>
        public bool Confirmed { get; set; }

        /// <summary>
        /// Pidm de estudiante
        /// </summary>
        public decimal PidmStudent { get; set; }

        /// <summary>
        /// IDAlumno
        /// </summary>
        public string IdAlumno { get; set; }

        /// <summary>
        /// Fecha de confirmación
        /// </summary>
        public DateTime? DateConfirmed { get; set; }

        /// <summary>
        /// Inicio del periodo
        /// </summary>
        public string StartPeriod { get; set; }

        /// <summary>
        /// Nrc del curso
        /// </summary>
        public string Nrc { get; set; }

        public string Phone { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Retorna a los estudiantes inscritos en la actividade programa/taller.
        /// </summary>
        /// <param name="idProgramming">Id de la programación de actividad</param>
        /// <param name="idActivityType">Id del tipo de actividad</param>
        /// <returns></returns>
        public static async Task<IEnumerable<EnrolledProgramStudent>> GetByEnrolled(int idProgramming, 
            int idActivityType)
        {
            using (var nexoContext = new NEXOContext())
            {
                var enrollments = await nexoContext.vuc_activity_types
                    .Join(nexoContext.vuc_activities, activityType => activityType.Id, act => act.IdActivityType,
                        (activityType, act) => new { activityType, act })
                    .Join(nexoContext.vuc_programming_activity, act => new { id = act.act.Id },
                        programming => new { id = programming.IdActivity },
                        (act, programming) => new { act, programming })
                    .Join(nexoContext.vuc_enrolled_activity, programming => new { id = programming.programming.Id },
                        enrolled => new { id = enrolled.IdProgramActivity }, 
                        (programming, enrolled) => new { programming, enrolled })
                    .Where(x => x.programming.programming.Id == idProgramming 
                        && x.programming.act.activityType.Id == idActivityType && x.programming.programming.Active 
                        && x.enrolled.Active)
                    .Select(model => new EnrolledProgramStudent
                    {
                        ActivityType = model.programming.act.activityType.Name,
                        PidmStudent = model.enrolled.PidmStudent,
                        Name = model.programming.act.act.Name,
                        Confirmed = model.enrolled.Confirmed,
                        IdCampus = model.enrolled.IdCampus,
                        StartDateActivity = model.programming.programming.StartDateActivity,
                        EndDateActivity = model.programming.programming.EndDateActivity,
                        Eap = model.enrolled.EAP,
                        DateConfirmed = model.enrolled.DateConfirmed,
                        Curriculum = model.enrolled.Curriculum,
                        Cycle = model.enrolled.Cycle,
                        StartPeriod = model.programming.programming.StartPeriod,
                        Nrc = model.programming.programming.nrc
                    }).ToListAsync();

                enrollments.ForEach(x =>
                {
                    var student = Student.GetStudent(x.PidmStudent);
                    if (student != null)
                    {
                        x.IdAlumno = student.IdAlumno;
                        x.DocumentNumber = student.id;
                        x.FullName = student.full_name;
                        x.Phone = Person.GetPhone(student.id);
                    }
                });

                return enrollments;
            }
        }

        /// <summary>
        /// Retorna a los estudiantes matriculados en la actividad Taller.
        /// </summary>
        /// <param name="term">Periodo académico</param>
        /// <returns></returns>
        public static async Task<IEnumerable<EnrolledProgramStudent>> GetByTerm(string term)
        {
            using (var nexoContext = new NEXOContext())
            {
                var data = await nexoContext.vuc_activity_types
                .Join(nexoContext.vuc_activities, activityType => activityType.Id, act => act.IdActivityType,
                     (activityType, act) => new { activityType, act })
                .Join(nexoContext.vuc_programming_activity, act => new { id = act.act.Id },
                     programming => new { id = programming.IdActivity },
                     (act, programming) => new { act, programming })
                .Join(nexoContext.vuc_enrolled_activity, programming => new { id = programming.programming.Id },
                     enrolled => new { id = enrolled.IdProgramActivity },
                     (programming, enrolled) => new { programming, enrolled })
                .Where(x => x.programming.act.activityType.Id == 2 && x.programming.programming.Active 
                     && x.enrolled.Active).Where(x => x.enrolled.Term == term)
                .Select(model => new EnrolledProgramStudent
                {
                    StartPeriod = model.programming.programming.StartPeriod,
                    PidmStudent = model.enrolled.PidmStudent,
                    Nrc = model.programming.programming.nrc,
                }).ToListAsync();

                data.ForEach(x =>
                {
                    var student = Student.GetStudent(x.PidmStudent);
                    if (student != null)
                    {
                        x.IdAlumno = student.IdAlumno;
                        x.DocumentNumber = student.id;
                    }
                });

                return data;
            }
        }
        #endregion
    }
}