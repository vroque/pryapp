﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter.Documents;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BANNER;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.BANNER.SATURN;
using ACPermanence.DataBases.NEXO.VUC;
using _acadConst = ACTools.Constants.AcademicConstants;

namespace ACBusiness.ExtracurricularActivities
{
    public class Tracing
    {
        #region Properties
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la tabla ProgrammingActivity
        /// </summary>
        public int IdProgrammingActivity { get; set; }

        /// <summary>
        /// Pidm del estudiante
        /// </summary>
        public decimal PidmStudent { get; set; }

        /// <summary>
        /// Hora
        /// </summary>
        public short Hours { get; set; }

        /// <summary>
        /// Nota
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// Observación
        /// </summary>
        public string Observation { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }

        #endregion

        #region Implicit Operator
        public static implicit operator VUCTracingProgram(Tracing model)
        {
            if (model == null) return null;
            return new VUCTracingProgram
            {
                Id = model.Id,
                IdProgrammingActivity = model.IdProgrammingActivity,
                PidmStudent = model.PidmStudent,
                Score = model.Score,
                Hours = model.Hours,
                Observation = model.Observation,
                Active = model.Active,
                CreatedAt = DateTime.Now,
                UpdatedAt = model.UpdatedAt
            };
        }

        public static implicit operator Tracing(VUCTracingProgram model)
        {
            if (model == null) return null;
            return new Tracing
            {
                Id = model.Id,
                IdProgrammingActivity = model.IdProgrammingActivity,
                PidmStudent = model.PidmStudent,
                Score = model.Score,
                Hours = model.Hours,
                Observation = model.Observation,
                Active = model.Active,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt
            };
        }
        #endregion

        #region Methods
        /// <summary>
        /// Registra un seguimiento para un programa.
        /// </summary>
        /// <param name="model">Lista de reconocimiento a insertar</param>
        /// <returns></returns>
        public static async Task<List<Tracing>> Insert(List<Tracing> model)
        {
            using (var nexoContext = new NEXOContext())
            {
                var tracing = model.Select<Tracing, VUCTracingProgram>(x => x).ToList();
                nexoContext.vuc_tracing_program.AddRange(tracing);
                var success = (await nexoContext.SaveChangesAsync()) > 0;
                return success ? model : null;
            }
        }

        /// <summary>
        /// Retorna la información para generar el reporte de programas.
        /// </summary>
        /// <param name="idProgrammingActivity">Id de la programación de la actividad</param>
        /// <returns></returns>
        public static async Task<IEnumerable<TracingReport>> GetReport(int idProgrammingActivity)
        {
            List<SZVMAJR> programs;
            using (var bnContext = new BNContext())
            {
                programs = bnContext.tbl_SZVMAJR.ToList();
            }

            using (var nexoContext = new NEXOContext())
            {
                var data = await nexoContext.vuc_tracing_program.AsNoTracking()
                    .Join(nexoContext.vuc_enrolled_activity,
                      tracing => new { Id = tracing.IdProgrammingActivity, Pidm = tracing.PidmStudent },
                      enrolled => new { Id = enrolled.IdProgramActivity, Pidm = enrolled.PidmStudent },
                      (tracing, enrolled) => new { tracing, enrolled })
                    .Join(nexoContext.dim_persons,
                      tracing => tracing.tracing.PidmStudent,
                      person => person.pidm,
                      (enroll, person) => new { enroll, person })
                    .Join(nexoContext.vuc_programming_activity,
                      enrolled => enrolled.enroll.enrolled.IdProgramActivity,
                      program => program.Id,
                      (enrolled, program) => new { data = enrolled, programming = program })
                    .Join(nexoContext.vuc_activities,
                       a => a.programming.IdActivity,
                       b => b.Id,
                       (a, b) => new { enroll = a, activity = b })
                    .Where(x => x.enroll.data.enroll.enrolled.IdProgramActivity == idProgrammingActivity &&
                               x.enroll.data.enroll.tracing.Active)
                    .Select(x => new TracingReport
                    {
                        Eap = x.enroll.data.enroll.enrolled.EAP,
                        ActivityName = x.activity.Name,
                        FullName = x.enroll.data.person.fullName,
                        Program = x.enroll.data.enroll.enrolled.EAP,
                        Hours = x.enroll.data.enroll.tracing.Hours,
                        Score = x.enroll.data.enroll.tracing.Score,
                        Observation = x.enroll.data.enroll.tracing.Observation,
                        CreatedAt = x.enroll.data.enroll.tracing.CreatedAt,
                        Dni = x.enroll.data.person.documentNumber,
                        Pidm = x.enroll.data.person.pidm,
                        State = x.enroll.data.enroll.tracing.Score >= 14 ? "Aprobado" : "Desaprobado",
                    }).ToListAsync();

                data.ForEach(x => x.Eap = programs.FirstOrDefault(y => y.SZVMAJR_CODE == x.Eap)?.SZVMAJR_DESCRIPTION);
                return data;
            }
        }

        /// <summary>
        /// Genera el request y el PDF para el certificado de culminación y aprobación de la actividad.
        /// </summary>
        /// <param name="pidm">Pidm del estudiante</param>
        /// <param name="user">Usuario del backoffice</param>
        /// <param name="idProgramActivity">Id de la programcación de la actividad</param>
        /// <returns></returns>
        public static async Task<string> GetCertificatePath(decimal pidm, string user, int idProgramActivity)
        {
            string activityName, activityTerm;
            int idActivityType;
            byte credits;
            using (var nexoContext = new NEXOContext())
            {
                var activity = await nexoContext.vuc_programming_activity
                    .Join(nexoContext.vuc_enrolled_activity, programming => programming.Id, 
                        enrolled => enrolled.IdProgramActivity, (programming, enrolled) => new { programming, enrolled })
                    .Join(nexoContext.vuc_activities, programming => programming.programming.IdActivity,
                        activ => activ.Id, (programming, activ) => new { programming, activ })
                    .FirstOrDefaultAsync(x => x.programming.enrolled.PidmStudent == pidm
                        && x.programming.enrolled.IdProgramActivity == idProgramActivity);

                if (activity == null) return null;

                activityName = activity.activ.ShortName;
                activityTerm = activity.programming.programming.StartPeriod;
                idActivityType = activity.activ.IdActivityType;
                credits = activity.activ.Credits;
            }

            var student = new Student(pidm);

            AcademicProfile academicProfile = new AcademicProfile
            {
                div = _acadConst.DEFAULT_DIV,
                campus = Campus.get(student.profiles.LastOrDefault()?.campus.id),
                department = Department.toBanner(student.profiles.LastOrDefault()?.department),
                program = Program.get(student.profiles.LastOrDefault()?.program.id),
                college = College.getByProgramID(student.profiles.LastOrDefault()?.program.id)
            };

            var activityProgram = new ActivityProgramCertificate(academicProfile);

            var data = await activityProgram.CreateRequest(student, academicProfile, idProgramActivity, 
                $"Certificado de aprobación de actividad denominada {activityName}", user);

            return activityProgram.Generate(data, student, user, activityName, activityTerm, idActivityType, credits);
        }
        #endregion
    }
}
