﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.VUC;
using _acadeConst = ACTools.Constants.AcademicConstants;
using _learningConst = ACTools.Constants.LearningAssessmentConstants;

namespace ACBusiness.ExtracurricularActivities
{
    public class ProgrammingActivity
    {
        #region Properties
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la tabla actividad
        /// </summary>
        public int IdActivity { get; set; }

        /// <summary>
        /// Inicio de inscripción de la actividad
        /// </summary>
        public DateTime StartDateEnrollment { get; set; }

        /// <summary>
        /// Fin de inscripción de la actividad
        /// </summary>
        public DateTime EndDateEnrollment { get; set; }

        /// <summary>
        /// Inicio de la actividad
        /// </summary>
        public DateTime StartDateActivity { get; set; }

        /// <summary>
        /// Fin de la actividad
        /// </summary>
        public DateTime EndDateActivity { get; set; }

        /// <summary>
        /// Costo de la actividad
        /// </summary>
        public int Cost { get; set; }

        /// <summary>
        /// Moneda
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Perfil del estudiante
        /// </summary>
        public string StudentProfile { get; set; }

        /// <summary>
        /// Inicio del periodo de la actividad
        /// </summary>
        public string StartPeriod { get; set; }

        /// <summary>
        /// Fin del periodo de la actividad
        /// </summary>
        public string EndPeriod { get; set; }

        /// <summary>
        /// Identificador de la tabla tipo de horario
        /// </summary>
        public string IdScheduleType { get; set; }

        /// <summary>
        /// Descripción del horario
        /// </summary>
        public string ScheduleDescription { get; set; }

        /// <summary>
        /// Día de la sesión de la actividad
        /// </summary>
        public string SessionDay { get; set; }

        /// <summary>
        /// Hora de inicio de la actividad
        /// </summary>
        public string StartHour { get; set; }

        /// <summary>
        /// Hora de fin de la actividad
        /// </summary>
        public string EndHour { get; set; }

        /// <summary>
        /// Lugar donde se desarrollara la actividad
        /// </summary>
        public string SessionPlace { get; set; }

        /// <summary>
        /// Cantidad de vacantes
        /// </summary>
        public int QuantityVacancy { get; set; }

        /// <summary>
        /// Cantidad máxima de vacantes
        /// </summary>
        public int MaxQuantityVacancy { get; set; }

        /// <summary>
        /// DNI del docente
        /// </summary>
        public string DocumentTeacher { get; set; }

        /// <summary>
        /// Nombre del docente
        /// </summary>
        public string NameTeacher { get; set; }

        /// <summary>
        /// Identificador de la tabla campus
        /// </summary>
        public string IdCampus { get; set; }

        /// <summary>
        /// Identificador de la modalidad
        /// </summary>
        public string IdModality { get; set; }

        /// <summary>
        /// Estado de réplica
        /// </summary>
        public bool Replicated { get; set; }

        /// <summary>
        /// Código del NRC de la programación
        /// </summary>
        public string Nrc { get; set; }

        /// <summary>
        /// Estado de que se encuentre activo la programación
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha de creación del registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha de actualización de la actividad
        /// </summary>
        public DateTime? UpdatedAt { get; set; }

        // Campos de ayuda (no se envian a la BD)

        /// <summary>
        /// Estado para determinar si una programación (programas) ya tiene registrado seguimiento de sus estudiantes
        /// </summary>
        public bool HasTracing { get; set; }

        /// <summary>
        /// Estado para determinar si una programación (programas) ya tiene estudiantes seleccionados
        /// </summary>
        public bool HasSelected { get; set; }

        public Activity Activity { get; set; }
        public List<int> ProgrammingActCurriculumId { get; set; }
        public List<ProgrammingActivityCurriculum> ProgrammingActivityCurriculum { get; set; }
        #endregion

        #region Implicit methods
        public static implicit operator VUCProgrammingActivity(ProgrammingActivity model)
        {
            if (model == null) return null;

            return new VUCProgrammingActivity
            {
                Id = model.Id,
                IdActivity = model.IdActivity,
                StartDateEnrollment = model.StartDateEnrollment,
                EndDateEnrollment = model.EndDateEnrollment,
                StartDateActivity = model.StartDateActivity,
                EndDateActivity = model.EndDateActivity,
                Cost = model.Cost,
                Currency = model.Currency,
                StudentProfile = model.StudentProfile,
                StartPeriod = model.StartPeriod,
                EndPeriod = model.EndPeriod,
                IdScheduleType = model.IdScheduleType,
                ScheduleDescription = model.ScheduleDescription,
                SessionDay = model.SessionDay,
                StartHour = model.StartHour,
                EndHour = model.EndHour,
                SessionPlace = model.SessionPlace,
                QuantityVacancy = model.QuantityVacancy,
                MaxQuantityVacancy = model.MaxQuantityVacancy,
                DocumentTeacher = model.DocumentTeacher,
                NameTeacher = model.NameTeacher,
                nrc = model.Nrc,
                IdCampus = model.IdCampus,
                IdModality = model.IdModality,
                Replicated = model.Replicated,
                Active = true,
                CreatedAt = model.CreatedAt
            };
        }

        public static implicit operator ProgrammingActivity(VUCProgrammingActivity model)
        {
            if (model == null) return null;

            return new ProgrammingActivity
            {
                Id = model.Id,
                IdActivity = model.IdActivity,
                StartDateEnrollment = model.StartDateEnrollment,
                EndDateEnrollment = model.EndDateEnrollment,
                StartDateActivity = model.StartDateActivity,
                EndDateActivity = model.EndDateActivity,
                Cost = model.Cost,
                Currency = model.Currency,
                StudentProfile = model.StudentProfile,
                StartPeriod = model.StartPeriod,
                EndPeriod = model.EndPeriod,
                IdScheduleType = model.IdScheduleType,
                ScheduleDescription = model.ScheduleDescription,
                SessionDay = model.SessionDay,
                StartHour = model.StartHour,
                EndHour = model.EndHour,
                SessionPlace = model.SessionPlace,
                QuantityVacancy = model.QuantityVacancy,
                MaxQuantityVacancy = model.MaxQuantityVacancy,
                DocumentTeacher = model.DocumentTeacher,
                NameTeacher = model.NameTeacher,
                IdCampus = model.IdCampus,
                IdModality = model.IdModality,
                Replicated = model.Replicated,
                Active = true
            };
        }
        #endregion

        #region Methods
        /// <summary>
        /// Método para obtener las programaciones por periodo académico.
        /// </summary>
        /// <param name="term">periodo academico</param>
        /// <returns></returns>
        public static async Task<IEnumerable<ProgrammingActivity>> GetProgrammingActivityByTerm(string term)
        {
            using (var nexoContext = new NEXOContext())
            {
                return await nexoContext.vuc_programming_activity
                    .Join(nexoContext.vuc_activities,
                         programmingActivity => programmingActivity.IdActivity,
                         activity => activity.Id,
                         (programmingActivity, activity) => new { programmingActivity, activity })
                    .Where(programmingActivity => programmingActivity.programmingActivity.StartPeriod == term 
                                                  && programmingActivity.activity.IdActivityType == 2)
                    .Select(model => new ProgrammingActivity
                    {
                        Id = model.programmingActivity.Id,
                        IdActivity = model.programmingActivity.IdActivity,
                        StartDateEnrollment = model.programmingActivity.StartDateEnrollment,
                        EndDateEnrollment = model.programmingActivity.EndDateEnrollment,
                        StartDateActivity = model.programmingActivity.StartDateActivity,
                        EndDateActivity = model.programmingActivity.EndDateActivity,
                        Cost = model.programmingActivity.Cost,
                        Currency = model.programmingActivity.Currency,
                        StudentProfile = model.programmingActivity.StudentProfile,
                        StartPeriod = model.programmingActivity.StartPeriod,
                        EndPeriod = model.programmingActivity.EndPeriod,
                        IdScheduleType = model.programmingActivity.IdScheduleType,
                        ScheduleDescription = model.programmingActivity.ScheduleDescription,
                        SessionDay = model.programmingActivity.SessionDay,
                        StartHour = model.programmingActivity.StartHour,
                        EndHour = model.programmingActivity.EndHour,
                        SessionPlace = model.programmingActivity.SessionPlace,
                        QuantityVacancy = model.programmingActivity.QuantityVacancy,
                        MaxQuantityVacancy = model.programmingActivity.MaxQuantityVacancy,
                        DocumentTeacher = model.programmingActivity.DocumentTeacher,
                        IdCampus = model.programmingActivity.IdCampus,
                        IdModality = model.programmingActivity.IdModality,
                        Replicated = model.programmingActivity.Replicated,
                        Activity = nexoContext.vuc_activities.Where(x => x.Active && x.Id == model.activity.Id)
                        .Select(y => new Activity
                        {
                            Id = y.Id,
                            IdActivityType = y.IdActivityType,
                            Name = y.Name,
                            ShortName = y.ShortName
                        }).FirstOrDefault()
                    }).ToListAsync();
            }
        }

        /// <summary>
        /// Obtiene las programaciones de actividades en estado activo y sus tipos de planes correspondientes.
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<ProgrammingActivity>> GetAllAsync()
        {
            using (var nexoContext = new NEXOContext())
            {
                var programmingActivities = await nexoContext.vuc_programming_activity.Where(x => x.Active)
                    .OrderByDescending(orderPa => orderPa.Id)
                    .Select(programActivity => new ProgrammingActivity
                    {
                        Id = programActivity.Id,
                        IdActivity = programActivity.IdActivity,
                        StartDateEnrollment = programActivity.StartDateEnrollment,
                        EndDateEnrollment = programActivity.EndDateEnrollment,
                        StartDateActivity = programActivity.StartDateActivity,
                        EndDateActivity = programActivity.EndDateActivity,
                        Cost = programActivity.Cost,
                        Currency = programActivity.Currency,
                        StudentProfile = programActivity.StudentProfile,
                        StartPeriod = programActivity.StartPeriod,
                        EndPeriod = programActivity.EndPeriod,
                        IdScheduleType = programActivity.IdScheduleType,
                        ScheduleDescription = programActivity.ScheduleDescription,
                        SessionDay = programActivity.SessionDay,
                        StartHour = programActivity.StartHour,
                        EndHour = programActivity.EndHour,
                        SessionPlace = programActivity.SessionPlace,
                        QuantityVacancy = programActivity.QuantityVacancy,
                        MaxQuantityVacancy = programActivity.MaxQuantityVacancy,
                        DocumentTeacher = programActivity.DocumentTeacher,
                        NameTeacher = programActivity.NameTeacher,
                        IdCampus = nexoContext.dim_campus
                                .FirstOrDefault(campus => campus.campus == programActivity.IdCampus).campusDescription,
                        IdModality = nexoContext.dim_departaments
                                .FirstOrDefault(modality => modality.departament == programActivity.IdModality)
                                .departamentDescription,
                        Replicated = programActivity.Replicated,
                        Activity = nexoContext.vuc_activities
                            .Where(act => act.Id == programActivity.IdActivity && act.Active).ToList()
                        .Select(act => new Activity
                        {
                            Id = act.Id,
                            IdActivityType = act.IdActivityType,
                            Name = act.Name,
                            ShortName = act.ShortName,
                            ActivityType = nexoContext.vuc_activity_types
                                .Where(actType => actType.Id == act.IdActivityType && actType.Active).ToList()
                            .Select(actType => new ActivityType
                            {
                                Id = actType.Id,
                                Name = actType.Name,
                            }).FirstOrDefault(),
                        }).FirstOrDefault(),
                        ProgrammingActivityCurriculum = nexoContext.vuc_programming_activity_Curriculum
                        .Where(progAct => progAct.Active 
                            && progAct.IdProgrammingActivity == programActivity.Id)
                        .Join(nexoContext.vuc_view_curriculum, progAct => progAct.IdCurriculum,
                            curriculunProgAct => curriculunProgAct.Curriculum, 
                            (progAct, curriculunProgAct) => new { progAct, curriculunProgAct })
                        .Select(progAct => new ProgrammingActivityCurriculum
                        {
                            IdProgrammingActivity = programActivity.Id,
                            IdCurriculum = progAct.curriculunProgAct.Curriculum
                        }).ToList()
                    }).ToListAsync();

                return programmingActivities;
            }
        }

        /// <summary>
        /// Obtiene las programaciones de actividades
        /// </summary>
        /// <param name="term">Periodo academico</param>
        /// <returns></returns>
        public static async Task<IEnumerable<ProgrammingActivity>> GetProgrammingWorkshopActivity(string term)
        {
            List<ProgrammingActivity> programming;

            using (var nexoContext = new NEXOContext())
            {
                programming = await nexoContext.vuc_programming_activity.AsNoTracking()
                   .Where(programmingActivity => programmingActivity.Active && programmingActivity.StartPeriod == term)
                   .Join(nexoContext.vuc_activities,
                        programmingActivity => programmingActivity.IdActivity,
                        activity => activity.Id,
                        (programmingActivity, activity) => new { programmingActivity, activity })
                   .Where(programmingActivity => programmingActivity.activity.IdActivityType == 2
                        && programmingActivity.activity.Active)
                    .Select(model => new ProgrammingActivity
                    {
                        Id = model.programmingActivity.Id,
                        StartDateActivity = model.programmingActivity.StartDateActivity,
                        EndDateActivity = model.programmingActivity.EndDateActivity,
                        StartPeriod = model.programmingActivity.StartPeriod,
                        SessionDay = model.programmingActivity.SessionDay,
                        StartHour = model.programmingActivity.StartHour,
                        EndHour = model.programmingActivity.EndHour,
                        SessionPlace = model.programmingActivity.SessionPlace,
                        MaxQuantityVacancy = model.programmingActivity.MaxQuantityVacancy,
                        DocumentTeacher = model.programmingActivity.DocumentTeacher,
                        IdCampus = model.programmingActivity.IdCampus,
                        IdModality = model.programmingActivity.IdModality,
                        Replicated = model.programmingActivity.Replicated,
                        Nrc = model.programmingActivity.nrc,
                        Activity = nexoContext.vuc_activities.Where(x => x.Active && x.Id == model.activity.Id)
                       .Select(y => new Activity
                       {
                           Id = y.Id,
                           BannerCode = y.BannerCode,
                           Name = y.Name,
                           ShortName = y.ShortName,
                           Credits = y.Credits
                       }).FirstOrDefault()
                    }).ToListAsync();
            }
            using (var dboContext = new DBOContext())
            {
                programming.ForEach(x =>
                {
                    // Caso que indica que la actividad no tiene docente asignado
                    if (x.DocumentTeacher == null && x.NameTeacher == null)
                    {
                        x.NameTeacher = "No asignado";
                    }
                    // Caso que indica que una actividad tiene un docente
                    else if (x.DocumentTeacher != null && x.NameTeacher == null)
                    {
                        x.NameTeacher = dboContext.view_docentes.AsNoTracking()
                            .FirstOrDefault(teacherName => teacherName.DNI == x.DocumentTeacher)?.NomCompleto;
                    }
                    else if (x.DocumentTeacher == null && x.NameTeacher != null)
                    {
                        x.DocumentTeacher = "No asignado";
                    }
                });
            }
            return programming;
        }

        /// <summary>
        /// Obtiene las programaciones de actividades en estado activo y sus tipos de planes correspondientes.
        /// </summary>
        /// <param name="profile">Perfil del estudiante</param>
        /// <param name="criterion">Expresion LINQ</param>
        /// <returns></returns>
        public static IEnumerable<ProgrammingActivity> GetAll(AcademicProfile profile,
            Expression<Func<VUCProgrammingActivity, bool>> criterion)
        {
            var term = int.Parse(profile.term_catalg);
            var curriculum = term <= 201800 ? "2015" : "2018";

            using (var nexoContext = new NEXOContext())
            {
                return nexoContext.vuc_programming_activity.Where(criterion)
                    .Join(nexoContext.vuc_programming_activity_Curriculum, programActivity => programActivity.Id,
                        b => b.IdProgrammingActivity, (programActivity, b) => new { programActivity, b })
                    .Where(x => x.b.IdCurriculum == curriculum && x.b.Active)
                    .Select(x => new ProgrammingActivity
                    {
                        Id = x.programActivity.Id,
                        IdActivity = x.programActivity.IdActivity,
                        StartDateEnrollment = x.programActivity.StartDateEnrollment,
                        EndDateEnrollment = x.programActivity.EndDateEnrollment,
                        StartDateActivity = x.programActivity.StartDateActivity,
                        EndDateActivity = x.programActivity.EndDateActivity,
                        Cost = x.programActivity.Cost,
                        Currency = x.programActivity.Currency,
                        StudentProfile = x.programActivity.StudentProfile,
                        StartPeriod = x.programActivity.StartPeriod,
                        EndPeriod = x.programActivity.EndPeriod,
                        IdScheduleType = x.programActivity.IdScheduleType,
                        ScheduleDescription = x.programActivity.ScheduleDescription,
                        SessionDay = x.programActivity.SessionDay,
                        StartHour = x.programActivity.StartHour,
                        EndHour = x.programActivity.EndHour,
                        SessionPlace = x.programActivity.SessionPlace,
                        QuantityVacancy = x.programActivity.QuantityVacancy,
                        MaxQuantityVacancy = x.programActivity.MaxQuantityVacancy - nexoContext.vuc_enrolled_activity
                                                 .Count(y => y.IdProgramActivity == x.programActivity.Id && y.Active),
                        DocumentTeacher = x.programActivity.DocumentTeacher,
                        NameTeacher = x.programActivity.NameTeacher,
                        IdCampus = x.programActivity.IdCampus,
                        IdModality = x.programActivity.IdModality,
                        Replicated = x.programActivity.Replicated,
                        ProgrammingActivityCurriculum = nexoContext.vuc_programming_activity_Curriculum
                            .Where(progAct => progAct.Active && progAct.IdProgrammingActivity == x.programActivity.Id)
                            .Select(progAct => new ProgrammingActivityCurriculum
                            {
                              IdProgrammingActivity = x.programActivity.Id,
                              IdCurriculum = progAct.IdCurriculum
                            }).ToList()
                    }).ToList();
            }
        }

        /// <summary>
        /// Obtiene una programacion de una actividad mediante su Id.
        /// </summary>
        /// <param name="id">Id de la programación de las actividades</param>
        /// <returns></returns>
        public static async Task<ProgrammingActivity> GetById(int id)
        {
            using (var nexoContext = new NEXOContext())
            {
                
                ProgrammingActivity programmingActivity =
                 await nexoContext.vuc_programming_activity
                     .FirstOrDefaultAsync(programactivity => programactivity.Active && programactivity.Id == id);

                programmingActivity.Activity = nexoContext.vuc_activities
                        .FirstOrDefault(act => act.Id == programmingActivity.IdActivity && act.Active);

                programmingActivity.ProgrammingActivityCurriculum = nexoContext.vuc_programming_activity_Curriculum
                    .Where(x => x.IdProgrammingActivity == id && x.Active)
                    .Select(x => new ProgrammingActivityCurriculum
                    {
                        IdCurriculum = x.IdCurriculum,
                        IdProgrammingActivity = x.IdProgrammingActivity
                    }).ToList();

                return programmingActivity;
            }
        }

        /// <summary>
        /// Registra una nueva programacioón de una determinada actividad.
        /// </summary>
        /// <param name="model">Instancia de la clase de programación de actividades</param>
        /// <returns></returns>
        public static async Task<ProgrammingActivity> Create(ProgrammingActivity model)
        {
            bool success;
            DateTime currentDate = DateTime.Now;
            using (var nexoContext = new NEXOContext())
            {
                using (var transaction = nexoContext.Database.BeginTransaction())
                {
                    model.CreatedAt = currentDate;
                    model.Active = true;
                    if (model.IdModality == _acadeConst.DEPA_VIR)
                    {
                        model.IdCampus = _learningConst.CAMPUS_VALUES[_learningConst.PART_PERIOD_CAMPUS_VIR];
                    }

                    var programActivity = nexoContext.vuc_programming_activity.Add(model);
                    success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }
                    foreach (var actProgramCurriculum in model.ProgrammingActCurriculumId)
                    {
                        nexoContext.vuc_programming_activity_Curriculum.Add(new VUCProgrammingActivityCurriculum
                        {
                            IdProgrammingActivity = programActivity.Id,
                            IdCurriculum = actProgramCurriculum.ToString(),
                            Active = true,
                            CreatedAt = currentDate
                        });
                    }
                    success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    transaction.Commit();
                    return model;
                }
            }
        }

        /// <summary>
        /// Actualiza una programación de una determinada actividad.
        /// </summary>
        /// <param name="model">Instancia de la clase de programación de actividades</param>
        /// <returns></returns>
        public static async Task<ProgrammingActivity> Update(ProgrammingActivity model)
        {
            var currentDate = DateTime.Now;

            using (var nexoContext = new NEXOContext())
            {
                using (var transaction = nexoContext.Database.BeginTransaction())
                {
                    var programActivity = await nexoContext.vuc_programming_activity.FindAsync(model.Id);

                    if (programActivity == null)
                    {
                        transaction.Dispose();
                        return null;
                    }

                    if (model.IdModality == _acadeConst.DEPA_VIR)
                    {
                        model.IdCampus = _learningConst.CAMPUS_VALUES[_learningConst.PART_PERIOD_CAMPUS_VIR];
                    }

                    programActivity.IdActivity = model.IdActivity;
                    programActivity.StartDateEnrollment = model.StartDateEnrollment;
                    programActivity.EndDateEnrollment = model.EndDateEnrollment;
                    programActivity.StartDateActivity = model.StartDateActivity;
                    programActivity.EndDateActivity = model.EndDateActivity;
                    programActivity.Cost = model.Cost;
                    programActivity.Currency = model.Currency;
                    programActivity.StudentProfile = model.StudentProfile;
                    programActivity.StartPeriod = model.StartPeriod;
                    programActivity.EndPeriod = model.EndPeriod;
                    programActivity.IdScheduleType = model.IdScheduleType;
                    programActivity.ScheduleDescription = model.ScheduleDescription;
                    programActivity.SessionDay = model.SessionDay;
                    programActivity.StartHour = model.StartHour;
                    programActivity.EndHour = model.EndHour;
                    programActivity.SessionPlace = model.SessionPlace;
                    programActivity.QuantityVacancy = model.QuantityVacancy;
                    programActivity.MaxQuantityVacancy = model.MaxQuantityVacancy;
                    programActivity.DocumentTeacher = model.DocumentTeacher;
                    programActivity.NameTeacher = model.NameTeacher;
                    programActivity.IdCampus = model.IdCampus;
                    programActivity.IdModality = model.IdModality;
                    programActivity.UpdatedAt = currentDate;

                    nexoContext.Entry(programActivity).Property(progact => progact.CreatedAt).IsModified = false;
                    var success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    var progActCurriculum = await nexoContext.vuc_programming_activity_Curriculum
                                .Where(progact => progact.IdProgrammingActivity == programActivity.Id).ToListAsync();
                    progActCurriculum.ForEach(progact => progact.Active = false);
                    success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    foreach (var curriculumActProg in model.ProgrammingActCurriculumId)
                    {
                        nexoContext.vuc_programming_activity_Curriculum.Add(new VUCProgrammingActivityCurriculum
                        {
                            IdProgrammingActivity = programActivity.Id,
                            IdCurriculum = curriculumActProg.ToString(),
                            Active = true,
                            CreatedAt = currentDate
                        });
                    }
                    success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    transaction.Commit();
                    return model;
                }
            }
        }

        /// <summary>
        /// Realiza un eliminado lógico (Cambia su estado Active a 0 = False)
        /// </summary>
        /// <param name="id">Id de la programacion</param>
        /// <returns></returns>
        public static async Task<ProgrammingActivity> Delete(int id)
        {
            using (var nexoContext = new NEXOContext())
            {
                var programActivity = await nexoContext.vuc_programming_activity.FindAsync(id);

                if (programActivity == null) return null;

                // No eliminar el horario si existen estudiantes inscritos
                if (nexoContext.vuc_enrolled_activity.Any(x => x.IdProgramActivity == id && x.Active)) return null;

                programActivity.Active = false;
                programActivity.UpdatedAt = DateTime.Now;
                nexoContext.Entry(programActivity).Property(x => x.UpdatedAt).IsModified = true;
                nexoContext.Entry(programActivity).Property(x => x.Active).IsModified = true;
                var success = (await nexoContext.SaveChangesAsync() > 0);
                return success ? programActivity : null;
            }
        }

        /// <summary>
        /// Retorna las programaciones basados en un filtro.
        /// </summary>
        /// <param name="idActivityType">Id del tipo de actividad</param>
        /// <param name="idModality">Modalidad</param>
        /// <param name="idCampus">Id del campus</param>
        /// <param name="shortName">Nombre corto de la actividad (opcional)</param>
        /// <returns></returns>
        public static async Task<IEnumerable<ProgrammingActivity>> GetProgrammingActivitiesBy(int idActivityType, 
            string idModality, string idCampus, string shortName)
        {
            using (var nexoContext = new NEXOContext())
            {
                var shortNameFilter = shortName ?? string.Empty;
                var programmingActivities = await nexoContext.vuc_programming_activity
                    .Where(programming => programming.Active && programming.IdCampus == idCampus 
                        && programming.IdModality == idModality)
                    .Join(nexoContext.vuc_activities, programmingActivity => programmingActivity.IdActivity,
                        activity => activity.Id, (programmingActivity, activity) => new { programmingActivity, activity })
                    .Where(data => data.activity.IdActivityType == idActivityType 
                        && data.activity.ShortName.Contains(shortNameFilter))
                    .Select(data => new ProgrammingActivity
                    {
                        Id = data.programmingActivity.Id,
                        IdActivity = data.programmingActivity.IdActivity,
                        StartDateEnrollment = data.programmingActivity.StartDateEnrollment,
                        EndDateEnrollment = data.programmingActivity.EndDateEnrollment,
                        StartDateActivity = data.programmingActivity.StartDateActivity,
                        EndDateActivity = data.programmingActivity.EndDateActivity,
                        Cost = data.programmingActivity.Cost,
                        Currency = data.programmingActivity.Currency,
                        StudentProfile = data.programmingActivity.StudentProfile,
                        StartPeriod = data.programmingActivity.StartPeriod,
                        EndPeriod = data.programmingActivity.EndPeriod,
                        IdScheduleType = data.programmingActivity.IdScheduleType,
                        ScheduleDescription = data.programmingActivity.ScheduleDescription,
                        SessionDay = data.programmingActivity.SessionDay,
                        StartHour = data.programmingActivity.StartHour,
                        EndHour = data.programmingActivity.EndHour,
                        SessionPlace = data.programmingActivity.SessionPlace,
                        QuantityVacancy = data.programmingActivity.QuantityVacancy,
                        MaxQuantityVacancy = data.programmingActivity.MaxQuantityVacancy,
                        DocumentTeacher = data.programmingActivity.DocumentTeacher,
                        NameTeacher = data.programmingActivity.NameTeacher,
                        IdCampus = data.programmingActivity.IdCampus,
                        IdModality = data.programmingActivity.IdModality,
                        Replicated = data.programmingActivity.Replicated,
                        Activity = new Activity
                        {
                            Id = data.activity.Id,
                            ShortName = data.activity.ShortName,
                            Name = data.activity.Name
                        }
                    }).ToListAsync();

                foreach (var programmingActivity in programmingActivities)
                {
                    var idActivity = programmingActivity.Id;
                    var hasTracing = await nexoContext.vuc_tracing_program
                        .AnyAsync(x => x.IdProgrammingActivity == idActivity && x.Active);
                    var hasSelectedStudents = await nexoContext.vuc_enrolled_activity
                        .AnyAsync(x => x.IdProgramActivity == idActivity && x.Confirmed && x.Active);
                    programmingActivity.HasTracing = hasTracing;
                    programmingActivity.HasSelected = hasSelectedStudents;
                }

                return programmingActivities;
            }
        }

        /// <summary>
        /// Retorna las programaciones basados en un filtro.
        /// </summary>
        /// <param name="idActivityType">Id del tipo de actividad (obligatorio)</param>
        /// <param name="idModality">Id de modalidad (opcional)</param>
        /// <param name="idCampus">Id de campus (opcional)</param>
        /// <param name="term">term periodo académico (opcional)</param>
        /// <returns></returns>
        public static async Task<IEnumerable<ProgrammingActivity>> GetProgrammingActivitiesFilterBy(int idActivityType, 
            string idModality, string idCampus, string term)
        {
            List<ProgrammingActivity> programActivities;
            using (var nexoContext = new NEXOContext())
            {
                var idModalityFilter = idModality ?? string.Empty;
                var idCampusFilter = idCampus ?? string.Empty;
                var termFilter = term ?? string.Empty;

                var programmingActivities = nexoContext.vuc_programming_activity
                   .Join(nexoContext.vuc_activities, programmingActivity => programmingActivity.IdActivity,
                         activity => activity.Id, (programmingActivity, activity) => new { programmingActivity, activity })
                    .Where(data => data.activity.IdActivityType == idActivityType && data.programmingActivity.Active 
                        && (data.programmingActivity.IdCampus.Contains(idCampusFilter) 
                        && data.programmingActivity.IdModality.Contains(idModalityFilter) 
                        && data.programmingActivity.StartPeriod.Contains(termFilter)));

                programActivities = await programmingActivities.Select(data => new ProgrammingActivity
                {
                    Id = data.programmingActivity.Id,
                    IdActivity = data.programmingActivity.IdActivity,
                    StartDateEnrollment = data.programmingActivity.StartDateEnrollment,
                    EndDateEnrollment = data.programmingActivity.EndDateEnrollment,
                    StartDateActivity = data.programmingActivity.StartDateActivity,
                    EndDateActivity = data.programmingActivity.EndDateActivity,
                    Currency = data.programmingActivity.Currency,
                    StudentProfile = data.programmingActivity.StudentProfile,
                    StartPeriod = data.programmingActivity.StartPeriod,
                    EndPeriod = data.programmingActivity.EndPeriod,
                    IdScheduleType = data.programmingActivity.IdScheduleType,
                    ScheduleDescription = data.programmingActivity.ScheduleDescription,
                    SessionDay = data.programmingActivity.SessionDay,
                    StartHour = data.programmingActivity.StartHour,
                    EndHour = data.programmingActivity.EndHour,
                    SessionPlace = data.programmingActivity.SessionPlace,
                    MaxQuantityVacancy = data.programmingActivity.MaxQuantityVacancy,
                    DocumentTeacher = data.programmingActivity.DocumentTeacher,
                    NameTeacher = data.programmingActivity.NameTeacher,
                    IdCampus = data.programmingActivity.IdCampus,
                    IdModality = data.programmingActivity.IdModality,
                    Replicated = data.programmingActivity.Replicated,
                    Activity = nexoContext.vuc_activities
                        .Where(act => act.Id == data.programmingActivity.IdActivity && act.Active).ToList()
                        .Select(act => new Activity
                        {
                            Id = act.Id,
                            IdActivityType = act.IdActivityType,
                            Name = act.Name,
                            ShortName = act.ShortName,
                            ActivityType = nexoContext.vuc_activity_types
                            .Where(actType => actType.Id == act.IdActivityType && actType.Active).ToList()
                            .Select(actType => new ActivityType
                            {
                                Id = actType.Id,
                                Name = actType.Name,
                            }).FirstOrDefault(),
                        }).FirstOrDefault()
                }).ToListAsync();
            }

            using (var dboContext = new DBOContext())
            {
                programActivities.ForEach(x =>
                {
                    // Cuando el horario no tiene docente asignado
                    if (x.DocumentTeacher == null && x.NameTeacher == null)
                    {
                        x.NameTeacher = "No asignado";
                    }
                    else if (x.DocumentTeacher != null && x.NameTeacher == null)
                    {
                        x.NameTeacher = dboContext.view_docentes.AsNoTracking()
                            .FirstOrDefault(teacherName => teacherName.DNI == x.DocumentTeacher)?.NomCompleto;
                    }
                });
            }

            return programActivities;
        }


        ///<summary>
        /// Cambia el estado de pendiente a replicado de los horarios de talleres.
        ///</summary>
        public static async Task<bool> ReplicateActivitySchedules()
        {
            using (var nexoContext = new NEXOContext())
            {
                var programmingActivities = await nexoContext.vuc_programming_activity
                    .Join(nexoContext.vuc_activities,
                        programmingActivity => programmingActivity.IdActivity, activity => activity.Id,
                        (programmingActivity, activity) => new { programmingActivity, activity })
                    .Where(programmingActivity => programmingActivity.programmingActivity.Replicated == false
                        && programmingActivity.activity.IdActivityType == 2).ToListAsync();
                programmingActivities.ForEach(x => x.programmingActivity.Replicated = true);
                programmingActivities.ForEach(x => x.programmingActivity.UpdatedAt = DateTime.Now);

                return (await nexoContext.SaveChangesAsync()) > 0;
            }
        }
        #endregion
    }
}