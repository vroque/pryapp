﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACTools.Configuration;
using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.VUC;
using _acadConst = ACTools.Constants.AcademicConstants;
using _vuc = ACTools.Constants.ExtracurricularActivitiesConstants;

namespace ACBusiness.ExtracurricularActivities
{
    public class Activity
    {
        #region Properties
        /// <summary>
        /// Identificador de la tabla actividad
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre corto de la actividad
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Nombre de la actividad
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripción de la actividad
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Otra información adicional a la actividad
        /// </summary>
        public string Other { get; set; }

        /// <summary>
        /// Cantidad de créditos que tendra la actividad
        /// </summary>
        public byte Credits { get; set; }

        /// <summary>
        /// Imagen de la actividad
        /// </summary>
        public string ImagePath { get; set; }

        /// <summary>
        /// Id de tipos de actividad
        /// </summary>
        public int IdActivityType { get; set; }

        /// <summary>
        /// Id de ejes
        /// </summary>
        public int IdAxi { get; set; }

        /// <summary>
        /// Estado que identifica si se envio a replicar a banner
        /// </summary>
        public bool IsReplicated { get; set; }

        /// <summary>
        /// Código de banner
        /// </summary>
        public string BannerCode { get; set; }

        /// <summary>
        /// Si se encuentra visible la actividad
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// Si se encuentra activo la actividad
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha de creación de la actividad
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        // Campos de ayuda (no se envian a la BD)
        public List<int> ConvalidationTypesId { get; set; }
        public ActivityType ActivityType { get; set; }
        public Axi Axi { get; set; }
        public List<ConvalidationType> ConvalidationTypes { get; set; }
        public List<ProgrammingActivity> ProgrammingActivities { get; set; }
        public string ImageEncode { get; set; }
        #endregion

        #region Implicit operator
        public static implicit operator VUCActivity(Activity model)
        {
            if (model == null) return null;

            return new VUCActivity
            {
                Id = model.Id,
                ShortName = model.ShortName,
                Name = model.Name,
                Description = model.Description,
                Other = model.Other,
                Credits = model.Credits,
                IdActivityType = model.IdActivityType,
                IdAxi = model.IdAxi,
                ImagePath = model.ImagePath,
                Visible = true,
                Active = true,
                CreatedAt = model.CreatedAt
            };
        }

        public static implicit operator Activity(VUCActivity model)
        {
            if (model == null) return null;

            return new Activity
            {
                Id = model.Id,
                ShortName = model.ShortName,
                Name = model.Name,
                Description = model.Description,
                Other = model.Other,
                Credits = model.Credits,
                IdActivityType = model.IdActivityType,
                IdAxi = model.IdAxi,
                IsReplicated = model.IsReplicated,
                BannerCode = model.BannerCode,
                ImagePath = model.ImagePath,
                Visible = model.Visible,
                Active = true
            };
        }
        #endregion

        #region Methods
        /// <summary>
        /// Obtiene las actividades en estado activo y sus tipos de convalidaciones correspondientes.
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<Activity>> GetAll()
        {
            using (var nexoContext = new NEXOContext())
            {
                var activities = await nexoContext.vuc_activities.Where(activity => activity.Active)
                    .Select<VUCActivity, Activity>(x => x).ToListAsync();

                activities.ForEach(activity => activity.ConvalidationTypes = nexoContext.vuc_activity_convalidation
                    .Where(actConva => actConva.Active && actConva.IdActivity == activity.Id)
                    .Join(nexoContext.vuc_convalidation_type, actConva => actConva.Id, convaType => convaType.Id,
                        (actConva, convaType) => new { actConva, convaType })
                    .Select(actConva => new ConvalidationType
                    {
                        Id = actConva.convaType.Id,
                        Name = actConva.convaType.Name,
                    }).ToList());

                return activities;
            }
        }

        /// <summary>
        /// Obtiene una actividad mediante su nombre.
        /// </summary>
        /// <param name="name">Texto de búsqueda</param>
        /// <returns></returns>
        public static async Task<Activity> GetByName(string name)
        {
            using (var nexoContext = new NEXOContext())
            {
                return await nexoContext.vuc_activities
                    .Where(activity => activity.Name == name && activity.Active).FirstOrDefaultAsync();
            }
        }

        /// <summary>
        /// Obtiene una actividad mediante su nombre corto.
        /// </summary>
        /// <param name="shortName">Texto de búsqueda</param>
        /// <returns></returns>
        public static async Task<Activity> GetByShortName(string shortName)
        {
            using (var nexoContext = new NEXOContext())
            {
                return await nexoContext.vuc_activities
                    .Where(activity => activity.ShortName == shortName && activity.Active).FirstOrDefaultAsync();
            }
        }

        /// <summary>
        /// Obtiene la lista de actividades por su tipo, pueden ser:
        ///  - Talleres
        ///  - Programas
        /// </summary>
        /// <param name="idActivityType">ID del tipo de actividad</param>
        /// <returns></returns>
        public static IEnumerable<Activity> GetByActivityType(int idActivityType)
        {
            using (var nexoContext = new NEXOContext())
            {
                return nexoContext.vuc_activities.AsNoTracking()
                    .Where(activity => activity.IdActivityType == idActivityType 
                                    && activity.Active && activity.Visible).ToList()
                    .Select(activity => new Activity
                    {
                        Id = activity.Id,
                        Name = activity.Name,
                        ShortName = activity.ShortName,
                        IdActivityType = activity.IdActivityType,
                        IdAxi = activity.IdAxi
                    }).ToList();
            }
        }

        /// <summary>
        /// Obtiene las programaciones de una actividad para mostrar al estudiante los horarios disponibles para
        /// que se matricule.
        /// </summary>
        /// <param name="profile">Perfil del estudiante</param>
        /// <param name="idActivity">Id de la actividad</param>
        /// <returns></returns>
        public static async Task<Activity> GetProgrammingActivities(AcademicProfile profile, int idActivity)
        {
            var term = new ACTerm().getCurrent(profile.campus.id, profile.department);

            var activity = await GetById(idActivity);
            
            if (!string.IsNullOrEmpty(activity.ImagePath))
            {
                var imageSplit = activity.ImagePath.Split('/');
                activity.ImagePath = $"{AppSettings.images[_vuc.URI_FRONT_VUC_IMAGE_ACTIVITY]}{imageSplit[imageSplit.Length - 2]}/";
            }

            activity.ActivityType = await ActivityType.GetById(activity.IdActivityType);
            activity.Axi = await Axi.GetById(activity.IdAxi);

            if (profile.department == _acadConst.DEPA_VIR)
            {
                activity.ProgrammingActivities = ProgrammingActivity.GetAll(profile, prog => prog.Active 
                    && prog.IdModality == profile.department && prog.StartPeriod == term.term 
                    && DbFunctions.TruncateTime(prog.StartDateEnrollment) <= DbFunctions.TruncateTime(DateTime.Today) 
                    && DbFunctions.TruncateTime(DateTime.Today) <= DbFunctions.TruncateTime(prog.EndDateEnrollment)
                    && prog.IdActivity == activity.Id && prog.StartPeriod == term.term).ToList();
            }
            else
            {
                activity.ProgrammingActivities = ProgrammingActivity.GetAll(profile, prog => prog.Active 
                    && prog.IdModality == profile.department && prog.StartPeriod == term.term 
                    && DbFunctions.TruncateTime(prog.StartDateEnrollment) <= DbFunctions.TruncateTime(DateTime.Today) 
                    && DbFunctions.TruncateTime(DateTime.Today) <= DbFunctions.TruncateTime(prog.EndDateEnrollment)
                    && prog.IdCampus == profile.campus.id && prog.IdActivity == activity.Id 
                    && prog.StartPeriod == term.term).ToList();
            }

            if (activity.ProgrammingActivities.Any())
            {
                using (var dboContext = new DBOContext())
                {
                    activity.ProgrammingActivities.ForEach(prog =>
                    {
                        // Esto funciona de esta manera para mostrar al estudiante en un horario el docente registrado
                        // Solo usamos el campo DocumentTeacher para mostrar los 3 tipos de casos, evitamos usar un
                        // campo adicional. Entonces en el frontend solo mostramos el valor de este campo.
                        // 1. Docente no asignado 
                        if (prog.DocumentTeacher == null && prog.NameTeacher == null)
                        {
                            prog.DocumentTeacher = "No asignado";
                        }
                        // 2. Cuando se ingresa solo el nombre de docente (Para programas)
                        else if (prog.DocumentTeacher == null && prog.NameTeacher != null)
                        {
                            prog.DocumentTeacher = prog.NameTeacher;
                        }
                        // 3. Cuando se ha ingresado un DNI de un docente (para taller o programa)
                        else
                        {
                            prog.DocumentTeacher = dboContext.view_docentes.AsNoTracking()
                                .FirstOrDefault(teacher => teacher.DNI == prog.DocumentTeacher)?.NomCompleto;
                        }
                    });
                }
            }
            return activity;
        }

        /// <summary>
        /// Obtiene una actividad mediante su Id.
        /// </summary>
        /// <param name="id">Id de la actividad</param>
        /// <returns></returns>
        public static async Task<Activity> GetById(int id)
        {
            using (var nexoContext = new NEXOContext())
            {
                Activity activity = await nexoContext.vuc_activities.AsNoTracking()
                    .Where(act => act.Active && act.Id == id).FirstOrDefaultAsync();

                if (!string.IsNullOrEmpty(activity.ImagePath))
                {
                    activity.ImagePath = $"{AppSettings.images[_vuc.URI_VUC_IMAGE_ACTIVITY]}{activity.ImagePath.Split('.')[0]}/";
                }

                activity.ConvalidationTypes = nexoContext.vuc_activity_convalidation.AsNoTracking()
                    .Join(nexoContext.vuc_convalidation_type, a => a.IdConvalidationType, b => b.Id,
                        (a, b) => new { a, b })
                    .Where(x => x.a.IdActivity == id && x.a.Active && x.b.Active)
                    .Select(x => new ConvalidationType { Id = x.b.Id, Name = x.b.Name }).ToList();
                return activity;
            }
        }

        /// <summary>
        /// Registra una nueva actividad.
        /// </summary>
        /// <param name="model">Modelo de actividad (Activity)</param>
        /// <returns></returns>
        public static async Task<Activity> Create(Activity model)
        {
            if (model.ShortName.Length > 30) return null;

            var currentDate = DateTime.Now;
            using (var nexoContext = new NEXOContext())
            {
                using (var transaction = nexoContext.Database.BeginTransaction())
                {
                    // Obtener el último taller creado
                    var lastActivityCreated = await nexoContext.vuc_activities.AsNoTracking()
                        .Where(act => act.Active && act.IdActivityType == 2)
                        .OrderByDescending(act => act.CreatedAt).FirstOrDefaultAsync();

                    model.CreatedAt = currentDate;
                    model.Active = true;
                    var activity = nexoContext.vuc_activities.Add(model);
                    var success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    // Actualizar código banner solo para talleres
                    if (model.IdActivityType == 2)
                    {
                        // Generamos el código de banner, el cual es uno mayor al último taller creado
                        // Ejemplo: 00015 (5 dígitos)
                        if (lastActivityCreated != null)
                        {
                            var newBannerCode = (int.Parse(lastActivityCreated.BannerCode) + 1).ToString().PadLeft(5, '0');

                            // Si el código ya existe se pasa al siguiente
                            // Esto debido a que existe un taller con el código 01122 con el nombre de Herramientas para la
                            // vida universitaria.
                            while (nexoContext.vuc_activities.Any(act => act.BannerCode == newBannerCode && act.Active))
                            {
                                newBannerCode = (int.Parse(newBannerCode) + 1).ToString().PadLeft(5, '0');
                            }

                            // Actualizamos el código de banner del taller creado
                            activity.BannerCode = newBannerCode;
                        }
                        else
                        {
                            activity.BannerCode = "00001";
                        }

                        success = (await nexoContext.SaveChangesAsync()) > 0;
                        if (!success)
                        {
                            transaction.Rollback();
                            return null;
                        }
                    }

                    foreach (var idConvalidationType in model.ConvalidationTypesId)
                    {
                        nexoContext.vuc_activity_convalidation.Add(new VUCActivityConvalidation
                        {
                            IdActivity = activity.Id,
                            IdConvalidationType = idConvalidationType,
                            Active = true,
                            CreatedAt = currentDate
                        });
                    }
                    success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    transaction.Commit();
                    return model;
                }
            }
        }

        /// <summary>
        /// Actualiza una actividad.
        /// </summary>
        /// <param name="model">Modelo de la actividad (Activity)</param>
        /// <returns></returns>
        public static async Task<Activity> Update(Activity model)
        {
            if (model.ShortName.Length > 30) return null;

            var currentDate = DateTime.Now;

            using (var nexoContext = new NEXOContext())
            {
                using (var transaction = nexoContext.Database.BeginTransaction())
                {
                    var activity = await nexoContext.vuc_activities.FindAsync(model.Id);

                    if (activity == null) 
                    {
                        transaction.Dispose();
                        return null;
                    }

                    activity.Name = model.Name;
                    activity.ShortName = model.ShortName;
                    activity.ImagePath = string.IsNullOrEmpty(model.ImagePath) ? activity.ImagePath : model.ImagePath;
                    activity.Other = model.Other;
                    activity.Credits = model.Credits;
                    activity.Description = model.Description;
                    activity.IdActivityType = model.IdActivityType;
                    activity.UpdatedAt = currentDate;

                    nexoContext.Entry(activity).Property(act => act.CreatedAt).IsModified = false;
                    var success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    var activityConvalidations = await nexoContext.vuc_activity_convalidation
                        .Where(act => act.IdActivity == activity.Id).ToListAsync();
                    activityConvalidations.ForEach(act => act.Active = false);
                    success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    foreach (var idConvalidationType in model.ConvalidationTypesId)
                    {
                        nexoContext.vuc_activity_convalidation.Add(new VUCActivityConvalidation
                        {
                            IdActivity = activity.Id,
                            IdConvalidationType = idConvalidationType,
                            Active = true,
                            CreatedAt = currentDate
                        });
                    }
                    success = (await nexoContext.SaveChangesAsync()) > 0;
                    if (!success)
                    {
                        transaction.Rollback();
                        return null;
                    }

                    transaction.Commit();
                    return model;
                }
            }
        }

        /// <summary>
        /// Cambiar el estado de visibilidad de una actividad.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static async Task<Activity> Desactivate(int id)
        {
            using (var nexoContext = new NEXOContext())
            {
                var activity = await nexoContext.vuc_activities.FindAsync(id);

                if (activity == null) return null;

                activity.Visible = !activity.Visible;
                activity.UpdatedAt = DateTime.Now;
                nexoContext.Entry(activity).Property(x => x.UpdatedAt).IsModified = true;
                nexoContext.Entry(activity).Property(x => x.Visible).IsModified = true;

                var success = (await nexoContext.SaveChangesAsync()) > 0;
                return success ? activity : null;
            }
        }

        /// <summary>
        /// Realiza un eliminado lógico (cambia su estado Active a 0)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static async Task<Activity> Delete(int id)
        {
            using (var nexoContext = new NEXOContext())
            {
                var activity = await nexoContext.vuc_activities.FindAsync(id);

                if (activity == null) return null;

                // No eliminar si tiene programaciones
                if (nexoContext.vuc_programming_activity.Any(x => x.Active && x.IdActivity == id)) return null;

                activity.Active = false;
                activity.UpdatedAt = DateTime.Now;
                nexoContext.Entry(activity).Property(x => x.UpdatedAt).IsModified = true;
                nexoContext.Entry(activity).Property(x => x.Active).IsModified = true;

                var success = (await nexoContext.SaveChangesAsync()) > 0;
                return success ? activity : null;
            }
        }

        ///<summary>
        ///Cambiar el estado de réplica de un conjunto de actividades tipo taller.
        ///</summary>
        public static async Task<bool> ProcessIsReplicated()
        {
            using (var nexoContext = new NEXOContext())
            {
                var activities = await nexoContext.vuc_activities
                    .Where(act => act.IsReplicated == false && act.IdActivityType == 2).ToListAsync();
                activities.ForEach(activity => activity.IsReplicated = true);
                activities.ForEach(activity => activity.UpdatedAt = DateTime.Now);

                return (await nexoContext.SaveChangesAsync()) > 0;
            }
        }

        /// <summary>
        /// Obtiene las actividades (solo talleres) en estado activo y las que esten visibles.
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<Activity>> GetWorkshopActivities()
        {
            using (var nexoContext = new NEXOContext())
            {
                return await nexoContext.vuc_activities
                    .Where(activity => activity.Active && activity.Visible && activity.IdActivityType == 2)
                    .Select(activity => new Activity
                    {
                        Id = activity.Id,
                        BannerCode = activity.BannerCode,
                        Name = activity.Name.ToUpper(),
                        ShortName = activity.ShortName.ToUpper(),
                        Credits = activity.Credits,
                        IsReplicated = activity.IsReplicated
                    }).ToListAsync();
            }
        }
        #endregion
    }
}
