﻿using System;

namespace ACBusiness.ExtracurricularActivities
{
    /// <summary>
    /// Clase que retorna a la vista para mostrar el reporte de seguimiento.
    /// </summary>
    public class TracingReport
    {
        /// <summary>
        /// Nombre de la actividad
        /// </summary>
        public string ActivityName { get; set; }

        /// <summary>
        /// Pidm del estudiante
        /// </summary>
        public decimal Pidm { get; set; }

        /// <summary>
        /// DNI del estudiante
        /// </summary>
        public string Dni { get; set; }

        /// <summary>
        /// Nombre completo del estudiante
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Carrera del estudiante
        /// </summary>
        public string Eap { get; set; }

        /// <summary>
        /// Programa
        /// </summary>
        public string Program { get; set; }

        /// <summary>
        /// Estado
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Horas
        /// </summary>
        public short Hours { get; set; }

        /// <summary>
        /// Nota
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// Observación
        /// </summary>
        public string Observation { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }
    }
}
