﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;

namespace ACBusiness.ExtracurricularActivities
{
    public class ScheduleType
    {
        #region Properties
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Nombre tipo de horario
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Obtiene los tipos de horarios.
        /// Puede ser descriptibo o no descriptivo.
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<ScheduleType>> GetAll()
        {
            using (var nexoContext = new NEXOContext())
            {
                return await nexoContext.vuc_schedule_type.Where(schedule => schedule.Active)
                    .Select(schedule => new ScheduleType {Id = schedule.Id, Name = schedule.Name}).ToListAsync();
            }
        }
        #endregion
    }
}
