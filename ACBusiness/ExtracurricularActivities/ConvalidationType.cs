﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;

namespace ACBusiness.ExtracurricularActivities
{
    public class ConvalidationType
    {
        #region Properties
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre de los tipos de convalidación
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Obtiene los tipos de convalidaciones para un actividad
        /// Por el momento pueden ser extracurricular o proyección social.
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<ConvalidationType>> GetAll()
        {
            using (var nexoContext = new NEXOContext())
            {
                return await nexoContext.vuc_convalidation_type.AsNoTracking()
                    .Where(convalidation => convalidation.Active)
                    .Select(convalidation => new ConvalidationType { Id = convalidation.Id, Name = convalidation.Name })
                    .ToListAsync();
            }
        }
        #endregion
    }
}