﻿namespace ACBusiness.ExtracurricularActivities
{
    /// <summary>
    /// Modelo que envia la vista para registrar los estudiantes seleccionados en programas.
    /// </summary>
    public class SelectedStudent
    {
        #region Properties
        /// <summary>
        /// Identificador de la tabla ProgramActivity
        /// </summary>
        public int IdProgramActivity { get; set; }

        /// <summary>
        /// Pidm de los estudiantes que han sido seleccionados
        /// </summary>
        public decimal[] PidmStudent { get; set; }
        #endregion
    }
}
