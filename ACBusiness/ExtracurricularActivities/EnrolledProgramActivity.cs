﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.VUC;
using ACTools.Mail;
using ACTools.Util;

namespace ACBusiness.ExtracurricularActivities
{
    public class EnrolledProgramActivity
    {
        #region Properties
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la tabla ProgrammingActivity
        /// </summary>
        public int IdProgramActivity { get; set; }

        /// <summary>
        /// Ciclo del estudiante
        /// </summary>
        public int Cycle { get; set; }

        /// <summary>
        /// Plan de estudios del estudiante
        /// </summary>
        public string Curriculum { get; set; }

        /// <summary>
        /// Identificador de la tabla campus
        /// </summary>
        public string IdCampus { get; set; }

        /// <summary>
        /// Identificador de la tabla modalidades
        /// </summary>
        public string IdModality { get; set; }

        /// <summary>
        /// Fecha de Inscripción
        /// </summary>
        public DateTime? DateEnrolled { get; set; }

        /// <summary>
        /// Código de carrera profesional
        /// </summary>
        public string Eap { get; set; }
        
        /// <summary>
        /// Estado de confirmación
        /// </summary>
        public bool Confirmed { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }

        /// <summary>
        /// Pidm del estudiante
        /// </summary>
        public decimal PidmStudent { get; set; }

        /// <summary>
        /// Concepto de la cuenta corriente
        /// </summary>
        public string IdConcept { get; set; }

        /// <summary>
        /// Fecha de confirmación
        /// </summary>
        public DateTime? DateConfirmed { get; set; }

        public AccreditedStudent Student { get; set; }

        public ProgrammingActivity ProgrammingActivity { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Actualiza el estado de "confirmed" de los estudiantes seleccionados de la actividad: Programa.
        /// </summary>
        /// <param name="model">model clase "SelectedStudent" MAS ESPECIFICO</param> 
        /// <returns></returns>
        public static async Task<SelectedStudent> UpdateStudentStatus(SelectedStudent model)
        {
            using (var nexoContext = new NEXOContext())
            {
                using (var transaction = nexoContext.Database.BeginTransaction())
                {
                    var pidms = new List<decimal>();
                    // Revisar si el estudiante que se ha enviado se encuentra matriculado
                    foreach (var pidm in model.PidmStudent)
                    {
                        var isEnrolled = nexoContext.vuc_enrolled_activity
                            .Any(enrolled => enrolled.PidmStudent == pidm && enrolled.IdProgramActivity == model.IdProgramActivity);
                        if (isEnrolled)
                        {
                            pidms.Add(pidm);
                        }
                    }

                    List<decimal> notSelectedStudents = new List<decimal>();
                    DateTime currentDate = DateTime.Now;
                    bool success;

                    // Obtenemos todos los inscritos
                    var allStudents = await nexoContext.vuc_enrolled_activity
                                            .Where(x => x.IdProgramActivity == model.IdProgramActivity).ToListAsync();

                    // Registra los seleccionados
                    foreach (var pidm in pidms)
                    {
                        var enrollment = await nexoContext.vuc_enrolled_activity
                            .Where(x => x.PidmStudent == pidm && x.IdProgramActivity == model.IdProgramActivity).FirstOrDefaultAsync();
                        if (enrollment != null)
                        {
                            enrollment.Confirmed = true;
                            enrollment.DateConfirmed = currentDate;
                            enrollment.UpdatedAt = currentDate;
                        }
                        else
                        {
                            transaction.Rollback();
                            return null;
                        }

                        success = (await nexoContext.SaveChangesAsync()) > 0;
                        if (success) continue;
                        transaction.Rollback();
                        return null;
                    }

                    // Obtenemos los estudiantes que no fueron aceptados en el programa
                    foreach (var student in allStudents)
                    {
                        if (pidms.All(x => x != student.PidmStudent))
                        {
                            notSelectedStudents.Add(student.PidmStudent);
                        }
                    }

                    // Registra los no seleccionados
                    foreach (var pidm in notSelectedStudents)
                    {
                        var enrollment = await nexoContext.vuc_enrolled_activity.Where(x => x.PidmStudent == pidm
                                            && x.IdProgramActivity == model.IdProgramActivity).FirstOrDefaultAsync();
                        if (enrollment != null)
                        {
                            enrollment.Confirmed = false;
                            enrollment.DateConfirmed = currentDate;
                            enrollment.UpdatedAt = currentDate;
                        }
                        else
                        {
                            transaction.Rollback();
                            return null;
                        }
                        success = (await nexoContext.SaveChangesAsync()) > 0;

                        if (success) continue;
                        transaction.Rollback();
                        return null;
                    }

                    transaction.Commit();

                    // Obtenemos la información de la actividad
                    var activity = await nexoContext.vuc_activities
                                   .Join(nexoContext.vuc_programming_activity, act => act.Id, 
                                        programmingActivity => programmingActivity.IdActivity,
                                        (act, programmingActivity) => new { act, programmingActivity })
                                   .FirstOrDefaultAsync(x => x.programmingActivity.Id == model.IdProgramActivity);
                    // Enviamos los correos
                    var mail = new MailSender();
                    if (pidms.Any())
                    {
                        // Enviar correo a los aceptados
                        foreach (var pidm in model.PidmStudent)
                        {
                            var student = nexoContext.dim_persons.FirstOrDefault(f => f.pidm == pidm);
                            if (student != null)
                            {
                                // Intenta enviar los correos en caso de que no este configurado en el web.config
                                try
                                {
                                    mail.compose("accepted_program",
                                            new { first_name = student.fullName, activity_name = activity?.act.ShortName })
                                        .destination($"{student.documentNumber}@continental.edu.pe",
                                            "Vida Universitaria Continental - Registro de programas").send();
                                }
                                catch { }
                            }
                        }
                    }

                    if (notSelectedStudents.Any())
                    {
                        // Enviar correo a los aceptados
                        foreach (var pidm in notSelectedStudents)
                        {
                            var student = nexoContext.dim_persons.FirstOrDefault(f => f.pidm == pidm);
                            if (student != null)
                            {
                                // Intenta enviar los correos en caso de que no este configurado en el web.config
                                try
                                {
                                    mail.compose("not_accepted_program",
                                            new { first_name = student.fullName, activity_name = activity?.act.ShortName })
                                        .destination($"{student.documentNumber}@continental.edu.pe",
                                            "Resultados de proceso de selección").send();
                                }
                                catch { }
                            }
                        }
                    }
                    return model;
                }
            }
        }

        /// <summary>
        /// Retorna a los estudiantes inscritos de la actividad programa que fueron seleccionados, 
        /// con el fin de registrar su seguimiento.
        /// </summary>
        /// <param name="idProgrammingActivity"> ID de la programación de la actividad </param>
        /// <returns></returns>
        public static async Task<IEnumerable<EnrolledProgramActivity>> GetEnrolledStudents(int idProgrammingActivity)
        {
            List<EnrolledProgramActivity> enrollments;
            using (var nexoContext = new NEXOContext())
            {
                enrollments = await nexoContext.vuc_enrolled_activity.AsNoTracking()
                    .Where(enroll => enroll.IdProgramActivity == idProgrammingActivity && enroll.Active
                        && enroll.Confirmed)
                    .Select(enroll => new EnrolledProgramActivity
                    {
                        Cycle = enroll.Cycle,
                        Curriculum = enroll.Curriculum,
                        PidmStudent = enroll.PidmStudent,
                        Student = new AccreditedStudent()
                    }).ToListAsync();
            }

            enrollments.ForEach(x =>
            {
                var student = Academic.Student.GetStudent(x.PidmStudent);
                if (student != null)
                {
                    x.Student.FullNameStudent = student.full_name;
                    x.Student.DocumentNumber = student.id;
                }
            });

            return enrollments;
        }

        /// <summary>
        /// Retorna a los estudiantes aprobados en la actividad taller.
        /// </summary>
        /// <param name="idProgrammingActivity">id de programacion</param>
        /// <param name="term">periodo académico</param>
        /// <returns></returns>
        public static async Task<IEnumerable<EnrolledProgramActivity>> GetStudents(int idProgrammingActivity, 
            string term)
        {
            using (var nexoContext = new NEXOContext())
            {
                var programmingActivity = await nexoContext.vuc_programming_activity
                    .FirstOrDefaultAsync(x => x.Id == idProgrammingActivity && x.Active && x.StartPeriod == term 
                                              && !string.IsNullOrEmpty(x.nrc));

                if (string.IsNullOrEmpty(programmingActivity?.nrc))
                {
                    // Retorna una lista vacia
                    return new List<EnrolledProgramActivity>();
                }

                return nexoContext.vuc_enrolled_activity
                   .Where(x => x.Active && x.IdProgramActivity == idProgrammingActivity).ToList()
                   .Select(enrolledProgramActivity => new EnrolledProgramActivity
                   {
                       Id = enrolledProgramActivity.Id,
                       IdProgramActivity = enrolledProgramActivity.IdProgramActivity,
                       IdModality = enrolledProgramActivity.IdModality,
                       PidmStudent = enrolledProgramActivity.PidmStudent,
                       Student = AccreditedStudent
                                .GetStudentAssistanceFinalScoreCourse(enrolledProgramActivity.PidmStudent, 
                                programmingActivity.nrc, term).FirstOrDefault()
                   }).ToList();
            }
        }

        /// <summary>
        /// Matrícula a un estudiante en un taller (actividad extracurricular).
        /// </summary>
        /// <param name="profile">Perfil académico del estudiante</param>
        /// <param name="idProgrammingActivity">Id de la programación</param>
        /// <returns></returns>
        public static MessageServer EnrollStudent(AcademicProfile profile, int idProgrammingActivity)
        {
            MessageServer message = new MessageServer
            {
                Status = "success",
                Title = "Registro de extracurriculares",
                Message = "Tu registro ha sido satisfactorio, revisa tu correo para más información"
            };
            var term = new ACTerm().getCurrent(profile.campus.id, profile.department);
            int? idActivityType;
            VUCProgrammingActivity programmingActivity;

            // 1. Verificar si el estudiante ya se encuentra registrado en la actividad
            if (!VerifyRegisteredInProgramming(profile, idProgrammingActivity))
            {
                message.Status = "alert";
                message.Title = "Doble registro";
                message.Message = "Ya te encuentras inscrito en la actividad";
                return message;
            }

            // 2. Verificar que existan vacantes
            if (!VerifyAvailableVancancy(idProgrammingActivity, out programmingActivity))
            {
                message.Status = "danger";
                message.Title = "Error";
                message.Message = @"La cantidad de vacantes ha sido completada, no se pueden recibir más estudiantes.";
                return message;
            }

            using (var nexoContext = new NEXOContext())
            {
                /*
                * 3. Verificar cruce de horario
                * Verificar cruce de horaraio para los que tienen horarios no descriptivos, por lo general vienen a 
                * ser los talleres. Anteriormente se validaba solo el cruce de horario a los de plan 2018 en adelante,
                * pero debido a que un estudiante del plan 2015 puede llevar un taller (que no le suma en nada) se
                * verifica que no se le cruce en sus horarios.
                */
                if (programmingActivity.IdScheduleType == "ND")
                {
                    // Se obtiene los cursos del estudiante
                    var schedules = new Schedule().GetByStudent(profile);

                    // Verificar que los objectos no sean nulos
                    if (schedules != null)
                    {
                        // Recorrer los cursos y comparar las horas
                        foreach (var schedule in schedules)
                        {
                            if (schedule.day.ToUpper() == programmingActivity.SessionDay.ToUpper())
                            {
                                foreach (var course in schedule.courses)
                                {
                                    if (VerifyTimeCrossing(course, programmingActivity.StartHour,
                                        programmingActivity.EndHour))
                                    {
                                        message.Status = "danger";
                                        message.Title = "Cruce de horario";
                                        message.Message = "Seleccione otro horario o actividad.";
                                        return message;
                                    }
                                }
                            }
                        }
                        
                    }
                }

                idActivityType = nexoContext.vuc_activities.FirstOrDefault(act => act.Id == programmingActivity.IdActivity)?.IdActivityType;
            }

            // 4. Se obtiene la información del estudiante
            var student = Academic.Student.GetStudent(profile.person_id);

            // 5. Registrar la matrícula
            using (var nexoContext = new NEXOContext())
            {
                var actualCycle = Academic.Student.cycleByAcademicPeriod(term.term, profile);
                DateTime? currentDate = DateTime.Now;

                var enrollment = nexoContext.vuc_enrolled_activity.Add(new VUCEnrolledProgramActivity
                {
                    Term = term.term,
                    Confirmed = idActivityType != 1,
                    CreatedAt = currentDate,
                    Curriculum = profile.term_catalg,
                    EAP = profile.program.id,
                    PidmStudent = profile.person_id,
                    IdCampus = profile.campus.id,
                    IdModality = profile.department,
                    IdProgramActivity = idProgrammingActivity,
                    Cycle = actualCycle,
                    IDDependencia = profile.div,
                    IDConcepto = string.Empty,
                    DateEnrolled = currentDate,
                    DateConfirmed = idActivityType == 1 ? null : currentDate,
                    Active = true
                });
                nexoContext.vuc_enrolled_activity.Add(enrollment);
                if (!VerifyAvailableVancancy(idProgrammingActivity, out programmingActivity))
                {
                    message.Status = "danger";
                    message.Title = "Error";
                    message.Message = @"La cantidad de vacantes ha sido completada, no se pueden recibir más estudiantes.";
                    return message;
                }
                var success = nexoContext.SaveChanges() > 0;

                var activity = nexoContext.vuc_activities.Find(programmingActivity.IdActivity);

                var schedule = programmingActivity.IdScheduleType == "ND" 
                                ? $@"{programmingActivity.SessionDay} de 
                                    {programmingActivity.StartHour} hasta {programmingActivity.EndHour}"
                                : programmingActivity.ScheduleDescription;

                if (success)
                {
                    if (student != null && activity != null)
                    {
                        var mail = new MailSender();
                        try
                        {
                            mail.compose("enroll_activity", new
                                {
                                    firstName = student.first_name,
                                    activityName = activity.ShortName,
                                    hasCost = programmingActivity.Cost > 0,
                                    location = programmingActivity.SessionPlace,schedule,
                                    startDate = programmingActivity.StartDateActivity.ToShortDateString()
                                })
                                .destination($"{student.id}@continental.edu.pe",
                                    "Vida Universitaria Continental - Actividad extracurricular").send();
                        }
                        catch { }
                    }
                }
                else
                {
                    message.Status = "alert";
                    message.Title = "Error al finalizar la matrícula";
                    message.Message = "Ha ocurrido un error al intentar matricularte en esta actividad";
                }

                return message;
            }
        }

        /// <summary>
        /// Retorna true si existen vacantes disponibles.
        /// </summary>
        /// <param name="idProgrammingActivity">Id de la programación de la actividad</param>
        /// <param name="programmingActivity">Programación de la actividad Modelo</param>
        /// <returns></returns>
        public static bool VerifyAvailableVancancy(int idProgrammingActivity, out VUCProgrammingActivity programmingActivity)
        {
            using (var nexoContext = new NEXOContext())
            {
                programmingActivity = nexoContext.vuc_programming_activity.Find(idProgrammingActivity);
                var quantityEnrolledStudents = nexoContext.vuc_enrolled_activity
                    .Count(enroll => enroll.IdProgramActivity == idProgrammingActivity);
                return programmingActivity != null && programmingActivity?.MaxQuantityVacancy - quantityEnrolledStudents > 0;
            }
        }

        /// <summary>
        /// Verifica si un estudiante ya se encuentra inscrito en una actividad.
        /// </summary>
        /// <param name="profile">Perfil del estudiante</param>
        /// <param name="idProgrammingActivity">Id de la programación de la actividad</param>
        /// <returns>Retorna true si el estudiante no esta registrado en la actividad</returns>
        public static bool VerifyRegisteredInProgramming(AcademicProfile profile, int idProgrammingActivity)
        {
            var term = new ACTerm().getCurrent(profile.campus.id, profile.department);
            using (var nexoContext = new NEXOContext())
            {
                // Actividad de la programación
                var idActivity = nexoContext.vuc_programming_activity
                                .Where(programming => programming.Id == idProgrammingActivity && programming.Active)
                                .Join(nexoContext.vuc_activities,
                                    programming => programming.IdActivity,
                                    activity => activity.Id,
                                    (programming, activity) => new { programming, activity })
                                .FirstOrDefault()?.activity.Id;

                // Matriculas en un determinado periodo
                var data = nexoContext.vuc_enrolled_activity.Where(enroll => enroll.Term == term.term 
                            && enroll.PidmStudent == profile.person_id && enroll.Active)
                           .Join(nexoContext.vuc_programming_activity,
                                enrolled => enrolled.IdProgramActivity,
                                programming => programming.Id,
                                (enrolled, programming) => new { enrolled, programming })
                           .Join(nexoContext.vuc_activities,
                                enroll => enroll.programming.IdActivity,
                                activity => activity.Id,
                                (enroll, activity) => new { enroll, activity })
                           .ToList();

                // Revisa si alguna matrícula no coincide con la actividad que se envia en la programación
                return data.FirstOrDefault(x => x.activity.Id == idActivity) == null;
            }
        }

        /// <summary>
        /// Verifica si existe cruce de horario.
        /// </summary>
        /// <param name="course">Curso</param>
        /// <param name="start">Hora de inicio</param>
        /// <param name="end">Hora fin</param>
        /// <returns></returns>
        private static bool VerifyTimeCrossing(ScheduleCourse course, string start, string end)
        {
            var startLimit = TimeHelper.ConvertTimeToMinutes(course.start);
            var endLimit = TimeHelper.ConvertTimeToMinutes(course.end);

            var startH = TimeHelper.ConvertTimeToMinutes(start);
            var endH = TimeHelper.ConvertTimeToMinutes(end);

            if (startLimit <= startH && startH <= endLimit)
                return true;
            if (startLimit <= endH && endH <= endLimit)
                return false;

            return false;
        }
        #endregion
    }
}