﻿using System.Data.Entity;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO.views;

namespace ACBusiness.ExtracurricularActivities
{
    public class TeacherSearch
    {
        #region Properties
        /// <summary>
        /// Pidm del docente
        /// </summary>
        public decimal Pidm { get; set; }

        /// <summary>
        /// DNI del docente
        /// </summary>
        public string DocumentNumber { get; set; }

        /// <summary>
        /// Nombre completo del docente
        /// </summary>
        public string FullName { get; set; }
        #endregion

        #region Implicit Operators
        public static implicit operator TeacherSearch(DBODatDocente model)
        {
            if (model == null) return null;

            return new TeacherSearch
            {
                Pidm = model.IDPersonaN,
                DocumentNumber = model.DNI,
                FullName = model.NomCompleto
            };
        }
        #endregion

        #region Methods
        /// <summary>
        /// Obtiene la información de un docente por su DNI.
        /// Consulta a la vista DAT_Docente de BDUCCI.
        /// </summary>
        /// <param name="documentNumber">DNI del docente</param>
        /// <returns></returns>
        public static async Task<TeacherSearch> GetByDni(string documentNumber)
        {
            using (var dboContext = new DBOContext())
            {
                return await dboContext.view_docentes.FirstOrDefaultAsync(teacher => teacher.DNI == documentNumber);
            }
        }
        #endregion
    }
}