﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BANNER;

namespace ACBusiness.ExtracurricularActivities
{
    public class Curriculum
    {
        #region Properties
        /// <summary>
        /// Plan de estudios
        /// </summary>
        public string curriculum { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Obtiene los planes de estudio para la programación de actividades, se aplican filtros para obtener solo
        /// los planes 2018 y 2015
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<Curriculum>> GetAll()
        {
            using (var bnContext = new BNContext())
            {
                var curriculums = await bnContext.tbl_SZVCRED.Where(x => x.SZVCRED_TERM_CODE_CTLG_END != "999999" &&
                    !x.SZVCRED_TERM_CODE_CTLG_END.StartsWith("2019")).Select(x => x.SZVCRED_TERM_CODE_CTLG_END)
                    .ToListAsync();
                return curriculums.Distinct().OrderByDescending(x => x).Take(2).Select(x => 
                    new Curriculum { curriculum = x.Substring(0, 4)}).ToList();
            }
        }
        #endregion Methods
    }
}
