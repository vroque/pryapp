using System.Collections.Generic;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACBusiness.Graduation
{
    /// <summary>
    /// Información de egresado 
    /// </summary>
    public class GraduateInformation
    {
        #region Properties

        /// <summary>
        /// Modalidad de Egresado
        /// </summary>
        public string ModalityId { get; set; }

        /// <summary>
        /// Nombre de Modalidad de Egresado
        /// </summary>
        public string ModalityName { get; set; }

        /// <summary>
        /// Período académico de egreso de la UC
        /// </summary>
        public string GraduationTerm { get; set; }

        #endregion Properties

        #region Constructors

        public GraduateInformation()
        {
        }

        public GraduateInformation(string modalityId)
        {
            var departmentCodeList = new List<string> {_acad.DEPA_REG, _acad.DEPA_VIR, _acad.DEPA_PGT};
            if (!departmentCodeList.Contains(modalityId)) return;

            ModalityId = modalityId;
            ModalityName = _acad.DEPARTMENT_NAMES_CAU[modalityId];
        }

        public GraduateInformation(string modalityId, string graduationTerm)
        {
            var departmentCodeList = new List<string> {_acad.DEPA_REG, _acad.DEPA_VIR, _acad.DEPA_PGT};
            if (!departmentCodeList.Contains(modalityId)) return;
            
            var g = new GraduateInformation(modalityId);
            ModalityId = modalityId;
            ModalityName = _acad.DEPARTMENT_NAMES_CAU[modalityId];
            GraduationTerm = graduationTerm;
        }

        #endregion Constructors
    }
}