﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.CAU;
using _personal = ACTools.Constants.PersonalConstants;
using _s = ACTools.strings.Errors;
using System.Net.Http;
using System.Net;

namespace ACBusiness.Apoderapp
{
    public class Apoderado : Person
    {
        #region Properties

        /// <summary>
        /// Acceso a ver info del estudiante
        /// </summary>
        public bool has_access { get; set; }

        public string comment { get; set; }

        /// <summary>
        /// Parentesco
        /// </summary>
        public string kinshipName { get; set; }

        public int KinshipId { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Lista de Apoderados
        /// </summary>
        /// <param name="personApoderado">Lista de apoderados relacionados a un estudiante</param>
        /// <returns>Lista de Apoderados</returns>
        public static List<Apoderado> GetApoderados(List<CAUPersonApoderado> personApoderado)
        {
            if (!personApoderado.Any()) return null;

            var apoderados = new List<Apoderado>();
            foreach (var pa in personApoderado)
            {
                var person = new Person(pa.PIDM);
                var apoderado = FromPerson(person);
                apoderado.has_access = pa.Active;
                apoderado.comment = pa.LastModificationComment;
                apoderado.KinshipId = pa.KinshipID;
                apoderado.kinshipName = _personal.KinshipDictionary[pa.KinshipID];
                apoderados.Add(apoderado);
            }
            return apoderados;
        }

        /// <summary>
        /// Registro de Apoderado
        /// </summary>
        /// <param name="student">Objeto Estudiante</param>
        /// <param name="apoderado">Objeto Apoderado</param>
        /// <param name="kinshipId">Parentesco ID</param>
        /// <param name="comment">Comentario del registro</param>
        /// <param name="userId">PIDM del usuario que realiza la operación</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static async Task<StudentDetail> AddApoderadoTask(Student student, Person apoderado, int kinshipId,
            string comment, decimal userId)
        {
            var nexoContext = new NEXOContext();
            var exist = nexoContext.cau_person_apoderado.Any(w =>
                w.PIDM == apoderado.id && w.StudentPIDM == student.id);

            if (exist)
            {
                throw new Exception(
                    $"Persona {apoderado.document_number} ya está asociado al estudiante {student.document_number}");
            }

            var persona = new CAUPersonApoderado
            {
                PIDM = apoderado.id,
                StudentPIDM = student.id,
                KinshipID = kinshipId,
                Active = true,
                ActivationDate = DateTime.Now,
                ActivationUserPIDM = userId,
                LastModificationDate = DateTime.Now,
                LastModificationComment = comment,
                LastModificationUserPIDM = userId
            };
            nexoContext.cau_person_apoderado.Add(persona);
            var rpta = await nexoContext.SaveChangesAsync();

            if (rpta != 1) throw new Exception("No se pudo realizar el registro del apoderado");

            // Registro de nuevo email

            var values = new Dictionary<string, string>
            {
                {"user", userId.ToString()},
                {"lastName", apoderado.fathers_last_name + " " + apoderado.mothers_last_name},
                {"firstName", apoderado.first_name},
                {"dni", apoderado.document_number},
                {"email", apoderado.document_number}
            };
            var content = new FormUrlEncodedContent(values);

            var response = await ACTools.Request.Requests.CreateNewEmailAsync(content);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                // TODO: Realizar validaciones con el resultado del registro del nuevo email
                throw new Exception(
                    string.Format(_s.HTTP_CLIENT, response.StatusCode, 
                                  "servicio de creacion de correo")
                );
            }

            return await StudentDetail.GetTask(student.id);
        }

        /// <summary>
        /// Actualización de accesos del apoderado.
        /// Retorna detalle del estudiante con información actualizada de apoderados que tienen acceso para
        /// ver su información
        /// </summary>
        /// <param name="student">Estudiante</param>
        /// <param name="apoderado">Apoderado</param>
        /// <param name="userId">PIDM del usuario que realiza al operación</param>
        /// <returns>Detalle estudiante con información actualizada de apoderados con acceso</returns>
        /// <exception cref="Exception"></exception>
        public static async Task<StudentDetail> UpdateApoderadoAccessTask(Student student, Apoderado apoderado,
            decimal userId)
        {
            var nexoContext = new NEXOContext();

            var persona = await nexoContext.cau_person_apoderado
                .Where(w => w.PIDM == apoderado.id && w.StudentPIDM == student.id).FirstOrDefaultAsync();

            // Datos que se actualizarán
            persona.Active = apoderado.has_access;
            persona.LastModificationDate = DateTime.Now;
            persona.LastModificationComment = apoderado.comment;
            persona.LastModificationUserPIDM = userId;

            var rpta = await nexoContext.SaveChangesAsync();

            if (rpta != 1) throw new Exception("No se pudo realizar la modificación de acceso del apoderado");

            if (apoderado.has_access)
            {
                // Registro de nuevo email

                var values = new Dictionary<string, string>
                {
                    {"user", userId.ToString()},
                    {"lastName", apoderado.fathers_last_name + " " + apoderado.mothers_last_name},
                    {"firstName", apoderado.first_name},
                    {"dni", apoderado.document_number},
                    {"email", apoderado.document_number}
                };
                var content = new FormUrlEncodedContent(values);

                var response = await ACTools.Request.Requests.CreateNewEmailAsync(content);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    // TODO: Realizar validaciones con el resultado del registro del nuevo email
                    throw new Exception(
                        string.Format(_s.HTTP_CLIENT, response.StatusCode,
                                      "servicio de creacion de correo")
                    );
                }
            }

            return await StudentDetail.GetTask(student.id);

        }

        /// <summary>
        /// Busca personas a partir de un arreglo de DNI o CE.
        /// Retorna info sobre si son apoderados del estudiante o no.
        /// </summary>
        /// <param name="dni">DNI de personas a buscar</param>
        /// <param name="student">Estudiante UC</param>
        /// <returns>Personas con info si son apoderados del estudiante o no</returns>
        public static async Task<List<Apoderado>> SearchStudentApoderadoByDocumentNumberTask(string[] dni,
            Student student)
        {
            DBOContext dboContext = new DBOContext();
            NEXOContext nexoContext = new NEXOContext();

            // Busca personas
            var basicData = await dboContext.tbl_persona.Where(w => dni.Contains(w.DNI) ||
                                                                    dni.Contains(w.CE)).ToListAsync();
            List<Person> peopleFound = basicData.Select<DBOTblPersona, Person>(s => s).ToList();

            // Personas NO encontradas
            dni = basicData.Aggregate(dni, (current, tblPersona) => current.Where(w => w != tblPersona.DNI &&
                                                                                       w != tblPersona.CE).ToArray());
            List<Person> peopleNotFound = dni.Select(s => new Person
            {
                document_number = s,
            }).ToList();

            // Apoderados registrados del estudiante
            var apoderadosPidmList =
                await nexoContext.cau_person_apoderado.Where(w => w.StudentPIDM == student.id).Select(s => s.PIDM)
                    .ToListAsync();


            var peopleApoderado = peopleFound.Where(w => apoderadosPidmList.Contains(w.id)).ToList();
            var peopleNoApoderado = peopleFound.Where(w => !apoderadosPidmList.Contains(w.id)).ToList();

            peopleNoApoderado = peopleNoApoderado.Union(peopleNotFound).OrderBy(o => o.last_name)
                .ThenBy(o => o.first_name).ThenBy(o => o.document_number).ToList();

            var apoderados =
                (from person in peopleApoderado let apoderado = new Apoderado() select FromPerson(person)).ToList();

            foreach (var apoderado in apoderados)
            {
                /**
                 * Aunque este apoderado no tenga acceso a ver la info del estudiante
                 * colocamos "True" para indicar en el resultado de búsqueda que
                 * esta persona si es apoderado del estudiante
                 */
                apoderado.has_access = true;
            }

            var noApoderados =
                (from person in peopleNoApoderado let noApoderado = new Apoderado() select FromPerson(person)).ToList();

            var persona = apoderados.Union(noApoderados).OrderBy(o => o.last_name).ThenBy(o => o.first_name)
                .ThenBy(o => o.document_number).ToList();
            return persona;
        }

        #endregion Methods

        #region Implicit Operator

        private static Apoderado FromPerson(Person person)
        {
            if (person == null) return null;
            var apoderado = new Apoderado
            {
                id = person.id,
                document_number = person.document_number,
                document_type = person.document_type,
                first_name = person.first_name,
                last_name = person.last_name,
                fathers_last_name = person.fathers_last_name,
                mothers_last_name = person.mothers_last_name,
                address = person.address,
                gender = person.gender,
                birthdate = person.birthdate,
                country = person.country,
                ubigeo = person.ubigeo,
                marital_status = person.marital_status
            };
            return apoderado;
        }

        #endregion Implicit Operator
    }
}