﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Personal;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Apoderapp
{
    public class StudentDetail : Student
    {
        #region Properties

        public List<Apoderado> apoderado { get; set; }

        private const int FOUND = 1;

        #endregion Properties

        #region Methods

        /// <summary>
        /// Información sobre apoderados de un estudiante
        /// </summary>
        /// <param name="studentId">Código de estudiante</param>
        /// <returns></returns>
        public static async Task<StudentDetail> GetTask(string studentId)
        {
            var pidm = Academic.Student.getPidmFromStudentId(studentId);

            var estudianteDetalle = await GetTask(pidm);

            return estudianteDetalle;
        }

        /// <summary>
        /// Información sobre apoderados de un estudiante
        /// </summary>
        /// <param name="pidm">PIDM</param>
        /// <returns></returns>
        public static async Task<StudentDetail> GetTask(decimal pidm)
        {
            var nexoContext = new NEXOContext();
            
            var personApoderado = nexoContext.cau_person_apoderado.Where(w => w.StudentPIDM == pidm);

            if (personApoderado.Any())
            {
                var person = personApoderado.FirstOrDefault();
                var personaList = new List<CAUPersonApoderado> {person};
                var estudiante = personaList.Select<CAUPersonApoderado, Student>(s => s).FirstOrDefault();

                var studentDetail = FromPerson(estudiante);
                studentDetail.status = FOUND;
                studentDetail.apoderado = Apoderado.GetApoderados(await personApoderado.ToListAsync());

                if (!studentDetail.apoderado.Any()) return studentDetail;

                studentDetail.has_apoderado = true;
                studentDetail.active_apoderado = studentDetail.apoderado.Count(w => w.has_access);
                studentDetail.inactive_apoderado = studentDetail.apoderado.Count(w => !w.has_access);
                return studentDetail;
            }

            var persona = new Person(pidm);
            var estudianteDetalle = FromPerson(persona);
            return estudianteDetalle;
        }

        #endregion Methods

        #region Implicit Operator

        private static StudentDetail FromPerson(Person student)
        {
            if (student == null) return null;
            var studentDetail = new StudentDetail
            {
                id = student.id,
                document_number = student.document_number,
                document_type = student.document_type,
                first_name = student.first_name,
                last_name = student.last_name,
                fathers_last_name = student.fathers_last_name,
                mothers_last_name = student.mothers_last_name,
                address = student.address,
                gender = student.gender,
                birthdate = student.birthdate,
                country = student.country,
                ubigeo = student.ubigeo,
                marital_status = student.marital_status
            };
            return studentDetail;
        }

        #endregion Implicit Operator
    }
}