﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO.views;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Apoderapp
{
    public class Student : Personal.Person
    {
        #region Properties

        /// <summary>
        /// Estado de registro de estudiante
        /// [0] -> NO existe registro
        /// [1] -> SI existe registro
        /// </summary>
        public int status { get; set; }
        public bool has_apoderado { get; set; }
        public int active_apoderado { get; set; }
        public int inactive_apoderado { get; set; }

        private const int FOUND = 1;
        private const int NOT_FOUND = 0;

        #endregion Properties

        #region Methods

        /// <summary>
        /// Busca Estudiantes a partir de un arreglo de DNI y muestra info básica
        /// </summary>
        /// <param name="dni">Arreglo de DNI de estudiantes</param>
        /// <returns></returns>
        public static async Task<List<Student>> SearchByDniTask(string[] dni)
        {
            var dboContext = new DBOContext();
            var nexoContext = new NEXOContext();

            // Busca estudiantes
            List<DBODATAlumnoBasico> basicDataList = new List<DBODATAlumnoBasico>();
            foreach (var d in dni)
            {
                var data = await dboContext.view_alumno_basico.Where(w => w.DNI == d).FirstOrDefaultAsync();
                if (data != null)
                {
                    basicDataList.Add(data);
                }
            }

            List<Student> found = basicDataList.Select(s =>
                    new Student
                    {
                        id = s.IDPersonaN,
                        document_number = s.DNI,
                        first_name = s.Nombres,
                        last_name = s.ApPat != null ? s.ApPat + " " + s.ApMat : string.Empty,
                        status = FOUND
                    }).Distinct().OrderBy(o => o.last_name).ThenBy(o => o.first_name).ThenBy(o => o.document_number)
                .ToList();

            // Estudiantes NO encontrados

            /*
            foreach (var alumnoBasico in basic_data)
            {
                dni = dni.Where(w => w != alumnoBasico.DNI).ToArray();
            }
             */
            dni = basicDataList.Aggregate(dni,
                (current, alumnoBasico) => current.Where(w => w != alumnoBasico.DNI).ToArray());
            List<Student> not_found = dni.Select(s => new Student
                {
                    document_number = s,
                    status = NOT_FOUND
                })
                .ToList();

            var students = found.Union(not_found).OrderBy(o => o.status).ThenBy(o => o.full_name).ToList();

            foreach (var student in students)
            {
                if (student.status != FOUND) continue;
                
                // Buscamos si existen apoderados para los DNI que estan asociados a un estudiante
                student.has_apoderado = nexoContext.cau_person_apoderado.Any(w => w.StudentPIDM == student.id);

                if (!student.has_apoderado) continue;

                student.active_apoderado =
                    nexoContext.cau_person_apoderado.Count(
                        w => w.StudentPIDM == student.id && w.Active);
                student.inactive_apoderado =
                    nexoContext.cau_person_apoderado.Count(w =>
                        w.StudentPIDM == student.id && !w.Active);
            }

            return students;
        }

        #endregion Methods

        #region Implicit Operator

        public static implicit operator Student(CAUPersonApoderado personApoderado)
        {
            if (personApoderado == null) return null;

            var person = new Personal.Person(personApoderado.StudentPIDM);

            Student student = new Student
            {
                id = person.id,
                document_number = person.document_number,
                document_type = person.document_type,
                first_name = person.first_name,
                last_name = person.last_name,
                fathers_last_name = person.fathers_last_name,
                mothers_last_name = person.mothers_last_name,
                address = person.address,
                gender = person.gender,
                birthdate = person.birthdate,
                country = person.country,
                ubigeo = person.ubigeo,
                marital_status = person.marital_status
            };
            return student;
        }

        #endregion Implicit Operator
    }
}