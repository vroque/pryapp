﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.BNE;
using ACPermanence.DataBases.DBUCCI.BNE.StoredProcedure;

namespace ACBusiness.Benefit
{
    /// <summary>
    /// Muestra o actualiza el detalle de los convenios
    /// </summary>
    public class AgreementDetail : Agreement
    {
        #region Properties
        public string modality { get; set; }
        public int percent_enrollment { get; set; }
        public int percent_cuot { get; set; } 

        private DBOContext db = new DBOContext();
        #endregion Properties

        #region methods

        public List<AgreementDetail> getdetail (string id)
        {
            List<AgreementDetail> getdetail;
            getdetail = db.bne_tbl_convenios_detalle
                .Where(f => f.IDConvenio == id).ToList()
                .Select<BNETblConveniosDetalle, AgreementDetail>(f => f).ToList();

            return getdetail;
        }

        public List<AgreementDetail> postDetail (List<AgreementDetail> detail)
        {
            if (detail == null)
            {
                throw new Exception("Error al obtener detalles del convenio.");
            }

            foreach (var d in detail)
            {
                BNETblConveniosDetalle detalle = db.bne_tbl_convenios_detalle.Where(x => x.IDConvenio == d.id && x.IDEscuelaADM == d.modality).FirstOrDefault();
                detalle.IDEscuelaADM = d.modality;
                detalle.MatriPorcent = d.percent_enrollment;
                detalle.CuotaPorcent = d.percent_cuot;
            }
            int res = db.SaveChanges();

            return detail;
            
        }
        #endregion methods

        public static implicit operator AgreementDetail(BNETblConveniosDetalle row)
        {
            if (row == null) return null;
            AgreementDetail agreement = new AgreementDetail
            {
                id = row.IDConvenio,
                modality = row.IDEscuelaADM,
                percent_enrollment = row.MatriPorcent,
                percent_cuot = row.CuotaPorcent
            };
            return agreement;
        }

    }
}
