﻿using System;
using System.Linq;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.BNE.StoredProcedure;
using ACBusiness.Institutional;

namespace ACBusiness.Benefit
{
    public class AsignBenefit
    {
        #region Properties
        public string campus { get; set; }
        public string term { get; set; }
        /// <summary>
        /// Modalidad en formato BANNER
        /// </summary>
        public string modality { get; set; }
        public string student_id { get; set; }
        public string program { get; set; }
        public string schedule { get; set; }
        public string scale { get; set; }
        public string beca { get; set; }
        public string discount_id{ get; set; }
        public string user { get; set; }
        public string observation { get; set; }
        private DBOContext db = new DBOContext();
        
        #endregion Properties
        /// <summary>
        /// Respuesta del procedimiento que crea el descuento al estudiante
        /// </summary>
        public class messageAsignBenefit
        {
            public Boolean validate { get; set; }
            public string message { get; set; }
            public static implicit operator messageAsignBenefit(BNESPAsignarDescuento obj)
            {
                if (obj == null) return null;
                messageAsignBenefit newasignacion = new messageAsignBenefit
                {
                    validate = obj.Valor,
                    message = obj.Mensaje
                };
                return newasignacion;
            }
        }

        #region Method
        /// <summary>
        /// Crea el registro de descuento al estudiante seleccionado.
        /// </summary>
        /// <param name="nasignacion">Datos recibidos para la asignacion del descuento</param>
        /// <param name="user_id">ID del usuario que registra el descuento</param>
        /// <returns></returns>
        public messageAsignBenefit Create(AsignBenefit nAsignacion, string userId)
        {
            var modalityApec = Department.toAPEC(nAsignacion.modality);
            messageAsignBenefit nuevaAsignacion = new messageAsignBenefit();

            if (nAsignacion.beca != "N" && (Int32.Parse(nAsignacion.term.Substring(0, 4)) > 2019 || 
               (Int32.Parse(nAsignacion.term.Substring(0, 4)) == 2019 && Int32.Parse(nAsignacion.term.Substring(5, 1)) >= 1)))
            {
                try
                {
                    db.bne_sp_asignar_beneficio("UCCI", nAsignacion.campus, nAsignacion.term, nAsignacion.student_id,
                                                nAsignacion.beca, nAsignacion.scale, nAsignacion.discount_id, userId, 
                                                nAsignacion.observation);

                    nuevaAsignacion.validate = true;
                    nuevaAsignacion.message = "Se creó y asignó correctamente el descuento.";
                }
                catch
                {
                    nuevaAsignacion.validate = false;
                    nuevaAsignacion.message = "No asignó el descuento.";
                }
            }
            else
            {
                nuevaAsignacion = db.bne_sp_asignar_descuento
                (nAsignacion.campus, nAsignacion.term, modalityApec, nAsignacion.student_id, nAsignacion.program, nAsignacion.schedule,
                nAsignacion.scale, nAsignacion.beca, nAsignacion.discount_id,
                userId, nAsignacion.observation).FirstOrDefault();
            }

            return nuevaAsignacion;
        }
        #endregion Method
    }
}
