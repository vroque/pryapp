﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.BNE;

namespace ACBusiness.Benefit
{
    public class Discount
    {
        #region Properties
        public string divs { get; set; }
        public string discount_type { get; set; }
        public string discount_id { get; set; }
        public string discount_name { get; set; }
        public string description { get; set; }
        public int desc_enrollment { get; set; }
        public int desc_cout { get; set; }
        public Boolean scale { get; set; }
        public Boolean beca { get; set; }
        public Boolean half_beca { get; set; }
        public Boolean third_beca { get; set; }
        public string id_personal { get; set; }
        public Boolean active { get; set; }
        #endregion Properties
        DBOContext db = new DBOContext();

        #region Methods
        public List<Discount> list()
        {
            List<Discount> listDiscount;
            listDiscount = db.bne_tbl_descuentos
                .ToList()
                .Select<BNETblDescuentos, Discount>(f => f)
                .Where(f => f.active == true)
                .ToList();
            return listDiscount;
        }
        #endregion Methods
        public static implicit operator Discount(BNETblDescuentos row)
        {
            if (row == null) return null;
            Discount discount = new Discount
            {
                divs = row.IDDependencia,
                discount_type = row.TipoDescuento,
                discount_id = row.IDDescuento,
                discount_name = row.NomDescuento,
                description = row.Descripcion,
                desc_enrollment = row.DescMat,
                desc_cout = row.DescCuo,
                scale = row.Escala,
                beca = row.Beca,
                half_beca = row.MediaBeca,
                third_beca = row.TercioBeca,
                id_personal = row.IDPersonal,
                active = row.Activo,
            };
            return discount;
        }
    }
}
