﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.BNE;
using ACPermanence.DataBases.DBUCCI.BNE.StoredProcedure;

namespace ACBusiness.Benefit
{
    public class CreateAgreement
    {
        #region Properties
        public string cod_contract { get; set; }
        public string company_name { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string grad_fam { get; set; }
        public Boolean renovation_auto { get; set; }
        public Boolean disapproved { get; set; }
        public int percent_enrollmentADM { get; set; }
        public int percent_cuotADM { get; set; }
        public int percent_enrollmentADG { get; set; }
        public int percent_cuotADG { get; set; }
        public int percent_enrollmentADV { get; set; }
        public int percent_cuotADV { get; set; }
        
        private DBOContext db = new DBOContext();
        #endregion Properties
        public class messageCreate
        {
            public Boolean validate { get; set; }
            public string message { get; set; }
            public static implicit operator messageCreate(BNESPCrearConvenio obj)
            {
                if (obj == null) return null;
                messageCreate newconvenio = new messageCreate
                {
                    validate = obj.Valor,
                    message = obj.Mensaje
                };
                return newconvenio;
            }
        }

        #region methods
        public messageCreate Create (CreateAgreement nconvenio, string user_id)
        {
            DBOContext db = new DBOContext();
            //obtener el codigo de un alumno para apec
            messageCreate convenio = db.bne_sp_crear_convenio
                (nconvenio.cod_contract, nconvenio.company_name,
                nconvenio.start_date, nconvenio.end_date, nconvenio.grad_fam,
                nconvenio.disapproved, nconvenio.renovation_auto,
                nconvenio.percent_enrollmentADM, nconvenio.percent_cuotADM,
                nconvenio.percent_enrollmentADG, nconvenio.percent_cuotADG,
                nconvenio.percent_enrollmentADV, nconvenio.percent_cuotADV,
                user_id).FirstOrDefault();

                //.Select<BNESPCrearConvenio, messageCreate>(f => f).FirstOrDefault();

            return convenio;
        }


        #endregion Methods

    }
}
