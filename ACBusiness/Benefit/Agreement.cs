﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.BNE;
using ACPermanence.DataBases.DBUCCI.BNE.StoredProcedure;

namespace ACBusiness.Benefit
{
    /// <summary>
    /// Registrar nuevos o actualizar convenios
    /// </summary>
    public class Agreement
    {
        #region Properties
        public string divs { get; set; }
        public string id { get; set; }
        public string cod_contract { get; set; }
        public string company_name { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string grad_fam { get; set; }
        public Boolean renovation_auto { get; set; }
        public Boolean disapproved { get; set; }
        public DateTime create_date { get; set; }
        public string id_user { get; set; }
        public int active { get; set; }

        private DBOContext db = new DBOContext();
        #endregion Properties

        #region methods

        public List<Agreement> list()
        {
            List<Agreement> list;
            list = db.bne_tbl_convenios
                .ToList()
                .Select<BNETblConvenios, Agreement>(f => f)
                .ToList();

            return list;
        }

        /// <summary>
        /// Obtiene el convenio en base al IDConvenio que recibe
        /// </summary>
        /// <param name="id">IDConvenio que recibe para hacer la consulta</param>
        /// <returns></returns>
        public Agreement getAgreement(string id)
        {
            Agreement ag = db.bne_tbl_convenios.FirstOrDefault(f => f.IDConvenio == id);
            return ag;
        }
        /// <summary>
        /// Actualiza el registro del convenio en base al ID que recibe.
        /// </summary>
        /// <param name="data">Todo el registro completo del convenio</param>
        /// <returns></returns>
        public Agreement PostAgreement(Agreement data)
        {
            if (data == null)
            {
                throw new Exception("Error al obtener información del convenio.");
            }

            BNETblConvenios convenio = db.bne_tbl_convenios.FirstOrDefault(w => w.IDConvenio == data.id);
            convenio.IDGrdFam = data.grad_fam;
            convenio.NomEmpresa = data.company_name;
            convenio.RenovAuto = data.renovation_auto;
            convenio.ClauDesap = data.disapproved;
            convenio.FechaIniCont = data.start_date;
            convenio.FechaFinCont = data.end_date;
            convenio.CodContrato = data.cod_contract;

            if (data.cod_contract == null || data.cod_contract.Length == 0)
            {
                throw new Exception("Código de contrato no puede estar vacio. Revisar");
            }
            if (data.company_name == null || data.company_name.Length == 0)
            {
                throw new Exception("Nombre de la empresa no puede estar vacio. Revisar");
            }
            if (data.company_name == null || data.company_name.Length == 0)
            {
                throw new Exception("Nombre de la empresa no puede estar vacio. Revisar");
            }
            if (data.start_date > data.end_date)
            {
                throw new Exception("La fecha fin no peude ser menor a la fecha de inicio. Revisar");
            }
            db.SaveChanges();
            return convenio;
        }

        #endregion methods

        public static implicit operator Agreement(BNETblConvenios row)
        {
            if (row == null) return null;
            Agreement agreement = new Agreement
            {   
                id = row.IDConvenio,
                cod_contract = row.CodContrato,
                company_name = row.NomEmpresa,
                start_date = row.FechaIniCont,
                end_date = row.FechaFinCont,
                grad_fam = row.IDGrdFam,
                renovation_auto = row.RenovAuto,
                disapproved = row.ClauDesap,
                create_date = row.FechaCreacion,
                id_user = row.IDPersonal,
                active = row.Activo
            };
            return agreement;
        }
    }
}
