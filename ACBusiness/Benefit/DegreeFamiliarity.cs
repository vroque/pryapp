﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.BNE;

namespace ACBusiness.Benefit
{
    public class DegreeFamiliarity
    {
        #region Properties
        public string id { get; set; }
        public string description { get; set; }
        public string detail { get; set; }
        #endregion Properties
        DBOContext db = new DBOContext();

        #region Methods
        public List<DegreeFamiliarity> list()
        {
            List<DegreeFamiliarity> list;
            list = db.bne_tbl_grad_familiaridad
                .ToList()
                .Select<BNETblGradFamiliaridad, DegreeFamiliarity>(f => f)
                .ToList();

            return list;
        }
        #endregion Methods

        public static implicit operator DegreeFamiliarity(BNETblGradFamiliaridad row)
        {
            if (row == null) return null;
            DegreeFamiliarity degreefamiliarity = new DegreeFamiliarity
            {
                id = row.IDGrdFam,
                description = row.Descripcion,
                detail = row.Detalle
            };
            return degreefamiliarity;
        }
    }
}
