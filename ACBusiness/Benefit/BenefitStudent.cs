﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.BNE;

namespace ACBusiness.Benefit
{
    public class BenefitStudent
    {
        #region Properties

        public string campus { get; set; }
        public string term { get; set; }
        public string modality { get; set; }
        public string id { get; set; }
        public string idBenefit { get; set; }
        public string benefit { get; set; }
        public string observation { get; set; }
        public string user { get; set; }
        public DateTime date { get; set; }
        public int active { get; set; }
        #endregion Properties

        DBOContext db = new DBOContext();

        #region Methods
        public List<BenefitStudent> list(string student_id)
        {
            var query = db.bne_tbl_alumno_beneficio.Join(
                db.bne_tbl_descuentos,
                ab => ab.IDBeneficio,
                de => de.IDDescuento,
                (ab, de) => new { ab, de }
                ).Where(f => f.ab.IDAlumno == student_id)
                .Select(f => new { f.ab.IDSede, f.ab.IDPerAcad, f.ab.IDAlumno, f.ab.IDEscuelaADM, f.ab.IDBeneficio, Beneficio = "Descuento: "+ f.de.NomDescuento, f.ab.Observacion, f.ab.IDUsuario, f.ab.FechaRegistro, f.ab.Activo });

            var query2 = db.bne_tbl_alumno_beneficio.Join(
                db.bne_tbl_convenios,
                (ab => (string)ab.IDBeneficio),
                (co => (string)co.IDConvenio),
                (ab, co) => new { ab, co }
                ).Where(f => (bool)(f.ab.IDAlumno == student_id))
                .Select(f => new { f.ab.IDSede, f.ab.IDPerAcad, f.ab.IDAlumno, f.ab.IDEscuelaADM, f.ab.IDBeneficio, Beneficio = "Convenio: "+ f.co.NomEmpresa, f.ab.Observacion, f.ab.IDUsuario, f.ab.FechaRegistro, f.ab.Activo });
            query = query.Union(query2);

            List<BenefitStudent> res = query.ToList()
                .Select(f => new BenefitStudent {
                    campus = f.IDSede,
                    term = f.IDPerAcad,
                    modality = f.IDEscuelaADM,
                    id = f.IDAlumno,
                    idBenefit=f.IDBeneficio,
                    benefit = f.Beneficio,
                    observation = f.Observacion,
                    user = f.IDUsuario,
                    date = f.FechaRegistro,
                    active = f.Activo,
                })
                .ToList();

            return res;
        }

        /// <summary>
        /// Elimina beneficio seleccionado según el estudiante
        /// </summary>
        /// <param name="benefitStudent">datos del beneficio a eliminar</param>
        /// <param name="idUser">ID del usuario</param>
        public bool DeleteBenefit(BenefitStudent benefitStudent, string idUser)
        {
            try
            {
                db.bne_sp_eliminar_beneficio("UCCI", benefitStudent.campus, benefitStudent.modality, 
                                             benefitStudent.term, benefitStudent.id, benefitStudent.idBenefit, idUser);
                return true;
            }
            catch 
            {
                return false;
            }
        }        

        #endregion Methods
    }
}
