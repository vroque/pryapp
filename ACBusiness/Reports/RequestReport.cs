﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Reports
{
    public class RequestReport
    {
        #region properties
        public int year { get; set; }
        public int month { get; set; }
        public int document_id { get; set; }
        public int quantity { get; set; }
        private NEXOContext _db = new NEXOContext();
        #endregion properties

        #region methods
        public List<RequestReport> all()
        {
            DateTime today = DateTime.Now;
            
            // traer todas las solicitudes
            List<CAUTblRequest> requests = _db.cau_requests.ToList();
            // filtrarlas por año mes
            List<int> year_list = requests.Select(f => f.requestDate.Year)
                                          .Distinct()
                                          .ToList();
            //filtrar por tipo de documento
            List<int> docs_list = requests.Select(f => f.documentID)
                                          .Distinct()
                                          .ToList();
            //contarlas
            List<RequestReport> list = new List<RequestReport>();
            foreach (int y in year_list)
            {
                foreach (int m in new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 })
                {
                    if (y == today.Year && m > today.Month) break;
                    foreach(int d in docs_list)
                    {
                        RequestReport r = new RequestReport();
                        r.year = y;
                        r.month = m;
                        r.document_id = d;
                        r.quantity = requests.Count(f =>
                           f.requestDate.Year == y
                           && f.requestDate.Month == m
                           && f.documentID == d
                        );
                        list.Add(r);
                    }
                }
            }
            
            return list.OrderBy( f => f.year)
                       .ThenBy(f=> f.month)
                       .ThenBy( f=> f.document_id)
                       .ToList();
        }
        #endregion methods

    }
}
