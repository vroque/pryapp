﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Reports
{
    /// <summary>
    /// Datos para reporte de firmantes
    /// </summary>
    public class DocumentSigners
    {
        #region properties
        public int document_id { get; set; }
        public int quantity { get; set; }
        public int year { get; set; }
        public int month { get; set; }
        public string sign_as {get; set;}
        // public DateTime date { get; set; }
        private NEXOContext _db = new NEXOContext();
        #endregion properties

        #region methods
        public List<DocumentSigners> listByPerson(decimal person_id)
        {
            List<DocumentSigners> list = new List<DocumentSigners>();
            DateTime today = DateTime.Now;
            //List<CAUTblSignatoryDocument>
            var document_list = _db.cau_signatories.Join(
                    _db.cau_signatories_documents,
                    sign => sign.signatoryID,
                    doc_sign => doc_sign.signatoryID,
                    (sign, doc_sign) => new { sign = sign, doc_sign = doc_sign }
                ).Where(f =>
                   f.sign.pidm == person_id
                ).ToList();
            foreach(var item in document_list)
            {
                // obtener todos los documentos por sus college e ID
                List<CAUTblRequest> reqs;
                if (item.doc_sign.college == null) {
                    reqs = _db.cau_requests.Join(
                            _db.dim_colleges_programs,
                            req => req.program,
                            col => col.program,
                            (req, col) => new { req = req, col = col }
                        ).Where(f =>
                           f.req.documentID == item.doc_sign.documentID
                           && f.req.updateDate >= item.sign.startDate
                           && f.req.updateDate <= item.sign.endDate
                        ).Select(f =>
                           f.req
                        ).ToList();
                }
                else
                {
                    reqs = _db.cau_requests.Join(
                            _db.dim_colleges_programs,
                            req => req.program,
                            col => col.program,
                            (req, col) => new { req = req, col = col }
                        ).Where(f =>
                           f.col.college == item.doc_sign.college
                           && f.req.documentID == item.doc_sign.documentID
                           && f.req.updateDate >= item.sign.startDate
                           && f.req.updateDate <= item.sign.endDate
                        ).Select(f =>
                           f.req
                        ).ToList();
                }
                    // SUMAR :)
                    //obtener años
                    List<int> year_list = reqs.Select(f => f.updateDate.Year)
                                              .Distinct()
                                              .ToList();
                foreach(int y in year_list)
                {
                    foreach (int m in new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 })
                    {
                        if (y == today.Year && m > today.Month) break;
                        DocumentSigners doc = new DocumentSigners();
                        doc.document_id = item.doc_sign.documentID;
                        doc.year = y;
                        doc.month = m;
                        doc.sign_as = item.sign.name;
                        doc.quantity = reqs.Count(f =>
                           f.updateDate.Year == y
                           && f.updateDate.Month == m
                        );
                        list.Add(doc);
                    }
                }
                

            }
            return list.OrderBy(f => f.year)
                       .ThenBy(f => f.month)
                       .ThenBy(f => f.document_id)
                       .ToList();
        }
        #endregion methods
    }
}
