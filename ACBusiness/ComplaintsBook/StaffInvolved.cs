﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.ClaimsBook;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.LDR;
using ACTools.Mail;
using _constants_ldr = ACTools.Constants.ClaimsBookConstants;

namespace ACBusiness.ComplaintsBook
{
    public class StaffInvolved
    {
        public int staff_involved_id { get; set; }
        public int page_id { get; set; }
        public int derivate_id { get; set; }
        public string staff_involved_pidm { get; set; }
        public string staff_position_company { get; set; }
        public string staff_involved_note { get; set; }
        public string staff_involved_type { get; set; }
        public string staff_derivate_state { get; set; }
        public DateTime staff_staff_date_created { get; set; }
        public DateTime? staff_date_modify { get; set; }
        public string staff_involved_name_position_company { get; set; }
        public string staff_involved_user_id { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        public async Task<StaffInvolved> getStaffInvolvedPage(string compania, int page_id)
        {
            var bducci = new DBOContext();
            List<StaffInvolved> staf_involved = _nexoContext.ldr_tblStaffInvolved
                .Join(_nexoContext.adryan_tbl_puesto_compania,
                    sta => new {Staff_position_company = sta.staff_position_company},
                    tpc => new {Staff_position_company = tpc.puesto},
                    (sta, tpc) => new {sta, tpc})
                .Where(result => result.sta.staff_derivate_state == _constants_ldr.LDR_STATE_STAFF_NOT_DERIVATED &&
                                 result.tpc.compania == compania &&
                                 result.sta.page_id == page_id)
                .ToList().OrderByDescending(result => result.sta.staff_date_created)
                .ToList()
                .Select(result => new StaffInvolved
                {
                    staff_involved_id = result.sta.staff_involved_id,
                    page_id = result.sta.page_id,
                    derivate_id = result.sta.derivate_id,
                    staff_involved_pidm = result.sta.staff_involved_pidm,
                    staff_position_company = result.sta.staff_position_company,
                    staff_involved_note = result.sta.staff_involved_note,
                    staff_involved_type = result.sta.staff_involved_type,
                    staff_derivate_state = result.sta.staff_derivate_state,
                    staff_staff_date_created = result.sta.staff_date_created,
                    staff_date_modify = result.sta.staff_date_modify,
                    staff_involved_name_position_company = result.tpc.descripcion_puesto
                })
                .ToList();
            return staf_involved.FirstOrDefault();
        }

        public async Task<StaffInvolved> getStaffInvolvedDerivationPidm(string compania, int page_id, int derivate_id,
            string pidm)
        {
            var bducci = new DBOContext();
            List<StaffInvolved> staf_involved = _nexoContext.ldr_tblStaffInvolved
                .Join(_nexoContext.adryan_tbl_puesto_compania,
                    sta => new {Staff_position_company = sta.staff_position_company},
                    tpc => new {Staff_position_company = tpc.puesto},
                    (sta, tpc) => new {sta, tpc})
                .Where(result => result.tpc.compania == compania &&
                                 result.sta.page_id == page_id &&
                                 result.sta.derivate_id == derivate_id &&
                                 result.sta.staff_involved_pidm == pidm)
                .ToList()
                .OrderByDescending(result => result.sta.staff_date_created)
                .ToList()
                .Select(result => new StaffInvolved
                {
                    staff_involved_id = result.sta.staff_involved_id,
                    page_id = result.sta.page_id,
                    derivate_id = result.sta.derivate_id,
                    staff_involved_pidm = result.sta.staff_involved_pidm,
                    staff_position_company = result.sta.staff_position_company,
                    staff_involved_note = result.sta.staff_involved_note,
                    staff_involved_type = result.sta.staff_involved_type,
                    staff_derivate_state = result.sta.staff_derivate_state,
                    staff_staff_date_created = result.sta.staff_date_created,
                    staff_date_modify = result.sta.staff_date_modify,
                    staff_involved_name_position_company = result.tpc.descripcion_puesto
                })
                .ToList();
            return staf_involved.FirstOrDefault();
        }

        public async Task<bool> saveStaffInvolved()
        {
            var staff = new LDRtblStaffInvolved
            {
                page_id = this.page_id,
                derivate_id = this.derivate_id,
                staff_involved_pidm = this.staff_involved_pidm,
                staff_position_company = this.staff_position_company,
                staff_involved_note = this.staff_involved_note,
                staff_involved_type = this.staff_involved_type,
                staff_derivate_state = _constants_ldr.LDR_STATE_STAFF_NOT_DERIVATED,
                staff_date_created = DateTime.Now
            };

            _nexoContext.ldr_tblStaffInvolved.Add(staff);
            var i = await _nexoContext.SaveChangesAsync();
            this.staff_involved_id = staff.staff_involved_id;

            if (i > 0)
            {
                if (staff.staff_involved_type == _constants_ldr.LDR_TYPE_INVOLVED_COLLABORATOR)
                {
                    List<LDRtblStaffInvolved> staff_boos = _nexoContext.ldr_tblStaffInvolved.Where(result =>
                        result.page_id == this.page_id &&
                        result.derivate_id == this.derivate_id &&
                        result.staff_involved_type == _constants_ldr.LDR_TYPE_INVOLVED_BOSS
                    ).ToList();

                    if (staff_boos.Any())
                    {
                        staff_boos.ForEach(rows =>
                        {
                            rows.staff_date_modify = DateTime.Now;
                            rows.staff_derivate_state = _constants_ldr.LDR_STATE_STAFF_DERIVATED;
                        });
                        _nexoContext.SaveChanges();
                    }
                }

                this.staff_involved_id = staff.staff_involved_id;

                /*================Enviar Correo=============*/

                var page = new PageClaimBook();
                var page_details = await page.getPageClaimsBook(this.page_id);
                var derivation = new DerivationArea();
                var derivate = await derivation.getDerivationClaim(staff.derivate_id, staff.staff_involved_pidm);

                var emails = new List<string>();

                if (!string.IsNullOrEmpty(this.staff_involved_user_id))
                {
                    emails.Add($"{this.staff_involved_user_id}@continental.edu.pe");
                }


                if (staff.staff_involved_type == _constants_ldr.LDR_TYPE_INVOLVED_BOSS)
                {
                    emails.AddRange(derivate.derivate_mail_recipient.Split(',').ToList());
                }

                if (emails.Count > 0)
                {
                    var mail = new MailSender();
                    var file_list_page = new FilePage();
                    List<FilePage> list_file;
                    list_file = await file_list_page.ListFilePage(this.page_id);

                    if (list_file != null && list_file.Count > 0)
                    {
                        foreach (var item in list_file)
                        {
                            mail.addFile(item.file_route);
                        }
                    }

                    mail.compose("ldr_derivate_claim",
                            new
                            {
                                type = page_details.page_type_claim,
                                year = page_details.page_year,
                                numeration = page_details.page_numeration,
                                date_creation = page_details.page_date_create,
                                names = $"{page_details.page_first_name} {page_details.page_last_name}",
                                identification = page_details.page_identification,
                                email = page_details.page_email,
                                phone = page_details.page_phone,
                                home = page_details.page_home,
                                legal_guardian = page_details.page_legal_guardian,
                                origin = _constants_ldr.LDR_PAGE_TYPE_ORIGIN[page_details.page_origin],
                                campus = Campus.get(page_details.page_campus).name,
                                type_well = page_details.page_type_well,
                                type_claim = page_details.page_type_claim,
                                description_well = page_details.page_description_well,
                                description_claim = page_details.page_description_claim,
                                name_area = derivate.derivate_funtional_unity_name,
                                name_person = derivate.staff_Involved_name,
                                note_derivation = staff.staff_involved_note,
                                username = this.staff_involved_user_id,
                            })
                        .destination(emails,
                            $"[Libro de Reclamaciones][Derivación] {page_details.page_type_well} {page_details.page_numeration}")
                        .send();
                }

                return true;
            }

            return false;
        }
    }
}
