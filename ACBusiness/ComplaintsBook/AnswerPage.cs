﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Adryan;
using ACBusiness.Aplication;
using ACBusiness.ClaimsBook;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.LDR;
using _ldr_states = ACTools.Constants.ClaimsBookConstants;

namespace ACBusiness.ComplaintsBook
{
    public class AnswerPage
    {
        #region propiedades
        public int answer_id { get; set; }
        public int page_id { get; set; }
        public int? derivate_id { get; set; }
        public string answer_person_creator_pidm { get; set; }
        public string answer_person_validate_pidm { get; set; }
        public string answer_note { get; set; }
        public string answer_state { get; set; }
        public DateTime answer_date_created { get; set; }
        public DateTime? answer_date_validate { get; set; }
        #endregion

        #region Propiedades extras
        public string answer_names { get; set; }
        public List<FilePage> files { get; set; }
        #endregion Propiedades extras

        NEXOContext _db = new NEXOContext();


        public async Task<AnswerPage> getAnswer(int answer_id) {
            AnswerPage answer = _db.ldr_tblAnswerPage.Where(result =>
               result.answer_id == answer_id             
            ).ToList().Select(result => new AnswerPage
            {
                answer_id = result.answer_id,
                page_id = result.page_id,
                answer_person_creator_pidm = result.answer_person_creator_pidm,
                answer_person_validate_pidm = result.answer_person_validate_pidm,
                answer_note = result.answer_note,
                derivate_id = result.derivate_id,
                answer_state = result.answer_state,
                answer_date_created = result.answer_date_created,
                answer_date_validate = result.answer_date_validate
            }).FirstOrDefault();

            if (answer != null)
            {
                DBOContext bducci = new DBOContext();
                DBOTblPersona person = bducci.tbl_persona.Where(result =>
                    result.IDPersonaN.ToString() == answer.answer_person_creator_pidm
                    ).FirstOrDefault();
                if (person != null)
                {
                    answer.answer_names = $"{person.Nombres} {person.Appat} {person.ApMat}";
                }
            }
            return (answer);
        }

        /// <summary>
        /// OBTENER LA RESPUESTA VIGENTE DEL RECLAMO
        /// </summary>
        /// <param name="page_id">CÓDIGO UNICO DE RECLAMO</param>
        /// <returns></returns>
        public async Task<AnswerPage> getAnswerPage(int page_id) {
            AnswerPage answer = _db.ldr_tblAnswerPage.Where(result =>
               result.page_id == page_id &&
               (result.answer_state == _ldr_states.LDR_STATE_ANSWER_WAIT || result.answer_state == _ldr_states.LDR_STATE_ANSWER_ACCEPTED || result.answer_state == _ldr_states.LDR_STATE_ANSWER_ACCEPTED_FINISH)
            ).ToList().OrderByDescending( result =>
                result.answer_date_validate
            ).Select(result => new AnswerPage
            {
                answer_id = result.answer_id,
                page_id = result.page_id,
                answer_person_creator_pidm = result.answer_person_creator_pidm,
                answer_person_validate_pidm = result.answer_person_validate_pidm,
                answer_note = result.answer_note,
                derivate_id = result.derivate_id,
                answer_state = result.answer_state,
                answer_date_created = result.answer_date_created,
                answer_date_validate = result.answer_date_validate
            }).FirstOrDefault();

            if (answer != null)
            {
                DBOContext bducci = new DBOContext();
                DBOTblPersona person = bducci.tbl_persona.Where(result =>
                    result.IDPersonaN.ToString() == answer.answer_person_creator_pidm
                    ).FirstOrDefault();
                if (person != null)
                {
                    answer.answer_names = $"{person.Nombres} {person.Appat} {person.ApMat}";
                }
            }
            return (answer);
        }

        public async Task<List<AnswerPage>> getAnswerDerivationOfPage(int page_id) {
            List<AnswerPage> answers = _db.ldr_tblAnswerPage.Where(result =>
               result.page_id == page_id &&
               result.derivate_id != null &&
               result.answer_state == _ldr_states.LDR_STATE_ANSWER_ACCEPTED &&
               result.answer_state != _ldr_states.LDR_STATE_ANSWER_REJECTED
            ).ToList().Select(result => new AnswerPage
            {
                answer_id = result.answer_id,
                page_id = result.page_id,
                answer_person_creator_pidm = result.answer_person_creator_pidm,
                answer_person_validate_pidm = result.answer_person_validate_pidm,
                answer_note = result.answer_note,
                derivate_id = result.derivate_id,
                answer_state = result.answer_state,
                answer_date_created = result.answer_date_created,
                answer_date_validate = result.answer_date_validate
            }).ToList();

            if (answers != null && answers.Count > 0)
            {
                /*=====================Obteniendo nombres=====================*/
                DBOContext bducci = new DBOContext();
                List<string> person_ids = answers.Select(f => f.answer_person_creator_pidm).Distinct().ToList();
                
                List<DBOTblPersona> persons = bducci.tbl_persona.Where(result =>
                    person_ids.Contains(result.IDPersonaN.ToString())
                    ).ToList();                
                if (persons != null  && persons.Count > 0)
                {

                    foreach (AnswerPage item_answer in answers)
                    {
                        foreach (DBOTblPersona item_persona in persons)
                        {
                            if (int.Parse(item_answer.answer_person_creator_pidm) == item_persona.IDPersonaN)
                            {
                                item_answer.answer_names = $"{item_persona.Nombres} {item_persona.Appat} {item_persona.ApMat}";
                            }
                        }
                    }
                }
                /*==============================================================*/

                /*===================Obteniendo Archivos de respuesta============*/
                FilePage file_options = new FilePage();
                List<FilePage> files = await file_options.ListFilesAnswerOfPage(page_id);
                if (files != null && files.Count > 0)
                {
                    answers.ForEach(
                            item_answers => item_answers.files = files.
                                Where(
                                    item_files => item_files.answer_id == item_answers.answer_id
                                ).ToList()
                        );
                }

                /*===============================================================*/

            }
            return (answers);

        }


        public async Task<AnswerPage> getAnswerAreaLegalOfPage(int page_id, int? derivate_id = null)
        {
            AnswerPage answer = _db.ldr_tblAnswerPage
                .Where(result =>
                           result.page_id == page_id &&
                           result.derivate_id == derivate_id  &&
                           (result.answer_state == _ldr_states.LDR_STATE_ANSWER_ACCEPTED ||result.answer_state == _ldr_states.LDR_STATE_ANSWER_ACCEPTED_FINISH) &&
                           result.answer_state != _ldr_states.LDR_STATE_ANSWER_REJECTED
                ).ToList()
                .OrderBy(result =>
                            result.answer_date_created)
               .Select(result => new AnswerPage
                    {
                        answer_id = result.answer_id,
                        page_id = result.page_id,
                        answer_person_creator_pidm = result.answer_person_creator_pidm,
                        answer_person_validate_pidm = result.answer_person_validate_pidm,
                        answer_note = result.answer_note,
                        derivate_id = result.derivate_id,
                        answer_state = result.answer_state,
                        answer_date_created = result.answer_date_created,
                        answer_date_validate = result.answer_date_validate
                    })
                .FirstOrDefault();


            if (answer != null)
            {
                /*=====================Obteniendo nombres=====================*/
                DBOContext bducci = new DBOContext();

                DBOTblPersona person = bducci.tbl_persona.Where(result =>
                        result.IDPersonaN.ToString() == answer.answer_person_validate_pidm
                    ).FirstOrDefault();
                if (person != null)
                {
                    answer.answer_names = $"{person.Nombres} {person.Appat} {person.ApMat}";
                }
                /*==============================================================*/

                /*===================Obteniendo Archivos de respuesta============*/
                FilePage file_options = new FilePage();
                List<FilePage> files = await file_options.ListFileAnswerClaim(answer.answer_id);
                if (files != null && files.Count > 0)
                {
                    answer.files = files;
                }

                /*===============================================================*/

            }
            return (answer);

        }

        public async Task<List<AnswerPage>> getAnswersDerivation(int page_id, int derivate_id) {
            List<AnswerPage> answers = _db.ldr_tblAnswerPage.Where(result =>
               result.page_id == page_id &&
               result.derivate_id == derivate_id &&
               result.answer_state != _ldr_states.LDR_STATE_ANSWER_REJECTED
            ).ToList().Select(result => new AnswerPage
            {
                answer_id = result.answer_id,
                page_id = result.page_id,
                answer_person_creator_pidm = result.answer_person_creator_pidm,
                answer_person_validate_pidm = result.answer_person_validate_pidm,
                answer_note = result.answer_note,
                derivate_id = result.derivate_id,
                answer_state = result.answer_state,
                answer_date_created = result.answer_date_created,
                answer_date_validate = result.answer_date_validate
            }).ToList();

            if (answers != null && answers.Count > 0)
            {
                /*=====================Obteniendo nombres=====================*/
                DBOContext bducci = new DBOContext();
                List<string> person_ids = answers.Select(f => f.answer_person_creator_pidm).Distinct().ToList();

                List<DBOTblPersona> persons = bducci.tbl_persona.Where(result =>
                    person_ids.Contains(result.IDPersonaN.ToString())
                    ).ToList();
                if (persons != null && persons.Count > 0)
                {

                    foreach (AnswerPage item_answer in answers)
                    {
                        foreach (DBOTblPersona item_persona in persons)
                        {
                            if (int.Parse(item_answer.answer_person_creator_pidm) == item_persona.IDPersonaN)
                            {
                                item_answer.answer_names = $"{item_persona.Nombres} {item_persona.Appat} {item_persona.ApMat}";
                            }
                        }
                    }
                }
                /*==============================================================*/

                /*===================Obteniendo Archivos de respuesta============*/
                FilePage file_options = new FilePage();
                List<FilePage> files = await file_options.ListFilesAnswerOfPage(page_id);
                if (files != null && files.Count > 0)
                {
                    answers.ForEach(
                            item_answers => item_answers.files = files.
                                Where(
                                    item_files => item_files.answer_id == item_answers.answer_id
                                ).ToList()
                        );
                }

                /*===============================================================*/

            }
            return (answers);

        }



        public async Task<bool> saveAnswer()
        {
            LDRtblDerivationArea derivate = _db.ldr_tblDerivationArea.Find(this.derivate_id);
            


            AppUser app_user = new AppUser();
            List<AppUser> list_user = app_user.listUserByModule("bo_claims-book-adm");
            AppUser validate_user = list_user.Find(x => x.pid.ToString() == this.answer_person_creator_pidm);

            bool save_answer;

            /**/

            if (derivate == null && validate_user != null)
            {
                //Guardar respuesta de administrador
                save_answer = await this.saveAnswerAdministrator();
                return save_answer;
            }
            
            /*==================VALIDACIÓNES================*/
            if (derivate == null)
            {
                throw new Exception("Error al obtener detalles de la derivación.");
            }

            /*==============================================*/
            if (derivate.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_INVOLVED_AREA || derivate.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_LEGAL_AREA)
            {
                // Ejecutar funcion para guardar area involucrada
                /*save_answer = await this.saveAnswerInvolvedArea();
                return save_answer;*/
                save_answer = await this.saveAnswerArea();
                return save_answer;
            }
            /*else if (derivate.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_LEGAL_AREA)
            {
                // Ejecutar funcion para guardar area legal
                save_answer = await this.saveAnswerLegalArea();
                return save_answer;
            }*/
            else
            {
                throw new Exception("Error en tipo de derivación");
            }
        }

        /// <summary>
        /// Guardar Respuesta de derivación de Area Involucrada
        /// </summary>
        /// <returns></returns>
        public async Task<bool> saveAnswerArea() {
            LDRtblAnswerPage answer = new LDRtblAnswerPage();

            LDRtblDerivationArea derivate = _db.ldr_tblDerivationArea.Find(this.derivate_id);

            StaffInvolved staff_involved = new StaffInvolved();
            staff_involved = await staff_involved.getStaffInvolvedDerivationPidm(_ldr_states.LDR_ID_COMPANY_UCCI, this.page_id, (int)this.derivate_id, this.answer_person_creator_pidm);


            answer.page_id = this.page_id;
            answer.derivate_id = this.derivate_id;
            answer.answer_person_creator_pidm = this.answer_person_creator_pidm;
            answer.answer_note = this.answer_note;
            answer.answer_state = _ldr_states.LDR_STATE_ANSWER_WAIT;
            answer.answer_date_created = DateTime.Now;
            /*==================VALIDACIÓNES================*/
            if (derivate == null)
            {
                throw new Exception("Error al obtener detalles de la derivación.");
            }

            if (derivate.derivate_state == _ldr_states.LDR_STATE_DERIVATE_FINALIZED || derivate.derivate_state == _ldr_states.LDR_STATE_DERIVATE_DISMISSED)
            {
                throw new Exception("La derivación ya esta finalizada o fue desestimada");
            }

            if (staff_involved == null)
            {
                throw new Exception("Error al obtener detalle de personal a cargo.");
            }
            /*==============================================*/

            derivate.derivate_state = _ldr_states.LDR_STATE_DERIVATE_RESOLVED;
            derivate.derivate_date_modify = DateTime.Now;

            int change_derivate = _db.SaveChanges();

            if (change_derivate <= 0)
            {
                throw new Exception("No se pudo modificar la derivación.");
            }

            _db.ldr_tblAnswerPage.Add(answer);
            int save_answer = _db.SaveChanges();

            if (save_answer <= 0)
            {
                throw new Exception("No se pudo Agregar la respuesta.");
            }

            this.answer_id = answer.answer_id;

            if (staff_involved.staff_involved_type == _ldr_states.LDR_TYPE_INVOLVED_BOSS)
            {
                bool accept_naswer_derivate = await this.acceptAnswerArea(answer.answer_id, answer.answer_person_creator_pidm);
                return accept_naswer_derivate;
            }
            else {
                return true;
            }
            

        }

        public async Task<bool> acceptAnswer() {
            LDRtblDerivationArea derivate = _db.ldr_tblDerivationArea.Find(this.derivate_id);
            bool accept_answer;

            /*==================VALIDACIÓNES================*/
            if (derivate == null)
            {
                throw new Exception("Error al obtener detalles de la derivación.");
            }

            /*==============================================*/
            if (derivate.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_INVOLVED_AREA || derivate.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_LEGAL_AREA && this.answer_state == _ldr_states.LDR_STATE_ANSWER_ACCEPTED)
            {
                // Ejecutar funcion para aceptar respuesta area involucrada
                accept_answer = await this.acceptAnswerArea(this.answer_id,this.answer_person_validate_pidm);
                return accept_answer;
            }

            else if (derivate.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_LEGAL_AREA && this.answer_state == _ldr_states.LDR_STATE_ANSWER_ACCEPTED_FINISH)
            {
                // Ejecutar funcion para aceptar respuesta area involucrada
                accept_answer = await this.acceptAnswerAdministrator(this.answer_id, this.answer_person_validate_pidm);
                return accept_answer;
            }
            /*else if (derivate.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_LEGAL_AREA)
            {
                // Ejecutar funcion para aceptar respuesta  area legal
                accept_answer = await this.acceptAnswerLegalArea(this.answer_id, this.answer_person_validate_pidm);
                return accept_answer;
            }*/
            else
            {
                throw new Exception("Error en tipo de derivación");
            }
        }

        /// <summary>
        /// Aceptar respuesta de area legal
        /// </summary>
        /// <param name="answer_id">código de respuesta</param>
        /// <param name="pidm_validate">PIDM Usuario que esta aceptando respuesta</param>
        /// <returns></returns>
        public async Task<bool> acceptAnswerArea(int answer_id, string pidm_validate) {
            LDRtblAnswerPage answer = new LDRtblAnswerPage();
            answer = _db.ldr_tblAnswerPage.Find(answer_id);

            LDRtblDerivationArea derivation = _db.ldr_tblDerivationArea.Find(answer.derivate_id);

            StaffInvolved staff_involved = new StaffInvolved();
            staff_involved = await staff_involved.getStaffInvolvedDerivationPidm(_ldr_states.LDR_ID_COMPANY_UCCI, answer.page_id, (int)answer.derivate_id, pidm_validate);
            /*==================VALIDACIÓNES================*/
            if (answer == null)
            {
                throw new Exception("Error al obtener información de la respuesta.");
            }
            if (staff_involved == null)
            {
                throw new Exception("Error al obtener información del personal a cargo.");
            }
            if (derivation == null)
            {
                throw new Exception("Error al obtener información de la derivación");
            }
            if (staff_involved.staff_involved_type != _ldr_states.LDR_TYPE_INVOLVED_BOSS)
            {
                throw new Exception("Usted no puede acceptar la respuesta.");
            }
            if (answer.answer_state != _ldr_states.LDR_STATE_ANSWER_WAIT)
            {
                throw new Exception("La respuesta no esta en espera.");
            }
            /*==============================================*/
            answer.answer_person_validate_pidm = pidm_validate;
            answer.answer_state = _ldr_states.LDR_STATE_ANSWER_ACCEPTED;
            answer.answer_date_validate = DateTime.Now;

            int update_answer = _db.SaveChanges();

            if (update_answer<=0)
            {
                throw new Exception("No se pudo modificar la respuesta.");
            }

            derivation.derivate_state = _ldr_states.LDR_STATE_DERIVATE_FINALIZED;
            derivation.derivate_date_modify = DateTime.Now;
            int change_derivation = _db.SaveChanges();

            if (change_derivation <= 0)
            {
                throw new Exception("No se pudo modificar la derivación.");
            }

            DerivationArea derivate_options = new DerivationArea();
            List<string> states_search = new List<string>();
            states_search.Add(_ldr_states.LDR_STATE_DERIVATE_PENDING);
            states_search.Add(_ldr_states.LDR_STATE_DERIVATE_RESOLVED);
            List<DerivationArea> list_derivation_states = await derivate_options.listDerivationStates(_ldr_states.LDR_ID_COMPANY_UCCI, derivation.page_id, states_search);
            if (list_derivation_states != null && list_derivation_states.Count <=0)
            {
                
                if (derivation.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_INVOLVED_AREA)
                {
                    /*========================Obtener usuarios del modulo===============================*/
                    AppUser app_user = new AppUser();
                    List<AppUser> emails_adm = app_user.listUserByModule("bo_claims-book-adm");
                    string derivate_mail_recipient = "";
                    for (int co = 0; co < emails_adm.Count; co++)
                    {
                        derivate_mail_recipient = $"{derivate_mail_recipient}{emails_adm[co].id}@continental.edu.pe,";
                    }
                    derivate_mail_recipient = derivate_mail_recipient.Trim();
                    derivate_mail_recipient = derivate_mail_recipient.TrimEnd(',');

                    /*==================================================================================*/

                    /*========================Guardar derivación a Area Legal===============================*/
                    derivate_options.page_id = answer.page_id;
                    // derivate_options.derivate_person_creator_pidm = answer.derivate_person_creator_pidm;
                    derivate_options.derivate_funtional_unity = _ldr_states.LDR_CODE_FUNTIONAL_UNITY_LEGAL_AREA; //--> código de UC-HYO-SAC-RECTORADO-SECRETARIA GENERAL-DIRECCIÓN  //answer.derivate_funtional_unity;
                    derivate_options.derivate_mail_recipient = derivate_mail_recipient;
                    derivate_options.derivate_type = _ldr_states.LDR_TYPE_DERIVATE_LEGAL_AREA;
                    bool save_derivate = await derivate_options.saveDerivation();
                    if (!save_derivate)
                    {
                        throw new Exception("Error al guardar Derivación para Area Legal");
                    }
                    /*=======================================================================================*/

                    /*========================Guardar personal a cargo de Area Legal===============================*/

                    Employee collaborator_options = new Employee();
                    collaborator_options = await collaborator_options.getColaboratosByPidm(_ldr_states.LDR_ID_COMPANY_UCCI, int.Parse(_ldr_states.LDR_PIDM_BOOS_UNITY_LEGAL_AREA)); // --> obtener puesto del jefe de area legal



                    StaffInvolved staff_involved_legal_area = new StaffInvolved();
                    staff_involved_legal_area.page_id = answer.page_id;
                    staff_involved_legal_area.derivate_id = derivate_options.derivate_id; // CÓDIGO DE DERIVACIÓN AL AREA LEGAL
                    staff_involved_legal_area.staff_involved_pidm = _ldr_states.LDR_PIDM_BOOS_UNITY_LEGAL_AREA; // -> PIDM DE ARMANDO PRIETO
                    staff_involved_legal_area.staff_position_company = collaborator_options.puesto_organica; //00000183 --> código de UC-HYO-SAC-RECTORADO-SECRETARIA GENERAL-DIRECCIÓN
                    staff_involved_legal_area.staff_involved_note = "Para su atención."; // --> nota de derivación
                    staff_involved_legal_area.staff_involved_type = _ldr_states.LDR_TYPE_INVOLVED_BOSS;
                    bool save_staff_involved = await staff_involved_legal_area.saveStaffInvolved();
                    if (!save_staff_involved)
                    {
                        throw new Exception("Error al guardar personal a Cargo del area legal");
                    }

                    /*PageClaimBook page_answer = new PageClaimBook();
                    page_answer.page_id = answer.page_id;
                    page_answer.page_state = _ldr_states.LDR_STATE_PAGE_ANSWERED;
                    bool change_page = page_answer.updatePage();
                    if (!change_page)
                    {
                        throw new Exception("Error al responder Reclamo");
                    }*/ // YA SE ESTA HACIENDO AL GUARDAR DERIVACIÓN
                    /*=============================================================================================*/
                }

                if (derivation.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_LEGAL_AREA)
                {
                    PageClaimBook page_answer = new PageClaimBook();
                    page_answer.page_id = answer.page_id;
                    page_answer.page_state = _ldr_states.LDR_STATE_PAGE_RESOLVED;
                    bool change_page = page_answer.updatePage();
                    if (!change_page)
                    {
                        throw new Exception("Error al resolver Reclamo");
                    }
                }

                return true;
            }


            /*===================MODIFICAR OTRAS RESPUESTAS DE LA DERIVACIÓN===========================*/
            List<LDRtblAnswerPage> list_other_answer_derivation = _db.ldr_tblAnswerPage
                .Where(
                    result =>
                        result.page_id == answer.page_id &&
                        result.derivate_id == answer.derivate_id &&
                        result.answer_id != answer.answer_id
                ).ToList();

            /*if (list_other_answer_derivation != null && list_other_answer_derivation.Count>0)
            {
                list_other_answer_derivation.ForEach(other_answer =>
                {
                    other_answer.answer_state = _ldr_states.LDR_STATE_ANSWER_REJECTED;
                });
                _db.SaveChanges();
                ///Si es necesario Ejecutar Funcion que rechaza respuestas
            }*/
            /*==========================================================================================*/

            return true;
          
        }



        public async Task<bool> acceptAnswerAdministrator(int answer_id, string pidm_validate)
        {
            LDRtblAnswerPage answer = _db.ldr_tblAnswerPage.Find(answer_id);
            if (answer == null)
            {
                throw new Exception("Error al obtener información de la respuesta.");
            }
            LDRtblDerivationArea derivation = _db.ldr_tblDerivationArea.Find(answer.derivate_id);
            if (derivation == null)
            {
                throw new Exception("Error al obtener información de la derivación");
            }
            /*==================VALIDACIÓNES================*/


            if (answer.answer_state != _ldr_states.LDR_STATE_ANSWER_ACCEPTED)
            {
                throw new Exception("La respuesta no fue aceptada.");
            }
            /*==========================================================*/
            //answer.answer_person_validate_pidm = pidm_validate;
            answer.answer_state = _ldr_states.LDR_STATE_ANSWER_ACCEPTED_FINISH;
            answer.answer_date_validate = DateTime.Now;

            int update_answer = _db.SaveChanges();

            if (update_answer <= 0)
            {
                throw new Exception("No se pudo modificar la respuesta.");
            }
            PageClaimBook page_answer = new PageClaimBook();
            page_answer.page_id = answer.page_id;
            page_answer.page_state = _ldr_states.LDR_STATE_PAGE_FINALIZED;
            bool change_page = page_answer.updatePage();

            if (!change_page)
            {
                throw new Exception("No pudo modificar el reclamo.");
            }
            this.answer_id = answer.answer_id;

            SatisfactionSurvey satisfaction_survey = new SatisfactionSurvey();
            satisfaction_survey.page_id = answer.page_id;
            bool save_survey = await satisfaction_survey.generateSurvey();

            return true;
        }
        /// <summary>
        /// RECHAZAR RESPUESTA
        /// </summary>
        /// <returns></returns>
        public async Task<bool> rejectAnswerAdministrator(int answer_id, string pidm_validate) {
            LDRtblAnswerPage answer = new LDRtblAnswerPage();
            answer = _db.ldr_tblAnswerPage.Find(answer_id);
            PageClaimBook page_answer = new PageClaimBook();

            LDRtblDerivationArea derivation = _db.ldr_tblDerivationArea.Find(answer.derivate_id);

            answer = _db.ldr_tblAnswerPage.Find(this.answer_id);
            /*==================VALIDACIÓNES================*/
            if (answer == null || answer.answer_state != _ldr_states.LDR_STATE_ANSWER_ACCEPTED)
            {
                throw new Exception("La respuesta no fue aceptada.");
            }
            /*==============================================*/

            page_answer.page_id = answer.page_id;
            page_answer.page_state = _ldr_states.LDR_STATE_PAGE_ANSWERED;
            bool change_page = page_answer.updatePage();
            if (!change_page)
            {
                throw new Exception("Error al Cambiar reclamo.");
            }

            derivation.derivate_state = _ldr_states.LDR_STATE_DERIVATE_PENDING;
            derivation.derivate_date_modify = DateTime.Now;
            int change_derivation = _db.SaveChanges();

            if (change_derivation <= 0)
            {
                throw new Exception("No se pudo modificar la derivación.");
            }

            List<LDRtblAnswerPage> list_answers_derivation = _db.ldr_tblAnswerPage
                .Where(
                    result =>
                        result.page_id == answer.page_id &&
                        result.derivate_id == answer.derivate_id
                ).ToList();

            if (list_answers_derivation != null && list_answers_derivation.Count>0)
            {
                list_answers_derivation.ForEach(item_answer =>
                {
                    item_answer.answer_state = _ldr_states.LDR_STATE_ANSWER_REJECTED;
                });
                _db.SaveChanges();
            }
            
            

            return true;



        }


        public async Task<bool> saveAnswerAdministrator()
        {
            LDRtblAnswerPage answer = new LDRtblAnswerPage();
            PageClaimBook page = new PageClaimBook();

            answer.page_id = this.page_id;
            answer.answer_person_creator_pidm = this.answer_person_creator_pidm;
            answer.answer_note = this.answer_note;
            answer.answer_state = _ldr_states.LDR_STATE_ANSWER_ACCEPTED_FINISH;
            answer.answer_date_created = DateTime.Now;
            answer.answer_person_validate_pidm = this.answer_person_creator_pidm;
            answer.answer_date_validate = DateTime.Now;

            /*==================VALIDACIÓNES================*/

            AppUser app_user = new AppUser();
            List<AppUser> list_user = app_user.listUserByModule("bo_claims-book-adm");
            AppUser validate_user = list_user.Find(x => x.pid.ToString() == this.answer_person_creator_pidm);
            if (validate_user == null)
            {
                throw new Exception("Usted no tiene permisos de administrador.");
            }
            /*==============================================*/

            _db.ldr_tblAnswerPage.Add(answer);
            int save_answer = _db.SaveChanges();

            if (save_answer <= 0)
            {
                throw new Exception("No se pudo Agregar la respuesta.");
            }

            page.page_id = answer.page_id;
            page.page_state = _ldr_states.LDR_STATE_PAGE_FINALIZED;
            bool change_page = page.updatePage();
            if (!change_page)
            {
                throw new Exception("No se pudo modificar el reclamo.");
            }

           /* SatisfactionSurvey satisfaction_survey = new SatisfactionSurvey();
            satisfaction_survey.page_id = answer.page_id;
            bool save_survey = await satisfaction_survey.generateSurvey();*/
            this.answer_id = answer.answer_id;
            return true;

        }



        /// <summary>
        /// ACEPTAR RESPUESTA
        /// </summary>
        /// <returns></returns>
        public async Task<bool> acceptAnswerbck()
        {
            LDRtblAnswerPage answer = _db.ldr_tblAnswerPage.Find(this.answer_id);
            PageClaimBook page_answer = new PageClaimBook();

            /*==================VALIDACIÓNES================*/
            if (answer == null || answer.answer_state != _ldr_states.LDR_STATE_ANSWER_WAIT)
            {
                throw new Exception("La respuesta no esta en espera.");
            }
            /*==============================================*/
            answer.answer_person_validate_pidm = this.answer_person_validate_pidm;
            answer.answer_state = _ldr_states.LDR_STATE_ANSWER_ACCEPTED;
            answer.answer_date_validate = DateTime.Now;
            
            int update_answer = _db.SaveChanges();

            if (update_answer > 0)
            {
                page_answer.page_id = answer.page_id;
                page_answer.page_state = _ldr_states.LDR_STATE_PAGE_FINALIZED;
                bool change_page = page_answer.updatePage();
                if (change_page)
                {
                    this.answer_id = answer.answer_id;

                    SatisfactionSurvey satisfaction_survey = new SatisfactionSurvey();
                    satisfaction_survey.page_id = answer.page_id;
                    bool save_survey = await satisfaction_survey.generateSurvey();

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        public static implicit operator AnswerPage(LDRtblAnswerPage temp)
        {
            if (temp == null) return null;
            AnswerPage item = new AnswerPage();
            item.answer_id = temp.answer_id;
            item.page_id = temp.page_id;
            item.derivate_id = temp.derivate_id;
            item.answer_person_creator_pidm = temp.answer_person_creator_pidm;
            item.answer_person_validate_pidm = temp.answer_person_validate_pidm;
            item.answer_note = temp.answer_note;
            item.answer_state = temp.answer_state;
            item.answer_date_created = temp.answer_date_created;
            item.answer_date_validate = temp.answer_date_validate;
            return item;
        }
    }
}
