﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.LDR;
using ACTools.Configuration;
using ACTools.Mail;
using _ldr_states = ACTools.Constants.ClaimsBookConstants;
namespace ACBusiness.ComplaintsBook
{
    public class SatisfactionSurvey
    {
        #region Propiedades
        public int survey_id { get; set; }
        public int page_id { get; set; }
        public int? survey_answer { get; set; }
        public string survey_state { get; set; }
        public DateTime survey_date_create { get; set; }
        public DateTime? survey_date_resolved { get; set; }
        #endregion

        NEXOContext _db = new NEXOContext();
        public async Task<bool> generateSurvey() {
            LDRtblSatisfactionSurveyClaim survey = new LDRtblSatisfactionSurveyClaim();
            survey.page_id = this.page_id;
            survey.survey_state = _ldr_states.LDR_STATE_SATISFACTION_SURVEY_PENDING;
            survey.survey_date_create = DateTime.Now;
            _db.ldr_tblSatisfactionSurvey.Add(survey);
            int save_survey = _db.SaveChanges();

            PageClaimBook page = new PageClaimBook();
            PageClaimBook page_details = await page.getPageClaimsBook(this.page_id);


            if (save_survey > 0 && page_details.page_email != null && page_details.page_email.Trim().Length > 0)
            {
                string urlTest = "http://librodereclamacionestest.continental.edu.pe";
                /*===============Validar producción===============*/
                string env = AppSettings.app_env;
                
                if (env == "prod")
                {
                    urlTest = "http://librodereclamaciones.continental.edu.pe";
                }
                /*================================================*/

                this.survey_id = survey.survey_id;

                

                MailSender mail = new MailSender();
                if (page_details.page_state == _ldr_states.LDR_STATE_PAGE_DISMISSED)
                {

                    mail.compose("ldr_dismissed_claim",
                        new
                        {
                            year = page_details.page_year,
                            numeration = page_details.page_numeration,
                            survey_id = survey.survey_id,
                            note_dismissed = page_details.page_note_dissmised,
                            urltestsatisfaction = urlTest,
                        })
                        .destination(page_details.page_email, $"[Respuesta][{page_details.page_type_well}][{page_details.page_year}][{page_details.page_numeration}]")
                        .send();
                }
                else {
                    AnswerPage answer_page = new AnswerPage();
                    AnswerPage answer_page_details = await answer_page.getAnswerPage(this.page_id);

                    FilePage files_page = new FilePage();
                    List<FilePage> files_answer_page = await files_page.ListFileAnswerClaim(answer_page_details.answer_id);

                    if (files_answer_page != null && files_answer_page.Count > 0)
                    {
                        foreach (FilePage item in files_answer_page)
                        {
                            mail.addFile($"{AppSettings.reports["output-ldr"]}{item.file_route}");
                        }
                    }
                    mail.compose("ldr_finalized_claim",
                        new
                        {
                            year = page_details.page_year,
                            numeration = page_details.page_numeration,
                            survey_id = survey.survey_id,
                            urltestsatisfaction = urlTest,
                        })
                        .destination(page_details.page_email, $"[Respuesta][{page_details.page_type_well}][{page_details.page_year}][{page_details.page_numeration}]")
                        .send();
                }

                

                return true;
            }
            else
            {
                return false;
            }
        }


        public async Task<bool> answerSurvey(int survey_id, int valor) {
            LDRtblSatisfactionSurveyClaim survey = _db.ldr_tblSatisfactionSurvey.Find(survey_id);
            if (survey.survey_state != _ldr_states.LDR_STATE_SATISFACTION_SURVEY_RESOLVED && (valor == 0 || valor == 1))
            {
                survey.survey_answer = valor;
                survey.survey_state = _ldr_states.LDR_STATE_SATISFACTION_SURVEY_RESOLVED;
                survey.survey_date_resolved = DateTime.Now;
                int update_survey = _db.SaveChanges();
                if (update_survey > 0)
                {
                    return true;
                }
                else {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

    }
}
