﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Aplication;
using ACBusiness.ClaimsBook;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.LDR;
using ACTools.Configuration;
using ACTools.Mail;
using ACTools.PDF.Documents;
using _page_states = ACTools.Constants.ClaimsBookConstants;

namespace ACBusiness.ComplaintsBook
{
    public class PageClaimBook
    {
        #region propiedades
        public int page_id { get; set; }
        public string page_div { get; set; }
        public string page_origin { get; set; }
        public int page_year { get; set; }
        public string page_campus { get; set; }
        public string page_bussines_unity { get; set; }
        public int page_numeration { get; set; }
        public DateTime page_date_create { get; set; }
        public string page_email { get; set; }
        public string page_first_name { get; set; }
        public string page_last_name { get; set; }
        public string page_identification { get; set; }
        public string page_phone { get; set; }
        public string page_home { get; set; }
        public string page_legal_guardian { get; set; }
        public string page_type_well { get; set; }
        public string page_description_well { get; set; }
        public string page_type_claim { get; set; }
        public string page_description_claim { get; set; }
        public DateTime? page_date_finalized { get; set; }
        public string page_state { get; set; }
        public string page_register_user_pidm { get; set; }
        public string page_note_dissmised { get; set; }
        //public string page_final_response { get; set; }

        public string page_person_claim { get; set; }
        public bool? page_avoid { get; set; }
        public string page_avoid_description { get; set; }

        public string page_person_claim_names { get; set; }

        #endregion propiedades

        #region propiedades staffinvolved
        public int? derivate_id { get; set; }
        public string staff_involved_pidm { get; set; }
        public string staff_involved_type { get; set; }
        public string staff_derivate_state { get; set; }
        #endregion propiedades staffinvolved

        #region propiedades derivate
        public string derivate_state { get; set; }
        #endregion

        #region propiedades extras
        public string campus_name { get; set; }
        #endregion


        NEXOContext _db = new NEXOContext();

        /// <summary>
        /// Lista Reclamos para administradores
        /// </summary>
        /// <param name="page_div">Dependencia</param>
        /// <param name="page_campus">Sede</param>
        /// <returns>retorna lista de reclamos para CAU</returns>
        public async Task <List<PageClaimBook>> listPageClaimsBookCAU(string page_div, string page_campus) 
        {
            List<PageClaimBook> peding_claims_book_list = _db.ldr_TblPageClaimBook
                .Join(
                    _db.dim_campus,
                    tpc => new { id_campus = tpc.page_campus },
                    dc => new { id_campus = dc.campus },
                    (tpc, dc) => new { tpc, dc })
                .Where(lpcb =>
                    //lpcb.page_state == page_state &&
                    lpcb.tpc.page_div == page_div //&&
                    //lpcb.tpc.page_campus == page_campus
                ).ToList().OrderByDescending(lpcblist =>
                    lpcblist.tpc.page_date_create
                ).Select(lpcblist => new PageClaimBook
                {
                    page_id = lpcblist.tpc.page_id,
                    page_div = lpcblist.tpc.page_div,
                    page_year = lpcblist.tpc.page_year,
                    page_campus = lpcblist.tpc.page_campus,
                    page_bussines_unity = lpcblist.tpc.page_bussines_unity,
                    page_numeration = lpcblist.tpc.page_numeration,
                    page_date_create = lpcblist.tpc.page_date_create,
                    page_email = lpcblist.tpc.page_email,
                    page_first_name = lpcblist.tpc.page_first_name,
                    page_last_name = lpcblist.tpc.page_last_name,
                    page_identification = lpcblist.tpc.page_identification,
                    page_phone = lpcblist.tpc.page_phone,
                    page_home = lpcblist.tpc.page_home,
                    page_legal_guardian = lpcblist.tpc.page_legal_guardian,
                    page_type_well = lpcblist.tpc.page_type_well,
                    page_description_well = lpcblist.tpc.page_description_well,
                    page_type_claim = lpcblist.tpc.page_type_claim,
                    page_description_claim = lpcblist.tpc.page_description_claim,
                    page_date_finalized = lpcblist.tpc.page_date_finalized,
                    page_state = lpcblist.tpc.page_state,
                    page_register_user_pidm = lpcblist.tpc.page_register_user_pidm,
                    page_origin = lpcblist.tpc.page_origin,
                    //physical_number_page = lpcblist.tpc.physical_number_page,
                    page_note_dissmised = lpcblist.tpc.page_note_dissmised,
                    //page_final_response = lpcblist.tpc.page_final_response
                    campus_name = lpcblist.dc.campusDescription,


                    page_person_claim = lpcblist.tpc.page_person_claim,
                    page_avoid = lpcblist.tpc.page_avoid,
                    page_avoid_description = lpcblist.tpc.page_avoid_description,

                }
                ).ToList();


            return peding_claims_book_list;
        }

        /// <summary>
        /// Lista reclamos para area involucrada
        /// </summary>
        /// <param name="page_div">Dependencia</param>
        /// <param name="user_pidm">código de persona</param>
        /// <returns></returns>
        public async Task<List<PageClaimBook>> listPageClaimsInvolvedArea(string page_div,string user_pidm) {
            List<PageClaimBook> list_page_claims = _db.ldr_TblPageClaimBook
                .Join(
                    _db.dim_campus,
                    tpc => new { id_campus = tpc.page_campus },
                    dc => new { id_campus = dc.campus },
                    (tpc, dc) => new { tpc, dc })
                .Join(
                    _db.ldr_tblStaffInvolved,
                    tpc2 => new { page_id = tpc2.tpc.page_id },
                    tsi => new { page_id = tsi.page_id },
                    (tpc2, tsi) => new { tpc2.tpc, tpc2.dc, tsi })
                .Join(
                    _db.ldr_tblDerivationArea,
                    tsi3 => new { page_id = tsi3.tsi.page_id, derivate_id = tsi3.tsi.derivate_id },
                    tda => new { page_id = tda.page_id, derivate_id = tda.derivate_id },
                    (tsi2, tda) => new { tsi2.tpc, tsi2.dc, tsi2.tsi, tda })
                .Where(result =>
                   result.tpc.page_div == page_div &&
                   result.tsi.staff_involved_pidm == user_pidm &&
                   result.tda.derivate_type == _page_states.LDR_TYPE_DERIVATE_INVOLVED_AREA &&
                   result.tpc.page_state != _page_states.LDR_STATE_PAGE_DISMISSED.ToString()
                    /*result.tda.derivate_state == _page_states.LDR_STATE_DERIVATE_PENDING &&
                    result.tpc.page_state != _page_states.LDR_STATE_PAGE_DISMISSED.ToString() &&
                    result.tpc.page_state != _page_states.LDR_STATE_PAGE_RESOLVED.ToString() &&
                    result.tpc.page_state != _page_states.LDR_STATE_PAGE_ANSWERED.ToString()*/
                ).ToList().OrderByDescending(result =>
                   result.tpc.page_date_create
                ).Select(result => new PageClaimBook
                {
                    page_id = result.tpc.page_id,
                    page_div = result.tpc.page_div,
                    page_year = result.tpc.page_year,
                    page_campus = result.tpc.page_campus,
                    page_bussines_unity = result.tpc.page_bussines_unity,
                    page_numeration = result.tpc.page_numeration,
                    page_date_create = result.tpc.page_date_create,
                    page_email = result.tpc.page_email,
                    page_first_name = result.tpc.page_first_name,
                    page_last_name = result.tpc.page_last_name,
                    page_identification = result.tpc.page_identification,
                    page_phone = result.tpc.page_phone,
                    page_home = result.tpc.page_home,
                    page_legal_guardian = result.tpc.page_legal_guardian,
                    page_type_well = result.tpc.page_type_well,
                    page_description_well = result.tpc.page_description_well,
                    page_type_claim = result.tpc.page_type_claim,
                    page_description_claim = result.tpc.page_description_claim,
                    page_date_finalized = result.tpc.page_date_finalized,
                    page_state = result.tpc.page_state,
                    page_register_user_pidm = result.tpc.page_register_user_pidm,
                    page_origin = result.tpc.page_origin,
                    page_note_dissmised = result.tpc.page_note_dissmised,
                    //page_final_response = result.tpc.page_final_response,

                    derivate_id = result.tsi.derivate_id,
                    staff_involved_pidm  = result.tsi.staff_involved_pidm,
                    staff_involved_type = result.tsi.staff_involved_type,
                    staff_derivate_state = result.tsi.staff_derivate_state,

                    derivate_state = result.tda.derivate_state,

                    campus_name = result.dc.campusDescription,


                    page_person_claim = result.tpc.page_person_claim,
                    page_avoid = result.tpc.page_avoid,
                    page_avoid_description = result.tpc.page_avoid_description,

                }).ToList();

            PageClaimBook item;
            for (int i = list_page_claims.Count - 1; i >= 0; i--)
            {
                item = list_page_claims[i];
                if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_PENDING && item.staff_derivate_state == _page_states.LDR_STATE_STAFF_DERIVATED)
                {
                    item.page_state = _page_states.LDR_STATE_PAGE_DERIVATED;
                }
                else if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_PENDING && item.staff_derivate_state == _page_states.LDR_STATE_STAFF_NOT_DERIVATED)
                {
                    item.page_state = _page_states.LDR_STATE_PAGE_PENDING;
                }
                else if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_RESOLVED && item.staff_involved_type == _page_states.LDR_TYPE_INVOLVED_BOSS)
                {
                    item.page_state = _page_states.LDR_STATE_DERIVATE_RESOLVED;
                }
                else if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_FINALIZED)
                {
                    item.page_state = _page_states.LDR_STATE_DERIVATE_FINALIZED;
                }
                else
                {
                    list_page_claims.RemoveAt(i);
                }
            }
            /* foreach (PageClaimBook item in list_page_claims)
            {
                if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_PENDING && item.staff_derivate_state == _page_states.LDR_STATE_STAFF_DERIVATED)
                {
                    item.page_state = _page_states.LDR_STATE_PAGE_DERIVATED;
                }
                else if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_PENDING && item.staff_derivate_state == _page_states.LDR_STATE_STAFF_NOT_DERIVATED)
                {
                    item.page_state = _page_states.LDR_STATE_PAGE_PENDING;
                }
                else if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_RESOLVED && item.staff_involved_type == _page_states.LDR_TYPE_INVOLVED_BOSS)
                 {
                    item.page_state = _page_states.LDR_STATE_DERIVATE_RESOLVED;
                }
                else if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_FINALIZED && item.staff_involved_type == _page_states.LDR_TYPE_INVOLVED_BOSS)
                {
                    item.page_state = _page_states.LDR_STATE_DERIVATE_FINALIZED;
                }
                else
                {
                    list_page_claims.RemoveAt[i]
                }
            }*/
            return list_page_claims;
        }

        /// <summary>
        /// Lista reclamos para area legal
        /// </summary>
        /// <returns>Lista re reclamos area legal</returns>
        public async Task<List<PageClaimBook>> listPageClaimsLegalArea(string page_div, string user_pidm)
        {
            List<PageClaimBook> list_pages = _db.ldr_tblStaffInvolved
                .Join(
                    _db.ldr_tblDerivationArea,
                    tsi => new { page_id = tsi.page_id, derivate_id = tsi.derivate_id },
                    tda => new { page_id = tda.page_id, derivate_id = tda.derivate_id },
                    (tsi, tda) => new { tsi, tda }
                )
                .Join(
                    _db.ldr_TblPageClaimBook,
                    tsi => new { page_id = tsi.tsi.page_id },
                    tpc => new { page_id = tpc.page_id },
                    (tsi, tpc) => new { tsi.tsi, tsi.tda, tpc }
                )
                .Join(
                    _db.dim_campus,
                    tpc => new { id_campus = tpc.tpc.page_campus },
                    dc => new { id_campus = dc.campus },
                    (tpc, dc) => new { tpc.tsi, tpc.tda, tpc.tpc, dc }
                 )
                .Where(
                    result =>
                        result.tpc.page_div == page_div &&
                        result.tsi.staff_involved_pidm == user_pidm &&
                        result.tpc.page_state != _page_states.LDR_STATE_PAGE_DISMISSED.ToString() &&
                        result.tda.derivate_type == _page_states.LDR_TYPE_DERIVATE_LEGAL_AREA /*&&
                        result.tpc.page_state == _page_states.LDR_STATE_PAGE_ANSWERED*/
                )
                .OrderByDescending(
                    result =>
                        result.tpc.page_date_create
                )
                .Select(
                    result => new PageClaimBook
                    {
                        page_id = result.tpc.page_id,
                        page_div = result.tpc.page_div,
                        page_year = result.tpc.page_year,
                        page_campus = result.tpc.page_campus,
                        page_bussines_unity = result.tpc.page_bussines_unity,
                        page_numeration = result.tpc.page_numeration,
                        page_date_create = result.tpc.page_date_create,
                        page_email = result.tpc.page_email,
                        page_first_name = result.tpc.page_first_name,
                        page_last_name = result.tpc.page_last_name,
                        page_identification = result.tpc.page_identification,
                        page_phone = result.tpc.page_phone,
                        page_home = result.tpc.page_home,
                        page_legal_guardian = result.tpc.page_legal_guardian,
                        page_type_well = result.tpc.page_type_well,
                        page_description_well = result.tpc.page_description_well,
                        page_type_claim = result.tpc.page_type_claim,
                        page_description_claim = result.tpc.page_description_claim,
                        page_date_finalized = result.tpc.page_date_finalized,
                        page_state = result.tpc.page_state,
                        page_register_user_pidm = result.tpc.page_register_user_pidm,
                        page_origin = result.tpc.page_origin,
                        page_note_dissmised = result.tpc.page_note_dissmised,
                        //page_final_response = result.tpc.page_final_response,

                        derivate_id = result.tsi.derivate_id,
                        staff_involved_pidm = result.tsi.staff_involved_pidm,
                        staff_involved_type = result.tsi.staff_involved_type,
                        staff_derivate_state = result.tsi.staff_derivate_state,

                        derivate_state = result.tda.derivate_state,

                        campus_name = result.dc.campusDescription,


                        page_person_claim = result.tpc.page_person_claim,
                        page_avoid = result.tpc.page_avoid,
                        page_avoid_description = result.tpc.page_avoid_description,
                    }
                ).ToList();

            PageClaimBook item;
            for (int i = list_pages.Count - 1; i >= 0; i--)
            {
                item = list_pages[i];
                if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_PENDING && item.staff_derivate_state == _page_states.LDR_STATE_STAFF_DERIVATED)
                {
                    item.page_state = _page_states.LDR_STATE_PAGE_DERIVATED;
                }
                else if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_PENDING && item.staff_derivate_state == _page_states.LDR_STATE_STAFF_NOT_DERIVATED)
                {
                    item.page_state = _page_states.LDR_STATE_PAGE_PENDING;
                }
                else if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_RESOLVED && item.staff_involved_type == _page_states.LDR_TYPE_INVOLVED_BOSS)
                {
                    item.page_state = _page_states.LDR_STATE_DERIVATE_RESOLVED;
                }
                else if (item.derivate_state == _page_states.LDR_STATE_DERIVATE_FINALIZED)
                {
                    item.page_state = _page_states.LDR_STATE_DERIVATE_FINALIZED;
                }
                else
                {
                    list_pages.RemoveAt(i);
                }
            }

            return list_pages;
        }

        /// <summary>
        /// Obtener datos de un reclamo especifico
        /// </summary>
        /// <param name="page_id">código unico de reclamo</param>
        /// <returns>retorna reclamo</returns>
        public async Task<PageClaimBook> getPageClaimsBook(int page_id, string user_pidm = null )
        {

            PageClaimBook peding_claims_book_list = _db.ldr_TblPageClaimBook
                .Join(
                    _db.dim_campus,
                    tpc => new { id_campus = tpc.page_campus },
                    dc => new { id_campus = dc.campus },
                    (tpc, dc) => new { tpc, dc })
                .Where(lpcb =>
                    lpcb.tpc.page_id == page_id
                ).ToList().OrderBy(lpcblist =>
                    lpcblist.tpc.page_date_create
                ).Select(lpcblist => new PageClaimBook
                {
                    page_id = lpcblist.tpc.page_id,
                    page_div = lpcblist.tpc.page_div,
                    page_year = lpcblist.tpc.page_year,
                    page_campus = lpcblist.tpc.page_campus,
                    page_bussines_unity = lpcblist.tpc.page_bussines_unity,
                    page_numeration = lpcblist.tpc.page_numeration,
                    page_date_create = lpcblist.tpc.page_date_create,
                    //area = lpcblist.tpc.area,
                    //sub_area = lpcblist.tpc.sub_area,
                    page_email = lpcblist.tpc.page_email,
                    page_first_name = lpcblist.tpc.page_first_name,
                    page_last_name = lpcblist.tpc.page_last_name,
                    page_identification = lpcblist.tpc.page_identification,
                    page_phone = lpcblist.tpc.page_phone,
                    page_home = lpcblist.tpc.page_home,
                    page_legal_guardian = lpcblist.tpc.page_legal_guardian,
                    page_type_well = lpcblist.tpc.page_type_well,
                    page_description_well = lpcblist.tpc.page_description_well,
                    page_type_claim = lpcblist.tpc.page_type_claim,
                    page_description_claim = lpcblist.tpc.page_description_claim,
                    page_date_finalized = lpcblist.tpc.page_date_finalized,
                    page_state = lpcblist.tpc.page_state,
                    page_register_user_pidm = lpcblist.tpc.page_register_user_pidm,
                    page_origin = lpcblist.tpc.page_origin,
                    //physical_number_page = lpcblist.tpc.physical_number_page,
                    page_note_dissmised = lpcblist.tpc.page_note_dissmised,
                    //page_final_response = lpcblist.tpc.page_final_response

                    campus_name = lpcblist.dc.campusDescription,


                    page_person_claim = lpcblist.tpc.page_person_claim,
                    page_avoid = lpcblist.tpc.page_avoid,
                    page_avoid_description = lpcblist.tpc.page_avoid_description,
                }
                ).FirstOrDefault();

            if (user_pidm != null)
            {
                /*
                .Join(
                _db.ldr_tblDerivationArea,
                tsi2 => new { page_id = tsi2.tsi.page_id, derivate_id = tsi2.tsi.derivate_id },
                tda => new { page_id = tda.page_id, derivate_id = tda.derivate_id },
                (tsi2, tda) => new { tsi2.tpc, tsi2.tsi, tda }
                 */

                LDRtblStaffInvolved staffs = _db.ldr_tblStaffInvolved.Join(
                _db.ldr_tblDerivationArea,
                tsi => new { page_id = tsi.page_id, derivate_id = tsi.derivate_id },
                tda => new { page_id = tda.page_id, derivate_id = tda.derivate_id },
                (tsi, tda) => new { tsi, tda }
                ).Where(result =>
                result.tsi.page_id == peding_claims_book_list.page_id &&
                result.tda.derivate_state != _page_states.LDR_STATE_DERIVATE_DISMISSED &&
                result.tsi.staff_involved_pidm == user_pidm).ToList().Select(result => new LDRtblStaffInvolved
                {
                    derivate_id = result.tsi.derivate_id,
                    staff_involved_pidm = result.tsi.staff_involved_pidm,
                    staff_involved_type = result.tsi.staff_involved_type,
                    staff_derivate_state  = result.tsi.staff_derivate_state
                }).FirstOrDefault();


                if (staffs != null)
                {
                    peding_claims_book_list.derivate_id = staffs.derivate_id;
                    peding_claims_book_list.staff_involved_pidm = staffs.staff_involved_pidm;
                    peding_claims_book_list.staff_involved_type = staffs.staff_involved_type;
                    peding_claims_book_list.staff_derivate_state = staffs.staff_derivate_state;
                }
            }

            if (peding_claims_book_list != null && peding_claims_book_list.page_person_claim != null && peding_claims_book_list.page_person_claim.ToString().Length > 0)
            {
                DBOContext bducci = new DBOContext();
                DBOTblPersona person = bducci.tbl_persona.Where(result =>
                    result.IDPersonaN.ToString() == peding_claims_book_list.page_person_claim
                    ).FirstOrDefault();
                if (person != null)
                {
                    peding_claims_book_list.page_person_claim_names = $"{person.Nombres} {person.Appat} {person.ApMat}";
                }
            }
            
            

            return peding_claims_book_list;
        }

        /// <summary>
        /// Retorna lista de reclamos que compartan div, sede, y numeración
        /// </summary>
        /// <param name="page_div">Dependencia</param>
        /// <param name="page_campus">Sede</param>
        /// <param name="page_numeration">Numero</param>
        /// <returns></returns>
        public List<PageClaimBook> listPageClaimsBookNumeration(string page_div, string page_campus, int page_numeration)
        {
            int year = int.Parse(DateTime.Now.Year.ToString());
            List<PageClaimBook> peding_claims_book_list = _db.ldr_TblPageClaimBook.Where(result =>
                    result.page_div == page_div &&
                    result.page_campus == page_campus &&
                    result.page_numeration == page_numeration &&
                    result.page_year == year
                ).ToList().OrderBy(lpcblist =>
                    lpcblist.page_date_create
                ).Select(result => new PageClaimBook
                {
                    page_id = result.page_id,
                    page_div = result.page_div,
                    page_year = result.page_year,
                    page_campus = result.page_campus,
                    page_bussines_unity = result.page_bussines_unity,
                    page_numeration = result.page_numeration,
                    page_date_create = result.page_date_create,
                    page_email = result.page_email,
                    page_first_name = result.page_first_name,
                    page_last_name = result.page_last_name,
                    page_identification = result.page_identification,
                    page_phone = result.page_phone,
                    page_home = result.page_home,
                    page_legal_guardian = result.page_legal_guardian,
                    page_type_well = result.page_type_well,
                    page_description_well = result.page_description_well,
                    page_type_claim = result.page_type_claim,
                    page_description_claim = result.page_description_claim,
                    page_date_finalized = result.page_date_finalized,
                    page_state = result.page_state,
                    page_register_user_pidm = result.page_register_user_pidm,
                    page_origin = result.page_origin,
                    page_note_dissmised = result.page_note_dissmised,


                    page_person_claim = result.page_person_claim,
                    page_avoid = result.page_avoid,
                    page_avoid_description = result.page_avoid_description,
                    //page_final_response = result.page_final_response
                }
                ).ToList();
            return peding_claims_book_list;
        }



        /// <summary>
        /// obtener numeración para reclamo digital
        /// </summary>
        /// <returns>retorna numero maximo mas uno de la numeración actual</returns>
        public int getPageNumerationPageDigital()
        {
            int year = int.Parse(DateTime.Now.Year.ToString());
            int max_number = _db.ldr_TblPageClaimBook.Where(result => result.page_origin == _page_states.LDR_TYPE_ORIGIN_PAGE_DIGITAL && result.page_year == year)
                .Select(result => result.page_numeration).DefaultIfEmpty(0).Max();
            return max_number + 1;
        }

        /// <summary>
        /// Guarda un nuevo reclamo
        /// </summary>
        /// <returns>retorna si se guardo true</returns>
        public async Task<bool> savePage()
        {
            LDRtblPageClaimBook ldr = new LDRtblPageClaimBook();
            //ldr.id = this.id;
            ldr.page_div = this.page_div;
            
            ldr.page_campus = this.page_campus;
            ldr.page_bussines_unity = this.page_bussines_unity;

            if (this.page_origin == _page_states.LDR_TYPE_ORIGIN_PAGE_DIGITAL)
            {
                ldr.page_year = int.Parse(DateTime.Now.Year.ToString());
                ldr.page_numeration = getPageNumerationPageDigital();
                ldr.page_date_create = DateTime.Now;
            }
            else if (this.page_origin == _page_states.LDR_TYPE_ORIGIN_PAGE_PHYSICAL)
            {
                ldr.page_year = int.Parse(page_date_create.Year.ToString());
                ldr.page_date_create = this.page_date_create;
                List<PageClaimBook> list_pages = this.listPageClaimsBookNumeration(this.page_div, this.page_campus, this.page_numeration);
                if (list_pages.Count <= 0)
                {
                    ldr.page_numeration = this.page_numeration;
                }
                else
                {
                    throw new Exception("Existe un reclamo con esa numeración");
                }
            }
            
            ldr.page_email = this.page_email;
            ldr.page_first_name = this.page_first_name;
            ldr.page_last_name = this.page_last_name;
            ldr.page_identification = this.page_identification;
            ldr.page_phone = this.page_phone;
            ldr.page_home = this.page_home;
            ldr.page_legal_guardian = this.page_legal_guardian;
            ldr.page_type_well = this.page_type_well;
            ldr.page_description_well = this.page_description_well;
            ldr.page_type_claim = this.page_type_claim;
            ldr.page_description_claim = this.page_description_claim;
            ldr.page_date_finalized = this.page_date_finalized;
            ldr.page_state = _page_states.LDR_STATE_PAGE_PENDING.ToString();
            ldr.page_register_user_pidm = this.page_register_user_pidm;

            ldr.page_origin = this.page_origin;
            //ldr.page_note_dissmised = this.page_note_dissmised;
            //ldr.page_final_response = this.page_final_response;

            _db.ldr_TblPageClaimBook.Add(ldr);
            
            int i = _db.SaveChanges();

            this.page_id = ldr.page_id;


            /*============================================Generar PDF==================================================*/
            LDRPageClaimDigitalPDF pdf = new LDRPageClaimDigitalPDF();
            LDRPageClaimDigitalPDF.Data data = new LDRPageClaimDigitalPDF.Data();

            data.name_file = $"Hoja[{ldr.page_numeration}]";
            data.type = ldr.page_type_claim;
            data.year = ldr.page_year.ToString();
            data.numeration = ldr.page_numeration.ToString();
            data.date_creation = ldr.page_date_create;
            data.names = $"{ldr.page_first_name} {ldr.page_last_name}";
            data.identification = ldr.page_identification;
            data.email = ldr.page_email;
            data.phone = ldr.page_phone;
            data.home = ldr.page_home;
            data.legal_guardian = ldr.page_legal_guardian;
            data.origin = _page_states.LDR_PAGE_TYPE_ORIGIN[ldr.page_origin];
            data.campus = Campus.get(ldr.page_campus).name;
            data.type_well = ldr.page_type_well;
            data.type_claim = ldr.page_type_claim;
            data.description_well = ldr.page_description_well;
            data.description_claim = ldr.page_description_claim;

            FilePage file_claims = new FilePage();
            string name_state_file = _page_states.LDR_FILE_STATUS[_page_states.LDR_STATE_FILE_CREATE];
            string source = string.Format("{0}/{1}/{2}", AppSettings.reports["output-ldr"], ldr.page_id, name_state_file);
            string output_path = LDRPageClaimDigitalPDF.generate(data, source);
            if (output_path != null)
            {
                file_claims.saveFileInfo(ldr.page_id, output_path, _page_states.LDR_STATE_FILE_CREATE);

                //guardar en tblfile
            }
            /*=========================================================================================================*/

            

            if (i > 0)
            {
                return true;
            }
            else
                return false;

        }

        public async Task<bool> sendEmailCreate(int page_id) {

            MailSender mail = new MailSender();
            FilePage file_list_page = new FilePage();
            List<FilePage> list_file;
            
            PageClaimBook ldr = await this.getPageClaimsBook(page_id);

            /*========================Obtener usuarios del modulo===============================*/
            AppUser app_user = new AppUser();
            List<AppUser> list_user = app_user.listUserByModule("bo_claims-book-adm");
            List<string> emails_adm = new List<string>();
            for (int co = 0; co < list_user.Count; co++)
            {
                emails_adm.Add(list_user[co].id + "@continental.edu.pe");
            }
            /*==================================================================================*/

            /*========================Obtener archivos del reclamo===============================*/
            list_file = await file_list_page.ListFilePage(page_id);
            if (list_file != null && list_file.Count > 0)
            {
                foreach (FilePage item in list_file)
                {
                    mail.addFile(item.file_route);
                }
            }
            /*==================================================================================*/

            if (ldr.page_email != null && ldr.page_email.Trim().Length > 0)
            {
                emails_adm.AddRange(ldr.page_email.Split(',').ToList());
            }

            


            mail.compose("ldr_create_claim",
                new
                {
                    type = ldr.page_type_claim,
                    year = ldr.page_year,
                    numeration = ldr.page_numeration,
                    date_creation = ldr.page_date_create,
                    names = $"{ldr.page_first_name} {ldr.page_last_name}",
                    identification = ldr.page_identification,
                    email = ldr.page_email,
                    phone = ldr.page_phone,
                    home = ldr.page_home,
                    legal_guardian = ldr.page_legal_guardian,
                    origin = _page_states.LDR_PAGE_TYPE_ORIGIN[ldr.page_origin],
                    campus = Campus.get(ldr.page_campus).name,
                    type_well = ldr.page_type_well,
                    type_claim = ldr.page_type_claim,
                    description_well = ldr.page_description_well,
                    description_claim = ldr.page_description_claim
                })
                .destination(emails_adm, $"[Libro de Reclamaciones] {ldr.page_type_well} { ldr.page_numeration}")
                .send();
            this.page_id = ldr.page_id;

            return true;

        }

        /// <summary>
        /// Actualiza reclamo
        /// </summary>
        /// <returns></returns>
        public bool updatePage() {
            LDRtblPageClaimBook page = _db.ldr_TblPageClaimBook.Find(this.page_id);
            /*if (this.page_numeration != null){
                page.page_numeration = this.page_numeration;
            }*/
            if (this.page_email != null){
                page.page_email = this.page_email;
            }
            if (this.page_first_name != null){
                page.page_first_name = this.page_first_name;
            }
            if (this.page_last_name != null){
                page.page_last_name = this.page_last_name;
            }
            if (this.page_identification != null){
                page.page_identification = this.page_identification;
            }
            if (this.page_phone != null){
                page.page_phone = this.page_phone;
            }
            if (this.page_home != null){
                page.page_home = this.page_home;
            }
            if (this.page_legal_guardian != null){
                page.page_legal_guardian = this.page_legal_guardian;
            }
            if (this.page_type_well != null){
                page.page_type_well = this.page_type_well;
            }
            if (this.page_description_well != null){
                page.page_description_well = this.page_description_well;
            }
            if (this.page_type_claim != null){
                page.page_type_claim = this.page_type_claim;
            }
            if (this.page_description_claim != null){
                page.page_description_claim = this.page_description_claim;
            }
            if (this.page_state != null){
                page.page_state = this.page_state;
            }
            if (page.page_state == _page_states.LDR_STATE_PAGE_DISMISSED.ToString() || page.page_state == _page_states.LDR_STATE_PAGE_FINALIZED)
            {
                page.page_date_finalized = DateTime.Now;
            }
            if (this.page_note_dissmised != null){
                page.page_note_dissmised = this.page_note_dissmised;
            }
            /*if (this.page_final_response != null)
            {
                page.page_final_response = this.page_final_response;
            }*/
            int save = _db.SaveChanges();
            if (save > 0)
            {
                return true;
            }
            else {
                return false;
            }


        }


        public async Task<bool> dismissedPage() {
            LDRtblPageClaimBook page_dismissed = _db.ldr_TblPageClaimBook.Find(this.page_id);

            /*==================VALIDACIÓNES================*/
            if (page_dismissed.page_state != _page_states.LDR_STATE_PAGE_PENDING) {
                throw new Exception("Solo se puede desestimar reclamos con estado pendiente.");
            }
            /*==============================================*/
            page_dismissed.page_note_dissmised = this.page_note_dissmised;
            page_dismissed.page_state = _page_states.LDR_STATE_PAGE_DISMISSED;
            page_dismissed.page_date_finalized = DateTime.Now;

            int save_page = _db.SaveChanges();
            if (save_page > 0){
                SatisfactionSurvey satisfaction_survey = new SatisfactionSurvey();
                satisfaction_survey.page_id = this.page_id;
                bool save_survey = await satisfaction_survey.generateSurvey();
                return true;
            }
            else{
                return false;
            }

        }


        public async Task<bool> updatePersonClaim() {
            LDRtblPageClaimBook page = _db.ldr_TblPageClaimBook.Find(this.page_id);
            page.page_person_claim = this.page_person_claim;
            page.page_avoid = this.page_avoid;
            page.page_avoid_description = this.page_avoid_description;
            int change_page = _db.SaveChanges();
            if (change_page <= 0)
            {
                throw new Exception("No se pudo guardar.");
            }
            return true;
        }
    }
}
