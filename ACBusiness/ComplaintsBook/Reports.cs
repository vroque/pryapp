﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.ClaimsBook;
using ACPermanence.Contexts.NEXO;
using _ldr_constants = ACTools.Constants.ClaimsBookConstants;

namespace ACBusiness.ComplaintsBook
{
    public class Reports
    {
        #region Propiedades

        #region Datos para Obtener Reporte

        public string div { get; set; }
        public int? year { get; set; }
        public string campus { get; set; }
        public string origin { get; set; }
        public int number_days { get; set; }
        public DateTime? date_start { get; set; }
        public DateTime? date_end { get; set; }
        public int number_report { get; set; }

        #endregion

        #region Reporte 1

        public int r1_page_id { get; set; }
        public string area_id { get; set; }
        public string area_name { get; set; }
        public int page_of_area { get; set; }
        public List<PageClaimBook> claims_area { get; set; }

        #endregion

        #region Reporte 2

        public string campus_id { get; set; }
        public string campus_name { get; set; }
        public int page_of_campus { get; set; }
        public int page_of_campus_digital { get; set; }
        public int page_of_campus_physical { get; set; }
        public string page_origin { get; set; }

        #endregion

        #region Reporte 3

        public int page_id { get; set; }
        public string page_campus { get; set; }
        public int page_year { get; set; }
        public int page_numeration { get; set; }
        public Dictionary<string, int> derivate_involved_area { get; set; }
        public Dictionary<string, int> answer_involved_area { get; set; }
        public int? days_legal_area { get; set; }
        public int? days_finalized { get; set; }
        public int? days_end { get; set; }

        #endregion

        #region Reporte 4

        public DateTime date { get; set; }
        public int pages_create_date { get; set; }
        public int pages_end_date { get; set; }
        public List<PageClaimBook> admitted_claims { get; set; }
        public List<PageClaimBook> claims_completed { get; set; }

        #endregion

        #region Reporte 5

        public int number_day { get; set; }
        public int pages_number_day { get; set; }

        #endregion

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion

        #region Funciones

        /// <summary>
        /// Reporte 1 reclamos por area.
        /// </summary>
        /// <param name="dataReport"></param>
        /// <returns>Retorna numero de reclamos por area derivada</returns>
        public async Task<List<Reports>> getReport1(Reports dataReport)
        {
            List<Reports> list = _nexoContext.ldr_TblPageClaimBook
                .Join(_nexoContext.ldr_tblDerivationArea,
                    tpc => new {page_id = tpc.page_id},
                    tda => new {page_id = tda.page_id},
                    (tpc, tda) => new {tpc, tda})
                .Join(_nexoContext.adryan_tbl_unidad_funcional,
                    tda => new {funtional_unity = tda.tda.derivate_funtional_unity},
                    vuf => new {funtional_unity = vuf.unidad_funcional},
                    (tda, vuf) => new {tda.tpc, tda.tda, vuf})
                .Where(result => result.vuf.compania == dataReport.div
                                 && result.tda.derivate_type == _ldr_constants.LDR_TYPE_DERIVATE_INVOLVED_AREA
                                 && (dataReport.year == null || result.tpc.page_year == dataReport.year)
                                 && (dataReport.campus == null || result.tpc.page_campus == dataReport.campus)
                                 && (dataReport.origin == null || result.tpc.page_origin == dataReport.origin)
                                 && (dataReport.date_start == null ||
                                     result.tpc.page_date_create >= dataReport.date_start)
                                 && (dataReport.date_end == null || result.tpc.page_date_create <= dataReport.date_end))
                .ToList()
                .OrderBy(result => result.vuf.nombre_unidad_funcional)
                .ToList()
                .Select(result => new Reports
                {
                    r1_page_id = result.tda.page_id,
                    area_id = result.tda.derivate_funtional_unity,
                    area_name = result.vuf.nombre_unidad_funcional,
                })
                .ToList();

            List<int> pages_id = list.Select(f => f.r1_page_id).Distinct().ToList();

            List<PageClaimBook> list_pages = _nexoContext.ldr_TblPageClaimBook
                .Join(_nexoContext.dim_campus,
                    tpc => new {id_campus = tpc.page_campus},
                    dc => new {id_campus = dc.campus},
                    (tpc, dc) => new {tpc, dc})
                .Where(result => pages_id.Contains(result.tpc.page_id))
                .ToList()
                .Select(result => new PageClaimBook
                {
                    page_id = result.tpc.page_id,
                    page_div = result.tpc.page_div,
                    page_year = result.tpc.page_year,
                    page_campus = result.tpc.page_campus,
                    page_bussines_unity = result.tpc.page_bussines_unity,
                    page_numeration = result.tpc.page_numeration,
                    page_date_create = result.tpc.page_date_create,
                    page_email = result.tpc.page_email,
                    page_first_name = result.tpc.page_first_name,
                    page_last_name = result.tpc.page_last_name,
                    page_identification = result.tpc.page_identification,
                    page_phone = result.tpc.page_phone,
                    page_home = result.tpc.page_home,
                    page_legal_guardian = result.tpc.page_legal_guardian,
                    page_type_well = result.tpc.page_type_well,
                    page_description_well = result.tpc.page_description_well,
                    page_type_claim = result.tpc.page_type_claim,
                    page_description_claim = result.tpc.page_description_claim,
                    page_date_finalized = result.tpc.page_date_finalized,
                    page_state = result.tpc.page_state,
                    page_register_user_pidm = result.tpc.page_register_user_pidm,
                    page_origin = result.tpc.page_origin,
                    page_note_dissmised = result.tpc.page_note_dissmised,
                    page_person_claim = result.tpc.page_person_claim,
                    page_avoid = result.tpc.page_avoid,
                    page_avoid_description = result.tpc.page_avoid_description,
                    campus_name = result.dc.campusDescription,
                })
                .ToList();

            list.ForEach(f => f.page_of_area = list.Count(g => g.area_id == f.area_id));

            var reports_whith_pages = new List<Reports>();
            reports_whith_pages = list;

            list = list.Select(f => new {a = f.area_id, b = f.area_name, c = f.page_of_area}).Distinct()
                .Select(f => new Reports {area_id = f.a, area_name = f.b, page_of_area = f.c}).ToList();

            if (reports_whith_pages != null && reports_whith_pages.Count > 0 && list_pages != null &&
                list_pages.Count > 0)
            {
                foreach (var report in reports_whith_pages)
                {
                    report.claims_area = new List<PageClaimBook>();
                    foreach (var page in list_pages)
                    {
                        if (report.r1_page_id == page.page_id)
                        {
                            report.claims_area.Add(page);
                        }
                    }
                }
            }

            if (reports_whith_pages != null && reports_whith_pages.Count > 0 && list != null && list.Count > 0)
            {
                foreach (var report in list)
                {
                    report.claims_area = new List<PageClaimBook>();
                    foreach (var report2 in reports_whith_pages)
                    {
                        if (report.area_id == report2.area_id)
                        {
                            report.claims_area.AddRange(report2.claims_area);
                        }
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Reporte 2 reclamos por Sede.
        /// </summary>
        /// <param name="dataReport"></param>
        /// <returns>Retorna reporte de reclamo por sede </returns>
        public async Task<List<Reports>> getReport2(Reports dataReport)
        {
            List<Reports> list = _nexoContext.ldr_TblPageClaimBook
                .Join(
                    _nexoContext.dim_campus,
                    tpc => new {campus_id = tpc.page_campus},
                    tc => new {campus_id = tc.campus},
                    (tpc, tc) => new {tpc, tc})
                .Where(result => (dataReport.year == null || result.tpc.page_year == dataReport.year)
                                 && (dataReport.origin == null || result.tpc.page_origin == dataReport.origin)
                                 && (dataReport.date_start == null ||
                                     result.tpc.page_date_create >= dataReport.date_start)
                                 && (dataReport.date_end == null || result.tpc.page_date_create <= dataReport.date_end))
                .ToList()
                .OrderBy(result => result.tc.campusDescription)
                .ToList()
                .Select(result => new Reports
                {
                    campus_id = result.tc.campus,
                    campus_name = result.tc.campusDescription,
                    page_origin = result.tpc.page_origin
                })
                .ToList();

            list.ForEach(f => f.page_of_campus = list.Count(g => g.campus_id == f.campus_id));
            list.ForEach(f => f.page_of_campus_digital = list
                .Count(g => g.campus_id == f.campus_id &&
                            g.page_origin == _ldr_constants.LDR_TYPE_ORIGIN_PAGE_DIGITAL));

            list.ForEach(f => f.page_of_campus_physical = list.Count(g =>
                g.campus_id == f.campus_id && g.page_origin == _ldr_constants.LDR_TYPE_ORIGIN_PAGE_PHYSICAL));


            list = list.Select(f => new
                {
                    a = f.campus_id, b = f.campus_name, c = f.page_of_campus, d = f.page_of_campus_digital,
                    e = f.page_of_campus_physical
                })
                .Distinct()
                .Select(f => new Reports
                {
                    campus_id = f.a, campus_name = f.b, page_of_campus = f.c, page_of_campus_digital = f.d,
                    page_of_campus_physical = f.e
                })
                .ToList();

            return list;
        }

        public async Task<List<Reports>> getReport3(Reports dataReport)
        {
            var report = new List<Reports>();
            List<PageClaimBook> list_pages = _nexoContext.ldr_TblPageClaimBook
                .Where(result =>
                    (dataReport.year == null || result.page_year == dataReport.year)
                    && (dataReport.campus == null || result.page_campus == dataReport.campus)
                    && (dataReport.origin == null || result.page_origin == dataReport.origin)
                    && result.page_state == _ldr_constants.LDR_STATE_PAGE_FINALIZED
                    && result.page_date_finalized != null
                    && (dataReport.date_start == null || result.page_date_create >= dataReport.date_start)
                    && (dataReport.date_end == null || result.page_date_create <= dataReport.date_end))
                .ToList()
                .Select(result => new PageClaimBook
                {
                    page_id = result.page_id,
                    page_div = result.page_div,
                    page_year = result.page_year,
                    page_campus = result.page_campus,
                    page_bussines_unity = result.page_bussines_unity,
                    page_numeration = result.page_numeration,
                    page_date_create = result.page_date_create,
                    page_email = result.page_email,
                    page_first_name = result.page_first_name,
                    page_last_name = result.page_last_name,
                    page_identification = result.page_identification,
                    page_phone = result.page_phone,
                    page_home = result.page_home,
                    page_legal_guardian = result.page_legal_guardian,
                    page_type_well = result.page_type_well,
                    page_description_well = result.page_description_well,
                    page_type_claim = result.page_type_claim,
                    page_description_claim = result.page_description_claim,
                    page_date_finalized = result.page_date_finalized,
                    page_state = result.page_state,
                    page_register_user_pidm = result.page_register_user_pidm,
                    page_origin = result.page_origin,
                    page_note_dissmised = result.page_note_dissmised,
                    page_person_claim = result.page_person_claim,
                    page_avoid = result.page_avoid,
                    page_avoid_description = result.page_avoid_description,
                })
                .ToList();

            List<int> list_page_id = list_pages.Select(result => result.page_id).ToList();

            List<DerivationArea> list_derivate = _nexoContext.ldr_tblDerivationArea
                .Join(_nexoContext.adryan_tbl_unidad_funcional,
                    ltd => new {derivate_funtional_unity = ltd.derivate_funtional_unity},
                    tuf => new {derivate_funtional_unity = tuf.unidad_funcional},
                    (ltd, tuf) => new {ltd, tuf})
                .Where(result => result.tuf.compania == dataReport.div &&
                                 list_page_id.Contains(result.ltd.page_id) &&
                                 result.ltd.derivate_state == _ldr_constants.LDR_STATE_DERIVATE_FINALIZED)
                .Select(result => new DerivationArea
                {
                    derivate_id = result.ltd.derivate_id,
                    page_id = result.ltd.page_id,
                    derivate_person_creator_pidm = result.ltd.derivate_person_creator_pidm,
                    derivate_funtional_unity = result.ltd.derivate_funtional_unity,
                    derivate_mail_recipient = result.ltd.derivate_mail_recipient,
                    derivate_type = result.ltd.derivate_type,
                    derivate_state = result.ltd.derivate_state,
                    derivate_date_created = result.ltd.derivate_date_created,
                    derivate_date_modify = result.ltd.derivate_date_modify,
                    derivate_funtional_unity_name = result.tuf.nombre_unidad_funcional,
                })
                .ToList();

            List<AnswerPage> list_answer = _nexoContext.ldr_tblAnswerPage
                .Where(result => list_page_id.Contains(result.page_id) &&
                                 result.derivate_id != null &&
                                 (result.answer_state == _ldr_constants.LDR_STATE_ANSWER_ACCEPTED_FINISH ||
                                  result.answer_state == _ldr_constants.LDR_STATE_ANSWER_ACCEPTED))
                .ToList()
                .Select(result => new AnswerPage
                {
                    answer_id = result.answer_id,
                    page_id = result.page_id,
                    answer_person_creator_pidm = result.answer_person_creator_pidm,
                    answer_person_validate_pidm = result.answer_person_validate_pidm,
                    answer_note = result.answer_note,
                    derivate_id = result.derivate_id,
                    answer_state = result.answer_state,
                    answer_date_created = result.answer_date_created,
                    answer_date_validate = result.answer_date_validate
                })
                .ToList();

            foreach (var page in list_pages)
            {
                var data = new Reports
                {
                    page_id = page.page_id,
                    page_numeration = page.page_numeration,
                    page_campus = page.page_campus,
                    page_year = page.page_year,
                    derivate_involved_area = new Dictionary<string, int>(),
                    answer_involved_area = new Dictionary<string, int>()
                };

                foreach (var derivate in list_derivate)
                {
                    if (page.page_id == derivate.page_id)
                    {
                        if (derivate.derivate_type == _ldr_constants.LDR_TYPE_DERIVATE_INVOLVED_AREA)
                        {
                            data.derivate_involved_area.Add(derivate.derivate_funtional_unity_name.ToString(),
                                this.getDate(derivate.derivate_date_created)
                                    .Subtract(this.getDate(page.page_date_create)).Days);
                        }

                        foreach (var answer in list_answer)
                        {
                            if (derivate.page_id == answer.page_id && derivate.derivate_id == answer.derivate_id)
                            {
                                if (derivate.derivate_type == _ldr_constants.LDR_TYPE_DERIVATE_INVOLVED_AREA)
                                {
                                    data.answer_involved_area.Add(derivate.derivate_funtional_unity_name.ToString(),
                                        (this.getDate(answer.answer_date_created)
                                            .Subtract(this.getDate(derivate.derivate_date_created)).Days));
                                }

                                if (derivate.derivate_type == _ldr_constants.LDR_TYPE_DERIVATE_LEGAL_AREA)
                                {
                                    data.days_legal_area = this.getDate(answer.answer_date_created)
                                        .Subtract(this.getDate(derivate.derivate_date_created)).Days;
                                    data.days_finalized = this.getDate(page.page_date_finalized)
                                        .Subtract(this.getDate(answer.answer_date_created)).Days;
                                }
                            }
                        }
                    }
                }

                data.days_end = this.getDate(page.page_date_finalized).Subtract(this.getDate(page.page_date_create))
                    .Days;
                if (data.derivate_involved_area != null && data.derivate_involved_area.Count > 0)
                {
                    report.Add(data);
                }
            }

            return report;
        }

        /// <summary>
        /// Reporte 4 reclamos ingresados y finalizados por fecha.
        /// </summary>
        /// <param name="d_start"></param>
        /// <param name="d_end"></param>
        /// <returns>Retorna lista de fechas con cuantos reclamos se registraron y cuantos reclamos se finalizaron en dicha fecha</returns>
        public async Task<List<Reports>> getReport4(DateTime? d_start, DateTime? d_end)
        {
            var d_start2 = getDate(d_start);
            var d_end2 = getDate(d_end);
            var report = new List<Reports>();

            List<PageClaimBook> list_pages = _nexoContext.ldr_TblPageClaimBook
                .Join(_nexoContext.dim_campus,
                    tpc => new {id_campus = tpc.page_campus},
                    dc => new {id_campus = dc.campus},
                    (tpc, dc) => new {tpc, dc})
                .Where(result => (result.tpc.page_date_create >= d_start2 && result.tpc.page_date_create <= d_end2) ||
                                 (result.tpc.page_date_finalized >= d_start2 &&
                                  result.tpc.page_date_finalized <= d_end2))
                .ToList()
                .Select(result => new PageClaimBook
                {
                    page_id = result.tpc.page_id,
                    page_div = result.tpc.page_div,
                    page_year = result.tpc.page_year,
                    page_campus = result.tpc.page_campus,
                    page_bussines_unity = result.tpc.page_bussines_unity,
                    page_numeration = result.tpc.page_numeration,
                    page_date_create = result.tpc.page_date_create,
                    page_email = result.tpc.page_email,
                    page_first_name = result.tpc.page_first_name,
                    page_last_name = result.tpc.page_last_name,
                    page_identification = result.tpc.page_identification,
                    page_phone = result.tpc.page_phone,
                    page_home = result.tpc.page_home,
                    page_legal_guardian = result.tpc.page_legal_guardian,
                    page_type_well = result.tpc.page_type_well,
                    page_description_well = result.tpc.page_description_well,
                    page_type_claim = result.tpc.page_type_claim,
                    page_description_claim = result.tpc.page_description_claim,
                    page_date_finalized = result.tpc.page_date_finalized,
                    page_state = result.tpc.page_state,
                    page_register_user_pidm = result.tpc.page_register_user_pidm,
                    page_origin = result.tpc.page_origin,
                    page_note_dissmised = result.tpc.page_note_dissmised,
                    page_person_claim = result.tpc.page_person_claim,
                    page_avoid = result.tpc.page_avoid,
                    page_avoid_description = result.tpc.page_avoid_description,
                    campus_name = result.dc.campusDescription,
                })
                .ToList();

            if (list_pages != null && list_pages.Count > 0)
            {
                for (var i = d_start2; i <= d_end; i = i.AddDays(1))
                {
                    var data = new Reports
                    {
                        admitted_claims = new List<PageClaimBook>(),
                        claims_completed = new List<PageClaimBook>(),
                        date = i,
                        pages_create_date = list_pages.Count(f => this.getDate(f.page_date_create) == this.getDate(i)),
                        pages_end_date = list_pages.Count(f =>
                            f.page_date_finalized != null && this.getDate(f.page_date_finalized) == this.getDate(i))
                    };

                    foreach (var page in list_pages)
                    {
                        if (this.getDate(page.page_date_create) == this.getDate(i))
                        {
                            data.admitted_claims.Add(page);
                        }
                        else if (page.page_date_finalized != null &&
                                 this.getDate(page.page_date_finalized) == this.getDate(i))
                        {
                            data.claims_completed.Add(page);
                        }
                    }

                    if (data.pages_create_date > 0 || data.pages_end_date > 0)
                    {
                        report.Add(data);
                    }
                }
            }


            return report;
        }

        /// <summary>
        /// Reporte 5 de numero de reclamos atendidos en X días.
        /// </summary>
        /// <param name="number_day">Numero de días</param>
        /// <returns>Retorna cuantos reclamos se atendió en "number_day" dias</returns>
        public async Task<List<Reports>> getReport5(int number_day)
        {
            var report = new List<Reports>();

            List<PageClaimBook> list_pages = _nexoContext.ldr_TblPageClaimBook
                .Where(result =>
                    result.page_state == _ldr_constants.LDR_STATE_PAGE_FINALIZED &&
                    result.page_date_finalized != null &&
                    EntityFunctions.DiffDays(result.page_date_create, result.page_date_finalized) <= number_day)
                .ToList()
                .Select(result => new PageClaimBook
                {
                    page_id = result.page_id,
                    page_div = result.page_div,
                    page_year = result.page_year,
                    page_campus = result.page_campus,
                    page_bussines_unity = result.page_bussines_unity,
                    page_numeration = result.page_numeration,
                    page_date_create = result.page_date_create,
                    page_email = result.page_email,
                    page_first_name = result.page_first_name,
                    page_last_name = result.page_last_name,
                    page_identification = result.page_identification,
                    page_phone = result.page_phone,
                    page_home = result.page_home,
                    page_legal_guardian = result.page_legal_guardian,
                    page_type_well = result.page_type_well,
                    page_description_well = result.page_description_well,
                    page_type_claim = result.page_type_claim,
                    page_description_claim = result.page_description_claim,
                    page_date_finalized = result.page_date_finalized,
                    page_state = result.page_state,
                    page_register_user_pidm = result.page_register_user_pidm,
                    page_origin = result.page_origin,
                    page_note_dissmised = result.page_note_dissmised,
                    page_person_claim = result.page_person_claim,
                    page_avoid = result.page_avoid,
                    page_avoid_description = result.page_avoid_description,
                })
                .ToList();

            for (var nmb = 0; nmb <= number_day; nmb++)
            {
                var data = new Reports
                {
                    number_day = nmb,
                    pages_number_day = list_pages.Count(f =>
                        this.getDate(f.page_date_finalized).Subtract(this.getDate(f.page_date_create)).Days == nmb)
                };
                if (data.pages_number_day > 0)
                {
                    report.Add(data);
                }
            }

            return report;
        }

        public DateTime getDate(DateTime? fecha)
        {
            var date_1 = DateTime.Parse(fecha.ToString());
            var date = new DateTime(date_1.Year, date_1.Month, date_1.Day);
            return date;
        }

        #endregion
    }
}
