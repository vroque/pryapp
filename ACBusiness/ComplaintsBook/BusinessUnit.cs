﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.ADRYAN.Synonyms;

namespace ACBusiness.ComplaintsBook
{
    public class BusinessUnit
    {
        public int idUnidadNegocio { get; set; }
        public string nombre { get; set; }
        public string nombreCorto { get; set; }
        private readonly NEXOContext _nexoContext = new NEXOContext();

        /// <summary>
        /// Unidad de Negocio
        /// </summary>
        /// <returns></returns>
        public async Task<List<BusinessUnit>> GetAllBusinessUnit()
        {
            List<ADRYANtbl_Unidad_Negocio>
                businessUnitRawList = await _nexoContext.adryan_tbl_unidad_negocio.ToListAsync();
            List<BusinessUnit> businessUnitList = businessUnitRawList
                .Select(s => new BusinessUnit
                {
                    idUnidadNegocio = s.idUnidadNegocio,
                    nombre = s.nombre,
                    nombreCorto = s.nombreCorto
                })
                .ToList();
            return businessUnitList;
        }
    }
}
