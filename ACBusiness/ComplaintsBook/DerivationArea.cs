﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.ComplaintsBook;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.LDR;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;
using _ldr_states = ACTools.Constants.ClaimsBookConstants;
using ACTools.Mail;
using ACBusiness.Institutional;

namespace ACBusiness.ClaimsBook
{
    public class DerivationArea
    {
        #region propiedades

        public int derivate_id { get; set; }
        public int page_id { get; set; }
        public string derivate_person_creator_pidm { get; set; }
        public string derivate_funtional_unity { get; set; }
        public string derivate_mail_recipient { get; set; }
        public string derivate_type { get; set; }
        public string derivate_state { get; set; }
        public string derivate_note_disabled { get; set; }
        public DateTime derivate_date_created { get; set; }
        public DateTime? derivate_date_modify { get; set; }
        public string derivate_person_answer_names { get; set; }
        public string derivate_funtional_unity_name { get; set; }
        public string staff_involved_pidm { get; set; }
        public string staff_position_company { get; set; }
        public string staff_involved_note { get; set; }
        public string staff_position_description { get; set; }
        public string staff_involved_type { get; set; }
        public string staff_Involved_name { get; set; }
        public string action_derivate { get; set; }

        #endregion propiedades

        #region propiedades AnswerPage

        public List<AnswerPage> answers { get; set; }

        #endregion

        private readonly NEXOContext _nexoContext = new NEXOContext();

        /// <summary>
        /// Lista las derivaciones acivas de un reclamo
        /// </summary>
        /// <param name="compania"> código de compañia (ucci -> 01) (iesc -> 02)</param>
        /// <param name="page_id"> código de reclamo</param>
        /// <returns></returns>
        public async Task<List<DerivationArea>> listDerivationInvolvedArea(string compania, int page_id)
        {
            List<DerivationArea> list_derivation_page_book = _nexoContext.ldr_tblDerivationArea
                .Join(_nexoContext.adryan_tbl_unidad_funcional,
                    ltd => new {derivate_funtional_unity = ltd.derivate_funtional_unity},
                    tuf => new {derivate_funtional_unity = tuf.unidad_funcional},
                    (ltd, tuf) => new {ltd, tuf})
                .Join(_nexoContext.ldr_tblStaffInvolved,
                    ltd2 => new {page_id = ltd2.ltd.page_id, derivation_id = ltd2.ltd.derivate_id},
                    sta => new {page_id = sta.page_id, derivation_id = sta.derivate_id},
                    (ltd2, sta) => new {ltd2.ltd, ltd2.tuf, sta})
                .Join(_nexoContext.adryan_tbl_puesto_compania,
                    sta2 => new
                    {
                        Staff_position_company = sta2.sta.staff_position_company, compania = sta2.tuf.compania
                    },
                    tpc => new {Staff_position_company = tpc.puesto, compania = tpc.compania},
                    (sta2, tpc) => new {sta2.ltd, sta2.sta, sta2.tuf, tpc})
                .Where(result => result.ltd.page_id == page_id &&
                                 result.tuf.compania == compania &&
                                 result.sta.staff_involved_type == _ldr_states.LDR_TYPE_INVOLVED_BOSS &&
                                 result.ltd.derivate_state != _ldr_states.LDR_STATE_DERIVATE_DISMISSED &&
                                 result.ltd.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_INVOLVED_AREA
                ).ToList()
                .OrderBy(result => result.ltd.derivate_date_created)
                .Select(ldpb => new DerivationArea
                    {
                        derivate_id = ldpb.ltd.derivate_id,
                        page_id = ldpb.ltd.page_id,
                        derivate_person_creator_pidm = ldpb.ltd.derivate_person_creator_pidm,
                        derivate_funtional_unity = ldpb.ltd.derivate_funtional_unity,
                        derivate_mail_recipient = ldpb.ltd.derivate_mail_recipient,
                        derivate_type = ldpb.ltd.derivate_type,
                        derivate_state = ldpb.ltd.derivate_state,
                        derivate_date_created = ldpb.ltd.derivate_date_created,
                        derivate_date_modify = ldpb.ltd.derivate_date_modify,
                        derivate_funtional_unity_name = ldpb.tuf.nombre_unidad_funcional,
                        staff_involved_pidm = ldpb.sta.staff_involved_pidm,
                        staff_position_company = ldpb.sta.staff_position_company,
                        staff_involved_note = ldpb.sta.staff_involved_note,
                        staff_position_description = ldpb.tpc.descripcion_puesto,
                        staff_involved_type = ldpb.sta.staff_involved_type,
                    }
                )
                .ToList();

            if (list_derivation_page_book != null && list_derivation_page_book.Count > 0)
            {
                List<string> persons_id = list_derivation_page_book
                    .Select(f => f.staff_involved_pidm).Distinct().ToList();
                List<int?> ids_derivates = list_derivation_page_book
                    .Select(f => (int?) f.derivate_id).Distinct().ToList();

                var answer_option = new AnswerPage();
                List<AnswerPage> lst_aswers = await answer_option.getAnswerDerivationOfPage(page_id);

                if (lst_aswers != null && lst_aswers.Count > 0)
                {
                    list_derivation_page_book.ForEach(item_list_derivation => item_list_derivation.answers = lst_aswers
                        .Where(item_lst_aswer => item_lst_aswer.derivate_id == item_list_derivation.derivate_id)
                        .ToList()
                    );
                }

                persons_id = persons_id.Distinct().ToList();

                var bducci = new DBOContext();
                List<DBOTblPersona> persons = bducci.tbl_persona
                    .Where(result => persons_id.Contains(result.IDPersonaN.ToString()))
                    .ToList();

                foreach (var item in list_derivation_page_book)
                {
                    var personasiggment = persons
                        .FirstOrDefault(result => result.IDPersonaN.ToString() == item.staff_involved_pidm);
                    if (personasiggment != null)
                    {
                        item.staff_Involved_name =
                            $"{personasiggment.Nombres} {personasiggment.Appat} {personasiggment.ApMat}";
                    }
                }
            }

            return list_derivation_page_book;
        }


        public async Task<DerivationArea> listDerivationLegalArea(string compania, int page_id)
        {
            var list_derivation_page_book = _nexoContext.ldr_tblDerivationArea
                .Join(_nexoContext.adryan_tbl_unidad_funcional,
                    ltd => new {derivate_funtional_unity = ltd.derivate_funtional_unity},
                    tuf => new {derivate_funtional_unity = tuf.unidad_funcional},
                    (ltd, tuf) => new {ltd, tuf})
                .Join(_nexoContext.ldr_tblStaffInvolved,
                    ltd2 => new {page_id = ltd2.ltd.page_id, derivation_id = ltd2.ltd.derivate_id},
                    sta => new {page_id = sta.page_id, derivation_id = sta.derivate_id},
                    (ltd2, sta) => new {ltd2.ltd, ltd2.tuf, sta})
                .Join(_nexoContext.adryan_tbl_puesto_compania,
                    sta2 => new
                    {
                        Staff_position_company = sta2.sta.staff_position_company, compania = sta2.tuf.compania
                    },
                    tpc => new {Staff_position_company = tpc.puesto, compania = tpc.compania},
                    (sta2, tpc) => new {sta2.ltd, sta2.sta, sta2.tuf, tpc})
                .Where(result => result.ltd.page_id == page_id &&
                                 result.tuf.compania == compania &&
                                 result.sta.staff_involved_type == _ldr_states.LDR_TYPE_INVOLVED_BOSS &&
                                 result.ltd.derivate_state != _ldr_states.LDR_STATE_DERIVATE_DISMISSED &&
                                 result.ltd.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_LEGAL_AREA)
                .ToList()
                .OrderBy(result => result.ltd.derivate_date_created)
                .Select(ldpb => new DerivationArea
                {
                    derivate_id = ldpb.ltd.derivate_id,
                    page_id = ldpb.ltd.page_id,
                    derivate_person_creator_pidm = ldpb.ltd.derivate_person_creator_pidm,
                    derivate_funtional_unity = ldpb.ltd.derivate_funtional_unity,
                    derivate_mail_recipient = ldpb.ltd.derivate_mail_recipient,
                    derivate_type = ldpb.ltd.derivate_type,
                    derivate_state = ldpb.ltd.derivate_state,
                    derivate_date_created = ldpb.ltd.derivate_date_created,
                    derivate_date_modify = ldpb.ltd.derivate_date_modify,
                    derivate_funtional_unity_name = ldpb.tuf.nombre_unidad_funcional,
                    staff_involved_pidm = ldpb.sta.staff_involved_pidm,
                    staff_position_company = ldpb.sta.staff_position_company,
                    staff_involved_note = ldpb.sta.staff_involved_note,
                    staff_position_description = ldpb.tpc.descripcion_puesto,
                    staff_involved_type = ldpb.sta.staff_involved_type,
                })
                .FirstOrDefault();

            if (list_derivation_page_book != null)
            {
                var answer_option = new AnswerPage();
                var answer = await answer_option.getAnswerAreaLegalOfPage(list_derivation_page_book.page_id,
                    list_derivation_page_book.derivate_id);
                list_derivation_page_book.answers = new List<AnswerPage>();
                if (answer != null)
                {
                    list_derivation_page_book.answers.Add(answer);
                }


                var bducci = new DBOContext();
                var person = bducci.tbl_persona
                    .FirstOrDefault(result =>
                        result.IDPersonaN.ToString() == list_derivation_page_book.staff_involved_pidm);
                if (person != null)
                {
                    list_derivation_page_book.staff_Involved_name = $"{person.Nombres} {person.Appat} {person.ApMat}";
                }
            }

            if (list_derivation_page_book == null)
            {
                list_derivation_page_book = new DerivationArea();
                var answer_option = new AnswerPage();
                var answer = await answer_option.getAnswerAreaLegalOfPage(page_id);
                list_derivation_page_book.answers = new List<AnswerPage>();
                if (answer != null)
                {
                    list_derivation_page_book.answers.Add(answer);
                }

                var bducci = new DBOContext();
                var person = bducci.tbl_persona.FirstOrDefault(result =>
                    result.IDPersonaN.ToString() == list_derivation_page_book.staff_involved_pidm);
                if (person != null)
                {
                    list_derivation_page_book.staff_Involved_name = $"{person.Nombres} {person.Appat} {person.ApMat}";
                }
            }

            return list_derivation_page_book;
        }

        /// <summary>
        /// Obtiene información de una derivación
        /// </summary>
        /// <param name="derivate_id">Código de derivación</param>
        /// <returns></returns>
        public async Task<DerivationArea> getDerivationClaim(int derivate_id, string pidm_user)
        {
            var derivation = _nexoContext.ldr_tblDerivationArea
                .Join(_nexoContext.adryan_tbl_unidad_funcional,
                    ltd => new {derivate_funtional_unity = ltd.derivate_funtional_unity},
                    tuf => new {derivate_funtional_unity = tuf.unidad_funcional},
                    (ltd, tuf) => new {ltd, tuf})
                .Join(_nexoContext.ldr_tblStaffInvolved,
                    ltd2 => new {page_id = ltd2.ltd.page_id, derivation_id = ltd2.ltd.derivate_id},
                    sta => new {page_id = sta.page_id, derivation_id = sta.derivate_id},
                    (ltd2, sta) => new {ltd2.ltd, ltd2.tuf, sta})
                .Join(_nexoContext.adryan_tbl_puesto_compania,
                    sta2 => new
                    {
                        Staff_position_company = sta2.sta.staff_position_company, compania = sta2.tuf.compania
                    },
                    tpc => new {Staff_position_company = tpc.puesto, compania = tpc.compania},
                    (sta2, tpc) => new {sta2.ltd, sta2.sta, sta2.tuf, tpc})
                .Where(result => result.ltd.derivate_id == derivate_id &&
                                 result.sta.staff_involved_pidm == pidm_user)
                .ToList()
                .OrderByDescending(result => result.ltd.derivate_date_created)
                .Select(ldpb => new DerivationArea
                {
                    derivate_id = ldpb.ltd.derivate_id,
                    page_id = ldpb.ltd.page_id,
                    derivate_person_creator_pidm = ldpb.ltd.derivate_person_creator_pidm,
                    derivate_funtional_unity = ldpb.ltd.derivate_funtional_unity,
                    derivate_mail_recipient = ldpb.ltd.derivate_mail_recipient,
                    derivate_type = ldpb.ltd.derivate_type,
                    derivate_state = ldpb.ltd.derivate_state,
                    derivate_date_created = ldpb.ltd.derivate_date_created,
                    derivate_date_modify = ldpb.ltd.derivate_date_modify,
                    derivate_funtional_unity_name = ldpb.tuf.nombre_unidad_funcional,
                    staff_involved_pidm = ldpb.sta.staff_involved_pidm,
                    staff_position_company = ldpb.sta.staff_position_company,
                    staff_involved_note = ldpb.sta.staff_involved_note,
                    staff_involved_type = ldpb.sta.staff_involved_type,
                    staff_position_description = ldpb.tpc.descripcion_puesto
                })
                .FirstOrDefault();

            if (derivation != null)
            {
                var bducci = new DBOContext();
                var person = bducci.tbl_persona.FirstOrDefault(result =>
                    result.IDPersonaN.ToString() == derivation.staff_involved_pidm);

                if (person != null)
                {
                    derivation.staff_Involved_name = $"{person.Nombres} {person.Appat} {person.ApMat}";
                }

                var answer_option = new AnswerPage();
                List<AnswerPage> lst_aswers =
                    await answer_option.getAnswersDerivation(derivation.page_id, derivation.derivate_id);

                if (lst_aswers != null && lst_aswers.Count > 0)
                {
                    derivation.answers = lst_aswers;
                }
            }

            return derivation;
        }


        /// <summary>
        /// Lista las derivaciones de un reclamo x unidad funcional
        /// </summary>
        /// <param name="compania">código de compañia (ucci -> 01) (iesc -> 02)</param>
        /// <param name="page_id">código de reclamo</param>
        /// <param name="funtional_unity">códig de unidad funcional</param>
        /// <returns></returns>
        public async Task<List<DerivationArea>> listDerivationFuntionalUnity(string compania, int page_id,
            string funtional_unity)
        {
            List<DerivationArea> list_derivation_page_book = _nexoContext.ldr_tblDerivationArea
                .Join(_nexoContext.adryan_tbl_unidad_funcional,
                    ltd => new {derivate_funtional_unity = ltd.derivate_funtional_unity},
                    tuf => new {derivate_funtional_unity = tuf.unidad_funcional},
                    (ltd, tuf) => new {ltd, tuf})
                .Join(_nexoContext.ldr_tblStaffInvolved,
                    ltd2 => new {page_id = ltd2.ltd.page_id, derivation_id = ltd2.ltd.derivate_id},
                    sta => new {page_id = sta.page_id, derivation_id = sta.derivate_id},
                    (ltd2, sta) => new {ltd2.ltd, ltd2.tuf, sta})
                .Join(_nexoContext.adryan_tbl_puesto_compania,
                    sta2 => new
                    {
                        Staff_position_company = sta2.sta.staff_position_company, compania = sta2.tuf.compania
                    },
                    tpc => new {Staff_position_company = tpc.puesto, compania = tpc.compania},
                    (sta2, tpc) => new {sta2.ltd, sta2.sta, sta2.tuf, tpc})
                .Where(result => result.ltd.page_id == page_id &&
                                 result.tuf.compania == compania &&
                                 result.ltd.derivate_funtional_unity == funtional_unity &&
                                 result.sta.staff_derivate_state == _ldr_states.LDR_STATE_STAFF_NOT_DERIVATED &&
                                 result.ltd.derivate_state == _ldr_states.LDR_STATE_DERIVATE_PENDING)
                .ToList().OrderByDescending(result => result.ltd.derivate_date_created)
                .Select(ldpb => new DerivationArea
                {
                    derivate_id = ldpb.ltd.derivate_id,
                    page_id = ldpb.ltd.page_id,
                    derivate_person_creator_pidm = ldpb.ltd.derivate_person_creator_pidm,
                    derivate_funtional_unity = ldpb.ltd.derivate_funtional_unity,
                    derivate_mail_recipient = ldpb.ltd.derivate_mail_recipient,
                    derivate_type = ldpb.ltd.derivate_type,
                    derivate_state = ldpb.ltd.derivate_state,
                    derivate_date_created = ldpb.ltd.derivate_date_created,
                    derivate_date_modify = ldpb.ltd.derivate_date_modify,
                    derivate_funtional_unity_name = ldpb.tuf.nombre_unidad_funcional,
                    staff_involved_pidm = ldpb.sta.staff_involved_pidm,
                    staff_position_company = ldpb.sta.staff_position_company,
                    staff_involved_note = ldpb.sta.staff_involved_note,
                    staff_position_description = ldpb.tpc.descripcion_puesto
                })
                .ToList();

            List<string> persons_id = list_derivation_page_book.Select(f => f.staff_involved_pidm).Distinct().ToList();
            var bducci = new DBOContext();
            List<DBOTblPersona> persons = bducci.tbl_persona
                .Where(result => persons_id.Contains(result.IDPersonaN.ToString())).ToList();
            foreach (var item in list_derivation_page_book)
            {
                var person = persons.FirstOrDefault(result => result.IDPersonaN.ToString() == item.staff_involved_pidm);
                item.staff_Involved_name = $"{person.Nombres} {person.Appat} {person.ApMat}";
            }

            return list_derivation_page_book;
        }

        /// <summary>
        /// Lista derivaciones de un reclamo por estado de derivación
        /// </summary>
        /// <param name="compania">dependencia</param>
        /// <param name="page_id">código de reclamo</param>
        /// <param name="state">estado</param>
        /// <returns></returns>
        public async Task<List<DerivationArea>> listDerivationStates(string compania, int page_id, List<string> states)
        {
            List<DerivationArea> list_derivation_page_book = _nexoContext.ldr_tblDerivationArea
                .Join(_nexoContext.adryan_tbl_unidad_funcional,
                    ltd => new {derivate_funtional_unity = ltd.derivate_funtional_unity},
                    tuf => new {derivate_funtional_unity = tuf.unidad_funcional},
                    (ltd, tuf) => new {ltd, tuf})
                .Join(_nexoContext.ldr_tblStaffInvolved,
                    ltd2 => new {page_id = ltd2.ltd.page_id, derivation_id = ltd2.ltd.derivate_id},
                    sta => new {page_id = sta.page_id, derivation_id = sta.derivate_id},
                    (ltd2, sta) => new {ltd2.ltd, ltd2.tuf, sta})
                .Join(_nexoContext.adryan_tbl_puesto_compania,
                    sta2 => new
                    {
                        Staff_position_company = sta2.sta.staff_position_company, compania = sta2.tuf.compania
                    },
                    tpc => new {Staff_position_company = tpc.puesto, compania = tpc.compania},
                    (sta2, tpc) => new {sta2.ltd, sta2.sta, sta2.tuf, tpc})
                .Where(result => result.ltd.page_id == page_id &&
                                 result.tuf.compania == compania &&
                                 states.Contains(result.ltd.derivate_state))
                .ToList()
                .OrderByDescending(result => result.ltd.derivate_date_created)
                .Select(ldpb => new DerivationArea
                {
                    derivate_id = ldpb.ltd.derivate_id,
                    page_id = ldpb.ltd.page_id,
                    derivate_person_creator_pidm = ldpb.ltd.derivate_person_creator_pidm,
                    derivate_funtional_unity = ldpb.ltd.derivate_funtional_unity,
                    derivate_mail_recipient = ldpb.ltd.derivate_mail_recipient,
                    derivate_type = ldpb.ltd.derivate_type,
                    derivate_state = ldpb.ltd.derivate_state,
                    derivate_date_created = ldpb.ltd.derivate_date_created,
                    derivate_date_modify = ldpb.ltd.derivate_date_modify,
                    derivate_funtional_unity_name = ldpb.tuf.nombre_unidad_funcional,

                    staff_involved_pidm = ldpb.sta.staff_involved_pidm,
                    staff_position_company = ldpb.sta.staff_position_company,
                    staff_involved_note = ldpb.sta.staff_involved_note,
                    staff_position_description = ldpb.tpc.descripcion_puesto
                })
                .ToList();

            List<string> persons_id = list_derivation_page_book.Select(f => f.staff_involved_pidm).Distinct().ToList();
            var bducci = new DBOContext();
            List<DBOTblPersona> persons = bducci.tbl_persona
                .Where(result => persons_id.Contains(result.IDPersonaN.ToString())).ToList();
            foreach (var item in list_derivation_page_book)
            {
                var person = persons.FirstOrDefault(result => result.IDPersonaN.ToString() == item.staff_involved_pidm);
                item.staff_Involved_name = $"{person.Nombres} {person.Appat} {person.ApMat}";
            }

            return list_derivation_page_book;
        }

        /// <summary>
        /// Guarda una derivación
        /// </summary>
        /// <returns></returns>
        public async Task<bool> saveDerivation()
        {
            var derivation = new LDRtblDerivationArea();
            var page = new PageClaimBook();
            derivation.page_id = this.page_id;
            derivation.derivate_person_creator_pidm = this.derivate_person_creator_pidm;
            derivation.derivate_funtional_unity = this.derivate_funtional_unity;
            derivation.derivate_mail_recipient = this.derivate_mail_recipient;
            derivation.derivate_type = this.derivate_type;
            derivation.derivate_state = _ldr_states.LDR_STATE_DERIVATE_PENDING;
            derivation.derivate_date_created = DateTime.Now;

            var page_details = await page.getPageClaimsBook(this.page_id);
            if (page_details == null || (page_details.page_state != _ldr_states.LDR_STATE_PAGE_DERIVATED &&
                                         page_details.page_state != _ldr_states.LDR_STATE_PAGE_PENDING &&
                                         page_details.page_state != _ldr_states.LDR_STATE_PAGE_ANSWERED))
            {
                throw new Exception("Solo se puede derivar Reclamos con estado Pendiente o derivado.");
            }

            List<DerivationArea> list = await this.listDerivationFuntionalUnity(_ldr_states.LDR_ID_COMPANY_UCCI,
                derivation.page_id, derivation.derivate_funtional_unity);
            if (list.Any())
            {
                throw new Exception("ya derivaron a la unidad funcional.");
            }

            page.page_id = derivation.page_id;
            page.page_state = _ldr_states.LDR_STATE_PAGE_DERIVATED.ToString();

            if (derivation.derivate_type == _ldr_states.LDR_TYPE_DERIVATE_LEGAL_AREA)
            {
                page.page_state = _ldr_states.LDR_STATE_PAGE_ANSWERED.ToString();
            }

            var change_page = page.updatePage();

            if (change_page != true)
            {
                throw new Exception("No se pudo modificar el estado del reclamo.");
            }

            _nexoContext.ldr_tblDerivationArea.Add(derivation);
            var save = _nexoContext.SaveChanges();
            this.derivate_id = derivation.derivate_id;

            if (save <= 0)
            {
                throw new Exception("No se agrego la derivación.");
            }

            this.derivate_id = derivation.derivate_id;
            return true;
        }

        /// <summary>
        /// Modifica una derivación
        /// </summary>
        /// <returns></returns>
        public async Task<bool> updateDerivation()
        {
            var derivation = _nexoContext.ldr_tblDerivationArea.Find(this.derivate_id);
            if (this.derivate_person_creator_pidm != null)
            {
                derivation.derivate_person_creator_pidm = this.derivate_person_creator_pidm;
                derivation.derivate_date_modify = DateTime.Now;
            }

            if (this.derivate_funtional_unity != null)
            {
                derivation.derivate_funtional_unity = this.derivate_funtional_unity;
                derivation.derivate_date_modify = DateTime.Now;
            }

            if (this.derivate_mail_recipient != null)
            {
                derivation.derivate_mail_recipient = this.derivate_mail_recipient;
                derivation.derivate_date_modify = DateTime.Now;
            }

            if (this.derivate_state != null)
            {
                derivation.derivate_state = this.derivate_state;
                derivation.derivate_date_modify = DateTime.Now;
            }

            if (this.derivate_note_disabled != null)
            {
                derivation.derivate_note_disabled = this.derivate_note_disabled;
                derivation.derivate_date_modify = DateTime.Now;
            }

            var update = _nexoContext.SaveChanges();

            return update > 0 || derivation != null;
        }

        /// <summary>
        /// desestima una dericación
        /// </summary>
        /// <returns></returns>
        public async Task<bool> disableDerivation()
        {
            var derivation = _nexoContext.ldr_tblDerivationArea.Find(this.derivate_id);
            derivation.derivate_state = _ldr_states.LDR_STATE_DERIVATE_DISMISSED;
            derivation.derivate_date_modify = DateTime.Now;
            derivation.derivate_note_disabled = this.derivate_note_disabled;

            var update = _nexoContext.SaveChanges();
            if (update > 0)
            {
                /*========================================Enviar email========================================================*/

                var page = new PageClaimBook();
                var page_details = await page.getPageClaimsBook(this.page_id);

                List<string> emails = derivation.derivate_mail_recipient.Split(',').ToList();
                var mail = new MailSender();
                mail.compose("ldr_disable_derivation",
                        new
                        {
                            sede = Campus.get(page_details.page_campus).name,
                            year = page_details.page_year,
                            numeration = page_details.page_numeration,
                            note = derivation.derivate_note_disabled,
                        })
                    .destination(emails,
                        $"[Retiro][Derivación][{page_details.page_year}][{page_details.page_numeration}]")
                    .send();
                /*============================================================================================================*/

                var state_pending = new List<string> {_ldr_states.LDR_STATE_DERIVATE_PENDING};

                var state_resolved = new List<string> {_ldr_states.LDR_STATE_DERIVATE_RESOLVED};

                List<DerivationArea> list_derivation_pending =
                    await this.listDerivationStates(_ldr_states.LDR_ID_COMPANY_UCCI, derivation.page_id, state_pending);
                List<DerivationArea> list_derivation_answer =
                    await this.listDerivationStates(_ldr_states.LDR_ID_COMPANY_UCCI, derivation.page_id,
                        state_resolved);
                if (!list_derivation_pending.Any() && !list_derivation_answer.Any())
                {
                    page.page_id = derivation.page_id;
                    page.page_state = _ldr_states.LDR_STATE_PAGE_PENDING.ToString();
                    var change_page = page.updatePage();
                    return true;
                }

                if (!list_derivation_pending.Any() && list_derivation_answer.Any())
                {
                    page.page_id = derivation.page_id;
                    page.page_state = _ldr_states.LDR_STATE_PAGE_ANSWERED.ToString();
                    var change_page = page.updatePage();
                    return true;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Responder a una derivación
        /// </summary>
        /// <returns></returns>
        public async Task<bool> answerDerivation()
        {
            var derivation = _nexoContext.ldr_tblDerivationArea.Find(this.derivate_id);

            if (derivation == null || derivation.derivate_state != _ldr_states.LDR_STATE_DERIVATE_PENDING)
            {
                throw new Exception("La derivación no tiene el estado pendiente.");
            }

            derivation.derivate_state = _ldr_states.LDR_STATE_DERIVATE_RESOLVED;
            derivation.derivate_date_modify = DateTime.Now;

            var derivation_change = _nexoContext.SaveChanges();

            return derivation_change > 0;
        }

        /// <summary>
        /// aceptar la respuesta de derivación
        /// </summary>
        /// <param name="user_pidm">código de usuario</param>
        /// <returns></returns>
        public async Task<bool> acceptAnswerDerivation(string user_pidm)
        {
            var derivation = _nexoContext.ldr_tblDerivationArea.Find(this.derivate_id);

            var details_derivate = await this.getDerivationClaim(this.derivate_id, user_pidm);

            if (details_derivate.staff_involved_type != _ldr_states.LDR_TYPE_INVOLVED_BOSS)
            {
                throw new Exception("USted no puede aceptar la respuesta");
            }

            if (derivation == null || derivation.derivate_state != _ldr_states.LDR_STATE_DERIVATE_RESOLVED)
            {
                throw new Exception("La derivación no tiene el estado resuelto.");
            }

            derivation.derivate_state = _ldr_states.LDR_STATE_DERIVATE_FINALIZED;
            derivation.derivate_date_modify = DateTime.Now;
            var derivation_change = _nexoContext.SaveChanges();
            if (derivation_change > 0)
            {
                var state_pending = new List<string>
                {
                    _ldr_states.LDR_STATE_DERIVATE_PENDING,
                    _ldr_states.LDR_STATE_DERIVATE_RESOLVED
                };
                List<DerivationArea> list_derivation = await this.listDerivationStates(_ldr_states.LDR_ID_COMPANY_UCCI,
                    derivation.page_id, state_pending);
                if (!list_derivation.Any())
                {
                    var page = new PageClaimBook
                    {
                        page_id = derivation.page_id,
                        page_state = _ldr_states.LDR_STATE_PAGE_ANSWERED
                    };
                    var change_page = page.updatePage();
                    return true;
                }

                return true;
            }

            return false;
        }

        public static implicit operator DerivationArea(LDRtblDerivationArea ltd)
        {
            if (ltd == null) return null;
            var item = new DerivationArea
            {
                derivate_id = ltd.derivate_id,
                page_id = ltd.page_id,
                derivate_person_creator_pidm = ltd.derivate_person_creator_pidm,
                derivate_funtional_unity = ltd.derivate_funtional_unity,
                derivate_mail_recipient = ltd.derivate_mail_recipient,
                derivate_state = ltd.derivate_state,
                derivate_date_created = ltd.derivate_date_created,
                derivate_date_modify = ltd.derivate_date_modify
            };
            return item;
        }
    }
}
