﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.LDR;
using ACTools.Configuration;
using _statesClaims = ACTools.Constants.ClaimsBookConstants;
namespace ACBusiness.ComplaintsBook
{
    public class FilePage
    {
        public int file_id { get; set; }
        public int page_id { get; set; }
        public int? derivate_id { get; set; }
        public int? answer_id { get; set; }
        public string file_name { get; set; }
        public string file_route { get; set; }
        public string file_type { get; set; }
        public string user_pidm { get; set; }
        public DateTime date_registry { get; set; }
        public string file_state { get; set; }

        NEXOContext _db = new NEXOContext();

        /// <summary>
        /// Lista archivos de un reclamo
        /// </summary>
        /// <param name="page_id">código de reclamo</param>
        /// <returns></returns>
        public async Task<List<FilePage>> ListFilePage(int page_id)
        {
            List<FilePage> list_book_file = _db.ldr_tblFile.Where(tbf =>
                tbf.page_id == page_id &&
                tbf.derivate_id == null &&
                (tbf.file_state == _statesClaims.LDR_STATE_FILE_CREATE || tbf.file_state == _statesClaims.LDR_STATE_FILE_FINALIZED)
                ).ToList().Select(tfc => new FilePage
                {
                    file_id = tfc.file_id,
                    page_id = tfc.page_id,
                    derivate_id = tfc.derivate_id,
                    answer_id = tfc.answer_id,
                    file_name = tfc.file_name,
                    file_route = AppSettings.reports["output-ldr"] + tfc.file_route,
                    file_type = tfc.file_type,
                    user_pidm = tfc.user_pidm,
                    date_registry = tfc.date_registry,
                    file_state = tfc.file_state
                }
                ).ToList();
            return list_book_file;
        }

        /// <summary>
        /// Listar archivos de una derivación
        /// </summary>
        /// <param name="derivate_id">código de derivación</param>
        /// <returns></returns>
        public async Task<List<FilePage>> ListFileDerivation(int derivate_id)
        {
            List<FilePage> list_book_file = _db.ldr_tblFile.Where(tbf =>
                tbf.derivate_id == derivate_id
                ).ToList().Select(tfc => new FilePage
                {
                    file_id = tfc.file_id,
                    page_id = tfc.page_id,
                    derivate_id = tfc.derivate_id,
                    answer_id = tfc.answer_id,
                    file_type = tfc.file_type,
                    file_name = tfc.file_name,
                    file_route = tfc.file_route,
                    user_pidm = tfc.user_pidm,
                    date_registry = tfc.date_registry,
                    file_state = tfc.file_state,
                }
                ).ToList();
            return list_book_file;
        }

        /// <summary>
        /// Lista los archivos de una respuesta
        /// </summary>
        /// <param name="answer_id">código de respuesta</param>
        /// <returns></returns>
        public async Task<List<FilePage>> ListFileAnswerClaim(int answer_id)
        {
            List<FilePage> list_book_file = _db.ldr_tblFile.Where(result =>
                result.answer_id == answer_id &&
                (result.file_state == _statesClaims.LDR_STATE_FILE_RESOLVED ||
                 result.file_state == _statesClaims.LDR_STATE_FILE_FINALIZED)
                ).ToList().Select(result => new FilePage
                {
                    file_id = result.file_id,
                    page_id = result.page_id,
                    derivate_id = result.derivate_id,
                    answer_id = result.answer_id,
                    file_type = result.file_type,
                    file_name = result.file_name,
                    file_route = result.file_route,
                    user_pidm = result.user_pidm,
                    date_registry = result.date_registry,
                    file_state = result.file_state,
                }
                ).ToList();
            return list_book_file;
        }

        public async Task<List<FilePage>> ListFilesAnswerOfPage(int page_id) {
            List<FilePage> list_book_file = _db.ldr_tblFile.Where(result =>
                result.page_id == page_id &&
                result.answer_id != null &&
                result.file_state != _statesClaims.LDR_STATE_FILE_DISMISSED
                ).ToList().Select(result => new FilePage
                {
                    file_id = result.file_id,
                    page_id = result.page_id,
                    derivate_id = result.derivate_id,
                    answer_id = result.answer_id,
                    file_type = result.file_type,
                    file_name = result.file_name,
                    file_route = result.file_route,
                    user_pidm = result.user_pidm,
                    date_registry = result.date_registry,
                    file_state = result.file_state,
                }
                ).ToList();
            return list_book_file;
        }

        public async Task<List<FilePage>> ListFilesAnswerOfDerivate(int page_id, int derivate_id)
        {
            List<FilePage> list_book_file = _db.ldr_tblFile.Where(result =>
                result.page_id == page_id &&
                result.answer_id == derivate_id &&
                result.file_state != _statesClaims.LDR_STATE_FILE_DISMISSED
                ).ToList().Select(result => new FilePage
                {
                    file_id = result.file_id,
                    page_id = result.page_id,
                    derivate_id = result.derivate_id,
                    answer_id = result.answer_id,
                    file_type = result.file_type,
                    file_name = result.file_name,
                    file_route = result.file_route,
                    user_pidm = result.user_pidm,
                    date_registry = result.date_registry,
                    file_state = result.file_state,
                }
                ).ToList();
            return list_book_file;
        }


        /// <summary>
        /// Obtiene un archivo
        /// </summary>
        /// <param name="file_id">código de archivo</param>
        /// <returns></returns>
        public List<string> GetFileData(int file_id) {
            LDRtblFile file = _db.ldr_tblFile.FirstOrDefault(result => result.file_id == file_id);
            List<string> data = new List<string> { };
            if (file != null)
            {
                string file_path = null;
                string file_type = null;
                try
                {

                    file_path = $"{AppSettings.reports["output-ldr"]}{file.file_route}";
                    file_type = $"{file.file_type}";
                    data.Add(file_path);
                    data.Add(file_type);
                }
                catch (System.IO.FileNotFoundException ex) { }
                catch (System.ArgumentNullException ex) { }
                finally { }
                return data;
            }
            else {
                return null;
            }
        }

        public bool SaveFile(string page_id, string derivation_id, string answer_id, string user_pidm, HttpPostedFile file, string state_file) {
            LDRtblFile datafile = new LDRtblFile();
            string name_state_file = _statesClaims.LDR_FILE_STATUS[state_file];
            var _path = string.Format("{0}/{1}/{2}", AppSettings.reports["output-ldr"], page_id, name_state_file);
            if (state_file.Trim() == _statesClaims.LDR_STATE_FILE_DERIVATED || state_file.Trim() == _statesClaims.LDR_STATE_FILE_ANSWER)
            {
                _path = string.Format("{0}/{1}", _path, derivation_id);
                datafile.derivate_id = int.Parse(derivation_id);
            }
            if (state_file.Trim() == _statesClaims.LDR_STATE_FILE_RESOLVED || state_file.Trim() == _statesClaims.LDR_STATE_FILE_FINALIZED)
            {
                datafile.answer_id = int.Parse(answer_id);
            }
            //id = this.id;
            datafile.page_id = int.Parse(page_id);
            datafile.file_name = file.FileName;
            datafile.file_route = _path + "/" + file.FileName; // definir
            datafile.file_type = file.ContentType;
            datafile.user_pidm = user_pidm;
            datafile.date_registry = DateTime.Now;
            datafile.file_state = state_file;
            _db.ldr_tblFile.Add(datafile);

            int save = _db.SaveChanges();
            this.file_id = datafile.file_id;
            if (save > 0) {
                try
                {
                    string type = Path.GetExtension(datafile.file_name);
                    var filePath = string.Format("{0}/{1}{2}", _path, datafile.file_id, type);
                    if (!Directory.Exists(filePath))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(_path);
                    }
                    if (File.Exists(filePath))
                    {
                        file.SaveAs(filePath);
                    }
                    else
                    {
                        file.SaveAs(filePath);
                    }
                    LDRtblFile fileupdate = new LDRtblFile();
                    fileupdate = _db.ldr_tblFile.Find(datafile.file_id);
                    fileupdate.file_route = $"{page_id}/{name_state_file}/{datafile.file_id}{type}";
                    if (state_file.Trim() == _statesClaims.LDR_STATE_FILE_DERIVATED || state_file.Trim() == _statesClaims.LDR_STATE_FILE_ANSWER)
                    {
                        fileupdate.file_route = $"{page_id}/{name_state_file}/{derivation_id}/{datafile.file_id}{type}";
                    }
                    
                    _db.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    LDRtblFile filedel = new LDRtblFile();
                    filedel = _db.ldr_tblFile.Find(datafile.file_id);
                    _db.SaveChanges();
                    return false;
                    // throw;
                }
            }
            else
            {
                return false;
            }
        }
        
        public bool saveFileInfo(int page_id, string path, string file_state)
        {
            
            FileInfo info_pdf = new FileInfo(path);

            
            LDRtblFile datafile = new LDRtblFile();

            string name_state_file = _statesClaims.LDR_FILE_STATUS[file_state];
            string _path = string.Format("{0}/{1}/{2}", page_id, name_state_file, info_pdf.Name);
            string contentType = MimeMapping.GetMimeMapping(info_pdf.Name);

            datafile.page_id = page_id;
            datafile.file_state = file_state;
            datafile.file_type = contentType;
            datafile.file_name = info_pdf.Name;
            datafile.date_registry = DateTime.Now;
            datafile.file_route = _path;

            _db.ldr_tblFile.Add(datafile);
            int save = _db.SaveChanges();
            this.file_id = datafile.file_id;

            if (save > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        
    }
}



