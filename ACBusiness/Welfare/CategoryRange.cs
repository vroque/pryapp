﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.BUW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Welfare
{
    /// <summary>
    /// Rangos de precios de Escalas para 
    /// pesion de los alumnos
    /// </summary>
    public class CategoryRange
    {
        #region properties
        public string div { get; set; }
        public string levl { get; set; }
        public string department { get; set; }
        public string program { get; set; }
        public string term { get; set; }
        public string category { get; set; }
        public decimal ammount_min { get; set; }
        public decimal ammount_max { get; set; }
        #endregion properties

        #region methods

        #endregion methods

        #region static methods
        public static string getRange(string div, string levl, 
                               string department, string term, 
                               string program, int ammount)
        {
            NEXOContext db = new NEXOContext();

            BUWTblCategoryRange range = db.buw_category_ranges.FirstOrDefault(f =>
                f.div == div
                && f.levl == levl
                && f.department == department
                && f.term == term
                && f.program == program
                &&  (
                    f.ammountMax >= ammount
                    && f.ammountMin <= ammount
                    )
                );
            if (range == null)
                return null;

            return range.category;
        }
        #endregion atatic methods

    }
}
