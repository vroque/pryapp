﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CIC;

namespace ACBusiness.LanguagesCenter
{
    public class Enrolled
    {
        #region Properties

        public int Id { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string AcademicPeriod { get; set; }
        public int Cycle { get; set; }
        public string Classroom { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? EnrollmentDate { get; set; }
        public string TypeOfStudent { get; set; }
        public string LanguageProgram { get; set; }
        public string LastApprovedCycle { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Matriculados en el CIC
        /// </summary>
        /// <returns>Lista de matriculados en el CIC</returns>
        public static IEnumerable<Enrolled> GetAll()
        {
            var nexoContext = new NEXOContext();
            var cicEnrolled = nexoContext.cic_enrolled.ToList();
            var enrolled = cicEnrolled.Select<CICEnrolled, Enrolled>(e => e).OrderBy(o => o.StudentName).ToList();
            return enrolled;
        }

        /// <summary>
        /// Matriculados en el CIC por Idioma-Programa-Nivel
        /// </summary>
        /// <param name="languageId">Código de idioma CIC</param>
        /// <param name="languageProgram">Código de Programa CIC</param>
        /// <param name="languageLevelId">Código de Nivel de Idioma</param>
        /// <returns></returns>
        public static IEnumerable<Enrolled> GetByLangAndLangProgramAndLangLevel(string languageId,
            string languageProgram,
            string languageLevelId)
        {
            var nexoContext = new NEXOContext();

            // Total matriculados
            var enrolled = GetAll();

            // Niveles
            var cicLanguageLevel =
                nexoContext.cic_language_level.FirstOrDefault(w => w.LanguageLevelId == languageLevelId);

            if (cicLanguageLevel == null) return null;

            var startCycle = cicLanguageLevel.StartCycle;
            var finalCycle = cicLanguageLevel.FinalCycle;

            var enrolledByLevel = enrolled.Where(w => w.LanguageId == languageId &&
                                                      string.Equals(w.LanguageProgram, languageProgram) &&
                                                      (w.Cycle >= startCycle && w.Cycle <= finalCycle))
                .OrderBy(o => o.StudentName).ToList();

            return enrolledByLevel;
        }

        #endregion Methods

        #region Implicit Operators

        /// <summary>
        /// Conversión 'CICEnrolled' a 'Enrolled'
        /// </summary>
        /// <param name="cicEnrolled">Tabla CIC.Enrolled</param>
        /// <returns></returns>
        public static implicit operator Enrolled(CICEnrolled cicEnrolled)
        {
            var enrolled = new Enrolled
            {
                Id = cicEnrolled.Id,
                StudentId = cicEnrolled.StudentId,
                StudentName = cicEnrolled.StudentName,
                LanguageId = cicEnrolled.LanguageId,
                LanguageName = cicEnrolled.LanguageName,
                AcademicPeriod = cicEnrolled.AcademicPeriod,
                Cycle = cicEnrolled.Cycle,
                Classroom = cicEnrolled.Classroom,
                StartDate = cicEnrolled.StartDate,
                EndDate = cicEnrolled.EndDate,
                EnrollmentDate = cicEnrolled.EnrollmentDate,
                TypeOfStudent = cicEnrolled.TypeOfStudent,
                LanguageProgram = cicEnrolled.LanguageProgram,
                LastApprovedCycle = cicEnrolled.LastApprovedCycle
            };
            return enrolled;
        }

        #endregion Implicit Operators
    }
}