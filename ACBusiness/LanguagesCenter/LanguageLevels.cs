﻿using System.Collections.Generic;
using System.Linq;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.CIC;

namespace ACBusiness.LanguagesCenter
{
    public class LanguageLevels
    {
        #region Properties

        public Language Language { get; set; }
        public int LastCycle { get; set; }
        public List<LanguageLevel> LanguageLevelList { get; set; }
        
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Methods

        public List<LanguageLevels> GetLevelsByClassroom(IEnumerable<DBOtblNotasEXT> tblNotasExtList)
        {
            var niveles = new List<LanguageLevels>();
            
            foreach (var tblNotasExt in tblNotasExtList)
            {
                var languagesLevel = new LanguageLevels();
                var level = new LanguageLevel().GetLevelByClassroom(tblNotasExt.IDSeccionC);
                List<int> completedLangLevelFinalCycleList =
                    new LanguageLevel().CompletedLangLevelFinalCycleList(tblNotasExt);
                
                List<CICLanguageLevel> cicLanguageLevel = _nexoContext.cic_language_level
                    .Where(w => w.LanguageId == level.LanguageId &&
                                w.LanguageProgramId == level.LanguageProgramId &&
                                completedLangLevelFinalCycleList.Contains(w.FinalCycle))
                    .OrderBy(o => o.StartCycle).ToList();
                List<LanguageLevel> languageLevel =
                    cicLanguageLevel.Select<CICLanguageLevel, LanguageLevel>(s => s).ToList();

                languagesLevel.Language = new Language
                {
                    LanguageId = level.LanguageId,
                    LanguageName = level.LanguageName
                };
                languagesLevel.LastCycle = int.Parse(Cycle.GetCycleNumber(tblNotasExt));
                languagesLevel.LanguageLevelList = languageLevel;

                if (languageLevel.Any()) niveles.Add(languagesLevel);
            }

            return niveles;
        }
        
        #endregion Methods
    }
}