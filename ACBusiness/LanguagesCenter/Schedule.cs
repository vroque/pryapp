﻿using ACBusiness.Academic;
using ACBusiness.Institutional;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACBusiness.LanguagesCenter
{
    /// <summary>
    /// Objeto de ofertas horarias para el centro de idiomas
    /// </summary>
    public class CICOffer
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string section { get; set; }
        public string studyPlan { get; set; }
        public string course { get; set; }
        public string teacherID { get; set; }
        public string teacher { get; set; }
        public string campus { get; set; }
        public string program { get; set; }
        public string modality
        {
            get { return new CICSection(this.section).modality;}
        }
        public int cycle
        {
            get { return new CICSection(this.section).cycle; }
        }
        private readonly DBOContext _apec = new DBOContext();

        /// <summary>
        /// Busca todas las ofertas horarias del centro de idioma
        /// segun un campus
        /// </summary>
        /// <param name="campus">filial o campus</param>
        /// <returns></returns>
        public List<CICOffer> getActiveOffers(string campus)
        {
            Campus campusObj = Campus.get(campus);
            if (campusObj == null) throw new Exception("No existe la sede/filial");
            DateTime today = DateTime.Now;
            List<CICOffer> offers = _apec.tbl_seccion_acad
                .Join(
                    _apec.view_docentes,
                    sec => sec.IDDocente,
                    doc => doc.IDDocente,
                    (sec, doc) => new { sec, doc }
                ).Where(f =>
                    f.sec.IDDependencia == "UCCI"
                    && _cic.ACTIVE_LANGUAGES_LIST.Contains(f.sec.IDEscuela)
                    && f.sec.IDSede == campusObj.code
                    && f.sec.FechaIni >= today
                ).Select(f => new CICOffer
                {
                    startDate = f.sec.FechaIni ?? DateTime.Today,
                    endDate = f.sec.FechaFin ?? DateTime.Today,
                    section = f.sec.IDSeccion,
                    course = f.sec.IDAsignatura,
                    teacherID = f.sec.IDDocente,
                    teacher = f.doc.NomCompleto,
                    campus = f.sec.IDSede,
                    program = f.sec.IDEscuela,
                    studyPlan = f.sec.IDPlanEstudio
                })
                .ToList();
            // hacemos este consulta en codigo por facilidad
            // para no hacer substriong en la BD
            offers = offers.Where(f =>
                    _cic.UC_ACTIVE_LANGUAGES_MODALITIES.Contains(f.modality)
                ).ToList();
            return offers;

        }

        /// <summary>
        /// Busca todas las ofertas horarias del centro de idioma
        /// segun el campus y u el idioma 
        /// </summary>
        /// <param name="campus">filial o campus</param>
        /// <param name="language">codigo de idioma</param>
        /// <returns></returns>
        public async Task<List<CICOffer>> getActiveOffersByLanguage(string campus, string language)
        {
            Campus campusObj = Campus.get(campus);
            if (campusObj == null) throw new Exception("No existe la sede/filial");
            DateTime today = DateTime.Now;
            List<CICOffer> offers = await _apec.tbl_seccion_acad.Join(
                    _apec.view_docentes,
                    sec => sec.IDDocente,
                    doc => doc.IDDocente,
                    (sec, doc) => new { sec, doc }
                ).Where(f =>
                    f.sec.IDDependencia == "UCCI"
                    && f.sec.IDEscuela == language
                    && f.sec.IDSede == campusObj.code
                    && f.sec.FechaIni >= today
                ).Select(f => new CICOffer
                {
                    startDate = f.sec.FechaIni ?? DateTime.Today,
                    endDate = f.sec.FechaFin ?? DateTime.Today,
                    section = f.sec.IDSeccion,
                    course = f.sec.IDAsignatura,
                    teacherID = f.sec.IDDocente,
                    teacher = f.doc.NomCompleto,
                    campus = f.sec.IDSede,
                    program = f.sec.IDEscuela
                }).OrderBy(f => f.campus)
                .ToListAsync();
            return offers;

        }

        /// <summary>
        /// Busca todas las ofertas horarias del centro de idioma
        /// por campus, idioma y ciclo
        /// </summary>
        /// <param name="campus">filial o campus</param>
        /// <param name="language">codigo del idioma</param>
        /// <param name="cycle">numero del ciclo</param>
        /// <returns></returns>
        public async Task<List<CICOffer>> getActiveOfferByCycle(string campus, string language, int cycle)
        {
            List<CICOffer> list = await getActiveOffersByLanguage(campus, language);
            list = list.Where(f => f.cycle == cycle).ToList();
            return list;
        }

        /// <summary>
        /// Obtener la oferta horaria actual de un alumno 
        /// * Que el curso de idiomas este llevandoce en la actualidad
        /// </summary>
        /// <param name="person_id"></param>
        /// <returns></returns>
        public async Task<CICOffer> getByStudent(decimal person_id)
        {
            DateTime today = DateTime.Today;
            var a = await _apec.tbl_alumno_estado
                .Join(
                    _apec.view_alumno_basico,
                    es => es.IDAlumno,
                    al => al.IDAlumno,
                    (es, al) => new { es, al }
                ).Join(
                    _apec.tbl_seccion_acad,
                    ea => new { d = ea.es.IDDependencia, e = ea.es.IDEscuela, s = ea.es.IDSeccionC, c = ea.es.IDSede },
                    sa => new { d = sa.IDDependencia, e = sa.IDEscuela, s = sa.IDSeccion, c = sa.IDSede },
                    (ea, sa) => new { ea.es, ea.al, sa }
                ).Join(
                    _apec.view_docentes,
                    data => data.sa.IDDocente,
                    doc => doc.IDDocente,
                    (data, doc) => new { data.es, data.al, data.sa, doc }
                ).Where(f =>
                    f.es.IDDependencia == _acad.DEFAULT_DIV
                    && _cic.ACTIVE_LANGUAGES_LIST.Contains(f.es.IDEscuela)
                    && f.al.IDPersonaN == person_id
                    && f.sa.FechaFin >= today
                ).OrderByDescending(f => f.sa.FechaIni)
                .FirstOrDefaultAsync();
            if (a == null) return null;
            CICOffer offer = new CICOffer
            {
                startDate = a.sa.FechaIni ?? DateTime.Today,
                endDate = a.sa.FechaFin ?? DateTime.Today,
                section = a.sa.IDSeccion,
                studyPlan = a.sa.IDPlanEstudio,
                course = a.sa.IDAsignatura,
                teacherID = a.sa.IDDocente,
                teacher = a.doc.NomCompleto,
                campus = a.sa.IDSede,
                program = a.sa.IDEscuela,
            };
            return offer;
        }

        /// <summary>
        /// Obtener la oferta horaria actual de un alumno 
        /// * Que el curso de idiomas este llevandoce en la actualidad
        /// </summary>
        /// <param name="person_id"></param>
        /// <returns></returns>
        public async Task<CICOffer> getByStudentLast(decimal person_id)
        {
            DateTime today = DateTime.Today;
            var a = await _apec.tbl_alumno_estado
                .Join(
                    _apec.view_alumno_basico,
                    es => es.IDAlumno,
                    al => al.IDAlumno,
                    (es, al) => new { es, al }
                ).Join(
                    _apec.tbl_seccion_acad,
                    ea => new { d = ea.es.IDDependencia, e = ea.es.IDEscuela, s = ea.es.IDSeccionC, c = ea.es.IDSede },
                    sa => new { d = sa.IDDependencia, e = sa.IDEscuela, s = sa.IDSeccion, c = sa.IDSede },
                    (ea, sa) => new { ea.es, ea.al, sa }
                ).Join(
                    _apec.view_docentes,
                    data => data.sa.IDDocente,
                    doc => doc.IDDocente,
                    (data, doc) => new { data.es, data.al, data.sa, doc }
                ).Where(f =>
                    f.es.IDDependencia == _acad.DEFAULT_DIV
                    && _cic.ACTIVE_LANGUAGES_LIST.Contains(f.es.IDEscuela)
                    && f.al.IDPersonaN == person_id
                ).OrderByDescending(f => f.sa.FechaIni)
                .FirstOrDefaultAsync();
            if (a == null) return null;
            CICOffer offer = new CICOffer
            {
                startDate = a.sa.FechaIni ?? DateTime.Today,
                endDate = a.sa.FechaFin ?? DateTime.Today,
                section = a.sa.IDSeccion,
                studyPlan = a.sa.IDPlanEstudio,
                course = a.sa.IDAsignatura,
                teacherID = a.sa.IDDocente,
                teacher = a.doc.NomCompleto,
                campus = a.sa.IDSede,
                program = a.sa.IDEscuela,
            };
            return offer;
        }

    }

    /// <summary>
    /// Lista dehorarios del centro de idiomas
    /// </summary>
    public class CICSchedule: Schedule
    {
        public string modality { get; set; } 
        private DBOContext _apec = new DBOContext();

        /// <summary>
        /// Obtiene el horario de idiomas actual segun el codigo de alumno
        /// </summary>
        /// <param name="person_id">codigo de persona de banner(PIDM)</param>
        /// <returns></returns>
        public async Task<CICOfferSchedule> getOfferSchedule(decimal person_id)
        {
            CICOffer offer = new CICOffer();
            offer = await offer.getByStudent(person_id);
            List<CICSchedule> schedule = getSchedule(offer);
            return new CICOfferSchedule {
                offer = offer,
                schedule = schedule
            };
        }

        /// <summary>
        /// Obtiene el horario de idiomas actual segun el codigo de alumno
        /// </summary>
        /// <param name="person_id">codigo de persona de banner(PIDM)</param>
        /// <returns></returns>
        public async Task<CICOfferSchedule> getOfferScheduleLast(decimal person_id)
        {
            CICOffer offer = new CICOffer();
            offer = await offer.getByStudentLast(person_id);
            List<CICSchedule> schedule = getSchedule(offer);
            return new CICOfferSchedule
            {
                offer = offer,
                schedule = schedule
            };
        }



        /// <summary>
        /// OBtiene el horaira de idiomas de un determinado oferta (seccion)
        /// </summary>
        /// <param name="offer">Objeto de detalles de una seccion de idiomas</param>
        /// <returns></returns>
        public List<CICSchedule> getSchedule(CICOffer offer)
        {
            List<DBOTblProgHorario> list = _apec.tbl_programacion_horario.Where(f =>
               f.IDSede == offer.campus
               && f.IDDependencia == "UCCI"
               && f.IDEscuela == offer.program
               && f.IDSeccion == offer.section
               && !( // ignorea los campos que no dan valor
                f.IDOferta1 == "0" &&
                f.IDOferta2 == "0" &&
                f.IDOferta3 == "0" &&
                f.IDOferta4 == "0" &&
                f.IDOferta5 == "0" &&
                f.IDOferta6 == "0" &&
                f.IDOferta7 == "0"
               )
            ).ToList();
            List<CICSchedule> data = new List<CICSchedule>();
           
            if (list.Count <= 0) return null;

            data.Add(getScheduleByDay(list.Where(f => f.IDOferta1 == "1").ToList(), offer, "Lunes"));
            data.Add(getScheduleByDay(list.Where(f => f.IDOferta2 == "1").ToList(), offer, "Martes"));
            data.Add(getScheduleByDay(list.Where(f => f.IDOferta3 == "1").ToList(), offer, "Miércoles"));
            data.Add(getScheduleByDay(list.Where(f => f.IDOferta4 == "1").ToList(), offer, "Jueves"));
            data.Add(getScheduleByDay(list.Where(f => f.IDOferta5 == "1").ToList(), offer, "Viernes"));
            data.Add(getScheduleByDay(list.Where(f => f.IDOferta6 == "1").ToList(), offer, "Sábado"));
            data.Add(getScheduleByDay(list.Where(f => f.IDOferta7 == "1").ToList(), offer, "Domingo"));

            return data;
        }

        /// <summary>
        /// Compone un grupo de horario por dia
        /// </summary>
        /// <param name="list"> Lista de horarios</param>
        /// <param name="offer">Objeto de oferta</param>
        /// <param name="day">Dia</param>
        /// <returns></returns>
        private CICSchedule getScheduleByDay(List<DBOTblProgHorario> list, CICOffer offer, string day)
        {
            var schedule = new CICSchedule { day = day, courses = new List<ScheduleCourse>() };
            schedule.modality = offer.modality;
            List<ScheduleCourse> listFilter = new List<ScheduleCourse>();
            foreach(var item in list)
            {
                ScheduleCourse c = new ScheduleCourse();
                c.id = item.IDAsignatura;
                c.nrc = item.IDAsignatura;
                //course.classroom = item.classroom;
                c.start = $"{item.IDHoraPH:HH}:{item.IDHoraPH:mm}";
                c.end = $"{item.IDHoraPHfin:HH}:{item.IDHoraPHfin:mm}";
                c.dateStart = offer.startDate;
                c.dateEnd = offer.endDate;
                c.name = "";
                c.module = item.IDAsignatura;
                c.teacher = offer.teacher;
                
                switch (day)
                {
                    case "Lunes": c.classroom = item.IDAmbiente1 ?? "--"; break;
                    case "Martes": c.classroom = item.IDAmbiente2 ?? "--"; break;
                    case "Miércoles": c.classroom = item.IDAmbiente3 ?? "--"; break;
                    case "Jueves": c.classroom = item.IDAmbiente4 ?? "--"; break;
                    case "Viernes": c.classroom = item.IDAmbiente5 ?? "--"; break;
                    case "Sábado": c.classroom = item.IDAmbiente6 ?? "--"; break;
                    case "Domingo": c.classroom = item.IDAmbiente7 ?? "--"; break;
                    // default: c.classroom = "--";
                }
                listFilter.Add(c);
            }
            listFilter = listFilter.OrderBy(f => f.start).ToList();
            var course = listFilter.FirstOrDefault();
            if (course == null) return schedule;

            for (var i = 1; i < listFilter.Count; i++)
            {
                if (this.compareTimes(course.end, listFilter[i].start)) //son continuos
                {
                    course.end = listFilter[i].end;
                }
                else
                {
                    schedule.courses.Add(course);
                    course = listFilter[i];
                }
            }

            schedule.courses.Add(course);

            for (var x = 0; x < schedule.courses.Count; x++)
            {
                for (var y = x + 1; y < schedule.courses.Count; y++)
                {
                    if (timeMinutes(schedule.courses[x].start) > timeMinutes(schedule.courses[y].start))
                    {
                        ScheduleCourse a = schedule.courses[x];
                        schedule.courses[x] = schedule.courses[y];
                        schedule.courses[y] = a;
                    }
                }
            }

            return schedule;
        }

        /// <summary>
        /// Compara fecha final e inicial para decidir si se fusionan dos bloques de horarios
        /// </summary>
        /// <param name="endTime"></param>
        /// <param name="startTime"></param>
        /// <returns></returns>
        private new bool compareTimes(string endTime, string startTime)
        {
            var h1 = int.Parse(endTime.Substring(0, 2));
            var m1 = int.Parse(endTime.Substring(3, 2));
            var h2 = int.Parse(startTime.Substring(0, 2));
            var m2 = int.Parse(startTime.Substring(3, 2));

            var t1 = h1 * 60 + m1;
            var t2 = h2 * 60 + m2;

            return t1 == t2;
        }

    }

    public struct CICOfferSchedule
    {
        public CICOffer offer { get; set; }
        public List<CICSchedule> schedule { get; set; }

    }
}

