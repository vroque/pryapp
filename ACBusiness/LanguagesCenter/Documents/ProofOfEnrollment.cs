﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.BDUCCI;
using ACTools.PDF.Documents.LanguagesCenter;
using ACTools.SuperString;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using _cau = ACTools.Constants.AtentionCenterConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter.Documents
{
    /// <inheritdoc />
    /// <summary>
    /// Constancia de Matrícula
    /// </summary>
    public class ProofOfEnrollment : Document
    {
        #region Properties

        public List<Cycle> CycleList { get; set; }
        public bool Enrolled { get; set; }
        private readonly DBOContext _dboContext = new DBOContext();

        private const int DocumentId = _cau.DOC_CIC_PROOF_OF_ENROLLMENT;

        #endregion Properties

        #region Constructors

        public ProofOfEnrollment()
        {
        }

        public ProofOfEnrollment(Academic.AcademicProfile academicProfile)
        {
            Sync(DocumentId, academicProfile);
            SyncEnrolledCycles(academicProfile);
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Llena la lista de ciclos en los cuales el estudiante registra matrícula con antiguedad NO mayor a 7 días
        /// </summary>
        private void SyncEnrolledCycles(Academic.AcademicProfile academicProfile)
        {
            var enrolledCycles = new Student().GetAllActiveEnrolledCycles(academicProfile.person_id);
            CycleList = new List<Cycle>();

            foreach (var cycle in enrolledCycles)
            {
                var c = new Cycle(cycle);
                CycleList.Add(c);
            }

            Enrolled = CycleList.Any();
        }

        public static List<Cycle> GetEnrolledCyclesByLanguageId(decimal studentPidm, string languageId)
        {
            var enrolledCycles = new Student().GetAllActiveEnrolledCycles(studentPidm);

            return enrolledCycles.Select(cycle => new Cycle(cycle)).ToList();
        }

        /// <summary>
        /// Creación de solicitud para Constancia de Matrícula
        /// </summary>
        /// <param name="student">Estudiante UC</param>
        /// <param name="academicProfile">Perfil académico del estudiante UC</param>
        /// <param name="classroom">Código de aula</param>
        /// <param name="description">Descripción de la solicitud</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public async Task<ACRequest> CreateRequestAsync(Academic.Student student,
            Academic.AcademicProfile academicProfile,
            string classroom, string description, string user)
        {
            var json = JsonConvert.SerializeObject(new {description, classroom});

            var cycle = new Cycle(classroom);

            var cicStudent = new Student(student.person_id, cycle.LanguageLevel.LanguageId);

            var request = await ComposeRequestAsync(cicStudent, academicProfile);

            request.document_id = DocumentId;
            request.description = json;
            request.cost = cost.amount;

            if (request.state != _cau.STATE_CANCEL) request.state = _cau.STATE_PAYMENT_PENDING;

            var i = await request.CreateAsync(user);

            return i == 1 ? request : null;
        }

        /// <summary>
        /// Generación de documento para la Constancia de Matrícula del Centro de Idiomas
        /// </summary>
        /// <param name="request">Solicitud</param>
        /// <param name="student">Estudiante UC</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public async Task<string> GenerateDocumentAsync(ACRequest request, Academic.Student student, string user)
        {
            var acSignatory = new ACSignatory();

            var cicStudent = new Student(student.person_id, request.program);

            var signs = acSignatory.listByACDocument(DocumentId, null);

            var classroom = JObject.Parse(request.description)["classroom"].ToString();
            var langLevel = new LanguageLevel().GetLevelByClassroom(classroom);
            var cycle = new Cycle(classroom);
            var cycleDuration = _dboContext.tbl_seccion_c.FirstOrDefault(w => w.IDSeccionC == classroom);

            var data = new ProofOfEnrollmentPDF.Data
            {
                name_file = $"{request.id}-{request.document_number}-{student.id}",
                user = user,
                signs = acSignatory.listPDFSign(signs),
                student_id = student.id,
                student_names = student.full_name,
                student_gender = student.gender,
                college_name = string.Empty,
                program_name = string.Empty,
                request_id = request.id,
                document_number = request.document_number,
                GenderData = Academic.Student.getGenderData(student.id),
                LanguageCycle = cycle.CycleNumber.numberToRoman(),
                LanguageLevel = langLevel.LanguageLevelName,
                LanguageName = _cic.LANGUAGES[request.program],
                StartDate = cycleDuration?.FecInic ?? DateTime.Now,
                EndDate = cycleDuration?.FecFin ?? DateTime.Now,
                Schedule = "",
                Source = _cic.SRC_CIC_PDF
            };

            var outputPath = ProofOfEnrollmentPDF.Generate(data);

            // Actualizar solicitud
            if (outputPath == null) return null;
            var message = $"Solicitud generada el {DateTime.Now}, por el usuario: {user}";
            await request.goDeliveryPending(message, user);
            return outputPath;
        }

        #endregion Methods
    }
}