﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.AtentionCenter;
using ACBusiness.Personal;
using ACPermanence.Contexts.NEXO;
using ACTools.PDF.Documents.LanguagesCenter;
using ACTools.SuperString;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using _cau = ACTools.Constants.AtentionCenterConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter.Documents
{
    /// <inheritdoc />
    /// <summary>
    /// Certificado de estudios
    /// </summary>
    public class StudiesCertificate : Document
    {
        #region Properties

        public List<StudentLanguageRecord> StudiedLanguagesList { get; set; }

        private const int DocumentId = _cau.DOC_CIC_STUDIES_CERTIFICATE;
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public StudiesCertificate()
        {
        }

        public StudiesCertificate(Academic.AcademicProfile academicProfile)
        {
            Sync(DocumentId, academicProfile);
            Task<List<StudentLanguageRecord>> langListTask = Task.Run(async () =>
                await StudentLanguageRecord.GetByStudentPidmAsync(academicProfile.person_id));
            StudiedLanguagesList = langListTask.Result;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Creación de solicitud para Certificado de Estudios
        /// </summary>
        /// <param name="student">Estudiante UC</param>
        /// <param name="academicProfile">Perfil académico del estudiante UC</param>
        /// <param name="languageId">Código de Idioma</param>
        /// <param name="studentLanguageRecord">Lista de ciclos aprobados por el estudiante</param>
        /// <param name="description">Descripción de la solicitud</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public async Task<ACRequest> CreateRequestAsync(Academic.Student student,
            Academic.AcademicProfile academicProfile,
            string languageId, IEnumerable<StudentLanguageRecord> studentLanguageRecord, string description,
            string user)
        {
            List<string> classroomList = studentLanguageRecord.FirstOrDefault(w => w.Language.LanguageId == languageId)?
                .LanguageRecord.Select(s => s.Classroom).ToList();

            var json = JsonConvert.SerializeObject(new {description, classrooms = classroomList});

            var cicStudent = new Student(student.person_id, languageId);

            var request = await ComposeRequestAsync(cicStudent, academicProfile);

            request.document_id = DocumentId;
            request.description = json;
            request.cost = cost.amount;

            if (request.state != _cau.STATE_CANCEL) request.state = _cau.STATE_PAYMENT_PENDING;

            var i = await request.CreateAsync(user);

            return i == 1 ? request : null;
        }

        /// <summary>
        /// Genera de documento para el Certificado de Estudios del Centro de Idiomas
        /// </summary>
        /// <param name="request">Datos de la solicitud</param>
        /// <param name="student">Datos de estudiante UC</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public async Task<string> GenerateDocumentAsync(ACRequest request, Academic.Student student, string user)
        {
            var acSignatory = new ACSignatory();

            var cicStudent = new Student(student.person_id, request.program);

            List<ACSignatory> signs = acSignatory.listByACDocument(DocumentId, null);

            var json = JObject.Parse(request.description)["classrooms"];
            List<string> classrooms = json.Select(item => item.ToString()).ToList();

            var data = new StudiesCertificatePDF.Data
            {
                name_file = $"{request.id}-{request.document_number}-{cicStudent.StudentId}",
                user = user,
                signs = acSignatory.listPDFSign(signs),
                student_id = Person.GetDocumentNumber(request.person_id),
                student_names = student.full_name,
                request_id = request.id,
                document_number = request.document_number,
                photo_bytes = new Person().getPhoto(cicStudent.Persona.id),
                LanguageId = request.program,
                LanguageName = cicStudent.Profile.Language.LanguageName,
                Source = _cic.SRC_CIC_PDF,
                Scores = new List<StudiesCertificatePDF.Score>()
            };

            List<LanguageRecord> records = LanguageRecord.GetByClassrooms(classrooms, request.person_id).ToList();
            records = records.OrderBy(o => o.StartDate).ThenBy(o => o.Cycle.CycleNumber).ToList();
            foreach (var record in records)
            {
                var dataRecord = new StudiesCertificatePDF.Score
                {
                    Term = record.Term,
                    Cycle = record.Cycle.CycleNumber.numberToRoman(),
                    Classroom = record.Classroom,
                    P1 = record.N40,
                    P2 = record.N41,
                    P3 = record.N42,
                    P4 = record.N43,
                    Begin = _cic.EXAM_CLASSROOM_LIST.Contains(record.Classroom.Substring(4, 1)) ? record.StartDate : record.Cycle.Begin,
                    End = _cic.EXAM_CLASSROOM_LIST.Contains(record.Classroom.Substring(4, 1)) ? record.StartDate : record.Cycle.End,
                    Modality = _cic.ModalityShortNameList[record.Classroom.Substring(4, 1)],
                    Style = record.Style
                };


                switch (record.Style)
                {
                    case 43:
                        dataRecord.Pp1 = record.N46;
                        dataRecord.Wb = record.N17;
                        dataRecord.Lab = record.N18;
                        dataRecord.Pp2 = record.N47;
                        dataRecord.Ep = record.N19;
                        dataRecord.Ef = record.N20;
                        dataRecord.Pp3 = record.N48;
                        dataRecord.Pp4 = record.N49;
                        dataRecord.Prom = record.N50;
                        break;
                    case 44:
                        dataRecord.Pr = record.N46;
                        dataRecord.Lab = record.N47;
                        dataRecord.Ft = record.N48;
                        dataRecord.Ep = record.N49;
                        dataRecord.Ef = record.N50;
                        dataRecord.Prom = record.N51;
                        break;
                    default:
                        dataRecord.P5 = record.N44;
                        dataRecord.P6 = record.N45;
                        dataRecord.Pp = record.N46;
                        dataRecord.Ef = record.N47;
                        dataRecord.Pr = record.N48;
                        dataRecord.Ee = record.N49;
                        dataRecord.Prom = record.N50;
                        break;
                }

                data.Scores.Add(dataRecord);
            }

            var outputPath = StudiesCertificatePDF.Generate(data);

            // Actualizar solicitud
            if (outputPath == null) return null;
            var message = $"Solicitud generada el {DateTime.Now}, por el usuario: {user}";
            await request.goDeliveryPending(message, user);

            return outputPath;
        }

        #endregion Methods
    }
}