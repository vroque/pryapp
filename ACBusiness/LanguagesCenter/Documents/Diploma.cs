﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACTools.PDF.Documents.LanguagesCenter;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using _cau = ACTools.Constants.AtentionCenterConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter.Documents
{
    /// <inheritdoc />
    /// <summary>
    /// Diploma
    /// </summary>
    public class Diploma : Document
    {
        #region Properties

        public List<LanguageLevels> LanguageLevels { get; set; }

        private const int DocumentId = _cau.DOC_CIC_DIPLOMA;
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public Diploma()
        {
        }

        public Diploma(Academic.AcademicProfile academicProfile)
        {
            Sync(DocumentId, academicProfile);
            SynLanguageLevels(academicProfile);
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Obtiene lista de nivel de idiomas (básico, intermedio...) que el estudiante a alcanzado
        /// </summary>
        /// <param name="academicProfile"></param>
        private void SynLanguageLevels(Academic.AcademicProfile academicProfile)
        {
            Task<List<AcademicProfile>> languageLevelTask =
                Task.Run(async () => await Student.GetLanguageLevelAsync(academicProfile.person_id));
            List<AcademicProfile> languageLevel = languageLevelTask.Result;

            List<StudentLanguageLevel> studentLanguageLevel = languageLevel.Select(s => new StudentLanguageLevel
            {
                Pidm = academicProfile.person_id,
                LanguageId = s.Language.LanguageId,
                LanguageName = s.Language.LanguageName,
                LastCycle = s.Cycle
            }).ToList();

            List<DBOtblNotasEXT> langLevels = new StudentLanguageLevel().GetRegInfoByStudentLevel(studentLanguageLevel);
            LanguageLevels = new LanguageLevels().GetLevelsByClassroom(langLevels);
        }

        /// <summary>
        /// Creación de solicitud para Diploma
        /// </summary>
        /// <param name="student">Estudiante UC</param>
        /// <param name="academicProfile">Perfil académico del estudiante UC</param>
        /// <param name="languageLevelId">Código de Nivel de Idioma</param>
        /// <param name="description">Descripción de la solicitud</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public async Task<ACRequest> CreateRequestAsync(Academic.Student student,
            Academic.AcademicProfile academicProfile,
            string languageLevelId, string description, string user)
        {
            var json = JsonConvert.SerializeObject(new {description, languageLevelId});

            var languageLevel = LanguageLevel.GetInfo(languageLevelId);
            if (languageLevel == null) throw new NullReferenceException("No se encontró nivel de idioma");

            var cicStudent = new Student(student.person_id, languageLevel.LanguageId);

            var request = await ComposeRequestAsync(cicStudent, academicProfile);

            request.document_id = DocumentId;
            request.description = json;
            request.cost = cost.amount;

            if (request.state != _cau.STATE_CANCEL) request.state = _cau.STATE_PAYMENT_PENDING;

            var i = await request.CreateAsync(user);

            return i == 1 ? request : null;
        }

        /// <summary>
        /// Genera el documento para la Diploma del Centro de Idiomas
        /// </summary>
        /// <param name="request">Datos de la solicitud</param>
        /// <param name="student">Datos del estudiante UC</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public async Task<string> GenerateDocumentAsync(ACRequest request, Academic.Student student, string user)
        {
            var acSignatory = new ACSignatory();

            var cicStudent = new Student(student.person_id, request.program);

            List<ACSignatory> signs = acSignatory.listByACDocument(DocumentId, null);

            var languageLevelId = JObject.Parse(request.description)["languageLevelId"].ToString();
            var langLevel = LanguageLevel.GetInfo(languageLevelId);

            if (cicStudent.Profile == null)
            {
                throw new NullReferenceException($"No se encontró nivel de idioma {request.program}");
            }

            var data = new DiplomaPDF.Data
            {
                name_file = $"{request.id}-{request.document_number}-{Academic.Student.getDNI(cicStudent.StudentId)}",
                user = user,
                signs = acSignatory.listPDFSign(signs),
                student_id = Academic.Student.getDNI(cicStudent.StudentId),
                student_names = student.full_name,
                request_id = request.id,
                document_number = request.document_number,
                Cycle = int.Parse(cicStudent.Profile.Cycle),
                LanguageId = request.program,
                LanguageLevelName = langLevel.LangLevelName,
                Source = _cic.SRC_CIC_PDF
            };

            var outputPath = DiplomaPDF.Generate(data);

            // Actualizar solicitud
            if (outputPath == null) return null;
            var message = $"Solicitud generada el {DateTime.Now}, por el usuario: {user}";
            await request.goDeliveryPending(message, user);

            return outputPath;
        }

        #endregion Methods
    }
}