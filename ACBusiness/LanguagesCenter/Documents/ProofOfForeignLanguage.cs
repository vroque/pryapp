﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACTools.PDF.Documents.LanguagesCenter;
using Newtonsoft.Json;
using _cau = ACTools.Constants.AtentionCenterConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.LanguagesCenter.Documents
{
    public class ProofOfForeignLanguage : Document
    {
        #region Properties

        public List<StudentLanguageLevel> LanguageLevels { get; set; }

        private const int ExternalCaieId = _cau.DOC_CIC_CAIE_EXT;
        private readonly DBOContext _dboContext = new DBOContext();
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public ProofOfForeignLanguage()
        {
        }

        public ProofOfForeignLanguage(Academic.AcademicProfile academicProfile)
        {
            Sync(ExternalCaieId, academicProfile);
            SyncLanguageLevels(academicProfile);
        }

        #endregion Constructors

        #region Methods

        private void SyncLanguageLevels(Academic.AcademicProfile academicProfile)
        {
            List<StudentLanguageLevel> languages = _nexoContext.cic_caie.Where(w => w.Pidm == academicProfile.person_id)
                .ToList()
                .Select(s => new StudentLanguageLevel
                {
                    LanguageId = s.LanguageId,
                    LanguageName = s.LanguageName,
                    LastCycle = s.Cycle
                }).ToList();
            LanguageLevels = languages;
        }

        /// <summary>
        /// Creación de solicitud Constancia de Idioma Extranjero Uso Externo
        /// </summary>
        /// <param name="student">Estudiante UC</param>
        /// <param name="academicProfile">Perfil académico del estudiante UC</param>
        /// <param name="languageId">Código de Idioma</param>
        /// <param name="cycle">Último ciclo aprobado en el CIC</param>
        /// <param name="description">Descripción de la solicitud</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public async Task<ACRequest> CreateRequestAsync(Academic.Student student,
            Academic.AcademicProfile academicProfile, string languageId, string cycle, string description, string user)
        {
            var json = JsonConvert.SerializeObject(new {description, cycle});

            var cicStudent = new Student(student.person_id, languageId);

            var request = await ComposeRequestAsync(cicStudent, academicProfile);

            request.document_id = ExternalCaieId;
            request.description = json;
            request.cost = cost.amount;

            if (request.state != _cau.STATE_CANCEL) request.state = _cau.STATE_PAYMENT_PENDING;

            var i = await request.CreateAsync(user);

            return i == 1 ? request : null;
        }

        /// <summary>
        /// Genera el documento para la Constancia de Idioma Extranjero de Uso Externo
        /// </summary>
        /// <param name="request"></param>
        /// <param name="student"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public async Task<string> GenerateDocumentAsync(ACRequest request, Academic.Student student, string user)
        {
            var acSignatory = new ACSignatory();

            var cicStudent = new Student(student.person_id, request.program);

            List<ACSignatory> signers = acSignatory.listByACDocument(ExternalCaieId, null);

            if (!signers.Any()) throw new NullReferenceException("No se encontraron firmantes para este documento");

            var cicCertificate =
                new ACBusiness.LanguagesCenter.ProofOfForeignLanguage.Data(student.person_id, cicStudent.Profile);

            var data = new ProofOfForeignLanguagePDF.Data
            {
                name_file = $"{request.id}-{request.document_number}-{Academic.Student.getDNI(student.id)}",
                user = user,
                signs = acSignatory.listPDFSign(signers),
                student_id = Academic.Student.getDNI(student.id),
                student_names = student.full_name,
                request_id = request.id,
                document_number = request.document_number,
                GenderData = Academic.Student.getGenderData(cicStudent.StudentId),
                LanguageLevelName =
                    LanguageLevel.GetInfo(Student.GetLanguageLevelId(cicStudent.StudentId,
                        cicStudent.Profile.Language.LanguageId, int.Parse(cicStudent.Profile.Cycle))).LanguageLevelName,
                LanguageName = cicStudent.Profile.Language.LanguageName,
                Avg = cicCertificate.GetAvg(),
                TotalHours = cicCertificate.GetTotalHours(),
                StartDate = cicCertificate.StartDate,
                EndDate = cicCertificate.EndDate,
                DocumentType = request.document_id,
                Source = _cic.SRC_CAIE_PDF
            };

            var outputPath = ProofOfForeignLanguagePDF.Generate(data);

            // Actualizar solicitud
            if (outputPath == null) return null;
            var message = $"Solicitud generada el {DateTime.Now}, por el usuario: {user}";
            await request.goDeliveryPending(message, user);

            return outputPath;
        }

        #endregion Methods
    }
}