﻿using System;
using ACPermanence.Contexts.NEXO;
using System.Collections.Generic;
using System.Linq;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.CIC;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter
{
    public class LanguageLevel
    {
        #region Properties

        public string LanguageLevelId { get; set; }
        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string LanguageLevelName { get; set; }
        public string LangName { get; set; }
        public string LangLevelName { get; set; }
        public int LanguageProgramId { get; set; }
        public string LanguageProgramName { get; set; }
        public int StartCycle { get; set; }
        public int FinalCycle { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Methods
        
        /// <summary>
        /// Info sobre un nivel de idioma
        /// </summary>
        /// <param name="languageLevelId"></param>
        /// <returns></returns>
        public static LanguageLevel GetInfo(string languageLevelId)
        {
            var nexoContext = new NEXOContext();
            return nexoContext.cic_language_level.FirstOrDefault(w => w.LanguageLevelId == languageLevelId);;
        }

        /// <summary>
        /// Listado de todos los Niveles de Idiomas del CIC.
        /// </summary>
        /// <returns>Nivel de Idiomas CIC</returns>
        public static List<LanguageLevel> GetAll()
        {
            var nexoContext = new NEXOContext();
            var levels = nexoContext.cic_language_level.Where(w => w.Active)
                .Select(s => new LanguageLevel
                {
                    LanguageLevelId = s.LanguageLevelId,
                    LanguageId = s.LanguageId,
                    LanguageName = s.LanguageName,
                    LanguageLevelName = s.LanguageLevelName,
                    LanguageProgramId = s.LanguageProgramId,
                    StartCycle = s.StartCycle,
                    FinalCycle = s.FinalCycle
                })
                .ToList();
            return levels;
        }

        /// <summary>
        /// Nivel de idioma de una sección
        /// </summary>
        /// <param name="classroom">Código de sección</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public LanguageLevel GetLevelByClassroom(string classroom)
        {
            if (classroom.Length != 10) throw new ArgumentException("Longitud de sección inválida");

            var lang = classroom.Substring(2, 2);
            var type = classroom.Substring(4, 1);
            var cycle = int.Parse(classroom.Substring(5, 2));
            string langId;
            int langProgram;

            switch (lang)
            {
                case "IN":
                    langId = _cic.INGLES;
                    break;
                case "PO":
                    langId = _cic.PORTUGUES;
                    break;
                case "IT":
                    langId = _cic.ITALIANO;
                    break;
                default:
                    langId = "000";
                    break;
            }

            switch (type)
            {
                case "C":
                case "F":
                case "I":
                case "R":
                case "S":
                case "U":
                case "X":
                    langProgram = _cic.PROG_REGULAR;
                    break;
                case "K":
                case "P":
                    langProgram = _cic.PROG_SCHOOLCHILD;
                    break;
                case "L":
                    langProgram = _cic.PROG_VIRTUAL;
                    break;
                default:
                    langProgram = _cic.PROG_UNKNOWN;
                    break;
            }

            var langLevel =
                _nexoContext.cic_language_level.Where(w => w.LanguageId == langId && w.Active &&
                                                           w.LanguageProgramId == langProgram &&
                                                           w.StartCycle <= cycle && w.FinalCycle >= cycle).ToList();
            return langLevel.Select<CICLanguageLevel, LanguageLevel>(s => s).FirstOrDefault();
        }

        /// <summary>
        /// Lista últimos ciclos de Niveles de Idiomas (básico, intermedio...) completados por el estudiante
        /// </summary>
        /// <param name="tblNotasExt"></param>
        /// <returns></returns>
        public List<int> CompletedLangLevelFinalCycleList(DBOtblNotasEXT tblNotasExt)
        {
            var level = new LanguageLevel().GetLevelByClassroom(tblNotasExt.IDSeccionC);
            var cycle = int.Parse(Cycle.GetCycleNumber(tblNotasExt));

            var finalCycleList = _nexoContext.cic_language_level
                .Where(w => w.LanguageId == level.LanguageId && w.Active &&
                            w.LanguageProgramId == level.LanguageProgramId &&
                            w.FinalCycle <= cycle)
                .OrderBy(o => o.StartCycle).Select(s => s.FinalCycle).ToList();

            return finalCycleList.Where(w => w <= cycle).ToList();
        }

        /// <summary>
        /// Niveles de Idiomas según Idioma y Programa CIC
        /// </summary>
        /// <param name="languageId">Código de Idioma CIC</param>
        /// <param name="languageProgramId">Código de Programa CIC</param>
        /// <returns></returns>
        public static IEnumerable<LanguageLevel> GetByLanguageIdAndLanguageProgram(string languageId,
            int languageProgramId)
        {
            var nexoContext = new NEXOContext();
            var levels = nexoContext.cic_language_level
                .Where(w => w.Active && w.LanguageId == languageId &&
                            w.LanguageProgramId == languageProgramId)
                .Select(s => new LanguageLevel
                {
                    LanguageLevelId = s.LanguageLevelId,
                    LanguageId = s.LanguageId,
                    LanguageName = s.LanguageName,
                    LanguageLevelName = s.LanguageLevelName,
                    LanguageProgramId = s.LanguageProgramId,
                    StartCycle = s.StartCycle,
                    FinalCycle = s.FinalCycle
                }).OrderBy(o => o.StartCycle).ToList();
            return levels;
        }

        #endregion Methods

        #region Implicit Operators

        public static implicit operator LanguageLevel(CICLanguageLevel cicLanguageLevel)
        {
            if (cicLanguageLevel == null) return null;

            var languageLevel = new LanguageLevel
            {
                LanguageLevelId = cicLanguageLevel.LanguageLevelId,
                LanguageId = cicLanguageLevel.LanguageId,
                LanguageName = cicLanguageLevel.LanguageName,
                LanguageLevelName = cicLanguageLevel.LanguageLevelName,
                LangName = cicLanguageLevel.LangName,
                LangLevelName = cicLanguageLevel.LangLevelName,
                LanguageProgramId = cicLanguageLevel.LanguageProgramId,
                StartCycle = cicLanguageLevel.StartCycle,
                FinalCycle = cicLanguageLevel.FinalCycle
            };
            languageLevel.LanguageProgramName = _cic.LANGUAGE_PROGRAM[languageLevel.LanguageProgramId];
            return languageLevel;
        }

        #endregion Implicit Operators
    }
}