﻿using ACPermanence.Contexts.BDUCCI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _acad = ACTools.Constants.AcademicConstants;
using System.Data.Entity;

namespace ACBusiness.LanguagesCenter
{
    public class CICStatus
    {
        public string language { get; set; }
        public string modality { get; set; }
        public string modality_name { get; set; }
        public int cycle { get; set; }
        public int last_cycle { get; set; }
        public bool need_recategorize { get; set; }
        public string enrollment_status { get; set; }
        private DBOContext _apec = new DBOContext();

        public async Task<CICStatus> getCICStatus(decimal pidm)
        {
            int person_id = (int)pidm;
            var state = await _apec.tbl_notas_ext.Join(
                    _apec.tbl_seccion_docente_ext,
                    nota => new { s = nota.IDSeccionC, e = nota.IDEscuela, d = nota.IDDependencia },
                    xt => new { s = xt.IDSeccionC, e = xt.IDEscuela, d = xt.IDDependencia },
                    (nota, xt) => new { nota, xt }
                ).Join(
                    _apec.view_alumno_basico,
                    nx => nx.nota.IDAlumno,
                    al => al.IDAlumno,
                    (nx, al) => new { nx.nota, nx.xt, al }
                ).Join(
                    _apec.tbl_seccion_acad,
                    dd => new { s = dd.xt.IDSeccionC, e = dd.xt.IDEscuela, d = dd.xt.IDDependencia, c = dd.xt.IDsede },
                    se => new { s = se.IDSeccion, e = se.IDEscuela, d = se.IDDependencia, c = se.IDSede },
                    (dd, se) => new { dd.nota, dd.xt, dd.al, se }
                )
                .Where(f =>
                    f.al.IDPersonaN == person_id
                    && _cic.ACTIVE_LANGUAGES_LIST.Contains(f.nota.IDEscuela)
                    && f.nota.IDDependencia == _acad.DEFAULT_DIV
                    && f.nota.n50 != null
                ).Select(f => new
                {
                    section = f.nota.IDSeccionC,
                    style = f.xt.IDEstilo,
                    score_min = f.xt.notaaprob ?? 1000,
                    score50 = f.nota.n50 ?? 0,
                    score51 = f.nota.n51 ?? 0,
                    date_start = f.se.FechaIni,
                    date_end = f.se.FechaFin,
                    cycle = f.se.Ciclo,
                    modality = f.xt.modalidad,
                    college = f.nota.IDEscuela
                })
                .OrderByDescending(f => f.date_start)
                .ThenByDescending(f => f.cycle)
                .FirstOrDefaultAsync();
            if (state == null) return null;
            string modality_name;
            CICSection sec = new CICSection(state.section);
            _cic.MODALITIES_NAME.TryGetValue(sec.modality, out modality_name);
            CICStatus st = new CICStatus {
                cycle = sec.cycle,
                last_cycle = sec.cycle,
                language = sec.language,
                modality = sec.modality,
                modality_name = modality_name
            };
            //validamos que este aprobado, si esta aprobado de corresponde el sgte
            if (state.style == 44)
            {
                if (state.score_min <= state.score51) st.cycle += 1;
            } else
            {
                if (state.score_min <= state.score50) st.cycle += 1;
            }
            // validamos si paso 3 meses desde su ultimo fin de matricula
            DateTime max = state.date_end ?? DateTime.Today;
            DateTime cut = max.AddMonths(3).Date; // only date (whitour time)
            if (cut < DateTime.Today)
            {
                st.need_recategorize = true;
            }
            else
            {
                st.need_recategorize = false;
            }
            st.enrollment_status = _apec.sp_comprobar_matricula_cic(pidm,state.college);
            return st;
        }
    }
}
