﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.DBUCCI.DBO.views;
using _acad = ACTools.Constants.AcademicConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter
{
    public class Student
    {
        #region Properties

        public Person Persona { get; set; }
        public string StudentId { get; set; }
        public AcademicProfile Profile { get; set; }
        public List<AcademicProfile> Profiles { get; set; } = new List<AcademicProfile>();

        private readonly DBOContext _dboContext = new DBOContext();

        #endregion Properties

        #region Constructors

        public Student()
        {
        }

        public Student(decimal pidm)
        {
            SyncCicStudent(pidm);
        }

        public Student(decimal pidm, string languageId)
        {
            SyncCicStudent(pidm, languageId);
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Obtiene el PIDM de un estudiante del CIC
        /// Si no se encuentra PIDM, entonces retorna 0
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public static decimal GetStudentPidm(string studentId)
        {
            var dboContext = new DBOContext();
            return dboContext.view_alumno_basico.FirstOrDefault(w => w.IDAlumno == studentId)?.IDPersonaN ?? 0;
        }

        /// <summary>
        /// Lista con información de todos los códigos asociados a un estudiante del Centro de Idiomas
        /// </summary>
        /// <param name="pidm">PIDM del estudiante del CIC</param>
        /// <returns></returns>
        public static List<DBODATAlumnoBasico> GetViewAlumnoBasico(decimal pidm)
        {
            var dboContext = new DBOContext();
            return dboContext.view_alumno_basico.Where(w => w.IDPersonaN == pidm).ToList();
        }

        /// <summary>
        /// Datos básicos de estudiante CIC
        /// </summary>
        /// <param name="pidm"></param>
        private void SyncCicStudent(decimal pidm)
        {
            Persona = new Person(pidm);
            Profiles = AcademicProfile.GetProfileList(pidm);
        }

        /// <summary>
        /// Datos básicos de estudiante CIC
        /// </summary>
        /// <param name="pidm"></param>
        /// <param name="languageId"></param>
        private void SyncCicStudent(decimal pidm, string languageId)
        {
            Persona = new Person(pidm);
            var tempProfile = new AcademicProfile(pidm, languageId);
            Profile = tempProfile.Cycle == null ? null : tempProfile;
            StudentId = Profile?.StudentId;
        }

        /// <summary>
        /// Aula de clases (sección) de un estudiante CIC de acuerdo al idioma y nivel de idioma que estudia
        /// </summary>
        /// <param name="studentId">Código de estudiante</param>
        /// <param name="languageId">Código de Idioma CIC</param>
        /// <param name="languageCycle">Ciclo en curso</param>
        /// <returns></returns>
        public static string GetClassroom(string studentId, string languageId, int languageCycle)
        {
            var dboContext = new DBOContext();
            var cycle = languageCycle.ToString().PadLeft(2, '0');
            DateTime? lastEnrollmentDateCycle = dboContext.tbl_notas_ext
                .Where(w => w.IDAlumno == studentId &&
                            w.IDEscuela == languageId &&
                            (w.IDSeccionC.Substring(5, 2) == cycle ||
                             w.ciclo == cycle))
                .Max(m => m.fechamat);

            var classrooms = dboContext.tbl_notas_ext
                .Where(w => w.IDAlumno == studentId &&
                            w.IDEscuela == languageId &&
                            w.fechamat == lastEnrollmentDateCycle).OrderByDescending(o => o.FecInic).FirstOrDefault();

            return classrooms?.IDSeccionC ?? "0000000000";
        }

        /// <summary>
        /// Nivel de Idioma de un Estudiante CIC de acuerdo al idioma y ciclo
        /// </summary>
        /// <param name="studentId">Código de Estudiante</param>
        /// <param name="languageId">Código de Idioma CIC</param>
        /// <param name="languageCycle">Ciclo CIC</param>
        /// <returns></returns>
        public static string GetLanguageLevelId(string studentId, string languageId, int languageCycle)
        {
            var nexoContext = new NEXOContext();
            var classroomType = GetClassroom(studentId, languageId, languageCycle).Substring(5, 1);
            string languageLevel = null;
            switch (classroomType)
            {
                case "L":
                    var cicVirtualLevel = nexoContext.cic_language_level
                        .FirstOrDefault(w => w.LanguageId == languageId &&
                                             w.LanguageProgramId == _cic.PROG_VIRTUAL &&
                                             (languageCycle >= w.StartCycle && languageCycle <= w.FinalCycle));

                    if (cicVirtualLevel != null) languageLevel = cicVirtualLevel.LanguageLevelId;

                    break;
                case "P":
                case "K":
                    var cicEscolarLevel = nexoContext.cic_language_level
                        .FirstOrDefault(w => w.LanguageId == languageId &&
                                             w.LanguageProgramId == _cic.PROG_SCHOOLCHILD &&
                                             languageCycle >= w.StartCycle &&
                                             languageCycle <= w.FinalCycle);

                    if (cicEscolarLevel != null) languageLevel = cicEscolarLevel.LanguageLevelId;

                    break;
                default:
                    var cicRegularLevel = nexoContext.cic_language_level
                        .FirstOrDefault(w => w.LanguageId == languageId &&
                                             w.LanguageProgramId == _cic.PROG_REGULAR &&
                                             languageCycle >= w.StartCycle &&
                                             languageCycle <= w.FinalCycle);

                    if (cicRegularLevel != null) languageLevel = cicRegularLevel.LanguageLevelId;

                    break;
            }

            return languageLevel;
        }

        /// <summary>
        /// Ordena una lista de estudiantes CIC en orden alfabético de acuerdo al campo "full_name" (Apellidos, Nombres)
        /// </summary>
        /// <param name="students">Lista de Estudiantes CIC</param>
        /// <returns>Lista ordenada de estudiantes CIC</returns>
        public static List<Student> OrderByFullName(IEnumerable<Student> students)
        {
            var personNotExist = new List<Student>();
            var studentNotFound = new List<Student>();
            var studentFound = new List<Student>();

            foreach (var student in students)
            {
                if (student.Persona == null)
                {
                    personNotExist.Add(student);
                }
                else
                {
                    if (student.Profiles.Any())
                    {
                        studentFound.Add(student);
                    }
                    else
                    {
                        studentNotFound.Add(student);
                    }
                }
            }

            personNotExist = personNotExist.OrderBy(o => o.StudentId).ToList();
            studentNotFound = studentNotFound.OrderBy(o => o.Persona.full_name).ToList();
            studentFound = studentFound.OrderBy(o => o.Persona.full_name).ToList();

            return personNotExist.Union(studentNotFound).Union(studentFound).ToList();
        }

        /// <summary>
        /// Obtiene lista de ciclos de matriculas activas
        /// </summary>
        /// <param name="studentPidm"></param>
        /// <returns></returns>
        public IEnumerable<string> GetAllActiveEnrolledCycles(decimal studentPidm)
        {
            List<string> studentIdList = _dboContext.view_alumno_basico.Where(w => w.IDPersonaN == studentPidm)
                .Select(s => s.IDAlumno).ToList();
            var oneWeekAgo = DateTime.Now.AddDays(-7);
            List<string> alumnoEstado = _dboContext.tbl_alumno_estado
                .Where(w => studentIdList.Contains(w.IDAlumno) &&
                            _cic.ACTIVE_LANGUAGES_LIST.Contains(w.IDEscuela) &&
                            w.FecMatricula >= oneWeekAgo)
                .Select(s => s.IDSeccionC).ToList();
            return alumnoEstado;
        }

        /// <summary>
        /// Fecha de inicio o reinicio de estudios en el Centro de Idiomas de un idioma
        /// </summary>
        /// <param name="studentPidm">PIDm del estudiante</param>
        /// <param name="languageId">Código de idioma</param>
        /// <returns></returns>
        public static DateTime GetStartDateAtCicByLanguageId(decimal studentPidm, string languageId)
        {
            var startDateInfo = GetStartStudiesByLanguageIdAsync(studentPidm, languageId).Result;

            return startDateInfo.StartDate;
        }

        /// <summary>
        /// Último registro de inicio de estudios en el Centro de Idiomas
        /// </summary>
        /// <param name="studentPidm">PIDM del estudiante</param>
        /// <param name="languageId">Código de idioma</param>
        /// <returns></returns>
        public static async Task<LanguageRecord> GetStartStudiesByLanguageIdAsync(decimal studentPidm,
            string languageId)
        {
            List<DBOtblNotasEXT> languageRecord =
                StudentLanguageRecord.GetApprovedCyclesByLanguageId(studentPidm, languageId);
            languageRecord = languageRecord
                .Where(w => w.IDSeccionC.Substring(4, 1) == _cic.MOD_EXAMEN_CLASIFICACION ||
                            w.IDSeccionC.Substring(4, 1) == _cic.MOD_EXAMEN_SUFICIENCIA ||
                            w.IDSeccionC.Substring(4, 1) == _cic.MOD_CONVALIDACION ||
                            w.IDSeccionC.Substring(5, 2) == "01")
                .ToList();
            var lastStartCycleByLanguage = languageRecord.OrderByDescending(o => o.FecInic).FirstOrDefault();

            return lastStartCycleByLanguage;
        }

        /// <summary>
        /// Niveles de idioma que tiene un estudiante
        /// </summary>
        /// <param name="studentPidm">PIDM del estudiante</param>
        /// <returns></returns>
        public static async Task<List<AcademicProfile>> GetLanguageLevelAsync(decimal studentPidm)
        {
            List<Language> languagesByStudent = await Language.GetByStudentAsync(studentPidm);
            var languageLevelList = new List<AcademicProfile>();
            foreach (var language in languagesByStudent)
            {
                var profileByLanguage = new AcademicProfile(studentPidm, language.LanguageId);
                if (profileByLanguage.Language != null)
                {
                    languageLevelList.Add(profileByLanguage);
                }
            }

            return languageLevelList;
        }

        /// <summary>
        /// Nivel de idioma de un estudiante por idioma cursado
        /// </summary>
        /// <param name="studentPidm">PIDM del estudiante</param>
        /// <param name="languageId">Código del idioma</param>
        /// <returns></returns>
        public static async Task<LanguageRecord> GetLanguageLevelByLanguageAsync(decimal studentPidm, string languageId)
        {
            List<DBOtblNotasEXT> recordByLanguage =
                await StudentLanguageRecord
                    .GetApprovedCyclesByLanguageIdValidForLangLevelAsync(studentPidm, languageId);

            if (!recordByLanguage.Any()) return null;

            var lastCycleCompleted = recordByLanguage.OrderByDescending(o => o.FecInic).FirstOrDefault();

            return lastCycleCompleted;
        }

        /// <summary>
        /// Indica si un estudiante califica o no para poder matricularse en la asignatura de inglés profesional de la UC
        /// </summary>
        /// <param name="studentPidm">PIDM del estudiante</param>
        /// <returns></returns>
        public static async Task<bool> QualifyForEnrollInProfessionalEnglish(decimal studentPidm)
        {
            List<DBOtblNotasEXT> englishRecord =
                await StudentLanguageRecord.GetApprovedCyclesValidForProfessionalEnglishAsync(studentPidm);

            if (!englishRecord.Any()) return false;

            LanguageRecord lastCycleCompleted = englishRecord.OrderByDescending(o => o.FecInic).FirstOrDefault();

            return lastCycleCompleted.Cycle.CycleNumber >= _cic.MinimumCycleToEnrollInProfessionalEnglish;
        }

        /// <summary>
        /// Verifica si un estudiante, de acuerdo a la carerra activa que tiene en UC, tiene, o no, segundo idioma
        /// para su constancia de idioma extranjero
        /// </summary>
        /// <param name="studentPidm"></param>
        /// <returns></returns>
        public static async Task<bool> HasSecondLanguageForProofOfForeignLanguageAsync(decimal studentPidm)
        {
            var mov = Academic.Student.GetLastMovimientoEstudiante(studentPidm);
            var hasSecondLanguage =
                await HasSecondLanguageForProofOfForeignLanguageByProgramIdAsync(studentPidm, mov.CODIGO_PROGRAMA);
            return hasSecondLanguage;
        }

        /// <summary>
        /// Verifica si un estudiante, de acuerdo a la modalidad de su carrera UC, tiene, o no, segundo idioma
        /// para su constancia de idioma extranjero
        /// </summary>
        /// <param name="studentPidm"></param>
        /// <param name="programId"></param>
        /// <returns></returns>
        public static async Task<bool> HasSecondLanguageForProofOfForeignLanguageByProgramIdAsync(decimal studentPidm,
            string programId)
        {
            var lastCycleCompletedByLanguageList = new List<LanguageRecord>();
            List<Language> languageList = await Language.GetByStudentAsync(studentPidm, true);
            var mov = Academic.Student.GetLastMovimientoEstudianteBySchoolId(studentPidm, programId);
            foreach (var language in languageList)
            {
                var lastStartCycleAtLanguagesCenter =
                    await GetStartStudiesByLanguageIdAsync(studentPidm, language.LanguageId);
                List<DBOtblNotasEXT> languageRecord =
                    await StudentLanguageRecord.GetApprovedCyclesByLanguageIdValidForCaieAsync(studentPidm,
                        language.LanguageId);
                List<LanguageRecord> langRecordList = languageRecord
                    .Select<DBOtblNotasEXT, LanguageRecord>(s => s)
                    .Where(w => w.StartDate >= lastStartCycleAtLanguagesCenter.StartDate)
                    .ToList();

                langRecordList = langRecordList
                    .Where(w => // REGULAR
                                mov.CODIGO_DEPARTAMENTO == _acad.DEPA_REG &&
                                // REGULAR INGLÉS
                                (w.LanguageId == _cic.INGLES &&
                                 lastStartCycleAtLanguagesCenter.StartDate.Year >= 2015 && w.Cycle.CycleNumber >= 14 ||
                                 (lastStartCycleAtLanguagesCenter.StartDate.Year <= 2014 && lastStartCycleAtLanguagesCenter.StartDate.Year >= 2013) && w.Cycle.CycleNumber >= 13 ||
                                 (lastStartCycleAtLanguagesCenter.StartDate.Year <= 2012 && lastStartCycleAtLanguagesCenter.StartDate.Year >= 2007) && w.Cycle.CycleNumber >= 12 ||
                                 /*
                                  * la regla está para los períodos 2006-2 y 2006-1 pero solo existe registro de 2006-0
                                  * (lastStartCycleAtLanguagesCenter.Term == "2006-2" && w.Cycle.CycleNumber >= 11) ||
                                  * (lastStartCycleAtLanguagesCenter.Term == "2006-1" && w.Cycle.CycleNumber >= 10) ||
                                  */
                                 lastStartCycleAtLanguagesCenter.StartDate.Year == 2006 && w.Cycle.CycleNumber >= 10 ||
                                 /*
                                  * la regla está para los períodos 2005-2 y 2005-1 pero también se encontraron registros de 2005-0
                                  * se procede a aplicar la regla de 2005-1 para el año 2005 completo
                                  * (lastStartCycleAtLanguagesCenter.Term == "2005-2" && w.Cycle.CycleNumber >= 9) ||
                                  * (lastStartCycleAtLanguagesCenter.Term == "2005-1" && w.Cycle.CycleNumber >= 8) ||
                                  */
                                 lastStartCycleAtLanguagesCenter.StartDate.Year == 2005 && w.Cycle.CycleNumber >= 8 ||
                                 /*
                                  * la regla está para los período 2004-2 y 2004-1 pero también se encontraron registros de 2004-0
                                  * se procede a aplicar la regla para el año 2004 completo
                                  * ((lastStartCycleAtLanguagesCenter.Term == "2004-1" || lastStartCycleAtLanguagesCenter.Term == "2004-2") && w.Cycle.CycleNumber >= 4)
                                  */
                                 lastStartCycleAtLanguagesCenter.StartDate.Year == 2004 && w.Cycle.CycleNumber >= 4
                                ) ||
                                // REGULAR PORTUGUES E ITALIANO
                                (w.LanguageId == _cic.PORTUGUES || w.LanguageId == _cic.ITALIANO) &&
                                (lastStartCycleAtLanguagesCenter.StartDate.Year >= 2008 && w.Cycle.CycleNumber >= 10 ||
                                 lastStartCycleAtLanguagesCenter.StartDate.Year == 2007 && w.Cycle.CycleNumber >= 8) ||
                                // VIRTUAL Y GENTE QUE TRABAJA
                                (mov.CODIGO_DEPARTAMENTO == _acad.DEPA_VIR || mov.CODIGO_DEPARTAMENTO == _acad.DEPA_PGT) &&
                                (w.LanguageId == _cic.INGLES &&
                                 (lastStartCycleAtLanguagesCenter.StartDate.Year >= 2017 &&
                                  // UVIR O UPGT con plan de estudios <= 2007 solo deben cursar 7 ciclos de inglés, el resto debe cursar 14 ciclos
                                  ((int.Parse(mov.CATALOGO_REAL) >= 201500 && w.Cycle.CycleNumber >= 14) || 
                                   (int.Parse(mov.CATALOGO_REAL) < 201500 && w.Cycle.CycleNumber >= 7)) ||
                                  lastStartCycleAtLanguagesCenter.StartDate.Year == 2016 && w.Cycle.CycleNumber >= 7 ||
                                  lastStartCycleAtLanguagesCenter.StartDate.Year <= 2015 && w.Cycle.CycleNumber >= 6) ||
                                 // VIRTUAL Y GENTE QUE TRABAJA PORTUGUES O ITALIANO
                                 (w.LanguageId == _cic.PORTUGUES || w.LanguageId == _cic.ITALIANO) && w.Cycle.CycleNumber >= 5) ||
                                // Adicionales
                                (w.Classroom == "19FRU0101M" || w.Classroom == "19QEU0101M" || w.Classroom == "19ALU0101M")
                        )
                    .ToList();
                var lang = langRecordList.OrderByDescending(o => o.StartDate).FirstOrDefault();
                if (lang != null) lastCycleCompletedByLanguageList.Add(lang);
            }

            return lastCycleCompletedByLanguageList.Any();
        }

        #endregion Methods
    }
}