﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using _admission = ACTools.Constants.AdmissionConstant;
using _cau = ACTools.Constants.AtentionCenterConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _corp = ACTools.Constants.InstitucionalConstants;
using _util = ACTools.Util;

namespace ACBusiness.LanguagesCenter
{
    public class StudentIC : Student
    {
        #region Properties

        /// <summary>
        /// <para>En la tabla se almacenan dos valores:</para>
        ///  * 1 : El estudiante realizó el pago por el trámite
        ///  * 0 : El estudiante aún no ha realizado el pago por el trámite
        /// <para>Adicionalmente se usa los siguientes valores para mostrar en el resultado de búsqueda por lotes:</para>
        ///  * 2 : Estudiantes que no cumplen con la regla para obtener CAIE
        ///  * 3 : Estudiantes no encontrados en la DB
        /// </summary>
        public int PaymentStatus { get; set; }

        /// <summary>
        /// Constancia de Idioma Extranjero Instituto
        /// </summary>
        private const int DocumentCaieIdInstitute = _cau.DOC_CIC_CAIE_INST;

        #endregion Properties

        #region Methods

        /// <summary>
        /// Estudiantes de Instituto Continental con Constancia de Idioma Extranjero
        /// </summary>
        /// <param name="students">Arreglo de códigos de estudiantes</param>
        /// <returns></returns>
        public static List<StudentIC> GetStudentsWithCaie(string[] students)
        {
            var dboContext = new DBOContext();
            var nexoContext = new NEXOContext();

            var studentsWithCaie = new List<StudentIC>();
            var studentsWithoutCaie = new List<StudentIC>();
            var studentNotFound = new List<StudentIC>();

            foreach (var student in students)
            {
                var basicInfo = dboContext.view_alumno_basico.FirstOrDefault(w => w.IDAlumno == student);

                // Códigos NO encontrados
                if (basicInfo == null)
                {
                    var notFound = new StudentIC
                    {
                        Persona = new Person(),
                        StudentId = student,
                        PaymentStatus = _cic.STUDENT_NOT_FOUND
                    };
                    studentNotFound.Add(notFound);
                    continue;
                }

                List<string> studentIdList = Academic.Student.GetAllStudentId(student);

                List<DBOTblPostulante> instituteStudent = dboContext.tbl_postulante.Where(w =>
                    studentIdList.Contains(w.IDAlumno) && w.IDDependencia == _corp.IC).ToList();

                // No son estudiantes de Instituto
                if (!instituteStudent.Any())
                {
                    var notInstituteStudent = new StudentIC
                    {
                        Persona = new Person(basicInfo.IDPersonaN),
                        StudentId = student,
                        PaymentStatus = _cic.STUDENT_NOT_FOUND
                    };
                    studentsWithoutCaie.Add(notInstituteStudent);
                    continue;
                }

                var init = new List<string> {"01", "02", "03", "04", "05", "06"};

                var langLevel = nexoContext.cic_student_level
                    .FirstOrDefault(w => w.Pidm == basicInfo.IDPersonaN &&
                                         w.LanguageId == _cic.INGLES &&
                                         !init.Contains(w.LastCycle));

                // Estudiantes de Instituto pero NO cumplen reglas CIC
                if (langLevel == null)
                {
                    var withoutCaie = new StudentIC
                    {
                        Persona = new Person(basicInfo.IDPersonaN),
                        StudentId = student,
                        PaymentStatus = _cic.FAILS_CAIE_RULES
                    };
                    studentsWithoutCaie.Add(withoutCaie);
                    continue;
                }

                // Estudiantes de Instituto con CAIE
                var studentInfo = new Student(basicInfo.IDPersonaN, langLevel.LanguageId);
                var withCaie = new StudentIC
                {
                    Persona = new Person(basicInfo.IDPersonaN),
                    StudentId = student,
                    Profile = studentInfo.Profile,
                    PaymentStatus = _cic.NOT_PAID
                };
                studentsWithCaie.Add(withCaie);
            }

            return studentNotFound.Union(studentsWithoutCaie).Union(studentsWithCaie)
                .OrderBy(o => o.Persona.full_name).ToList();
        }

        /// <summary>
        /// Información sobre la última postulación al instituto
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public static DBOTblPostulante GetLastEntrantInfo(decimal pidm)
        {
            var dboContext = new DBOContext();
            List<string> studentIdList = Academic.Student.GetAllStudentId(pidm).ToList();
            var lastEntrantInfo = dboContext.tbl_postulante
                .Where(w => w.IDDependencia == _corp.IC &&
                            studentIdList.Contains(w.IDAlumno) &&
                            w.Ingresante == _admission.INGRESANTE &&
                            w.Renuncia == _admission.NO_RENUNCIA_CARRERA)
                .OrderByDescending(o => o.IDExamen).FirstOrDefault();

            return lastEntrantInfo;
        }

        /// <summary>
        /// Genera CAIE para el Instituto a partir de un array de objeto de estudiantes
        /// </summary>
        /// <param name="students">Array de objeto de estudiantes</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public static async Task<List<ProofOfForeignLanguage>> GenerateProofOfForeignLanguageAsync(
            ProofOfForeignLanguage[] students, string user)
        {
            return await ProofOfForeignLanguage.GenerateProofOfForeignLanguageAsync(students, user,
                DocumentCaieIdInstitute);
        }

        #endregion Methods
    }
}