﻿using System;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Accounts;
using ACBusiness.AtentionCenter;
using Newtonsoft.Json;
using _cau = ACTools.Constants.AtentionCenterConstants;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.LanguagesCenter
{
    /// <inheritdoc />
    /// <summary>
    /// Documento generado mediante solicitudes del Centro de Idiomas
    /// </summary>
    public class Document : ACDocument
    {
        #region Properties

        #endregion Properties

        #region Methods

        /// <summary>
        /// Carga datos generales del documento
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="academicProfile"></param>
        protected void Sync(int documentId, Academic.AcademicProfile academicProfile)
        {
            sync(documentId, academicProfile);
            cost = GetDocumentCost(documentId);
        }

        /// <summary>
        /// Calcula el monto de la solicitud para un determinado tipo de documento
        /// </summary>
        /// <param name="documentId">Tipo de documento</param>
        /// <returns></returns>
        private DocumentCost GetDocumentCost(int documentId)
        {
            return new DocumentCost().GetCicDocumentCost(documentId);
        }

        /// <summary>
        /// Crea solicitud para el documento
        /// </summary>
        /// <param name="student"></param>
        /// <param name="academicProfile"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        protected async Task<ACRequest> ComposeRequestAsync(Student student, Academic.AcademicProfile academicProfile)
        {
            if (student == null) throw new NullReferenceException("Se debe de indicar información del estudiante");

            var request = new ACRequest
            {
                divs = _corp.CIC,
                /*
                 * perfil academico viene null cuando es una constancia de idioma extranjero de tipo externo
                 * personalizado desde backoffice
                 */
                campus = academicProfile?.campus != null ? academicProfile.campus.id : string.Empty,
                program = student.Profile.Language.LanguageId,
                person_id = student.Persona.id,
                student_id = student.StudentId,
                document_number = 1,
                relationship_id = 0, // NOTE: NPI
                student_name = student.Persona.full_name,
                delivery_date = DateTime.Now.AddDays(3), // NOTE: Ver ACDocuments
                office_user_id = 0,
                office_id = _corp.OFFICE_CAU,
                term = $"{DateTime.Today.Year}00",
                aproved = true,
                update_date = DateTime.Now,
                creation_date = DateTime.Now,
                department = _corp.CIC
            };

            /**
             * NOTE: El proceso de generación de CAIE UC de uso interno y CAIE IC de uso interno no verifica deudas.
             * En caso que a futuro se realice la validación de deudas y si es PRONABEC o no, se debe de realizar aquí.
             */

            request.state = _cau.STATE_INITIAL;

            return request;
        }

        /// <summary>
        /// Crea solicitud asociada a la generación de Constancia de Idioma Extranjero.
        /// Se usa para el proceso de generación de CAIE para tipo de documento: UC de uso interno y IC de uso interno.
        /// </summary>
        /// <param name="student"></param>
        /// <param name="academicProfile"></param>
        /// <param name="languageId"></param>
        /// <param name="cycle"></param>
        /// <param name="description"></param>
        /// <param name="documentType"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ACRequest> CreateCaieRequestWithoutDebtAsync(Student student,
            Academic.AcademicProfile academicProfile, string languageId, string cycle, string description,
            int documentType, string user)
        {
            var json = JsonConvert.SerializeObject(new {description, cycle});

            var request = await ComposeRequestAsync(student, academicProfile);

            request.document_id = documentType;
            request.description = json;

            if (request.state != _cau.STATE_CANCEL) request.state = _cau.STATE_ATENTION;

            var i = await request.CreateAsync(user, false);

            return i == 1 ? request : null;
        }

        #endregion Methods
    }
}