﻿using System.Collections.Generic;
using System.Linq;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.LanguagesCenter
{
    public class Periods
    {
        #region Properties

        public string period_id { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Lista de Períodos Académicos del CIC
        /// </summary>
        /// <returns>Períodos Académicos del CIC</returns>
        public static List<Periods> getAll()
        {
            DBOContext dbo_context = new DBOContext();
            List<Periods> periods = dbo_context.tbl_notas_ext.Select(s => new Periods {period_id = s.IDPerAcad})
                .Distinct()
                .OrderByDescending(o => o.period_id)
                .ToList();
            return periods;
        }

        #endregion Methods
    }
}