﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CIC;
using ACPermanence.DataBases.NEXO.CIC.Views;
using _acad = ACTools.Constants.AcademicConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter
{
    public class StudentUC : Student
    {
        #region Properties

        public string ModalityId { get; set; }
        public string ModalityName { get; set; }

        /// <summary>
        /// <para>En la tabla se almacenan dos valores:</para>
        ///  * 1 : El estudiante realizó el pago por el trámite
        ///  * 0 : El estudiante aún no ha realizado el pago por el trámite
        /// <para>Adicionalmente se usa los siguientes valores para mostrar en el resultado de búsqueda por lotes:</para>
        ///  * 2 : Estudiantes que no cumplen con la regla para obtener CAIE
        ///  * 3 : Estudiantes no encontrados en la DB
        /// </summary>
        public int PaymentStatus { get; set; }

        public Academic.AcademicProfile GraduateProfile { get; set; }
        public List<Academic.AcademicProfile> GraduateProfiles { get; } = new List<Academic.AcademicProfile>();

        #endregion Properties

        #region Constructors

        public StudentUC()
        {
        }

        public StudentUC(decimal pidm) : base(pidm)
        {
            List<Academic.AcademicProfile> graduationsList = Academic.AcademicProfile.GraduationsList(pidm);
            var graduateStateIdList = new List<int> {_acad.STATE_GRADUATE, _acad.STATE_BACHELOR, _acad.STATE_DEGREE};

            var lastGraduateInfo = Graduated.GetLastGraduationInfo(pidm);
            if (lastGraduateInfo != null)
            {
                GraduateProfile = graduationsList.FirstOrDefault(w =>
                    graduateStateIdList.Contains(w.status) && w.program.id == lastGraduateInfo.IDEscuela);
            }

            GraduateProfiles = graduationsList.Where(w => graduateStateIdList.Contains(w.status)).ToList();
        }

        public StudentUC(decimal pidm, string programId) : this(pidm)
        {
            var studentUC = new Academic.Student(pidm);
            var graduateStateIdList = new List<int> {_acad.STATE_GRADUATE, _acad.STATE_BACHELOR, _acad.STATE_DEGREE};
            var lastGraduateInfo = Graduated.GetLastGraduationInfo(pidm);
            if (lastGraduateInfo != null)
            {
                GraduateProfile = studentUC.profiles
                    .FirstOrDefault(w => graduateStateIdList.Contains(w.status) && w.program.id == programId);
            }
            else
            {
                GraduateProfile = null;
            }
        }

        public StudentUC(decimal pidm, string programId, string languageId) : this(pidm, programId)
        {
            var student = new Student(pidm, languageId);
            Profile = student.Profile;
            Profiles = student.Profiles;
        }

        #endregion Constructors

        #region Public Methods

        // TODO: Dejar de usar el view 'cic_caie_payment_status' y en su lugar preparar esa data aquí.

        /// <summary>
        /// Lista de estudiantes que pueden solicitar "Constancia de Acreditación de Idioma Extranjero" (búsqueda batch)
        /// Incluye campo con estado sobre el pago por el trámite de bachiller:
        /// </summary>
        /// <param name="students">Código de estudiantes</param>
        /// <returns>Estudiantes que pueden solicitar CAIE</returns>
        public static List<StudentUC> GetStudentsWithCaie(string[] students)
        {
            var dboContext = new DBOContext();
            var nexoContext = new NEXOContext();

            // Estudiantes que SI cumplieron reglas CIC para solicitar Constancia de Acreditación de Idioma Extranjero
            List<CICProofOfForeignLanguagePaymentStatus> caiePaymentStatus =
                nexoContext.cic_caie_payment_status.Where(w => students.Contains(w.StudentId)).ToList();
            List<StudentUC> studentsWithCaie = caiePaymentStatus
                .Select(s => new StudentUC
                {
                    Persona = new Person(s.Pidm),
                    Profile = new AcademicProfile
                    {
                        StudentId = s.StudentId, Language = new Language(s.LanguageId), Cycle = s.Cycle,
                        StartDateAtCic = s.StartDateAtCic
                    },
                    Profiles = null,
                    StudentId = s.StudentId,
                    ModalityId = s.ModalityId,
                    ModalityName = s.ModalityName,
                    PaymentStatus = s.PaymentStatus
                }).ToList();

            // Estudiantes que NO tienen Constancia de Acreditación de Idioma Extranjero
            foreach (var s in studentsWithCaie)
            {
                students = students.Where(w => w != s.StudentId).ToArray();
            }

            List<StudentUC> studentsWithoutCaie = dboContext.view_alumno_basico
                .Where(w => students.Contains(w.IDAlumno)).ToList()
                .Select(s => new StudentUC
                {
                    Persona = new Person(s.IDPersonaN),
                    Profiles = null,
                    StudentId = s.IDAlumno,
                    PaymentStatus = _cic.FAILS_CAIE_RULES, // Para mensaje: "Sin Constancia"
                }).ToList();

            // Códigos no encontrados
            foreach (var s in studentsWithoutCaie)
            {
                students = students.Where((w => w != s.StudentId)).ToArray();
            }

            var studentsNotFound = new List<StudentUC>();
            foreach (var s in students)
            {
                var tmp = new StudentUC
                {
                    StudentId = s,
                    PaymentStatus = _cic.STUDENT_NOT_FOUND // Para mensaje: "No Encontrado"
                };
                studentsNotFound.Add(tmp);
            }

            List<StudentUC> st = studentsWithCaie.Union(studentsWithoutCaie).OrderBy(o => o.Persona.full_name).ToList();
            List<StudentUC> nf = studentsNotFound.OrderBy(o => o.StudentId).ToList();

            return nf.Union(st).ToList();
        }

        /// <summary>
        /// Lista de estudiantes que pueden solicitar "Constancia de Acreditación de Idioma Extranjero"
        /// filtrados por periodo de egreso UC e idioma CIC.
        /// Incluye campo con estado sobre el pago por el trámite de bachiller:
        /// </summary>
        /// <param name="graduationPeriod">Periodo de graduación de la UC</param>
        /// <param name="languageId">Código de idioma</param>
        /// <returns>Estudiantes que pueden solicitar CAIE</returns>
        public static List<StudentUC> GetStudentsWithCaie(string graduationPeriod, string languageId)
        {
            var nexoContext = new NEXOContext();
            List<CICProofOfForeignLanguagePaymentStatus> caiePaymentStatus =
                nexoContext.cic_caie_payment_status
                    .Where(w => w.GraduationPeriod == graduationPeriod && w.LanguageId == languageId).ToList();
            List<StudentUC> studentsWithCaie =
                caiePaymentStatus.Select(c => (StudentUC) c).OrderBy(o => o.Persona.full_name).ToList();
            return studentsWithCaie;
        }

        /// <summary>
        /// Lista de estudiantes a los que se puede crear "Constancia de Acreditación de Idioma Extranjero" (a demanda)
        /// </summary>
        /// <param name="students">Array de códigos de estudiantes</param>
        /// <returns>Estudiantes para generar CAIE</returns>
        public static async Task<List<ProofOfForeignLanguageOnDemand>> GetStudentWithCaieOnDemand(string[] students)
        {
            var dboContext = new DBOContext();
            var nexoContext = new NEXOContext();

            var graduatesWithCaie = new List<ProofOfForeignLanguageOnDemand>();
            var graduatesWithoutCaie = new List<ProofOfForeignLanguageOnDemand>();

            var noGraduatesWithCaie = new List<ProofOfForeignLanguageOnDemand>();
            var noGraduatesWithoutCaie = new List<ProofOfForeignLanguageOnDemand>();

            var notFoundCodes = new List<ProofOfForeignLanguageOnDemand>();

            foreach (var student in students)
            {
                List<string> studentIdList = Academic.Student.GetAllStudentId(student);
                List<CICProofOfForeignLanguage> proofOfForeignLanguage = nexoContext
                    .cic_caie.Where(w => studentIdList.Contains(w.StudentId)).ToList();
                var basicData = dboContext.view_alumno_basico.FirstOrDefault(w => studentIdList.Contains(w.IDAlumno));

                var egresado = Graduated.GetLastGraduationInfo(Academic.Student.getPidmFromStudentId(student));
                if (egresado != null) // Egresado
                {
                    if (proofOfForeignLanguage.Any()) // Egresado con CAIE
                    {
                        List<ProofOfForeignLanguageOnDemand> cie = proofOfForeignLanguage
                            .Select<CICProofOfForeignLanguage, ProofOfForeignLanguageOnDemand>(s => s).ToList();

                        foreach (var s in cie)
                        {
                            s.StudentUcId = student;
                            s.GraduationPeriod = egresado.IDPerAcad;
                            s.GraduationSchoolId = egresado.IDEscuela;
                            s.GraduationModalityId = egresado.IDSede;
                            // NOTE: Realizar la verificación de pago implica más tiempo de espera
                            s.PaymentStatus =
                                await new PaymentStatus().IsPaidProofOfForeignLanguageAsync(student,
                                    s.Profile.Language.LanguageId)
                                    ? _cic.PAID
                                    : _cic.NOT_PAID;
                            s.Printable = true;
                            graduatesWithCaie.Add(s);
                        }
                    }
                    else // Egresado sin CAIE
                    {
                        var onDemand = new ProofOfForeignLanguageOnDemand
                        {
                            Persona = new Person().getByStudentCode(student),
                            StudentUcId = student,
                            GraduationPeriod = egresado.IDPerAcad,
                            GraduationSchoolId = egresado.IDEscuela,
                            GraduationModalityId = egresado.IDSede,
                            PaymentStatus = _cic.FAILS_CAIE_RULES
                            // QUESTION: llenar más datos de estudiante?
                        };
                        graduatesWithoutCaie.Add(onDemand);
                    }
                }
                else // No egresado
                {
                    if (basicData != null) // Existe codigo de estudiante en DB
                    {
                        if (proofOfForeignLanguage.Any()) // Estudiante con CAIE
                        {
                            List<ProofOfForeignLanguageOnDemand> cie = proofOfForeignLanguage
                                .Select<CICProofOfForeignLanguage, ProofOfForeignLanguageOnDemand>(s => s).ToList();

                            foreach (var s in cie)
                            {
                                s.Persona = new Person(basicData.IDPersonaN);
                                s.StudentUcId = student;
                                // NOTE: Realizar la verificación de pago implica más tiempo de espera
                                s.PaymentStatus =
                                    await new PaymentStatus().IsPaidProofOfForeignLanguageAsync(student,
                                        s.Profile.Language.LanguageId)
                                        ? _cic.PAID
                                        : _cic.NOT_PAID;
                                s.Printable = true;
                                noGraduatesWithCaie.Add(s);
                            }
                        }
                        else // Estudiante sin CAIE
                        {
                            var onDemand = new ProofOfForeignLanguageOnDemand
                            {
                                Persona = new Person(basicData.IDPersonaN),
                                StudentUcId = student,
                                PaymentStatus = _cic.FAILS_CAIE_RULES
                            };
                            noGraduatesWithoutCaie.Add(onDemand);
                        }
                    }
                    else // Código NO encontrado
                    {
                        var onDemand = new ProofOfForeignLanguageOnDemand
                        {
                            Persona = new Person(),
                            StudentUcId = student,
                            PaymentStatus = _cic.STUDENT_NOT_FOUND,
                            Printable = false
                        };
                        notFoundCodes.Add(onDemand);
                    }
                }
            }

            return graduatesWithCaie.Union(graduatesWithoutCaie).Union(noGraduatesWithCaie)
                .Union(noGraduatesWithoutCaie).Union(notFoundCodes).OrderBy(o => o.Persona.full_name).ToList();
        }

        /// <summary>
        /// Ordena un alista de estudiantes UC en orden alfabético de acuerdo al campo "full_name" (Apellidos, Nombres)
        /// </summary>
        /// <param name="students">Lista de Estudiantes UC</param>
        /// <returns>Lilsta ordenada de estudiantes UC</returns>
        public static List<StudentUC> OrderByFullName(IEnumerable<StudentUC> students)
        {
            var personNotExist = new List<StudentUC>();
            var studentNotFound = new List<StudentUC>();
            var studentFound = new List<StudentUC>();

            foreach (var student in students)
            {
                if (student.Persona == null)
                {
                    personNotExist.Add(student);
                }
                else
                {
                    if (student.Profiles.Any())
                    {
                        studentFound.Add(student);
                    }
                    else
                    {
                        studentNotFound.Add(student);
                    }
                }
            }

            personNotExist = personNotExist.OrderBy(o => o.StudentId).ToList();
            studentNotFound = studentNotFound.OrderBy(o => o.Persona.full_name).ToList();
            studentFound = studentFound.OrderBy(o => o.Persona.full_name).ToList();

            return personNotExist.Union(studentNotFound).Union(studentFound).ToList();
        }

        #endregion Public Methods

        #region Operators

        /// <summary>
        /// Conversión 'CICProofOfForeignLangAccreditationPaymentStatus' a 'StudentUC'
        /// </summary>
        /// <param name="caiePaymentStatus">View 'CIC.ProofOfForeignLangAccreditationPaymentStatus'</param>
        /// <returns>Objeto StudentUC</returns>
        public static explicit operator StudentUC(CICProofOfForeignLanguagePaymentStatus caiePaymentStatus)
        {
            var student = new Student(caiePaymentStatus.Pidm, caiePaymentStatus.LanguageId);
            student.Profile.StartDateAtCic = caiePaymentStatus.StartDateAtCic;
            var studentUc = new StudentUC(student.Persona.id, caiePaymentStatus.GraduationSchoolId)
            {
                StudentId = caiePaymentStatus.StudentId,
                Profile = student.Profile,
                ModalityId = caiePaymentStatus.ModalityId,
                ModalityName = caiePaymentStatus.ModalityName,
                PaymentStatus = caiePaymentStatus.PaymentStatus,
            };
            return studentUc;
        }

        #endregion Operators
    }
}
