﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter
{
    public class CICSection
    {
        public string section { get; set; }
        public string modality { get { return getModality(); } }
        public int cycle { get { return getCycle(); } }
        public string term { get { return getTerm(); } }
        public string language { get { return getLanguage(); } }
        public CICSection(string section) {
            this.section = section;
        }
        private string getTerm()
        {
            if (!validate())
                throw new Exception($"Error con la longitud del codigo({section.Length})");
            string mod = section.ToUpper().Substring(0, 2);
            return $"20{mod}00";
        }
        private string getModality()
        {
            if (!validate())
                throw new Exception($"Error con la longitud del codigo({section.Length})");
            string mod = section.ToUpper().Substring(4, 1);
            return mod;
        }
        private int getCycle()
        {
            if (!validate())
                throw new Exception($"Error con la longitud del codigo({section.Length})");
            string cycleStr = section.Substring(5, 2);
            int cycle;
            if (!int.TryParse(cycleStr, out cycle))
            {
                throw new Exception("formato de ciclo invalido");
            }
            return cycle;
        }
        private string getLanguage() {
            if (!validate())
                throw new Exception($"Error con la longitud del codigo({section.Length})");
            string mod = section.ToUpper().Substring(2, 2);
            switch (mod) {
                case "IN": return _cic.INGLES;
                case "PO": return _cic.PORTUGUES;
                case "IT": return _cic.ITALIANO;
            }
            return _cic.INGLES;
        }
        private bool validate() {
            if (section.Length != 10)
                return false;
            return true;
        }
    }
}
