using System.Collections.Generic;

namespace ACBusiness.LanguagesCenter
{
    public class Search
    {
        #region Methods

        /// <summary>
        /// Busca estudiantes del CIC
        /// </summary>
        /// <param name="studentIdList">Lista de códigos a buscar</param>
        /// <returns></returns>
        public static List<Student> SearchStudent(IEnumerable<string> studentIdList)
        {
            var studentList = new List<Student>();

            foreach (var student in studentIdList)
            {
                if (Academic.Student.ExistStudent(student))
                {
                    var pidm = Academic.Student.getPidmFromStudentId(student);
                    var s = new Student(pidm) {StudentId = student};
                    studentList.Add(s);
                }
                else
                {
                    var s = new Student {StudentId = student};
                    studentList.Add(s);
                }
            }

            return Student.OrderByFullName(studentList);
        }

        /// <summary>
        /// Busca estudiantes UC
        /// </summary>
        /// <param name="studentIdList">Lista de códigos a buscar</param>
        /// <returns></returns>
        public static List<StudentUC> SearchStudentUC(IEnumerable<string> studentIdList)
        {
            var studentList = new List<StudentUC>();

            foreach (var student in studentIdList)
            {
                if (Academic.Student.ExistStudent(student))
                {
                    var pidm = Academic.Student.getPidmFromStudentId(student);
                    var s = new StudentUC(pidm) {StudentId = student};
                    studentList.Add(s);
                }
                else
                {
                    var s = new StudentUC {StudentId = student};
                    studentList.Add(s);
                }
            }

            return StudentUC.OrderByFullName(studentList);
        }

        #endregion Methods
    }
}