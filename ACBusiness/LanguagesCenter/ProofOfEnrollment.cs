﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACBusiness.AtentionCenter;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CIC;
using ACTools.PDF.Documents.LanguagesCenter;
using _cau = ACTools.Constants.AtentionCenterConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter
{
    public class ProofOfEnrollment : Enrolled
    {
        #region Properties

        public bool is_selected { get; set; }
        public string doc_url { get; set; }
        public bool download { get; set; }
        public string save_as { get; set; }

        private const string DOCUMENT_ID = _cic.DOC_CONS_MAT;
        private const int DOC_CONS_MAT_SIGNERS = _cau.DOC_CIC_PROOF_OF_ENROLLMENT;
        private const int PAID = _cic.PAID;
        private const int SAVE_SUCCESSFUL = _cic.SAVE_SUCCESSFUL;
        private const string SRC_CIC_PDF = _cic.SRC_CIC_PDF;
        private const string URI_CIC_PDF = _cic.URI_CIC_PDF;

        #endregion Properties

        #region Implicit Operators

        #endregion Implicit Operators

        #region Static Methods

        /// <summary>
        /// Genera Constancia de Matricula a partir de un array de objeto estudiantes
        /// </summary>
        /// <param name="students">Array de objeto de estudiantes</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public static List<ProofOfEnrollment> generateProofOfEnrollment(ProofOfEnrollment[] students, string user)
        {
            foreach (var student in students)
            {
                // NOTE: Si algun día se asocia a una solicitud, verificar pago aquí
                // if (!student.is_selected || student.payment_status != PAID) continue;
                if (!student.is_selected) continue;

                var doc_path = getProofOfEnrollmentPath(student, user);
                student.doc_url = null;
                student.save_as = null;

                if (student.doc_url.Length > 0)
                {
                    student.download = true;
                }
            }

            return students.OrderBy(o => o.StudentName).ToList();
        }

        /// <summary>
        /// Obtiene el path de la Constancia de Matrícula generado pro estudiante
        /// </summary>
        /// <param name="student">Datos del estudiante para generar el documento</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static Dictionary<string, string> getProofOfEnrollmentPath(ProofOfEnrollment student, string user)
        {
            var nexoContext = new NEXOContext();
            ACSignatory ac_signatory = new ACSignatory();
            List<ACSignatory>
                signers = ac_signatory.listByACDocument(DOC_CONS_MAT_SIGNERS, null); // TODO: NOTE: Definir college.id

            Academic.Student estudiante = new Academic.Student(student.StudentId);

            // Buscamos si existe un registro en la tabla para generar el PDF con esos datos
            CICCertificates cic_certificates =
                nexoContext.cic_certificates
                    .Where(w => w.StudentId == student.StudentId && w.DocumentId == DOCUMENT_ID &&
                                w.LanguageId == student.LanguageId && w.LanguageCycle == student.Cycle &&
                                w.LanguageLevelId == Student.GetLanguageLevelId(student.StudentId, student.LanguageId,
                                    student.Cycle))
                    .OrderByDescending(o => o.CertificateId).FirstOrDefault();

            ProofOfEnrollmentPDF.Data data = null;

            /*DBOtblConstanciaCIC tbl_constancia_cic =
                dbo_context.tbl_constancia_cic.Where(w =>
                        w.IDAlumno == student.student_id && w.AreaEstudio == student.language_id.Substring(1, 2))
                    .OrderByDescending(o => o.NroConstacia).FirstOrDefault();*/

            if (cic_certificates == null) // Si no existe registro, creamos uno
            {
                var new_reg = newProofOfEnrollment(student.StudentId, student.LanguageId, user);

                if (new_reg.StatusId == SAVE_SUCCESSFUL)
                {
                    cic_certificates = nexoContext.cic_certificates
                        .Where(w => w.CertificateId == new_reg.CertificateId).OrderByDescending(o => o.DocumentId)
                        .FirstOrDefault();
                }
                else
                {
                    throw new Exception("No hay datos para continuar debido a que no se relizó el registro");
                }
            }
            else
            {
                data = new ProofOfEnrollmentPDF.Data
                {
                    name_file = $"{cic_certificates.DocumentId}-{student.StudentId}",
                    user = user,
                    signs = ac_signatory.listPDFSign(signers),
                    student_id = student.StudentId,
                    student_names = student.StudentName,
                    student_gender = estudiante.gender,
                    college_name = string.Empty,
                    program_name = string.Empty,
                    request_id = 0, // TODO: Asociar a una solicitud
                    document_number = int.Parse(cic_certificates.DocumentId),
                    GenderData = Academic.Student.getGenderData(student.StudentId),
                    LanguageCycle = string.Empty,
                    LanguageLevel = string.Empty,
                    LanguageName = student.LanguageName,
                    StartDate = cic_certificates.StartDate ?? DateTime.Now,
                    EndDate = cic_certificates.EndDate ?? DateTime.Now,
                    Schedule = cic_certificates.Schedule,
                    Source = SRC_CIC_PDF
                };
            }

            if (data != null)
            {
                return new Dictionary<string, string>
                {
                    {"uri", ProofOfEnrollmentPDF.GetUri(data)},
                    {"save_as", data.name_file}
                };
            }

            throw new Exception("No existe data para continuar.");
        }

        /// <summary>
        /// Guarda un registro para la generación de la Constancia de Matrícula y retorna información sobre el estado
        /// de guardado
        /// </summary>
        /// <param name="student_id">Código de estudiante</param>
        /// <param name="language_id">Código de idioma CIC</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns>Objeto con info sobre el estado de guardado del registro</returns>
        /// <exception cref="Exception">Si no se guarda, se levanta una excepción</exception>
        public static StatusOfNewSavedCertificate newProofOfEnrollment(string student_id, string language_id,
            string user)
        {
            // TODO: Dejar de usar SP «cic_set_new_caie» para guardar registros, en su lugar usar ORM

            return null;
        }

        #endregion Static Methods
    }
}