﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.LanguagesCenter
{
    public class ConstAcredIdiomExt
    {
        #region Propiedades
        public int PIDM { get; set; }
        public string IDPersona { get; set; }
        public string IDAlumno { get; set; }
        public string NomCompleto { get; set; }
        public string IDEscuela { get; set; }
        public string Idioma { get; set; }
        public char Ciclo { get; set; }
        public DateTime FechaInicioCIC { get; set; }
        public char IDEscuelaADM { get; set; }
        public string NombreModalidad { get; set; }
        public DateTime? updateDate { get; set; }

        DBOContext _db = new DBOContext();
        #endregion

        #region Metodos

        public List<ConstAcredIdiomExt> getConstAcredIdiomExtPIDM(int Pidm) {
            List<ConstAcredIdiomExt> list = _db.cic_const_acred_idiom_ext
                .Where(
                    result =>
                        result.PIDM == Pidm
                )
                .ToList()
                .Select(
                    result => new ConstAcredIdiomExt {
                        PIDM = result.PIDM,
                        IDPersona = result.IDPersona,
                        IDAlumno = result.IDAlumno,
                        NomCompleto = result.NomCompleto,
                        IDEscuela = result.IDEscuela,
                        Idioma = result.Idioma,
                        Ciclo = result.Ciclo,
                        FechaInicioCIC = result.FechaInicioCIC,
                        IDEscuelaADM = result.IDEscuelaADM,
                        NombreModalidad = result.NombreModalidad,
                        updateDate = result.updateDate,
                    }
                ).ToList();
            return list;
        }
        #endregion

    }
}
