﻿using ACBusiness.Personal;
using ACPermanence.DataBases.NEXO.CIC;
using ACPermanence.DataBases.NEXO.CIC.Views;

namespace ACBusiness.LanguagesCenter
{
    public class ProofOfForeignLanguageOnDemand : Student
    {
        #region Properties

        public string StudentUcId { get; set; }
        public string ModalityId { get; set; }
        public string ModalityName { get; set; }

        /// <summary>
        /// <para>En la tabla se almacenan tres valores:</para>
        ///  * 2 : Estudiantes que no cumplen con la regla para obtener CAIE
        ///  * 1 : El estudiante realizó el pago por el trámite
        ///  * 0 : El estudiante aún no ha realizado el pago por el trámite
        /// </summary>
        public int PaymentStatus { get; set; }

        public bool Printable { get; set; }
        public string GraduationPeriod { get; set; }
        public string GraduationSchoolId { get; set; }
        public string GraduationModalityId { get; set; }

        #endregion Properties

        #region Implicit Operators

        /// <summary>
        /// Convesión 'CICProofOfForeignLangAccreditationOnDemand' a 'ProofOfForeignLangAccreditationOnDemand'
        /// </summary>
        /// <param name="caieOnDemand">View 'CIC.ProofOfForeignLangAccreditationOnDemand'</param>
        /// <returns>Objeto ProofOfForeignLangAccreditationOnDemand</returns>
        public static implicit operator ProofOfForeignLanguageOnDemand(
            CICProofOfForeignLanguageOnDemand caieOnDemand)
        {
            var onDemand = new ProofOfForeignLanguageOnDemand
            {
                Persona = new Person(caieOnDemand.Pidm),
                StudentId = caieOnDemand.StudentId,
                StudentUcId = caieOnDemand.StudentCicId,
                ModalityId = caieOnDemand.ModalityId,
                ModalityName = caieOnDemand.ModalityName,
                PaymentStatus = caieOnDemand.PaymentStatus,
                Printable = caieOnDemand.Printable == 1
            };
            return onDemand;
        }

        /// <summary>
        /// Conversión 'CICProofOfForeignLanguage' a 'ProofOfForeignLanguageOnDemand'
        /// </summary>
        /// <param name="proofOfForeignLanguage">Tabla 'CIC.ProofOfForeignLanguage'</param>
        /// <returns>Objeto 'ProofOfForeignLanguageOnDemand'</returns>
        public static implicit operator ProofOfForeignLanguageOnDemand(CICProofOfForeignLanguage proofOfForeignLanguage)
        {
            var onDemand = new ProofOfForeignLanguageOnDemand
            {
                Persona = new Person(proofOfForeignLanguage.Pidm),
                StudentId = proofOfForeignLanguage.StudentId,
                ModalityId = proofOfForeignLanguage.ModalityId,
                ModalityName = proofOfForeignLanguage.ModalityName,
            };
            return onDemand;
        }

        #endregion Implicit Operators
    }
}