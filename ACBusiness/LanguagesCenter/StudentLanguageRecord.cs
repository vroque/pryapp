﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter
{
    /// <summary>
    /// Historial de notas de idiomas de un estudiante
    /// </summary>
    public class StudentLanguageRecord
    {
        #region Properties

        public Language Language { get; set; }
        public List<LanguageRecord> LanguageRecord { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Historial de notas de idiomas de una persona a partir de un código de estudiante
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public static async Task<List<StudentLanguageRecord>> GetByStudentIdAsync(string studentId)
        {
            var pidm = Academic.Student.getPidmFromStudentId(studentId);

            return pidm == 0 ? new List<StudentLanguageRecord>() : await GetByStudentPidmAsync(pidm);
        }

        /// <summary>
        /// Historial de notas de idiomas de una persona a través de un PIDM
        /// </summary>
        /// <param name="studentPidm"></param>
        /// <returns></returns>
        public static async Task<List<StudentLanguageRecord>> GetByStudentPidmAsync(decimal studentPidm)
        {
            var studentLanguageRecordList = new List<StudentLanguageRecord>();

            List<Language> languageList = await Language.GetByStudentAsync(studentPidm);

            foreach (var language in languageList)
            {
                var slr = new StudentLanguageRecord
                {
                    Language = language,
                    LanguageRecord = GetApprovedCyclesByLanguageId(studentPidm, language.LanguageId)
                        .Select<DBOtblNotasEXT, LanguageRecord>(s => s).ToList()
                };
                studentLanguageRecordList.Add(slr);
            }

            return studentLanguageRecordList;
        }

        /// <summary>
        /// <para>
        /// Historial de ciclos aprobados según idioma
        /// </para>
        /// <para>
        /// Se modifica el campo 'FecInic' con la fecha de inicio de clases o fecha de examen de la sección,
        /// según corresponda.
        /// </para>
        /// </summary>
        /// <param name="studentPidm">PIDM del estudiante</param>
        /// <param name="languageId">Código del idioma</param>
        /// <returns></returns>
        public static List<DBOtblNotasEXT> GetApprovedCyclesByLanguageId(decimal studentPidm,
            string languageId)
        {
            List<string> studentIdList = Academic.Student.GetAllStudentId(studentPidm);
            var dboContext = new DBOContext();
            List<DBOtblNotasEXT> approvedCycles = dboContext.tbl_notas_ext
                .Where(w => studentIdList.Contains(w.IDAlumno) &&
                            w.IDEscuela == languageId &&
                            ((w.IDSeccionC.Substring(4, 1) != _cic.MOD_EXAMEN_CLASIFICACION &&
                              w.IDSeccionC.Substring(4, 1) != _cic.MOD_EXAMEN_SUFICIENCIA &&
                              w.IDSeccionC.Substring(4, 1) != _cic.MOD_CONVALIDACION &&
                              w.IDSeccionC.Substring(4, 1) != _cic.MOD_SIMULACRO_Y_O_EXAMEN_CAMBRIDGE) &&
                             (w.n51 == null
                                 ? w.n50 >= _cic.MinimumMarkToPassCycle
                                 : w.n51 >= _cic.MinimumMarkToPassCycle) ||
                             (w.IDSeccionC.Substring(4, 1) == _cic.MOD_EXAMEN_CLASIFICACION ||
                              w.IDSeccionC.Substring(4, 1) == _cic.MOD_CONVALIDACION) &&
                             (w.n51 == null
                                 ? w.n50 >= _cic.MinimumMarkToPassExam
                                 : w.n51 >= _cic.MinimumMarkToPassExam) ||
                             // Nota aprobatoria para exámenes de suficiencia "U" = 11 hasta 2018/12/31
                             (w.IDSeccionC.Substring(4, 1) == _cic.MOD_EXAMEN_SUFICIENCIA &&
                              w.fechaexamen.CompareTo(_cic.DateToApplyNewMinimumMarkToPassU) < 0) &&
                             (w.n51 == null
                                 ? w.n50 >= _cic.MinimumMarkToPassExam
                                 : w.n51 >= _cic.MinimumMarkToPassExam) ||
                             // Nota aprobatoria para exámenes de suficiencia "U" = 14 a partir de 2019/01/01
                             (w.IDSeccionC.Substring(4, 1) == _cic.MOD_EXAMEN_SUFICIENCIA &&
                              w.fechaexamen.CompareTo(_cic.DateToApplyNewMinimumMarkToPassU) >= 0) &&
                             (w.n51 == null
                                 ? w.n50 >= _cic.NewMinimumMarkToPassU
                                 : w.n51 >= _cic.NewMinimumMarkToPassU) ||
                             // Excepciones particulares a las reglas definidas
                             (w.IDSeccionC == "18INU0105M" || w.IDSeccionC == "18INU0106M") &&
                             (w.n51 == null ? w.n50 >= 14 : w.n51 >= 14)))
                .ToList();

            if (!approvedCycles.Any()) return approvedCycles;

            var languageRecord = approvedCycles
                .Join(dboContext.tbl_seccion_c,
                    notasExt => new {seccion_ = notasExt.IDSeccionC, sede_ = notasExt.IDsede},
                    seccionC => new {seccion_ = seccionC.IDSeccionC, sede_ = seccionC.IDsede},
                    (notas, seccion) => new {notas, seccion})
                .ToList();

            /*
             * Actualizamos el campo 'FecInic' de 'DBOtblNotasEXT' con información de 'fechaexamen' o
             * 'FecInic' de 'DBOtblSeccionC' según corresponda
             */
            foreach (var record in languageRecord)
            {
                if (record.notas.IDSeccionC.Substring(4, 1) == _cic.MOD_EXAMEN_CLASIFICACION ||
                    record.notas.IDSeccionC.Substring(4, 1) == _cic.MOD_EXAMEN_SUFICIENCIA ||
                    record.notas.IDSeccionC.Substring(4, 1) == _cic.MOD_CONVALIDACION)
                {
                    record.notas.FecInic = record.notas.fechaexamen;
                }
                else
                {
                    record.notas.FecInic = record.seccion.FecInicReal ?? record.seccion.FecInic;
                }
            }

            List<DBOtblNotasEXT> approvedLanguageRecord = languageRecord
                .Select(s => s.notas).OrderBy(o => o.FecInic).ToList();

            return approvedLanguageRecord;
        }

        /// <summary>
        /// <para>
        /// Historial de ciclos aprobados por idioma, válidos para sacar el nivel de idioma que tiene un estudiante.
        /// </para>
        /// <para>
        /// Se modifica el campo 'FecInic' con la fecha de inicio de clases o fecha de examen de la sección,
        /// según corresponda.
        /// </para>
        /// <para>
        /// No se incluyen el historial de exámenes de clasificación 'X',
        /// ni exámenes de suficiencia 'U', debido a que estos no cuenta para el cálculo
        /// del nivel de idioma de un estudiante
        /// </para>
        /// </summary>
        /// <param name="studentPidm">PIDm del estudiante</param>
        /// <param name="languageId">Código del idioma</param>
        /// <returns></returns>
        public static async Task<List<DBOtblNotasEXT>> GetApprovedCyclesByLanguageIdValidForLangLevelAsync(
            decimal studentPidm, string languageId)
        {
            List<DBOtblNotasEXT> approvedCycles =
                GetApprovedCyclesByLanguageId(studentPidm, languageId);
            List<DBOtblNotasEXT> approvedCyclesRecord = approvedCycles
                .Where(w => w.IDSeccionC.Substring(4, 1) != _cic.MOD_EXAMEN_CLASIFICACION)
                .ToList();

            return approvedCyclesRecord;
        }

        /// <summary>
        /// <para>
        /// Historial de ciclos aprobados por idioma, válidos para sacar la constancia de idioma extranjero
        /// </para>
        /// <para>
        /// Se modifica el campo 'FecInic' con la fecha de inicio de clases o fecha de examen de la sección,
        /// según corresponda.
        /// </para>
        /// <para>
        /// No se incluyen el historial de exámenes de clasificación 'X',
        /// debido a que estos no cuenta para el cálculo /// del nivel de idioma de un estudiante
        /// </para>
        /// </summary>
        /// <param name="studentPidm">PIDM del estudiante</param>
        /// <param name="languageId">Código de idioma</param>
        /// <returns></returns>
        public static async Task<List<DBOtblNotasEXT>> GetApprovedCyclesByLanguageIdValidForCaieAsync(
            decimal studentPidm, string languageId)
        {
            List<DBOtblNotasEXT> approvedCycles = GetApprovedCyclesByLanguageId(studentPidm, languageId);
            List<DBOtblNotasEXT> approvedCyclesRecord = approvedCycles
                .Where(w => w.IDSeccionC.Substring(4, 1) != _cic.MOD_EXAMEN_CLASIFICACION).ToList();

            return approvedCyclesRecord;
        }

        /// <summary>
        /// <para>
        /// Historial de ciclos aprobados en inglés válidos para sacar el nivel de inglés que debe de tener un
        /// estudiante para que se pueda matricular en la asignatura de "Inglés Profesional" en la UC.
        /// </para>
        /// <para>
        /// Se modifica el campo 'FecInic' con la fecha de inicio de clases o fecha de examen de la sección,
        /// según corresponda.
        /// </para>
        /// </summary>
        /// <param name="studentPidm">PIDM del estudiante</param>
        /// <returns></returns>
        public static async Task<List<DBOtblNotasEXT>> GetApprovedCyclesValidForProfessionalEnglishAsync(
            decimal studentPidm)
        {
            List<DBOtblNotasEXT> englishRecord =
                await GetApprovedCyclesByLanguageIdValidForLangLevelAsync(studentPidm, _cic.INGLES);

            return englishRecord;
        }

        /// <summary>
        /// <para>
        /// Idiomas que tiene un estudiante para solicitar la constancia de idioma extranjero
        /// </para>
        /// <para>
        /// Se modifica el campo 'FecInic' con la fecha de inicio de clases o fecha de examen de la sección,
        /// según corresponda.
        /// </para>
        /// </summary>
        /// <param name="studentPidm">PIDM del estudiante</param>
        /// <returns></returns>
        public static async Task<List<DBOtblNotasEXT>> GetApprovedCyclesValidForProofOfForeignLanguageAsync(
            decimal studentPidm)
        {
            var languageRecord = new List<DBOtblNotasEXT>();
            List<Language> languageList = await Language.GetByStudentAsync(studentPidm);
            foreach (var language in languageList)
            {
                var lastApprovedCycleByLanguage =
                    (await GetApprovedCyclesByLanguageIdValidForCaieAsync(studentPidm, language.LanguageId))
                    .OrderByDescending(o => o.FecInic).FirstOrDefault();
                languageRecord.Add(lastApprovedCycleByLanguage);
            }

            return languageRecord;
        }

        #endregion Methods
    }
}
