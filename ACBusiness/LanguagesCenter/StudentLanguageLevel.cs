﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.CIC.Views;
using Newtonsoft.Json;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter
{
    /// <summary>
    /// Nivel de Idiomas de Estudiante
    /// </summary>
    public class StudentLanguageLevel
    {
        #region Properties

        [JsonIgnore]
        public decimal Pidm { get; set; }

        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string LastCycle { get; set; }

        private readonly DBOContext _dboContext = new DBOContext();
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Methods

        /// <summary>
        /// Información de registro en dbo.TblNotasEXT según el nivel de idioma del estudiante
        /// </summary>
        /// <param name="studentLangLevels"></param>
        /// <returns></returns>
        public List<DBOtblNotasEXT> GetRegInfoByStudentLevel(IEnumerable<StudentLanguageLevel> studentLangLevels)
        {
            if (studentLangLevels == null) throw new ArgumentNullException();

            var regInfo = new List<DBOtblNotasEXT>();

            foreach (var studentLangLevel in studentLangLevels)
            {
                List<string> studentIdList = Academic.Student.GetAllStudentId(studentLangLevel.Pidm);

                List<DBOtblNotasEXT> notasExt = _dboContext.tbl_notas_ext
                    .Where(w => w.IDEscuela == studentLangLevel.LanguageId &&
                                studentIdList.Contains(w.IDAlumno) &&
                                (w.IDSeccionC.Substring(5, 2) == studentLangLevel.LastCycle ||
                                 w.ciclo == studentLangLevel.LastCycle)).ToList();

                if (!notasExt.Any()) continue;

                foreach (var ext in notasExt)
                {
                    if (ext.ciclo != null)
                    {
                        ext.FecInic = ext.fechaexamen;
                    }
                }

                var firstNotasExt = notasExt.OrderByDescending(o => o.FecInic).FirstOrDefault();

                regInfo.Add(firstNotasExt);
            }

            return regInfo;
        }

        #endregion Methods

        #region Operators

        public static implicit operator StudentLanguageLevel(CICStudentLevel cicStudentLevel)
        {
            var studentLevel = new StudentLanguageLevel
            {
                Pidm = cicStudentLevel.Pidm,
                LanguageId = cicStudentLevel.LanguageId,
                LanguageName = cicStudentLevel.LanguageName,
                LastCycle = cicStudentLevel.LastCycle
            };

            return studentLevel;
        }

        #endregion Operators
    }
}