﻿using System;
using System.Linq;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter
{
    public class Cycle
    {
        #region Properties

        public string Classroom { get; set; }
        public int CycleNumber { get; set; }

        /// <summary>
        /// Tipo de Sección (Intensivo, SuperIntensivo,...)
        /// </summary>
        public string Type { get; set; }

        public DateTime? Begin { get; set; }
        public DateTime? End { get; set; }

        public LanguageLevel LanguageLevel { get; set; }

        private readonly DBOContext _dboContext = new DBOContext();
        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public Cycle(string classroom)
        {
            Classroom = classroom;
            SyncCycleNumber(classroom);
            SyncClassroomType(classroom);
            SyncCycleDuration(classroom);
            SyncLanguageLevel(classroom);
        }

        public Cycle(string classroom, string cycle) : this(classroom)
        {
            if (Convert.ToInt32(cycle) > 0)
            {
                CycleNumber = Convert.ToInt32(cycle);
            }
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Número de ciclo
        /// </summary>
        /// <param name="classroom">Sección</param>
        /// <exception cref="ArgumentException"></exception>
        private void SyncCycleNumber(string classroom)
        {
            if (classroom.Length != 10) throw new ArgumentException("Longitud de sección inválida");

            var type = classroom.Substring(4, 1);
            int cycleNumber;

            switch (type)
            {
                case "I":
                case "R":
                case "S":
                case "K":
                case "P":
                case "L":
                    cycleNumber = int.Parse(classroom.Substring(5, 2));
                    break;
                //case "C":
                //case "F":
                //case "U":
                //case "X":
                default:
                    cycleNumber = 0;
                    break;
            }

            CycleNumber = cycleNumber;
        }

        /// <summary>
        /// Tipo de sección
        /// </summary>
        /// <param name="classroom"></param>
        private void SyncClassroomType(string classroom)
        {
            var abbr = classroom.Substring(4, 1);
            Type = _nexoContext.cic_settings.FirstOrDefault(w => w.Active && w.SettingTypeId == 1 && w.Abbr == abbr)
                ?.Name;
        }

        /// <summary>
        /// Duración de ciclo (sección)
        /// </summary>
        /// <param name="classroom"></param>
        private void SyncCycleDuration(string classroom)
        {
            var info = _dboContext.tbl_seccion_c.FirstOrDefault(w => w.IDSeccionC == classroom);
            Begin = info?.FecInic;
            End = info?.FecFin;
        }

        /// <summary>
        /// Nivel de idioma
        /// </summary>
        /// <param name="classroom">Sección</param>
        private void SyncLanguageLevel(string classroom)
        {
            LanguageLevel = new LanguageLevel().GetLevelByClassroom(classroom);
        }

        /// <summary>
        /// Número de ciclo
        /// </summary>
        /// <param name="tblNotasExt"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static string GetCycleNumber(DBOtblNotasEXT tblNotasExt)
        {
            if (tblNotasExt == null) throw new ArgumentNullException();

            var type = tblNotasExt.IDSeccionC.Substring(4, 1);
            string cycleNumber;

            switch (type)
            {
                case "I":
                case "R":
                case "S":
                case "K":
                case "P":
                case "L":
                    cycleNumber = tblNotasExt.IDSeccionC.Substring(5, 2);
                    break;
                //case "C":
                //case "F":
                //case "U":
                //case "X":
                default:
                    cycleNumber = tblNotasExt.ciclo;
                    break;
            }

            return cycleNumber;
        }

        #endregion Methods
    }
}