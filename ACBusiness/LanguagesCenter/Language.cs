﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.LanguagesCenter
{
    public class Language
    {
        #region Properties

        public string LanguageId { get; set; }
        public string LanguageName { get; set; }

        #endregion Properties

        #region Constructors

        public Language()
        {
        }

        public Language(string languageId)
        {
            SyncLanguage(languageId);
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Lista de Idiomas activos del CIC
        /// </summary>
        /// <returns>Idiomas CIC</returns>
        public static async Task<IEnumerable<Language>> GetAllAsync()
        {
            var dboContext = new DBOContext();
            var languages = await dboContext.tbl_escuela
                .Where(w => w.IDDependencia == _corp.UC &&
                            w.IDTipoEsc == "CIC" && _cic.ACTIVE_LANGUAGES_LIST.Contains(w.IDEscuela))
                .Select(s => new Language
                {
                    LanguageId = s.IDEscuela,
                    LanguageName = s.NomEscuela
                })
                .ToListAsync();
            return languages;
        }

        /// <summary>
        /// Lista de Idiomas según Programa CIC
        /// </summary>
        /// <param name="languageProgramId"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<Language>> GetByLanguageProgramAsync(int languageProgramId)
        {
            var nexoContext = new NEXOContext();
            var languages = await nexoContext.cic_language_level
                .Where(w => w.LanguageProgramId == languageProgramId)
                .Select(s => new Language
                {
                    LanguageId = s.LanguageId,
                    LanguageName = s.LanguageName
                })
                .Distinct().ToListAsync();
            return languages;
        }

        /// <summary>
        /// Lista de idiomas que ha cursado un estudiante
        /// </summary>
        /// <param name="pidm">PIDM del estudiante</param>
        /// <param name="caieFilter">filtro para CAIE?</param>
        /// <returns></returns>
        public static async Task<List<Language>> GetByStudentAsync(decimal pidm, bool caieFilter = false)
        {
            List<string> studentIdList = Academic.Student.GetAllStudentId(pidm);
            var dboContext = new DBOContext();
            List<DBOtblNotasEXT> lang = await dboContext.tbl_notas_ext
                .Where(w => studentIdList.Contains(w.IDAlumno)
                            && _cic.ACTIVE_LANGUAGES_LIST.Contains(w.IDEscuela)
                            && (w.n50 != null || w.n51 != null)
                            && w.IDSeccionC.Substring(4, 1) != _cic.MOD_EXAMEN_CLASIFICACION)
                .ToListAsync();

            // examen de suficiencia "____U_____" debe de ser considerado solo para
            // la constancia de idioma extranjero, inglés intermedio (inglés profesional) y segundo idioma
            if (!caieFilter)
            {
                lang = lang.Where(w => w.IDSeccionC.Substring(4, 1) != _cic.MOD_EXAMEN_SUFICIENCIA).ToList();
            }

            List<Language> languages = lang.Select(s => s.IDEscuela).Distinct().ToList()
                .Select(s => new Language(s)).ToList();

            return languages;
        }

        private void SyncLanguage(string languageId)
        {
            if (!_cic.ACTIVE_LANGUAGES_LIST.Contains(languageId)) return;
            LanguageId = languageId;
            LanguageName = _cic.LANGUAGES[languageId];
        }

        #endregion Methods
    }
}
