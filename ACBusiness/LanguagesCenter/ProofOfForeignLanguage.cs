﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Academic;
using ACBusiness.Admision;
using ACBusiness.AtentionCenter;
using ACBusiness.Institutional;
using ACBusiness.Personal;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.NEXO.CIC.Views;
using ACTools.PDF.Documents.LanguagesCenter;
using _admission = ACTools.Constants.AdmissionConstant;
using _cau = ACTools.Constants.AtentionCenterConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _corp = ACTools.Constants.InstitucionalConstants;
using _util = ACTools.Util;

namespace ACBusiness.LanguagesCenter
{
    /// <summary>
    /// Constancia de Idioma Extranjero UC - Uso Interno
    /// </summary>
    public class ProofOfForeignLanguage : StudentUC
    {
        #region Properties

        public bool IsSelected { get; set; }
        public string DocUrl { get; set; }
        public bool Download { get; set; }
        public string SaveAs { get; set; }
        public bool Processing { get; set; }
        public bool ProcessingError { get; set; }

        /// <summary>
        /// Constancia de Idioma Extranjero UC - Uso Interno
        /// </summary>
        private const int InternalCaieId = _cau.DOC_CIC_CAIE;

        /// <summary>
        /// Constancia de Idioma Extranjero UC - Uso Externo
        /// </summary>
        private const int ExternalCaieId = _cau.DOC_CIC_CAIE_EXT;

        /// <summary>
        /// Constancia de Idioma Extranjero Instituto
        /// </summary>
        private const int DocumentIdInstitute = _cau.DOC_CIC_CAIE_INST;

        private const string Description = "Solicitud autogenerada para el proceso de graduación.";

        #endregion Properties

        #region Constructors

        public ProofOfForeignLanguage()
        {
        }

        public ProofOfForeignLanguage(decimal pidm) : base(pidm)
        {
        }

        public ProofOfForeignLanguage(decimal pidm, string program) : this(pidm)
        {
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Genera CAIE UC de Uso Interno a partir de un array de objeto de estudiantes
        /// </summary>
        /// <param name="students">Array de objeto de estudiantes</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public static async Task<List<ProofOfForeignLanguage>> GenerateProofOfForeignLanguageAsync(
            ProofOfForeignLanguage[] students, string user)
        {
            return await GenerateProofOfForeignLanguageAsync(students, user, InternalCaieId);
        }

        /// <summary>
        /// Genera CAIE Interno con datos personalizados
        /// </summary>
        /// <param name="customData">Datos personalizados</param>
        /// <param name="student">Información extra del estudiante</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public static async Task<ProofOfForeignLanguage> GenerateCustomInternalProofOfForeignLanguageAsync(
            ProofOfForeignLanguagePDF.Data customData, ProofOfForeignLanguage student, string user)
        {
            Dictionary<string, string> caiePath =
                await GenerateCustomProofOfForeignLanguageAsync(customData, student, user, InternalCaieId);
            student.DocUrl = caiePath["url"];
            student.SaveAs = caiePath["saveAs"];
            if (!string.IsNullOrEmpty(student.DocUrl)) student.Download = true;
            if (string.IsNullOrEmpty(student.DocUrl)) student.ProcessingError = true;
            return student;
        }

        /// <summary>
        /// Genera CAIE Externo con datos personalizados
        /// </summary>
        /// <param name="customData">Datos personalizados</param>
        /// <param name="student">Información extra del estudiante</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <returns></returns>
        public static async Task<ProofOfForeignLanguage> GenerateCustomExternalProofOfForeignLanguageAsync(
            ProofOfForeignLanguagePDF.Data customData, ProofOfForeignLanguage student, string user)
        {
            Dictionary<string, string> caiePath =
                await GenerateCustomProofOfForeignLanguageAsync(customData, student, user, ExternalCaieId);
            student.DocUrl = caiePath["url"];
            student.SaveAs = caiePath["saveAs"];
            if (!string.IsNullOrEmpty(student.DocUrl)) student.Download = true;
            if (string.IsNullOrEmpty(student.DocUrl)) student.ProcessingError = true;
            return student;
        }

        /// <summary>
        /// Genera CAIE para UC e IC a partir de un array de objeto de estudiantes
        /// </summary>
        /// <param name="students">Array de objeto de estudiantes</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <param name="documentType">Tipo de documento (Solo puede ser CAIE de uso interno para UC e IC)</param>
        /// <returns></returns>
        public static async Task<List<ProofOfForeignLanguage>> GenerateProofOfForeignLanguageAsync(
            ProofOfForeignLanguage[] students, string user, int documentType)
        {
            if (documentType != InternalCaieId && documentType != DocumentIdInstitute) return students.ToList();

            foreach (var student in students)
            {
                if (student.IsSelected && student.Processing &&
                    _cic.ACTIVE_LANGUAGES_LIST.Contains(student.Profile.Language.LanguageId) &&
                    (student.PaymentStatus == _cic.PAID || student.PaymentStatus == _cic.NOT_PAID))
                {
                    Dictionary<string, string> caiePath =
                        await GetProofOfForeignLanguagePathAsync(student, user, documentType);
                    student.DocUrl = caiePath["url"];
                    student.SaveAs = caiePath["saveAs"];

                    if (!string.IsNullOrEmpty(student.DocUrl)) student.Download = true;

                    if (string.IsNullOrEmpty(student.DocUrl)) student.ProcessingError = true;
                }

                student.Processing = false;
            }

            return students.ToList();
        }

        /// <summary>
        /// Obtiene el path del CAIE generado por estudiante
        /// </summary>
        /// <param name="student">Datos del estudiante para generar CAIE</param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <param name="documentType"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private static async Task<Dictionary<string, string>> GetProofOfForeignLanguagePathAsync(StudentUC student,
            string user, int documentType)
        {
            var nexoContext = new NEXOContext();

            var acSignatory = new ACSignatory();
            List<ACSignatory> signers = acSignatory.listByACDocument(documentType, null);
            if (!signers.Any()) throw new NullReferenceException("No se encontraron firmantes para este documento");

            Academic.Student estudiante;

            var cicCertificate = new Data(student.Persona.id, student.Profile);

            Academic.AcademicProfile academicProfile;

            switch (documentType)
            {
                case _cau.DOC_CIC_CAIE:
                    var mov = Academic.Student.GetMovimientosEstudiante(student.Persona.id).ToList()
                        .FirstOrDefault(s => s.PERIODO_FIN == "ACTIVO");
                    if (mov == null) throw new NullReferenceException("No se encontró movimiento de estudiante.");

                    academicProfile = new Academic.AcademicProfile
                    {
                        person_id = student.Persona.id,
                        div = _corp.UC,
                        campus = Campus.get(mov.CODIGO_CAMPUS),
                        department = mov.CODIGO_DEPARTAMENTO,
                        subject = mov.CATALOGO_REAL,
                        college = Program.getCollege(mov.CODIGO_PROGRAMA),
                        program = Program.get(mov.CODIGO_PROGRAMA),
                        adm_type = AdmisionType.get(Entrant.GetEntrantInfo(student.StudentId, mov.CODIGO_PROGRAMA)
                            .type_of_income_id.ToString(CultureInfo.InvariantCulture), source: "apec")
                    };
                    break;
                case _cau.DOC_CIC_CAIE_INST:
                    var person = new Person().getByStudentCode(student.StudentId);
                    estudiante = new Academic.Student
                    {
                        id = student.StudentId,
                        person_id = student.Persona.id,
                        first_name = person.first_name,
                        last_name = person.last_name
                    };

                    var entrantInfo = StudentIC.GetLastEntrantInfo(student.Persona.id);
                    if (entrantInfo == null)
                    {
                        throw new NullReferenceException("No se encontró información de postulante.");
                    }

                    var program = new Program {id = entrantInfo.IDEscuela, name = string.Empty};

                    academicProfile = new Academic.AcademicProfile
                    {
                        person_id = student.Persona.id,
                        div = _corp.IC,
                        campus = Campus.get("S01"),
                        department = Department.toBanner(entrantInfo.IDEscuelaADM),
                        subject = entrantInfo.IDPlanestudio,
                        college = null,
                        program = program,
                        adm_type = null
                    };
                    break;
                default:
                    throw new NullReferenceException("Tipo de documento inválido.");
            }

            /**
             * Generación de Solicitud para Constancia de Idioma Extranjero
             */
            ACRequest acRequest;

            // Se busca si existe solicitud asociada a la creación de CAIE
            var existRequest = nexoContext.cau_requests
                .Where(w => w.documentID == documentType &&
                            w.pidm == student.Persona.id &&
                            w.program == student.Profile.Language.LanguageId &&
                            w.requestStateID > _cau.STATE_PAYMENT_PENDING &&
                            w.requestStateID < _cau.STATE_FINAL)
                .OrderByDescending(o => o.requestID).FirstOrDefault();
            // Si existe solicitud asociada, utilizamos los datos de éste para la generación de la constancia
            if (existRequest != null)
            {
                acRequest = await ACRequest.get(existRequest.requestID);
            }
            else
            {
                acRequest = await new Document().CreateCaieRequestWithoutDebtAsync(student, academicProfile,
                    student.Profile.Language.LanguageId, student.Profile.Cycle, Description, documentType, user);
                if (ACRequest.isNull(acRequest)) return null;
            }

            ProofOfForeignLanguagePDF.Data data;

            switch (documentType)
            {
                case _cau.DOC_CIC_CAIE:
                    var graduateProfile = Academic.AcademicProfile.GraduationsList(student.Persona.id).FirstOrDefault();
                    if (student.GraduateProfile == null) student.GraduateProfile = graduateProfile;
                    var modalityInfo = Academic.Student.GetMovimientosEstudiante(student.Persona.id)
                        .FirstOrDefault(w => w.CODIGO_PROGRAMA == graduateProfile?.program.id);

                    if (modalityInfo == null)
                    {
                        throw new NullReferenceException(
                            $"No se encontró modalidad de {student.StudentId} en la carrera {student.GraduateProfile.program.id}");
                    }

                    data = new ProofOfForeignLanguagePDF.Data
                    {
                        name_file =
                            $"{acRequest.id}-{acRequest.document_number}-{Academic.Student.getDNI(student.StudentId)}",
                        user = user,
                        signs = acSignatory.listPDFSign(signers),
                        student_id = Academic.Student.getDNI(student.StudentId),
                        student_names = student.Persona.full_name,

                        college_name = College.getByProgramID(student.GraduateProfile?.program.id).name,
                        program_name = Program.get(student.GraduateProfile?.program.id).name,
                        request_id = acRequest.id,
                        document_number = acRequest.document_number,

                        GenderData = Academic.Student.getGenderData(student.StudentId),
                        LanguageLevelName = (await Settings.GetCaieLanguageLevelName()).Name,
                        LanguageName = student.Profile.Language.LanguageName,

                        Avg = cicCertificate.GetAvg(),
                        TotalHours = cicCertificate.GetTotalHours(),
                        StartDate = cicCertificate.StartDate,
                        EndDate = cicCertificate.EndDate,
                        GraduationModality = _admission.MODALITY_OF_ADMISSION[modalityInfo.CODIGO_DEPARTAMENTO],
                        DocumentType = acRequest.document_id,
                        Source = _cic.SRC_CAIE_PDF
                    };
                    break;
                case _cau.DOC_CIC_CAIE_INST:
                    data = new ProofOfForeignLanguagePDF.Data
                    {
                        name_file =
                            $"{acRequest.id}-{acRequest.document_number}-{Academic.Student.getDNI(student.StudentId)}",
                        user = user,
                        signs = acSignatory.listPDFSign(signers),
                        student_id = Academic.Student.getDNI(student.StudentId),
                        student_names = student.Persona.full_name,
                        request_id = acRequest.id,
                        document_number = acRequest.document_number,
                        GenderData = Academic.Student.getGenderData(student.StudentId),
                        LanguageLevelName = (await Settings.GetCaieLanguageLevelName()).Name,
                        LanguageName = student.Profile.Language.LanguageName,
                        Avg = cicCertificate.GetAvg(),
                        TotalHours = cicCertificate.GetTotalHours(),
                        StartDate = cicCertificate.StartDate,
                        EndDate = cicCertificate.EndDate,
                        DocumentType = acRequest.document_id,
                        Source = _cic.SRC_CAIE_PDF
                    };
                    break;
                default:
                    throw new NullReferenceException("Tipo de documento inválido.");
            }


            if (!data.signs.Any()) throw new NullReferenceException("No se encontraron firmas para este documento");

            string msg;

            if (_util.LanguagesCenter.ExistCaieDocument(data.name_file))
            {
                msg = $"Solicitud generada el {DateTime.Now}, por el usuario: {user}";
                await acRequest.goFinal(msg, user);

                return new Dictionary<string, string>
                {
                    {"url", ProofOfForeignLanguagePDF.GetUri(data)},
                    {"saveAs", data.name_file}
                };
            }

            var docUrl = ProofOfForeignLanguagePDF.GetUri(data);

            if (!string.IsNullOrEmpty(docUrl))
            {
                msg = $"Solicitud generada el {DateTime.Now}, por el usuario: {user}";
                await acRequest.goFinal(msg, user);

                return new Dictionary<string, string>
                {
                    {"url", docUrl},
                    {"saveAs", data.name_file}
                };
            }

            msg = $"Error al generar documento el {DateTime.Now}, por el usuario: {user}";
            await acRequest.goCancel(msg, user);

            return new Dictionary<string, string>
            {
                {"url", null},
                {"saveAs", null}
            };
        }

        /// <summary>
        /// Genera CAIE (interno/externo) a demanda a partir de datos personalizados ingresados por el usuario
        /// </summary>
        /// <param name="customData">Data personalizada para generar el archivo PDF</param>
        /// <param name="student"></param>
        /// <param name="user">Usuario que realiza la operación</param>
        /// <param name="documentType">Tipo de documento</param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        private static async Task<Dictionary<string, string>> GenerateCustomProofOfForeignLanguageAsync(
            ProofOfForeignLanguagePDF.Data customData, StudentUC student, string user, int documentType)
        {
            var nexoContext = new NEXOContext();
            const string desc =
                "Solicitud generada automáticamente para constancias procesadas de manera personalizada";

            var acSignatory = new ACSignatory();
            List<ACSignatory> signers = acSignatory.listByACDocument(documentType, null);
            if (!signers.Any()) throw new NullReferenceException("No se encontraron firmantes para este documento");

            /**
             * Generación de Solicitud para Constancia de Idioma Extranjero
             */

            ACRequest acRequest;
            string changeRequestStateMsg;

            // Se busca si existe solicitud asociada a la creación de CAIE
            var existRequest = nexoContext.cau_requests
                .Where(w => w.documentID == documentType &&
                            w.pidm == student.Persona.id &&
                            w.program == student.Profile.Language.LanguageId &&
                            w.requestStateID > _cau.STATE_PAYMENT_PENDING &&
                            w.requestStateID < _cau.STATE_FINAL)
                .OrderByDescending(o => o.requestID).FirstOrDefault();
            // Si existe solicitud asociada, utilizamos los datos de éste para la generación de la constancia
            if (existRequest != null)
            {
                acRequest = await ACRequest.get(existRequest.requestID);
            }
            else
            {
                var customDescription = $"{desc} - Nivel: {customData.LanguageLevelName}";
                if (documentType == ExternalCaieId)
                {
                    customDescription = $"{desc} - Nivel: {customData.LanguageLevelName}, Prom: {customData.Avg}" +
                                        $", Duración {customData.TotalHours}, Inicio: {customData.StartDate}" +
                                        $", Fin: {customData.EndDate}";
                }

                acRequest = await new Document().CreateCaieRequestWithoutDebtAsync(student, student.GraduateProfile,
                    student.Profile.Language.LanguageId, student.Profile.Cycle, customDescription, documentType, user);
                if (ACRequest.isNull(acRequest)) return null;
            }

            var data = customData;
            data.name_file =
                $"{acRequest.id}-{acRequest.document_number}-{student.Persona.document_number}";
            data.user = user;
            data.signs = acSignatory.listPDFSign(signers);
            data.request_id = acRequest.id;
            data.document_number = acRequest.document_number;
            data.GenderData = Academic.Student.getGenderData(student.Persona.document_number);
            data.DocumentType = acRequest.document_id;
            data.Source = _cic.SRC_CAIE_PDF;

            if (!data.signs.Any())
            {
                changeRequestStateMsg = $"Cancelado porque no se encontraron firmas para el documento, usuario: {user}";
                await acRequest.goCancel(changeRequestStateMsg, user);
                throw new NullReferenceException("No se encontraron firmas para este documento");
            }

            if (_util.LanguagesCenter.ExistCaieDocument(data.name_file))
            {
                changeRequestStateMsg = $"Solicitud generada el {DateTime.Now}, por el usuario: {user}";
                await acRequest.goFinal(changeRequestStateMsg, user);

                return new Dictionary<string, string>
                {
                    {"url", ProofOfForeignLanguagePDF.GetUri(data)},
                    {"saveAs", data.name_file}
                };
            }

            var docUrl = ProofOfForeignLanguagePDF.GetUri(data);

            if (!string.IsNullOrEmpty(docUrl))
            {
                changeRequestStateMsg = $"Solicitud generada el {DateTime.Now}, por el usuario: {user}";
                await acRequest.goFinal(changeRequestStateMsg, user);

                return new Dictionary<string, string>
                {
                    {"url", docUrl},
                    {"saveAs", data.name_file}
                };
            }

            changeRequestStateMsg = $"Error al generar documento el {DateTime.Now}, por el usuario: {user}";
            await acRequest.goCancel(changeRequestStateMsg, user);

            return new Dictionary<string, string>
            {
                {"url", null},
                {"saveAs", null}
            };
        }

        #endregion Methods

        #region Operators

        /// <summary>
        /// Conversion 'CICProofOfForeignLangAccreditationPaymentStatus' a 'ProofOfForeignLangAccreditation'
        /// </summary>
        /// <param name="caiePaymentStatus">View CIC.ProofOfForeignLangAccreditationPaymentStatus</param>
        /// <returns>Objeto ProofOfForeignLangAccreditation</returns>
        public static explicit operator ProofOfForeignLanguage(CICProofOfForeignLanguagePaymentStatus caiePaymentStatus)
        {
            var caie = new ProofOfForeignLanguage(caiePaymentStatus.Pidm, caiePaymentStatus.GraduationSchoolId)
            {
                ModalityId = caiePaymentStatus.ModalityId,
                ModalityName = caiePaymentStatus.ModalityName,
                PaymentStatus = caiePaymentStatus.PaymentStatus,
                StudentId = caiePaymentStatus.StudentId,
            };

            return caie;
        }

        #endregion Operators

        /// <summary>
        /// Datos para Nueva Constancia de Idioma Extranjero
        /// </summary>
        public class Data
        {
            public decimal Pidm { get; set; }
            public AcademicProfile Profile { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }

            public Data(decimal pidm, AcademicProfile academicProfile)
            {
                Pidm = pidm;
                Profile = academicProfile;
                SyncDate();
            }

            /// <summary>
            /// Promedio general
            /// </summary>
            /// <returns></returns>
            public double GetAvg()
            {
                List<LanguageRecord> languageRecord = GetLanguageRecord();

                var lastInitDate = GetStartDate(languageRecord);

                if (lastInitDate == null) return 0;

                languageRecord = languageRecord.Where(w => w.StartDate >= lastInitDate.StartDate).ToList();

                double? average = languageRecord.Average(w => w.N51 ?? w.N50);
                var avg = average ?? 0;

                return Math.Round(avg, 0);
            }

            /// <summary>
            /// Cálculo de horas "cursadas" según idioma
            /// </summary>
            /// <returns></returns>
            public int GetTotalHours()
            {
                var hoursByCycle = Profile.Language.LanguageId == _cic.INGLES ? 40 : 30;
                return hoursByCycle * int.Parse(Profile.Cycle);
            }

            private void SyncDate()
            {
                List<LanguageRecord> languageRecord = GetLanguageRecord();
                StartDate = GetStartDate(languageRecord).StartDate;
                EndDate = GetEndDate(languageRecord).StartDate;
            }

            /// <summary>
            /// Historial de ciclos aprobados por idioma
            /// </summary>
            /// <returns></returns>
            private List<LanguageRecord> GetLanguageRecord()
            {
                List<DBOtblNotasEXT> record =
                    StudentLanguageRecord.GetApprovedCyclesByLanguageId(Pidm,
                        Profile.Language.LanguageId);
                List<LanguageRecord> languageRecord = record.Select(s => (LanguageRecord) s).ToList();

                return languageRecord;
            }

            /// <summary>
            /// Fecha de inicio de estudios en el CIC según idioma
            /// </summary>
            /// <param name="languageRecord"></param>
            /// <returns></returns>
            private static LanguageRecord GetStartDate(List<LanguageRecord> languageRecord)
            {
                languageRecord = languageRecord
                    .Where(w => w.Cycle.CycleNumber == 1 ||
                                w.Classroom.Substring(4, 1) == _cic.MOD_EXAMEN_CLASIFICACION ||
                                w.Classroom.Substring(4, 1) == _cic.MOD_EXAMEN_SUFICIENCIA ||
                                w.Classroom.Substring(4, 1) == _cic.MOD_CONVALIDACION)
                    .OrderByDescending(o => o.StartDate).ToList();

                return languageRecord.FirstOrDefault();
            }

            /// <summary>
            /// Fecha de fin de estudios en el CIC según idioma
            /// </summary>
            /// <param name="languageRecord"></param>
            /// <returns></returns>
            private static LanguageRecord GetEndDate(List<LanguageRecord> languageRecord)
            {
                var dboContext = new DBOContext();
                var lastCycle = languageRecord.OrderByDescending(o => o.StartDate).FirstOrDefault();
                var classroomInfo = dboContext.tbl_seccion_c.FirstOrDefault(w => w.IDSeccionC == lastCycle.Classroom);

                if (lastCycle != null) lastCycle.StartDate = classroomInfo?.FecFin ?? DateTime.Now;

                return lastCycle;
            }
        }
    }
}
