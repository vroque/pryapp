﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using ACPermanence.DataBases.DBUCCI.DBO;

namespace ACBusiness.LanguagesCenter
{
    /// <summary>
    /// Historial de notas de idiomas
    /// </summary>
    public class LanguageRecord
    {
        #region Properties

        /// <summary>
        /// IDSede
        /// </summary>
        public string Headquarters { get; set; }

        /// <summary>
        /// IDPerAcad
        /// </summary>
        public string Term { get; set; }

        /// <summary>
        /// IDSeccionC
        /// </summary>
        public string Classroom { get; set; }

        /// <summary>
        /// FecInic o fechaexamen, según corresponda
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// IDEscuela
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// ciclo
        /// </summary>
        public Cycle Cycle { get; set; }

        /// <summary>
        /// fechamat
        /// </summary>
        public DateTime? EnrolledDate { get; set; }

        public int? N17 { get; set; }
        public int? N18 { get; set; }
        public int? N19 { get; set; }
        public int? N20 { get; set; }
        public int? N40 { get; set; }
        public int? N41 { get; set; }
        public int? N42 { get; set; }
        public int? N43 { get; set; }
        public int? N44 { get; set; }
        public int? N45 { get; set; }
        public int? N46 { get; set; }
        public int? N47 { get; set; }
        public int? N48 { get; set; }
        public int? N49 { get; set; }
        public int? N50 { get; set; }
        public int? N51 { get; set; }
        public int? Style { get; set; }
        public string TestCycle { get; set; }
        public string TestComment { get; set; }
        public string StudentId { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Historial de notas de un estudiante del CIC
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public static async Task<List<LanguageRecord>> GetByStudentAsync(string studentId)
        {
            var pidm = Student.GetStudentPidm(studentId);

            return pidm == 0 ? new List<LanguageRecord>() : await GetByStudentPidmAsync(pidm);
        }

        /// <summary>
        /// Historial de notas de un estudiante del CIC
        /// </summary>
        /// <param name="pidm">Student PIDM</param>
        /// <returns></returns>
        public static async Task<List<LanguageRecord>> GetByStudentPidmAsync(decimal pidm)
        {
            var dboContext = new DBOContext();

            List<string> studentCodeList = Student.GetViewAlumnoBasico(pidm).Select(s => s.IDAlumno).ToList();
            List<DBOtblNotasEXT> marks = await dboContext.tbl_notas_ext
                .Where(w => studentCodeList.Contains(w.IDAlumno)).ToListAsync();
            List<LanguageRecord> languageRecordList = marks.Select<DBOtblNotasEXT, LanguageRecord>(s => s).ToList();

            return languageRecordList;
        }

        /// <summary>
        /// Información de aulas del Centro de Idiomas
        /// </summary>
        /// <param name="classroomList"></param>
        /// <param name="studentPidm"></param>
        /// <returns></returns>
        public static List<LanguageRecord> GetByClassrooms(IEnumerable<string> classroomList, decimal studentPidm)
        {
            var dboContext = new DBOContext();
            List<string> studentIdList = Academic.Student.GetAllStudentId(studentPidm);
            var recordByClassroomList = new List<DBOtblNotasEXT>();

            foreach (var classroom in classroomList)
            {
                List<DBOtblNotasEXT> recordByClassroom = dboContext.tbl_notas_ext
                    .Where(w => studentIdList.Contains(w.IDAlumno) && w.IDSeccionC == classroom).ToList();
                if (recordByClassroom.Any())
                {
                    recordByClassroomList.Add(recordByClassroom.FirstOrDefault());
                }
            }

            return recordByClassroomList.Select<DBOtblNotasEXT, LanguageRecord>(s => s).ToList();
        }

        /// <summary>
        /// Estilo de sección.
        /// Esto se usa para plantilla de certificado de estudios.
        /// </summary>
        /// <param name="classroom">Sección</param>
        /// <returns></returns>
        public static int GetStyle(string classroom)
        {
            if (classroom == null) return 0;

            var dboContext = new DBOContext();
            List<DBOtblSeccionCDocenteEXT> style = dboContext.tbl_seccion_docente_ext
                .Where(w => w.IDSeccionC == classroom).ToList();

            return style.Any() ? style.FirstOrDefault()?.IDEstilo ?? 0 : 0;
        }

        #endregion Methods

        #region Operators

        public static implicit operator LanguageRecord(DBOtblNotasEXT notasExt)
        {
            if (notasExt == null) return null;
            var dboContext = new DBOContext();

            var languageRecord = new LanguageRecord
            {
                Headquarters = notasExt.IDsede,
                Term = notasExt.IDPerAcad,
                Classroom = notasExt.IDSeccionC,
                StartDate = notasExt.ciclo == null ? notasExt.FecInic : notasExt.fechaexamen,
                LanguageId = notasExt.IDEscuela,
                Cycle = new Cycle(notasExt.IDSeccionC, notasExt.ciclo),
                EnrolledDate = notasExt.fechamat,
                N17 = notasExt.n17,
                N18 = notasExt.n18,
                N19 = notasExt.n19,
                N20 = notasExt.n20,
                N40 = notasExt.n40,
                N41 = notasExt.n41,
                N42 = notasExt.n42,
                N43 = notasExt.n43,
                N44 = notasExt.n44,
                N45 = notasExt.n45,
                N46 = notasExt.n46,
                N47 = notasExt.n47,
                N48 = notasExt.n48,
                N49 = notasExt.n49,
                N50 = notasExt.n50,
                N51 = notasExt.n51,
                Style = GetStyle(notasExt.IDSeccionC),
                TestCycle = notasExt.ciclo,
                TestComment = notasExt.obs,
                StudentId = notasExt.IDAlumno,
            };

            var classroomInfo = dboContext.tbl_seccion_c.FirstOrDefault(w => w.IDSeccionC == notasExt.IDSeccionC);

            languageRecord.StartDate = notasExt.ciclo == null
                ? classroomInfo.FecInicReal ?? classroomInfo.FecInic
                : notasExt.fechaexamen;

            return languageRecord;
        }

        #endregion Operators
    }
}
