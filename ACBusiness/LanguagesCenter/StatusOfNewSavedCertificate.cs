﻿namespace ACBusiness.LanguagesCenter
{
    /// <summary>
    /// Estructura para info sobre el estado de guardado de un registro en CIC.Certificates
    /// </summary>
    public class StatusOfNewSavedCertificate
    {
        public int CertificateId { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}