using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;

namespace ACBusiness.LanguagesCenter
{
    public class AcademicProfile
    {
        #region Properties

        public string StudentId { get; set; }
        public Language Language { get; set; }
        public string Cycle { get; set; }
        public DateTime StartDateAtCic { get; set; }

        #endregion Properties

        #region Constructors

        public AcademicProfile()
        {
        }

        public AcademicProfile(decimal studentPidm, string languageId)
        {
            Task<LanguageRecord> studentLanguageLevelTask =
                Task.Run(async () => await Student.GetLanguageLevelByLanguageAsync(studentPidm, languageId));
            var studentLanguageLevel = studentLanguageLevelTask.Result;

            if (studentLanguageLevel == null) return;

            StudentId = studentLanguageLevel.StudentId;
            Language = new Language(studentLanguageLevel.LanguageId);
            Cycle = studentLanguageLevel.Cycle.CycleNumber.ToString();
            StartDateAtCic = Student.GetStartDateAtCicByLanguageId(studentPidm, languageId);
        }

        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// Profile list
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public static List<AcademicProfile> GetProfileList(decimal pidm)
        {
            var academicProfileList = new List<AcademicProfile>();
            
            Task<List<AcademicProfile>> studentLanguageLevelListTask = Task.Run(async () => await Student.GetLanguageLevelAsync(pidm));
            List<AcademicProfile> studentLanguageLevelList = studentLanguageLevelListTask.Result;

            return studentLanguageLevelList.Any() ? studentLanguageLevelList : academicProfileList;
        }

        #endregion Public Methods
    }
}