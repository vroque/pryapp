﻿using ACPermanence.Contexts.BDUCCI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.LanguagesCenter
{
    public class Enrollment
    {
        public string rpta { get; set; }

        public string ejecutarMatricula(decimal pidm,string seccion)
        {
            DBOContext db = new DBOContext();
            return db.sp_ejecutar_matricula_cic(pidm, seccion);
        }
        public string comprobarMatricula(decimal pidm, string escuela)
        {
            DBOContext db = new DBOContext();
            return db.sp_comprobar_matricula_cic(pidm, escuela);
        }
    }
}
