﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.BDUCCI;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _corp = ACTools.Constants.InstitucionalConstants;

namespace ACBusiness.LanguagesCenter
{
    public class PaymentStatus
    {
        #region Properties

        private readonly DBOContext _dboContext = new DBOContext();

        #endregion Properties

        #region Methods

        /// <summary>
        /// Estado sobre el pago de Constancia de Idioma Extranjero. 
        /// </summary>
        /// <param name="studentId">Código de estudiante UC</param>
        /// <param name="languageId">Código de idioma</param>
        /// <returns></returns>
        public async Task<bool> IsPaidProofOfForeignLanguageAsync(string studentId, string languageId)
        {
            var studentIdList = Academic.Student.GetAllStudentId(studentId);
            var conceptId = _cic.CAIE_CONCEPTS_BY_LANGUAGE_ID[languageId];
            var tblCtaCorriente = await _dboContext.tbl_cta_corriente
                .Where(w => w.IDEscuela == "GYT" &&
                            w.IDSeccionC.Substring(0, 2) == "BA" &&
                            w.IDConcepto == conceptId &&
                            w.Abono > 0 &&
                            w.IDDependencia == _corp.UC &&
                            studentIdList.Contains(w.IDAlumno)).ToListAsync();

            return tblCtaCorriente.Any();
        }

        #endregion Methods
    }
}