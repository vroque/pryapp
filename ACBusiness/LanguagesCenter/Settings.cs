﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CIC;
using Newtonsoft.Json;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter
{
    public class Settings
    {
        #region Properties

        public int Id { get; set; }

        [JsonIgnore]
        public string Abbr { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        [JsonIgnore]
        public int SettingTypeId { get; set; }

        public bool Active { get; set; }

        [JsonIgnore]
        public DateTime UpdateDate { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Información sobre Nivel de Idioma que se imprimirá en la Constancia de Idioma Extranjero
        /// </summary>
        /// <returns></returns>
        public static async Task<Settings> GetCaieLanguageLevelName()
        {
            var nexoContext = new NEXOContext();
            var info = await nexoContext.cic_settings
                .FirstOrDefaultAsync(w => w.SettingTypeId == _cic.CaieLanguageLevel && w.Active);

            return info;
        }

        /// <summary>
        /// Actualiza el Nivel de Idioma que se imprimirá en la Constancia de Idioma Extranjero
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        /// <exception cref="DbUpdateException"></exception>
        public static async Task<Settings> SaveCaieLanguageLevelName(Settings settings)
        {
            if (settings == null) return null;

            var nexoContext = new NEXOContext();
            var info = nexoContext.cic_settings
                .FirstOrDefault(w => w.SettingTypeId == _cic.CaieLanguageLevel && w.Id == settings.Id && w.Active);

            if (info == null) return null;

            info.Name = settings.Name;

            var rpta = await nexoContext.SaveChangesAsync();

            if (rpta == 1) return info;

            throw new DbUpdateException("Error al actualizar nombre de nivel de idioma para CAIE");
        }

        #endregion Methods

        #region Implicit Operators

        public static implicit operator Settings(CICSettings cicSettings)
        {
            if (cicSettings == null) return null;

            var settings = new Settings
            {
                Id = cicSettings.Id,
                Abbr = cicSettings.Abbr,
                Name = cicSettings.Name,
                Description = cicSettings.Description,
                SettingTypeId = cicSettings.SettingTypeId,
                Active = cicSettings.Active,
                UpdateDate = cicSettings.UpdateDate
            };

            return settings;
        }

        #endregion Implicit Operators
    }
}