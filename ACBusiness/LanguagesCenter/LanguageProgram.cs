﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACBusiness.LanguagesCenter
{
    public class LanguageProgram
    {
        #region Properties

        public int LanguageProgramId { get; set; }
        public string LanguageProgramName { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Get language program by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static async Task<LanguageProgram> GetByIdAsync(int id)
        {
            using (var nexoContext = new NEXOContext())
            {
                var languageProgram = await nexoContext.cic_language_program.FirstOrDefaultAsync(w => w.Id == id);

                if (languageProgram == null) return null;

                var programs = new LanguageProgram
                {
                    LanguageProgramId = languageProgram.Id,
                    LanguageProgramName = languageProgram.LanguageProgramName
                };

                return programs;
            }
        }

        /// <summary>
        /// Lista de todos los Programas CIC.
        /// Ejemplo: Regular, Virtual, Escolares...
        /// </summary>
        /// <returns>Programas CIC</returns>
        public static async Task<List<LanguageProgram>> GetAllAsync()
        {
            using (var nexoContext = new NEXOContext())
            {
                List<LanguageProgram> programs = nexoContext.cic_language_level
                    .Where(w => w.Active)
                    .Select(s => new LanguageProgram()
                    {
                        LanguageProgramId = s.LanguageProgramId,
                    })
                    .Distinct()
                    .OrderBy(o => o.LanguageProgramId)
                    .ToList();

                foreach (var program in programs)
                {
                    var prg = await GetByIdAsync(program.LanguageProgramId);
                    program.LanguageProgramName = prg.LanguageProgramName;
                }
                
                return programs;
            }
        }

        #endregion Methods
    }
}
