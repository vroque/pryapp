using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Incident
{
    public class IncidentRequestSource
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public IncidentRequestSource()
        {
        }

        public IncidentRequestSource(IncidentRequestSource incidentRequestSource)
        {
            Id = incidentRequestSource.Id;
            Name = incidentRequestSource.Name;
            Description = incidentRequestSource.Description;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all incident request source
        /// </summary>
        /// <returns></returns>
        public static async Task<List<IncidentRequestSource>> GetAllAsync()
        {
            using (var nexoContext = new NEXOContext())
            {
                List<CAUIncidentRequestSource> requestSourceList =
                    await nexoContext.cau_incident_request_source.ToListAsync();

                return requestSourceList.Select(s => (IncidentRequestSource) s).OrderBy(o => o.Name).ToList();
            }
        }

        /// <summary>
        /// Get incident request source info
        /// </summary>
        /// <param name="id">Request source id</param>
        /// <returns></returns>
        public static async Task<IncidentRequestSource> GetByIdAsync(int id)
        {
            using (var nexoContext = new NEXOContext())
            {
                var requestSource = await nexoContext.cau_incident_request_source
                    .FirstOrDefaultAsync(w => w.IncidentRequestSourceId == id);

                return (IncidentRequestSource) requestSource;
            }
        }

        /// <summary>
        /// Create new request source
        /// </summary>
        /// <returns></returns>
        public async Task<IncidentRequestSource> CreateAsync()
        {
            var r = new CAUIncidentRequestSource();
            Compose(ref r);

            if (r == null) return null;

            _nexoContext.cau_incident_request_source.Add(r);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (IncidentRequestSource) r : null;
        }

        /// <summary>
        /// Update an incident request source
        /// </summary>
        /// <returns></returns>
        public async Task<IncidentRequestSource> UpdateAsync()
        {
            var r = await _nexoContext.cau_incident_request_source
                .FirstOrDefaultAsync(w => w.IncidentRequestSourceId == Id);

            if (r == null) return null;

            Compose(ref r);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (IncidentRequestSource) r : null;
        }

        private void Compose(ref CAUIncidentRequestSource cauIncidentRequestSource)
        {
            if (cauIncidentRequestSource == null) return;

            cauIncidentRequestSource.IncidentRequestSourceId = Id;
            cauIncidentRequestSource.IncidentRequestSourceName = Name;
            cauIncidentRequestSource.IncidentRequestSourceDescription = Description;
        }

        #endregion Methods

        #region Operators

        public static explicit operator IncidentRequestSource(CAUIncidentRequestSource cauIncidentRequestSource)
        {
            if (cauIncidentRequestSource == null) return null;

            var requestSource = new IncidentRequestSource
            {
                Id = cauIncidentRequestSource.IncidentRequestSourceId,
                Name = cauIncidentRequestSource.IncidentRequestSourceName,
                Description = cauIncidentRequestSource.IncidentRequestSourceDescription
            };

            return requestSource;
        }

        #endregion Operators
    }
}
