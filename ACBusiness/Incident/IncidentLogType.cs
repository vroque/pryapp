using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Incident
{
    public class IncidentLogType
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public IncidentLogType()
        {
        }

        public IncidentLogType(IncidentLogType incidentLogType)
        {
            Id = incidentLogType.Id;
            Name = incidentLogType.Name;
            Description = incidentLogType.Description;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all incident log types
        /// </summary>
        /// <returns></returns>
        public async Task<List<IncidentLogType>> GetAllAsync()
        {
            List<CAUIncidentLogType> incidentLogTypeList = await _nexoContext.cau_incident_log_type.ToListAsync();

            return incidentLogTypeList.Select(s => (IncidentLogType) s).ToList();
        }

        /// <summary>
        /// Get incident log type info
        /// </summary>
        /// <param name="id">Incident log type id</param>
        /// <returns></returns>
        public static async Task<IncidentLogType> GetByIdAsync(int id)
        {
            var nexoContext = new NEXOContext();
            var type = await nexoContext.cau_incident_log_type.FirstOrDefaultAsync(w => w.IncidentLogTypeId == id);

            return (IncidentLogType) type;
        }

        /// <summary>
        /// Create new incident log type
        /// </summary>
        /// <returns></returns>
        public async Task<IncidentLogType> CreateAsync()
        {
            var t = new CAUIncidentLogType();
            Compose(ref t);

            if (t == null) return null;

            _nexoContext.cau_incident_log_type.Add(t);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (IncidentLogType) t : null;
        }

        /// <summary>
        /// Update an incident log type
        /// </summary>
        /// <returns></returns>
        public async Task<IncidentLogType> UpdateAsync()
        {
            var t = await _nexoContext.cau_incident_log_type.FirstOrDefaultAsync(w => w.IncidentLogTypeId == Id);

            if (t == null) return null;

            Compose(ref t);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (IncidentLogType) t : null;
        }

        private void Compose(ref CAUIncidentLogType cauIncidentLogType)
        {
            if (cauIncidentLogType == null) return;

            cauIncidentLogType.IncidentLogTypeId = Id;
            cauIncidentLogType.Name = Name;
            cauIncidentLogType.Description = Description;
        }

        #endregion Methods

        #region Operators

        public static explicit operator IncidentLogType(CAUIncidentLogType cauIncidentLogType)
        {
            if (cauIncidentLogType == null) return null;

            var incidentLogType = new IncidentLogType
            {
                Id = cauIncidentLogType.IncidentLogTypeId,
                Name = cauIncidentLogType.Name,
                Description = cauIncidentLogType.Description
            };

            return incidentLogType;
        }

        #endregion Operators
    }
}
