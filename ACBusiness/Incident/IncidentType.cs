using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Incident
{
    public class IncidentType
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public IncidentType()
        {
        }

        public IncidentType(IncidentType incidentType)
        {
            Id = incidentType.Id;
            Name = incidentType.Name;
            Description = incidentType.Description;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all incident types
        /// </summary>
        /// <returns></returns>
        public async Task<List<IncidentType>> GetAllAsync()
        {
            List<CAUIncidentType> incidentTypeList = await _nexoContext.cau_incident_type.ToListAsync();

            return incidentTypeList.Select(s => (IncidentType) s).OrderBy(o => o.Name).ToList();
        }

        /// <summary>
        /// Get incident type info
        /// </summary>
        /// <param name="id">Incident type id</param>
        /// <returns></returns>
        public static async Task<IncidentType> GetByIdAsync(int id)
        {
            var nexoContext = new NEXOContext();
            var type = await nexoContext.cau_incident_type.FirstOrDefaultAsync(w => w.IncidentTypeId == id);

            return (IncidentType) type;
        }

        /// <summary>
        /// Create new incident type
        /// </summary>
        /// <returns></returns>
        public async Task<IncidentType> CreateAsync()
        {
            var t = new CAUIncidentType();
            Compose(ref t);

            if (t == null) return null;

            _nexoContext.cau_incident_type.Add(t);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (IncidentType) t : null;
        }

        /// <summary>
        /// Update an incident type
        /// </summary>
        /// <returns></returns>
        public async Task<IncidentType> UpdateAsync()
        {
            var t = await _nexoContext.cau_incident_type.FirstOrDefaultAsync(w => w.IncidentTypeId == Id);

            if (t == null) return null;

            Compose(ref t);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (IncidentType) t : null;
        }

        private void Compose(ref CAUIncidentType cauIncidentType)
        {
            if (cauIncidentType == null) return;

            cauIncidentType.IncidentTypeId = Id;
            cauIncidentType.IncidentTypeName = Name;
            cauIncidentType.IncidentTypeDescription = Description;
        }

        #endregion Methods

        #region Operators

        public static explicit operator IncidentType(CAUIncidentType cauIncidentType)
        {
            if (cauIncidentType == null) return null;

            var incidentType = new IncidentType
            {
                Id = cauIncidentType.IncidentTypeId,
                Name = cauIncidentType.IncidentTypeName,
                Description = cauIncidentType.IncidentTypeDescription
            };

            return incidentType;
        }

        #endregion Operators
    }
}
