using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Incident
{
    public class IncidentStatus
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public IncidentStatus()
        {
        }

        public IncidentStatus(IncidentStatus incidentStatus)
        {
            Id = incidentStatus.Id;
            Name = incidentStatus.Name;
            Description = incidentStatus.Description;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all incident statuses
        /// </summary>
        /// <returns></returns>
        public async Task<List<IncidentStatus>> GetAllAsync()
        {
            List<CAUIncidentStatus> incidentStatusList = await _nexoContext.cau_incident_status.ToListAsync();

            return incidentStatusList.Select(s => (IncidentStatus) s).ToList();
        }

        /// <summary>
        /// Get incident status info
        /// </summary>
        /// <param name="id">Incident status id</param>
        /// <returns></returns>
        public static async Task<IncidentStatus> GetByIdAsync(int id)
        {
            var nexoContext = new NEXOContext();
            var status = await nexoContext.cau_incident_status.FirstOrDefaultAsync(w => w.IncidentStatusId == id);

            return (IncidentStatus) status;
        }

        /// <summary>
        /// Create new incident status
        /// </summary>
        /// <returns></returns>
        public async Task<IncidentStatus> CreateAsync()
        {
            var s = new CAUIncidentStatus();
            Compose(ref s);

            if (s == null) return null;

            _nexoContext.cau_incident_status.Add(s);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (IncidentStatus) s : null;
        }

        /// <summary>
        /// Update an incident status
        /// </summary>
        /// <returns></returns>
        public async Task<IncidentStatus> UpdateAsync()
        {
            var s = await _nexoContext.cau_incident_status.FirstOrDefaultAsync(w => w.IncidentStatusId == Id);

            if (s == null) return null;

            Compose(ref s);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (IncidentStatus) s : null;
        }

        private void Compose(ref CAUIncidentStatus cauIncidentStatus)
        {
            if (cauIncidentStatus == null) return;

            cauIncidentStatus.IncidentStatusId = Id;
            cauIncidentStatus.IncidentStatusName = Name;
            cauIncidentStatus.IncidentStatusDescription = Description;
        }

        #endregion Methods

        #region Operators

        public static explicit operator IncidentStatus(CAUIncidentStatus cauIncidentStatus)
        {
            if (cauIncidentStatus == null) return null;

            var incidentStatus = new IncidentStatus
            {
                Id = cauIncidentStatus.IncidentStatusId,
                Name = cauIncidentStatus.IncidentStatusName,
                Description = cauIncidentStatus.IncidentStatusDescription
            };

            return incidentStatus;
        }

        #endregion Operators
    }
}
