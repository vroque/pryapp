using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Incident
{
    public class SubCategory
    {
        #region Properties

        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public SubCategory()
        {
        }

        public SubCategory(SubCategory subCategory)
        {
            Id = subCategory.Id;
            ParentId = subCategory.ParentId;
            Name = subCategory.Name;
            Description = subCategory.Description;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all subcategories
        /// </summary>
        /// <returns></returns>
        public static async Task<List<SubCategory>> GetAllAsync()
        {
            using (var nexoContext = new NEXOContext())
            {
                List<CAUIncidentSubCategory>
                    subCategoryList = await nexoContext.cau_incident_sub_category.ToListAsync();

                return subCategoryList.Select(s => (SubCategory) s).ToList();
            }
        }

        /// <summary>
        /// Get subcategory info
        /// </summary>
        /// <param name="id">Subcategory id</param>
        /// <returns></returns>
        public static async Task<SubCategory> GetByIdAsync(int id)
        {
            using (var nexoContext = new NEXOContext())
            {
                var subCategory = await nexoContext.cau_incident_sub_category
                    .FirstOrDefaultAsync(w => w.IncidentSubCategoryId == id);

                return (SubCategory) subCategory;
            }
        }

        /// <summary>
        /// Get all subcategories by category id
        /// </summary>
        /// <param name="categoryId">Category id</param>
        /// <returns></returns>
        public static async Task<List<SubCategory>> GetAllByCategoryIdAsync(int categoryId)
        {
            using (var nexoContext = new NEXOContext())
            {
                List<CAUIncidentSubCategory> subcategories = await nexoContext.cau_incident_sub_category
                    .Where(w => w.IncidentCategoryId == categoryId).ToListAsync();

                return subcategories.Select(s => (SubCategory) s).OrderBy(o => o.Name).ToList();
            }
        }

        /// <summary>
        /// Create new subcategory
        /// </summary>
        /// <returns></returns>
        public async Task<SubCategory> CreateAsync()
        {
            var s = new CAUIncidentSubCategory();
            Compose(ref s);

            if (s == null) return null;

            _nexoContext.cau_incident_sub_category.Add(s);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (SubCategory) s : null;
        }

        /// <summary>
        /// Update a subcategory
        /// </summary>
        /// <returns></returns>
        public async Task<SubCategory> UpdateAsync()
        {
            var s = await _nexoContext.cau_incident_sub_category.FirstOrDefaultAsync(w =>
                w.IncidentSubCategoryId == Id);

            if (s == null) return null;

            Compose(ref s);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (SubCategory) s : null;
        }

        private void Compose(ref CAUIncidentSubCategory cauIncidentSubCategory)
        {
            if (cauIncidentSubCategory == null) return;

            cauIncidentSubCategory.IncidentSubCategoryId = Id;
            cauIncidentSubCategory.IncidentCategoryId = ParentId;
            cauIncidentSubCategory.IncidentSubCategoryName = Name;
            cauIncidentSubCategory.IncidentSubCategoryDescription = Description;
        }

        #endregion Methods

        #region Operators

        public static explicit operator SubCategory(CAUIncidentSubCategory cauIncidentSubCategory)
        {
            if (cauIncidentSubCategory == null) return null;

            var subCategory = new SubCategory
            {
                Id = cauIncidentSubCategory.IncidentSubCategoryId,
                ParentId = cauIncidentSubCategory.IncidentCategoryId,
                Name = cauIncidentSubCategory.IncidentSubCategoryName,
                Description = cauIncidentSubCategory.IncidentSubCategoryDescription
            };

            return subCategory;
        }

        #endregion Operators
    }
}
