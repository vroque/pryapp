using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Adryan;
using ACBusiness.Personal;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using _incident = ACTools.Constants.IncidentConstants;

namespace ACBusiness.Incident
{
    public class IncidentLog
    {
        #region Properties

        public long Id { get; set; }
        public long IncidentId { get; set; }
        public int LogTypeId { get; set; }
        public decimal AuthorPidm { get; set; }
        public string AuthorName { get; set; }
        public string TeamId { get; set; }
        public string TeamName { get; set; }
        public string Comment { get; set; }
        public DateTime PublicationDate { get; set; }
        public bool IsPublic { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public IncidentLog()
        {
        }

        public IncidentLog(IncidentLog incidentLog)
        {
            Id = incidentLog.Id;
            IncidentId = incidentLog.IncidentId;
            LogTypeId = incidentLog.LogTypeId;
            AuthorPidm = incidentLog.AuthorPidm;
            AuthorName = incidentLog.AuthorName;
            TeamId = incidentLog.TeamId;
            TeamName = incidentLog.TeamName;
            Comment = incidentLog.Comment;
            PublicationDate = incidentLog.PublicationDate;
            IsPublic = incidentLog.IsPublic;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all incident logs
        /// </summary>
        /// <returns></returns>
        public static async Task<List<IncidentLog>> GetAllAsync()
        {
            using (var nexoContext = new NEXOContext())
            {
                List<CAUIncidentLog> incidentLogList = await nexoContext.cau_incident_log.ToListAsync();

                return incidentLogList.Select(s => (IncidentLog) s).ToList();
            }
        }

        /// <summary>
        /// Get incident log info
        /// </summary>
        /// <param name="incidentLogId">Incident log id</param>
        /// <returns></returns>
        public static async Task<IncidentLog> GetByIdAsync(long incidentLogId)
        {
            using (var nexoContext = new NEXOContext())
            {
                var incidentLog =
                    await nexoContext.cau_incident_log.FirstOrDefaultAsync(w => w.IncidentLogId == incidentLogId);

                return (IncidentLog) incidentLog;
            }
        }

        /// <summary>
        /// Create a new log
        /// </summary>
        /// <param name="incidentId">Incident id</param>
        /// <param name="taskId">Task id</param>
        /// <param name="description">Log description</param>
        /// <param name="userPidm">User PIDM</param>
        /// <returns></returns>
        public static async Task<IncidentLog> NewIncidentLogAsync(long incidentId, int taskId, string description,
            decimal userPidm)
        {
            using (var nexoContext = new NEXOContext())
            {
                var incident = await nexoContext.cau_incident.FirstOrDefaultAsync(w => w.IncidentId == incidentId);

                if (incident == null) return null;

                var employee = await Employee.GetByPidmAndCompanyIdAsync(userPidm);
                var publicTasks = new List<int> {_incident.LogCreate, _incident.LogSolution, _incident.LogClose};

                var newIncidentLog = new IncidentLog
                {
                    IncidentId = incident.IncidentId,
                    LogTypeId = taskId,
                    AuthorPidm = userPidm,
                    AuthorName = $"{employee.apellido_paterno} {employee.apellido_materno} {employee.nombre}",
                    TeamId = employee.unidad_funcional_organica,
                    TeamName = employee.nombre_unidad_funcional,
                    Comment = description,
                    PublicationDate = DateTime.Now,
                    IsPublic = publicTasks.Contains(taskId)
                };

                var response = await newIncidentLog.CreateAsync();

                return response;
            }
        }

        /// <summary>
        /// Get all incident log by incident
        /// </summary>
        /// <param name="incidentId">Incident id</param>
        /// <returns></returns>
        public static async Task<List<IncidentLog>> GetAllByIncidentId(long incidentId)
        {
            using (var nexoContext = new NEXOContext())
            {
                List<CAUIncidentLog> incidentLogListByIncident =
                    await nexoContext.cau_incident_log.Where(w => w.IncidentId == incidentId).ToListAsync();

                return incidentLogListByIncident.Select(s => (IncidentLog) s).OrderBy(o => o.PublicationDate).ToList();
            }
        }

        /// <summary>
        /// Create new incident log
        /// </summary>
        /// <returns></returns>
        public async Task<IncidentLog> CreateAsync()
        {
            PublicationDate = DateTime.Now;
            var persona = new Person(AuthorPidm);
            AuthorName = persona.full_name;
            var i = new CAUIncidentLog();
            Compose(ref i);
            if (i == null) return null;

            _nexoContext.cau_incident_log.Add(i);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (IncidentLog) i : null;
        }

        /// <summary>
        /// Update an incident log
        /// </summary>
        /// <returns></returns>
        public async Task<IncidentLog> UpdateAsync()
        {
            var i = await _nexoContext.cau_incident_log.FirstOrDefaultAsync(w => w.IncidentLogId == Id);

            if (i == null) return null;

            Compose(ref i);

            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (IncidentLog) i : null;
        }

        private void Compose(ref CAUIncidentLog cauIncidentLog)
        {
            if (cauIncidentLog == null) return;

            cauIncidentLog.IncidentLogId = Id;
            cauIncidentLog.IncidentId = IncidentId;
            cauIncidentLog.IncidentLogTypeId = LogTypeId;
            cauIncidentLog.IncidentLogAuthorPidm = AuthorPidm;
            cauIncidentLog.IncidentLogAuthorName = AuthorName;
            cauIncidentLog.IncidentLogTeamId = TeamId;
            cauIncidentLog.IncidentLogTeamName = TeamName;
            cauIncidentLog.IncidentLogComment = Comment;
            cauIncidentLog.IncidentLogPublicationDate = PublicationDate;
            cauIncidentLog.IncidentLogIsPublic = IsPublic;
        }

        #endregion Methods

        #region Operators

        public static explicit operator IncidentLog(CAUIncidentLog cauIncidentLog)
        {
            if (cauIncidentLog == null) return null;

            var incidentLog = new IncidentLog
            {
                Id = cauIncidentLog.IncidentId,
                IncidentId = cauIncidentLog.IncidentId,
                LogTypeId = cauIncidentLog.IncidentLogTypeId,
                AuthorPidm = cauIncidentLog.IncidentLogAuthorPidm,
                AuthorName = cauIncidentLog.IncidentLogAuthorName,
                TeamId = cauIncidentLog.IncidentLogTeamId,
                TeamName = cauIncidentLog.IncidentLogTeamName,
                Comment = cauIncidentLog.IncidentLogComment,
                PublicationDate = cauIncidentLog.IncidentLogPublicationDate,
                IsPublic = cauIncidentLog.IncidentLogIsPublic
            };

            return incidentLog;
        }

        #endregion Operators
    }
}
