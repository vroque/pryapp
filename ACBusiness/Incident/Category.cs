using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Incident
{
    /// <summary>
    /// Incident category
    /// </summary>
    public class Category
    {
        #region Properties

        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public Category()
        {
        }

        public Category(Category category)
        {
            Id = category.Id;
            ParentId = category.ParentId;
            Name = category.Name;
            Description = category.Description;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all categories
        /// </summary>
        /// <returns></returns>
        public async Task<List<Category>> GetAllAsync()
        {
            List<CAUIncidentCategory> categoryList = await _nexoContext.cau_incident_category.ToListAsync();

            return categoryList.Select(s => (Category) s).ToList();
        }

        /// <summary>
        /// Get all categories without a parent category
        /// </summary>
        /// <returns></returns>
        public async Task<List<Category>> GetAllWithoutParentCategoryAsync()
        {
            List<CAUIncidentCategory> categoriesWithoutParentCategory = await _nexoContext.cau_incident_category
                .Where(w => w.IncidentParentCategoryId == null).ToListAsync();

            return categoriesWithoutParentCategory.Select(s => (Category) s).OrderBy(o => o.Name).ToList();
        }

        /// <summary>
        /// Get all categories with a parent category
        /// </summary>
        /// <returns></returns>
        public async Task<List<Category>> GetAllWithParentCategoryAsync()
        {
            List<CAUIncidentCategory> categoriesWithParentCategory = await _nexoContext.cau_incident_category
                .Where(w => w.IncidentParentCategoryId != null).ToListAsync();

            return categoriesWithParentCategory.Select(s => (Category) s).OrderBy(o => o.Name).ToList();
        }

        /// <summary>
        /// Get category info
        /// </summary>
        /// <param name="id">Category id</param>
        /// <returns></returns>
        public static async Task<Category> GetByIdAsync(int id)
        {
            var nexoContext = new NEXOContext();
            var category =
                await nexoContext.cau_incident_category.FirstOrDefaultAsync(w => w.IncidentCategoryId == id);

            return (Category) category;
        }

        /// <summary>
        /// Get parent category info of a category
        /// </summary>
        /// <returns></returns>
        public async Task<Category> GetParentCategoryAsync()
        {
            var parentCategory =
                await _nexoContext.cau_incident_category.FirstOrDefaultAsync(w => w.IncidentCategoryId == ParentId);

            return (Category) parentCategory;
        }

        /// <summary>
        /// Get all child categories of a category
        /// </summary>
        /// <returns></returns>
        public async Task<List<Category>> GetChildCategoriesAsync()
        {
            List<CAUIncidentCategory> childCategories =
                await _nexoContext.cau_incident_category.Where(w => w.IncidentParentCategoryId == Id).ToListAsync();

            return childCategories.Select(s => (Category) s).OrderBy(o => o.Name).ToList();
        }

        /// <summary>
        /// Create new Category
        /// </summary>
        /// <returns></returns>
        public async Task<Category> CreateAsync()
        {
            var c = new CAUIncidentCategory();
            Compose(ref c);

            if (c == null) return null;

            _nexoContext.cau_incident_category.Add(c);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (Category) c : null;
        }

        /// <summary>
        /// Update a category
        /// </summary>
        /// <returns></returns>
        public async Task<Category> UpdateAsync()
        {
            var c = await _nexoContext.cau_incident_category.FirstOrDefaultAsync(w => w.IncidentCategoryId == Id);

            if (c == null) return null;

            Compose(ref c);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (Category) c : null;
        }

        private void Compose(ref CAUIncidentCategory cauIncidentCategory)
        {
            if (cauIncidentCategory == null) return;

            cauIncidentCategory.IncidentCategoryId = Id;
            cauIncidentCategory.IncidentParentCategoryId = ParentId;
            cauIncidentCategory.IncidentCategoryName = Name;
            cauIncidentCategory.IncidentCategoryDescription = Description;
        }

        #endregion Methods

        #region Operators

        public static explicit operator Category(CAUIncidentCategory cauIncidentCategory)
        {
            if (cauIncidentCategory == null) return null;

            var category = new Category
            {
                Id = cauIncidentCategory.IncidentCategoryId,
                ParentId = cauIncidentCategory.IncidentParentCategoryId,
                Name = cauIncidentCategory.IncidentCategoryName,
                Description = cauIncidentCategory.IncidentCategoryDescription
            };

            return category;
        }

        #endregion Operators
    }
}
