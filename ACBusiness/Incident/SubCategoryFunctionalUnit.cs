using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACBusiness.Incident
{
    public class SubCategoryFunctionalUnit
    {
        #region Properties

        public int Id { get; set; }
        public int SubcategoryId { get; set; }
        public string SubcategoryName { get; set; }
        public string FunctionalUnitId { get; set; }
        public string FunctionalUnitName { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public SubCategoryFunctionalUnit()
        {
        }

        public SubCategoryFunctionalUnit(SubCategoryFunctionalUnit subCategoryFunctionalUnit)
        {
            Id = subCategoryFunctionalUnit.Id;
            SubcategoryId = subCategoryFunctionalUnit.SubcategoryId;
            SubcategoryName = subCategoryFunctionalUnit.SubcategoryName;
            FunctionalUnitId = subCategoryFunctionalUnit.FunctionalUnitId;
            FunctionalUnitName = subCategoryFunctionalUnit.FunctionalUnitName;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all relationship between subcategories and functional units
        /// </summary>
        /// <returns></returns>
        public static async Task<List<SubCategoryFunctionalUnit>> GetAllAsync()
        {
            using (var nexoContext = new NEXOContext())
            {
                List<CAUIncidentSubCategoryFunctionalUnit> incidentSubCategoryFunctionalUnits =
                    await nexoContext.cau_incident_sub_category_functional_unit.ToListAsync();

                return incidentSubCategoryFunctionalUnits.Select(s => (SubCategoryFunctionalUnit) s).ToList();
            }
        }

        /// <summary>
        /// Get all relationship between subcategories and functional units by subcategory id
        /// </summary>
        /// <returns></returns>
        public static async Task<List<SubCategoryFunctionalUnit>> GetAllBySubcategoryIdAsync(int subcategoryId)
        {
            using (var nexoContext = new NEXOContext())
            {
                List<CAUIncidentSubCategoryFunctionalUnit> incidentSubCategoryFunctionalUnits =
                    await nexoContext.cau_incident_sub_category_functional_unit
                        .Where(w => w.IncidentSubCategoryId == subcategoryId).ToListAsync();

                return incidentSubCategoryFunctionalUnits.Select(s => (SubCategoryFunctionalUnit) s).ToList();
            }
        }

        /// <summary>
        /// Get SubCategoryFunctionalUnit relationship info
        /// </summary>
        /// <param name="id">SubCategoryFunctionalUnit id</param>
        /// <returns></returns>
        public static async Task<SubCategoryFunctionalUnit> GetByIdAsync(int id)
        {
            var nexoContext = new NEXOContext();
            var incidentSubCategoryFunctionalUnit =
                await nexoContext.cau_incident_sub_category_functional_unit.FirstOrDefaultAsync(w => w.Id == id);

            return (SubCategoryFunctionalUnit) incidentSubCategoryFunctionalUnit;
        }

        /// <summary>
        /// Create new SubCategoryFunctionalUnit relationship
        /// </summary>
        /// <returns></returns>
        public async Task<SubCategoryFunctionalUnit> CreateAsync()
        {
            var s = new CAUIncidentSubCategoryFunctionalUnit();
            Compose(ref s);

            if (s == null) return null;

            _nexoContext.cau_incident_sub_category_functional_unit.Add(s);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (SubCategoryFunctionalUnit) s : null;
        }

        /// <summary>
        /// Update a SubCategoryFunctionalUnit relationship
        /// </summary>
        /// <returns></returns>
        public async Task<SubCategoryFunctionalUnit> UpdateAsync()
        {
            var s = await _nexoContext.cau_incident_sub_category_functional_unit.FirstOrDefaultAsync(w => w.Id == Id);

            if (s == null) return null;

            Compose(ref s);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1 ? (SubCategoryFunctionalUnit) s : null;
        }

        /// <summary>
        /// Delete a SubCategoryFunctionalUnit relationship
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteAsync()
        {
            var findRegistry =
                await _nexoContext.cau_incident_sub_category_functional_unit.FirstOrDefaultAsync(w => w.Id == Id);
            if (findRegistry == null) return false;

            _nexoContext.cau_incident_sub_category_functional_unit.Remove(findRegistry);
            var response = await _nexoContext.SaveChangesAsync();

            return response == 1;
        }

        private void Compose(ref CAUIncidentSubCategoryFunctionalUnit cauIncidentSubCategoryFunctionalUnit)
        {
            if (cauIncidentSubCategoryFunctionalUnit == null) return;

            cauIncidentSubCategoryFunctionalUnit.Id = Id;
            cauIncidentSubCategoryFunctionalUnit.IncidentSubCategoryId = SubcategoryId;
            cauIncidentSubCategoryFunctionalUnit.IncidentSubCategoryName = SubcategoryName;
            cauIncidentSubCategoryFunctionalUnit.FunctionalUnitId = FunctionalUnitId;
            cauIncidentSubCategoryFunctionalUnit.FunctionalUnitName = FunctionalUnitName;
        }

        #endregion Methods

        #region Operators

        public static explicit operator SubCategoryFunctionalUnit(
            CAUIncidentSubCategoryFunctionalUnit cauIncidentSubCategoryFunctionalUnit)
        {
            if (cauIncidentSubCategoryFunctionalUnit == null) return null;

            var subCategoryFunctionalUnit = new SubCategoryFunctionalUnit
            {
                Id = cauIncidentSubCategoryFunctionalUnit.Id,
                SubcategoryId = cauIncidentSubCategoryFunctionalUnit.IncidentSubCategoryId,
                SubcategoryName = cauIncidentSubCategoryFunctionalUnit.IncidentSubCategoryName,
                FunctionalUnitId = cauIncidentSubCategoryFunctionalUnit.FunctionalUnitId,
                FunctionalUnitName = cauIncidentSubCategoryFunctionalUnit.FunctionalUnitName
            };

            return subCategoryFunctionalUnit;
        }

        #endregion Operators
    }
}
