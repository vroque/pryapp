using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ACBusiness.Adryan;
using ACBusiness.Personal;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.CAU;
using _adryan = ACTools.Constants.AdryanConstants;
using _incident = ACTools.Constants.IncidentConstants;

namespace ACBusiness.Incident
{
    public class Incident
    {
        #region Properties

        public long Id { get; set; }
        public decimal AuthorPidm { get; set; }
        public decimal ReporterPidm { get; set; }
        public string ReporterName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime PublicationDate { get; set; }
        public int? CategoryId { get; set; }
        public int? SubCategoryId { get; set; }
        public int TypeId { get; set; }
        public int StatusId { get; set; }
        public decimal? AssignedToPidm { get; set; }
        public string AssignedToTeamId { get; set; }
        public int RequestSource { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion Properties

        #region Constructors

        public Incident()
        {
        }

        public Incident(Incident incident)
        {
            Id = incident.Id;
            AuthorPidm = incident.AuthorPidm;
            ReporterPidm = incident.ReporterPidm;
            ReporterName = incident.ReporterName;
            Title = incident.Title;
            Description = incident.Description;
            PublicationDate = incident.PublicationDate;
            CategoryId = incident.CategoryId;
            SubCategoryId = incident.SubCategoryId;
            TypeId = incident.TypeId;
            StatusId = incident.StatusId;
            AssignedToPidm = incident.AssignedToPidm;
            AssignedToTeamId = incident.AssignedToTeamId;
            RequestSource = incident.RequestSource;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Get all incidents
        /// </summary>
        /// <returns></returns>
        public async Task<List<Incident>> GetAllAsync()
        {
            List<CAUIncident> incidentList = await _nexoContext.cau_incident.ToListAsync();

            return incidentList.Select(s => (Incident) s).OrderByDescending(o => o.PublicationDate).ToList();
        }

        /// <summary>
        /// Get incident info
        /// </summary>
        /// <param name="id">Incident id</param>
        /// <returns></returns>
        public static async Task<Incident> GetByIdAsync(int id)
        {
            var nexoContext = new NEXOContext();
            var incident = await nexoContext.cau_incident.FirstOrDefaultAsync(w => w.IncidentId == id);

            return (Incident) incident;
        }

        /// <summary>
        /// Get all incidents reported by a student
        /// </summary>
        /// <param name="studentPidm">Student PIDM</param>
        /// <returns></returns>
        public static async Task<List<Incident>> GetAllByStudentPidm(decimal studentPidm)
        {
            using (var nexoContect = new NEXOContext())
            {
                List<CAUIncident> incidents = await nexoContect.cau_incident
                    .Where(w => w.ReporterPidm == studentPidm).ToListAsync();

                return incidents.Select(s => (Incident) s).OrderByDescending(o => o.PublicationDate).ToList();
            }
        }

        /// <summary>
        /// Search incidents
        /// </summary>
        /// <param name="reporterPidm">Reporter PIDM</param>
        /// <param name="authorPidm">Author PIDM</param>
        /// <param name="categoryId">Incident category id</param>
        /// <param name="typeId">Incident type id</param>
        /// <param name="statusId">Incident status id</param>
        /// <param name="ownerPidm">Assigned to PIDM</param>
        /// <param name="teamId">Assigned to team id</param>
        /// <param name="userPidm"></param>
        /// <returns></returns>
        public static async Task<List<Incident>> SearchAsync(int reporterPidm, int authorPidm, int categoryId,
            int typeId, int statusId, int ownerPidm, string teamId, decimal userPidm)
        {
            var nexoContext = new NEXOContext();
            var incidents = new List<CAUIncident>();

            if (reporterPidm > 0)
            {
                incidents = await nexoContext.cau_incident.Where(w => w.ReporterPidm == reporterPidm).ToListAsync();
            }
            else
            {
                incidents = await nexoContext.cau_incident.Where(w => w.ReporterPidm != reporterPidm).ToListAsync();
            }

            if (authorPidm > 0)
            {
                incidents = incidents.Where(w => w.AuthorPidm == authorPidm).ToList();
            }

            if (categoryId > 0)
            {
                incidents = incidents.Where(w => w.IncidentCategoryId == categoryId).ToList();
            }

            if (typeId > 0)
            {
                incidents = incidents.Where(w => w.IncidentTypeId == typeId).ToList();
            }

            if (statusId > 0)
            {
                incidents = incidents.Where(w => w.IncidentStatusId == statusId).ToList();
            }

            if (ownerPidm > 0)
            {
                incidents = incidents.Where(w => w.AssignedToPidm == ownerPidm).ToList();
            }

            if (!teamId.Equals("all"))
            {
                if (teamId.Equals("other"))
                {
                    List<Employee> employee = await Employee.GetByPidmAndCompanyIdAndStatusAsync(userPidm,
                        _adryan.ContinentalUniversityCompany, "active");

                    List<string> functionalUnitsByUser = employee.Select(s => s.unidad_funcional_organica).ToList();

                    incidents = incidents.Where(w => !functionalUnitsByUser.Contains(w.AssignedToTeam)).ToList();
                }
                else if (teamId.Equals("none"))
                {
                    incidents = incidents.Where(w => w.AssignedToTeam == _adryan.NoFunctionalUnit).ToList();
                }
                else
                {
                    incidents = incidents.Where(w => w.AssignedToTeam == teamId).ToList();
                }
            }

            return incidents.Select(s => (Incident) s).OrderByDescending(o => o.PublicationDate).ToList();
        }

        /// <summary>
        /// Create new incident
        /// </summary>
        /// <returns></returns>
        public async Task<Incident> CreateAsync()
        {
            PublicationDate = DateTime.Now;
            var persona = new Person(ReporterPidm);
            ReporterName = $"{persona.full_name}";
            StatusId = 1; // Status: Open
            var i = new CAUIncident();
            Compose(ref i);
            if (i == null) return null;

            _nexoContext.cau_incident.Add(i);
            var response = await _nexoContext.SaveChangesAsync();

            if (response == 1)
            {
                await IncidentLog.NewIncidentLogAsync(i.IncidentId, _incident.LogCreate, "creó el incidente",
                    AuthorPidm);
            }

            return response == 1 ? (Incident) i : null;
        }

        /// <summary>
        /// Update an incident
        /// </summary>
        /// <param name="userPidm"></param>
        /// <returns></returns>
        public async Task<Incident> UpdateAsync(decimal userPidm)
        {
            var i = await _nexoContext.cau_incident.FirstOrDefaultAsync(w => w.IncidentId == Id);

            if (i == null) return null;

            var assignedToTeamId = i.AssignedToTeam;
            decimal? assignedToPidm = i.AssignedToPidm;
            int? categoryId = i.IncidentCategoryId;
            int? subcategoryId = i.IncidentSubCategoryId;
            var statusId = i.IncidentStatusId;

            Compose(ref i);

            var response = await _nexoContext.SaveChangesAsync();

            if (response == 1)
            {
                if (StatusId != statusId)
                {
                    var status = await IncidentStatus.GetByIdAsync(StatusId);
                    var desc = $"cambió el estado del ticket a {status.Name}";
                    await IncidentLog.NewIncidentLogAsync(i.IncidentId, _incident.LogTask, desc, userPidm);
                }

                if (AssignedToTeamId != assignedToTeamId)
                {
                    var team = await FunctionalUnit.GetByIdAndCompanyIdAsync(AssignedToTeamId);
                    var desc = $"asignó el ticket al equipo {team.nombre_unidad_funcional}";
                    await IncidentLog.NewIncidentLogAsync(i.IncidentId, _incident.LogAssignDerive, desc, userPidm);
                }

                if (AssignedToPidm != assignedToPidm && AssignedToPidm != null)
                {
                    var persona = new Person(AssignedToPidm ?? 0);
                    var desc = $"asignó el ticket a {persona.full_name}";
                    await IncidentLog.NewIncidentLogAsync(i.IncidentId, _incident.LogAssignDerive, desc, userPidm);
                }

                if (CategoryId != categoryId)
                {
                    var oldCategory = await Category.GetByIdAsync(categoryId ?? 0);
                    var newCategory = await Category.GetByIdAsync(CategoryId ?? 0);
                    var desc = $"cambió de categoría a {newCategory.Name}";
                    if (oldCategory != null)
                    {
                        desc = $"cambió de categoría {oldCategory.Name} a {newCategory.Name}";
                    }
                    await IncidentLog.NewIncidentLogAsync(i.IncidentId, _incident.LogCategorize, desc, userPidm);
                }

                if (SubCategoryId != subcategoryId)
                {
                    var oldSubcategory = await SubCategory.GetByIdAsync(subcategoryId ?? 0);
                    var newSubcategory = await SubCategory.GetByIdAsync(SubCategoryId ?? 0);
                    var desc = $"cambió de subcategoría a {newSubcategory.Name}";
                    if (oldSubcategory != null)
                    {
                        desc = $"cambió de subcategoría {oldSubcategory.Name} a {newSubcategory.Name}";
                    }
                    await IncidentLog.NewIncidentLogAsync(i.IncidentId, _incident.LogCategorize, desc, userPidm);
                }
            }

            return response == 1 ? (Incident) i : null;
        }

        private void Compose(ref CAUIncident cauIncident)
        {
            if (cauIncident == null) return;

            cauIncident.AuthorPidm = AuthorPidm;
            cauIncident.ReporterPidm = ReporterPidm;
            cauIncident.ReporterName = ReporterName;
            cauIncident.Title = Title;
            cauIncident.Description = Description;
            cauIncident.PublicationDate = PublicationDate;
            cauIncident.IncidentCategoryId = CategoryId;
            cauIncident.IncidentSubCategoryId = SubCategoryId;
            cauIncident.IncidentTypeId = TypeId;
            cauIncident.IncidentStatusId = StatusId;
            cauIncident.AssignedToPidm = AssignedToPidm;
            cauIncident.AssignedToTeam = AssignedToTeamId;
            cauIncident.RequestSource = RequestSource;
        }

        #endregion Methods

        #region Operators

        public static explicit operator Incident(CAUIncident cauIncident)
        {
            if (cauIncident == null) return null;

            var incident = new Incident
            {
                Id = cauIncident.IncidentId,
                AuthorPidm = cauIncident.AuthorPidm,
                ReporterPidm = cauIncident.ReporterPidm,
                ReporterName = cauIncident.ReporterName,
                Title = cauIncident.Title,
                Description = cauIncident.Description,
                PublicationDate = cauIncident.PublicationDate,
                CategoryId = cauIncident.IncidentCategoryId,
                SubCategoryId = cauIncident.IncidentSubCategoryId,
                TypeId = cauIncident.IncidentTypeId,
                StatusId = cauIncident.IncidentStatusId,
                AssignedToPidm = cauIncident.AssignedToPidm,
                AssignedToTeamId = cauIncident.AssignedToTeam,
                RequestSource = cauIncident.RequestSource
            };

            return incident;
        }

        #endregion Operators
    }
}
