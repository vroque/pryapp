﻿using ACBusiness.Academic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.DataBases.NEXO.OPP;
using ACPermanence.Contexts.NEXO;
using ACTools.PDF;
using ACTools.Configuration;

namespace ACBusiness.GyT
{
    /// <summary>
    /// Practicas Pre Profecionales
    /// </summary>
    public class PPPractices
    {
        #region properties
        //private AcademicProfile profile { get; set; }
        public decimal pidm { get; set; }
        public string student_id { get; set; }
        public string student_names { get; set; }
        public string div { get; set; }
        public string lvl { get; set; }
        public string program { get; set; }
        public DateTime date { get; set; }
        public int certified_id { get; set; }
        public string practice_id { get; set; }
        public string uri { get; set; }

        private NEXOContext db = new NEXOContext();
        #endregion properties

        #region methods

        /// <summary>
        /// Busca un alumno que haya cumplido sus practicas
        /// </summary>
        /// <param name="student_id"></param>
        /// <returns></returns>
        public PPPractices get(string student_id)
        {
            List<string> list = new List<string>();
            list.Add(student_id);
            List<PPPractices> practices = this.listByCodes(list);
            return practices.FirstOrDefault(); ;
        }

        /// <summary>
        /// Busca a los alumnos que hayan culminado practicas por
        /// una lista de codigos
        /// </summary>
        /// <param name="student_list"></param>
        /// <returns></returns>
        public List<PPPractices> listByCodes(List<string> student_list)
        {
            List<PPPractices> practices = db.opp_practices_documents.Join(
                    db.dim_persons,
                    pr => new { pr.pidm },
                    pe => new { pe.pidm },
                    (pr, pe) => new { pr = pr, pe = pe }
                ).Where(f =>
                   student_list.Contains(f.pe.documentNumber)
                ) 
                .Select(f => new PPPractices
                {
                    certified_id = f.pr.practiceDocumentID,
                    practice_id = f.pr.practiceID,
                    div = f.pr.div,
                    lvl = f.pr.lvl,
                    program = f.pr.program,
                    pidm = f.pr.pidm,
                    student_id = f.pe.documentNumber,
                    student_names = f.pe.fullName,
                    date = f.pr.createDate
                }).ToList();
            this.chekDocumentUris(ref practices);
            return practices;
        }

        /// <summary>
        /// Busca a los alumnos que hayan culminado practicas por sus nombres
        /// y los lista
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<PPPractices> listByName(string name)
        {
            List<PPPractices> practices = new List<PPPractices>();

            return practices;
        }

        /// <summary>
        /// Busca a los alumnos que hayan culminado practicas por 
        /// un rango de fechas y los lista
        /// </summary>
        /// <param name="date_start"></param>
        /// <param name="date_end"></param>
        /// <returns></returns>
        public List<PPPractices> listByDateRange(DateTime date_start, DateTime date_end)
        {
            List<PPPractices> practices = db.opp_practices_documents.Join(
                    db.dim_persons,
                    pr => new { pr.pidm },
                    pe => new { pe.pidm },
                    (pr, pe) => new {pr = pr, pe = pe}
                ).Where(f =>
                   f.pr.createDate >= date_start
                   && f.pr.createDate <= date_end
                ) 
                .Select(f => new PPPractices {
                    certified_id = f.pr.practiceDocumentID,
                    practice_id = f.pr.practiceID,
                    div = f.pr.div,
                    lvl = f.pr.lvl,
                    program = f.pr.program,
                    pidm = f.pr.pidm,
                    student_id = f.pe.documentNumber,
                    student_names = f.pe.fullName,
                    date = f.pr.createDate
                }).ToList();
            this.chekDocumentUris(ref practices);
            return practices;
        }

        /// <summary>
        /// Busca a los alumnos que hayan culminado practicas por 
        /// la escuela academica a la que pertenecen
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<PPPractices> listByProgram(string program_id)
        {
            List<PPPractices> practices = db.opp_practices_documents.Join(
                    db.dim_persons,
                    pr => new { pr.pidm },
                    pe => new { pe.pidm },
                    (pr, pe) => new { pr = pr, pe = pe }
                ).Where(f =>
                   f.pr.program == program_id
                )
                .Select(f => new PPPractices
                {
                    certified_id = f.pr.practiceDocumentID,
                    practice_id = f.pr.practiceID,
                    div = f.pr.div,
                    lvl = f.pr.lvl,
                    program = f.pr.program,
                    pidm = f.pr.pidm,
                    student_id = f.pe.documentNumber,
                    student_names = f.pe.fullName,
                    date = f.pr.createDate
                }).ToList();
            this.chekDocumentUris(ref practices);
            return practices;
        }

        private void chekDocumentUris(ref List<PPPractices> practices)
        {
            // listar todos los pdf
            List<string> list = PDFGenerator.listDocumentInPath("output-opp").ToList();
            foreach(PPPractices item in practices)
            {
                //comprobarlos con las listas
                string name_file = string.Format("{0}-A-1", item.student_id);
                string file = list.FirstOrDefault(f => f.Contains(name_file));
                if (file != null)
                {
                    // generar la url
                    item.uri = string.Format("{0}gyt/{1}/pdf/", AppSettings.reports["output-client"], name_file);
                }
            }
        }
        #endregion methods

        #region implicite operators
        public static implicit operator PPPractices(OPPTblPracticeDocument obj)
        {
            if (obj == null) return null;
            PPPractices practice = new PPPractices();
            practice.certified_id = obj.practiceDocumentID;
            practice.practice_id = obj.practiceID;
            //Student student = new Student(obj.studentID);//obj.pidm
            //student.syncProfile(obj.div, obj.lvl, obj.program);
            practice.div = obj.div;

            practice.date = obj.createDate;
            return practice;
        }
        #endregion implicite operators
    }
}
