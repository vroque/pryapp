﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.ACCESS;

namespace ACBusiness.Aplication
{
    public class AppUser
    {
        #region properties

        public string id { get; set; }
        public decimal pid { get; set; }
        public string state { get; set; }
        public DateTime update_date { get; set; }


        private NEXOContext _db = new NEXOContext();
        #endregion properties
        public static List<AppUser>queryFilter(int id)
        {
            NEXOContext db = new NEXOContext();
            List<AppUser> user = db.access_users
                .Join(db.access_group_users,
                users => users.userID ,
                group_user => group_user.userID,
                (users, group_user) => new {
                    Users = users,
                    Group_user = group_user
                })
                .Where(e =>
                    e.Group_user.groupID == id
                )
                .ToList()
                .Select(
                    c => new AppUser
                    {
                        pid = c.Users.pidm,
                        update_date = c.Users.updateDate,
                        id = c.Users.userID,
                        state = c.Users.userState
                    }
                )
                .OrderBy(o => o.id)
                .ToList();
            return user;
        }
        public static List<AppUser> queryFilterName(string id)
        {
            NEXOContext db = new NEXOContext();
            List<AppUser> user = db.access_users
                .Where(e =>
                  e.userID.Contains(id)
                )
                .ToList()
                .Select<ACCESSTblUser, AppUser>(f => f)
                .ToList();
            return user;
        }

        /// <summary>
        /// Lista usuarios de un modulo
        /// </summary>
        /// <param name="module_name">Nombre de modulo</param>
        /// <returns></returns>
        public List<AppUser> listUserByModule(string module_name)
        {
            List<AppUser> users = _db.access_modules.Join(
                    _db.access_group_modules,
                    tm => new { moduleID = tm.moduleID },
                    tgm => new { moduleID = tgm.moduleID },
                    (tm, tgm) => new { tm, tgm }
                )
                .Join(
                    _db.access_group_users,
                    tgm2 => new { groupID = tgm2.tgm.groupID },
                    tgu => new { groupID = tgu.groupID },
                    (tgm2, tgu) => new { tgm2.tm, tgm2.tgm, tgu }
                )
                .Join(
                    _db.access_users,
                    tgu2 => new { userID = tgu2.tgu.userID },
                    tu => new { userID = tu.userID },
                    (tgu2, tu) => new { tgu2.tm, tgu2.tgm, tgu2.tgu, tu }
                )
                .Where(result =>
                    result.tm.moduleName == module_name
                ).Select(
                    result => new AppUser {
                        pid = result.tu.pidm,
                        id = result.tu.userID,
                        state = result.tu.userState,
                })
                .Distinct().ToList();

            return users;

        }

        #region implicite operators
        public static implicit operator AppUser(ACCESSTblUser obj)
        {
            if (obj == null) return null;
            AppUser user = new AppUser();
            user.pid = obj.pidm;
            user.update_date = obj.updateDate;
            user.id = obj.userID;
            user.state = obj.userState;
            return user;
        }
        #endregion implicite operators
    }
}
