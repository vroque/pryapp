﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.ACCESS;

namespace ACBusiness.Aplication
{
    public class AppGroup
    {
        #region properties

        public int id { get; set; }
        public string name { get; set; }
        public DateTime update_date { get; set; }

        private NEXOContext _db = new NEXOContext();
        #endregion properties

        #region methods

        public static List<AppGroup> query()
        {
            NEXOContext db = new NEXOContext();
            List<AppGroup> groups = db.access_group
                .ToList()
                .Select<ACCESSTblGroup, AppGroup>(f => f)
                .OrderBy(o => o.name)
                .ToList();
            return groups;
        }

        public static List<AppGroup> queryFilter( int id_module, string id_campus )
        {
            NEXOContext db = new NEXOContext();
            List<AppGroup> groups = db.access_group
                .Join(db.access_group_modules,
                    group => group.groupID,
                    group_modules => group_modules.groupID,

                     (group, group_modules) => new {
                         Group = group,
                         Group_modules = group_modules
                     }
                )
                .Where(e =>

                    e.Group_modules.campus == id_campus &&
                    e.Group_modules.moduleID == id_module 
                
                )
                .ToList()
                .Select(
                     c => new AppGroup
                     {
                         id = c.Group.groupID,
                         name = c.Group.groupName,
                         update_date = c.Group.updateDate
                     }
                )
                .ToList();
            return groups;
        }

        #endregion methods

        public bool save() {
            ACCESSTblGroup g = new ACCESSTblGroup();
            g.groupName = this.name;
            g.updateDate = DateTime.Now;

            _db.access_group.Add(g);
            int i = _db.SaveChanges();

            this.id = g.groupID;
            if (i > 0)
                return true;
            else
                return false;            
        }

        public bool update(int id, string name)
        {


            byte[] a;

            AppGroup g = _db.access_group.FirstOrDefault(f => f.groupID == id);
            g.name = name;

           // _db.access_group.Add(g);
            int i = _db.SaveChanges();

            if (i > 0)
                return true;
            return false;
        }

        #region implicite operators
        public static implicit operator AppGroup(ACCESSTblGroup obj)
        {
            if (obj == null) return null;
            AppGroup group = new AppGroup();
            group.id = obj.groupID;
            group.name = obj.groupName;
            group.update_date = obj.updateDate;
            return group;
        }
        #endregion implicite operators
    }
}
