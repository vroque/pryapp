﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.ACCESS;
using System;
using System.Collections.Generic;
using System.Linq;
using _app = ACTools.Constants.AppConstants;

namespace ACBusiness.Aplication
{
    public class AppModule
    {
        #region properties

        public int id { get; set; }
        public int app_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime update_date { get; set; }
        public string abbr { get; set; }

        private readonly NEXOContext _nexoContext = new NEXOContext();

        #endregion properties

        #region methods

        public static List<AppModule> query()
        {
            var db = new NEXOContext();

            List<AppModule> modules = db.access_modules
                .Where(f => f.applicationID == _app.CAUAPP_ID
                    //&& !f.moduleName.StartsWith("fd_")
                ).ToList()
                .Select<ACCESSTblModule, AppModule>(f => f)
                .OrderBy(o => o.description)
                .ToList();

            return modules;
        }

        /// <summary>
        /// Crea un nuevo modulo en base los datos
        /// </summary>
        /// <returns></returns>
        public bool create()
        {
            var module = new ACCESSTblModule
            {
                abbr = this.abbr,
                applicationID = this.app_id,
                moduleDescription = this.description,
                moduleName = this.name,
                updateDate = DateTime.Now
            };

            _nexoContext.access_modules.Add(module);
            var i = _nexoContext.SaveChanges();

            return i > 0;
        }

        /// <summary>
        /// Actualiza los modulos
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool update(int id, string name)
        {
            var modules = _nexoContext.access_modules.FirstOrDefault(f => f.moduleID == id);
            
            if (modules == null) return false;
            
            modules.moduleName = name;

            var i = _nexoContext.SaveChanges();

            return i > 0;
        }

        #endregion methods


        #region implicite operators

        public static implicit operator AppModule(ACCESSTblModule obj)
        {
            if (obj == null) return null;
            var module = new AppModule
            {
                id = obj.moduleID,
                app_id = obj.applicationID,
                name = obj.moduleName,
                description = obj.moduleDescription,
                update_date = obj.updateDate,
                abbr = obj.abbr
            };
            return module;
        }

        #endregion implicite operators
    }
}