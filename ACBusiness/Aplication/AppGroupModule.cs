﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.ACCESS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACBusiness.Aplication
{
    public class AppGroupModule
    {

        #region properties

        public int groupID { get; set; }
        public int moduleID { get; set; }
        public string campus { get; set; }
        public DateTime updateDate { get; set; }

        private NEXOContext _db = new NEXOContext();
        #endregion properties

        #region methods

        public static List<AppGroupModule> query()
        {
            NEXOContext db = new NEXOContext();
            List<AppGroupModule> group_module = db.access_group_modules
                .ToList()
                .Select<ACCESSTblGroupModule, AppGroupModule>(f => f)
                .ToList();
            return group_module;
        }

        public bool save()
        {
            ACCESSTblGroupModule gm = new ACCESSTblGroupModule();
            gm.groupID = this.groupID;
            gm.moduleID = this.moduleID;
            gm.campus = this.campus;
            gm.updateDate = DateTime.Now;

            _db.access_group_modules.Add(gm);
            int i = _db.SaveChanges();

            if (i > 0)
                return true;
            return false;
        }

        public bool remove()
        {
            ACCESSTblGroupModule g_m = _db.access_group_modules
            .FirstOrDefault(e =>
                e.moduleID == this.moduleID
                && e.groupID == this.groupID
                && e.campus == this.campus
            );

            _db.access_group_modules.Remove(g_m);

            int i = _db.SaveChanges();

            if (i > 0)
                return true;
            return false;
        }

        public bool update(int id, string name)
        {

            AppGroupModule gm = _db.access_group_modules.FirstOrDefault(
                f => f.groupID == id &&
                f.moduleID == id
                );

            // _db.access_group.Add(g);
            int i = _db.SaveChanges();

            if (i > 0)
                return true;
            return false;
        }

        #endregion methods


        #region implicite operators
        public static implicit operator AppGroupModule(ACCESSTblGroupModule obj)
        {
            if (obj == null) return null;
            AppGroupModule group_module = new AppGroupModule();
            group_module.groupID = obj.groupID;
            group_module.moduleID = obj.moduleID;
            group_module.campus = obj.campus;
            group_module.updateDate = obj.updateDate;
   
            return group_module;
        }
        #endregion implicite operators
    }
}
