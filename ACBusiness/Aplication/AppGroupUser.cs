﻿using ACPermanence.Contexts.NEXO;
using ACPermanence.DataBases.NEXO.ACCESS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ACBusiness.Aplication
{
    public class AppGroupUser
    {
        #region properties

        public int group_id { get; set; }
        public string user_id { get; set; }
        public DateTime update_date { get; set; }

        private NEXOContext _db = new NEXOContext();
        #endregion properties


        #region methods
        public bool add()
        {
            ACCESSTblGroupUser g_u = new ACCESSTblGroupUser();
            g_u.groupID = this.group_id;
            g_u.userID = this.user_id;
            g_u.updateDate = DateTime.Now;

            _db.access_group_users.Add(g_u);
            int i = _db.SaveChanges();

            if (i > 0)
                return true;
            return false;
        }

        public bool remove(int group_id, string user_id)
        {

            ACCESSTblGroupUser g_u = _db.access_group_users
            .FirstOrDefault(e =>
                e.userID == user_id
                && e.groupID == group_id
            );

            _db.access_group_users.Remove(g_u);

            int i = _db.SaveChanges();

            if (i > 0)
                return true;
            return false;
        }

        #endregion methods
    


        #region implicite operators
        public static implicit operator AppGroupUser(ACCESSTblGroupUser obj)
        {
            if (obj == null) return null;
            AppGroupUser g_u = new AppGroupUser();
            g_u.group_id = obj.groupID;
            g_u.user_id = obj.userID;
            g_u.update_date = obj.updateDate;
            return g_u;
        }
        #endregion implicite operators
    }
}
