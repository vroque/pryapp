﻿using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.Contexts.BDUCCI;
using System.Collections.Generic;
using System.Linq;
using System;

namespace ACBusiness.Economic
{
    /// <summary>
    /// Escala socioeconomica
    /// * afecta en el monto de pensión
    /// </summary>
    public class Scala
    {
        public string scala { get; set; }

        /// <summary>
        /// Obtiene la escala de una alumno por su codigo de estudiante
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public static string GetByStudent(string studentId){
            var scala = GetScalaAndTermAndCronogramaByStudent(studentId).Scala;
            return scala;
        }

        public static dynamic GetScalaAndTermAndCronogramaByStudent(string studentId)
        {
            var dboContext = new DBOContext();
            var scala = dboContext.tbl_alumno_escala.Where(f => f.IDAlumno == studentId)
                .OrderByDescending(f => f.IDPerAcad)
                .ThenByDescending(f => f.FechaEscala)
                .FirstOrDefault();
            //Tuple<string, string> data = new Tuple<string, string>(scala != null ? scala.IDEscala1 : "-", Term);
            var data = new
            {
                Scala = scala != null ? scala.IDEscala1 : "-",
                Term = scala.IDPerAcad,
                Chronogram = scala != null ? scala.Cronograma : "-"
            };
            return data;
        }

        public static Dictionary<string, string> getByStudentList(List<string> student_list)
        {
            // <student_id, scala>
            Dictionary<string, string> data = new Dictionary<string, string>();

            DBOContext db = new DBOContext();
            List<DBOTblAlumnoEscala> scalas = db.tbl_alumno_escala.Where(
                    f => student_list.Contains(f.IDAlumno)
                ).OrderByDescending(f => f.IDPerAcad)
                .ThenByDescending(f => f.FechaEscala)
                .ToList();
            // comprobar que todos estan
            foreach(string student in student_list)
            {
                DBOTblAlumnoEscala scala = scalas.FirstOrDefault(f => f.IDAlumno == student);
                string scala_id;
                if (scala != null)
                    scala_id = (scala.IDEscala5 == null) ? scala.IDEscala : scala.IDEscala5;
                else
                    scala_id = "-";
                // fill dictionary
                data.Add(student, scala_id);
            }
            return data;
        }
    }
}
