﻿using ACPermanence.Contexts.BDUCCI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ACBusiness.Documentary
{
    public class Application
    {
        public List<StudentAndHisApplications> reportNumberOfTimes(string id_dependecy,string id_headquarters,string id_student = "", int id_document = 0, int id_state = 0 )
        {
            //report: number of times a student make an application
            DBOContext db = new DBOContext();        

            var report_data = db.ate_tbl_solicitud.Join(
                db.ate_tbl_documento,
                a_base => a_base.IDDocumento,
                document => document.IDDocumento,
                (a_base, document) => new { a_base, document }
            )
            .Join(
                db.ate_tbl_solicitud_estado,
                a => a.a_base.IDSolicitudEstado,
                state => state.IDSolicitudEstado,
                (a, state) => new { a, state }
            )
            .Where(x =>
                (x.a.a_base.IDDocumento == id_document || id_document == 0) &&
                (x.a.a_base.IDSolicitudEstado == id_state || id_state == 0) &&
                (x.a.a_base.IDAlumno == id_student || id_student == null) &&
                x.a.a_base.IDDependencia==id_dependecy &&
                x.a.a_base.IDSede == id_headquarters
            )
            .OrderByDescending(x => x.a.a_base.FecSolicitud)
            .Select(x => new { 
                id_student = x.a.a_base.IDAlumno, 
                id_application = x.a.a_base.IDSolicitud, 
                str_state = x.state.descripcion, 
                str_document = x.a.document.NomDocumento,
                date_application=x.a.a_base.FecSolicitud
            }
            )
            
            .ToList();

            //try others ways to do this
            List<string> id_student_list = report_data.Select(x => x.id_student).GroupBy(x => x).Select(x=> x.First()).ToList();
            List<StudentAndHisApplications> report = new List<StudentAndHisApplications>();
            // see wath happens when we declare student inside the foreach
            
            foreach (string id_st in id_student_list) {
                StudentAndHisApplications student = new StudentAndHisApplications();
                student.id_student = id_st;
                student.applications = report_data
                    .Where(x => x.id_student == student.id_student)
                    .Select(x => new ApplicationSummary { 
                        id_application = x.id_application,
                        date_application = x.date_application.ToString("dd/MM/yy"), 
                        str_state= x.str_state,
                        str_document = x.str_document
                    })
                    .ToList();
                report.Add(student);
            }

            return report;
        }
        public class StudentAndHisApplications 
        {
            public string id_student { set; get; }
            public  List<ApplicationSummary> applications { set; get; }
        }
        public struct ApplicationSummary 
        {
            public int id_application { set; get; }
            public string date_application { set; get; }
            public string str_state { set; get; }
            public string str_document { set; get; }
        }
    }
}
