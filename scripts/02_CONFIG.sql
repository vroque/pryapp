USE BDUCCI

BEGIN TRY
    BEGIN TRANSACTION
        DECLARE @idApp INT
        DECLARE @idGrupoAdm INT
        DECLARE @idGrupoDoc INT
        DECLARE @idGrupoGen INT
        DECLARE @idModuloAdm INT
        DECLARE @idModuloDoc INT
        DECLARE @idModuloGen INT

/* Registrar aplicacion
*/
        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.tblApplication
                      WHERE applicationName = 'Proyectos UC')
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.tblApplication (applicationName, applicationDescription, updateDate)
                VALUES ('Proyectos UC', 'Plataforma web de proyectos continental', GETDATE());
                SET @idApp = SCOPE_IDENTITY();
            end
        ELSE
            BEGIN
                SET @idApp = (SELECT TOP 1 applicationID
                              FROM BDINTBANNER.ACCESS.tblApplication
                              WHERE applicationName = 'Proyectos UC')
            end


/* Registrar grupo
*/
        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.tblGroup
                      WHERE groupName = 'PRY-Administrador')
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.tblGroup (groupName, updateDate)
                VALUES ('PRY-Administrador', GETDATE());
                SET @idGrupoAdm = SCOPE_IDENTITY();
            end
        ELSE
            BEGIN
                SET @idGrupoAdm = (SELECT TOP 1 groupID
                                   FROM BDINTBANNER.ACCESS.tblGroup
                                   WHERE groupName = 'PRY-Administrador')
            end

        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.tblGroup
                      WHERE groupName = 'PRY-Docente')
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.tblGroup (groupName, updateDate)
                VALUES ('PRY-Docente', GETDATE());
                SET @idGrupoDoc = SCOPE_IDENTITY();
            end
        ELSE
            BEGIN
                SET @idGrupoDoc = (SELECT TOP 1 groupID
                                   FROM BDINTBANNER.ACCESS.tblGroup
                                   WHERE groupName = 'PRY-Docente')
            end

        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.tblGroup
                      WHERE groupName = 'PRY-General')
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.tblGroup (groupName, updateDate)
                VALUES ('PRY-General', GETDATE());
                SET @idGrupoGen = SCOPE_IDENTITY();
            end
        ELSE
            BEGIN
                SET @idGrupoGen = (SELECT TOP 1 groupID
                                   FROM BDINTBANNER.ACCESS.tblGroup
                                   WHERE groupName = 'PRY-General')
            end


/* Registrar modulo
*/
        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.tblModule
                      WHERE moduleName = 'bo_pry_administrador')
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.tblModule (applicationID, moduleName, moduleDescription, updateDate, abbr)
                VALUES (@idApp, 'bo_pry_administrador', 'Aprobadores de Proyectos UC', GETDATE(), 'pry-adm');
                SET @idModuloAdm = SCOPE_IDENTITY();
            end
        ELSE
            BEGIN
                SET @idModuloAdm = (SELECT TOP 1 moduleID
                                    FROM BDINTBANNER.ACCESS.tblModule
                                    WHERE moduleName = 'bo_pry_administrador')
            end

        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.tblModule
                      WHERE moduleName = 'bo_pry_docente')
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.tblModule (applicationID, moduleName, moduleDescription, updateDate, abbr)
                VALUES (@idApp, 'bo_pry_docente', 'Creadores de Proyectos UC', GETDATE(), 'pry-doc');
                SET @idModuloDoc = SCOPE_IDENTITY();
            end
        ELSE
            BEGIN
                SET @idModuloDoc = (SELECT TOP 1 moduleID
                                    FROM BDINTBANNER.ACCESS.tblModule
                                    WHERE moduleName = 'bo_pry_docente')
            end

        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.tblModule
                      WHERE moduleName = 'bo_pry_general')
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.tblModule (applicationID, moduleName, moduleDescription, updateDate, abbr)
                VALUES (@idApp, 'bo_pry_general', 'General proyectos UC', GETDATE(), 'pry-gen');
                SET @idModuloGen = SCOPE_IDENTITY();
            end
        ELSE
            BEGIN
                SET @idModuloGen = (SELECT TOP 1 moduleID
                                    FROM BDINTBANNER.ACCESS.tblModule
                                    WHERE moduleName = 'bo_pry_general')
            end


/* Modulo - grupo
*/
        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.TblGroupModule
                      WHERE groupID = @idGrupoAdm
                        AND moduleID = @idModuloAdm)
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.TblGroupModule (groupID, moduleID, campus, updateDate)
                VALUES (@idGrupoAdm, @idModuloAdm, 'S01', GETDATE());
            end

        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.TblGroupModule
                      WHERE groupID = @idGrupoDoc
                        AND moduleID = @idModuloDoc)
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.TblGroupModule (groupID, moduleID, campus, updateDate)
                VALUES (@idGrupoDoc, @idModuloDoc, 'S01', GETDATE());
            end

        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.TblGroupModule
                      WHERE groupID = @idGrupoGen
                        AND moduleID = @idModuloGen)
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.TblGroupModule (groupID, moduleID, campus, updateDate)
                VALUES (@idGrupoGen, @idModuloGen, 'S01', GETDATE());
            end


/* Usuario - grupo
*/
        DECLARE @user VARCHAR(50) = 'krojas'
        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.tblGroupUser
                      WHERE groupID = @idGrupoAdm
                        AND userID = @user)
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.tblGroupUser (groupID, userID, updateDate, levl)
                VALUES (@idGrupoAdm, @user, GETDATE(), 'PG');
            end
        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.tblGroupUser
                      WHERE groupID = @idGrupoDoc
                        AND userID = @user)
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.tblGroupUser (groupID, userID, updateDate, levl)
                VALUES (@idGrupoDoc, @user, GETDATE(), 'PG');
            end
        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.tblGroupUser
                      WHERE groupID = @idGrupoGen
                        AND userID = @user)
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.tblGroupUser (groupID, userID, updateDate, levl)
                VALUES (@idGrupoGen, @user, GETDATE(), 'PG');
            end

/* Menus
*/
        DECLARE @idMenuPadreAdm INT

        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.TblMenu
                      WHERE name = 'Aprobar proyectos'
                        and uri = 'proyectos-continental-adm')
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.TblMenu (moduleID, position, driver, name, icon, uri, cssStyle, status,
                                                        parentID,
                                                        updateDate, description, cardStyle, cardIcoStyle, [order],
                                                        ExternalLink)
                VALUES (@idModuloAdm, 'all', null, 'Aprobar proyectos', 'fa fa-book', 'proyectos-continental-adm', null,
                        '1', null, GETDATE(), null, null, null, 1, 0);
                SET @idMenuPadreAdm = SCOPE_IDENTITY();
                INSERT INTO BDINTBANNER.ACCESS.TblMenu (moduleID, position, driver, name, icon, uri, cssStyle, status,
                                                        parentID,
                                                        updateDate, description, cardStyle, cardIcoStyle, [order],
                                                        ExternalLink)
                VALUES (@idModuloAdm, 'side', null, 'Proyectos', 'fa fa-book', 'proyectos', null, '1',
                        @idMenuPadreAdm, GETDATE(), null, null, null, 1, 0);
            end

        DECLARE @idMenuPadreDoc INT

        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.TblMenu
                      WHERE name = 'Proyectos'
                        and uri = 'proyectos-continental-doc')
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.TblMenu (moduleID, position, driver, name, icon, uri, cssStyle, status,
                                                        parentID,
                                                        updateDate, description, cardStyle, cardIcoStyle, [order],
                                                        ExternalLink)
                VALUES (87, 'all', null, 'Proyectos', 'fa fa-book', 'proyectos-continental-doc', null,
                        '1', null, GETDATE(), null, null, null, 1, 0);
                SET @idMenuPadreDoc = SCOPE_IDENTITY();
                INSERT INTO BDINTBANNER.ACCESS.TblMenu (moduleID, position, driver, name, icon, uri, cssStyle, status,
                                                        parentID,
                                                        updateDate, description, cardStyle, cardIcoStyle, [order],
                                                        ExternalLink)
                VALUES (87, 'side', null, 'Mis Proyectos', 'fa fa-book', 'proyectos', null, '1',
                        @idMenuPadreDoc, GETDATE(), null, null, null, 1, 0);
            end

        DECLARE @idMenuPadreGen INT

        IF NOT EXISTS(SELECT *
                      FROM BDINTBANNER.ACCESS.TblMenu
                      WHERE name = 'Inscribirse en proyectos'
                        and uri = 'inscripcion')
            BEGIN
                INSERT INTO BDINTBANNER.ACCESS.TblMenu (moduleID, position, driver, name, icon, uri, cssStyle, status,
                                                        parentID,
                                                        updateDate, description, cardStyle, cardIcoStyle, [order],
                                                        ExternalLink)
                VALUES (@idModuloGen, 'all', null, 'Inscribirse en proyectos', 'fa fa-book', 'inscripcion', null,
                        '1', null, GETDATE(), null, null, null, 1, 0);
                SET @idMenuPadreGen = SCOPE_IDENTITY();
                INSERT INTO BDINTBANNER.ACCESS.TblMenu (moduleID, position, driver, name, icon, uri, cssStyle, status,
                                                        parentID,
                                                        updateDate, description, cardStyle, cardIcoStyle, [order],
                                                        ExternalLink)
                VALUES (@idModuloGen, 'side', null, 'Categorias', 'fa fa-book', 'proyectos', null, '1',
                        @idMenuPadreGen, GETDATE(), null, null, null, 1, 0);
            end
    COMMIT TRANSACTION
END TRY
BEGIN CATCH

    --INSTRUCCIONES EN CASO DE ERRORES
    DECLARE
        @ErrorMessage NVARCHAR(4000);
    DECLARE
        @ErrorSeverity INT;
    DECLARE
        @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
           @ErrorSeverity = ERROR_SEVERITY(),
           @ErrorState = ERROR_STATE();

    RAISERROR (
        @ErrorMessage, -- Message text.
        @ErrorSeverity, -- Severity.
        @ErrorState -- State.
        );
    ROLLBACK TRANSACTION
END CATCH