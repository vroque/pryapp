USE BDUCCI
GO

BEGIN TRY
    BEGIN TRANSACTION
        DECLARE @schema_name VARCHAR(3)
        SET @schema_name = 'PRY'

        IF NOT EXISTS(SELECT name
                      FROM sys.schemas
                      WHERE name = 'PRY')
            BEGIN
                exec ('create schema PRY');
            END

--         IF EXISTS(SELECT *
--                   FROM sys.tables
--                   WHERE name = 'tblCategory'
--                     AND OBJECT_SCHEMA_NAME(object_id) = @schema_name)
--             BEGIN
--                 DROP TABLE PRY.tblCategory
--             END

        CREATE TABLE [PRY].[tblCategory]
        (
            Id        INT PRIMARY KEY IDENTITY (1,1),
            Div       VARCHAR(12)   NOT NULL,
            Name      VARCHAR(1000) NOT NULL,
            Active    BIT           NOT NULL DEFAULT 1,
            CreatedAt DATETIME      NOT NULL DEFAULT GETDATE(),
            UpdatedAt DATETIME      NOT NULL DEFAULT GETDATE(),
            CreatedBy VARCHAR(50)   NOT NULL,
            UpdatedBy VARCHAR(50)   NOT NULL
        )

--         IF EXISTS(SELECT *
--                   FROM sys.tables
--                   WHERE name = 'tblProjectType'
--                     AND OBJECT_SCHEMA_NAME(object_id) = @schema_name)
--             BEGIN
--                 DROP TABLE PRY.tblProjectType
--             END

        CREATE TABLE [PRY].[tblProjectType]
        (
            Id        INT PRIMARY KEY IDENTITY (1,1),
            Div       VARCHAR(12)   NOT NULL,
            Name      VARCHAR(1000) NOT NULL,
            Active    BIT           NOT NULL DEFAULT 1,
            CreatedAt DATETIME      NOT NULL DEFAULT GETDATE(),
            UpdatedAt DATETIME      NOT NULL DEFAULT GETDATE(),
            CreatedBy VARCHAR(50)   NOT NULL,
            UpdatedBy VARCHAR(50)   NOT NULL
        )

--         IF EXISTS(SELECT *
--                   FROM sys.tables
--                   WHERE name = 'tblProject'
--                     AND OBJECT_SCHEMA_NAME(object_id) = @schema_name)
--             BEGIN
--                 DROP TABLE PRY.tblProject
--             END
--
--         IF EXISTS(SELECT *
--                   FROM sys.tables
--                   WHERE name = 'tblMemberType'
--                     AND OBJECT_SCHEMA_NAME(object_id) = @schema_name)
--             BEGIN
--                 DROP TABLE PRY.tblMemberType
--             END

        CREATE TABLE [PRY].[tblMemberType]
        (
            Id        INT PRIMARY KEY IDENTITY (1,1),
            Name      VARCHAR(1000) NOT NULL,
            Active    BIT           NOT NULL DEFAULT 1,
            CreatedAt DATETIME      NOT NULL DEFAULT GETDATE(),
            UpdatedAt DATETIME      NOT NULL DEFAULT GETDATE(),
            CreatedBy VARCHAR(50)   NOT NULL,
            UpdatedBy VARCHAR(50)   NOT NULL
        )
        --
--         DECLARE @schema_name VARCHAR(3)
--         SET @schema_name = 'PRY'

        CREATE TABLE [PRY].[tblProject]
        (
            Id               INT PRIMARY KEY IDENTITY (1,1),
            Div              VARCHAR(10)   NOT NULL,
            Campus           VARCHAR(10)   NOT NULL,
            OwnerPidm        DECIMAL(8, 0) NOT NULL,
            Title            VARCHAR(1000) NOT NULL,
            Objective        VARCHAR(MAX)  NOT NULL,
            Description      VARCHAR(MAX),
            Keywords         VARCHAR(2000),
            IdMicrosoftTeams VARCHAR(100)  NULL,
            IdMicrosoftGroup VARCHAR(100)  NULL,
            Approved         BIT           NULL,
            IdCategory       INT           NOT NULL,
            IdProjectType    INT           NOT NULL,
            ChangeStatusDate DATETIME      NULL,
            Active           BIT           NOT NULL DEFAULT 1,
            CreatedAt        DATETIME      NOT NULL DEFAULT GETDATE(),
            UpdatedAt        DATETIME      NOT NULL DEFAULT GETDATE(),
            CreatedBy        VARCHAR(50)   NOT NULL,
            UpdatedBy        VARCHAR(50)   NOT NULL,
            CONSTRAINT FK_Project_IdCategory FOREIGN KEY (IdCategory) REFERENCES PRY.tblCategory (Id),
            CONSTRAINT FK_Project_IdProjectType FOREIGN KEY (IdProjectType) REFERENCES PRY.tblProjectType (Id),
        )
--
--         IF EXISTS(SELECT *
--                   FROM sys.tables
--                   WHERE name = 'tblProfileProject'
--                     AND OBJECT_SCHEMA_NAME(object_id) = @schema_name)
--             BEGIN
--                 DROP TABLE PRY.tblProfileProject
--             END

        CREATE TABLE [PRY].[tblProfileProject]
        (
            Id              INT PRIMARY KEY IDENTITY (1,1),
            Title           VARCHAR(200) NOT NULL,
            Description     VARCHAR(MAX) NOT NULL,
            NumberVacancies INT          NOT NULL,
            IdProject       INT          NOT NULL,
            Active          BIT          NOT NULL DEFAULT 1,
            CreatedAt       DATETIME     NOT NULL DEFAULT GETDATE(),
            UpdatedAt       DATETIME     NOT NULL DEFAULT GETDATE(),
            CreatedBy       VARCHAR(50)  NOT NULL,
            UpdatedBy       VARCHAR(50)  NOT NULL,
            CONSTRAINT FK_ProfileProject_IdProject FOREIGN KEY (IdProject) REFERENCES PRY.tblProject (Id),
        )

--         IF EXISTS(SELECT *
--                   FROM sys.tables
--                   WHERE name = 'tblProjectMember'
--                     AND OBJECT_SCHEMA_NAME(object_id) = @schema_name)
--             BEGIN
--                 DROP TABLE PRY.tblProjectMember
--             END

        CREATE TABLE [PRY].[tblProjectMember]
        (
            Id               INT PRIMARY KEY IDENTITY (1,1),
            Div              VARCHAR(12)   NOT NULL,
            Campus           VARCHAR(6)    NULL,
            Program          VARCHAR(6)    NULL,
            Department       VARCHAR(6)    NULL,
            Pidm             DECIMAL(8, 0) NOT NULL,
            Approved         BIT           NULL,
            IdMemberType     INT           NOT NULL,
            IdProfileProject INT           NOT NULL,
            IdProject        INT           NOT NULL,
            Remarks          VARCHAR(2000) NULL,
            ChangeStatusDate DATETIME      NULL,
            Active           BIT           NOT NULL DEFAULT 1,
            CreatedAt        DATETIME      NOT NULL DEFAULT GETDATE(),
            UpdatedAt        DATETIME      NOT NULL DEFAULT GETDATE(),
            CreatedBy        VARCHAR(50)   NOT NULL,
            UpdatedBy        VARCHAR(50)   NOT NULL,
            CONSTRAINT FK_tblProjectMember_IdMemberType FOREIGN KEY (IdMemberType) REFERENCES PRY.tblMemberType (Id),
            CONSTRAINT FK_tblProjectMember_IdProfileProject FOREIGN KEY (IdProfileProject) REFERENCES PRY.tblProfileProject (Id),
            CONSTRAINT FK_tblProjectMember_IdProject FOREIGN KEY (IdProject) REFERENCES PRY.tblProject (Id),
        )
    COMMIT TRANSACTION
END TRY
BEGIN CATCH

    --INSTRUCCIONES EN CASO DE ERRORES
    DECLARE
        @ErrorMessage NVARCHAR(4000);
    DECLARE
        @ErrorSeverity INT;
    DECLARE
        @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
           @ErrorSeverity = ERROR_SEVERITY(),
           @ErrorState = ERROR_STATE();

    RAISERROR (
        @ErrorMessage, -- Message text.
        @ErrorSeverity, -- Severity.
        @ErrorState -- State.
        );
    ROLLBACK TRANSACTION
END CATCH