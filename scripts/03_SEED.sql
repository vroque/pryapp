USE BDUCCI
GO

BEGIN TRY
    BEGIN TRANSACTION
        --         TRUNCATE TABLE BDUCCI.PRY.tblCategory

        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Fin de la pobreza', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Hambre cero', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Salud y bienestar', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Educación de calidad', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Igualdad de género', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Agua limpia y saneamiento', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Energía asequible y no contaminante', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Trabajo decente y crecimiento económico', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Industria, innovación e infraestructura', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Reducción de las desigualdades', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Ciudades y comunidades sostenibles', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Producción y consumo responsables', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Acción por el clima', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Vida submarina', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Vida de ecosistemas terrestres', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Paz, justicia e instituciones sólidas', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblCategory (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Alianza para lograr los objetivos', 1, GETDATE(), GETDATE(), 'admin', 'admin');

--         TRUNCATE TABLE BDUCCI.PRY.tblCategory

        INSERT INTO BDUCCI.PRY.tblProjectType (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'De investigación', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblProjectType (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Aplicado', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblProjectType (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'Tecnológico', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblProjectType (Div, Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('UCCI', 'De emprendimiento', 1, GETDATE(), GETDATE(), 'admin', 'admin');

--         TRUNCATE TABLE BDUCCI.PRY.tblMemberType

        INSERT INTO BDUCCI.PRY.tblMemberType (Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('Owner', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblMemberType (Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('Leader', 1, GETDATE(), GETDATE(), 'admin', 'admin');
        INSERT INTO BDUCCI.PRY.tblMemberType (Name, Active, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
        VALUES ('Member', 1, GETDATE(), GETDATE(), 'admin', 'admin');

        SET IDENTITY_INSERT PRY.tblProject ON
        INSERT INTO BDUCCI.PRY.tblProject (Id, Div, Campus, OwnerPidm, Title, Objective, Description, Keywords,
                                           IdMicrosoftTeams, IdCategory, IdProjectType, CreatedBy, UpdatedBy)
        VALUES (0, 'UCCI', 'S01', 0, 'Por defecto', '-', '-', '-', null, 1, 1, 'admin', 'admin')
        SET IDENTITY_INSERT PRY.tblProject OFF

        SET IDENTITY_INSERT PRY.tblProfileProject ON
        INSERT INTO BDUCCI.PRY.tblProfileProject (Id, Title, Description, NumberVacancies, IdProject, CreatedBy,
                                                  UpdatedBy)
        VALUES (0, 'Owner', 'Por defecto', 0, 0, 'admin', 'admin')
        SET IDENTITY_INSERT PRY.tblProfileProject OFF
    COMMIT TRANSACTION
END TRY
BEGIN CATCH

    --INSTRUCCIONES EN CASO DE ERRORES
    DECLARE
        @ErrorMessage NVARCHAR(4000);
    DECLARE
        @ErrorSeverity INT;
    DECLARE
        @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
           @ErrorSeverity = ERROR_SEVERITY(),
           @ErrorState = ERROR_STATE();

    RAISERROR (
        @ErrorMessage, -- Message text.
        @ErrorSeverity, -- Severity.
        @ErrorState -- State.
        );
    ROLLBACK TRANSACTION
END CATCH