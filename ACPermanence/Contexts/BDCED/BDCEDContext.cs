using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using ACPermanence.DataBases.BDCED.CED.StoredProcedures;

namespace ACPermanence.Contexts.BDCED
{
    /// <summary>
    /// Contexto para la base de datos de encuestas: BDCED
    /// </summary>
    public class BDCEDContext : DbContext
    {
        public BDCEDContext() : base("name=BDCED")
        {
        }

        #region BDCED

        #region BDCED Stored Procedures

        /// <summary>
        /// Lista de encuestas activas de un estudiante
        /// </summary>
        /// <param name="div">IDDependencia</param>
        /// <param name="campus">Código de sede</param>
        /// <param name="studentId">Código de estudiante</param>
        /// <returns>Lista de encuestas activas de un estudiante</returns>
        public IEnumerable<CEDspListaEncuestasActivaByAlumno> ced_encuesta_activa_alumno(string div, string campus,
            string studentId)
        {
            return Database.SqlQuery<CEDspListaEncuestasActivaByAlumno>(
                "CED.sp_ListaEncuestasActiva_by_alumno, @c_IDDependencia, @c_IDSede, @c_IDAlumno",
                new SqlParameter("@c_IDDependencia", div),
                new SqlParameter("@c_IDSede", campus),
                new SqlParameter("@c_IDAlumno", studentId));
        }

        #endregion

        #endregion
    }
}