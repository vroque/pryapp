﻿using System;
using ACPermanence.DataBases.NEXO.ACCESS;
using ACPermanence.DataBases.NEXO.BUW;
using ACPermanence.DataBases.NEXO.CAU;
using ACPermanence.DataBases.NEXO.CAU.procedures;
using ACPermanence.DataBases.NEXO.CAU.views;
using ACPermanence.DataBases.NEXO.VUC.views;
using ACPermanence.DataBases.NEXO.DIM;
using ACPermanence.DataBases.NEXO.OPP;
using ACPermanence.DataBases.NEXO.UBIGEO;
using ACPermanence.DataBases.NEXO.LDR;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using ACPermanence.DataBases.NEXO.ADRYAN.Synonyms;
using ACPermanence.DataBases.NEXO.VUC;
using ACPermanence.DataBases.NEXO.CIC;
using ACPermanence.DataBases.NEXO.CIC.Views;
using ACPermanence.DataBases.NEXO.OEA;
using ACPermanence.DataBases.NEXO.TCU;

namespace ACPermanence.Contexts.NEXO
{
    /// <summary>
    /// Contexto para la base de datos que hace el nexo con
    /// BANNER
    /// </summary>
    public class NEXOContext : DbContext
    {
        public NEXOContext() : base("name=BANNERNexo")
        {
        }

        #region DIMENSIONS

        /// <summary>
        /// Todas las Personas
        /// Retirado para evitar confunciones 
        /// Con fé se reusara una vez no se dependa de BDUCCI
        /// </summary>
        public virtual DbSet<DIMTblPerson> dim_persons { get; set; }

        /// <summary>
        /// Carreras 
        /// </summary>
        [Obsolete("Use tbl_SZVMAJR instead", true)]
        public virtual DbSet<DIMTblProgram> dim_programs { get; set; }

        /// <summary>
        /// Facultades
        /// </summary>
        public virtual DbSet<DIMTblCollege> dim_colleges { get; set; }

        /// <summary>
        /// Relacion entre colleges(facultades) y programs(carreras)
        /// </summary>
        public virtual DbSet<DIMTblCollegeProgram> dim_colleges_programs { get; set; }

        /// <summary>
        /// Asignaturas
        /// </summary>
        public virtual DbSet<DIMTblSubjectCourse> dim_courses { get; set; }

        /// <summary>
        /// Sedes y filiales
        /// </summary>
        public virtual DbSet<DIMTblCampus> dim_campus { get; set; }


        /// <summary>
        /// Modalidades (regular, virtual, GQT, etc)
        /// </summary>
        public virtual DbSet<DIMTblDepartament> dim_departaments { get; set; }

        /// <summary>
        /// Dependencias (UCCI, ISTPC)
        /// </summary>
        public virtual DbSet<DIMTblDivs> dim_divs { get; set; }

        /// <summary>
        /// Niveles (pregrado, posgrado, etc)
        /// </summary>
        public virtual DbSet<DIMTblLevl> dim_levls { get; set; }

        /// <summary>
        /// Sub periodos
        /// </summary>
        public virtual DbSet<DIMTblPartTerm> dim_part_terms { get; set; }

        /// <summary>
        /// SUSTITUIDO POR LA TABLA SZVADMT DE BANNER
        /// Modalidadles de Admision (beca 18, examen, etc)
        /// </summary>
        ///public virtual DbSet<DIMTblAdmType> dim_admision_types { get; set; }
        /// <summary>
        /// planes de estudio o algo asi
        /// </summary>
        public virtual DbSet<DIMTblSubject> dim_subjects { get; set; }

        /// <summary>
        /// Peridodos (201620, 201700, 201710, etc)
        /// </summary>
        public virtual DbSet<DIMTblTerm> dim_terms { get; set; }


        /// <summary>
        /// Postulaciones por alumno
        /// REMOVE
        /// </summary>
        public virtual DbSet<DIMTblPostulant> dim_student_postulations { get; set; }

        /// <summary>
        /// alumnos egresados
        /// </summary>
        public virtual DbSet<DIMTblGraduate> dim_student_graduations { get; set; }

        /// <summary>
        /// Informacion de los alumnos matriculados por periodo
        /// </summary>
        public virtual DbSet<DIMTblStudentEnrollmentByPeriod> dim_student_enrollments { get; set; }

        /// <summary>
        /// Info de estudiantes matriculados por período y según asignaturas
        /// </summary>
        public virtual DbSet<DIMTblMatriculadosPeriodo> dim_matriculados_periodo { get; set; }

        /// <summary> 
        /// Tabla Horario de Docentes
        /// Para uso en las pantallas del CAU x(
        /// </summary> 
        public virtual DbSet<DIMtblTeachingHoursTerm> dim_teacher_schedule { get; set; }

        #endregion DIMENSIONS

        #region CAU

        /// <summary>
        /// Parentesco
        /// </summary>
        public virtual DbSet<CAUKinship> cau_kinship { get; set; }

        /// <summary>
        /// Relación entre Estudiante y Apoderado
        /// </summary>
        public virtual DbSet<CAUPersonApoderado> cau_person_apoderado { get; set; }

        /// <summary>
        /// solicitudes
        /// </summary>
        public virtual DbSet<CAUTblRequest> cau_requests { get; set; }

        /// <summary>
        /// Hostorial de solicitudes
        /// </summary>
        public virtual DbSet<CAUTblRequestLog> cau_request_logs { get; set; }

        /// <summary> 
        /// Nombre de los estados que se utilizan en las solicitudes 
        /// </summary> 
        public virtual DbSet<CAUtblRequestState> cau_request_state { get; set; }

        /// <summary>
        /// tipos de Documentos del CAU
        /// </summary>
        public virtual DbSet<CAUTblDocument> cau_documents { get; set; }

        /// <summary> 
        /// Requisitos de trámite de los documentos que se solicitan 
        /// </summary> 
        public virtual DbSet<CAUtblDocumentRequirement> cau_document_requirements { get; set; }

        /// <summary>
        /// Posibles estados de un documento
        /// </summary>
        public virtual DbSet<CAUTblDocumentState> cau_document_states { get; set; }

        /// <summary>
        /// Costo de los documentos que se solicitan
        /// </summary>
        public virtual DbSet<CAUTblDdocumentCost> cau_document_costs { get; set; }

        /// <summary>
        /// Firmantes
        /// </summary>
        public virtual DbSet<CAUTblSignatory> cau_signatories { get; set; }

        /// <summary>
        /// Firmante por documento
        /// </summary>
        public virtual DbSet<CAUTblSignatoryDocument> cau_signatories_documents { get; set; }

        /// <summary>
        /// Fecha de Term para documentos
        /// </summary>
        public virtual DbSet<CAUTblTermDocument> cau_tem_documents { get; set; }

        /// <summary>
        /// Fecha de períodos académicos UC para uso de la aplicación
        /// </summary>
        public virtual DbSet<CAUTerm> cau_term { get; set; }

        /// <summary>
        /// ARchuvis adjuntos para la solicitud
        /// </summary>
        public virtual DbSet<CAUTblRequestAttachment> cau_request_attachments { get; set; }

        public virtual DbSet<CAUTblRequirement> cau_requirements { get; set; }

        public virtual DbSet<CAUTblRequirementCategory> cau_requirement_categories { get; set; }

        public virtual DbSet<CAUTblRequirementFiles> cau_requirement_files { get; set; }

        /// <summary>
        /// Configuración para la aplicación
        /// </summary>
        public virtual DbSet<CAUSettings> cau_settings { get; set; }

        /// <summary>
        /// Tipos de configuración
        /// </summary>
        public virtual DbSet<CAUSettingsType> cau_settings_type { get; set; }

        /// <summary>
        /// Detalle de configuración
        /// </summary>
        public virtual DbSet<CAUSettingsDetail> cau_settings_detail { get; set; }

        // Display Block
        public virtual DbSet<CAUTblDisplayBlock> cau_display_block { get; set; }

        // Encuestas satisfaccion CAU
        public virtual DbSet<CAUPoll> cau_poll { get; set; }
        public virtual DbSet<CAUPollUser> cau_poll_user { get; set; }
        public virtual DbSet<CAUPollQuestion> cau_poll_question { get; set; }
        public virtual DbSet<CAUPollChoice> cau_poll_choice { get; set; }
        public virtual DbSet<CAUPollChoiceDetail> cau_poll_choice_detail { get; set; }

        // Oficinas registrados para el uso del cau
        public virtual DbSet<CAUOffices> CAU_Offices { get; set; }
        public DbSet<CAUIncident> cau_incident { get; set; }
        public DbSet<CAUIncidentCategory> cau_incident_category { get; set; }
        public DbSet<CAUIncidentLog> cau_incident_log { get; set; }
        public DbSet<CAUIncidentLogType> cau_incident_log_type { get; set; }
        public DbSet<CAUIncidentRequestSource> cau_incident_request_source { get; set; }
        public DbSet<CAUIncidentStatus> cau_incident_status { get; set; }
        public DbSet<CAUIncidentSubCategory> cau_incident_sub_category { get; set; }
        public DbSet<CAUIncidentSubCategoryFunctionalUnit> cau_incident_sub_category_functional_unit { get; set; }
        public DbSet<CAUIncidentType> cau_incident_type { get; set; }

        /// <summary>
        /// Información de estudiantes UC matriculados en un determinado período.
        /// La data se llena a demanda desde BANNER. 
        /// </summary>
        public DbSet<CAUtblStudentCICUC> cau_tbl_student_cic_uc { get; set; }

        #region CAU Stored Procedure

        /// <summary>
        /// Horario de Estudiantes UC
        /// </summary>
        /// <param name="studentID">Código de Estudiante UC</param>
        /// <param name="period">Período Académico UC</param>
        /// <returns></returns>
        public IEnumerable<CAUgetStudentSchedule> cau_student_schedule(string studentID, string period)
        {
            return this.Database.SqlQuery<CAUgetStudentSchedule>("CAU.getStudentSchedule @studentID, @period",
                new SqlParameter("@studentID", studentID),
                new SqlParameter("@period", period)
            );
        }

        /// <summary>
        /// Horario de Docentes UC
        /// </summary>
        /// <param name="teacherID">Código de Docente UC</param>
        /// <param name="period">Período Académico UC</param>
        /// <returns></returns>
        public IEnumerable<CAUgetTeacherSchedule> cau_teacher_schedule(string teacherID, string period)
        {
            return this.Database.SqlQuery<CAUgetTeacherSchedule>("CAU.getTeacherSchedule @teacherID, @period",
                new SqlParameter("@teacherID", teacherID),
                new SqlParameter("@period", period)
            );
        }

        /// <summary>
        /// Lista de encuestas activas de un estudiante
        /// </summary>
        /// <param name="div">IDDependencia</param>
        /// <param name="campus">Código de sede</param>
        /// <param name="studentId">Código de estudiante</param>
        /// <returns>Lista de encuestas activas de un estudiante</returns>
        public IEnumerable<CAUspListaEncuestasActivaByAlumno> cau_encuesta_activa_alumno(string div, string campus,
            string studentId)
        {
            return Database.SqlQuery<CAUspListaEncuestasActivaByAlumno>(
                "CAU.sp_ListaEncuestasActiva_by_alumno @c_IDDependencia, @c_IDSede, @c_IDAlumno",
                new SqlParameter("@c_IDDependencia", div),
                new SqlParameter("@c_IDSede", campus),
                new SqlParameter("@c_IDAlumno", studentId));
        }

        #endregion CAU Stored Procedure

        #region VIEWS

        /// <summary>
        /// view of graduates
        /// </summary>
        public virtual DbSet<CAUv_Graduate> cau_view_graduate { get; set; }

        #endregion VIEWS

        #endregion CAU

        #region VUC

        #region VUC Tables        

        /// <summary>
        /// Ejes
        /// </summary>
        public virtual DbSet<VUCAxi> vuc_axis { get; set; }

        /// <summary>
        /// Actividades
        /// </summary>
        public virtual DbSet<VUCActivity> vuc_activities { get; set; }

        /// <summary>
        /// Almacena los tipos de convalidaciones de una actividad
        /// </summary>
        public virtual DbSet<VUCActivityConvalidation> vuc_activity_convalidation { get; set; }

        /// <summary>
        /// Tipos de actividades
        /// </summary>
        public virtual DbSet<VUCActivityType> vuc_activity_types { get; set; }

        /// <summary>
        /// Reconocimientos
        /// </summary>
        public virtual DbSet<VUCRecognition> vuc_recognition { get; set; }

        /// <summary>
        /// Relación entre reconocimientos y estudiantes
        /// </summary>
        public virtual DbSet<VUCRecognitionStudent> vuc_recognition_student { get; set; }

        /// <summary>
        /// Programación de actividades
        /// </summary>
        public virtual DbSet<VUCProgrammingActivity> vuc_programming_activity { get; set; }

        /// <summary>
        /// Tipos de horarios
        /// </summary>
        public virtual DbSet<VUCScheduleType> vuc_schedule_type { get; set; }

        /// <summary>
        /// Seguimiento de programas
        /// </summary>
        public virtual DbSet<VUCTracingProgram> vuc_tracing_program { get; set; }

        /// <summary>
        /// Inscritos en actividades
        /// </summary>
        public virtual DbSet<VUCEnrolledProgramActivity> vuc_enrolled_activity { get; set; }

        /// <summary>
        /// Tipos de convalidaciones
        /// </summary>
        public virtual DbSet<VUCConvalidationType> vuc_convalidation_type { get; set; }

        /// <summary>
        /// Almacena los tipos de planes de una programación
        /// </summary>
        public virtual DbSet<VUCProgrammingActivityCurriculum> vuc_programming_activity_Curriculum { get; set; }

        #endregion

        #region VUC Views

        /// <summary>
        /// View of Curruculum
        /// </summary>
        public virtual DbSet<VUCv_Curriculum> vuc_view_curriculum { get; set; }

        #endregion

        #endregion

        #region BUW

        /// <summary>
        /// Log del proceso de categorizacion automatica
        /// </summary>
        public virtual DbSet<BUWTblCategorizationLog> buw_categorization_logs { get; set; }

        /// <summary>
        /// Parametros configurables para la categorización automatica
        /// </summary>
        public virtual DbSet<BUWTblCategorizeParameter> buw_categorize_parameters { get; set; }

        /// <summary>
        /// Parametros configurables para la categorización automatica
        /// </summary>
        public virtual DbSet<BUWTblCategoryRange> buw_category_ranges { get; set; }

        #endregion BUW

        #region OPP

        public virtual DbSet<OPPTblPracticeDocument> opp_practices_documents { get; set; }

        #endregion OPP

        #region ADMIN

        /// <summary>
        /// Encuestas
        /// </summary>
        public virtual DbSet<CAUSurvey> cau_survey { get; set; }

        /// <summary>
        /// Términos y Condiciones
        /// </summary>
        public virtual DbSet<CAUTermsAndConditions> cau_terms_conditions { get; set; }

        /// <summary>
        /// Tipo de Términos y Condiciones
        /// </summary>
        public virtual DbSet<CAUTermsAndConditionsType> cau_terms_and_conditions_type { get; set; }

        /// <summary>
        /// Encuestados
        /// </summary>
        public virtual DbSet<CAUSurveyed> cau_surveyed { get; set; }

        /// <summary>
        /// Términos y Condiciones aceptados
        /// </summary>
        public virtual DbSet<CAUAcceptedTerms> cau_accepted_terms { get; set; }

        #endregion ADMIN

        #region ACCESS

        /// <summary>
        /// Aplicaciones registradas
        /// </summary>
        public virtual DbSet<ACCESSTblApplication> access_applications { get; set; }

        /// <summary>
        /// Grupos para accesos a las aplicaciones
        /// </summary>
        public virtual DbSet<ACCESSTblGroupModule> access_group_modules { get; set; }

        /// <summary>
        /// Usuarios y sus grupos o
        /// grupos y sus usaurios
        /// **cuenta con sede
        /// </summary>
        public virtual DbSet<ACCESSTblGroupUser> access_group_users { get; set; }

        /// <summary>
        /// Sedes
        /// </summary>
        public virtual DbSet<ACCESSTblGroup> access_group { get; set; }

        /// <summary>
        /// <summary>
        /// Tabla de usuarios segun el pidm
        /// </summary>
        public virtual DbSet<ACCESSTblUser> access_users { get; set; }

        /// <summary>
        /// Tabla de Modulos de una aplicación
        /// </summary>
        public virtual DbSet<ACCESSTblModule> access_modules { get; set; }

        /// <summary>
        /// Tabla de Menus de la app
        /// </summary>
        public virtual DbSet<ACCESSTblMenu> access_menus { get; set; }

        #endregion ACCESS

        #region UBIGEO

        /// <summary>
        /// Tabla de Deppartamentos
        /// </summary>
        public virtual DbSet<UBIGEOTblCountry> ubigeo_country { get; set; }

        /// <summary>
        /// Tabla de Deppartamentos
        /// </summary>
        public virtual DbSet<UBIGEOTblDepartamento> ubigeo_departament { get; set; }

        /// <summary>
        /// Tabla de Provincias
        /// </summary>
        public virtual DbSet<UBIGEOTblProvincia> ubigeo_province { get; set; }

        /// <summary>
        /// Tabla de Distritos
        /// </summary>
        public virtual DbSet<UBIGEOTblDistrito> ubigeo_district { get; set; }

        #endregion UBIGEO

        #region LDR

        /// <summary>
        ///     tabla de libro de reclamaciones
        /// </summary>
        public virtual DbSet<LDRtblPageClaimBook> ldr_TblPageClaimBook { get; set; }

        /// <summary>
        /// Tabla donde se registran las respuestas
        /// </summary>
        public virtual DbSet<LDRtblAnswerPage> ldr_tblAnswerPage { get; set; }

        /// <summary>
        ///     tabla donde se registran las derivaciones
        /// </summary>
        public virtual DbSet<LDRtblDerivationArea> ldr_tblDerivationArea { get; set; }

        /// <summary>
        ///     tabla donde se registran los encargados
        /// </summary>
        public virtual DbSet<LDRtblStaffInvolved> ldr_tblStaffInvolved { get; set; }

        /// <summary>
        /// tabla donde se guardan rutas de los archivos de los libros de reclamaciones
        /// </summary>
        public virtual DbSet<LDRtblFile> ldr_tblFile { get; set; }

        /// <summary>
        ///     tabla donde se guardan los resultados de la encuesta de satisfacción
        /// </summary>
        public virtual DbSet<LDRtblSatisfactionSurveyClaim> ldr_tblSatisfactionSurvey { get; set; }

        /// <summary>
        ///     Tabla donde se guardan el seguimiento de una queja
        /// </summary>
        public virtual DbSet<LDRtblBookTracing> ldr_tblBookTracing { get; set; }

        /// <summary>
        ///    Tabla donde se guarda el tipo de seguimiento
        /// </summary>
        public virtual DbSet<LDRtblTypeTracing> ldr_tblTypeTracing { get; set; }

        #endregion LDR

        #region  ADRYAN

        public virtual DbSet<ADRYANtbl_Unidad_Negocio> adryan_tbl_unidad_negocio { get; set; }
        public virtual DbSet<ADRYANtbl_unidad_funcional> adryan_tbl_unidad_funcional { get; set; }
        public virtual DbSet<ADRYANtbl_puesto_compania> adryan_tbl_puesto_compania { get; set; }
        public virtual DbSet<ADRYANvTrabajador> adryan_v_trabajador { get; set; }

        #endregion

        #region CIC

        /**************
         * CIC TABLES *
         **************/

        /// <summary>
        /// Documentos emitidos por el CIC
        /// </summary>
        public DbSet<CICCertificates> cic_certificates { get; set; }

        /// <summary>
        /// Matriculados en el CIC
        /// </summary>
        public DbSet<CICEnrolled> cic_enrolled { get; set; }

        /// <summary>
        /// Nivel de Idiomas en el CIC
        /// </summary>
        public DbSet<CICLanguageLevel> cic_language_level { get; set; }

        /// <summary>
        /// Programas del CIC
        /// Regular, Escolar, Virtual
        /// </summary>
        public DbSet<CICLanguageProgram> cic_language_program { get; set; }

        /// <summary>
        /// Estudiantes que pueden solicitar Constancia de Acreditación de Idioma Extranjero
        /// </summary>
        public DbSet<CICProofOfForeignLanguage> cic_caie { get; set; }

        /// <summary>
        /// Tipos de configuración para el CIC
        /// </summary>
        public DbSet<CICSettingsType> cic_settings_type { get; set; }

        /// <summary>
        /// Configuraciones para el CIC
        /// </summary>
        public DbSet<CICSettings> cic_settings { get; set; }

        /*************
         * CIC VIEWS *
         *************/

        #region CIC Views

        /// <summary>
        /// Estado de pago para tramitar CAIE
        /// </summary>
        public DbSet<CICProofOfForeignLanguagePaymentStatus> cic_caie_payment_status { get; set; }

        /// <summary>
        /// Para CAIE a demanda
        /// </summary>
        public DbSet<CICProofOfForeignLanguageOnDemand> cic_caie_on_demand { get; set; }

        /// <summary>
        /// Nivel de idiomas de un estudiante
        /// </summary>
        public DbSet<CICStudentLevel> cic_student_level { get; set; }

        #endregion CIC Views

        #endregion CIC

        #region OEA

        #region tables

        /// <summary>
        /// Tabla que contiene los Nrc programados
        /// </summary>
        public virtual DbSet<OEANrcForSubstituteExam> oea_Nrc_for_substitute_exam { get; set; }

        /// <summary>
        /// Fechas de un Nrc
        /// </summary>
        public virtual DbSet<OEANrcProgrammned> oea_nrc_programmned { get; set; }

        /// <summary>
        /// Examenes que estudiante programo
        /// </summary>
        public virtual DbSet<OEASubstituteExamProgrammed> oea_substitute_exam_programmed { get; set; }

        /// <summary>
        /// Estados posibles de reclamo de notas
        /// </summary>
        public virtual DbSet<OEANoteClaimStatus> OEA_Note_Claim_Status { get; set; }
        /// <summary>
        /// Reclamo de notas
        /// </summary>
        public virtual DbSet<OEANoteClaim> OEA_Note_Claim { get; set; }
        /// <summary>
        /// Reportes de reclamo de nota
        /// </summary>
        public virtual DbSet<OEAReportNoteClaim> OEA_Report_Note_Claim { get; set; }
        /// <summary>
        /// Archivos de reclamo o informe de nota
        /// </summary>
        public virtual DbSet<OEAFileNoteClaim> OEA_File_Note_Claim { get; set; }
        #endregion tables

        #region StoreProcedures

        /// <summary>
        /// Generar deuda por examen sustitutorio
        /// </summary>
        /// <param name="div">Dependencia</param>
        /// <param name="campus">Sede</param>
        /// <param name="term">Periodo</param>
        /// <param name="programId">Código de escuela</param>
        /// <param name="pidm">Código de estudiante</param>
        /// <returns></returns>
        public IEnumerable<object> oea_spGenerateDebtSubstituteExamByStudent(string @div, string @campus, string @term,
            string @programId, decimal @pidm)
        {
            return this.Database.SqlQuery<object>(
                "OEA.spGenerateDebtSubstituteExamByStudent @div,@campus,@term,@programId,@pidm",
                new SqlParameter("@div", div),
                new SqlParameter("@campus", campus),
                new SqlParameter("@term", term),
                new SqlParameter("@programId", programId),
                new SqlParameter("@pidm", pidm)
            );
        }

        #endregion StoreProcedures

        #endregion

        #region TCU
        /// <summary>
        /// Tabla de carnets (TI lo llena)
        /// </summary>
        public virtual DbSet<TCUtblCarnet> tcu_tbl_carnet { get; set; }
        #endregion TCU
    }
}
