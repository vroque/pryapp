﻿using ACPermanence.DataBases.DBUCCI.ATE;
using ACPermanence.DataBases.DBUCCI.CAJ;
using ACPermanence.DataBases.DBUCCI.CAJ.StoredProcedures;
using ACPermanence.DataBases.DBUCCI.COU;
using ACPermanence.DataBases.DBUCCI.DBO;
using ACPermanence.DataBases.DBUCCI.DBO.procedures;
using ACPermanence.DataBases.DBUCCI.DBO.synonims;
using ACPermanence.DataBases.DBUCCI.DBO.views;
using ACPermanence.DataBases.DBUCCI.ADM;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using ACPermanence.DataBases.DBUCCI.ATE.StoredProcedures;
using ACPermanence.DataBases.DBUCCI.CIC;
using ACPermanence.DataBases.DBUCCI.RAU;
using ACPermanence.DataBases.DBUCCI.BNE;
using ACPermanence.DataBases.DBUCCI.BNE.StoredProcedure;
using ACPermanence.DataBases.DBUCCI.OPP;
using ACPermanence.DataBases.DBUCCI.PRS;
using ACPermanence.DataBases.DBUCCI.OEA.StoredProcedures;
using ACPermanence.DataBases.DBUCCI.BANNER;
using System.Linq;
using ACPermanence.DataBases.DBUCCI.PRY;
using ACPermanence.DataBases.NEXO.CAU;

namespace ACPermanence.Contexts.BDUCCI
{

    public class DBOContext : DbContext
    {
        public DBOContext() : base("name=DBUCCI") { }
            
        #region Tables DBO
        
        public DbSet<DBOtblCronogramas> tbl_cronogramas { get; set; }
        
        public DbSet<DBOTblDisplayConfiguracion> tbl_display_configuracion { get; set; }
        public DbSet<DBOTblDisplayCronograma> tbl_display_cronograma { get; set; }
        public DbSet<DBOTblDisplayCronoTipInformacion> tbl_display_crono_tipo_informacion { get; set; }
        public DbSet<DBOTblDisplayTipoInformacion> tbl_display_tipo_informacion { get; set; }

        public DbSet<DBOTblPersona> tbl_persona { get; set; }
        public DbSet<DBOTblPostulante> tbl_postulante { get; set; }
        public DbSet<DBOTblReglasEgresado> tbl_reglas_egresado { get; set; }
        public DbSet<DBOTblColegio> tbl_colegio { get; set; }
        /// <summary>
        /// Modalidad de Postulación
        /// </summary>
        public DbSet<DBOTblModalidadPostu> tbl_modalidad_postu { get; set; }
        /// <summary>
        /// Egresados UC
        /// </summary>
        public DbSet<DBOTblAlumnoEgresado> tbl_alumno_egresado { get; set; }
        public DbSet<DBOTblEscuela> tbl_escuela { get; set; }
        public DbSet<DBOTblPlanEstudio> tbl_plan_estudio { get; set; }
        public DbSet<DBOTblFacultad> tbl_facultad { get; set; }
        public DbSet<DBOTblPersonaAlumno> tbl_persona_alumno { get; set; }
        /// <summary>
        /// tabla de equivalencia
        /// </summary>
        public DbSet<DBOTblAsignaturaAcadUCONVA> tbl_asignatura_acad_u_conva { get; set; }
        public DbSet<DBOTblPlanEstudioDet> tbl_plan_estudio_det { get; set; }
        public DbSet<DBOtblAsignaturaAcad> tbl_asignatura_acad { get; set; }
        public DbSet<DBOTblMatricula> tbl_matricula { get; set; }
        // Ficha Socieconomica
        public virtual DbSet<DBOTblFichaSocEco> tbl_ficha_socioeconomica { get; set; }
        public DbSet<DBOTblRelacionFamiliar> tbl_relacion_familiar { get; set; }
        public DbSet<DBOTblPerAcad> tbl_per_acad { get; set; }
        public DbSet<DBOTblAlumnoEscala> tbl_alumno_escala { get; set; }

        // simulador de pago
        public DbSet<DBOTblCategoriaPens> tbl_categoria_pension { get; set; }
        public DbSet<DBOTblConceptos> tbl_conceptos { get; set; }
        public DbSet<DBOtblGrupoConceptos> tbl_grupo_conceptos { get; set; }
        public DbSet<DBOTblSeccionC> tbl_seccion_c { get; set; }
        public DbSet<DBOTblCtaCorriente> tbl_cta_corriente { get; set; }
        /// <summary>
        /// Tabla de Estado de alumno
        /// </summary>
        public virtual DbSet<DBOtblAlumnoEstado> tbl_alumno_estado { get; set; }

        /// <summary>
        /// Constancias Emitidas por el CIC
        /// </summary>
        public DbSet<DBOtblConstanciaCIC> tbl_constancia_cic { get; set; }
        /// <summary>
        /// Notas CIC
        /// </summary>
        public DbSet<DBOtblNotasEXT> tbl_notas_ext { get; set; }
        
        /// <summary>
        /// Estilo para plantilla ce Certificado de Estudios CIC 
        /// </summary>
        public DbSet<DBOtblSeccionCDocenteEXT> tbl_seccion_docente_ext { get; set; }
        
        public DbSet<DBOTblModalidadPostuRecruiter> tbl_equivalencia_modalidad_admision { get; set; }
        /// <summary>
        ///  cabecera de matricula
        /// </summary>
        public DbSet<DBOTblMatriculaDoc> tbl_matricula_doc { get; set; }

        /// <summary>
        /// Secciones academicas APEC
        /// </summary>
        public DbSet<DBOTblSeccionAcad> tbl_seccion_acad { get; set; }

        /// <summary>
        /// Programacion de horarios APEC
        /// </summary>
        public DbSet<DBOTblProgHorario> tbl_programacion_horario { get; set; }
        /// Tabla de examen sustitutorio
        /// </summary>

        public DbSet<DBOTblExamenSustitutorioBANNER> tbl_examen_sustitutorio_banner { get; set; }
        
        #endregion Tables DBO
        #region CAJ

        #region Tables
        public DbSet<CAJtblEmisionCarnetC5> caj_tbl_emision_carnet_c5 { get; set; }
        public DbSet<CAJtblEmisionCarnetC5Detalle> caj_tbl_emision_carnet_c5_detalle { get; set; }
        #endregion Tables

        #region Stores Procedures
        public IEnumerable<CAJSP_getGroupedDebtsPIDM> get_grouped_debts(string IDDependencia, string IDPeracad, int Pidm)
        {
            return Database.SqlQuery<CAJSP_getGroupedDebtsPIDM>(
                "CAJ.SP_getGroupedDebtsPIDM @IDDependencia, @IDPeracad, @Pidm",
                new SqlParameter("@IDDependencia", IDDependencia),
                new SqlParameter("@IDPeracad", IDPeracad),
                new SqlParameter("@Pidm", Pidm));
        }

        #endregion Stores Procedures
        #endregion CAJ
        #region Tables ATE
        public DbSet<ATETblDocumento> ate_tbl_documento { get; set; }
        public DbSet<ATETblSolicitud> ate_tbl_solicitud { get; set; }
        public DbSet<ATETblSolicitudEstado> ate_tbl_solicitud_estado { get; set; }
        
        #endregion Tables ATE

        #region StoreProcedures ATE

        /// <summary>
        /// Registro de nueva persona (apoderado) en dbo.tblPersona
        /// </summary>
        /// <param name="idPersona">IDPersona</param>
        /// <param name="apPat">Apellido Paterno</param>
        /// <param name="apMat">Apellido Materno</param>
        /// <param name="nombres">Nombres</param>
        /// <param name="genero">Género</param>
        /// <param name="fechaNacimiento">Fecha Nacimiento</param>
        /// <param name="dni">DNI</param>
        /// <param name="ce">Carnet de Extranjería</param>
        /// <returns></returns>
        public IEnumerable<ATEsetPersona> set_persona(string idPersona, string apPat, string apMat, string nombres,
            string genero, string fechaNacimiento, string dni, string ce)
        {
            return Database.SqlQuery<ATEsetPersona>(
                "ATE.setPersona @IdPersona, @Appat, @ApMat, @Nombres, @Sexo, @FecNac, @DNI, @CE",
                new SqlParameter("@IdPersona", idPersona),
                new SqlParameter("@Appat", apPat),
                new SqlParameter("@ApMat", apMat),
                new SqlParameter("@Nombres", nombres),
                new SqlParameter("@Sexo", genero),
                new SqlParameter("@FecNac", fechaNacimiento),
                new SqlParameter("@DNI", dni),
                new SqlParameter("@CE", ce));
        }

        #endregion StoreProcedures ATE
        
        #region Tables COU
        public DbSet<COUTblValidacionDocumento> cou_tbl_validacion_documento { get; set; }
        public DbSet<COUTblValidacionAsignatura> cou_tbl_validacion_asignatura { get; set; }
        public DbSet<COUTblValidacionMatricula> cou_tbl_validacion_matricula { get; set; }
        #endregion Tables COU
        #region  Tables RAU
        public DbSet<RAUTblTercioQuinto> rau_tbl_tercio_quinto { get; set; }
        public DbSet<RAUTblTercioQuintoPromo> rau_tbl_tercio_quinto_promo { get; set; }
        #endregion  Tables RAU
        #region Tables BNE
        public DbSet<BNETblConvenios> bne_tbl_convenios { get; set; }
        public DbSet<BNETblConveniosDetalle> bne_tbl_convenios_detalle { get; set; }
        public DbSet<BNETblDescuentos> bne_tbl_descuentos { get; set; }
        public DbSet<BNETblGradFamiliaridad> bne_tbl_grad_familiaridad { get; set; }
        public DbSet<BNETblAlumnoBeneficio> bne_tbl_alumno_beneficio { get; set; }
        #endregion Tables BNE
        #region Views DBO
        public DbSet<DBODATAlumno> view_alumnos { get; set; }
        /// <summary>
        /// View con datos básicos de estudiante (incluye pidm=IDPersonaN)
        /// </summary>
        public DbSet<DBODATAlumnoBasico> view_alumno_basico { get; set; }

        /// <summary>
        /// Vista de doncetes
        /// </summary>
        public DbSet<DBODatDocente> view_docentes { get; set; }

        #endregion Views DBO

        #region StoreProcedures DBO

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IDSede"></param>
        /// <param name="IDAlumno"></param>
        /// <param name="IDEscuela"></param>
        /// <param name="IDPlanEstudio"></param>
        /// <param name="Ciclo"></param>
        /// <returns></returns>
        public IEnumerable<DBOSPMallaAsigAprobWeb> sp_malla_asig_apro_web(string IDSede, string IDAlumno, string IDEscuela, string IDPlanEstudio, string Ciclo)
        {
            return this.Database.SqlQuery<DBOSPMallaAsigAprobWeb>("DBO.sp_crearidpagoonline @IDSede, @IDAlumno, @IDEscuela, @IDPlanEstudio, @Ciclo",
                new SqlParameter("@IDSede", IDSede),
                new SqlParameter("@IDAlumno", IDAlumno),
                new SqlParameter("@IDEscuela", IDEscuela),
                new SqlParameter("@IDPlanEstudio", IDPlanEstudio),
                new SqlParameter("@Ciclo", Ciclo)
                );
        }

        /// <summary>
        /// Boleta de notas desde APEC
        /// para anterior al 201710
        /// </summary>
        /// <param name="c_IDDependencia"></param>
        /// <param name="c_IDSede"></param>
        /// <param name="c_IDPerAcad"></param>
        /// <param name="c_IDAlumno"></param>
        /// <returns></returns>
        public IEnumerable<DBOSPBoletaDeNotas> sp_boleta_nota(string c_IDDependencia, string c_IDSede, string c_IDPerAcad, string c_IDAlumno)
        {
            return this.Database.SqlQuery<DBOSPBoletaDeNotas>("DBO.sp_boleta_nota @c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDAlumno",
                new SqlParameter("@c_IDDependencia", c_IDDependencia),
                new SqlParameter("@c_IDSede", c_IDSede),
                new SqlParameter("@c_IDPerAcad", c_IDPerAcad),
                new SqlParameter("@c_IDAlumno", c_IDAlumno)
                );
        }


        public IEnumerable<DBOSP_crearidpagoonline> sp_crear_id_pagoonline(string IDDependencia, string IDSede, string IDPerAcad, string IDAlumno, string IDConcepto, string Tipo, double Monto, string CodigoBanco)
        {
            return this.Database.SqlQuery<DBOSP_crearidpagoonline>("DBO.sp_crearidpagoonline @c_IDSede,@c_IDAlumno,@c_IDPerAcad,@c_IDDependencia,@c_IDConcepto,@c_Tipo,@n_Monto,@c_CodigoBanco",
                new SqlParameter("@c_IDSede", IDSede),
                new SqlParameter("@c_IDAlumno", IDAlumno),
                new SqlParameter("@c_IDPerAcad", IDPerAcad),
                new SqlParameter("@c_IDDependencia", IDDependencia),
                new SqlParameter("@c_IDConcepto", IDConcepto),
                new SqlParameter("@c_Tipo", Tipo),
                new SqlParameter("@n_Monto", Monto),
                new SqlParameter("@c_CodigoBanco", CodigoBanco)
                );
        }

        #endregion StoreProcedures DBO
        #region storeProcedure caja

        public IEnumerable<sp_InsertDebt> sp_InsertDebt(string @id_alumno, string @id_escuela, string @id_peracad, int @id_solicitud, string @id_cuenta, string @id_centrocosto, string @id_servicio, int @c_IDDocumento, string @c_IDDependencia, string @c_IDSede, double @c_Costo, string @c_Nombre_Documento,double @costo_proceso)
        {
           return  this.Database.SqlQuery<sp_InsertDebt>("ATE.sp_insert_debt_banner @id_alumno,  @id_escuela,  @id_peracad,  @id_solicitud, @id_cuenta,  @id_centrocosto,  @id_servicio,  @c_IDDocumento,@c_IDDependencia, @c_IDSede, @c_Costo ,@c_Nombre_Documento,@costo_proceso",
                new SqlParameter("@id_alumno", @id_alumno),
                new SqlParameter("@id_escuela", @id_escuela),
                new SqlParameter("@id_peracad", @id_peracad),
                new SqlParameter("@id_solicitud", @id_solicitud),
                new SqlParameter("@id_cuenta", @id_cuenta),
                new SqlParameter("@id_centrocosto", @id_centrocosto),
                new SqlParameter("@id_servicio", @id_servicio),
                new SqlParameter("@c_IDDocumento", @c_IDDocumento),
                new SqlParameter("@c_IDDependencia", @c_IDDependencia),
                new SqlParameter("@c_IDSede", @c_IDSede),
                new SqlParameter("@c_Costo", @c_Costo),
                new SqlParameter("@c_Nombre_Documento", @c_Nombre_Documento),
                new SqlParameter("@costo_proceso", @costo_proceso)
                
            );           
        }
        public IEnumerable<SPListDebt> sp_ListDebt(string div, string campus, string document_number)
        {
            return this.Database.SqlQuery<SPListDebt>("ATE.sp_debt_list_by_alumno @c_IDDependencia,@c_IDSede,@c_IDAlumno", 
                new SqlParameter("@c_IDDependencia", div),
                new SqlParameter("@c_IDSede", campus),
                new SqlParameter("@c_IDAlumno", document_number)
            );
        }

        #endregion storeProcedure caja

        #region StoreProcedures ADM - TEST de Admision

        public IEnumerable<TestAdmisionsp_PreguntasByTest_CF> TestAdmisionsp_PreguntasByTest_CF(int @c_IDTest, string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @d_IDExamen, string @c_IDAlumno)
        {                  
            return this.Database.SqlQuery<TestAdmisionsp_PreguntasByTest_CF>("dbo.sp_PreguntasByTest @c_IDTest, @c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @d_IDExamen, @c_IDAlumno",
                new SqlParameter("@c_IDTest", @c_IDTest),
                new SqlParameter("@c_IDDependencia", @c_IDDependencia),
                new SqlParameter("@c_IDSede", @c_IDSede),
                new SqlParameter("@c_IDPerAcad", @c_IDPerAcad),
                new SqlParameter("@c_IDSeccionC", @c_IDSeccionC),
                new SqlParameter("@d_IDExamen", @d_IDExamen),
                new SqlParameter("@c_IDAlumno", @c_IDAlumno)
            );
        }
        public IEnumerable<TestAdmisionsp_GetDatosAlumnoTestAdmision_CF> TestAdmisionsp_GetDatosAlumnoExamen_CF(string @c_IDAlumno)
        {
            return this.Database.SqlQuery<TestAdmisionsp_GetDatosAlumnoTestAdmision_CF>("ADM.sp_GetDatosAlumnoTestAdmision @c_IDAlumno",
                new SqlParameter("@c_IDAlumno", @c_IDAlumno)
            );
        }
        public IEnumerable<TestAdmisionsp_Update_tblTestRespuesta_CF> TestAdmisionsp_Update_tblTestRespuesta_CF(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @c_IDExamen, string @c_IDAlumno, int @c_IDTest, int @c_IDPreguntaByTest, string @c_Respuesta)
        {
            return this.Database.SqlQuery<TestAdmisionsp_Update_tblTestRespuesta_CF>("dbo.sp_Update_tblTestRespuesta @c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @c_IDExamen, @c_IDAlumno, @c_IDTest, @c_IDPreguntaByTest, @c_Respuesta",
                new SqlParameter("@c_IDDependencia", @c_IDDependencia),
                new SqlParameter("@c_IDSede", @c_IDSede),
                new SqlParameter("@c_IDPerAcad", @c_IDPerAcad),
                new SqlParameter("@c_IDSeccionC", @c_IDSeccionC),
                new SqlParameter("@c_IDExamen", @c_IDExamen),
                new SqlParameter("@c_IDAlumno", @c_IDAlumno),
                new SqlParameter("@c_IDTest", @c_IDTest),
                new SqlParameter("@c_IDPreguntaByTest", @c_IDPreguntaByTest),
                new SqlParameter("@c_Respuesta", @c_Respuesta)
            );
        }
        public IEnumerable<TestAdmisionsp_CompletoTest_CF> TestAdmisionCompletoTest_CF(string @c_IDDependencia, string @c_IDSede, string @c_IDSeccionC, string @c_IDPerAcad, DateTime @d_IDExamen, string @c_IDAlumno, int @c_IDTest)
        {
            return this.Database.SqlQuery<TestAdmisionsp_CompletoTest_CF>("ADM.sp_CompletoTest @c_IDDependencia, @c_IDSede, @c_IDSeccionC, @c_IDPerAcad, @d_IDExamen, @c_IDAlumno, @c_IDTest",

                new SqlParameter("@c_IDDependencia", @c_IDDependencia),
                new SqlParameter("@c_IDSede", @c_IDSede),
                new SqlParameter("@c_IDSeccionC", @c_IDSeccionC),
                new SqlParameter("@c_IDPerAcad", @c_IDPerAcad),
                new SqlParameter("@d_IDExamen", @d_IDExamen),
                new SqlParameter("@c_IDAlumno", @c_IDAlumno),
                new SqlParameter("@c_IDTest", @c_IDTest)
            );
        }
        public IEnumerable<TestAdmisionsp_ResultadoEscalaMotivacion_CF> ResultadoTestmotivacion(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @c_IDExamen, string @c_IDAlumno)
        {
            return this.Database.SqlQuery<TestAdmisionsp_ResultadoEscalaMotivacion_CF>("ADM.sp_ResultadoEscalaMotivacion @c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @c_IDExamen, @c_IDAlumno",

                new SqlParameter("@c_IDDependencia", @c_IDDependencia),
                new SqlParameter("@c_IDSede", @c_IDSede),
                new SqlParameter("@c_IDPerAcad", @c_IDPerAcad),
                new SqlParameter("@c_IDSeccionC", @c_IDSeccionC),
                new SqlParameter("@c_IDExamen", @c_IDExamen),
                new SqlParameter("@c_IDAlumno", @c_IDAlumno)
            );
        }
        public IEnumerable<TestAdmisionsp_ResultadoInteresesOcu1_CF> ResultadoInteresesOcu1(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @c_IDExamen, string @c_IDAlumno)
        {
            return this.Database.SqlQuery<TestAdmisionsp_ResultadoInteresesOcu1_CF>("ADM.sp_ResultadoInteresesOcu1 @c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @c_IDExamen, @c_IDAlumno",

                new SqlParameter("@c_IDDependencia", @c_IDDependencia),
                new SqlParameter("@c_IDSede", @c_IDSede),
                new SqlParameter("@c_IDPerAcad", @c_IDPerAcad),
                new SqlParameter("@c_IDSeccionC", @c_IDSeccionC),
                new SqlParameter("@c_IDExamen", @c_IDExamen),
                new SqlParameter("@c_IDAlumno", @c_IDAlumno)
            );
        }
        public IEnumerable<TestAdmisionsp_ResultadoInteresesOcu2_CF> ResultadoInteresesOcu2(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @c_IDExamen, string @c_IDAlumno)
        {
            return this.Database.SqlQuery<TestAdmisionsp_ResultadoInteresesOcu2_CF>("ADM.sp_ResultadoInteresesOcu2 @c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @c_IDExamen, @c_IDAlumno",

                new SqlParameter("@c_IDDependencia", @c_IDDependencia),
                new SqlParameter("@c_IDSede", @c_IDSede),
                new SqlParameter("@c_IDPerAcad", @c_IDPerAcad),
                new SqlParameter("@c_IDSeccionC", @c_IDSeccionC),
                new SqlParameter("@c_IDExamen", @c_IDExamen),
                new SqlParameter("@c_IDAlumno", @c_IDAlumno)
            );
        }
        public IEnumerable<TestAdmisionsp_ResultadoInteresesOcu4_CF> ResultadoInteresesOcu4(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @c_IDExamen, string @c_IDAlumno)
        {
            return this.Database.SqlQuery<TestAdmisionsp_ResultadoInteresesOcu4_CF>("ADM.sp_ResultadoInteresesOcu4 @c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @c_IDExamen, @c_IDAlumno",

                new SqlParameter("@c_IDDependencia", @c_IDDependencia),
                new SqlParameter("@c_IDSede", @c_IDSede),
                new SqlParameter("@c_IDPerAcad", @c_IDPerAcad),
                new SqlParameter("@c_IDSeccionC", @c_IDSeccionC),
                new SqlParameter("@c_IDExamen", @c_IDExamen),
                new SqlParameter("@c_IDAlumno", @c_IDAlumno)
            );
        }
        public IEnumerable<TestAdmisionsp_ResultadoEscalaHabilidades_CF> ResultadoTestHabilidades(string @c_IDDependencia, string @c_IDSede, string @c_IDPerAcad, string @c_IDSeccionC, DateTime @c_IDExamen, string @c_IDAlumno, string @c_IDTest)
        {
            return this.Database.SqlQuery<TestAdmisionsp_ResultadoEscalaHabilidades_CF>("ADM.sp_ResultadoEscalaHabilidades @c_IDDependencia, @c_IDSede, @c_IDPerAcad, @c_IDSeccionC, @c_IDExamen, @c_IDAlumno, @c_IDTest",

                new SqlParameter("@c_IDDependencia", @c_IDDependencia),
                new SqlParameter("@c_IDSede", @c_IDSede),
                new SqlParameter("@c_IDPerAcad", @c_IDPerAcad),
                new SqlParameter("@c_IDSeccionC", @c_IDSeccionC),
                new SqlParameter("@c_IDExamen", @c_IDExamen),
                new SqlParameter("@c_IDAlumno", @c_IDAlumno),
                new SqlParameter("@c_IDTest", @c_IDTest)
            );
        }
        public IEnumerable<TestAdmisionsp_RealizoTestAdmision_CF> TESTAdmisionsp_RealizoTestAdmision(string @IDAlumno)
        {
            return this.Database.SqlQuery<TestAdmisionsp_RealizoTestAdmision_CF>("ADM.sp_RealizoTestAdmision @IDAlumno",

                new SqlParameter("@IDAlumno", @IDAlumno)
            );
        }
        #endregion  StoreProcedures ADM - TEST de Admision
        #region StoreProcedure BNE
        /// <summary>
        /// Creación de nuevo registro en 'BNE.tblConvenios'
        /// </summary>
        /// <param name="cod_contrato">Código de contrato</param>
        /// <param name="nom_empresa">Nombre de la empresa de convenio</param>
        /// <param name="fec_ini_contrato">Fecha de inicio del convenio</param>
        /// <param name="fec_fin_contrato">Fecha de fin del convenio</param>
        /// <param name="grad_fam">Grado de familiaridad</param>
        /// <param name="clau_desap">Clausula de desaprobacion</param>
        /// <param name="renov_auto">Renovacion automatica</param>
        /// <param name="mat_adm">Descuento para la matricula en ADM</param>
        /// <param name="cuo_adm">Descuento para la cuota en ADM</param>
        /// <param name="mat_adg">Descuento para la matricula en ADG</param>
        /// <param name="cuo_adg">Descuento para la cuota en ADM</param>
        /// <param name="mat_adv">Descuento para la matricula en ADV</param>
        /// <param name="cuo_adv">Descuento para la cuota en ADM</param>
        /// <param name="id_personal">Usuario (autenticado) que crea el nuevo convenio</param>
        /// <returns></returns>
        public IEnumerable<BNESPCrearConvenio> bne_sp_crear_convenio (string cod_contrato, string nom_empresa, DateTime fec_ini_contrato, DateTime fec_fin_contrato, string grad_fam, Boolean clau_desap, Boolean renov_auto, int mat_adm, int cuo_adm, int mat_adg, int cuo_adg, int mat_adv, int cuo_adv, string id_personal)
        {
            return Database.SqlQuery<BNESPCrearConvenio>("BNE.sp_CrearConvenio @c_CodContrato, @c_NomEmpresa, @c_FecIniContrato, @c_FecFinContrato, @c_GradFam, @c_ClauDesap, @c_RenovAuto, @c_MatADM, @c_CuoADM, @c_MatADG, @c_CuoADG, @c_MatADV, @c_CuoADV, @c_IDPersonal",
                new SqlParameter("@c_CodContrato", cod_contrato),
                new SqlParameter("@c_NomEmpresa", nom_empresa),
                new SqlParameter("@c_FecIniContrato", fec_ini_contrato),
                new SqlParameter("@c_FecFinContrato", fec_fin_contrato),
                new SqlParameter("@c_GradFam", grad_fam),
                new SqlParameter("@c_ClauDesap", clau_desap),
                new SqlParameter("@c_RenovAuto", renov_auto),
                new SqlParameter("@c_MatADM", mat_adm),
                new SqlParameter("@c_CuoADM", cuo_adm),
                new SqlParameter("@c_MatADG", mat_adg),
                new SqlParameter("@c_CuoADG", cuo_adg),
                new SqlParameter("@c_MatADV", mat_adv),
                new SqlParameter("@c_CuoADV", cuo_adv),
                new SqlParameter("@c_IDPersonal", id_personal));
        }


        /// <summary>
        /// Asigna descuento al estudiante solicitante.
        /// </summary>
        /// <param name="id_sede">Codigo de la sede</param>
        /// <param name="id_peracad">Codigo del periodo academico</param>
        /// <param name="id_escuelaadm">Codigo de la modalidad del estudiante</param>
        /// <param name="id_alumno">Codigo del estudiante</param>
        /// <param name="id_escuela">Codigo de la carrera del estudiante</param>
        /// <param name="cronograma">Cronograma de pagos del estudiante</param>
        /// <param name="id_escala">Campo para cuando el descuento mande una escala para su asignacion</param>
        /// <param name="beca">Verificacion si el estudiante tiene o no beca</param>
        /// <param name="mediabeca">Verificacion si el estudiante tiene o no media beca</param>
        /// <param name="id_descuento">Codigo del descuento asignado</param>
        /// <param name="id_usuario">Codigo del usuario que asigno el descuento</param>
        /// <param name="observacion">Observacion adicional para la asignacion del descuento</param>
        /// <returns></returns>
        public IEnumerable<BNESPAsignarDescuento> bne_sp_asignar_descuento(string id_sede, string id_peracad, string id_escuelaadm, string id_alumno, string id_escuela, string cronograma, string id_escala, string beca, string id_descuento,string id_usuario, string observacion)
        {
            return Database.SqlQuery<BNESPAsignarDescuento>("BNE.sp_AsignarDescuento @c_IDSede, @c_IDPeracad, @c_IDEscuelaADM, @c_IDAlumno, @c_IDEscuela, @c_Cronograma, @c_IDEscala, @c_Beca, @c_IDDescuento, @c_IDUsuario, @c_Observacion",
                new SqlParameter("@c_IDSede", id_sede),
                new SqlParameter("@c_IDPeracad", id_peracad),
                new SqlParameter("@c_IDEscuelaADM", id_escuelaadm),
                new SqlParameter("@c_IDAlumno", id_alumno),
                new SqlParameter("@c_IDEscuela", id_escuela),
                new SqlParameter("@c_Cronograma", cronograma),
                new SqlParameter("@c_IDEscala", id_escala),
                new SqlParameter("@c_Beca", beca),
                new SqlParameter("@c_IDDescuento", id_descuento),
                new SqlParameter("@c_IDUsuario", id_usuario),
                new SqlParameter("@c_Observacion", observacion));
        }

        /// <summary>
        /// Elimina beneficio seleccionado según el estudiante
        /// </summary>
        /// <param name="idDependencia">Codigo de la Institución</param>
        /// <param name="idSede">Codigo de la sede</param>
        /// <param name="idPeracad">Codigo del periodo academico</param>
        /// <param name="idEscuelaAdm">Codigo de la modalidad del estudiante</param>
        /// <param name="idAlumno">Codigo del estudiante</param>
        /// <param name="idBeneficio">Codigo del descuento asignado</param>
        /// <param name="idUsuario">Codigo del usuario que elimina el beneficio seleccionado</param>
        public void bne_sp_eliminar_beneficio(string idDependencia, string idSede, string idEscuelaAdm, 
                                              string idPeracad, string idAlumno, string idBeneficio, string idUsuario)
        {
            Database.ExecuteSqlCommand(@"BNE.sp_EliminarBeneficio_UC @c_IDDependencia,@c_IDSede, @c_IDEscuelaADM,
                                       @c_IDPeracad, @c_IDAlumno, @c_IDBeneficio, @c_IDUsuario",
               new SqlParameter("@c_IDDependencia", idDependencia),
               new SqlParameter("@c_IDSede", idSede),
               new SqlParameter("@c_IDEscuelaADM", idEscuelaAdm),
               new SqlParameter("@c_IDPeracad", idPeracad),
               new SqlParameter("@c_IDAlumno", idAlumno),
               new SqlParameter("@c_IDBeneficio", idBeneficio),
               new SqlParameter("@c_IDUsuario", idUsuario));
        }

        /// <summary>
        /// Asigna descuento al estudiante solicitante si es para becas a partir del 2019.
        /// </summary>
        /// <param name="idSede">Codigo de la sede</param>
        /// <param name="idPeracad">Codigo del periodo academico</param>
        /// <param name="idAlumno">Codigo del estudiante</param>
        /// <param name="tipoBeca">Dato para saber si es Beca Mediabeca o Tercio de beca</param>
        /// <param name="idEscala">Campo para cuando el descuento mande una escala para su asignacion</param>
        /// <param name="idBeneficio">Codigo del descuento asignado</param>
        /// <param name="idUsuario">Codigo del usuario que asigno el descuento</param>
        /// <param name="observacion">Observacion adicional para la asignacion del descuento</param>
        public void bne_sp_asignar_beneficio(string idDependencia, string idSede, string idPeracad, string idAlumno, 
                                             string tipoBeca, string idEscala, string idBeneficio, string idUsuario, 
                                             string observacion)
        {
            Database.ExecuteSqlCommand(@"BNE.sp_AsignarBeneficio201910Becas_UC @c_IDDependencia,@c_IDSede,
                                       @c_IDPeracad, @c_IDAlumno, @c_TipoBeca, @c_IDEscala, @c_IDBeneficio, 
                                       @c_IDUsuario, @c_Observacion",
               new SqlParameter("@c_IDDependencia", idDependencia),
               new SqlParameter("@c_IDSede", idSede),
               new SqlParameter("@c_IDPeracad", idPeracad),
               new SqlParameter("@c_IDAlumno", idAlumno),
               new SqlParameter("@c_TipoBeca", tipoBeca),
               new SqlParameter("@c_IDEscala", idEscala),
               new SqlParameter("@c_IDBeneficio", idBeneficio),
               new SqlParameter("@c_IDUsuario", idUsuario),
               new SqlParameter("@c_Observacion", observacion));
        }
        #endregion StoreProcedure BNE
        #region synonims
        public DbSet<DBOSiUsuario> si_usuario { get; set; }
        #endregion synonims

        #region CIC
        
        #region CIC Tables

        public virtual DbSet<CICConstAcredIdiomExt> cic_const_acred_idiom_ext { get; set; }

        #endregion CIC Tables

        #region CIC Views

        #endregion CIC Views

        #region CIC Stored Procedures

        public string sp_ejecutar_matricula_cic(decimal pidm, string seccion)
        {
            string lista = Database.SqlQuery<string>(
                            "CIC.sp_EjecutarMatricula @pidm, @c_IDSeccionC",
                            new SqlParameter("@pidm", pidm),
                            new SqlParameter("@c_IDSeccionC", seccion)).First().ToString();
            return lista;
        }

        public string sp_comprobar_matricula_cic(decimal pidm, string idescuela)
        {
            string lista = Database.SqlQuery<string>(
                            "CIC.sp_ComprobarMatricula @pidm, @idescuela",
                            new SqlParameter("@pidm", pidm),
                            new SqlParameter("@idescuela", idescuela)).First().ToString();
            return lista;
        }

        #endregion CIC Stored Procedures

        #endregion CIC

        #region OPP
        #region tablas
        public DbSet<OPPtblPracticaDocumento> opp_tbl_practica_documento { get; set; }
        #endregion
        #endregion

        #region PRS
        #region tablas
        public DbSet<PRStblProyecto> prs_tbl_proyecto { get; set; }
        public DbSet<PRStblIntegranteProyecto> prs_tbl_integrante_proyecto { get; set; }
        #endregion
        #endregion

        #region CAU

        #region CAU_Stored_Procedures
            public IEnumerable<OEASPReportSubstituteExam> OEA_sp_report_substitute_exam(string div, string term, string group = null, string modality = null)
            {
            return Database.SqlQuery<OEASPReportSubstituteExam>("OEA.sp_reportSubstituteExam @c_IDDependencia, @C_IDPerAcad, @C_IDGrupo, @C_IDTipoPlan",
                new SqlParameter("@c_IDDependencia", div),
                new SqlParameter("@C_IDPerAcad", term),
                new SqlParameter("@C_IDGrupo", (object)group ?? DBNull.Value),
                new SqlParameter("@C_IDTipoPlan", (object)modality ?? DBNull.Value));
            }

        /// <summary>
        /// Creación de deuda al matricularse un estudiante a una actividad extracurricular que tiene un costo
        /// </summary>
        /// <param name="idAlumno">IDAlumno</param>
        /// <param name="quantity">Cantidad de actividades por las cuales se le generará deuda</param>
        public void Sp_GenerarDeudaCaja(string idAlumno, byte quantity)
        {
            this.Database.ExecuteSqlCommand("CAU.sp_GenerarDeudaCaja @IDDependencia, @IDSede, @IDPeracad, @IDSeccionC, @FecInic, @IDEscuela, @IDAlumno, @n_cantidad",
                new SqlParameter("@IDDependencia", "UCCI"),
                new SqlParameter("@IDSede", "HYO"),
                new SqlParameter("@IDPeracad", "2019-1"),
                new SqlParameter("@IDSeccionC", "Taller:vidaunivers"),
                new SqlParameter("@FecInic", "18/03/2019"),
                new SqlParameter("@IDEscuela", "EVE"),
                new SqlParameter("@IDAlumno", idAlumno),
                new SqlParameter("@n_cantidad", quantity)
                );
        }

        /// <summary>
        /// Cancela deuda, solicitud y registra en el log
        /// </summary>
        /// <param name="requestID"></param>
        /// <returns></returns>
        public IEnumerable<object> sp_CancelRequest(string requestID) {
            return this.Database.SqlQuery<object>("CAU.spCancelRequest @c_IDRequest",
                new SqlParameter("@c_IDRequest", requestID)
                );
        }

        #endregion
        
        #endregion

        #region BANNER
        #region Tables
        public DbSet<BANNERSpriden> banner_spriden { get; set; }
        #endregion
        #endregion

        #region PRY

        #region Tables

        public DbSet<PRYCategory> pry_category { get; set; }
        public DbSet<PRYMemberType> pry_member_type { get; set; }
        public DbSet<PRYProfileProject> pry_profile_project { get; set; }
        public DbSet<PRYProject> pry_project { get; set; }
        public DbSet<PRYProjectMember> pry_project_member { get; set; }
        public DbSet<PRYProjectType> pry_project_type { get; set; }

        #endregion

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Properties<string>().Configure(c => c.HasColumnType("varchar"));
            modelBuilder.Properties<string>().Configure(c => c.IsUnicode(false));
            base.OnModelCreating(modelBuilder);
        }
    }

    
}
