﻿using ACPermanence.DataBases.BANNER.BANINST1;
using ACPermanence.DataBases.BANNER.SATURN;
using ACPermanence.DataBases.BANNER.SZKPCAU.procedures;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;

namespace ACPermanence.Contexts.BANNER
{
    public class BNContext : DbContext
    {
        public BNContext() : base("BANNER")
        {
        }

        #region ACADEMIC TABLES

        public virtual DbSet<SHRTCKG> tbl_SHRTCKG { get; set; }
        public virtual DbSet<SHRTCKN> tbl_SHRTCKN { get; set; }
        public virtual DbSet<STVGCHG> tbl_STVGCHG { get; set; }
        public virtual DbSet<STVGCMT> tbl_STVGCMT { get; set; }

        /// <summary>
        /// Student base table
        /// </summary>
        public virtual DbSet<SGBSTDN> tbl_SGBSTDN { get; set; }

        /// <summary>
        /// Cursos para validaciones
        /// </summary>
        public virtual DbSet<SMRARUL> tbl_SMRARUL { get; set; }

        /// <summary>
        /// Nombre completo del curso
        /// </summary>
        public virtual DbSet<SCRSYLN> tbl_SCRSYLN { get; set; }

        /// <summary>
        /// Cursos Matriculados
        /// </summary>
        public virtual DbSet<SFRSTCR> tbl_SFRSTCR { get; set; }

        /// <summary>
        /// Notas de componentes de un curso
        /// </summary>
        public virtual DbSet<SHRGCOM> tbl_SHRGCOM { get; set; }

        /// <summary>
        /// Notas de componentes de un curso
        /// </summary>
        public virtual DbSet<SHRMRKS> tbl_SHRMRKS { get; set; }

        /// <summary>
        /// Sub componente detalle
        /// </summary>
        public virtual DbSet<SHRSCOM> tbl_SHRSCOM { get; set; }

        /// <summary>
        /// Nota subcomponente
        /// </summary>
        public virtual DbSet<SHRSMRK> tbl_SHRSMRK { get; set; }

        /// <summary>
        /// TABLA DE CONFUGRACION DE NRCS
        /// </summary>
        public virtual DbSet<SSBSECT> tbl_SSBSECT { get; set; }

        // CONVALIDACIONES

        /// <summary>
        /// Tabla de convalidaciones (AKA transferencias)
        /// </summary>
        public virtual DbSet<SHRTRAM> tbl_SHRTRAM { get; set; }

        /// <summary>
        /// vinculo entre transferencia y institución de transferencia
        /// </summary>
        public virtual DbSet<SHRTRIT> tbl_SHRTRIT { get; set; }

        /// <summary>
        /// Tabla de instituciones  
        /// </summary>
        public virtual DbSet<STVSBGI> tbl_STVSBGI { get; set; }

        /// <summary>
        /// Cursos convalidados de la institucion de procedencia
        /// </summary>
        public virtual DbSet<SHRTRCR> tbl_SHRTRCR { get; set; }

        /// <summary>
        /// Tabla de cursos convalidados en la UC
        /// </summary>
        public virtual DbSet<SHRTRCE> tbl_SHRTRCE { get; set; }

        /// <summary>
        /// Learner Curriculum repeating table to store level, college,
        /// program and other data associated with curriculum
        /// 
        /// </summary>
        public virtual DbSet<SORLCUR> tbl_SORLCUR { get; set; }

        /// <summary>
        /// Learner curriculum field of study. 
        /// </summary>
        public virtual DbSet<SORLFOS> tbl_SORLFOS { get; set; }


        /// <summary>
        /// Tabla de postulaciones de alumnos
        /// * solo es referencia ya que hicierom movimientos raros en 
        /// * la configuracion
        /// </summary>
        public virtual DbSet<SARADAP> tbl_SARADAP { get; set; }

        #endregion ACADEMIC TABLES

        #region  BANNINST VIEWS

        // <summary>
        // tabla a partir de una vista que tiene la informacion
        // del plan de estudios 
        // AKA Curriculum
        // </summary>
        // no se usa debido a que se cambio por el procedimiento  p_catalogo_carrera
        // porque en banner hacen cosas raraz que no permiten usar esta tabla de manera 
        // transparente
        public virtual DbSet<SZRCDAB> tbl_SZRCDAB { get; set; }

        /// <summary>
        /// TAbla/vista que contiene informacion de matriculas
        /// </summary>
        public virtual DbSet<SZVCMPP> tbl_SZVCMPP { get; set; }

        /// <summary>
        /// Vista de Nrcs
        /// </summary>
        public virtual DbSet<SZVNPES> view_SZVNPES { get; set; }

        #endregion

        #region DIMENSIONS

        /// <summary>
        /// Todas las Personas
        /// </summary>
        public virtual DbSet<SPRIDEN> dim_personas { get; set; }

        /// <summary>
        /// Horarios (para uso de Qhawarina)
        /// </summary>
        public virtual DbSet<SSRMEET> ban_horarios { get; set; }

        /// <summary>
        /// Docentes (para uso de Qhawarina)
        /// </summary>
        public virtual DbSet<SIRASGN> ban_docentes { get; set; }

        public virtual DbSet<SZBADMT> dim_admition_types { get; set; }

        public virtual DbSet<SMRALIB> tbl_SMRALIB { get; set; }

        public virtual DbSet<SZVCRED> tbl_SZVCRED { get; set; }

        #endregion DIMENSIONS

        /// <summary>
        /// Tablas usadas para consultas de cursos ofertados
        /// </summary>
        public virtual DbSet<PROJECTIONRESULTS> tbl_PROJECTIONRESULTS { get; set; }

        public virtual DbSet<CLASSSEARCH> tbl_CLASSSEARCH { get; set; }
        public virtual DbSet<SZVLXNA> tbl_SZVLXNA { get; set; }
        public virtual DbSet<SZTPROY> tbl_SZTPROY { get; set; }

        /// <summary>
        /// Nombre de escuela académica en banner
        /// </summary>
        public virtual DbSet<SZVMAJR> tbl_SZVMAJR { get; set; }

        /// <summary>
        /// Tabla de registro del cierre de matrícula de un estudiante por período
        /// </summary>
        public virtual DbSet<SFBRGRP> tbl_SFBRGRP { get; set; }

        #region StoreProcedures 

        /// <summary>
        /// El procedimiento almacenado BANINST1.SZKPCAU.P_CARRERASxALUMNO  
        /// devuelve todas las carreras por modalidad (departament) 
        /// y sede (campus) de un alumno al ingresar su DNI o codigo de alumno;
        /// </summary>
        /// <param name="person_id">pidm</param>
        /// <returns></returns>
        public IEnumerable<PCarrerasXAlumno> p_carreras_alumno(decimal person_id)
        {
            return this.Database.SqlQuery<PCarrerasXAlumno>(
                "BEGIN BANINST1.SZKPCAU.P_CARRERASxALUMNO(:P_PIDM, :P_RECORDER ); END;"
                , new OracleParameter("P_PIDM", person_id)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// El procedimiento almacenado BANINST1.SZKPCAU.P_CICLO_CREDITOS_ALUMNO
        /// devuelve el detalle del perido(ciclo) actual y de matricula del alumno 
        /// por carrera al ingresar su PIDM , un periodo de corte  y codigo 
        /// de carrera
        /// </summary>
        /// <param name="person_id">pidm</param>
        /// <param name="term">pidm</param>
        /// <param name="program">escuela academica(carrera)</param>
        /// <returns></returns>
        public IEnumerable<PCicloCreditosAlumno> p_creditos_alumno(decimal person_id, string term, string program)
        {
            return this.Database.SqlQuery<PCicloCreditosAlumno>(
                "BEGIN BANINST1.SZKPCAU.P_CICLO_CREDITOS_ALUMNO(:P_PIDM, :P_TERMCUT, :P_PROGRAM, :P_RECORDER ); END;"
                , new OracleParameter("P_PIDM", person_id)
                , new OracleParameter("P_TERMCUT", term)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// El procedimiento almacenado BANINST1.SZKPCAU.P_HISTORIAXCARRERAALUMNO 
        /// devuelve el historial del alumno por carrera al ingresar su PIDM  
        /// y codigo de carrera; 
        /// </summary>
        /// <param name="person_id">pidm</param>
        /// <param name="program">escuela academica(carrera)</param>
        /// <returns></returns>
        public IEnumerable<PHistoriaXCarreraAlumno> p_deudas_alumno(decimal person_id, string program)
        {
            return this.Database.SqlQuery<PHistoriaXCarreraAlumno>(
                "BEGIN BANINST1.SZKPCAU.P_HISTORIAXCARRERAALUMNO(:P_PIDM, :P_PROGRAM :P_RECORDER ); END;"
                , new OracleParameter("P_PIDM", person_id)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// Para [DIM].[tblPostulant] se implemento el siguiente procedimiento 
        /// almacenado: BANINST1.SZKPCAU.P_POSTULACIONESXALUMNO devuelve el 
        /// listado de postulaciones por alumno
        /// </summary>
        /// <param name="person_id">pidm</param>
        /// <returns></returns>
        public IEnumerable<PRequisitosXAlumno> p_requisitos_alumno(decimal person_id)
        {
            return this.Database.SqlQuery<PRequisitosXAlumno>(
                "BEGIN BANINST1.SZKPCAU.P_REQUISITOSXALUMNO(:P_PIDM, :P_RECORDER ); END;"
                , new OracleParameter("P_PIDM", person_id)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }


        /// <summary>
        /// El procedimiento almacenado BANINST1.SZKPCAU.P_HISTORIAXCARRERAALUMNO 
        /// devuelve el historial del alumno por carrera al ingresar 
        /// su PIDM  y codigo de carrera
        /// 
        /// NO CUENTA LA MATRICULA EN CURSO
        /// </summary>
        /// <param name="person_id">pidm</param>
        /// <param name="program">codigo de carrera </param>
        /// <returns></returns>
        public IEnumerable<PHistoriaXCarreraAlumno> p_historia_carrera_alumno(decimal person_id, string program)
        {
            return this.Database.SqlQuery<PHistoriaXCarreraAlumno>(
                "BEGIN BANINST1.SZKPCAU.P_HISTORIALTOTALXCARRERAALUMNO(:P_PIDM, :P_PROGRAM, :P_RECORDER ); END;"
                , new OracleParameter("P_PIDM", person_id)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// El procedimiento almacenado BANINST1.SZKPCAU.P_HISTORIAXCARRERAALUMNO 
        /// devuelve el historial del alumno por carrera al ingresar 
        /// su PIDM  y codigo de carrera
        /// SOLO TRAE CURSOS APROVADOS -> esto por un tema de temor de modificar el 
        /// procedimiento para obtener tambien los cursos desaprobados
        /// NO CUENTA LA MATRICULA EN CURSO
        /// </summary>
        /// <param name="person_id">pidm</param>
        /// <param name="program">Codigo de carrera</param>
        /// <returns></returns>
        public IEnumerable<PHistoriaXCarreraAlumno> p_certificado_carrera_alumno(decimal person_id, string program)
        {
            return this.Database.SqlQuery<PHistoriaXCarreraAlumno>(
                "BEGIN BANINST1.SZKPCAU.P_HISTORIAXCARRERAALUMNO(:P_PIDM, :P_PROGRAM, :P_RECORDER ); END;"
                , new OracleParameter("P_PIDM", person_id)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        public IEnumerable<PHistoriaXCarreraAlumno> p_historia_periodo_alumno(decimal person_id, string program,
            string term)
        {
            return this.Database.SqlQuery<PHistoriaXCarreraAlumno>(
                "BEGIN BANINST1.SZKPCAU.P_HISTORIAXPERIODOALUMNO(:P_PIDM, :P_PROGRAM, :P_TERM, :P_RECORDER ); END;"
                , new OracleParameter("P_PIDM", person_id)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_TERM", term)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// Para  [DIM].[tblPostulant] se implemento el siguiente
        /// procedimiento almacenado: BANINST1.SZKPCAU.P_POSTULACIONESXALUMNO devuelve el listado 
        /// de postulaciones por alumno;  al ingresar su DNI o codigo de alumno
        /// </summary>
        /// <param name="person_id">PIDM</param>
        /// <returns></returns>
        public IEnumerable<PPostulacionesXAlumno> p_postulaciones_alumno(decimal person_id)
        {
            return this.Database.SqlQuery<PPostulacionesXAlumno>(
                "BEGIN BANINST1.SZKPCAU.P_POSTULACIONESXALUMNO(:P_PIDM, :P_RECORDER); END;"
                , new OracleParameter("P_PIDM", person_id)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_HORARIO_X_ALUMNO
        /// Devuelve horario de estudiantes de acuerdo a un pidm estudiante y período académico
        /// </summary>
        /// <param name="studentPidm">PIDM estudiante</param>
        /// <param name="academicPeriod">Período académico</param>
        /// <returns></returns>
        public IEnumerable<PHorarioXAlumno> p_horario_alumno(decimal studentPidm, string academicPeriod)
        {
            return Database.SqlQuery<PHorarioXAlumno>(
                "BEGIN BANINST1.SZKPCAU.P_HORARIO_X_ALUMNO(:P_PIDM, :P_TERM, :P_RECORDER); END;",
                new OracleParameter("P_PIDM", studentPidm),
                new OracleParameter("P_TERM", academicPeriod),
                new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output));
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_HORARIO_X_DOCENTE
        /// Devuelve horario de docentes de acuerdo a un pidm docente y período académico
        /// </summary>
        /// <param name="teacherPidm">PIDM docente</param>
        /// <param name="academicPeriod">Período académico</param>
        /// <returns></returns>
        public IEnumerable<PHorarioXDocente> p_horario_docente(decimal teacherPidm, string academicPeriod)
        {
            return Database.SqlQuery<PHorarioXDocente>(
                "BEGIN BANINST1.SZKPCAU.P_HORARIO_X_DOCENTE(:P_PIDMTEACHER, :P_TERM, :P_RECORDER); END;",
                new OracleParameter("P_PIDMTEACHER", teacherPidm),
                new OracleParameter("P_TERM", academicPeriod),
                new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output));
        }

        public IEnumerable<SZRCDAB> p_catalogo_carrera(string term_catalog, string department, string program,
            string campus)
        {
            return this.Database.SqlQuery<SZRCDAB>(
                "BEGIN BANINST1.SZKPCAU.P_CATALOGOXCARRERA(:P_TERMCATALOG, :P_MODALIDAD, :P_PROGRAM ,:P_CAMPUS ,:P_RECORDER); END;"
                , new OracleParameter("P_TERMCATALOG", term_catalog)
                , new OracleParameter("P_MODALIDAD", department)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_CAMPUS", campus)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }


        public IEnumerable<PCatalogoPersonzalizado> p_catalogo_por_alumno(decimal person_id, string program)
        {
            return this.Database.SqlQuery<PCatalogoPersonzalizado>(
                "BEGIN BANINST1.SZKPCAU.P_CATALOGOPERSONALIZADO(:P_PIDM, :P_PROGRAM ,:P_RECORDER); END;"
                , new OracleParameter("P_PIDM", person_id)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        public IEnumerable<PHorarioXAlumno> p_horario_x_alumno(decimal person_id, string term)
        {
            return this.Database.SqlQuery<PHorarioXAlumno>(
                "BEGIN BANINST1.SZKPCAU.P_HORARIO_X_ALUMNO(:P_PIDM, :P_TERM ,:P_RECORDER); END;"
                , new OracleParameter("P_PIDM", person_id)
                , new OracleParameter("P_TERM", term)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        public IEnumerable<PHorarioXAlumno> p_get_nrc(decimal pidm, string term, string program, string subject,
            string course, string campus, string departament)
        {
            return this.Database.SqlQuery<PHorarioXAlumno>(
                "BEGIN BANINST1.SZKPROY.P_GET_NRC(:P_PIDM, :P_TERM, :P_PROGRAM, :P_SUBJECT, :P_COURSE, :P_CAMPUS, :P_DEPARTAMENT, :P_RECORDER); END;"
                , new OracleParameter("P_PIDM", pidm)
                , new OracleParameter("P_TERM", term)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_SUBJECT", subject)
                , new OracleParameter("P_COURSE", course)
                , new OracleParameter("P_CAMPUS", campus)
                , new OracleParameter("P_DEPARTAMENT", departament)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        public IEnumerable<P_INASISTENCIAXALUMNO> p_inasistencia_x_alumno(decimal person_id, string term)
        {
            return this.Database.SqlQuery<P_INASISTENCIAXALUMNO>(
                "BEGIN BANINST1.SZKPCAU.P_INASISTENCIAXALUMNO(:P_TERM, :P_PIDM ,:P_RECORDER); END;"
                , new OracleParameter("P_TERM", term)
                , new OracleParameter("P_PIDM", person_id)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_ASIGNATURASPROY
        /// Devuelve los cursos ofertados para un estudiante en un periodo
        /// </summary>
        public IEnumerable<P_ASIGNATURASPROY> p_asignaturasproy(decimal pidm, string program, string term)
        {
            return this.Database.SqlQuery<P_ASIGNATURASPROY>(
                "BEGIN BANINST1.SZKPCAU.P_ASIGNATURASPROY(:P_PIDM, :P_PROGRAM, :P_TERM, :P_RECORDER); END;"
                , new OracleParameter("P_PIDM", pidm)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_TERM", term)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_RETENCIONESXALUMNOMATRI123
        /// Devuelve las retenciones de un estudiante
        /// </summary>
        public IEnumerable<P_RETENCIONESXALUMNOMATRI123> p_retencionesxalumnomatri123(decimal pidm)
        {
            return this.Database.SqlQuery<P_RETENCIONESXALUMNOMATRI123>(
                "BEGIN BANINST1.SZKPCAU.P_RETENCIONESXALUMNOMATRI123(:P_PIDM, :P_RECORDER); END;"
                , new OracleParameter("P_PIDM", pidm)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_GRUPOINSXALUMNO
        /// Devuelve el grupo de inscripción de un estudiante para un periodo académico
        /// </summary>
        public IEnumerable<P_GRUPOINSXALUMNO> p_grupoinsxalumno(decimal pidm, string term)
        {
            return this.Database.SqlQuery<P_GRUPOINSXALUMNO>(
                "BEGIN BANINST1.SZKPCAU.P_GRUPOINSXALUMNO(:P_PIDM, :P_TERM, :P_RECORDER); END;"
                , new OracleParameter("P_PIDM", pidm)
                , new OracleParameter("P_TERM", term)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_PERIODOMATRIACTIVOXALUM
        /// PIDM: PIDM del estudiante
        /// TERM_ENROLLMENT: último periodo en que registra matrícula
        /// TERM: periodo vigente
        /// </summary>
        public IEnumerable<P_PERIODOMATRIACTIVOXALUM> p_periodomatriactivoxalum(decimal pidm)
        {
            return this.Database.SqlQuery<P_PERIODOMATRIACTIVOXALUM>(
                "BEGIN BANINST1.SZKPCAU.P_PERIODOMATRIACTIVOXALUM(:P_PIDM, :P_RECORDER); END;"
                , new OracleParameter("P_PIDM", pidm)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// Obtener ultimo estado del estudiante
        /// </summary>
        /// <param name="pidm">PIDM</param>
        /// <returns></returns>
        public IEnumerable<P_ESTADOXALUMNO> p_estadoxalumno(decimal pidm)
        {
            return this.Database.SqlQuery<P_ESTADOXALUMNO>(
                "BEGIN BANINST1.SZKPCAU.P_ESTADOXALUMNO(:P_PIDM, :P_RECORDER); END;"
                , new OracleParameter("P_PIDM", pidm)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_ACTUALIZAPROY
        /// Actualiza los Cursos NRC que tiene un curso ofertado
        /// </summary>
        public IEnumerable<P_NRCSPADREPROY> p_actualizaproy(decimal pidm, string program, string term, string subject,
            string course, string campus, string dept)
        {
            return this.Database.SqlQuery<P_NRCSPADREPROY>(
                "BEGIN BANINST1.SZKPCAU.P_ACTUALIZAPROY(:P_PIDM, :P_PROGRAM, :P_TERM, :P_SUBJECT, :P_COURSE, :P_CAMPUS, :P_DEPT); END;"
                , new OracleParameter("P_PIDM", pidm)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_TERM", term)
                , new OracleParameter("P_SUBJECT", subject)
                , new OracleParameter("P_COURSE", course)
                , new OracleParameter("P_CAMPUS", campus)
                , new OracleParameter("P_DEPT", dept)
            );
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_NRCSPADREPROY
        /// Devuelve los nrc padre ofertados de u curso
        /// </summary>
        public IEnumerable<P_NRCSPADREPROY> p_nrcspadreproy(decimal pidm, string program, string term, string subject,
            string course)
        {
            return this.Database.SqlQuery<P_NRCSPADREPROY>(
                "BEGIN BANINST1.SZKPCAU.P_NRCSPADREPROY(:P_PIDM, :P_PROGRAM, :P_TERM, :P_SUBJECT, :P_COURSE, :P_RECORDER); END;"
                , new OracleParameter("P_PIDM", pidm)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_TERM", term)
                , new OracleParameter("P_SUBJECT", subject)
                , new OracleParameter("P_COURSE", course)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_NRCSPADREPROY
        /// Devuelve los nrc padre ofertados de u curso
        /// </summary>
        public IEnumerable<P_NRCSPADREPROY> p_nrcshijoproy(decimal pidm, string program, string term, string subject,
            string course, string codliga)
        {
            return this.Database.SqlQuery<P_NRCSPADREPROY>(
                "BEGIN BANINST1.SZKPCAU.P_NRCSHIJOPROY(:P_PIDM, :P_PROGRAM, :P_TERM, :P_SUBJECT, :P_COURSE, :P_CODLIGA, :P_RECORDER); END;"
                , new OracleParameter("P_PIDM", pidm)
                , new OracleParameter("P_PROGRAM", program)
                , new OracleParameter("P_TERM", term)
                , new OracleParameter("P_SUBJECT", subject)
                , new OracleParameter("P_COURSE", course)
                , new OracleParameter("P_CODLIGA", codliga)
                , new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output)
            );
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_MOVIMIENTOSESTUDIANTE
        /// Devuelve info sobre tipo modalidad de cada escuela académica de un estudiante
        /// </summary>
        /// <param name="pidm">PIDM del estudiante</param>
        /// <returns>Info sobre tipo modalidad de cada escuela académica de un estudiante</returns>
        public IEnumerable<P_MOVIMIENTOSESTUDIANTE> p_movimientos_estudiante(decimal pidm)
        {
            return Database.SqlQuery<P_MOVIMIENTOSESTUDIANTE>(
                "BEGIN BANINST1.SZKPCAU.P_MOVIMIENTOSESTUDIANTE(:P_PIDM ,:P_RECORDER); END;",
                new OracleParameter("P_PIDM", pidm),
                new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output));
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_CICLO_PRIMERAMATRICULA
        /// Devuelve info sobre la primera matrícula de la primera vez que el estudiante inició estudios en la UC
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        public IEnumerable<PCicloPrimeraMatricula> p_ciclo_primera_matricula(decimal pidm)
        {
            return Database.SqlQuery<PCicloPrimeraMatricula>(
                "BEGIN BANINST1.SZKPCAU.P_CICLO_PRIMERAMATRICULA(:P_PIDM ,:P_RECORDER); END;",
                new OracleParameter("P_PIDM", pidm),
                new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output));
        }

        /// <summary>
        /// Stored Procedure: BANINST1.SZKPCAU.P_DOCENTESCARGAHORARIA
        /// Devuelve lista de PIDM de docentes que por dictan algun curso en un período académico determinado
        /// </summary>
        /// <param name="academicPeriod"></param>
        /// <returns></returns>
        public IEnumerable<PDocentesCargaHoraria> p_docentes_carga_horaria(string academicPeriod)
        {
            return Database.SqlQuery<PDocentesCargaHoraria>(
                "BEGIN BANINST1.SZKPCAU.P_DOCENTESCARGAHORARIA(:P_TERM, :P_RECORDER); END;",
                new OracleParameter("P_TERM", academicPeriod),
                new OracleParameter("P_RECORDER", OracleDbType.RefCursor, ParameterDirection.Output));
        }

        #endregion StoreProcedures

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("SATURN");
            base.OnModelCreating(modelBuilder);
        }
    }
}