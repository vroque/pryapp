﻿using System;

namespace ACPermanence.DataBases.DBUCCI.OEA.StoredProcedures
{
    /// <summary>
    /// Reporte temporal de examen sustitutorio
    /// </summary>
    public class OEASPReportSubstituteExam
    {
        public string IDAlumno { get; set; }
        public string NomCompleto { get; set; }
        public string NomFacultad { get; set; }
        public string NomEsc { get; set; }
        public string NRC { get; set; }
        public string PartePeriodo { get; set; }
        public string Modalidad { get; set; }
        public string NombreSede { get; set; }
        public string idasignatura { get; set; }
        public string nomasignatura { get; set; }
        public string C1 { get; set; }
        public string EP { get; set; }
        public string C2 { get; set; }
        public string EF { get; set; }
        public string Cargo { get; set; }
        public string Deuda { get; set; }
        public string Abono { get; set; }
        public string IDSeccion { get; set; }
        public string Grupo { get; set; }
        public DateTime? Fecha_Solicitud { get; set; }
        public string Just { get; set; }
        public string ClassRoom { get; set; }
        public DateTime ExamDate { get; set; }
    }
}
