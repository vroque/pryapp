﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.CIC
{
    [Table("CIC.ConstAcredIdiomExt")]
    public class CICConstAcredIdiomExt
    {
        [Key]
        public int PIDM { get; set; }
        public string IDPersona { get; set; }
        public string IDAlumno { get; set; }
        public string NomCompleto { get; set; }
        public string IDEscuela { get; set; }
        public string Idioma { get; set; }
        public char Ciclo { get; set; }
        public DateTime FechaInicioCIC { get; set; }
        public char IDEscuelaADM { get; set; }
        public string NombreModalidad { get; set; }
        public DateTime? updateDate { get; set; }
    }
}
