﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.CIC
{
    public class CICEnrolment
    {
        public decimal pidm { get; set; }
        public string section { get; set; }
        public string idescuela { get; set; }
    }
}
