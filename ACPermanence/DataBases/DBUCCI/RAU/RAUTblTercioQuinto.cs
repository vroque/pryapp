﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.RAU
{
    [Table("[RAU].[tbltercioquinto]")]
    public class RAUTblTercioQuinto
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDPlanEstudio { get; set; }
        [Key]
        [Column(Order = 6)]
        public string IDAlumno { get; set; }
        [Key]
        [Column(Order = 7)]
        public string Ciclo { get; set; }
        public decimal PromedioPonderado { get; set; }
        public decimal PromedioTercio { get; set; }
        public string Participa { get; set; }
        public string Tercio { get; set; }
        public string Quinto { get; set; }
        public string Decimo { get; set; }
    }
}
