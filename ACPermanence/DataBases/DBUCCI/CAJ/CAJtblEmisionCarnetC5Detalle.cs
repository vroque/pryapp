﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.CAJ
{
    [Table("CAJ.tblEmisionCarnetC5Detalle")]
    public class CAJtblEmisionCarnetC5Detalle
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDAlumno { get; set; }
        public string IDUsuario { get; set; }
        [Key]
        [Column(Order = 6)]
        public DateTime FechaCreacion { get; set; }
        public string Estado { get; set; }
        public string IDUsuarioMod { get; set; }
        public DateTime FechaModificacion { get; set; }


    }
}
