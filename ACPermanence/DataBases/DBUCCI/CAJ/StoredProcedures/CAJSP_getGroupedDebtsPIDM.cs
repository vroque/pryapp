﻿using System;

namespace ACPermanence.DataBases.DBUCCI.CAJ.StoredProcedures
{
    public class CAJSP_getGroupedDebtsPIDM
    {
        public string IDDependencia { get; set; }
        public string IDSede { get; set; }
        public string IDAlumno { get; set; }
        public string IDGrupoConcepto { get; set; }
        public Decimal CodigoBanco { get; set; }
        public string IDPerAcad { get; set; }
        public string Descripcion { get; set; }
        public DateTime? FecCargo { get; set; }
        public DateTime? FecProrroga { get; set; }
        public Double Cargo { get; set; }
        public Double Deuda { get; set; }
        public Double Abono { get; set; }
        public Double Descuento { get; set; }
        public Double DescMora { get; set; }
        public string IDSeccionC { get; set; }
        public DateTime? FecInic { get; set; }
        public bool DocPorCobrar { get; set; }

    }
}
