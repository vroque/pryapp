﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.PRY
{
    [Table("PRY.tblProject")]
    public class PRYProject
    {
        public int Id { get; set; }
        public string Div { get; set; }
        public string Campus { get; set; }
        public decimal OwnerPidm { get; set; }
        public string Title { get; set; }
        public string Objective { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string IdMicrosoftTeams { get; set; }
        public string IdMicrosoftGroup { get; set; }
        public DateTime? ChangeStatusDate { get; set; }
        public bool Approved { get; set; }
        public int IdCategory { get; set; }
        public int IdProjectType { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}
