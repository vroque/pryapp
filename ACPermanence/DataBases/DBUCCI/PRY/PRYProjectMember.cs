﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.PRY
{
    [Table("PRY.tblProjectMember")]
    public class PRYProjectMember
    {
        public int Id { get; set; }
        public string Div { get; set; }
        public string Campus { get; set; }
        public string Program { get; set; }
        public string Department { get; set; }
        public decimal Pidm { get; set; }
        public bool Approved { get; set; }
        public int IdMemberType { get; set; }
        public int IdProject { get; set; }
        public int IdProfileProject { get; set; }
        public string Remarks { get; set; }
        public DateTime? ChangeStatusDate { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}
