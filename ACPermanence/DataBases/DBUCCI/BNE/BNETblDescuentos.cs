﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.BNE
{
    [Table("BNE.tblDescuentos")]
    public class BNETblDescuentos
    {
        public string IDDependencia { get; set; }
        public string TipoDescuento { get; set; }
        [Key]
        [Column(Order = 1)]
        public string IDDescuento { get; set; }
        public string NomDescuento { get; set; }
        public string Descripcion { get; set; }
        public int DescMat { get; set; }
        public int DescCuo { get; set; }
        public Boolean Escala { get; set; }
        public Boolean Beca { get; set; }
        public Boolean MediaBeca { get; set; }
        public Boolean TercioBeca { get; set; }
        public string IDPersonal { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public Boolean Activo { get; set; }
    }
}
