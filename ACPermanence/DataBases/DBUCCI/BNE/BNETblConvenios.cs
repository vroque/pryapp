﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.BNE
{
    /// <summary>
    /// Convenios registrados por la universidad
    /// </summary>
    [Table("BNE.tblConvenios")]
    public class BNETblConvenios
    {
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 1)]
        public string IDConvenio { get; set; }
        public string CodContrato { get; set; }
        public string NomEmpresa { get; set; }
        public DateTime FechaIniCont { get; set; }
        public DateTime FechaFinCont { get; set; }
        public string IDGrdFam { get; set; }
        public Boolean ClauDesap { get; set; }
        public Boolean RenovAuto { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string IDPersonal { get; set; }
        public int Activo { get; set; }
    }
}