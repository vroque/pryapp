﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.BNE
{
    [Table("BNE.tblConveniosDetalle")]
    public class BNETblConveniosDetalle
    {
        [Key]
        [Column(Order = 1)]
        public string IDConvenio { get; set; }
        [Key]
        [Column(Order = 2)]
        public string Modalidad { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDEscuelaADM { get; set; }
        public int MatriPorcent { get; set; }
        public int CuotaPorcent { get; set; }
    }
}
