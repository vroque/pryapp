﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.BNE
{
    [Table("BNE.tblGradFamiliaridad")]
    public class BNETblGradFamiliaridad
    {
        [Key]
        [Column(Order = 1)]
        public string IDGrdFam { get; set; }
        public string Descripcion { get; set; }
        public string Detalle { get; set; }

    }
}
