﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.BNE.StoredProcedure
{
    public class BNESPAsignarDescuento
    {
        public Boolean Valor { get; set; }
        public string Mensaje { get; set; }
    }
}
