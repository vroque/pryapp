﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.BNE
{
    [Table("BNE.tblAlumnoBeneficio")]
    public class BNETblAlumnoBeneficio
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDEscuelaADM { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDAlumno { get; set; }
        public string TipoBeneficio { get; set; }
        [Key]
        [Column(Order = 6)]
        public string IDBeneficio { get; set; }
        public string Observacion { get; set; }
        public string IDUsuario { get; set; }
        public DateTime FechaRegistro { get; set; }
        [Key]
        [Column(Order = 7)]
        public int Activo { get; set; }
    }
}
