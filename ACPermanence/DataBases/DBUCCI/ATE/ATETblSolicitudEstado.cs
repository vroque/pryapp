﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.ATE
{
    [Table("ate.tblSolicitudEstado")]
    public class ATETblSolicitudEstado
    {
        [Key]
        [Column(Order = 1)]
        public int IDSolicitudEstado { set; get; }
        public string descripcion { set; get; }
        
    }
}
