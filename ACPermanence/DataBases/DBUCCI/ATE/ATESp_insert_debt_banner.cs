﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.ATE
{
        public partial class sp_InsertDebt
    {
            public string IDSede { get; set; }
            public string IDAlumno { get; set; }
            public string IDPerAcad { get; set; }
            public string IDDependencia { get; set; }
            public string IDSeccionC { get; set; }
            public DateTime FecInic { get; set; }
            public string IDConcepto { get; set; }
            public DateTime FechaActualizacion { get; set; }
            public string IDCuenta { get; set; }
            public string IDEscuela { get; set; }
            public Double Cargo { get; set; }
            public string IDPagoOnline { get; set; }
            }
}
