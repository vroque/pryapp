﻿namespace ACPermanence.DataBases.DBUCCI.ATE.StoredProcedures
{
    public class ATEspGetListaMatriculadosBeca18
    {
        public string IDAlumno { get; set; }
        public string NomCompleto { get; set; }
        public string IDModalidadPostu { get; set; }
        public string Modalidad { get; set; }
        public string IDEscuela { get; set; }
        public string NomEsc { get; set; }
    }
}