﻿using System;

namespace ACPermanence.DataBases.DBUCCI.ATE.StoredProcedures
{
    /// <summary>
    /// Registra nueva persona (apoderado) en dbo.tblPersona
    /// </summary>
    public class ATEsetPersona
    {
        public string IdPersona { get; set; }
        public string Appat { get; set; }
        public string ApMat { get; set; }
        public string Nombres { get; set; }
        public string Sexo { get; set; }
        public DateTime FecNac { get; set; }
        public string Dni { get; set; }
        public string Ce { get; set; }
    }
}