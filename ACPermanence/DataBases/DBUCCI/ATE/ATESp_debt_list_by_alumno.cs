﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.ATE
{ 
        public partial class SPListDebt
    {
  
            public string IDAlumno { get; set; }
            public string Descripcion { get; set; }
            public string IDConcepto { get; set; }
            public Double Cargo { get; set; }
            public Double Abono { get; set; }
            public Double Deuda { get; set; }
            public Double DescMora { get; set; }
            public Double Descuento { get; set; }
            public decimal  CodigoBanco { get; set; }            
            public DateTime FecCargo { get; set; }
            public DateTime? FecProrroga { get; set; }            
            public DateTime FecInic { get; set; }
           
            }
}
