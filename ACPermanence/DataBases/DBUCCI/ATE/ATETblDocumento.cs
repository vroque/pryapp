﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.ATE
{
    [Table("ate.tblDocumento")]
    public class ATETblDocumento
    {
        [Key]
        [Column(Order = 1)]
        public int IDDocumento { set; get; }
        public string NomDocumento { set; get; }
        
    }
}
