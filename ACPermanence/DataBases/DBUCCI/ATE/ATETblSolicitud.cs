﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.ATE
{
    [Table("ate.tblSolicitud")]
    public class ATETblSolicitud
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { set; get; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { set; get; }
        [Key]
        [Column(Order = 3)]
        public string IDAlumno { set; get; }
        [Key]
        [Column(Order = 4)]
        public int IDSolicitud { set; get; }
        public DateTime FecSolicitud { set; get; }
        public string NroDocumento { set; get; }
        public int IDDocumento { set; get; }
        public int IDParentesco { set; get; }
        public string NomSolicitante { set; get; }
        public DateTime FecEntrega { set; get; }
        public int IDSolicitudEstado { set; get; }
        public string DescSolicitud { set; get; }
        public string IDOficinaUsuario { set; get; }
        public string Monto { set; get; }
        public string IDConcepto { set; get; }
        public string NomRecoje { set; get; }
        public string DNIRecoje { set; get; }
        public string IDOficina { set; get; }
        public string IDPerAcad { set; get; }
    }
}
