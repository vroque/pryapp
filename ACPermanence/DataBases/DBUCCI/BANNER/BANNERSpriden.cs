﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.BANNER
{
    [Table("BANNER.SPRIDEN")]
    public class BANNERSpriden
    {
        [Key]
        public decimal SPRIDEN_PIDM { get; set; }
        public string CONTI_PASS { get; set; }
    }
}
