﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblAsignaturaAcadUCONVA")]
    public class DBOTblAsignaturaAcadUCONVA
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDAsignatura { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDPlanEstudio { get; set; }
        [Key]
        [Column(Order = 5)]
        public int IDEscuelaR { get; set; }
        [Key]
        [Column(Order = 6)]
        public string IDAsignaturaR { get; set; }
        [Key]
        [Column(Order = 7)]
        public string IDPlanEstudioR { get; set; }
        [Key]
        [Column(Order = 8)]
        public string IDEscuelaP { get; set; }
        [Key]
        [Column(Order = 9)]
        public string IDEscuelaP2 { get; set; }
        public string tipo { get; set; }
        public int Obligatorio { get; set; }
        public string Grupo { get; set; }
    }
}
