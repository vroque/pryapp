﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("DBO.tblCategoriaPens")]
    public class DBOTblCategoriaPens
    {
        [Key]
        [Column(Order = 1)]
        public string IDCategoriaPens { get; set; }
        [Key]
        [Column(Order = 2)]
        public double PensCred { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 6)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 7)]
        public string IDEscala { get; set; }
    }
}
