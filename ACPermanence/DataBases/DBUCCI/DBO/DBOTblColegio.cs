﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    
    /// <summary>
    /// Tabla de Colegios
    /// </summary>
    [Table("dbo.tblColegio")]
    public class DBOTblColegio
    {
        [Key]
        public decimal IDColegio { get; set; }
        public string NomColegio { get; set; }
        public string IDDistrito { get; set; }
        public string TipoColegio { get; set; }
        /*public string Director { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
        public string IDProvincia { get; set; }
        public string IDDepartamento { get; set; }*/

    }
}
