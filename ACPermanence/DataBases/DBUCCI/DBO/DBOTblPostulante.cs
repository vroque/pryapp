﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblPostulante")]
    public class DBOTblPostulante
    {
        [Key]
        [Column(Order = 1)]
        public string IDAlumno { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 4)]
        public System.DateTime IDExamen { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDEscuelaADM { get; set; }
        public string IDPlanestudio { get; set; }
        
        
        public string IDEscuela { get; set; }
        
        public string IDCategoriaPens { get; set; }
        
        public System.DateTime FechaInscripcion { get; set; }
        public string Renuncia { get; set; }
        public string Ingresante { get; set; }
        public decimal IDModalidadPostu { get; set; }
        
        
    }
}
