﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblPersona")]
    public partial class DBOTblPersona
    {
        [Key]
        [Column(Order = 1)]
        public string IDPersona { get; set; }
        public int IDPersonaN { get; set; }
        public string Appat { get; set; }
        public string ApMat { get; set; }
        public string Nombres { get; set; }
        public string IDUsuario { get; set; }
        public string DNI { get; set; }
        public string Sexo { get; set; }
        public Nullable<System.DateTime> FecNac { get; set; }
        public string IDDepartamentoNac { get; set; }
        public string IDProvinciaNac { get; set; }
        public string IDDistritoNac { get; set; }
        public string IDDepartamentoProc { get; set; }
        public string IDProvinciaProc { get; set; }
        public string IDDistritoProc { get; set; }
        public string Partida { get; set; }
        public string Direccion { get; set; }
        public string IDDepartamento { get; set; }
        public string IDProvincia { get; set; }
        public string IDDistrito { get; set; }
        public string TelfCasa { get; set; }
        public string TeflOficina { get; set; }
        public string Celular { get; set; }
        public string Rpm { get; set; }
        public string TelfReferencia { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
        public string Email4 { get; set; }
        public string EstCivil { get; set; }
        public string RPC { get; set; }
        public decimal IDColegio { get; set; }
        public Nullable<double> UltimaPension { get; set; }
        public string Nacionalidad { get; set; }
        public string QReferencia { get; set; }
        public string CE { get; set; }
        public string CEPass { get; set; }
        public string IDPais { get; set; }
        public string NivelEducacion { get; set; }
        public string ProfesionOcupacion { get; set; }
        public int AnoEgresoSec { get; set; }

        public string PaisTrabaja { get; set; }
        public string IDProvinciaTrabajo { get; set; }
        public string IDDepartamentoTrabajo { get; set; }
        public string IDDistritoTrabajo { get; set; }
        public string Trabaja { get; set; }
        public string Ocupacion { get; set; }
        public string EstableContratado { get; set; }
        public Nullable<double> CostoPension { get; set; }
        public string CentroEstudio { get; set; }
        public string DireccionReferencia { get; set; }

        public string Discapacidad { get; set; }
        public string Seguro { get; set; }
        public Byte[] Foto { get; set; }
        public string FotoRuta { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
