﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblPlanEstudio")]
    public class DBOTblPlanEstudio
    {
        [Key]
        [Column(Order = 1)]
        public string IDPlanEstudio { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDTipoPlanEstudio { get; set; }
        /// <summary>
        /// El nombre esta mal en la Base de datos
        /// deberia se Descripcion
        /// </summary>
        public string Desripcion { get; set; }
        public string Activo { get; set; }
        public string Observaciones { get; set; }
        public string Vigente { get; set; }
    }
}
