﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    /// <summary>
    /// Notas CIC
    /// </summary>
    [Table("dbo.tblNotasEXT")]
    public class DBOtblNotasEXT
    {
        [Key]
        [Column(Order = 1)]
        public string IDsede { get; set; }

        [Key]
        [Column(Order = 2)]
        public string IDPerAcad { get; set; }

        [Key]
        [Column(Order = 3)]
        public string IDDependencia { get; set; }

        [Key]
        [Column(Order = 4)]
        public string IDSeccionC { get; set; }

        [Key]
        [Column(Order = 5)]
        public DateTime FecInic { get; set; }

        [Key]
        [Column(Order = 6)]
        public string IDEscuela { get; set; }

        [Key]
        [Column(Order = 7)]
        public string IDCursoEXT { get; set; }

        [Key]
        [Column(Order = 8)]
        public string IDDocenteEXT { get; set; }

        [Key]
        [Column(Order = 9)]
        public string IDAlumno { get; set; }

        public int n1 { get; set; }
        public int n2 { get; set; }
        public int n3 { get; set; }
        public int n4 { get; set; }
        public int n5 { get; set; }
        public int n6 { get; set; }
        public int n7 { get; set; }
        public decimal prom { get; set; }
        public string sust { get; set; }
        public int n_sust { get; set; }
        public string cert { get; set; }
        public string mater { get; set; }
        public DateTime? f_cert { get; set; }
        public DateTime? f_mater { get; set; }
        public int? n8 { get; set; }
        public int? n9 { get; set; }
        public int? n10 { get; set; }
        public int? n11 { get; set; }
        public int? n12 { get; set; }
        public int? n13 { get; set; }
        public int? n14 { get; set; }
        public int? n15 { get; set; }
        public int? n16 { get; set; }
        public int? n17 { get; set; }
        public int? n18 { get; set; }
        public int? n19 { get; set; }
        public int? n20 { get; set; }
        public int? n21 { get; set; }
        public int? n22 { get; set; }
        public int? n23 { get; set; }
        public int? n24 { get; set; }
        public int? n25 { get; set; }
        public int? n26 { get; set; }
        public int? n27 { get; set; }
        public int? n28 { get; set; }
        public int? n29 { get; set; }
        public int? n30 { get; set; }
        public string estado { get; set; }
        public string jurado1 { get; set; }
        public string jurado2 { get; set; }
        public string jurado3 { get; set; }
        public DateTime fechaexamen { get; set; }
        public DateTime? hini { get; set; }
        public DateTime? hfin { get; set; }
        public string ciclo { get; set; }
        public string obs { get; set; }
        public string carrera { get; set; }
        public DateTime? fechamat { get; set; }
        public int? n31 { get; set; }
        public int? n32 { get; set; }
        public int? n33 { get; set; }
        public int? n34 { get; set; }
        public int? n35 { get; set; }
        public int? n36 { get; set; }
        public int? n37 { get; set; }
        public int? n38 { get; set; }
        public int? n39 { get; set; }
        public int? n40 { get; set; }
        public int? n41 { get; set; }
        public int? n42 { get; set; }
        public int? n43 { get; set; }
        public int? n44 { get; set; }
        public int? n45 { get; set; }
        public int? n46 { get; set; }
        public int? n47 { get; set; }
        public int? n48 { get; set; }
        public int? n49 { get; set; }
        public int? n50 { get; set; }
        public string idioma { get; set; }
        public string nivel { get; set; }
        public string Activo { get; set; }
        public int? Falta { get; set; }
        public int? FaltaSesion { get; set; }
        public byte FaltaPorcentajeHora { get; set; }
        public string IDOferta { get; set; }
        public int? n51 { get; set; }
    }
}