﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("DBO.tblGrupoConceptos")]
    public class DBOtblGrupoConceptos
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDSeccionC { get; set; }
        [Key]
        [Column(Order = 5)]
        public DateTime FecInic { get; set; }
        [Key]
        [Column(Order = 6)]
        public string IDGrupoConcepto { get; set; }
        [Key]
        [Column(Order = 7)]
        public decimal CodigoBanco { get; set; }
        public string Descripcion { get; set; }
    }
}
