﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblAlumnoEstado")]
    public class DBOtblAlumnoEstado
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDAlumno { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDSeccionC { get; set; }
        public DateTime? FecMatricula { get; set; }
        public DateTime FecInic { get; set; }
        public string IDEscala { get; set; }
        public string IDEscuela { get; set; }
    }
}
