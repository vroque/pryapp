﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblPersonaAlumno")]
    public class DBOTblPersonaAlumno
    {
        [Key]
        [Column(Order = 1)]
        public string IDAlumno { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDPersona { get; set; }
        // Colocar los demas datos de ser necesario
    }
}
