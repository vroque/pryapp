﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("[dbo].[tblSeccionAcad]")]
    public class DBOTblSeccionAcad
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; } // [char](4) NOT NULL,
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; } // [varchar](6) NOT NULL,
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; } // [char](6) NOT NULL,
        [Key]
        [Column(Order = 4)]
        public string IDEscuela { get; set; } // [char](3) NOT NULL,
        [Key]
        [Column(Order = 5)]
        public string IDSeccion { get; set; } // [varchar](10) NOT NULL,
        [Key]
        [Column(Order = 6)]
        public string IDPlanEstudio { get; set; } // [varchar](10) NOT NULL,
        [Key]
        [Column(Order = 7)]
        public string IDAsignatura { get; set; } // [varchar](6) NOT NULL,
        public string IDDocente { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblSeccionAcad_IDDocente]  DEFAULT ('N00'),
        public string IDDocenteP { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblSeccionAcad_IDDocenteP]  DEFAULT ('N00'),
        public string IDDocenteActa1 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblSeccionAcad_IDDocenteActa1]  DEFAULT ('N00'),
        public string IDDocenteActa2 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblSeccionAcad_IDDocenteActa2]  DEFAULT ('N00'),
        public string IDDocenteActa3 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblSeccionAcad_IDDocenteActa21]  DEFAULT ('N00'),
        public string IDPonderacion { get; set; } // [varchar](10) NOT NULL,
        // public int RegistroNota { get; set; } // [tinyint] NOT NULL CONSTRAINT [DF_tblSeccionAcad_RegistroNota]  DEFAULT ((0)),
        //public DateTime Acta1Par { get; set; } // [datetime] NULL,
        //public DateTime Acta2Par { get; set; } // [datetime] NULL,
        //public DateTime ActaFinal { get; set; } // [datetime] NULL,
        //public DateTime ActaSusti { get; set; } // [datetime] NULL,
        //public DateTime ActaFinalSusti { get; set; } // [datetime] NULL,
        public byte CantDisponible { get; set; } // [tinyint] NOT NULL,
        public DateTime? FechaIni { get; set; } // [datetime] NULL,
        public DateTime? FechaPar { get; set; } // [datetime] NULL,
        public DateTime? FechaFin { get; set; } // [datetime] NULL,
        public string iddocente1 { get; set; } // [varchar](10) NULL,
        public string iddocente2 { get; set; } // [varchar](10) NULL,
        public int SemanasDuracion { get; set; } // [int] NOT NULL CONSTRAINT [DF_tblSeccionAcad_SemanasDuracion]  DEFAULT ((18)),
        public string Observaciones { get; set; } // [varchar](255) NULL,
        /*
        public string Upload1 { get; set; } // [tinyint] NOT NULL CONSTRAINT [DF_tblSeccionAcad_Upload]  DEFAULT ((0)),
        public string Procesado1 { get; set; } // [tinyint] NOT NULL CONSTRAINT [DF_tblSeccionAcad_Procesado]  DEFAULT ((0)),
        public string Upload2 { get; set; } // [tinyint] NOT NULL CONSTRAINT [DF_tblSeccionAcad_Upload11]  DEFAULT ((0)),
        public string Procesado2 { get; set; } // [tinyint] NOT NULL CONSTRAINT [DF_tblSeccionAcad_Procesado11]  DEFAULT ((0)),
        public string AceptaUpload1 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblSeccionAcad_AceptaUpload]  DEFAULT ('1'),
        public string AceptaUpload2 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblSeccionAcad_AceptaUpload2]  DEFAULT ('1'),
        public string NroHPedago { get; set; } // [tinyint] NOT NULL CONSTRAINT [DF_tblSeccionAcad_NroHPedago]  DEFAULT ((0)),
        public string PHAutomatico { get; set; } // [char](1) NOT NULL CONSTRAINT [DF__tblSeccio__PHAut__0F27E337]  DEFAULT ('0'),
        public string IDMoodle { get; set; } // [numeric](18, 0) NULL,
        public string IDCourseElearning { get; set; } //  AS ((((([IDDependencia]+[idsede])+[idperacad])+[idescuela])+[idseccion])+[idasignatura]),
        public string IDMoodleDoc { get; set; } // [numeric](18, 0) NULL,
        public string IDMoodleDocP { get; set; } // [numeric](18, 0) NULL,
        public string NotaAsistencia { get; set; } // [char](1) NOT NULL DEFAULT ('0'),
        public string Orden { get; set; } // [int] NULL CONSTRAINT [DF_tblSeccionAcad_Orden]  DEFAULT ((0)),
        public string Turno { get; set; } //  AS ([dbo].[f_SeccionGRP_Turno]([IDDependencia],[IDSeccion],[IDEscuela])),
        public string CantMatricCaja { get; set; } //  AS ([dbo].[f_CantMatriculadoXAsignaturaCaja]([IDDependencia],[IDSede],[IDPerAcad],[IDEscuela],[IDSeccion],[IDPlanEstudio],[IDAsignatura])),
        public string ActaImpreso { get; set; } // [int] NOT NULL DEFAULT ((0)),
        public string EstadoCarga { get; set; } // [int] NOT NULL DEFAULT ((0)),
        public string CantPreMatric { get; set; } //  AS ([dbo].[f_CantPreMatriculadoXAsignatura]([IDDependencia],[IDSede],[IDPerAcad],[IDEscuela],[IDSeccion],[IDPlanEstudio],[IDAsignatura])),
        public string CantMatric { get; set; } //  AS ([dbo].[f_CantMatriculadoXAsignatura]([IDDependencia],[IDSede],[IDPerAcad],[IDEscuela],[IDSeccion],[IDPlanEstudio],[IDAsignatura])),
        public string Modulo { get; set; } // [int] NOT NULL DEFAULT ((0)),
        public string IDUsuarioC { get; set; } // [varchar](20) NULL,
        public string NivelOferta1 { get; set; } // [char](1) NOT NULL DEFAULT ('T'),
        public string NivelOferta2 { get; set; } // [char](1) NOT NULL DEFAULT ('P'),
        public string Grupo { get; set; } //  AS (CONVERT([int],case when isnumeric(right([idseccion],(3)))=(1) then right([idseccion],(3)) else '0' end,0)),
        public string IDHoraPHGrp { get; set; } // [int] NOT NULL DEFAULT ((0)),
        public string IDFacultad { get; set; } // [varchar](10) NULL CONSTRAINT [DF_tblSeccionAcad_IDFacultad]  DEFAULT ('T'),
        public string Dirigido { get; set; } // [char](1) NOT NULL DEFAULT ('0'),
        public string EstadoSeccion { get; set; } // [varchar](10) NULL DEFAULT ('A'),
        public string IDPerAcadFin { get; set; } //  AS (case when [fechaini]<getdate() AND [fechafin]>getdate() AND [estadoseccion]='A' then 'Activo' else [idperacad] end),
        public string IDTipoPlanEstudio { get; set; } // [varchar](3) NULL,
        public string CorteModular1 { get; set; } // [bit] NULL,
        public string FecEntregaCorte1 { get; set; } // [datetime] NULL,
        public string CorteModular2 { get; set; } // [bit] NULL,
        public string FecEntregaCorte2 { get; set; } // [datetime] NULL,
        public string CorteModular3 { get; set; } // [bit] NULL,
        public string FecEntregaCorte3 { get; set; } // [datetime] NULL,
        public string NotaDesempeno1 { get; set; } // [float] NULL,
        public string NotaDesempeno2 { get; set; } // [float] NULL,
        public string Meta1 { get; set; } // [float] NULL,
        public string Meta2 { get; set; } // [float] NULL,
        public string Meta3 { get; set; } // [float] NULL,
        public string ValidaEP { get; set; } // [char](1) NULL DEFAULT ('1'),
        public string Seccion { get; set; } //  AS ([dbo].[f_SeccionGRP_Seccion]([IDDependencia],[IDSeccion],[IDEscuela])),
        public string NroSesiones { get; set; } //  AS ([dbo].[f_Seccion_CalcluloHorasSesiones]([IDDependencia],[IDSede],[IDPerAcad],[IDEscuela],[IDSeccion],[IDPlanEstudio],[IDAsignatura],[SemanasDuracion],(2))),
        public string NroHoras { get; set; } //  AS ([dbo].[f_Seccion_CalcluloHorasSesiones]([IDDependencia],[IDSede],[IDPerAcad],[IDEscuela],[IDSeccion],[IDPlanEstudio],[IDAsignatura],[SemanasDuracion],(1))),
        */
        public string Ciclo { get; set; } //  AS ([dbo].[f_SeccionGRP_Ciclo]([IDDependencia],[IDSeccion],[IDEscuela])), { get; set; } //
        
    }
}
