﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    /// <summary>
    /// TAbla de comparacion codigos de admision APEC RECLUITER
    /// * BANNER usa los mismos codigos de RECLUITER
    /// </summary>
    [Table("DBO.tblModalidadPostuRecruiter")]
    public class DBOTblModalidadPostuRecruiter
    {
        [Key]
        public Int64 id { get; set; }
        public string IDModRecruiter { get; set; }
        public string IDModalidadPostu { get; set; }
        public string departament { get; set; }
    }
}
