﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("DBO.tblExamenSustitutorioBANNER")]
    public class DBOTblExamenSustitutorioBANNER
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDTipoPlanEstudio { get; set; }
        [Key]
        [Column(Order = 6)]
        public string IDAlumno { get; set; }
        [Key]
        [Column(Order = 7)]
        public string IDSeccion { get; set; }
        [Key]
        [Column(Order = 8)]
        public string IDAsignatura { get; set; }
        public string NomEsc { get; set; }
        public string NomFacultad { get; set; }
        public string IDPlanEstudio { get; set; }
        public string NomCompleto { get; set; }
        public string NomAsignatura { get; set; }
        public int? Creditos { get; set; }
        public string AsigHabilidato { get; set; }
        public string EstadoSolicitudAsig { get; set; }
        public decimal C1 { get; set; }
        public decimal C2 { get; set; }
        public decimal PromedioPonderado { get; set; }
        public decimal EP { get; set; }
        public decimal EF { get; set; }
        public string NRC { get; set; }
        public string DniDocente { get; set; }
        public string Activo { get; set; }
        public DateTime? FecFechaInicio { get; set; }
        public DateTime? FecFechaFin { get; set; }
        public int? PorcInasistencia { get; set; }
        public string Grupo { get; set; }
        public string Estado { get; set; }
        public DateTime? FecRegistro { get; set; }
        public DateTime? FecSolicitudExa { get; set; }
        public string Justificacion { get; set; }
        public DateTime? FecMaxPago { get; set; }
        public string IDSedeUCCI { get; set; }
        public int IDPersonaN { get; set; }
        public string PartePeriodo { get; set; }
    }
}
