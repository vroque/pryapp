﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    /// <summary>
    /// Estilo para plantilla de Certificado de Estudios CIC
    /// </summary>
    [Table("dbo.tblseccionCdocenteEXT")]
    public class DBOtblSeccionCDocenteEXT
    {
        [Key]
        [Column(Order = 1)]
        public string IDsede {get;set;}
        [Key]
        [Column(Order = 2)]
        public string IDPerAcad {get;set;}
        [Key]
        [Column(Order = 3)]
        public string IDDependencia {get;set;}
        [Key]
        [Column(Order = 4)]
        public string IDSeccionC {get;set;}
        [Key]
        [Column(Order = 5)]
        public DateTime FecInic {get;set;}
        [Key]
        [Column(Order = 6)]
        public string IDEscuela {get;set;}
        [Key]
        [Column(Order = 7)]
        public string IDCursoEXT {get;set;}
        [Key]
        [Column(Order = 8)]
        public string IDDocenteEXT {get;set;}
        public string registrofin {get;set;}
        public string activo {get;set;}
        public string cicloCIC {get;set;}
        public int? cur_peso {get;set;}
        public int? peso_cur {get;set;}
        public decimal? con_prom {get;set;}
        public string modalidad {get;set;}
        public decimal? notaaprob {get;set;}
        public int? IDEstilo {get;set;}
        public bool cerrar {get;set;}
        public bool cerrarmat {get;set;}
    }

}
