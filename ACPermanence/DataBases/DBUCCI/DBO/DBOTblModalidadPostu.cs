﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    /// <summary> 
    /// Tabla 'dbo.tblModalidadPostu' con info sobre las Modalidades de Postulación 
    /// </summary> 
    [Table("dbo.tblModalidadPostu")]
    public class DBOTblModalidadPostu
    {
        [Key]
        [Column(Order = 1)]
        public decimal IDModalidadPostu { get; set; }

        [Key]
        [Column(Order = 2)]
        public string IDPerAcad { get; set; }

        [Key]
        [Column(Order = 3)]
        public string IDDependencia { get; set; }

        public string Descripcion { get; set; }
        public string IDEscuelaADM { get; set; }
    }
}