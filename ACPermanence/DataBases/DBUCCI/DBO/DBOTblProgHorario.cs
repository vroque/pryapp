﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("[dbo].[tblProgHorario]")]
    public class DBOTblProgHorario
    {
        [Key]
        [Column(Order = 1)]
        public DateTime IDHoraPH { get; set; } // [datetime] NOT NULL,
        [Key]
        [Column(Order = 2)]
        public DateTime IDHoraPHfin { get; set; } // [datetime] NOT NULL,
        [Key]
        [Column(Order = 3)]
        public string IDDependencia { get; set; } // [char](4) NOT NULL,
        [Key]
        [Column(Order = 4)]
        public string IDSede { get; set; } // [varchar](6) NOT NULL,
        [Key]
        [Column(Order = 5)]
        public string IDPerAcad { get; set; } // [char](6) NOT NULL,
        [Key]
        [Column(Order = 6)]
        public string IDEscuela { get; set; } // [char](3) NOT NULL,
        [Key]
        [Column(Order = 7)]
        public string IDSeccion { get; set; } // [varchar](10) NOT NULL,
        [Key]
        [Column(Order = 8)]
        public string IDAsignatura { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF__tblProgHo__IDAsi__06688004]  DEFAULT ('X'),
        [Key]
        [Column(Order = 9)]
        public string IDPlanEstudioEsp { get; set; } // [varchar](10) NOT NULL DEFAULT ('X'),
        [Key]
        [Column(Order = 10)]
        public string IDOferta { get; set; } // [varchar](5) NOT NULL CONSTRAINT [DF_tblProgHorario_IDOferta]  DEFAULT ('X'),
        [Key]
        [Column(Order = 11)]
        public byte Ciclo { get; set; } // [tinyint] NOT NULL CONSTRAINT [DF_tblProgHorario_Ciclo]  DEFAULT ((0)),
        [Key]
        [Column(Order = 12)]
        public string IDDocente { get; set; } // [varchar](3) NOT NULL DEFAULT ('N00'),
        [Key]
        [Column(Order = 13)]
        public string IDTipoPlanEstudio { get; set; } // [varchar](3) NOT NULL CONSTRAINT [DF_tblProgHorario_IDTipoPlanEstudio  ]  DEFAULT ('X'),public string  { get; set; } //
        public string IDAmbiente1 { get; set; }
        public string IDAmbiente2 { get; set; }
        public string IDAmbiente3 { get; set; }
        public string IDAmbiente4 { get; set; }
        public string IDAmbiente5 { get; set; }
        public string IDAmbiente6 { get; set; }
        public string IDAmbiente7 { get; set; }
        /*
        public string IDPlanEstudio1 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDPlanEstudio1]  DEFAULT (''),
        public string IDAsignatura1 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAsignatura1]  DEFAULT (''),
        public string IDDocente1 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDDocente1]  DEFAULT (''),
        public string IDAmbiente1 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAmbiente1]  DEFAULT (''),
        public string Tipo1 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Tipo1]  DEFAULT (''),
        public string IDPlanEstudio2 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDPlanEstudio2]  DEFAULT (''),
        public string IDAsignatura2 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAsignatura2]  DEFAULT (''),
        public string IDDocente2 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDDocente2]  DEFAULT (''),
        public string IDAmbiente2 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAmbiente2]  DEFAULT (''),
        public string Tipo2 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Tipo2]  DEFAULT (''),
        public string IDPlanEstudio3 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDPlanEstudio3]  DEFAULT (''),
        public string IDAsignatura3 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAsignatura3]  DEFAULT (''),
        public string IDDocente3 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDDocente3]  DEFAULT (''),
        
        public string Tipo3 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Tipo3]  DEFAULT (''),
        public string IDPlanEstudio4 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDPlanEstudio4]  DEFAULT (''),
        public string IDAsignatura4 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAsignatura4]  DEFAULT (''),
        public string IDDocente4 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDDocente4]  DEFAULT (''),
        public string IDAmbiente4 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAmbiente4]  DEFAULT (''),
        public string Tipo4 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Tipo4]  DEFAULT (''),
        public string IDPlanEstudio5 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDPlanEstudio5]  DEFAULT (''),
        public string IDAsignatura5 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAsignatura5]  DEFAULT (''),
        public string IDDocente5 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDDocente5]  DEFAULT (''),
        public string IDAmbiente5 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAmbiente5]  DEFAULT (''),
        public string Tipo5 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Tipo5]  DEFAULT (''),
        public string IDPlanEstudio6 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDPlanEstudio6]  DEFAULT (''),
        public string IDAsignatura6 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAsignatura6]  DEFAULT (''),
        public string IDDocente6 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDDocente6]  DEFAULT (''),
        public string IDAmbiente6 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAmbiente6]  DEFAULT (''),
        public string Tipo6 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Tipo6]  DEFAULT (''),
        public string IDPlanEstudio7 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDPlanEstudio7]  DEFAULT (''),
        public string IDAsignatura7 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAsignatura7]  DEFAULT (''),
        public string IDDocente7 { get; set; } // [varchar](10) NOT NULL CONSTRAINT [DF_tblProgHorario_IDDocente7]  DEFAULT (''),
        public string IDAmbiente7 { get; set; } // [varchar](6) NOT NULL CONSTRAINT [DF_tblProgHorario_IDAmbiente7]  DEFAULT (''),
        public string Tipo7 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Tipo7]  DEFAULT (''),
        public string Cruce1 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Cruce1]  DEFAULT ((0)),
        public string Cruce2 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Cruce2]  DEFAULT ((0)),
        public string Cruce3 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Cruce3]  DEFAULT ((0)),
        public string Cruce4 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Cruce4]  DEFAULT ((0)),
        public string Cruce5 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Cruce5]  DEFAULT ((0)),
        public string Cruce6 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Cruce6]  DEFAULT ((0)),
        public string Cruce7 { get; set; } // [char](1) NOT NULL CONSTRAINT [DF_tblProgHorario_Cruce7]  DEFAULT ((0)),
        public string IDEscuela1 { get; set; } // [char](3) NOT NULL CONSTRAINT [DF__tblProgHo__IDEsc__5100938C]  DEFAULT (''),
        public string IDEscuela2 { get; set; } // [char](3) NOT NULL CONSTRAINT [DF__tblProgHo__IDEsc__51F4B7C5]  DEFAULT (''),
        public string IDEscuela3 { get; set; } // [char](3) NOT NULL CONSTRAINT [DF__tblProgHo__IDEsc__52E8DBFE]  DEFAULT (''),
        public string IDEscuela4 { get; set; } // [char](3) NOT NULL CONSTRAINT [DF__tblProgHo__IDEsc__53DD0037]  DEFAULT (''),
        public string IDEscuela5 { get; set; } // [char](3) NOT NULL CONSTRAINT [DF__tblProgHo__IDEsc__54D12470]  DEFAULT (''),
        public string IDEscuela6 { get; set; } // [char](3) NOT NULL CONSTRAINT [DF__tblProgHo__IDEsc__55C548A9]  DEFAULT (''),
        public string IDEscuela7 { get; set; } // [char](3) NOT NULL CONSTRAINT [DF__tblProgHo__IDEsc__56B96CE2]  DEFAULT (''),
        public string IDSeccion1 { get; set; } // [char](10) NOT NULL CONSTRAINT [DF__tblProgHo__IDSec__57AD911B]  DEFAULT (''),
        public string IDSeccion2 { get; set; } // [char](10) NOT NULL CONSTRAINT [DF__tblProgHo__IDSec__58A1B554]  DEFAULT (''),
        public string IDSeccion3 { get; set; } // [char](10) NOT NULL CONSTRAINT [DF__tblProgHo__IDSec__5995D98D]  DEFAULT (''),
        public string IDSeccion4 { get; set; } // [char](10) NOT NULL CONSTRAINT [DF__tblProgHo__IDSec__5A89FDC6]  DEFAULT (''),
        public string IDSeccion5 { get; set; } // [char](10) NOT NULL CONSTRAINT [DF__tblProgHo__IDSec__5B7E21FF]  DEFAULT (''),
        public string IDSeccion6 { get; set; } // [char](10) NOT NULL CONSTRAINT [DF__tblProgHo__IDSec__5C724638]  DEFAULT (''),
        public string IDSeccion7 { get; set; } // [char](10) NOT NULL CONSTRAINT [DF__tblProgHo__IDSec__5D666A71]  DEFAULT (''),
        */
        public int IDHoraPHGrp { get; set; } // [int] NOT NULL CONSTRAINT [DF__tblProgHo__IDHor__05745BCB]  DEFAULT ('1'),
        public string NSesion1 { get; set; } // [char](1) NOT NULL DEFAULT ((0)),
        public string NSesion2 { get; set; } // [char](1) NOT NULL DEFAULT ((0)),
        public string NSesion3 { get; set; } // [char](1) NOT NULL DEFAULT ((0)),
        public string NSesion4 { get; set; } // [char](1) NOT NULL DEFAULT ((0)),
        public string NSesion5 { get; set; } // [char](1) NOT NULL DEFAULT ((0)),
        public string NSesion6 { get; set; } // [char](1) NOT NULL DEFAULT ((0)),
        public string NSesion7 { get; set; } // [char](1) NOT NULL DEFAULT ((0)),
        public string IDOferta1 { get; set; } // [varchar](5) NOT NULL DEFAULT ('0'),
        public string IDOferta2 { get; set; } // [varchar](5) NOT NULL DEFAULT ('0'),
        public string IDOferta3 { get; set; } // [varchar](5) NOT NULL DEFAULT ('0'),
        public string IDOferta4 { get; set; } // [varchar](5) NOT NULL DEFAULT ('0'),
        public string IDOferta5 { get; set; } // [varchar](5) NOT NULL DEFAULT ('0'),
        public string IDOferta6 { get; set; } // [varchar](5) NOT NULL DEFAULT ('0'),
        public string IDOferta7 { get; set; } // [varchar](5) NOT NULL DEFAULT ('0'),
        
    }
}
