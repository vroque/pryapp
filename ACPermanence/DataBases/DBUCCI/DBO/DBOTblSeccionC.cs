﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblSeccionC")]
    public class DBOTblSeccionC
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDsede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDSeccionC { get; set; }
        [Key]
        [Column(Order = 6)]
        public string IDCategoriaPens { get; set; }
        public DateTime FecInic { get; set; }
        public DateTime? FecFin { get; set; }
        public DateTime? FecInicReal { get; set; }
        public DateTime? FecFincReal { get; set; }
    }
}
