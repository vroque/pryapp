﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblRelacionFamiliar")]
    public class DBOTblRelacionFamiliar
    {
        [Key]
        public int id { get; set; }
        public string persona { get; set; }
        public string familiar { get; set; }
        public string relacion { get; set; }
    }
}
