﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblReglasEgresado")]
    public class DBOTblReglasEgresado
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDEscuela { get; set; }
        public int CreditosO { get; set; }
        public byte CreditosE { get; set; }
        public string IDUsuario { get; set; }
        public string IDPlanEstudio { get; set; }
    }
}
