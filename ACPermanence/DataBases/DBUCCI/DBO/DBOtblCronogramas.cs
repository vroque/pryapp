﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblCronogramas")]
    public class DBOtblCronogramas
    {
        [Key]
        [Column(Order = 1)]
        public string IDEscuelaADM { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string Cronograma { get; set; }
        public string Traslado { get; set; }
        public string Reincorporacion { get; set; }
        public string IDDepartamento { get; set; }
        public string IDCampus { get; set; }
        public int Pronabec { get; set; }
        public int Activo { get; set; }
        public string NuevoPlan { get; set; }
    }
}