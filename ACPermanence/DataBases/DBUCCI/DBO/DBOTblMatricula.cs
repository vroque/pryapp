﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblMatricula")]
    public class DBOTblMatricula
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia {get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede {get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad {get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDEscuela {get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDSeccion {get; set; }
        [Key]
        [Column(Order = 6)]
        public string IDPlanEstudio {get; set; }
        [Key]
        [Column(Order = 7)]
        public string IDAsignatura {get; set; }
        [Key]
        [Column(Order = 8)]
        public string IDAlumno {get; set; }
        [Key]
        [Column(Order = 9)]
        public string IDtipoMatricula {get; set; }
        public int IDMatriculaDoc {get; set;}
        public DateTime FecMatricula {get; set;}
        public DateTime FechaNota {get; set;}
        public byte Nota {get; set; }
        public byte NotaP {get; set; }
        //public string NotaLetra {get; set; }
        public string Tipo {get; set; }
        public string Estado {get; set; }
        public byte Vez {get; set; }
        public byte FaltaSesion {get; set; }
        public byte Falta {get; set; }
        public byte Justificacion {get; set; }
        public string Resolucion {get; set; }
        public string Observaciones {get; set; }
        public byte CL1 { get; set; }
        public byte TA1 { get; set; }
        public byte EP { get; set; }
        public byte CL2 { get; set; }
        public byte TA2 { get; set; }
        public byte EF { get; set; }
        public int PROM { get; set; }
        //public string PL {get; set; }
        //public byte CL10 {get; set; }
        //public byte TA10 {get; set; }
        //public byte EP0 {get; set; }
        //public byte CL20 {get; set; }
        //public byte TA20 {get; set; }
        //public byte EF0 {get; set; }
        //public int PROM0 {get; set;}
        //public string PL0 {get; set; }
        //public DateTime Fecha {get; set;}
        //public string Susti {get; set; }
        //public byte NotaConv {get; set; }
        //public float idmoodle {get; set; }
        //public int ED {get; set;}
        //public string AEP {get; set; }
        //public string AEF {get; set; }
        //public int ImpedidoEF {get; set;}
        //public string NroSolicitud {get; set; }
        //public string AED {get; set; }
        //public string IDOferta {get; set; }
        //public byte EFR {get; set; }
        public string Rezagado {get; set; }
        public byte NotaRezagado {get; set; }
        public string AEFR {get; set; }
        //public byte FaltaPorcentaje {get; set; }
        //public byte FaltaPorcentajeHora {get; set; }
        //public string NroSoliRez {get; set; }
        //public string CPU {get; set; }
        //public string CSU {get; set; }
        //public string CPU2 {get; set; }
        //public string CSU2 {get; set; }
        //public string ObsCarga {get; set; }
        //public DateTime FechaCargaEP {get; set;}
        //public DateTime FechaCargaEF {get; set;}
        //public string TEP {get; set; }
        //public DateTime FechaTEP {get; set;}
        //public string TEF {get; set; }
        //public DateTime FechaTEF {get; set;}
        //public string PER {get; set; }
        //public byte NER {get; set; }
        //public string TE {get; set; }
        //public string Just {get; set; }
        //public string RecuAprobacion {get; set; }
        //public byte EPRecu {get; set; }
        //public DateTime FechaEPRecu {get; set;}
        //public string EPEstado {get; set; }
        //public byte EFRecu {get; set; }
        //public string EFEstado {get; set; }
        //public DateTime FechaEFRecu {get; set;}
        //public int NroCuadernillo {get; set;}
        //public byte AI {get; set; }
        //public byte INV {get; set; }
        //public byte SI {get; set; }
        //public string Ciclo {get; set; }
        //public byte INT1 {get; set; }
        //public byte INT2 {get; set; }
        //public byte INT3 {get; set; }
        //public byte INT4 {get; set; }
        //public byte INT5 {get; set; }
        //public byte INT6 {get; set; }
    }
}
