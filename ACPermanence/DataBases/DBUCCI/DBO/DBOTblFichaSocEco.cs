﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("DBO.TblFichaSocEco")]
    public class DBOTblFichaSocEco
    {
        [Key]
        public int id { get; set; }
        public string version { get; set; }
        public string persona { get; set; }
        public string nivel { get; set; }
        public string clave { get; set; }
        public string valor { get; set; }
        public string tag { get; set; }
        //DateTime nullable es necesario
        public DateTime? fecha { get; set; }
    }
}
