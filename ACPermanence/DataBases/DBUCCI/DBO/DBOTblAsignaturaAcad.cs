﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblAsignaturaAcad")]
    public class DBOtblAsignaturaAcad
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPlanEstudio { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDAsignatura { get; set; }
        public string NomAsignatura { get; set; }
        public string NomAsignaturaT { get; set; }
        public byte Ciclo { get; set; }
        public string Tipo { get; set; }
        public byte HTeoricas { get; set; }
        public byte HPracticas { get; set; }
        public byte Creditos { get; set; }
        public string letra { get; set; }
        public string CicloLetra { get; set; }
        public string Actividad { get; set; }
        public string IDEscuelaP { get; set; }
        public byte Grupo { get; set; }
        public string IDAsignaturaAnt { get; set; }
        public string IDAsignaturaEqVa { get; set; }
        public string Activo { get; set; }
        public int Color { get; set; }
        public string CreditosModular { get; set; }
        public byte CicloReferencial { get; set; }
        public string TipoAsig { get; set; }
    }
}
