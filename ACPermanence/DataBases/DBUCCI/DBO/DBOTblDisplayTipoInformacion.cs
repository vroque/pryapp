﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    /// <summary>
    /// Tipo de Widget a mostrat
    /// </summary>
    [Table("dbo.tblDisplayTipoInformacion")]
    public class DBOTblDisplayTipoInformacion
    {
        [Key]
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}