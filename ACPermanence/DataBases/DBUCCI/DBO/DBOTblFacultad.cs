﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblFacultad")]
    public class DBOTblFacultad
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { set; get; }
        [Key]
        [Column(Order = 2)]
        public string IDFacultad { set; get; }
        public string NomFacultad { set; get; }
        public string Ver { set; get; }

    }
}
