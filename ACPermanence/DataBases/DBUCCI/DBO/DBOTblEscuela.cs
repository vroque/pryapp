﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblEscuela")]
    public class DBOTblEscuela
    {
        [Key]
        [Column(Order=1)]
        public string IDCategoriaPens { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDCentroCostoTemp { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDCuentaCar { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDCuentaIns { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 6)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 7)]
        public string IDFacultad { get; set; }
        [Key]
        [Column(Order = 8)]
        public string IDTipoEsc { get; set; }
        [Key]
        [Column(Order = 9)]
        public string IDTituloCEP { get; set; }
        public string Activo { get; set; }
        public string AdmWebDisponible { get; set; }
        public double? Color { get; set; }
        public int? CredElectAprob { get; set; }
        public int? CredObligAprob { get; set; }
        public string DepAcademico { get; set; }
        public string Facultad { get; set; }
        public string Familia { get; set; }
        public string IngresoDirecto { get; set; }
        public string MailResponsable { get; set; }
        public string Nombre { get; set; }
        public string NombreSemestre { get; set; }
        public string NombreSemestreTipo { get; set; }
        public string NomEsc { get; set; }
        public string NomEscuela { get; set; }
        public string NumCiclos { get; set; }
        public string Programa { get; set; }
        public string RutaActa { get; set; }
        public string Ver { get; set; }
        public string VerWeb { get; set; }
    }
}
