﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    /// <summary>
    /// Valores de Configuracion para Los Widgets de la TV
    /// </summary>
    [Table("dbo.tblDisplayConfiguracion")]
    public class DBOTblDisplayConfiguracion
    {
        [Key]
        public int Id { get; set; }
        public string Valor { get; set; }
        public string Descripcion { get; set; }
    }
}