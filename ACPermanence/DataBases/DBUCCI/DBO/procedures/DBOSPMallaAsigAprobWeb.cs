﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.DBO.procedures
{
    public class DBOSPMallaAsigAprobWeb
    {
        public string IDAsignatura { get; set; }
        public string NomAsignatura { get; set; }
        public string CicloLetra { get; set; }
        public string TipoAsig { get; set; }
        public int Creditos { get; set; }
        public int CantAsigLLeva { get; set; }
        public int Nota { get; set; }
    }
}
