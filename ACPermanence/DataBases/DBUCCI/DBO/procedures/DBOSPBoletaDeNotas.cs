﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.DBO.procedures
{
    public class DBOSPBoletaDeNotas
    {
        public string IDAlumno { get; set; }
        public string NomCompleto { get; set; }
        public string NomEsc { get; set; }
        public string NomFacultad { get; set; }
        public string IDAsignatura { get; set; }
        public byte Creditos { get; set; }
        public string IDSeccion { get; set; }
        public string NomAsignatura { get; set; }
        public Nullable<byte> CL1 { get; set; }
        public Nullable<byte> TA1 { get; set; }
        public Nullable<byte> CL2 { get; set; }
        public Nullable<byte> TA2 { get; set; }
        public byte Nota { get; set; }
        public string IDPerAcad { get; set; }
        public Nullable<double> PromedioPonderado { get; set; }
        public Nullable<byte> EP { get; set; }
        public Nullable<byte> EF { get; set; }
        public string IDPlanEstudio { get; set; }
        public string IDEscuela { get; set; }
    }
}
