﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblConstanciaCIC")]
    public class DBOtblConstanciaCIC
    {
        [Key]
        [Column(Order = 1)]
        public string IDAlumno { get; set; }
        [Key]
        [Column(Order = 2)]
        public int idcons { get; set; }
        public string nivelAlcanzado { get; set; }
        [Key]
        [Column(Order = 4)]
        public string NroConstacia { get; set; }
        public string nhoras { get; set; }
        public string califica { get; set; }
        public DateTime fecha { get; set; }
        public DateTime desde { get; set; }
        public DateTime hasta { get; set; }
        public string obs { get; set; }
        public string estadonivel { get; set; }
        public DateTime? fechaMod { get; set; }
        public int impreso { get; set; }
        public string Usuario { get; set; }
        public DateTime fechaEmision { get; set; }
        public string Entregado { get; set; }
        public string IDDependencia { get; set; }
        public string IDSede { get; set; }
        public string IDPerAcad { get; set; }
        public string IDSecciónC { get; set; }
        public string IDConcepto { get; set; }
        public string Descripcion { get; set; }
        public double? Abono { get; set; }
        public string nivel { get; set; }
        public string AreaEstudio { get; set; }
        public string area { get; set; }
    }
}