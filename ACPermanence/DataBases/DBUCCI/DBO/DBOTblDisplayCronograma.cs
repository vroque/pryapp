﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    /// <summary>
    /// Muestra que 
    /// </summary>
    [Table("dbo.tblDisplayCronograma")]
    public class DBOTblDisplayCronograma
    {
         [Key]
         public int Id { get; set; }
         public string Dia { get; set; }
         public TimeSpan HoraInicio { get; set; }
         public TimeSpan HoraFin { get; set; }
    }
}
