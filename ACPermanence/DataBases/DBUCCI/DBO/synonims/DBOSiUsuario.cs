﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO.synonims
{
    [Table("dbo.SI_Usuario")]
    public class DBOSiUsuario
    {
        [Key]
        public string IDUsuario { get; set; }
        public string IDInstancia { get; set; }
        public string PassWord { get; set; }
        public string NomUSuario { get; set; }
        public string IDSQL { get; set; }
        public string Nkey { get; set; }
        public string dev { get; set; }

    }
}
