﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    /// <summary>
    /// Muestra los Widgets que se encuentran en un cronograma
    /// </summary>
    [Table("dbo.tblDisplayCronoTipInformacion")]
    public class DBOTblDisplayCronoTipInformacion
    {
        [Key]
        public int Id { get; set; }
        public int IdTipoInformacion { get; set; }
        public int IdCronograma { get; set; }
    }
}