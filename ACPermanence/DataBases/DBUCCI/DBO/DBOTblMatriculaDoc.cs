﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblMatriculaDOC")]
    public class DBOTblMatriculaDoc
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }

        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }

        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDAlumno { get; set; }

        [Key]
        [Column(Order = 5)]
        public string IDtipoMatricula { get; set; }

        [Key]
        [Column(Order = 6)]
        public int IDMatriculaDoc { get; set; }

        public string IDMatricula { get; set; }
        public string IDPreMatricula { get; set; }
        public string Condicion { get; set; }
        public DateTime? fecha { get; set; }
        public DateTime? hora { get; set; }
        public string IDAsesor { get; set; }
        public string UsuarioPC { get; set; }
        public string Observaciones { get; set; }
        public string IDEscuela { get; set; }
        public string CondicionTutoria { get; set; }
        public string IDTutor { get; set; }



    }
}
