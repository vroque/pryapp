﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("DBO.tblConceptos")]
    public class DBOTblConceptos
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDSeccionC { get; set; }
        [Key]
        [Column(Order = 6)]
        public DateTime FecInic { get; set; }
        [Key]
        [Column(Order = 7)]
        public string IDConcepto { get; set; }
        public string IDGrupoConcepto { get; set; }
        public int Orden { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionPreM { get; set; }
        public double monto { get; set; }
        
    }
}
