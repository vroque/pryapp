﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblPlanEstudioDet")]
    public class DBOTblPlanEstudioDet
    {
        [Key]
        [Column(Order=1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDEscuelaP { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDAsignatura { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDPlanEstudio { get; set; }
        public string Tipo { get; set; }
        public byte Ciclo { get; set; }
        public string IDAsignaturaAnt { get; set; }
        public string Activo { get; set; }
        public string IDAsignaturaEqVa { get; set; }
        public string Diploma { get; set; }
        public string CicloLetra { get; set; }
        public string IDEscuelaA { get; set; }
        public string Bloqueo { get; set; }
        public string PlanEstudioVersion { get; set; }
        public string PlanEstudioModulo { get; set; }
        public byte CicloReferencial { get; set; }
    }
}

