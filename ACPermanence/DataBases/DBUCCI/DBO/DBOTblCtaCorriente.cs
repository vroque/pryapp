﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblctacorriente")]
    public class DBOTblCtaCorriente
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDPerAcad { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDSeccionC { get; set; }
        [Key]
        [Column(Order = 6)]
        public DateTime FecInic { get; set; }
        [Key]
        [Column(Order = 7)]
        public string IDConcepto { get; set; }
        [Key]
        [Column(Order = 8)]
        public string IDAlumno { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string IDCuenta { get; set; }
        public string IDCuentaS { get; set; }
        public double Deuda { get; set; }
        public double Cargo { get; set; }
        public double Abono { get; set; }
        public double Descuento { get; set; }
        public double DescMora { get; set; }
        public DateTime? FecCargo { get; set; }
        public DateTime? FecProrroga { get; set; }

    }
}
