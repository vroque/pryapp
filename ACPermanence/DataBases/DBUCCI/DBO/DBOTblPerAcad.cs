﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    [Table("dbo.tblPerAcad")]
    public class DBOTblPerAcad
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia{ get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDPerAcad { get; set; }
        public string IDPerAcadAnt { get; set; }
        public string IDPerAcadSig { get; set; }
        public string IDPerAcadSigE { get; set; }
        public int CredMax { get; set; }
        public string Modificar { get; set; }
        public DateTime FechaINI { get; set; }
        public DateTime FechaFIN { get; set; }
        public int NotaAprobatoria { get; set; }
        public int NotaMinima { get; set; }
        public string ModificaEscala { get; set; }
        public string VerWebIngAdm { get; set; }
        // Colocar los demas datos de ser necesario
    }
}
