﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO
{
    /// <summary>
    /// Tabla de Egresados UC
    /// </summary>
    [Table("dbo.tblAlumnoEgresado")]
    public class DBOTblAlumnoEgresado
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public string IDEscuela { get; set; }
        [Key]
        [Column(Order = 4)]
        public string IDAlumno { get; set; }
        [Key]
        [Column(Order = 5)]
        public string IDPerAcad { get; set; }
        public string IDPerAcadE { get; set; }
        public int CreditosO { get; set; }
        public int CreditosE { get; set; }
        public string IDPerAcadBA { get; set; }
        public string Bachiller { get; set; }
        public DateTime? BAFechaComision { get; set; }
        public string BANroResolucion { get; set; }
        public DateTime? BAFechaResolucion { get; set; }
        public string BANroRegANR { get; set; }
        public string Titulado { get; set; }
        public DateTime? TIFechaResolucion { get; set; }
        public string IDPerAcadTI { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public string EstadoEG { get; set; }
        public DateTime? FecEstadoAPEC { get; set; }
        public DateTime? FecEstadoAPECI { get; set; }
        public DateTime? FecEstadoAPECR { get; set; }
        public string CertificadoRutaHoja1PDF { get; set; }
        public string CertificadoHoja1PDF { get; set; }
        public DateTime? FecGenCertificadoHoja1PDF { get; set; }
        public string CertificadoRutaHoja2PDF { get; set; }
        public string CertificadoHoja2PDF { get; set; }
        public DateTime? FecGenCertificadoHoja2PDF { get; set; }
        public string HistorialRutaPDF { get; set; }
        public string HistorialPDF { get; set; }
        public DateTime? FecGenHistorialPDF { get; set; }
        public string IDPlanEstudio { get; set; }
        public string ProySocial { get; set; }
        public DateTime? FecProySocial { get; set; }
        public string Idioma { get; set; }
        public string IDPeracadIni { get; set; }
        public DateTime? FecIdioma { get; set; }
        public string PracPreProfecio { get; set; }
        public DateTime? FecPracPreProfecio { get; set; }
        public string Taller { get; set; }
        public DateTime? FecTaller { get; set; }
        public DateTime? FechaEgresoUC { get; set; }
        public string ModalidadEgreso { get; set; }
    }
}
