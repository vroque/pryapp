﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO.views
{
    [Table("dbo.DAT_AlumnoBasico")]
    public partial class DBODATAlumnoBasico
    {
        [Key]
        [Column(Order = 1)]
        public string IDAlumno { get; set; }
        public string ApPat { get; set; }
        public string ApMat { get; set; }
        public string Nombres { get; set; }
        public string NomCompleto { get; set; }
        public string Sexo { get; set; }
        [Key]
        [Column(Order = 9)]
        public string IDPersona { get; set; }
        [Column(Order = 11)]
        public string DNI { get; set; }
        [Key]
        [Column(Order = 12)]
        public int IDPersonaN { get; set; }
    }
}