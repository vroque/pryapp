﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO.views
{
    [Table("[dbo].[DAT_Docente]")]
    public class DBODatDocente
    {
        [Key]
        [Column(Order = 1)]
        public string IDPersona { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDDocente { get; set; }
        public string Appat { get; set; }
        public string ApMat { get; set; }
        public string Nombres { get; set; }
        public string NomCompleto { get; set; }
        public string IDUsuarioNT { get; set; }
        public string IDUsuario { get; set; }
        public string TeflOficina { get; set; }
        public string TelfCasa { get; set; }
        public string TelfReferencia { get; set; }
        public string DNI { get; set; }
        public string Sexo { get; set; }
        public DateTime? FecNac { get; set; }
        public string EstCivil { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string IDDepartamento { get; set; }
        public string IDProvincia { get; set; }
        public string IDDistrito { get; set; }
        public string Celular { get; set; }
        public string Rpm { get; set; }
        public string Email3 { get; set; }
        public string Email4 { get; set; }
        public int IDPersonaN { get; set; }
    }
}
