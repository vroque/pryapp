﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.DBO.views
{
    [Table("dbo.DAT_Alumno")]
    public partial class DBODATAlumno
    {
        [Key]
        [Column(Order = 1)]
        public string IDAlumno { get; set; }
        public string ApPat { get; set; }
        public string ApMat { get; set; }
        public string Nombres { get; set; }
        //[Key]
        public string IDPersona { get; set; }
        public int IDPersonaN { get; set; }
    }
}

