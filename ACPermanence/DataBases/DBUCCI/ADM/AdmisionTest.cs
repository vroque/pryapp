﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.ADM
{
        public partial class TestAdmisionsp_PreguntasByTest_CF
        {
            public int ID { get; set; }
            public string NombreTest { get; set; }
            public int IDPreguntaByTest { get; set; }
            public string Pregunta { get; set; }
            public string Alt1 { get; set; }
            public string Alt2 { get; set; }
            public string Alt3 { get; set; }
            public string Alt4 { get; set; }
            public string Alt5 { get; set; }
            public string UrlImagen { get; set; }
            public string Respuesta { get; set; }

        }
        public partial class TestAdmisionsp_GetDatosAlumnoTestAdmision_CF
        {
            public string IDAlumno { get; set; }
            public string IDDependencia { get; set; }
            public string IDSede { get; set; }
            public DateTime IDExamen { get; set; }
            public string IDPerAcad { get; set; }
            public string IDSeccionC { get; set; }
            public string NomEscuela { get; set; }
            public string Modalidad { get; set; }
        }
        public partial class TestAdmisionsp_Update_tblTestRespuesta_CF
        {
            public string RESULT { get; set; }
        }
        public partial class TestAdmisionsp_CompletoTest_CF
        {
            public string RESULT { get; set; }
        }
        public partial class TestAdmisionsp_ResultadoEscalaMotivacion_CF
        {
            public int Orden { get; set; }
            public string Indicador { get; set; }
            public string Resultado { get; set; }
        }
        public partial class TestAdmisionsp_ResultadoInteresesOcu1_CF
        {
            public string CAMPOS { get; set; }
            public string INTENSIDAD { get; set; }
        }
        public partial class TestAdmisionsp_ResultadoInteresesOcu2_CF
        {
            public string CAMPOS { get; set; }
            public string INTENSIDAD { get; set; }
        }
        public partial class TestAdmisionsp_ResultadoInteresesOcu4_CF
        {
            public string CAMPOS { get; set; }
            public string INTENSIDAD { get; set; }
        }
        public partial class TestAdmisionsp_ResultadoEscalaHabilidades_CF
        {
            public string Area { get; set; }
            public string Resultado { get; set; }
        }
        public partial class TestAdmisionsp_RealizoTestAdmision_CF
        {
            public string RESULT { get; set; }
        }

}
