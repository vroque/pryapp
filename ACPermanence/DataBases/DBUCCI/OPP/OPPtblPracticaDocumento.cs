﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.OPP
{
    [Table("OPP.tblPracticaDocumento")]
    public class OPPtblPracticaDocumento
    {
        [Key]
        public string IDDependencia { get; set; }
        public string IDSede { get; set; }
        public int IDPracticaDocumento { get; set; }
        public string IDPractica { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string IDPersonaRegistro { get; set; }
        public string IDEscuela { get; set; }
        public string IDAlumno { get; set; }
    }
}
