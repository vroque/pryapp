﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.COU
{
    [Table("COU.tblValidacionAsignatura")]
    public class COUTblValidacionAsignatura
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public int ID { get; set; }
        public int IDDocumentoValidacion { get; set; }
        public string IDPlanEstudio { get; set; }
        public string IDAsignatura { get; set; }
        public string Nombre { get; set; }
        public int Creditos { get; set; }
        public string Nota { get; set; }
        public string Tipo { get; set; }
        public string Situacion { get; set; }
        public int Cantidad { get; set; }
        public string Extra { get; set; }
        public string IDEscuela { get; set; }
        public string IDEscuelaP { get; set; }
    }
}
