﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.DBUCCI.COU
{
    [Table("COU.tblValidacionDocumento")]
    public class COUTblValidacionDocumento
    {
        [Key]
        [Column(Order = 1)]
        public string IDDependencia { get; set; }
        [Key]
        [Column(Order = 2)]
        public string IDSede { get; set; }
        [Key]
        [Column(Order = 3)]
        public int ID { get; set; }
        public int IDValidacion { get; set; }
        public int IDSolicitud { get; set; }
        public string IDAlumno { get; set; }
        public string Nombre { get; set; }
        public string Numero { get; set; }
        public string Observaciones { get; set; }
        public string Anno { get; set; }
        public string IDPais { get; set; }
        public int IDUniversidadExt { get; set; }
        public int IDCarreraExt { get; set; }
        public string IDPerAcad { get; set; }
        public string Criterio { get; set; }
        public DateTime Fecha { get; set; }
    }
}
