﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ACPermanence.DataBases.DBUCCI.PRS
{
    [Table("PRS.tblIntegranteProyecto")]
    public class PRStblIntegranteProyecto
    {
        [Key]
        public int IDIntegrante { get; set; }
        public string IDAlumno { get; set; }
        public int IDProyecto { get; set; }
        public int IDEstado { get; set; }
        public string Responsable { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
