﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.DBUCCI.PRS
{
    [Table("PRS.tblProyecto")]
    public class PRStblProyecto
    {
        [Key]
        public int IDProyecto { get; set; }
        public string NombreProyecto { get; set; }
        public string Asesor { get; set; }
        public string Descripcion { get; set; }
        public int IDEstado { get; set; }
        public string IDPerAcad { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
