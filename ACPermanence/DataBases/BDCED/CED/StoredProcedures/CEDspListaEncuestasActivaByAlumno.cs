namespace ACPermanence.DataBases.BDCED.CED.StoredProcedures
{
    /// <summary>
    /// Datos que devuelve el procedimiento: CED.sp_ListaEncuestasActiva_by_alumno
    /// </summary>
    public class CEDspListaEncuestasActivaByAlumno
    {
        public int Id { get; set; }
        public string NombreEncuesta { get; set; }
        public string RutaEncuesta { get; set; }
    }
}