﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Grade Component Definition Table. 
    /// </summary>
    [Table("SHRSCOM")]
    public class SHRSCOM
    {
        /// <summary>
        /// SUB-COMPONENT TERM CODE: This field indicates the
        /// term associated with the Gradable Component
        /// </summary>
        [Key]
        [Column(Order = 1)]
        public string SHRSCOM_TERM_CODE { get; set; }
        /// <summary>
        /// SUB-COMPONENT COURSE NUMBER: This field indicates
        /// the CRN associated with the Gradable Component
        /// </summary>
        [Key]
        [Column(Order = 2)]
        public string SHRSCOM_CRN { get; set; }
        /// <summary>
        /// SUB-COMPONENT IDENTIFIER: This field the unique
        /// Gradable Sub Component Identifier
        /// </summary>
        [Key]
        [Column(Order = 3)]
        public int? SHRSCOM_ID { get; set; }
        /// <summary>
        /// SUB-COMPONENT COMPONENT IDENTIFIER: This field
        /// indicates the gradable component the
        /// sub-component is linked with.
        /// </summary>
        [Key]
        [Column(Order = 4)]
        public int? SHRSCOM_GCOM_ID { get; set; }
        //public float? SHRSCOM_SEQ_NO { get; set; }
        //public string SHRSCOM_NAME { get; set; }
        /// <summary>
        /// SUB-COMPONENT DESCRIPTION: This field indicates
        /// the sub-component description.
        /// </summary>
        public string SHRSCOM_DESCRIPTION { get; set; }
        //public string SHRSCOM_GSCH_NAME { get; set; }
        /// <summary>
        /// SUB-COMPONENT WEIGHT: This field indicates the
        /// weighting factor for composite calculations to
        /// the component level.
        /// </summary>
        public float? SHRSCOM_WEIGHT { get; set; }
        //public float SHRSCOM_TOTAL_SCORE { get; set; }
        //public string SHRSCOM_PASS_IND { get; set; }
        //public string SHRSCOM_USER_ID { get; set; }
        //public DateTime SHRSCOM_ACTIVITY_DATE { get; set; }
        //public DateTime SHRSCOM_DUE_DATE { get; set; }
        //public string SHRSCOM_MIN_PASS_SCORE { get; set; }
        //public string SHRSCOM_SUB_LATE_RULE { get; set; }
        //public string SHRSCOM_SUB_RESIT_RULE { get; set; }
        //public string SHRSCOM_SUB_SET { get; set; }
        //public string SHRSCOM_ANONYMOUS_IND { get; set; }
        //public int SHRSCOM_SURROGATE_ID { get; set; }
        //public int SHRSCOM_VERSION { get; set; }
        //public string SHRSCOM_DATA_ORIGIN { get; set; }
        //public string SHRSCOM_VPDI_CODE { get; set; }
    }
}
