﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("SMRPRLE")]
    public class SMRPRLE
    {
        [Key]
        [Column(Order = 1)]
        public string SMRPRLE_PROGRAM { get; set; }//código carerra
        public string SMRPRLE_PROGRAM_DESC { get; set; }//des carrera corta
        public string SMRPRLE_COLL_CODE { get; set; }//código facultad
    }
}
