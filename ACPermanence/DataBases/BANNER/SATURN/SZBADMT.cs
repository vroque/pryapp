﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Admission Type Validation Table
    /// </summary>
    [Table("SATURN.SZBADMT")]
    public class SZBADMT
    {
        /// <summary>
        /// This field identifies the admission type code
        /// referenced in the Admissions Module.
        /// VARCHAR2(2 CHAR)
        /// </summary>
        [Key]
        public string SZBADMT_CODE { get; set; }

        /// <summary>
        /// This field specifies the admission type (e.g. new
        /// admit, transfer, readmit, adult/continuing ed.,
        /// etc.) associated with the admission type code.
        /// VARCHAR2(30 CHAR)
        /// </summary>
        public string SZBADMT_DESC { get; set; }

        /// <summary>
        /// This field specifies the admission type (e.g. new
        /// admit, transfer, readmit,adult/continuing ed.,
        /// etc.) associated with the admission type code.
        /// VARCHAR2(300 CHAR)
        /// </summary>
        public string SZBADMT_DESCRIPTION { get; set; }

        /// <summary>
        /// This field identifies the most current date a
        /// record was created or updated.
        /// DATE
        /// </summary>
        public DateTime SZBADMT_ACTIVITY_DATE { get; set; }

        /// <summary>
        /// This field specifies whether the modality is validation
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SZBADMT_TRANSFER_IND { get; set; }
    }
}