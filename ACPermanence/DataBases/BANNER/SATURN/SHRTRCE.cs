﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Tabla de cursos convalidados en la UC
    /// </summary>
    [Table("SHRTRCE")]
    public class SHRTRCE
    {
        [Key]
        [Column(Order = 1)]
        public int SHRTRCE_PIDM { get; set; } //             NOT NULL NUMBER(8)         
        [Key]
        [Column(Order = 2)]
        public int SHRTRCE_TRIT_SEQ_NO { get; set; } //      NOT NULL NUMBER(2)         
        [Key]
        [Column(Order = 3)]
        public int SHRTRCE_TRAM_SEQ_NO { get; set; } //      NOT NULL NUMBER(2)         
        [Key]
        [Column(Order = 4)]
        public int SHRTRCE_SEQ_NO { get; set; } //           NOT NULL NUMBER(3)         
        [Key]
        [Column(Order = 5)]
        public int SHRTRCE_TRCR_SEQ_NO { get; set; } //               NUMBER(3)         
        public string SHRTRCE_TERM_CODE_EFF { get; set; } //    NOT NULL VARCHAR2(6 CHAR)  
        public string SHRTRCE_LEVL_CODE { get; set; } //        NOT NULL VARCHAR2(2 CHAR)  
        public string SHRTRCE_SUBJ_CODE { get; set; } //        NOT NULL VARCHAR2(4 CHAR)  
        public string SHRTRCE_CRSE_NUMB { get; set; } //        NOT NULL VARCHAR2(5 CHAR)  
        public string SHRTRCE_CRSE_TITLE { get; set; } //                VARCHAR2(30 CHAR) 
        public float SHRTRCE_CREDIT_HOURS { get; set; } //     NOT NULL NUMBER(7,3)       
        public string SHRTRCE_GRDE_CODE { get; set; } //        NOT NULL VARCHAR2(6 CHAR)  
        public string SHRTRCE_GMOD_CODE { get; set; } //        NOT NULL VARCHAR2(1 CHAR)  
        public string SHRTRCE_COUNT_IN_GPA_IND { get; set; } //          VARCHAR2(1 CHAR)  
        public DateTime SHRTRCE_ACTIVITY_DATE { get; set; } //    NOT NULL DATE              
        public string SHRTRCE_REPEAT_COURSE { get; set; } //             VARCHAR2(1 CHAR)  
        public string SHRTRCE_REPEAT_SYS { get; set; } //                VARCHAR2(1 CHAR)  
        public int SHRTRCE_SURROGATE_ID { get; set; } //     NOT NULL NUMBER(19)        
        public int SHRTRCE_VERSION { get; set; } //          NOT NULL NUMBER(19)        
        public string SHRTRCE_USER_ID { get; set; } //                   VARCHAR2(30 CHAR) 
        public string SHRTRCE_DATA_ORIGIN { get; set; } //               VARCHAR2(30 CHAR) 
        public string SHRTRCE_VPDI_CODE { get; set; } //                 VARCHAR2(6 CHAR) 
    }
}
