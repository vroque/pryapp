﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("SATURN.SIRASGN")]
    public class SIRASGN
    {
        /// <summary>
        /// The session indicator associated with the
        /// assignment
        /// VARCHAR2(2 CHAR) NOT NULL
        /// </summary>
        [Key]
        [Column(Order = 1)]
        public string SIRASGN_CATEGORY { get; set; }

        /// <summary>
        /// The course reference of the course that the
        /// instructor was assigned to
        /// VARCHAR2(5 CHAR) NOT NULL
        /// </summary>
        [Key]
        [Column(Order = 2)]
        public string SIRASGN_CRN { get; set; }

        /// <summary>
        /// The Pidm of the faculty member
        /// NUMBER(8,0) NOT NULL
        /// </summary>
        [Key]
        [Column(Order = 3)]
        public decimal SIRASGN_PIDM { get; set; }

        /// <summary>
        /// Term of the faculty member assignment
        /// VARCHAR2(6 CHAR) NOT NULL
        /// </summary>
        [Key]
        [Column(Order = 4)]
        public string SIRASGN_TERM_CODE { get; set; }

        /// <summary>
        /// Faculty members percentage of responsibility to
        /// the assignment
        /// NUMBER(3,0) NOT NULL
        /// </summary>
        public decimal SIRASGN_PERCENT_RESPONSE { get; set; }

        /// <summary>
        /// Faculty Adjustied Workload for instructional
        /// assignment
        /// NUMBER(9,3)
        /// </summary>
        public decimal? SIRASGN_WORKLOAD_ADJUST { get; set; }

        /// <summary>
        /// Faculty session percentage of responsibility of
        /// instructional assignment.
        /// NUMBER(3,0) NOT NULL
        /// </summary>
        public decimal SIRASGN_PERCENT_SESS { get; set; }

        /// <summary>
        /// The primary instructor of the course
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SIRASGN_PRIMARY_IND { get; set; }

        /// <summary>
        /// Override Indicator.
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SIRASGN_OVER_RIDE { get; set; }

        /// <summary>
        /// Faculty Position.
        /// NUMBER(8,0)
        /// </summary>
        public decimal? SIRASGN_POSITION { get; set; }

        /// <summary>
        /// Activity date
        /// DATE NOT NULL
        /// </summary>
        public DateTime SIRASGN_ACTIVITY_DATE { get; set; }

        /// <summary>
        /// The contract type that the instructional
        /// assignment is associated with
        /// VARCHAR2(2 CHAR)
        /// </summary>
        public string SIRASGN_FCNT_CODE { get; set; }

        /// <summary>
        /// This field is the Position number for the faculty
        /// assignment.  It is used to tie the faculty
        /// member's assignment to a position defined in the
        /// BANNER Human Resources system
        /// VARCHAR2(6 CHAR)
        /// </summary>
        public string SIRASGN_POSN { get; set; }

        /// <summary>
        /// This field is the Position number suffix.  It is
        /// used to tie the faculty member's assignment to a
        /// position defined in the BANNER Human Resources
        /// System
        /// VARCHAR2(2 CHAR)
        /// </summary>
        public string SIRASGN_SUFF { get; set; }

        /// <summary>
        /// Faculty Assignment Type Code
        /// VARCHAR2(4 CHAR)
        /// </summary>
        public string SIRASGN_ASTY_CODE { get; set; }

        /// <summary>
        /// DATA SOURCE: Source system that created or updated
        /// the row
        /// VARCHAR2(30 CHAR)
        /// </summary>
        public string SIRASGN_DATA_ORIGIN { get; set; }

        /// <summary>
        /// USER ID: User who inserted or last update the data
        /// VARCHAR2(30 CHAR)
        /// </summary>
        public string SIRASGN_USER_ID { get; set; }

        /// <summary>
        /// WORKLOAD INCREMENT: Incremental workload derived
        /// from enrollment count.
        /// NUMBER(9,3)
        /// </summary>
        public decimal? SIRASGN_WORKLOAD_INCR { get; set; }

        /// <summary>
        /// INCREMENT ENROLLMENT: Enrollment count used to
        /// calculate workload increment.
        /// NUMBER(4,0)
        /// </summary>
        public decimal? SIRASGN_INCR_ENRL { get; set; }

        /// <summary>
        /// INCREMENT ENROLLMENT DATE: Registration date used
        /// to derive workload increment from enrollment
        /// count.
        /// DATE
        /// </summary>
        public DateTime? SIRASGN_INCR_ENRL_DATE { get; set; }

        /// <summary>
        /// SURROGATE ID: Immutable unique key
        /// NUMBER(19,0) NOT NULL
        /// </summary>
        public decimal SIRASGN_SURROGATE_ID { get; set; }

        /// <summary>
        /// VERSION: Optimistic lock token.
        /// NUMBER(19,0) NOT NULL
        /// </summary>
        public decimal SIRASGN_VERSION { get; set; }

        /// <summary>
        /// VPDI CODE: Multi-entity processing code.
        /// VARCHAR2(6 CHAR)
        /// </summary>
        public string SIRASGN_VPDI_CODE { get; set; }
    }
}