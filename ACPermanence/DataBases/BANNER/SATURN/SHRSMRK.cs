﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("SHRSMRK")]
    public class SHRSMRK
    {
        [Key]
        [Column(Order = 1)]
        public string SHRSMRK_TERM_CODE { get; set; }
        [Key]
        [Column(Order = 2)]
        public string SHRSMRK_CRN { get; set; }
        [Key]
        [Column(Order = 3)]
        public decimal SHRSMRK_PIDM { get; set; }
        [Key]
        [Column(Order = 4)]
        public int? SHRSMRK_GCOM_ID { get; set; }
        [Key]
        [Column(Order = 5)]
        public int? SHRSMRK_SCOM_ID { get; set; }
        //public string SHRSMRK_GCHG_CODE { get; set; }
        //public string SHRSMRK_USER_ID { get; set; }
        //public DateTime SHRSMRK_ACTIVITY_DATE { get; set; }
        //public DateTime SHRSMRK_DUE_DATE { get; set; }
        //public DateTime SHRSMRK_MARK_CALC_DATE { get; set; }
        //public float SHRSMRK_SCORE { get; set; }
        //public float SHRSMRK_PERCENTAGE { get; set; }
        public string SHRSMRK_GRDE_CODE { get; set; }
        public DateTime? SHRSMRK_ACTIVITY_DATE { get; set; }
        //public DateTime SHRSMRK_RETURNED_DATE { get; set; }
        //public DateTime SHRSMRK_EXTENSION_DATE { get; set; }
        //public int SHRSMRK_MARKER { get; set; }
        //public string SHRSMRK_COMMENTS { get; set; }
        public string SHRSMRK_SUMMARY { get; set; }
        //public int SHRSMRK_SURROGATE_ID { get; set; }
        //public int SHRSMRK_VERSION { get; set; }
        //public string SHRSMRK_DATA_ORIGIN { get; set; }
        //public string SHRSMRK_VPDI_CODE { get; set; }
    }
}
