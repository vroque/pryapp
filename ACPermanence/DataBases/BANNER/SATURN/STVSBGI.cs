﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// La instución de transferencia
    /// </summary>
    [Table("STVSBGI")]
    public class STVSBGI
    {
        [Key]
        public string STVSBGI_CODE { get; set; } //          NOT NULL VARCHAR2(6 CHAR)  
        public string STVSBGI_TYPE_IND { get; set; } //      NOT NULL VARCHAR2(1 CHAR)  
        public string STVSBGI_SRCE_IND { get; set; } //               VARCHAR2(1 CHAR)  
        public string STVSBGI_DESC { get; set; } //                   VARCHAR2(30 CHAR) 
        public DateTime STVSBGI_ACTIVITY_DATE { get; set; } // NOT NULL DATE              
        public string STVSBGI_ADMR_CODE { get; set; } //              VARCHAR2(4 CHAR)  
        public string STVSBGI_EDI_CAPABLE { get; set; } //            VARCHAR2(1 CHAR)  
        public string STVSBGI_FICE { get; set; } //                   VARCHAR2(6 CHAR)  
        public int? STVSBGI_VR_MSG_NO { get; set; } //              NUMBER(6)         
        // public string STVSBGI_DISP_WEB_IND { get; set; } //           RAW(1 BYTE)       
        public int STVSBGI_SURROGATE_ID { get; set; } //  NOT NULL NUMBER(19)        
        public int STVSBGI_VERSION { get; set; } //       NOT NULL NUMBER(19)        
        public string STVSBGI_USER_ID { get; set; } //                VARCHAR2(30 CHAR) 
        public string STVSBGI_DATA_ORIGIN { get; set; } //            VARCHAR2(30 CHAR) 
        public string STVSBGI_VPDI_CODE { get; set; } //              VARCHAR2(6 CHAR) 
    }
}
