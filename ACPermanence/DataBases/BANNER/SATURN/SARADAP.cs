﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Admissions Application Repeating Table
    /// </summary>
    [Table("SARADAP")]
    public class SARADAP
    {
        /// <summary>
        /// This field identifies the applicant internal
        /// identification number.
        /// </summary>
        [Key]
        [Column(Order = 1)]
        public decimal SARADAP_PIDM { get;set;}
        /// <summary>
        /// This field identifies the term associated with the
        /// information on applicant record.
        /// </summary>
        [Key]
        [Column(Order = 2)]
        public string SARADAP_TERM_CODE_ENTRY { get; set; }
        /// <summary>
        /// This field identifies the level to which the
        /// applicant is applying.
        /// </summary>
        [Key]
        [Column(Order = 3)]
        public string SARADAP_LEVL_CODE { get; set; }
        /// <summary>
        /// This field identifies the campus to which
        /// applicant is applying.
        /// </summary>
        [Key]
        [Column(Order = 4)]
        public string SARADAP_CAMP_CODE { get; set; }
        /// <summary>
        /// This field identifies the primary major to which
        /// applicant is applying.
        /// </summary>
        [Key]
        [Column(Order = 5)]
        public string SARADAP_MAJR_CODE_1 { get; set; }
        /// <summary>
        /// This field identifies the admission type of the
        /// applicant.
        /// </summary>
        public string SARADAP_ADMT_CODE { get; set; }
        //SARADAP_PIDM NOT NULL NUMBER(8)
        //SARADAP_TERM_CODE_ENTRY NOT NULL VARCHAR2(6 CHAR)
        //SARADAP_APPL_NO NOT NULL NUMBER(2)
        //SARADAP_LEVL_CODE VARCHAR2(2 CHAR)
        //SARADAP_APPL_DATE NOT NULL DATE
        //SARADAP_APST_CODE NOT NULL VARCHAR2(1 CHAR)
        //SARADAP_APST_DATE NOT NULL DATE
        //SARADAP_MAINT_IND NOT NULL VARCHAR2(1 CHAR)
        //SARADAP_ADMT_CODE VARCHAR2(2 CHAR)
        //SARADAP_STYP_CODE NOT NULL VARCHAR2(1 CHAR)
        //SARADAP_CAMP_CODE VARCHAR2(3 CHAR)
        //SARADAP_SITE_CODE VARCHAR2(3 CHAR)
        //SARADAP_COLL_CODE_1 VARCHAR2(2 CHAR)
        //SARADAP_DEGC_CODE_1 VARCHAR2(6 CHAR)
        //SARADAP_MAJR_CODE_1 VARCHAR2(4 CHAR)
        //SARADAP_COLL_CODE_2 VARCHAR2(2 CHAR)
        //SARADAP_DEGC_CODE_2 VARCHAR2(6 CHAR)
        //SARADAP_MAJR_CODE_2 VARCHAR2(4 CHAR)
        //SARADAP_RESD_CODE NOT NULL VARCHAR2(1 CHAR)
        //SARADAP_FULL_PART_IND VARCHAR2(1 CHAR)
        //SARADAP_SESS_CODE VARCHAR2(1 CHAR)
        //SARADAP_WRSN_CODE VARCHAR2(2 CHAR)
        //SARADAP_INTV_CODE VARCHAR2(1 CHAR)
        //SARADAP_FEE_IND VARCHAR2(1 CHAR)
        //SARADAP_FEE_DATE DATE
        //SARADAP_ACTIVITY_DATE NOT NULL DATE
        //SARADAP_RATE_CODE VARCHAR2(5 CHAR)
        //SARADAP_EGOL_CODE VARCHAR2(2 CHAR)
        //SARADAP_EDLV_CODE VARCHAR2(3 CHAR)
        //SARADAP_MAJR_CODE_CONC_1 VARCHAR2(4 CHAR)
        //SARADAP_DEPT_CODE VARCHAR2(4 CHAR)
        //SARADAP_SBGI_CODE VARCHAR2(6 CHAR)
        //SARADAP_RECR_CODE VARCHAR2(3 CHAR)
        //SARADAP_RTYP_CODE VARCHAR2(2 CHAR)
        //SARADAP_DEPT_CODE_2 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_2 VARCHAR2(4 CHAR)
        //SARADAP_PROGRAM_1 VARCHAR2(12 CHAR)
        //SARADAP_TERM_CODE_CTLG_1 VARCHAR2(6 CHAR)
        //SARADAP_MAJR_CODE_1_2 VARCHAR2(4 CHAR)
        //SARADAP_DEPT_CODE_1_2 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_1_2 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_1_3 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_121 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_122 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_123 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_MINR_1_1 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_MINR_1_2 VARCHAR2(4 CHAR)
        //SARADAP_PROGRAM_2 VARCHAR2(12 CHAR)
        //SARADAP_TERM_CODE_CTLG_2 VARCHAR2(6 CHAR)
        //SARADAP_LEVL_CODE_2 VARCHAR2(2 CHAR)
        //SARADAP_CAMP_CODE_2 VARCHAR2(3 CHAR)
        //SARADAP_MAJR_CODE_2_2 VARCHAR2(4 CHAR)
        //SARADAP_DEPT_CODE_2_2 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_211 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_212 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_213 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_221 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_222 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_CONC_223 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_MINR_2_1 VARCHAR2(4 CHAR)
        //SARADAP_MAJR_CODE_MINR_2_2 VARCHAR2(4 CHAR)
        //SARADAP_CURR_RULE_1 NUMBER(8)
        //SARADAP_CMJR_RULE_1_1 NUMBER(8)
        //SARADAP_CCON_RULE_11_1 NUMBER(8)
        //SARADAP_CCON_RULE_11_2 NUMBER(8)
        //SARADAP_CCON_RULE_11_3 NUMBER(8)
        //SARADAP_CMJR_RULE_1_2 NUMBER(8)
        //SARADAP_CCON_RULE_12_1 NUMBER(8)
        //SARADAP_CCON_RULE_12_2 NUMBER(8)
        //SARADAP_CCON_RULE_12_3 NUMBER(8)
        //SARADAP_CMNR_RULE_1_1 NUMBER(8)
        //SARADAP_CMNR_RULE_1_2 NUMBER(8)
        //SARADAP_CURR_RULE_2 NUMBER(8)
        //SARADAP_CMJR_RULE_2_1 NUMBER(8)
        //SARADAP_CCON_RULE_21_1 NUMBER(8)
        //SARADAP_CCON_RULE_21_2 NUMBER(8)
        //SARADAP_CCON_RULE_21_3 NUMBER(8)
        //SARADAP_CMJR_RULE_2_2 NUMBER(8)
        //SARADAP_CCON_RULE_22_1 NUMBER(8)
        //SARADAP_CCON_RULE_22_2 NUMBER(8)
        //SARADAP_CCON_RULE_22_3 NUMBER(8)
        //SARADAP_CMNR_RULE_2_1 NUMBER(8)
        //SARADAP_CMNR_RULE_2_2 NUMBER(8)
        //SARADAP_WEB_ACCT_MISC_IND VARCHAR2(1 CHAR)
        //SARADAP_WEB_CASHIER_USER VARCHAR2(30 CHAR)
        //SARADAP_WEB_TRANS_NO NUMBER(8)
        //SARADAP_WEB_AMOUNT NUMBER(12,2)
        //SARADAP_WEB_RECEIPT_NUMBER NUMBER(8)
        //SARADAP_WAIV_CODE VARCHAR2(4 CHAR)
        //SARADAP_DATA_ORIGIN VARCHAR2(30 CHAR)
        //SARADAP_USER_ID VARCHAR2(30 CHAR)
        //SARADAP_APPL_PREFERENCE NUMBER(2)
        //SARADAP_SURROGATE_ID NOT NULL NUMBER(19)
        //SARADAP_VERSION NOT NULL NUMBER(19)
        //SARADAP_VPDI_CODE VARCHAR2(6 CHAR)
    }
}
