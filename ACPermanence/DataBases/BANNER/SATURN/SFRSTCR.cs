﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Cursos Matriculados
    /// Student Course Registration Repeating Table
    /// </summary>
    [Table("SFRSTCR")]
    public class SFRSTCR
    {
        /// <summary>
        /// This field identifies the registration term code
        /// </summary>
        [Key]
        [Column(Order = 1)]
        public string SFRSTCR_TERM_CODE { get; set; }
        /// <summary>
        /// This field identifies the course reference number
        ///associated with the class section.
        /// </summary>
        [Key]
        [Column(Order = 2)]
        public string SFRSTCR_CRN { get; set; }
        /// <summary>
        /// Internal Identification Number of the person
        /// registered.
        /// </summary>
        [Key]
        [Column(Order = 3)]
        public decimal SFRSTCR_PIDM { get; set; }
        /// <summary>
        /// This field identifies the part-of-term code
        /// associated with this CRN.
        /// </summary>
        public string SFRSTCR_PTRM_CODE { get; set; }
        /// <summary>
        /// This field identifies the course registration
        /// status associated with this CRN.
        /// </summary>
        public string SFRSTCR_RSTS_CODE { get; set; }
        /// <summary>
        /// This field identifies the grade code associated
        /// with this CRN.
        /// </summary>
        public string SFRSTCR_GRDE_CODE { get; set; }
        /// <summary>
        /// This field identifies the add date of the
        /// registrant in this CRN.
        /// </summary>
        public DateTime SFRSTCR_ADD_DATE { get; set; }

        public decimal SFRSTCR_CREDIT_HR { get; set; }
    }
}
