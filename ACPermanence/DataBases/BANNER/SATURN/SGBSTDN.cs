﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Student Base Table 
    /// </summary>
    [Table("SATURN.SGBSTDN")]
    public class SGBSTDN
    {
        /// <summary>
        /// This field identifies the internal identification
        /// number of student.
        /// </summary>
        [Key]
        [Column(Order=1)]
        public decimal SGBSTDN_PIDM { get; set; } //                 NOT NULL NUMBER(8)         
        /// <summary>
        /// This field identifies the level of the student for
        /// the effective term.
        /// </summary>
        public string SGBSTDN_LEVL_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_CAMP_CODE { get; set; } //                     VARCHAR2(3 CHAR)  
        public string SGBSTDN_DEPT_CODE { get; set; } //                     VARCHAR2(4 CHAR)  
        public string SGBSTDN_PROGRAM_1 { get; set; } //                     VARCHAR2(12 CHAR) 
        public string SGBSTDN_TERM_CODE_ADMIT { get; set; } //               VARCHAR2(6 CHAR)  
        public string SGBSTDN_ADMT_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        [Key]
        [Column(Order = 2)]
        public string SGBSTDN_TERM_CODE_EFF { get; set; } //        NOT NULL VARCHAR2(6 CHAR)  
        [Key]
        [Column(Order = 3)]
        public int SGBSTDN_SURROGATE_ID { get; set; } //         NOT NULL NUMBER(19)        
        
        
        public string SGBSTDN_STST_CODE { get; set; } //            NOT NULL VARCHAR2(2 CHAR)  

        public string SGBSTDN_STYP_CODE { get; set; } //            NOT NULL VARCHAR2(1 CHAR)  
        /*public string SGBSTDN_TERM_CODE_MATRIC { get; set; } //              VARCHAR2(6 CHAR)  
        
        public string SGBSTDN_EXP_GRAD_DATE { get; set; } //                 DATE              
        
        public string SGBSTDN_FULL_PART_IND { get; set; } //                 VARCHAR2(1 CHAR)  
        public string SGBSTDN_SESS_CODE { get; set; } //                     VARCHAR2(1 CHAR)  
        public string SGBSTDN_RESD_CODE { get; set; } //                     VARCHAR2(1 CHAR)  */
        public string SGBSTDN_COLL_CODE_1 { get; set; } //                   VARCHAR2(2 CHAR)  
        public string SGBSTDN_DEGC_CODE_1 { get; set; } //                   VARCHAR2(6 CHAR)  
        /*public string SGBSTDN_MAJR_CODE_1 { get; set; } //                   VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_MINR_1 { get; set; } //              VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_MINR_1_2 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_1 { get; set; } //              VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_1_2 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_1_3 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_COLL_CODE_2 { get; set; } //                   VARCHAR2(2 CHAR)  
        public string SGBSTDN_DEGC_CODE_2 { get; set; } //                   VARCHAR2(6 CHAR)  
        public string SGBSTDN_MAJR_CODE_2 { get; set; } //                   VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_MINR_2 { get; set; } //              VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_MINR_2_2 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_2 { get; set; } //              VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_2_2 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_2_3 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_ORSN_CODE { get; set; } //                     VARCHAR2(1 CHAR)  
        public string SGBSTDN_PRAC_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_ADVR_PIDM { get; set; } //                     NUMBER(8)         
        public string SGBSTDN_GRAD_CREDIT_APPR_IND { get; set; } //          VARCHAR2(1 CHAR)  
        public string SGBSTDN_CAPL_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_LEAV_CODE { get; set; } //                     VARCHAR2(1 CHAR)  
        public string SGBSTDN_LEAV_FROM_DATE { get; set; } //                DATE              
        public string SGBSTDN_LEAV_TO_DATE { get; set; } //                  DATE              
        public string SGBSTDN_ASTD_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_TERM_CODE_ASTD { get; set; } //                VARCHAR2(6 CHAR)  
        public string SGBSTDN_RATE_CODE { get; set; } //                     VARCHAR2(5 CHAR)  
        public string SGBSTDN_ACTIVITY_DATE { get; set; } //        NOT NULL DATE              
        public string SGBSTDN_MAJR_CODE_1_2 { get; set; } //                 VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_2_2 { get; set; } //                 VARCHAR2(4 CHAR)  
        public string SGBSTDN_EDLV_CODE { get; set; } //                     VARCHAR2(3 CHAR)  
        public string SGBSTDN_INCM_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        
        public string SGBSTDN_EMEX_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_APRN_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_TRCN_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_GAIN_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_VOED_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_BLCK_CODE { get; set; } //                     VARCHAR2(10 CHAR) 
        public string SGBSTDN_TERM_CODE_GRAD { get; set; } //                VARCHAR2(6 CHAR)  
        public string SGBSTDN_ACYR_CODE { get; set; } //                     VARCHAR2(4 CHAR)  
        
        public string SGBSTDN_SITE_CODE { get; set; } //                     VARCHAR2(3 CHAR)  
        public string SGBSTDN_DEPT_CODE_2 { get; set; } //                   VARCHAR2(4 CHAR)  
        public string SGBSTDN_EGOL_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_DEGC_CODE_DUAL { get; set; } //                VARCHAR2(6 CHAR)  
        public string SGBSTDN_LEVL_CODE_DUAL { get; set; } //                VARCHAR2(2 CHAR)  
        public string SGBSTDN_DEPT_CODE_DUAL { get; set; } //                VARCHAR2(4 CHAR)  
        public string SGBSTDN_COLL_CODE_DUAL { get; set; } //                VARCHAR2(2 CHAR)  
        public string SGBSTDN_MAJR_CODE_DUAL { get; set; } //                VARCHAR2(4 CHAR)  
        public string SGBSTDN_BSKL_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_PRIM_ROLL_IND { get; set; } //                 VARCHAR2(1 CHAR)  
        
        public string SGBSTDN_TERM_CODE_CTLG_1 { get; set; } //              VARCHAR2(6 CHAR)  
        public string SGBSTDN_DEPT_CODE_1_2 { get; set; } //                 VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_121 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_122 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_123 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_SECD_ROLL_IND { get; set; } //                 VARCHAR2(1 CHAR)  
        public string SGBSTDN_TERM_CODE_ADMIT_2 { get; set; } //             VARCHAR2(6 CHAR)  
        public string SGBSTDN_ADMT_CODE_2 { get; set; } //                   VARCHAR2(2 CHAR)  
        public string SGBSTDN_PROGRAM_2 { get; set; } //                     VARCHAR2(12 CHAR) 
        public string SGBSTDN_TERM_CODE_CTLG_2 { get; set; } //              VARCHAR2(6 CHAR)  
        public string SGBSTDN_LEVL_CODE_2 { get; set; } //                   VARCHAR2(2 CHAR)  
        public string SGBSTDN_CAMP_CODE_2 { get; set; } //                   VARCHAR2(3 CHAR)  
        public string SGBSTDN_DEPT_CODE_2_2 { get; set; } //                 VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_221 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_222 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_MAJR_CODE_CONC_223 { get; set; } //            VARCHAR2(4 CHAR)  
        public string SGBSTDN_CURR_RULE_1 { get; set; } //                   NUMBER(8)         
        public string SGBSTDN_CMJR_RULE_1_1 { get; set; } //                 NUMBER(8)         
        public string SGBSTDN_CCON_RULE_11_1 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CCON_RULE_11_2 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CCON_RULE_11_3 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CMJR_RULE_1_2 { get; set; } //                 NUMBER(8)         
        public string SGBSTDN_CCON_RULE_12_1 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CCON_RULE_12_2 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CCON_RULE_12_3 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CMNR_RULE_1_1 { get; set; } //                 NUMBER(8)         
        public string SGBSTDN_CMNR_RULE_1_2 { get; set; } //                 NUMBER(8)         
        public string SGBSTDN_CURR_RULE_2 { get; set; } //                   NUMBER(8)         
        public string SGBSTDN_CMJR_RULE_2_1 { get; set; } //                 NUMBER(8)         
        public string SGBSTDN_CCON_RULE_21_1 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CCON_RULE_21_2 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CCON_RULE_21_3 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CMJR_RULE_2_2 { get; set; } //                 NUMBER(8)         
        public string SGBSTDN_CCON_RULE_22_1 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CCON_RULE_22_2 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CCON_RULE_22_3 { get; set; } //                NUMBER(8)         
        public string SGBSTDN_CMNR_RULE_2_1 { get; set; } //                 NUMBER(8)         
        public string SGBSTDN_CMNR_RULE_2_2 { get; set; } //                 NUMBER(8)         
        public string SGBSTDN_PREV_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_TERM_CODE_PREV { get; set; } //                VARCHAR2(6 CHAR)  
        public string SGBSTDN_CAST_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SGBSTDN_TERM_CODE_CAST { get; set; } //                VARCHAR2(6 CHAR)  
        public string SGBSTDN_DATA_ORIGIN { get; set; } //                   VARCHAR2(30 CHAR) 
        public string SGBSTDN_USER_ID { get; set; } //                       VARCHAR2(30 CHAR) 
        public string SGBSTDN_SCPC_CODE { get; set; } //                     VARCHAR2(6 CHAR)  
        
        public string SGBSTDN_VERSION { get; set; } //              NOT NULL NUMBER(19)        
        public string SGBSTDN_VPDI_CODE { get; set; } //                     VARCHAR2(6 CHAR) 
        */
    }
}
