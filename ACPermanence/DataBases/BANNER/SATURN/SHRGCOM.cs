﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    ///  Detalles de los componentes de notas
    /// </summary>
    [Table("SHRGCOM")]
    public class SHRGCOM
    {
        /// <summary>
        /// Term associated with Gradable Component
        /// Grade Component Rule .
        /// </summary>
        [Key]
        [Column(Order = 1)]
        public string SHRGCOM_TERM_CODE { get; set; }
        /// <summary>
        /// CRN associated with Gradable Component
        /// </summary>
        [Key]
        [Column(Order = 2)]
        public string SHRGCOM_CRN { get; set; }
        /// <summary>
        /// Gradable Component Identifier
        /// </summary>
        [Key]
        [Column(Order = 3)]
        public int? SHRGCOM_ID { get; set; }
        /// <summary>
        /// Weighting factor
        /// </summary>
        public float? SHRGCOM_WEIGHT { get; set; }
        /// <summary>
        /// Gradable Component Name
        /// </summary>
        public string SHRGCOM_NAME { get; set; }
        /// <summary>
        /// Gradable Component Description
        /// </summary>
        public string SHRGCOM_DESCRIPTION { get; set; }
    }
}
