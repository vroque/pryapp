﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Area Subject/Course Attachment Rule Repeating Table 
    /// </summary>
    [Table("SATURN.SMRARUL")]
    public class SMRARUL
    {
        /// <summary>
        /// A rule is a way to segregate detail requirements
        /// which need additional controls over and above the
        /// controls already specified for the area detail
        /// requirements.
        /// </summary>
        [Key]
        [Column(Order = 1)]
        public string SMRARUL_KEY_RULE { get; set; } //             NOT NULL VARCHAR2(10 CHAR) 
        /// <summary>
        /// Area attachment of the rule.
        /// </summary>
        [Key]
        [Column(Order = 2)]
        public string SMRARUL_AREA { get; set; } //                 NOT NULL VARCHAR2(10 CHAR) 
        /// <summary>
        /// Effective term of the area course/attribute
        /// attachments.
        /// </summary>
        public string SMRARUL_TERM_CODE_EFF { get; set; } //        NOT NULL VARCHAR2(6 CHAR)  
        public int SMRARUL_SEQNO { get; set; } //                NOT NULL NUMBER(8)         
        public string SMRARUL_SPLIT_COURSE_IND { get; set; } //     NOT NULL VARCHAR2(1 CHAR)  
        public DateTime SMRARUL_ACTIVITY_DATE { get; set; } //        NOT NULL DATE              
        public string SMRARUL_TRANSFER_IND { get; set; } //         NOT NULL VARCHAR2(1 CHAR)  
        public string SMRARUL_CONNECTOR_REQ { get; set; } //        NOT NULL VARCHAR2(1 CHAR)  
        public string SMRARUL_CONNECTOR_MAX { get; set; } //        NOT NULL VARCHAR2(1 CHAR)  
        public string SMRARUL_CONNECTOR_TRANSFER { get; set; } //   NOT NULL VARCHAR2(1 CHAR)  
        public string SMRARUL_CNT_IN_GPA_IND { get; set; } //       NOT NULL VARCHAR2(1 CHAR)  
        public string SMRARUL_SET { get; set; } //                           VARCHAR2(3 CHAR)  
        public int? SMRARUL_SUBSET { get; set; } //                        NUMBER(3)         
        public string SMRARUL_RULE { get; set; } //                          VARCHAR2(10 CHAR) 
        /// <summary>
        /// Subject code.
        /// </summary>
        [Key]
        [Column(Order = 3)]
        public string SMRARUL_SUBJ_CODE { get; set; } //                     VARCHAR2(4 CHAR)  
        /// <summary>
        /// Low course number
        /// </summary>
        [Key]
        [Column(Order = 4)]
        public string SMRARUL_CRSE_NUMB_LOW { get; set; } //                 VARCHAR2(5 CHAR)  
        /// <summary>
        /// High course number.
        /// </summary>
        public string SMRARUL_CRSE_NUMB_HIGH { get; set; } //                VARCHAR2(5 CHAR)  
        public string SMRARUL_ATTR_CODE { get; set; } //                     VARCHAR2(4 CHAR)  
        public string SMRARUL_ATTS_CODE { get; set; } //                     VARCHAR2(4 CHAR)  
        public string SMRARUL_CAMP_CODE { get; set; } //                     VARCHAR2(3 CHAR)  
        public string SMRARUL_COLL_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SMRARUL_DEPT_CODE { get; set; } //                     VARCHAR2(4 CHAR)  
        public int? SMRARUL_YEAR_RULE { get; set; } //                     NUMBER(2)         
        public string SMRARUL_TERM_CODE_FROM { get; set; } //                VARCHAR2(6 CHAR)  
        public string SMRARUL_TERM_CODE_TO { get; set; } //                  VARCHAR2(6 CHAR)  
        public float? SMRARUL_REQ_CREDITS { get; set; } //                   NUMBER(11,3)      
        public int? SMRARUL_REQ_COURSES { get; set; } //                   NUMBER(3)         
        public float? SMRARUL_MAX_CREDITS { get; set; } //                   NUMBER(11,3)      
        public int? SMRARUL_MAX_COURSES { get; set; } //                   NUMBER(3)         
        public float? SMRARUL_MIN_CRED_CRSE { get; set; } //                 NUMBER(7,3)       
        public float? SMRARUL_MAX_CRED_CRSE { get; set; } //                 NUMBER(7,3)       
        public float? SMRARUL_COMPL_CREDITS { get; set; } //                 NUMBER(11,3)      
        public int? SMRARUL_COMPL_COURSES { get; set; } //                 NUMBER(3)         
        public string SMRARUL_GRDE_CODE_MIN { get; set; } //                 VARCHAR2(6 CHAR)  
        public float? SMRARUL_MAX_CREDITS_TRANSFER { get; set; } //          NUMBER(11,3)      
        public int? SMRARUL_MAX_COURSES_TRANSFER { get; set; } //          NUMBER(3)         
        public string SMRARUL_CATALOG_IND { get; set; } //          NOT NULL VARCHAR2(1 CHAR)  
        public string SMRARUL_TESC_CODE { get; set; } //                     VARCHAR2(6 CHAR)  
        public string SMRARUL_MIN_VALUE { get; set; } //                     VARCHAR2(15 CHAR) 
        public string SMRARUL_MAX_VALUE { get; set; } //                     VARCHAR2(15 CHAR) 
        public string SMRARUL_CONCURRENCY_IND { get; set; } //      NOT NULL VARCHAR2(1 CHAR)  
        public int SMRARUL_SURROGATE_ID { get; set; } //         NOT NULL NUMBER(19)        
        public int SMRARUL_VERSION { get; set; } //              NOT NULL NUMBER(19)        
        public string SMRARUL_USER_ID { get; set; } //                       VARCHAR2(30 CHAR) 
        public string SMRARUL_DATA_ORIGIN { get; set; } //                   VARCHAR2(30 CHAR) 
        public string SMRARUL_VPDI_CODE { get; set; } //                     VARCHAR2(6 CHAR)  
    }
}
