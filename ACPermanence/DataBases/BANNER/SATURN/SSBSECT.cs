﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// TABLA DE CONFUGRACION DE NRCS
    /// </summary>
    [Table("SSBSECT")]
    public class SSBSECT
    {
        [Key]
        [Column(Order = 1)]
        public string SSBSECT_TERM_CODE { get; set; }

        [Key]
        [Column(Order = 2)]
        public string SSBSECT_CRN { get; set; }

        [Key]
        [Column(Order = 3)]
        public string SSBSECT_SUBJ_CODE { get; set; }

        [Key]
        [Column(Order = 4)]
        public string SSBSECT_CRSE_NUMB { get; set; }
        public string SSBSECT_CAMP_CODE { get; set; }

        public string SSBSECT_PTRM_CODE { get; set; }
        public decimal SSBSECT_CREDIT_HRS { get; set; }

        public string SSBSECT_LINK_IDENT { get; set; }

        /// <summary>
        /// Horas lectivas (teóricas)
        /// </summary>
        public decimal? SSBSECT_LEC_HR { get; set; }

        /// <summary>
        /// Horas laboratorio (prácticas)
        /// </summary>
        public decimal? SSBSECT_LAB_HR { get; set; }

        /// <summary>
        /// This field is defined in the Start field in the
        /// Base Part of Term Block on the Term Control Form
        /// - SOATERM, and will default to this field based
        /// on the Key Block Term and Part/Term field in the
        /// Section Block of this form.
        /// </summary>
        public DateTime? SSBSECT_PTRM_START_DATE { get; set; }

        /// <summary>
        /// This field is defined in the End field in the Base
        /// Part of Term Block on the Term Control Form -
        /// SOATERM, and will default to this field based on
        /// the Key Block Term and Part/Term field in the
        /// Section Block of this form.
        /// </summary>
        public DateTime? SSBSECT_PTRM_END_DATE { get; set; }
    }
}