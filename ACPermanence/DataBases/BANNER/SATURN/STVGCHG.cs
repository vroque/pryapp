﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("STVGCHG")]
    public class STVGCHG
    {
        [Key]
        public string STVGCHG_CODE { get; set; } //           NOT NULL VARCHAR2(2 CHAR)  
        public string STVGCHG_DESC { get; set; } //                    VARCHAR2(30 CHAR) 
        public DateTime STVGCHG_ACTIVITY_DATE { get; set; } //  NOT NULL DATE              
        public string STVGCHG_PERMIT_DUP_IND { get; set; } // NOT NULL VARCHAR2(1 CHAR)  
        public string STVGCHG_SYSTEM_REQ_IND { get; set; } // NOT NULL VARCHAR2(1 CHAR)  
        public string STVGCHG_EXEMPT_IND { get; set; } //     NOT NULL VARCHAR2(1 CHAR)  
        public string STVGCHG_RESIT_IND { get; set; } //      NOT NULL VARCHAR2(1 CHAR)  
        public string STVGCHG_ACADHIST_IND { get; set; } //   NOT NULL VARCHAR2(1 CHAR)  
        public string STVGCHG_DEFINITIVE_IND { get; set; } // NOT NULL VARCHAR2(1 CHAR)  
        public string STVGCHG_CALCULATED_IND { get; set; } // NOT NULL VARCHAR2(1 CHAR)  
        public string STVGCHG_EGB_IND { get; set; } //        NOT NULL VARCHAR2(1 CHAR)  
        public string STVGCHG_REAS_GRDE_IND { get; set; } //  NOT NULL VARCHAR2(1 CHAR)  
        public string STVGCHG_GCAT_CODE { get; set; } //               VARCHAR2(2 CHAR)  
        public int STVGCHG_SURROGATE_ID { get; set; } //   NOT NULL NUMBER(19)        
        public int STVGCHG_VERSION { get; set; } //        NOT NULL NUMBER(19)        
        public string STVGCHG_USER_ID { get; set; } //                 VARCHAR2(30 CHAR) 
        public string STVGCHG_DATA_ORIGIN { get; set; } //             VARCHAR2(30 CHAR) 
        public string STVGCHG_VPDI_CODE { get; set; } //               VARCHAR2(6 CHAR) 
    }
}
