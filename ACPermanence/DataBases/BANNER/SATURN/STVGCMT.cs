﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("STVGCMT")]
    public class STVGCMT
    {
        [Key]
        public string STVGCMT_CODE { get; set; } //                NOT NULL VARCHAR2(7 CHAR)   
        public string STVGCMT_DESCRIPTION { get; set; } //         NOT NULL VARCHAR2(200 CHAR) 
        public string STVGCMT_STUDENT_DISPLAY_IND { get; set; } // NOT NULL VARCHAR2(1 CHAR)   
        public string STVGCMT_USER_ID { get; set; } //             NOT NULL VARCHAR2(30 CHAR)  
        public DateTime STVGCMT_ACTIVITY_DATE { get; set; } //       NOT NULL DATE               
        public int STVGCMT_SURROGATE_ID { get; set; } //        NOT NULL NUMBER(19)         
        public int STVGCMT_VERSION { get; set; } //             NOT NULL NUMBER(19)         
        public string STVGCMT_DATA_ORIGIN { get; set; } //                  VARCHAR2(30 CHAR)  
        public string STVGCMT_VPDI_CODE { get; set; } //                    VARCHAR2(6 CHAR)   
    }
}
