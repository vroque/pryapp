﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("SATURN.SMRALIB")]
    public class SMRALIB
    {
        [Key]
        [Column(Order = 1)]
        public string SMRALIB_AREA { get; set; } //             NOT NULL VARCHAR2(10 CHAR) 
        public string SMRALIB_AREA_DESC { get; set; } //        NOT NULL VARCHAR2(30 CHAR) 
        public string SMRALIB_LEVL_CODE_STU { get; set; } //    NOT NULL VARCHAR2(2 CHAR)  
        public string SMRALIB_LEVL_CODE { get; set; } //        NOT NULL VARCHAR2(2 CHAR)  
        public string SMRALIB_DYNAMIC_IND { get; set; } //      NOT NULL VARCHAR2(1 CHAR)  
        public string SMRALIB_PRINT_IND { get; set; } //        NOT NULL VARCHAR2(1 CHAR)  
        public DateTime SMRALIB_ACTIVITY_DATE { get; set; } //    NOT NULL DATE              
        public string SMRALIB_COMPL_USAGE_IND { get; set; } //  NOT NULL VARCHAR2(1 CHAR)  
        public string SMRALIB_PREREQ_USAGE_IND { get; set; } // NOT NULL VARCHAR2(1 CHAR)  
        // public string SMRALIB_PRESCR_USAGE_IND { get; set; } //          RAW(1 BYTE)       
        public int SMRALIB_SURROGATE_ID { get; set; } //     NOT NULL NUMBER(19)        
        public int SMRALIB_VERSION { get; set; } //          NOT NULL NUMBER(19)        
        public string SMRALIB_USER_ID { get; set; } //                   VARCHAR2(30 CHAR) 
        public string SMRALIB_DATA_ORIGIN { get; set; } //               VARCHAR2(30 CHAR) 
        public string SMRALIB_VPDI_CODE { get; set; } //                 VARCHAR2(6 CHAR)  
        public string SMRALIB_ATTS_CODE { get; set; } //                 VARCHAR2(4 CHAR)  
    }
}
