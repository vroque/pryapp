﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("SHRTCKN")]
    public class SHRTCKN
    {
        [Key]
        [Column(Order =1)]
        public int SHRTCKN_PIDM { get; set; } //                 NOT NULL NUMBER(8)          
        [Key]
        [Column(Order = 3)]
        public string SHRTCKN_TERM_CODE { get; set; } //            NOT NULL VARCHAR2(6 CHAR)   
        [Key]
        [Column(Order = 4)]
        public int SHRTCKN_SEQ_NO { get; set; } //               NOT NULL NUMBER(2)          
        public string SHRTCKN_CRN { get; set; } //                  NOT NULL VARCHAR2(5 CHAR)   
        public string SHRTCKN_SUBJ_CODE { get; set; } //            NOT NULL VARCHAR2(4 CHAR)   
        public string SHRTCKN_CRSE_NUMB { get; set; } //            NOT NULL VARCHAR2(5 CHAR)   
        public string SHRTCKN_COLL_CODE { get; set; } //                     VARCHAR2(2 CHAR)   
        public string SHRTCKN_CAMP_CODE { get; set; } //                     VARCHAR2(3 CHAR)   
        public string SHRTCKN_DEPT_CODE { get; set; } //                     VARCHAR2(4 CHAR)   
        public string SHRTCKN_DIVS_CODE { get; set; } //                     VARCHAR2(4 CHAR)   
        public string SHRTCKN_SESS_CODE { get; set; } //                     VARCHAR2(1 CHAR)   
        public string SHRTCKN_CRSE_TITLE { get; set; } //                    VARCHAR2(30 CHAR)  
        public int? SHRTCKN_REG_SEQ { get; set; } //                       NUMBER(4)          
        public string SHRTCKN_COURSE_COMMENT { get; set; } //                VARCHAR2(30 CHAR)  
        public string SHRTCKN_REPEAT_COURSE_IND { get; set; } //             VARCHAR2(1 CHAR)   
        public DateTime SHRTCKN_ACTIVITY_DATE { get; set; } //        NOT NULL DATE               
        public string SHRTCKN_PTRM_CODE { get; set; } //                     VARCHAR2(3 CHAR)   
        public string SHRTCKN_SEQ_NUMB { get; set; } //                      VARCHAR2(3 CHAR)   
        public DateTime? SHRTCKN_PTRM_START_DATE { get; set; } //               DATE               
        public DateTime? SHRTCKN_PTRM_END_DATE { get; set; } //                 DATE               
        public int? SHRTCKN_CONT_HR { get; set; } //                       NUMBER(7,3)        
        public string SHRTCKN_SCHD_CODE { get; set; } //                     VARCHAR2(3 CHAR)   
        public string SHRTCKN_REPEAT_SYS_IND { get; set; } //                VARCHAR2(1 CHAR)   
        public DateTime? SHRTCKN_REG_START_DATE { get; set; } //                DATE               
        public DateTime? SHRTCKN_REG_COMPLETION_DATE { get; set; } //           DATE               
        public int? SHRTCKN_NUMBER_OF_EXTENSIONS { get; set; } //          NUMBER(3)          
        public string SHRTCKN_LONG_COURSE_TITLE { get; set; } //             VARCHAR2(100 CHAR) 
        public int? SHRTCKN_STSP_KEY_SEQUENCE { get; set; } //             NUMBER(2)          
        public int SHRTCKN_SURROGATE_ID { get; set; } //         NOT NULL NUMBER(19)         
        public int SHRTCKN_VERSION { get; set; } //              NOT NULL NUMBER(19)         
        public string SHRTCKN_USER_ID { get; set; } //                       VARCHAR2(30 CHAR)  
        public string SHRTCKN_DATA_ORIGIN { get; set; } //                   VARCHAR2(30 CHAR)  
        public string SHRTCKN_VPDI_CODE { get; set; } //                     VARCHAR2(6 CHAR)   
    }
}
