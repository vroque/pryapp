﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Vinculo entre transferencia y institucion de trasnferencia
    /// </summary>
    [Table("SHRTRIT")]
    public class SHRTRIT
    {
        [Key]
        [Column(Order = 1)]
        public decimal SHRTRIT_PIDM { get; set; } //               NOT NULL NUMBER(8)         
        [Key]
        [Column(Order = 2)]
        public int SHRTRIT_SEQ_NO { get; set; } //             NOT NULL NUMBER(2)         
        public string SHRTRIT_SBGI_CODE { get; set; } //          NOT NULL VARCHAR2(6 CHAR)  
        public string SHRTRIT_SBGI_DESC { get; set; } //                   VARCHAR2(30 CHAR) 
        public string SHRTRIT_OFFICIAL_TRANS_IND { get; set; } //          VARCHAR2(1 CHAR)  
        public DateTime? SHRTRIT_TRANS_DATE_RCVD { get; set; } //             DATE              
        public DateTime SHRTRIT_ACTIVITY_DATE { get; set; } //      NOT NULL DATE              
        public int SHRTRIT_SURROGATE_ID { get; set; } //       NOT NULL NUMBER(19)        
        public int SHRTRIT_VERSION { get; set; } //            NOT NULL NUMBER(19)        
        public string SHRTRIT_USER_ID { get; set; } //                     VARCHAR2(30 CHAR) 
        public string SHRTRIT_DATA_ORIGIN { get; set; } //                 VARCHAR2(30 CHAR) 
        public string SHRTRIT_VPDI_CODE { get; set; } //                   VARCHAR2(6 CHAR)  
    }
}
