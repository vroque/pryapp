﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Nombre de escuela académican en banner
    /// </summary>
    [Table("SZVMAJR")]
    public class SZVMAJR
    {
        /// <summary>
        /// This field identifies the major code referenced in
        /// the Catalog, Class Sched., Recruit., Admissions,
        /// Gen. Student, Registr., and Acad. Hist. Modules.
        /// Reqd. value: 00 - Major Not Declared.
        /// NOT NULL
        /// </summary>
        [Key]
        [Column(Order = 1)]
        public string SZVMAJR_CODE { get; set; }

        /// <summary>
        /// This field specifies the major area of study
        /// associated with the major code.
        /// NULL
        /// </summary>
        public string SZVMAJR_DESC { get; set; }

        /// <summary>
        /// This field specifies the abbreviation of the main
        /// study area associated with the main code.
        /// NULL
        /// <para>
        /// No le hagas caso a lo que dice la descripción de la tabla.
        /// Este campo contiene el nombre completo de la carrera
        /// </para>
        /// </summary>
        public string SZVMAJR_DESCRIPTION { get; set; }
    }
}