﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("SFBRGRP")]
    public class SFBRGRP
    {
        [Key]
        [Column(Order = 1)]
        public string SFBRGRP_TERM_CODE { get; set; }           // NOT NULL VARCHAR2(6 CHAR)
        [Key]
        [Column(Order = 2)]
        public int SFBRGRP_PIDM { get; set; }                   // NOT NULL NUMBER(8)
        public string SFBRGRP_RGRP_CODE { get; set; }           // NOT NULL VARCHAR2(10 CHAR)
        public string SFBRGRP_USER { get; set; }                // NOT NULL VARCHAR2(30 CHAR)
        public DateTime SFBRGRP_ACTIVITY_DATE { get; set; }     // NOT NULL DATE
        public int SFBRGRP_SURROGATE_ID { get; set; }           // NOT NULL NUMBER(19)
        public int SFBRGRP_VERSION { get; set; }                // NOT NULL NUMBER(19)
        public string SFBRGRP_USER_ID { get; set; }             // VARCHAR2(30 CHAR)
        public string SFBRGRP_DATA_ORIGIN { get; set; }         // VARCHAR2(30 CHAR)
        public string SFBRGRP_VPDI_CODE { get; set; }           // VARCHAR2(6 CHAR)
    }
}
