﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Cursos convalidados de la institucion de procedencia
    /// </summary>
    [Table("SHRTRCR")]
    public class SHRTRCR
    {
        [Key]
        [Column(Order = 1)]
        public int SHRTRCR_PIDM { get; set; } //                NOT NULL NUMBER(8)         
        [Key]
        [Column(Order = 2)]
        public int SHRTRCR_TRIT_SEQ_NO { get; set; } //         NOT NULL NUMBER(2)         
        [Key]
        [Column(Order = 3)]
        public int SHRTRCR_TRAM_SEQ_NO { get; set; } //         NOT NULL NUMBER(2)         
        [Key]
        [Column(Order = 4)]
        public int SHRTRCR_SEQ_NO { get; set; } //              NOT NULL NUMBER(3)         
        public string SHRTRCR_TRANS_COURSE_NAME { get; set; } //            VARCHAR2(60 CHAR) 
        public string SHRTRCR_TRANS_COURSE_NUMBERS { get; set; } //         VARCHAR2(60 CHAR) 
        public float? SHRTRCR_TRANS_CREDIT_HOURS { get; set; } //           NUMBER(7,3)       
        public string SHRTRCR_TRANS_GRADE { get; set; } //                  VARCHAR2(6 CHAR)  
        public string SHRTRCR_TRANS_GRADE_MODE { get; set; } //             VARCHAR2(1 CHAR)  
        public DateTime SHRTRCR_ACTIVITY_DATE { get; set; } //       NOT NULL DATE              
        public string SHRTRCR_LEVL_CODE { get; set; } //                    VARCHAR2(2 CHAR)  
        public string SHRTRCR_TERM_CODE { get; set; } //                    VARCHAR2(6 CHAR)  
        public string SHRTRCR_GROUP { get; set; } //                        VARCHAR2(2 CHAR)  
        public string SHRTRCR_ART_IND { get; set; } //                      VARCHAR2(1 CHAR)  
        public string SHRTRCR_PROGRAM { get; set; } //                      VARCHAR2(12 CHAR) 
        public string SHRTRCR_GROUP_PRIMARY_IND { get; set; } //            VARCHAR2(1 CHAR)  
        public string SHRTRCR_DUPLICATE { get; set; } //                    VARCHAR2(2 CHAR)  
        public string SHRTRCR_TCRSE_TITLE { get; set; } //                  VARCHAR2(30 CHAR) 
        public int SHRTRCR_SURROGATE_ID { get; set; } //        NOT NULL NUMBER(19)        
        public int SHRTRCR_VERSION { get; set; } //             NOT NULL NUMBER(19)        
        public string SHRTRCR_USER_ID { get; set; } //                      VARCHAR2(30 CHAR) 
        public string SHRTRCR_DATA_ORIGIN { get; set; } //                  VARCHAR2(30 CHAR) 
        public string SHRTRCR_VPDI_CODE { get; set; } //                    VARCHAR2(6 CHA
    }
}
