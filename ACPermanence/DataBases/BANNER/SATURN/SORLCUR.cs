﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("SORLCUR")]
    public class SORLCUR
    {
        [Key]
        [Column(Order =1)]
        public decimal SORLCUR_PIDM { get; set; } //                 NOT NULL NUMBER(8)         
        [Key]
        [Column(Order = 2)]
        public int SORLCUR_SEQNO { get; set; } //                NOT NULL NUMBER(4)         
        public string SORLCUR_LMOD_CODE { get; set; } //            NOT NULL VARCHAR2(15 CHAR) 
        public string SORLCUR_TERM_CODE { get; set; } //            NOT NULL VARCHAR2(6 CHAR)  
        public int SORLCUR_KEY_SEQNO { get; set; } //            NOT NULL NUMBER(2)         
        public int SORLCUR_PRIORITY_NO { get; set; } //          NOT NULL NUMBER(4)         
        public string SORLCUR_ROLL_IND { get; set; } //             NOT NULL VARCHAR2(1 CHAR)  
        public string SORLCUR_CACT_CODE { get; set; } //            NOT NULL VARCHAR2(15 CHAR) 
        public string SORLCUR_USER_ID { get; set; } //              NOT NULL VARCHAR2(30 CHAR) 
        public string SORLCUR_DATA_ORIGIN { get; set; } //          NOT NULL VARCHAR2(30 CHAR) 
        public DateTime SORLCUR_ACTIVITY_DATE { get; set; } //        NOT NULL DATE              
        public string SORLCUR_LEVL_CODE { get; set; } //            NOT NULL VARCHAR2(2 CHAR)  
        public string SORLCUR_COLL_CODE { get; set; } //            NOT NULL VARCHAR2(2 CHAR)  
        public string SORLCUR_DEGC_CODE { get; set; } //            NOT NULL VARCHAR2(6 CHAR)  
        public string SORLCUR_TERM_CODE_CTLG { get; set; } //                VARCHAR2(6 CHAR)  
        public string SORLCUR_TERM_CODE_END { get; set; } //                 VARCHAR2(6 CHAR)  
        public string SORLCUR_TERM_CODE_MATRIC { get; set; } //              VARCHAR2(6 CHAR)  
        public string SORLCUR_TERM_CODE_ADMIT { get; set; } //               VARCHAR2(6 CHAR)  
        public string SORLCUR_ADMT_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public string SORLCUR_CAMP_CODE { get; set; } //                     VARCHAR2(3 CHAR)  
        public string SORLCUR_PROGRAM { get; set; } //                       VARCHAR2(12 CHAR) 
        public DateTime? SORLCUR_START_DATE { get; set; } //                    DATE              
        public DateTime? SORLCUR_END_DATE { get; set; } //                      DATE              
        public int? SORLCUR_CURR_RULE { get; set; } //                     NUMBER(8)         
        public int? SORLCUR_ROLLED_SEQNO { get; set; } //                  NUMBER(4)         
        public string SORLCUR_STYP_CODE { get; set; } //                     VARCHAR2(1 CHAR)  
        public string SORLCUR_RATE_CODE { get; set; } //                     VARCHAR2(5 CHAR)  
        public string SORLCUR_LEAV_CODE { get; set; } //                     VARCHAR2(1 CHAR)  
        public DateTime? SORLCUR_LEAV_FROM_DATE { get; set; } //                DATE              
        public DateTime? SORLCUR_LEAV_TO_DATE { get; set; } //                  DATE              
        public DateTime? SORLCUR_EXP_GRAD_DATE { get; set; } //                 DATE              
        public string SORLCUR_TERM_CODE_GRAD { get; set; } //                VARCHAR2(6 CHAR)  
        public string SORLCUR_ACYR_CODE { get; set; } //                     VARCHAR2(4 CHAR)  
        public string SORLCUR_SITE_CODE { get; set; } //                     VARCHAR2(3 CHAR)  
        public int? SORLCUR_APPL_SEQNO { get; set; } //                    NUMBER(4)         
        public int? SORLCUR_APPL_KEY_SEQNO { get; set; } //                NUMBER(2)         
        public string SORLCUR_USER_ID_UPDATE { get; set; } //                VARCHAR2(30 CHAR) 
        public DateTime? SORLCUR_ACTIVITY_DATE_UPDATE { get; set; } //          DATE              
        public int? SORLCUR_GAPP_SEQNO { get; set; } //                    NUMBER(4)         
        public string SORLCUR_CURRENT_CDE { get; set; } //                   VARCHAR2(1 CHAR)  
        public int SORLCUR_SURROGATE_ID { get; set; } //         NOT NULL NUMBER(19)        
        public int SORLCUR_VERSION { get; set; } //              NOT NULL NUMBER(19)        
        public string SORLCUR_VPDI_CODE { get; set; } //                     VARCHAR2(6 CHAR) 
    }
}
