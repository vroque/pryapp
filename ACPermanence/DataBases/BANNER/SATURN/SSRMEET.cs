﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("SATURN.SSRMEET")]
    public class SSRMEET
    {
        /// <summary>
        /// This field is not displayed on the form (page 0).
        /// It defines the term for which you are creating
        /// meeting times for the course section.  It is
        /// based on the Key Block Term.
        /// VARCHAR2(6 CHAR) NOT NULL
        /// </summary>
        public string SSRMEET_TERM_CODE { get; set; }

        /// <summary>
        /// This field is not displayed on the form (page 0).
        /// It defines the Course Reference Number for the
        /// course section for which you are creating meeting
        /// times
        /// VARCHAR2(5 CHAR) NOT NULL
        /// </summary>
        public string SSRMEET_CRN { get; set; }

        /// <summary>
        /// This field defines the Day code for which the Key
        /// Block section will be scheduled.  It is a
        /// required field to enter a meeting time record.
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SSRMEET_DAYS_CODE { get; set; }

        /// <summary>
        /// This field is not displayed on the form (page 0).
        /// It defines the day number as defined on the
        /// STVDAYS Validation Form
        /// NUMBER(1,0)
        /// </summary>
        public decimal? SSRMEET_DAY_NUMBER { get; set; }

        /// <summary>
        /// This field defines the Begin Time of the course
        /// section being scheduled.  It is a required field
        /// and is in the format HHMM using military times.
        /// The SSRSECT (Schedule of Classes) converts this
        /// time to standard times.
        /// VARCHAR2(4 CHAR)
        /// </summary>
        public string SSRMEET_BEGIN_TIME { get; set; }

        /// <summary>
        /// This field defines the End Time of the course
        /// section being scheduled.  It is a required field
        /// and is in the format HHMM using military times.
        /// The SSRSECT (Schedule of Classes) converts this
        /// time to standard times.
        /// VARCHAR2(4 CHAR)
        /// </summary>
        public string SSRMEET_END_TIME { get; set; }

        /// <summary>
        /// This field defines the Building where the course
        /// section will be scheduled.  It is not required
        /// when scheduling course section meeting times.  It
        /// is required when scheduling course section meeting
        /// rooms.
        /// VARCHAR2(6 CHAR)
        /// </summary>
        public string SSRMEET_BLDG_CODE { get; set; }

        /// <summary>
        /// This field defines the Room where the course
        /// section will be scheduled.  It is not required
        /// when scheduling course section meeting times.  It
        /// is required when scheduling a course section
        /// meeting building.
        /// VARCHAR2(10 CHAR)
        /// </summary>
        public string SSRMEET_ROOM_CODE { get; set; }

        /// <summary>
        /// This field specifies the most current date record
        /// was created or updated.
        /// DATE NOT NULL
        /// </summary>
        public DateTime SSRMEET_ACTIVITY_DATE { get; set; }

        /// <summary>
        /// Section Meeting Start Date.
        /// DATE NOT NULL
        /// </summary>
        public DateTime SSRMEET_START_DATE { get; set; }

        /// <summary>
        /// Section End Date.
        /// DATE NOT NULL
        /// </summary>
        public DateTime SSRMEET_END_DATE { get; set; }

        /// <summary>
        /// Section Indicator.
        /// VARCHAR2(2 CHAR)
        /// </summary>
        public string SSRMEET_CATAGORY { get; set; }

        /// <summary>
        /// Section Meeting Time Sunday Indicator.
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SSRMEET_SUN_DAY { get; set; }

        /// <summary>
        /// Section Meeting Time Monday Indicator.
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SSRMEET_MON_DAY { get; set; }

        /// <summary>
        /// Section Meeting Time Tuesday Indicator.
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SSRMEET_TUE_DAY { get; set; }

        /// <summary>
        /// Section Meeting Time Wednesday Indicator.
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SSRMEET_WED_DAY { get; set; }

        /// <summary>
        /// Section Meeting Time Thrusday Indicator.
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SSRMEET_THU_DAY { get; set; }

        /// <summary>
        /// Section Meeting Time Friday Indicator.
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SSRMEET_FRI_DAY { get; set; }

        /// <summary>
        /// Section Meeting Time Saturday Indicator.
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SSRMEET_SAT_DAY { get; set; }

        /// <summary>
        /// Section Schedule Type.
        /// VARCHAR2(3 CHAR)
        /// </summary>
        public string SSRMEET_SCHD_CODE { get; set; }

        /// <summary>
        /// Section Time Conflict Override Indicator.
        /// VARCHAR2(1 CHAR)
        /// </summary>
        public string SSRMEET_OVER_RIDE { get; set; }

        /// <summary>
        /// The session credit hours
        /// NUMBER(7,3)
        /// </summary>
        public decimal? SSRMEET_CREDIT_HR_SESS { get; set; }

        /// <summary>
        /// Total Section Meeting Number which is system
        /// generated.
        /// NUMBER(4,0)
        /// </summary>
        public decimal? SSRMEET_MEET_NO { get; set; }

        /// <summary>
        /// Section Metting Hours per Week.
        /// NUMBER(5,2)
        /// </summary>
        public decimal? SSRMEET_HRS_WEEK { get; set; }

        /// <summary>
        /// Function code assigned to an event
        /// VARCHAR2(12 CHAR)
        /// </summary>
        public string SSRMEET_FUNC_CODE { get; set; }

        /// <summary>
        /// Committee/Service Type code.
        /// VARCHAR2(6 CHAR)
        /// </summary>
        public string SSRMEET_COMT_CODE { get; set; }

        /// <summary>
        /// Schedule Status Code for use with Scheduling Tool
        /// Interface .
        /// VARCHAR2(3 CHAR)
        /// </summary>
        public string SSRMEET_SCHS_CODE { get; set; }

        /// <summary>
        /// Meeting Type Code. The meeting type code assigned
        /// to this meeting time of the section
        /// VARCHAR2(4 CHAR)
        /// </summary>
        public string SSRMEET_MTYP_CODE { get; set; }

        /// <summary>
        /// DATA SOURCE: Source system that created or updated
        /// the row
        /// VARCHAR2(30 CHAR)
        /// </summary>
        public string SSRMEET_DATA_ORIGIN { get; set; }

        /// <summary>
        /// USER ID: User who inserted or last update the data
        /// VARCHAR2(30 CHAR)
        /// </summary>
        public string SSRMEET_USER_ID { get; set; }

        /// <summary>
        /// SURROGATE ID: Immutable unique key
        /// NUMBER(19,0) NOT NULL
        /// </summary>
        [Key]
        [Column("SSRMEET_SURROGATE_ID")]
        public decimal SSRMEET_SURROGATE_ID { get; set; }

        /// <summary>
        /// VERSION: Optimistic lock token.
        /// NUMBER(19,0) NOT NULL
        /// </summary>
        public decimal SSRMEET_VERSION { get; set; }

        /// <summary>
        /// VPDI CODE: Multi-entity processing code.
        /// VARCHAR2(6 CHAR)
        /// </summary>
        public string SSRMEET_VPDI_CODE { get; set; }
    }
}