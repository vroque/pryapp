﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("SHRTCKG")]
    public class SHRTCKG
    {
        [Key]
        [Column(Order =1)]
        public int SHRTCKG_PIDM { get; set; }  //                       NOT NULL NUMBER(8)         
        [Key]
        [Column(Order = 2)]
        public string SHRTCKG_TERM_CODE { get; set; }  //                  NOT NULL VARCHAR2(6 CHAR)  
        [Key]
        [Column(Order = 3)]
        public int SHRTCKG_TCKN_SEQ_NO { get; set; }  //                NOT NULL NUMBER(2)         
        [Key]
        [Column(Order = 4)]
        public int SHRTCKG_SEQ_NO { get; set; }  //                     NOT NULL NUMBER(2)         
        public string SHRTCKG_FINAL_GRDE_SEQ_NO { get; set; }  //                   VARCHAR2(3 CHAR)  
        public string SHRTCKG_GRDE_CODE_FINAL { get; set; }  //                     VARCHAR2(6 CHAR)  
        public string SHRTCKG_GMOD_CODE { get; set; }  //                           VARCHAR2(1 CHAR)  
        public float SHRTCKG_CREDIT_HOURS { get; set; }  //               NOT NULL NUMBER(7,3)       
        public string SHRTCKG_GCHG_CODE { get; set; }  //                  NOT NULL VARCHAR2(2 CHAR)  
        public DateTime? SHRTCKG_INCOMPLETE_EXT_DATE { get; set; }  //                 DATE              
        public DateTime SHRTCKG_FINAL_GRDE_CHG_DATE { get; set; }  //        NOT NULL DATE              
        public string SHRTCKG_FINAL_GRDE_CHG_USER { get; set; }  //        NOT NULL VARCHAR2(30 CHAR) 
        public DateTime SHRTCKG_ACTIVITY_DATE { get; set; }  //              NOT NULL DATE              
        public string SHRTCKG_GCMT_CODE { get; set; }  //                           VARCHAR2(7 CHAR)  
        public string SHRTCKG_TERM_CODE_GRADE { get; set; }  //                     VARCHAR2(6 CHAR)  
        public string SHRTCKG_DATA_ORIGIN { get; set; }  //                         VARCHAR2(30 CHAR) 
        public string SHRTCKG_USER_ID { get; set; }  //                             VARCHAR2(30 CHAR) 
        public float SHRTCKG_HOURS_ATTEMPTED { get; set; }  //                     NUMBER(7,3)       
        public string SHRTCKG_GRDE_CODE_INCMP_FINAL { get; set; }  //               VARCHAR2(6 CHAR)  
        public int SHRTCKG_SURROGATE_ID { get; set; }  //               NOT NULL NUMBER(19)        
        public int SHRTCKG_VERSION { get; set; }  //                    NOT NULL NUMBER(19)        
        public string SHRTCKG_VPDI_CODE { get; set; }  //                           VARCHAR2(6 CHAR)
    }
}
