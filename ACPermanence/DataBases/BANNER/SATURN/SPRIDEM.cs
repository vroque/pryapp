﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("SATURN.SPRIDEN")]
    public class SPRIDEN
    {
        [Key]
        public decimal SPRIDEN_PIDM { get; set; }
        public string SPRIDEN_ID { get; set; }
        public string SPRIDEN_LAST_NAME { get; set; }
        public string SPRIDEN_FIRST_NAME { get; set; }
    }
}
