﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Informacion de convalidaciones (AKA transferencias)
    /// </summary>
    [Table("SHRTRAM")]
    public class SHRTRAM
    {
        [Key]
        [Column(Order = 1)]
        public decimal SHRTRAM_PIDM { get; set; } //         NOT NULL NUMBER(8)         
        [Key]
        [Column(Order = 2)]
        public int SHRTRAM_TRIT_SEQ_NO { get; set; } //      NOT NULL NUMBER(2)         
        public int SHRTRAM_SEQ_NO { get; set; } //           NOT NULL NUMBER(2)         
        public string SHRTRAM_LEVL_CODE { get; set; } //     VARCHAR2(2 CHAR)  
        public string SHRTRAM_ATTN_PERIOD { get; set; } //   VARCHAR2(12 CHAR) 
        public string SHRTRAM_TERM_CODE_ENTERED { get; set; } //NOT NULL VARCHAR2(6 CHAR)  
        public string SHRTRAM_DEGC_CODE { get; set; } //                 VARCHAR2(6 CHAR)  
        public string SHRTRAM_TERM_TYPE { get; set; } //                 VARCHAR2(30 CHAR) 
        public DateTime? SHRTRAM_ACCEPTANCE_DATE { get; set; } //           DATE              
        public DateTime SHRTRAM_ACTIVITY_DATE { get; set; } //    NOT NULL DATE              
        public DateTime? SHRTRAM_ATTN_BEGIN_DATE { get; set; } //           DATE              
        public DateTime? SHRTRAM_ATTN_END_DATE { get; set; } //             DATE              
        public int SHRTRAM_SURROGATE_ID { get; set; } //     NOT NULL NUMBER(19)        
        public int SHRTRAM_VERSION { get; set; } //          NOT NULL NUMBER(19)        
        public string SHRTRAM_USER_ID { get; set; } //                   VARCHAR2(30 CHAR) 
        public string SHRTRAM_DATA_ORIGIN { get; set; } //               VARCHAR2(30 CHAR) 
        public string SHRTRAM_VPDI_CODE { get; set; } //                 VARCHAR2(6 CHAR)  
    }
}
