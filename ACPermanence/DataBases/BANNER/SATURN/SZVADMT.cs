﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Obsolete("Use SZBADMT instead", true)]
    [Table("SATURN.SZVADMT")]
    public class SZVADMT
    {
        /// <summary>
        /// No This field identifies the admission type code
        /// referenced in the Admissions Module.
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [Key]
        public string SZVADMT_CODE { get; set; } // VARCHAR2(2 CHAR)     

        /// <summary>
        /// No This field specifies the admission type(e.g. new
        /// admit, transfer, readmit, adult/continuing ed.,
        /// etc.) associated with the admission type code.
        /// </summary>
        public string SZVADMT_DESC { get; set; } // VARCHAR2(30 CHAR)  
          
        /// <summary>
        /// No This field specifies the admission type(e.g. new
        /// admit, transfer, readmit,adult/continuing ed.,
        /// etc.) associated with the admission type code.
        /// </summary>
        public string SZVADMT_DESCRIPTION { get; set; } //    VARCHAR2(300 CHAR)   

        /// <summary>
        /// This field identifies the most current date a
        /// record was created or updated.
        /// </summary>
        public DateTime? SZVADMT_ACTIVITY_DATE { get; set; } //  DATE 

        /// <summary>
        /// Yes USER ID: The user ID of the person who inserted or
        /// last updated this record.
        /// </summary>
        public string SZVADMT_USER_ID { get; set; } // VARCHAR2(30 CHAR)  
         
        /// <summary>
        /// Yes DATA ORIGIN: Source system that created or updated
        /// the data.
        /// </summary>
        public string SZVADMT_DATA_ORIGIN { get; set; } // VARCHAR2(30 CHAR)    
    }
}
