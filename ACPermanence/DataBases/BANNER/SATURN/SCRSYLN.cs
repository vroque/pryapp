﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Nombre largo de cursos
    /// Course Syllabus course long name table 
    /// </summary>
    [Table("SCRSYLN")]
    public class SCRSYLN
    {
        /// <summary>
        /// Identifies the subject code of the course that the
        /// long name is attached to
        /// </summary>
        [Key]
        [Column(Order = 1)]
        public string SCRSYLN_SUBJ_CODE { get; set; }
        /// <summary>
        /// Identifies the course number of the course that
        /// the long name is attached to
        /// </summary>
        [Key]
        [Column(Order = 2)]
        public string SCRSYLN_CRSE_NUMB { get; set; }
        /// <summary>
        /// The long title of the course
        /// </summary>
        public string SCRSYLN_LONG_COURSE_TITLE { get; set; }
        /// <summary>
        /// Identifies the Start Term for which the long name
        /// is applicable to
        /// </summary>
        public string SCRSYLN_TERM_CODE_EFF { get; set; }
        /// <summary>
        /// VERSION: Optimistic lock token.
        /// </summary>
        public int SCRSYLN_VERSION { get; set; }
    }
}
