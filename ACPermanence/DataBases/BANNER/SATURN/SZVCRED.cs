﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    [Table("SATURN.SZVCRED")]
    public class SZVCRED
    {
        [Key]
        [Column(Order = 1)]
        public string SZVCRED_PROGRAM { get; set; } //                   VARCHAR2(12 CHAR) 
        [Key]
        [Column(Order = 2)]
        public string SZVCRED_TERM_CODE_CTLG { get; set; } //           VARCHAR2(6 CHAR)  
        public string SZVCRED_TERM_CODE_CTLG_END { get; set; } //       VARCHAR2(6 CHAR)  
        [Key]
        [Column(Order = 3)]
        public string SZVCRED_DEPT_CODE { get; set; } //                VARCHAR2(4 CHAR)  
        public int? SZVCRED_CRED_O { get; set; } //                   NUMBER(3)         
        public int? SZVCRED_CRED_E { get; set; } //                   NUMBER(3)         
        public int? SZVCRED_CRED { get; set; } //                     NUMBER(3)         
        public string SZVCRED_STUDY_PATH { get; set; } //               VARCHAR2(10 CHAR
    }
}
