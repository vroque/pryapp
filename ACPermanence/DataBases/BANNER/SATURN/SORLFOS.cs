﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Learner curriculum field of study
    /// </summary>
    [Table("SORLFOS")]
    public class SORLFOS
    {
        [Key]
        [Column(Order = 1)]
        public decimal SORLFOS_PIDM { get; set; } //                 NOT NULL NUMBER(8)         
        [Key]
        [Column(Order = 2)]
        public int SORLFOS_LCUR_SEQNO { get; set; } //           NOT NULL NUMBER(4)         
        public int SORLFOS_SEQNO { get; set; } //                NOT NULL NUMBER(4)         
        public string SORLFOS_LFST_CODE { get; set; } //            NOT NULL VARCHAR2(15 CHAR) 
        public string SORLFOS_TERM_CODE { get; set; } //            NOT NULL VARCHAR2(6 CHAR)  
        public int SORLFOS_PRIORITY_NO { get; set; } //          NOT NULL NUMBER(4)         
        public string SORLFOS_CSTS_CODE { get; set; } //            NOT NULL VARCHAR2(15 CHAR) 
        public string SORLFOS_CACT_CODE { get; set; } //            NOT NULL VARCHAR2(15 CHAR) 
        public string SORLFOS_DATA_ORIGIN { get; set; } //          NOT NULL VARCHAR2(30 CHAR) 
        public string SORLFOS_USER_ID { get; set; } //              NOT NULL VARCHAR2(30 CHAR) 
        public DateTime SORLFOS_ACTIVITY_DATE { get; set; } //        NOT NULL DATE              
        public string SORLFOS_MAJR_CODE { get; set; } //            NOT NULL VARCHAR2(4 CHAR)  
        public string SORLFOS_TERM_CODE_CTLG { get; set; } //                VARCHAR2(6 CHAR)  
        public string SORLFOS_TERM_CODE_END { get; set; } //                 VARCHAR2(6 CHAR)  
        public string SORLFOS_DEPT_CODE { get; set; } //                     VARCHAR2(4 CHAR)  
        public string SORLFOS_MAJR_CODE_ATTACH { get; set; } //              VARCHAR2(4 CHAR)  
        public int? SORLFOS_LFOS_RULE { get; set; } //                     NUMBER(8)         
        public int? SORLFOS_CONC_ATTACH_RULE { get; set; } //              NUMBER(8)         
        public DateTime? SORLFOS_START_DATE { get; set; } //                    DATE              
        public DateTime? SORLFOS_END_DATE { get; set; } //                      DATE              
        public string SORLFOS_TMST_CODE { get; set; } //                     VARCHAR2(2 CHAR)  
        public int? SORLFOS_ROLLED_SEQNO { get; set; } //                  NUMBER(4)         
        public string SORLFOS_USER_ID_UPDATE { get; set; } //                VARCHAR2(30 CHAR) 
        public DateTime? SORLFOS_ACTIVITY_DATE_UPDATE { get; set; } //          DATE              
        public string SORLFOS_CURRENT_CDE { get; set; } //                   VARCHAR2(1 CHAR)  
        public int SORLFOS_SURROGATE_ID { get; set; } //         NOT NULL NUMBER(19)        
        public int SORLFOS_VERSION { get; set; } //              NOT NULL NUMBER(19)        
        public string SORLFOS_VPDI_CODE { get; set; } //                     VARCHAR2(6 CHAR)
    }
}
