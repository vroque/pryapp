﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.SATURN
{
    /// <summary>
    /// Notas de componentes de un curso
    /// Grade Component Student Record
    /// </summary>
    [Table("SHRMRKS")]
    public class SHRMRKS
    {
        /// <summary>
        /// Term Code associated with Marks Record
        /// </summary>
        [Key]
        [Column(Order =1)]
        public string SHRMRKS_TERM_CODE { get; set; }
        /// <summary>
        /// CRN associated with Marks Record
        /// </summary>
        [Key]
        [Column(Order = 2)]
        public string SHRMRKS_CRN { get; set; }
        /// <summary>
        /// PIDM of student
        /// </summary>
        [Key]
        [Column(Order = 3)]
        public decimal SHRMRKS_PIDM { get; set; }
        /// <summary>
        /// Gradable Component ID
        /// </summary>
        [Key]
        [Column(Order = 4)]
        public int? SHRMRKS_GCOM_ID { get; set; }
        /// <summary>
        /// Grade Code Assigned
        /// </summary>
        public string SHRMRKS_GRDE_CODE { get; set; }
        /// <summary>
        /// Score of Gradable Component
        /// </summary>
        public float? SHRMRKS_SCORE { get; set; }
        /// <summary>
        /// SUMMARY: A brief summary of how the grade was
        /// calculated
        /// </summary>
        public string SHRMRKS_SUMMARY { get; set; }
        /// <summary>
        /// Date of Insert or Last Update
        /// </summary>
        public DateTime? SHRMRKS_ACTIVITY_DATE { get; set; }
    }
}
