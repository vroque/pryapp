﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.BANINST1
{
    [Table("BANINST1.SZTPROY")]
    public class SZTPROY
    {
        [Key]
        [Column(Order = 1)]
        public string SZTPROY_CRN { get; set; }                 //NOT NULL VARCHAR2(5 CHAR)
        [Key]
        [Column(Order = 2)]
        public int SZTPROY_PIDM { get; set; }                   //NOT NULL NUMBER(8)
        [Key]
        [Column(Order = 3)]
        public string SZTPROY_SUBJ_CODE { get; set; }           //VARCHAR2(4 CHAR)
        [Key]
        [Column(Order = 4)]
        public string SZTPROY_CRSE_NUMB { get; set; }           //VARCHAR2(5 CHAR)
        [Key]
        [Column(Order = 5)]
        public string SZTPROY_TERM_CODE { get; set; }           //NOT NULL VARCHAR2(6 CHAR)
        public string SZTPROY_PROGRAM { get; set; }             //NOT NULL VARCHAR2(12 CHAR)

        public string SZTPROY_SPRIDEN_ID { get; set; }          //NOT NULL VARCHAR2(9 CHAR)
        public string SZTPROY_CAMP_CODE { get; set; }           //NOT NULL VARCHAR2(3 CHAR)
        public string SZTPROY_DEPT_CODE { get; set; }           //VARCHAR2(4 CHAR)
        public string SZTPROY_COLL { get; set; }                //NOT NULL VARCHAR2(2 CHAR)
        public string SZTPROY_ATTRIBUTE { get; set; }           //NOT NULL VARCHAR2(4 CHAR)
        public string SZTPROY_SECT_CAMP_CODE_ { get; set; }     //NOT NULL VARCHAR2(3 CHAR)
        public string SZTPROY_VISIBLE_IND1 { get; set; }        //VARCHAR2(1 CHAR)
        public string SZTPROY_VISIBLE_IND2 { get; set; }        //VARCHAR2(1 CHAR)
        public string SZTPROY_CAMP_IND { get; set; }            //VARCHAR2(2 CHAR)
        public string SZTPROY_DEPT_IND { get; set; }            //VARCHAR2(2 CHAR)
        public string SZTPROY_COLL_IND { get; set; }            //VARCHAR2(2 CHAR)
        public string SZTPROY_PROGRAM_IND { get; set; }         //VARCHAR2(2 CHAR)
        public string SZTPROY_ATTRIBUTE_IND { get; set; }       //VARCHAR2(2 CHAR)
        public DateTime SZTPROY_ACTIVITY_DATE { get; set; }     //NOT NULL DATE
        public int SZTPROY_SVRPROY_SURROGATE_ID { get; set; }   //NOT NULL NUMBER(19)
        public int SZTPROY_SSBSECT_SURROGATE_ID { get; set; }   //NOT NULL NUMBER(19)
        public string SZTPROY_USER_ID { get; set; }             //VARCHAR2(30 CHAR)
    }
}
