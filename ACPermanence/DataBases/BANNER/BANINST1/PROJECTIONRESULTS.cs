﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.BANINST1
{
    [Table("BANINST1.SVQ_PROJECTIONRESULTS")]
    public class PROJECTIONRESULTS
    {
        [Key]
        [Column(Order = 1)]
        public int SFRREGP_SURROGATE_ID { get; set; }           //NOT NULL NUMBER(19)
        [Key]
        [Column(Order = 2)]
        public string SFRREGP_MANDATORY_IND { get; set; }       //NOT NULL VARCHAR2(1 CHAR)
        [Key]
        [Column(Order = 3)]
        public string SFRREGP_SUBJ_CODE { get; set; }           //VARCHAR2(4 CHAR)
        public string SFRREGP_CRSE_NUMB_LOW { get; set; }       //VARCHAR2(5 CHAR)
        [Key]
        [Column(Order = 4)]
        public int SFRREGP_PIDM { get; set; }                   //NOT NULL NUMBER(8)

        public int SFRREGP_VERSION { get; set; }                //NOT NULL NUMBER(19)
        public string SFRREGP_TERM_CODE { get; set; }           //NOT NULL VARCHAR2(6 CHAR)
        public string SFRREGP_TERM_CODE_SCHEDULE { get; set; }  //NOT NULL VARCHAR2(6 CHAR)
        public string SFRREGP_MAINT_IND { get; set; }           //NOT NULL VARCHAR2(1 CHAR)
        public DateTime SFRREGP_ACTIVITY_DATE {get; set;}       //NOT NULL DATE
        public string SFRREGP_PROGRAM { get; set; }             //NOT NULL VARCHAR2(12 CHAR)
        public int? SFRREGP_STSP_KEY_SEQUENCE { get; set; }     //NUMBER(2)
        public string SFRREGP_AREA { get; set; }                //VARCHAR2(10 CHAR)
        public int? SFRREGP_AREA_PRIORITY { get; set; }         //NUMBER(3)
        public int? SFRREGP_CAA_SEQNO { get; set; }             //NUMBER(8)
        public string SFRREGP_CRSE_NUMB_HIGH { get; set; }      //VARCHAR2(5 CHAR)
        public string SFRREGP_CRN_SCHEDULE { get; set; }        //VARCHAR2(5 CHAR)
        public string SFRREGP_ATTR_CODE { get; set; }           //VARCHAR2(4 CHAR)
        public string SFRREGP_LEVL_CODE { get; set; }           //VARCHAR2(2 CHAR)
        public string SFRREGP_CAMP_CODE { get; set; }           //VARCHAR2(3 CHAR)
        public string SFRREGP_COLL_CODE { get; set; }           //VARCHAR2(2 CHAR)
        public string SFRREGP_DEPT_CODE { get; set; }           //VARCHAR2(4 CHAR)
        public string SFRREGP_ELECTIVE { get; set; }            //VARCHAR2(1 CHAR)
        public string SFRREGP_ERROR_FLAG { get; set; }          //VARCHAR2(1 CHAR)
        public string SFRREGP_MOST_PROB { get; set; }           //VARCHAR2(1 CHAR)
        public string SFRREGP_SOURCE { get; set; }              //VARCHAR2(1 CHAR)
        public string SCBCRSE_TITLE { get; set; }               //VARCHAR2(120 CHAR)
        public int? SCBCRSE_CREDIT_HR_LOW { get; set; }         //NUMBER(7,3)
        public int? SCBCRSE_CREDIT_HR_HIGH { get; set; }        //NUMBER(7,3)
        public string SCBCRSE_CREDIT_HR_IND { get; set; }       //VARCHAR2(2 CHAR)
        public string SUBJECT_COURSE { get; set; }              //VARCHAR2(9 CHAR)
        public string STVATTR_DESC { get; set; }                //VARCHAR2(30 CHAR)
        public string STVSUBJ_DESC { get; set; }                //VARCHAR2(30 CHAR)
        public string SMRPRLE_PROGRAM_DESC { get; set; }        //VARCHAR2(30 CHAR)
        public string SFRREGP_AREA_DESCRIPTION { get; set; }    //VARCHAR2(30 CHAR)
    }
}
