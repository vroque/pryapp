﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.BANINST1
{
    [Table("BANINST1.SZVNPES")]
    public class SZVNPES
    {
        public string IDDEPENDENCIA { get; set; }
        public string MODALIDAD { get; set; }
        public string IDSEDE { get; set; }
        [Key]
        [Column(Order = 1)]
        public string PERIODO { get; set; }
        [Key]
        [Column(Order = 2)]
        public string NRC { get; set; }
        public string PARTEPERIODO { get; set; }
        public string MODULO { get; set; }
        public decimal? PIDM_DOCENTE { get; set; }
        public string IDASIGNATURA { get; set; }
        public string DESCASIGNATURA { get; set; }
        public string SECCION { get; set; }
        public string CREDITOS { get; set; }
    }
}
