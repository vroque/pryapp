﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.BANINST1
{

    [Table("BANINST1.SZVCMPP")]
    public class SZVCMPP
    {
        [Key]
        [Column(Order = 1)]
        public decimal SFRSTCR_PIDM { get; set; } //               NOT NULL NUMBER(8)         
        [Key]
        [Column(Order = 2)]
        public string SORLCUR_PROGRAM { get; set; } //                  VARCHAR2(12 CHAR) 
        [Key]
        [Column(Order = 3)]
        public string SORLFOS_DEPT_CODE { get; set; } //                VARCHAR2(4 CHAR)  
        [Key]
        [Column(Order = 4)]
        public string SORLCUR_CAMP_CODE { get; set; } //                VARCHAR2(3 CHAR)  
        [Key]
        [Column(Order = 5)]
        public string SSBSECT_TERM_CODE { get; set; } //       NOT NULL VARCHAR2(6 CHAR)  
        [Key]
        [Column(Order = 6)]
        public string SORLCUR_COLL_CODE { get; set; } //                VARCHAR2(2 CHAR)  
        [Key]
        [Column(Order = 7)]
        public string SORLCUR_LEVL_CODE { get; set; } //                VARCHAR2(2 CHAR)  
        [Key]
        [Column(Order = 8)]
        public string SORLCUR_ADMT_CODE { get; set; } //                VARCHAR2(2 CHAR)  
        [Key]
        [Column(Order = 9)]
        public string SORLCUR_TERM_CODE_ADMIT { get; set; } //          VARCHAR2(6 CHAR)  

        public int? NRCMATRICULADOS { get; set; } //                    NUMBER            
        public int? CREDITOS_MATRICULADO { get; set; } //               NUMBER            
        public DateTime? FECHA_MATRICULA { get; set; } //               DATE              
        [Key]
        [Column(Order = 10)]
        public string SORLCUR_TERM_CODE_CTLG { get; set; } //           VARCHAR2(6 CHAR) 
        public int SORLCUR_KEY_SEQNO { get; set; }  // INT
        public string SORLCUR_DEGC_CODE { get; set; } //            NOT NULL VARCHAR2(6 CHAR)  
    }
}
