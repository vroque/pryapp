﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.BANINST1
{
    /// <summary>
    ///  Se quita el esquema por que banner no quiere usar esquemas
    /// </summary>
    [Table("BANINST1.SZRCDAB")]
    public class SZRCDAB
    {
        public string SZRCDAB_ELEC { get; set; } //              CHAR(1 BYTE)         Yes              
        [Key]
        [Column(Order = 1)]
        public string SZRCDAB_DEPT_CODE { get; set; } //         VARCHAR2(4 CHAR)     Yes              
        [Key]
        [Column(Order = 2)]
        public string SZRCDAB_MARJ_CODE { get; set; } //         VARCHAR2(4 CHAR)     Yes              
        [Key]
        [Column(Order = 3)]
        public string SZRCDAB_CAMP_CODE { get; set; } //         VARCHAR2(3 CHAR)     Yes              
        [Key]
        [Column(Order = 4)]
        public string SZRCDAB_TERM_CODE_EFF { get; set; } //     VARCHAR2(6 CHAR)     No               
        public float? SZRCDAB_PGEN_REQ_CREDITS { get; set; } //  NUMBER(11,3)         Yes              
        public string SZRCDAB_AGEN_AREA { get; set; } //         VARCHAR2(10 CHAR)    No               
        public int? SZRCDAB_AREA_PRIORITY { get; set; } //     NUMBER(3,0)          Yes              
        public string SZRCDAB_CICL { get; set; } //              VARCHAR2(8 BYTE)     Yes              
        public float? SZRCDAB_AGEN_REQ_CREDITS { get; set; } //  NUMBER(11,3)         Yes              
        public string SZRCDAB_KEY_RULE { get; set; } //          VARCHAR2(10 CHAR)    No               
        [Key]
        [Column(Order = 5)]
        public string SZRCDAB_SUBJ_CODE { get; set; } //         VARCHAR2(4 CHAR)     Yes              
        [Key]
        [Column(Order = 6)]
        public string SZRCDAB_CRSE_NUMB { get; set; } //         VARCHAR2(5 CHAR)     No               
        [Key]
        [Column(Order = 7)]
        public string SZRCDAB_PAAP_AREA { get; set; } //         VARCHAR2(10 CHAR)    Yes              
        public string SZRCDAB_TITLE { get; set; } //             VARCHAR2(100 CHAR)   Yes              
        public float? SZRCDAB_CREDIT_HR_HIGH { get; set; } //    NUMBER(7,3)          Yes              
        public string SZRCDAB_COLL_CODE { get; set; } //         VARCHAR2(2 CHAR)     No               
        public string SZRCDAB_COLL_DESC { get; set; } //         VARCHAR2(30 CHAR)    No       
    }
}
