﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.BANINST1
{
    [Table("BANINST1.SVQ_CLASSSEARCH")]
    public class CLASSSEARCH
    {
        [Key]
        [Column(Order = 1)]
        public int SSBSECT_SURROGATE_ID { get; set; }                   //NOT NULL NUMBER(19)
        [Key]
        [Column(Order = 2)]
        public string SSBSECT_TERM_CODE { get; set; }                   //NOT NULL VARCHAR2(6 CHAR)
        [Key]
        [Column(Order = 3)]
        public string SSBSECT_CRN { get; set; }                         //NOT NULL VARCHAR2(5 CHAR)

        public string SSBSECT_PTRM_CODE { get; set; }                   //VARCHAR2(3 CHAR)
        [Key]
        [Column(Order = 4)]
        public string SSBSECT_SUBJ_CODE { get; set; }                   //NOT NULL VARCHAR2(4 CHAR)
        public string STVSUBJ_DESC { get; set; }                        //VARCHAR2(30 CHAR)
        public string SSBSECT_SEQ_NUMB { get; set; }                    //NOT NULL VARCHAR2(3 CHAR)
        public string SSBSECT_LINK_IDENT { get; set; }                  //VARCHAR2(2 CHAR)
        [Key]
        [Column(Order = 5)]
        public string SSBSECT_CRSE_NUMB { get; set; }                   //NOT NULL VARCHAR2(5 CHAR)

        public int SSBSECT_VERSION { get; set; }                        //NOT NULL NUMBER(19)
        public string TERM_DESC { get; set; }                           //VARCHAR2(30 CHAR)
        public string SSBSECT_SSTS_CODE { get; set; }                   //NOT NULL VARCHAR2(1 CHAR)
        public string SSBSECT_SCHD_CODE { get; set; }                   //NOT NULL VARCHAR2(3 CHAR)
        public string STVSCHD_DESC { get; set; }                        //VARCHAR2(30 CHAR)
        public string SSBSECT_CAMP_CODE { get; set; }                   //NOT NULL VARCHAR2(3 CHAR)
        public string STVCAMP_DESC { get; set; }                        //VARCHAR2(30 CHAR)
        public int? SSBSECT_CREDIT_HRS { get; set; }                    //NUMBER(7,3)
        public int? SSBSECT_BILL_HRS { get; set; }                      //NUMBER(7,3)
        public string SSBSECT_GMOD_CODE { get; set; }                   //VARCHAR2(30 CHAR)
        public string STVGMOD_DESC { get; set; }                        //VARCHAR2(1 CHAR)
        public string SSBSECT_SESS_CODE { get; set; }                   //VARCHAR2(1 CHAR)
        public int? IS_SECTION_LINKED { get; set; }                     //NUMBER
        public string SSBSECT_GRADABLE_IND { get; set; }                //VARCHAR2(1 CHAR)
        public string SSBSECT_TUIW_IND { get; set; }                    //VARCHAR2(1 CHAR)
        public int SSBSECT_REG_ONEUP { get; set; }                      //NOT NULL NUMBER(4)
        public int SSBSECT_MAX_ENRL { get; set; }                       //NOT NULL NUMBER(4)
        public int SSBSECT_ENRL { get; set; }                           //NOT NULL NUMBER(4)
        public int SSBSECT_SEATS_AVAIL { get; set; }                    //NOT NULL NUMBER(4)
        public DateTime SSBSECT_PTRM_START_DATE { get; set; }           //DATE
        public DateTime SSBSECT_PTRM_END_DATE { get; set; }             //DATE
        public int? SSBSECT_PTRM_WEEKS { get; set; }                    //NUMBER(3)
        public int? SSBSECT_WAIT_CAPACITY { get; set; }                 //NUMBER(4)
        public int? SSBSECT_WAIT_COUNT { get; set; }                    //NUMBER(4)
        public int? SSBSECT_WAIT_AVAIL { get; set; }                    //NUMBER(4)
        public int? SSBSECT_LEC_HR { get; set; }                        //NUMBER(9,3)
        public int? SSBSECT_LAB_HR { get; set; }                        //NUMBER(9,3)
        public int? SSBSECT_OTH_HR { get; set; }                        //NUMBER(9,3)
        public int? SSBSECT_CONT_HR { get; set; }                       //NUMBER(9,3)
        public string SSBSECT_CAPP_PREREQ_TEST_IND { get; set; }        //NOT NULL VARCHAR2(1 CHAR)
        public string SSBSECT_INSM_CODE { get; set; }                   //VARCHAR2(5 CHAR)
        public string GTVINSM_DESC { get; set; }                        //VARCHAR2(30 CHAR)
        public DateTime SSBSECT_REG_FROM_DATE { get; set; }             //DATE
        public DateTime SSBSECT_REG_TO_DATE { get; set; }               //DATE
        public DateTime SSBSECT_LEARNER_REGSTART_FDATE { get; set; }    //DATE
        public DateTime SSBSECT_LEARNER_REGSTART_TDATE { get; set; }    //DATE
        public string SSBSECT_DUNT_CODE { get; set; }                   //VARCHAR2(4 CHAR)
        public string GTVDUNT_DESC { get; set; }                        //VARCHAR2(30 CHAR)
        public int? SSBSECT_NUMBER_OF_UNITS { get; set; }               //NUMBER(7,2)
        public string SSBSECT_PREREQ_CHK_METHOD_CDE { get; set; }       //NOT NULL VARCHAR2(1 CHAR)
        public string SSBSECT_USER_ID { get; set; }                     //VARCHAR2(30 CHAR)
        public string SSBSECT_DATA_ORIGIN { get; set; }                 //VARCHAR2(30 CHAR)
        public string SSBSECT_CRSE_TITLE { get; set; }                  //VARCHAR2(30 CHAR)
        public string SSBSECT_SAPR_CODE { get; set; }                   //VARCHAR2(2 CHAR)
        public string STVSAPR_DESC { get; set; }                        //VARCHAR2(30 CHAR)
        public string CROSS_LIST { get; set; }                          //VARCHAR2(2 CHAR)
        public int? SCBCRSE_CREDIT_HR_LOW { get; set; }                 //NUMBER(7,3)
        public string SCBCRSE_CREDIT_HR_IND { get; set; }               //VARCHAR2(2 CHAR)
        public int? SCBCRSE_CREDIT_HR_HIGH { get; set; }                //NUMBER(7,3) SCBCRSE_CREDIT_HR_HIGH
        public string COURSE_TITLE { get; set; }                        //VARCHAR2(100CHAR)
        public string SUBJECT_COURSE { get; set; }                      //VARCHAR2(9 CHAR)
        public string SCBCRSE_COLL_CODE { get; set; }                   //VARCHAR2(2 CHAR)
        public string SCBCRSE_DEPT_CODE { get; set; }                   //VARCHAR2(4 CHAR)
        public string OPENSECTION { get; set; }                         //VARCHAR2(1 CHAR)
        public string SCBCRSE_EFF_TERM { get; set; }                    //VARCHAR2(6 CHAR)
        public string SSBSECT_KEYWORD_INDEX_ID { get; set; }            //VARCHAR2(30)

    }
}
