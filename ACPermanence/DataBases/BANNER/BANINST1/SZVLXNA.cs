﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.BANNER.BANINST1
{
    [Table("BANINST1.SZVLXNA")]
    public class SZVLXNA
    {
        [Key]
        [Column(Order = 1)]
        public string TERM { get; set; }       //NOT NULL VARCHAR2(6 CHAR)
        [Key]
        [Column(Order = 2)]
        public string NRCHIJO { get; set; }    //NOT NULL VARCHAR2(5 CHAR)
        public string NRCPADRE { get; set; }   //NOT NULL VARCHAR2(5 CHAR)
    }
}
