﻿namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    /// <summary>
    /// Stored Procedure: BANINST1.SZKPCAU.P_CICLO_PRIMERAMATRICULA
    /// </summary>
    public class PCicloPrimeraMatricula
    {
        public string TERM { get; set; }
        public string DEPARTAMENT { get; set; }
        public string PROGRAM { get; set; }
        public string LEVL { get; set; }
        public decimal PIDM { get; set; }
        public string CAMPUS { get; set; }
        public string TERMCATALOG { get; set; }
        public int REALCREDITS { get; set; }
        public int ACTUALCYCLE { get; set; }
        public int ENROLLEMENTCREDITS { get; set; }
        public int TOTALCREDITS { get; set; }
        public int NOMINALCYCLE { get; set; }    
    }
}