﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class PHistoriaTotalxAlumnoEquiv
    {
        public string DIVS { get; set; }
        public string CAMPUS { get; set; }
        public string LEVL { get; set; }
        public string PROGRAM { get; set; }
        public string TERM { get; set; }
        public decimal PIDM { get; set; }
        public string DEPARTAMENT { get; set; }
        public string NRC { get; set; }
        public string SUBJECT { get; set; }
        public string COURSE { get; set; }
        public string NAMECOURSE { get; set; }
        public int CREDIT { get; set; }
        // public int SCORE { get; set; }
        public string ELECTIVE { get; set; }
        public string CYCLE { get; set; }
        public string APROVED { get; set; }
        public string TYPEORIGIN { get; set; }
        public string TERMCATALOG { get; set; }
        //public float PGA { get; set; }
    }
}
