﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class P_ESTADOXALUMNO
    {
        public int sgbstdn_pidm { get; set; }
        public string sgbstdn_stst_code { get; set; }
        public string sgbstdn_term_code_eff { get; set; }

    }
}
