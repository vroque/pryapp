﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class P_INASISTENCIAXALUMNO
    {
        public double pidm { get; set; }
        public string term { get; set; }
        public string partterm { get; set; }
        public string campus { get; set; }
        public string departament { get; set; }
        public string program { get; set; }
        public string nrc { get; set; }
        public string subject { get; set; }
        public string course { get; set; }
        public string namecourse { get; set; }
        public DateTime absencedate { get; set; }
        public string percentageinasistence { get; set; }
    }
}
