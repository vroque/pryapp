﻿using System;

namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class PPostulacionesXAlumno
    {
        public string DIVS { get; set; }
        public string CAMPUS { get; set; }
        public string TERM { get; set; }
        public string LEVL { get; set; }
        public string COLLEGE { get; set; }
        public string PROGRAM { get; set; }
        public string DEPARTMENT { get; set; }
        public string ADMTYPE { get; set; }
        public decimal PIDM { get; set; }
        public string STATUS { get; set; }
        public DateTime REGISTRATIONDATE { get; set; }
        /// <summary>
        /// ('1' actual , '0' historico)
        /// </summary>
        public string CURRENTSTATE { get; set; }
        public DateTime UPDATEDATE { get; set; }
    }
}
