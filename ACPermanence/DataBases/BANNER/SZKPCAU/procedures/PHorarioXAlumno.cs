﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class PHorarioXAlumno
    {
        public string divs { get; set; } //               CHAR(4)            
        public string levl { get; set; } //               VARCHAR2(2 CHAR)   
        public string term { get; set; } //               VARCHAR2(6 CHAR)   
        public string program { get; set; } //            VARCHAR2(12 CHAR)  
        public string course { get; set; } //             VARCHAR2(5 CHAR)   
        public string courseName { get; set; } //         VARCHAR2(100 CHAR) 
        public string nrc { get; set; } //                VARCHAR2(5 CHAR)   
        public string beginTime { get; set; } //          VARCHAR2(5 CHAR)   
        public string endTime { get; set; } //            VARCHAR2(5 CHAR)   
        public string sunday { get; set; } //             CHAR(1)            
        public string monday { get; set; } //             CHAR(1)            
        public string tuesday { get; set; } //            CHAR(1)            
        public string wednesday { get; set; } //          CHAR(1)            
        public string thursday { get; set; } //           CHAR(1)            
        public string friday { get; set; } //             CHAR(1)            
        public string saturday { get; set; } //           CHAR(1)            
        public decimal? pidmTeacher { get; set; } //        NUMBER             
        public string nameTeacher { get; set; } //        VARCHAR2(482)      
        public decimal pidmStudent { get; set; } //        NUMBER(8)          
        public string hallClass { get; set; } //          VARCHAR2(30 CHAR)  
        public string classroom { get; set; } //          VARCHAR2(10 CHAR)  
        public string type { get; set; } //               VARCHAR2(30 CHAR)  
        public string module { get; set; } //             CHAR(5)            
        public string departament { get; set; } //        CHAR(4)
        public DateTime dateStart { get; set; } //        CHAR(4)
        public DateTime dateEnd { get; set; } //        CHAR(4)
    }
}
