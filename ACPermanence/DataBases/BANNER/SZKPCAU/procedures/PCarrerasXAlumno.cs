﻿namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class PCarrerasXAlumno
    {
        public decimal PIDM { get; set; }
        public string TERMSTART { get; set; }
        public string LEVL { get; set; }
        public string COLLEGE { get; set; }
        public string TERMCATALOG { get; set; }
        public string TERMEND { get; set; }
        public string TERMADM { get; set; }
        public string CAMPUS { get; set; }
        public string PROGRAM { get; set; }
        public string DEPARTAMENT { get; set; }
    }
}
