﻿namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class P_ASIGNATURASPROY
    {
        public int pidm { get; set; }
        public string term { get; set; }
        public string program { get; set; }
        public string departament { get; set; }
        public string campus { get; set; }
        public string cycle { get; set; }
        public string subject { get; set; }
        public string course { get; set; }
        public string nameCourse { get; set; }
        public int? credits { get; set; }
        public string MOST_PROB { get; set; }
        public string ELECTIVE { get; set; }
    }
}
