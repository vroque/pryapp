﻿using System;

namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class PRequisitosXAlumno
    {
        public string DIVS { get; set; }
        public string CAMPUS { get; set; }
        public string TERM { get; set; }
        public string DEPARTAMENT { get; set; }
        public string PROGRAM { get; set; }
        public string COLLEGE { get; set; }
        public string LEVL { get; set; }
        public decimal PIDM { get; set; }
        public string CODEREQUERIMENT { get; set; }
        public string REQUERIMENTDESCRIPTION { get; set; }

        /// <summary>
        /// estado de entrega de requerimiento 
        /// ('0' no entregado, '1' entregado )  
        /// </summary>
        public string DELIVERED { get; set; }

        /// <summary>
        /// Documento obligatorio
        /// '0' no obligatorio, '1' obligatorio
        /// </summary>
        public string REQUIRED { get; set; }

        /// <summary>
        /// Prórroga
        /// '0' sin prórroga, '1' con prórroga
        /// </summary>
        public string EXTENSION { get; set; }

        public DateTime UPDATEDATE { get; set; }
    }
}