﻿namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class PCatalogoPersonzalizado
    {
        public string ELECTIVE { get; set; }
        public string AREA { get; set; }
        public string DEPARTAMENT { get; set; }
        public string PROGRAM { get; set; }
        public string CAMPUS { get; set; }
        public string TERMCATALOG { get; set; }
        public string RULE { get; set; }
        public string SUBJECT { get; set; }
        public string COURSE { get; set; }
        public string NAMECOURSE { get; set; }
        public int? CREDITS { get; set; }
        public string CYCLE { get; set; }
        public string PREQSUBJECT { get; set; }
        public string PREQCOURSE { get; set; }
        public string PREQNAME { get; set; }
        public decimal? PREREQCRED { get; set; }
    }
}
