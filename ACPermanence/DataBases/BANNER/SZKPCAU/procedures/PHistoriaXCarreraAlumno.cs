﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class PHistoriaXCarreraAlumno
    {
        public string DIVS { get; set; }
        public string CAMPUS { get; set; }
        public string LEVL { get; set; }
        public string PROGRAM { get; set; }
        public string TERM { get; set; }
        public decimal PIDM { get; set; }
        public string DEPARTAMENT { get; set; }
        public string NRC { get; set; }
        public string RULE { get; set; }
        public string SUBJECT { get; set; }
        public string COURSE { get; set; }
        public string NAMECOURSE { get; set; }
        public double CREDIT { get; set; }
        public string SCORE { get; set; }
        /// <summary>
        /// electivo = '1' y no electivo ='0'
        /// </summary>
        public string ELECTIVE { get; set; }
        public string CYCLE { get; set; }
        // FIXME
        public DateTime? DATEFINALSCORE { get; set; }
        /// <summary>
        /// determina si es asignatura aprobada (aprobada='1' y desaprobada ='0')
        /// </summary>
        public string APROVED { get; set; }
        public string TYPEORIGIN { get; set; }
        /// <summary>
        /// determina el (tipo de origen de datos
        /// (CU--> cap usado osea cumplido ;
        /// CNU--> CAP no usado osea no cumplido) 
        /// </summary>
        public string TERMCATALOG { get; set; }
        public double PGA { get; set; }
        public string ACTIVE { get; set; }
    }
}
