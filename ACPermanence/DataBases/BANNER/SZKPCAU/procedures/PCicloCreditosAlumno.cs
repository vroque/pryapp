﻿namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class PCicloCreditosAlumno
    {
        public string TERM { get; set; }
        public string DEPARTMENT { get; set; }
        public string PROGRAM { get; set; }
        public string LEVL { get; set; }

        public decimal PIDM { get; set; }

        //public string PGA { get; set; }
        public string TERMCATALOG { get; set; }
        public int REALCREDITS { get; set; }
        public int ENROLLEMENTCREDITS { get; set; }
        public int TOTALCREDITS { get; set; }
        public int? ACTUALCYCLE { get; set; }
        public int? NOMINALCYCLE { get; set; }
    }
}