﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class P_NRCSPADREPROY
    {
        public string term { get; set; }
        public string campus { get; set; }
        public string departament { get; set; }
        public string partterm { get; set; }
        public string program { get; set; }
        public string nrc { get; set; }
        public string idseccion { get; set; }
        public string subject { get; set; }
        public string course { get; set; }
        public string namecourse { get; set; }
        public int? credits { get; set; }
        public string nrcpadre { get; set; }
        public int nroligas { get; set; }
        public string codliga { get; set; }
        public int? maxvacancies { get; set; }
        public int? vacanciesavailable { get; set; }
        public int? pidmteacher { get; set; }
        public string idteacher { get; set; }
        public string teacher { get; set; }
    }
}
