﻿using System;
namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class P_RETENCIONESXALUMNOMATRI123
    {
        public int PIDM { get; set; }
        public string HLDD_CODE { get; set; }
        public DateTime FROM_DATE { get; set; }
        public DateTime TO_DATE { get; set; }
        public string REASON { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION2 { get; set; }
        public int SURROGATE_ID { get; set; }
    }
}
