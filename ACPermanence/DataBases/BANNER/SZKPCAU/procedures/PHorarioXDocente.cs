﻿namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class PHorarioXDocente
    {
        public string divs { get; set; }
        public string levl { get; set; }
        public string term { get; set; }
        public string program { get; set; }
        public string course { get; set; }
        public string courseName { get; set; }
        public string nrc { get; set; }
        public string beginTime { get; set; }
        public string endTime { get; set; }
        public string sunday { get; set; }
        public string monday { get; set; }
        public string tuesday { get; set; }
        public string wednesday { get; set; }
        public string thursday { get; set; }
        public string friday { get; set; }
        public string saturday { get; set; }
        public decimal pidmTeacher { get; set; }
        public string nameTeacher { get; set; }
        public string hallclass { get; set; }
        public string classroom { get; set; }
        public string type { get; set; }
    }
}