﻿namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    /// <summary>
    /// Stored Procedure: BANINST1.SZKPCAU.P_MOVIMIENTOSESTUDIANTE
    /// </summary>
    public class P_MOVIMIENTOSESTUDIANTE
    {
        public decimal NUMERO { get; set; }

        /// <summary>
        /// Código de Estudiante
        /// </summary>
        public string CODIGO { get; set; }

        /// <summary>
        /// PIDM
        /// </summary>
        public decimal PIDM { get; set; }

        /// <summary>
        /// Código Campus (formerly IDSede)
        /// </summary>
        public string CODIGO_CAMPUS { get; set; }

        /// <summary>
        /// Nombre Campus
        /// </summary>
        public string DESCRIPCION_CAMPUS { get; set; }

        /// <summary>
        /// Código de Facultad
        /// </summary>
        public string CODIGO_FACULTAD { get; set; }

        /// <summary>
        /// Nombre Facultad
        /// </summary>
        public string DESCRIPCION_FACULTAD { get; set; }

        /// <summary>
        /// Código Escuela Académica (formerly IDEscuela)
        /// </summary>
        public string CODIGO_PROGRAMA { get; set; }

        /// <summary>
        /// Nombre Escuela Académica
        /// </summary>
        public string DESCRIPCION_PROGRAMA { get; set; }

        /// <summary>
        /// Código Modalidad (formerly IDEscuelaADM)
        /// </summary>
        public string CODIGO_DEPARTAMENTO { get; set; }

        /// <summary>
        /// Nombre de Modalidad
        /// </summary>
        public string DESCRIPCION_DEPARTAMENTO { get; set; }

        public string CATALOGO_REAL { get; set; }
        public string CATALOGO { get; set; }
        public string PERIODO_INICIO { get; set; }
        public string PERIODO_FIN { get; set; }

        /// <summary>
        /// formerly IDModalidadPostu
        /// </summary>
        public string CODIGO_MOD_ADM { get; set; }
    }
}