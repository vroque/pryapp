﻿using System;
namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class P_PERIODOMATRIACTIVOXALUM
    {
        public int PIDM { get; set; }
        public string TERM_ENROLLMENT { get; set; }
        public string TERM { get; set; }
    }
}
