﻿using System;
namespace ACPermanence.DataBases.BANNER.SZKPCAU.procedures
{
    public class P_GRUPOINSXALUMNO
    {
        public int SFBRGRP_PIDM { get; set; }
        public string SFBRGRP_TERM_CODE { get; set; }
        public string SFBRGRP_RGRP_CODE { get; set; }
        public int SFBWCTL_PRIORITY { get; set; }
        public DateTime SFRWCTL_BEGIN_DATE { get; set; }
        public DateTime SFRWCTL_END_DATE { get; set; }
        public int ENROLLMENT{ get; set; }
    }
}
