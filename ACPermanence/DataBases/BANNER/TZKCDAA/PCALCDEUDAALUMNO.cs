﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.BANNER.TZKCDAA
{
    public class PCALCDEUDAALUMNO
    {
        public string SessionID {get; set;}
        public decimal PIDM {get; set;}
        public string Anio {get; set;}
        public string Periodo {get; set;}
        public string IDConcepto {get; set;}
        public string Descripcion {get; set;}
        public string NumDocumento {get; set;}
        public decimal Deuda { get; set; }
        public decimal Mora { get; set; }
        public decimal Multa { get; set; }
        public DateTime FechaFin {get; set;}
        public DateTime FechaInicio { get; set; }

    }
}
