﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.OPP
{
    [Table("OPP.TblPracticeDocument")]
    public class OPPTblPracticeDocument
    {
        [Key]
        public int practiceDocumentID { get; set; }
        public string practiceID { get; set; }
        public string studentID { get; set; }
        public string div { get; set; }
        public string lvl { get; set; }
        public string program { get; set; }
        public decimal pidm { get; set; }
        public string campus { get; set; }
        public string user { get; set; }
        public DateTime createDate { get; set; }
        public DateTime updateDate { get; set; }
        
    }
}
