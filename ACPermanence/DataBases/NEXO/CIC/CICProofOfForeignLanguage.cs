﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CIC
{
    /// <summary>
    /// Estudiantes que pueden solicitar CAIE
    /// </summary>
    [Table("CIC.ProofOfForeignLanguage")]
    public class CICProofOfForeignLanguage
    {
        [Key]
        [Column(Order = 1)]
        public decimal Pidm { get; set; }
        [Key]
        [Column(Order = 2)]
        public string PersonId { get; set; }
        [Key]
        [Column(Order = 3)]
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string Cycle { get; set; }
        public DateTime StartDateAtCic { get; set; }
        public string ModalityId { get; set; }
        public string ModalityName { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
