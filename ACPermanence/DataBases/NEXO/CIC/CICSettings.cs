﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CIC
{
    [Table("CIC.Settings")]
    public class CICSettings
    {
        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }
        public string Abbr { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SettingTypeId { get; set; }
        public bool Active { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}