﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CIC
{
    /// <summary>
    /// Información de Constancias, Certificados, Diplomas, etc. emitidos por el CIC
    /// </summary>
    [Table("CIC.Certificates")]
    public class CICCertificates
    {
        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }
        public int CertificateId { get; set; }
        [Key]
        [Column(Order = 3)]
        public string DocumentId { get; set; }
        public int? RequestId { get; set; }
        public decimal Pidm { get; set; }
        public string StudentId { get; set; }
        public string LanguageId { get; set; }
        public int? LanguageCycle { get; set; }
        public string LanguageLevelId { get; set; }
        public decimal? TotalHoursStudied { get; set; }
        public decimal? Average { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Schedule { get; set; }
        public DateTime GenerationDate { get; set; }
        public decimal GenerationUserPidm { get; set; }
        public string Comment { get; set; }
        public DateTime? LastModificationDate { get; set; }
        public decimal? LastModificationUserPidm { get; set; }
        public bool Printed { get; set; }
        public bool Delivered { get; set; }
        public DateTime? DeliveredDate { get; set; }
        public string Campus { get; set; }
        public int? IdDocuVenta { get; set; }
        public string DataOrigin { get; set; }
    }
}