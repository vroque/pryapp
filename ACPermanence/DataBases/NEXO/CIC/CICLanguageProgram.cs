using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CIC
{
    [Table("CIC.LanguageProgram")]
    public class CICLanguageProgram
    {
        [Key, Column(Order = 1)]
        public int Id { get; set; }
        public string LanguageProgramName { get; set; }
        public string Description { get; set; }
    }
}
