﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CIC
{
    /// <summary>
    /// Documentos que emite el CIC
    /// </summary>
    [Table("CIC.Documents")]
    public class CICDocuments
    {
        [Key]
        [Column(Order = 1)]
        public string DocumentId { get; set; }
        public string DocumentName { get; set; }
    }
}