﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CIC
{
    /// <summary>
    /// Tabla con info sobre los Niveles de Idiomas del CIC
    /// </summary>
    [Table("CIC.LanguageLevel")]
    public class CICLanguageLevel
    {
        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }
        public string LanguageLevelId { get; set; }
        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string LanguageLevelName { get; set; }
        public string LangName { get; set; }
        public string LangLevelName { get; set; }
        public int LanguageProgramId { get; set; }
        public int StartCycle { get; set; }
        public int FinalCycle { get; set; }
        public int TotalCycles { get; set; }
        public string Curricula { get; set; }
        public bool Active { get; set; }
    }
}
