﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CIC.Views
{
    /// <summary>
    /// Información de Nivel de Idiomas de Estudiantes
    /// </summary>
    [Table("CIC.StudentLevel")]
    public class CICStudentLevel
    {
        [Key]
        [Column(Order = 1)]
        public decimal Pidm { get; set; }
        [Key]
        [Column(Order = 2)]
        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string LastCycle { get; set; }
    }
}