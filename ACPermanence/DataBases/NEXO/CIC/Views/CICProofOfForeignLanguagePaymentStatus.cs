﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CIC.Views
{
    /// <summary>
    /// Estado de pago para continuar con el proceso de generación de CAIE
    /// </summary>
    [Table("CIC.ProofOfForeignLanguagePaymentStatus")]
    public class CICProofOfForeignLanguagePaymentStatus
    {
        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }
        public decimal Pidm { get; set; }
        public string PersonId { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string Cycle { get; set; }
        public DateTime StartDateAtCic { get; set; }
        public string ModalityId { get; set; }
        public string ModalityName { get; set; }
        public int PaymentStatus { get; set; }
        public string GraduationPeriod { get; set; }
        public string GraduationSchoolId { get; set; }
        public string GraduationModalityId { get; set; }
    }
}
