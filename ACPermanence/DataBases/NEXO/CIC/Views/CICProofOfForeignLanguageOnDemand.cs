﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CIC.Views
{
    /// <summary>
    /// Data temporal para la generación de CAIE a demanda
    /// </summary>
    [Table("CIC.ProofOfForeignLanguageOnDemand")]
    public class CICProofOfForeignLanguageOnDemand
    {
        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }
        public int Pidm { get; set; }
        public string PersonId { get; set; }
        public string StudentId { get; set; }
        public string StudentCicId { get; set; }
        public string StudentName { get; set; }
        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string Cycle { get; set; }
        public DateTime StartDateAtCic { get; set; }
        public string ModalityId { get; set; }
        public string ModalityName { get; set; }
        public int PaymentStatus { get; set; }
        public int Printable { get; set; }
    }
}