﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CIC
{
    /// <summary>
    /// Matriculados en el CIC
    /// </summary>
    [Table("CIC.Enrolled")]
    public class CICEnrolled
    {
        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }
        public decimal Pidm { get; set; }
        public string PersonId { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string AcademicPeriod { get; set; }
        public int Cycle { get; set; }
        public string Classroom { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? EnrollmentDate { get; set; }
        public string TypeOfStudent { get; set; }
        public string LanguageProgram { get; set; }
        public string LastApprovedCycle { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}