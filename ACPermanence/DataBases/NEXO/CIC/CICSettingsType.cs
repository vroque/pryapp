﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CIC
{
    /// <summary>
    /// Tipo de configuración
    /// </summary>
    [Table("CIC.SettingsType")]
    public class CICSettingsType
    {
        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }
        public int Name { get; set; }
        public string Description { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}