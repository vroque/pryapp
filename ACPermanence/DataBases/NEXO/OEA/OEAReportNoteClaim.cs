﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.OEA
{
    [Table("OEA.ReportNoteClaim")]
    public class OEAReportNoteClaim
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public long NoteClaimId { get; set; }
        public bool Proceeds { get; set; }
        public string ScoreCorrect { get; set; }
        public string Reason { get; set; }
        public DateTime? DateChange { get; set; }
        public long Status { get; set; }
        public bool Active { get; set; }
        public bool? Approved { get; set; }
        public decimal Author { get; set; }
        public DateTime DateCreated { get; set; }
        public decimal? ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string Comment { get; set; }
    }
}
