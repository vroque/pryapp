﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.OEA
{
    [Table("OEA.NrcProgrammned")]
    public class OEANrcProgrammned
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public long NrcForSubstituteExamId { get; set; }
        public DateTime InicialRegistrationDate { get; set; }
        public DateTime FinalRegistrationDate { get; set; }
        public DateTime ExamDate { get; set; }
        public bool Active { get; set; }
        public string ClassRoom { get; set; }
        public bool IsPublic { get; set; }
        public string Author { get; set; }
        public DateTime DateCreated { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string Comment { get; set; }
    }
}
