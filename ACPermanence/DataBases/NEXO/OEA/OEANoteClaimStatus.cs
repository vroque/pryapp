﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.OEA
{
    [Table("OEA.NoteClaimStatus")]
    public class OEANoteClaimStatus
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public string Description { get; set; }
        public bool IsReport { get; set; }
    }
}
