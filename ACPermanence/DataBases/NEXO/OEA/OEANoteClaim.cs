﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.OEA
{
    [Table("OEA.NoteClaim")]
    public class OEANoteClaim
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public int Year { get; set; }
        public int NoteClaimNumber { get; set; }
        public long SubjectId { get; set; }
        public string Type { get; set; }
        public long Status { get; set; }
        public decimal PidmStudent { get; set; }
        public decimal PidmTeacher { get; set; }
        public string ProgramId { get; set; }
        public int Component { get; set; }
        public int? SubComponent { get; set; }
        public string ScoreClaim { get; set; }
        public string Reason { get; set; }
        public bool HasPhysicalEvidence { get; set; }
        public long OfficeId { get; set; }
        public bool Active { get; set; }
        public decimal Author { get; set; }
        public DateTime DateCreated { get; set; }
        public decimal? ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string Comment { get; set; }
    }
}
