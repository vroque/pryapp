﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.OEA
{
    [Table("OEA.SubstituteExamProgrammed")]
    public class OEASubstituteExamProgrammed
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public long NrcProgrammnedId { get; set; }
        public decimal Pidm { get; set; }
        public int ProgramId { get; set; }
        public string Justification { get; set; }
        public bool Active { get; set; }
        public string Author { get; set; }
        public DateTime DateCreated { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string Comment { get; set; }
    }
}
