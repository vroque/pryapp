﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.OEA
{
    [Table("OEA.FileNoteClaim")]
    public class OEAFileNoteClaim
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public long NoteClaimId { get; set; }
        public long? ReportNoteClaimId { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
    }
}
