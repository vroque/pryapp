﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.OEA
{
    [Table("OEA.NrcForSubstituteExam")]
    public class OEANrcForSubstituteExam
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public string Department { get; set; }
        public string Campus { get; set; }
        public string Term { get; set; }
        public string SubjetID { get; set; }
        public string Credits { get; set; }
        public string Section { get; set; }
        public string Nrc { get; set; }
        public string PartTerm { get; set; }
        public string Module { get; set; }
        public string SubjetName { get; set; }
        public bool Active { get; set; }
        public bool HasSusti { get; set; }
        public string Author { get; set; }
        public DateTime DateCreated { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string Comment { get; set; }
    }
}
