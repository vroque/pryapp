﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC
{
    /// <summary>
    /// Tabla de ejes
    /// </summary>
    [Table("[VUC].[tblAxi]")]
    public class VUCAxi
    {
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del eje
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripcion del eje
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ruta de la imagen almacenada en el servidor de aplicaciones
        /// </summary>
        public string ImagePath { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }
}