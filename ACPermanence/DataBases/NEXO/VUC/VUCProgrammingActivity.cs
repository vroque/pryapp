﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC
{
    /// <summary>
    /// Tabla de las programaciones de la actividad.
    /// </summary>
    [Table("[VUC].[tblProgrammingActivity]")]
    public class VUCProgrammingActivity
    {
        /// <summary>
        /// Id de la programación de actividad
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id de la actividad a la que pertenece
        /// </summary>
        public int IdActivity { get; set; }

        /// <summary>
        /// Fecha de inicio de la inscripción
        /// </summary>
        public DateTime StartDateEnrollment { get; set; }

        /// <summary>
        /// Fecha de fin de la inscripción
        /// </summary>
        public DateTime EndDateEnrollment { get; set; }

        /// <summary>
        /// Fecha de inicio de la actividad
        /// </summary>
        public DateTime StartDateActivity { get; set; }

        /// <summary>
        /// Fecha de fin de la actividad
        /// </summary>
        public DateTime EndDateActivity { get; set; }

        /// <summary>
        /// Costo para esta programación de actividad
        /// </summary>
        public int Cost { get; set; }

        /// <summary>
        /// Moneda del costo para esta programación de actividad
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Perfil de estudiante de la programación de actividad
        /// </summary>
        public string StudentProfile { get; set; }

        /// <summary>
        /// Período en el que inicia la programación de actividad
        /// </summary>
        public string StartPeriod { get; set; }

        /// <summary>
        /// Período en el que finaliza la programación de actividad
        /// </summary>
        public string EndPeriod { get; set; }

        /// <summary>
        /// Id del tipo de horario que tiene la programación de actividad
        /// </summary>
        public string IdScheduleType { get; set; }

        /// <summary>
        /// Descripción del horario, para tipo: Descriptivo
        /// </summary>
        public string ScheduleDescription { get; set; }

        /// <summary>
        /// Día de sesión, para tipo: No descriptivo
        /// </summary>
        public string SessionDay { get; set; }

        /// <summary>
        /// Hora de inicio, para tipo: No descriptivo
        /// </summary>
        public string StartHour { get; set; }

        /// <summary>
        /// Hora de fin, para tipo: No descriptivo
        /// </summary>
        public string EndHour { get; set; }

        /// <summary>
        /// Lugar de sesión, para tipo: No descriptivo
        /// </summary>
        public string SessionPlace { get; set; }

        /// <summary>
        /// Cantidad de vacantes para la programación de actividad
        /// </summary>
        public int QuantityVacancy { get; set; }

        /// <summary>
        /// Cantidad máxima de vacantes para la programación de actividad
        /// </summary>
        public int MaxQuantityVacancy { get; set; }

        /// <summary>
        /// DNI del docente de la programación de actividad
        /// </summary>
        public string DocumentTeacher { get; set; }

        /// <summary>
        /// Nombre de docente responsable de la actividad
        /// </summary>
        public string NameTeacher { get; set; }

        /// <summary>
        /// Id del campus al que pertenece
        /// </summary>
        public string IdCampus { get; set; }

        /// <summary>
        /// Id de la modalidad a la que pertenece
        /// </summary>
        public string IdModality { get; set; }

        /// <summary>
        /// Estado de replicado en Banner (Replicado = True/No replicado = False)
        /// </summary>
        public bool Replicated { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }

        /// <summary>
        /// Nrc de la sección
        /// </summary>
        public string nrc { get; set; }
    }
}