﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC
{
    /// <summary>
    /// Tabla donde se regitra el seguimiento de los programas
    /// </summary>
    [Table("[VUC].[tblTracingProgram]")]
    public class VUCTracingProgram
    {
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id de la programación de la actividad
        /// </summary>
        public int IdProgrammingActivity { get; set; }

        /// <summary>
        /// Pidm del estudiante
        /// </summary>
        public decimal PidmStudent { get; set; }

        /// <summary>
        /// Horas que participo en el programa
        /// </summary>
        public short Hours { get; set; }

        /// <summary>
        /// Calificacion final
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// Observaciones
        /// </summary>
        public string Observation { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }
}
