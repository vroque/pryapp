﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC
{
    /// <summary>
    /// Tabla intermedia entre estudiantes y reconocimientos
    /// </summary>
    [Table("[VUC].[tblRecognitionStudent]")]
    public class VUCRecognitionStudent
    {
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id del reconocimiento
        /// </summary>
        public int IdRecognition { get; set; }

        /// <summary>
        /// Pidm del estudiante
        /// </summary>
        public decimal PidmStudent { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }
}
