﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC
{
    /// <summary>
    /// Tabla de los inscritos a las actividades, tanto para los talleres
    /// y programas.
    /// </summary>
    [Table("[VUC].[tblEnrolledProgramActivity]")]
    public class VUCEnrolledProgramActivity
    {
        /// <summary>
        /// Id de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id de la tabla [VUC].[tblActivityProgramming]
        /// </summary>
        public int IdProgramActivity { get; set; }

        /// <summary>
        /// Ciclo del estudiante
        /// </summary>
        public int Cycle { get; set; }

        /// <summary>
        /// Plan de estudios del estudiante
        /// </summary>
        public string Curriculum { get; set; }

        /// <summary>
        /// Campus
        /// </summary>
        public string IdCampus { get; set; }

        /// <summary>
        /// Modalidad
        /// </summary>
        public string IdModality { get; set; }

        /// <summary>
        /// ID del concepto de la cuenta corriente
        /// </summary>
        public string IDConcepto { get; set; }

        /// <summary>
        /// Dependencia
        /// </summary>
        public string IDDependencia { get; set; }

        /// <summary>
        /// Periodo académico
        /// </summary>
        public string Term { get; set; }

        /// <summary>
        /// Fecha de inscripcion
        /// </summary>
        public DateTime? DateEnrolled { get; set; }

        /// <summary>
        /// Pidm del estudiante
        /// </summary>
        public decimal PidmStudent { get; set; }

        /// <summary>
        /// Fecha de confirmacion (para los programas)
        /// </summary>
        public DateTime? DateConfirmed { get; set; }

        /// <summary>
        /// Carrera
        /// </summary>
        public string EAP { get; set; }

        /// <summary>
        /// Estado de confirmado (si fue seleccionado, para los programas)
        /// </summary>
        public bool Confirmed { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }
}
