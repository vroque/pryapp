﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC
{
    [Table("[VUC].[tblActivityConvalidation]")]
    public class VUCActivityConvalidation
    {
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id de la actividad
        /// </summary>
        public int IdActivity { get; set; }

        /// <summary>
        /// Id del tipo de convalidacion
        /// </summary>
        public int IdConvalidationType { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }
}
