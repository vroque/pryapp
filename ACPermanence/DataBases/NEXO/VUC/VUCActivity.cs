﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC
{
    /// <summary>
    /// Tabla de actividades extracurriculares
    /// </summary>
    [Table("[VUC].[tblActivity]")]
    public class VUCActivity
    {

        /// <summary>
        /// Identicador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre corto, menor a 30 caracteres
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Nombre largo de la actividad
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripcion de la actividad
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Otra informacion relevante
        /// </summary>
        public string Other { get; set; }

        /// <summary>
        /// Cantidad de creditos que acredita
        /// </summary>
        public byte Credits { get; set; }

        /// <summary>
        /// Ruta de la imagen almacenada en el servidor de aplicaciones
        /// </summary>
        public string ImagePath { get; set; }

        /// <summary>
        /// Id del tipo de actividad (taller o actividad)
        /// </summary>
        public int IdActivityType { get; set; }

        /// <summary>
        /// Id del eje al que pertenece
        /// </summary>
        public int IdAxi { get; set; }

        /// <summary>
        /// Estado que identifica si se envio a replicar a banner
        /// </summary>
        public bool IsReplicated { get; set; }

        /// <summary>
        /// Codigo de banner
        /// </summary>
        public string BannerCode { get; set; }

        /// <summary>
        /// Estado para mostrar en la programacion de actividades
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }
}