﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC
{
    [Table("[VUC].[tblScheduleType]")]
    public class VUCScheduleType
    {
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Nombre del tipo de horario (descriptivo y  no descriptivo)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }
    }
}
