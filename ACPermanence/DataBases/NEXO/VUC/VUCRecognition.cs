﻿using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace ACPermanence.DataBases.NEXO.VUC
{
    /// <summary>
    /// Tabla de reconocimientos, donde el usuario ingresa si acredita con proyección social
    /// o créditos extracurriculares
    /// </summary>
    [Table("[VUC].[tblRecognition]")]
    public class VUCRecognition
    {
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Fecha del reconocimiento
        /// </summary>
        public DateTime? RecognitionDate { get; set; }

        /// <summary>
        /// Motivo del reconocimiento
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Descripción del reconocimiento
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ruta donde se almacenó las evidencias
        /// </summary>
        public string PathEvidency { get; set; }

        /// <summary>
        /// Tipo de acreditación (extracurricular o proyección social)
        /// </summary>
        public int IdConvalidationType { get; set; }

        /// <summary>
        /// Si es extracurricular, la cantidad de créditos que acredita
        /// </summary>
        public byte Credit { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }
}
