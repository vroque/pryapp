﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC
{
    [Table("[VUC].[tblProgrammingActivityCurriculum]")]
    public class VUCProgrammingActivityCurriculum
    {
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id de la programación de una actividad
        /// </summary>
        public int IdProgrammingActivity { get; set; }

        /// <summary>
        /// Id del tipo de plan de estudios
        /// </summary>
        public string IdCurriculum { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Fecha en la que se creó el registro
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Fecha en la que se actualizó el registro
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }
}

