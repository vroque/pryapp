﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC.views
{
    [Table("VUC.v_Curriculum")]
    public class VUCv_Curriculum
    {
        [Key]
        public string Curriculum { get; set; }
    }
}
