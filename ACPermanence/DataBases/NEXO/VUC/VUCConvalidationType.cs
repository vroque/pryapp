﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC
{
    /// <summary>
    /// Tabla de los tipos de convalidaciones
    ///     * Extracurricular
    ///     * Proyeccion social
    /// </summary>
    [Table("[VUC].[tblConvalidationType]")]
    public class VUCConvalidationType
    {
        /// <summary>
        /// Identificador de la tabla
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del tipo de convalidacion (extracurricular o proyeccion social)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripcion
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Estado del registro
        /// </summary>
        public bool Active { get; set; }
    }
}
