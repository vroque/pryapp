﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.VUC
{
    [Table("VUC.EnrolledProgramActivity")]
    public class VUCEnrolledActivity
    {
        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }
        [Column(Order = 2)]
        public int IdProgramActivity { get; set; }
        public int IdActivityType { get; set; }
        public int pidmStudent { get; set; }
        public int Cycle { get; set; }
        public string Malla { get; set; }
        public string Campus { get; set; }
        public string Modality { get; set; }
        public DateTime FechEnrolled { get; set; }
        public string EAP { get; set; }
        public bool Confirmed { get; set; }

    }
}
