﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.LDR
{
    [Table("LDR.tblTypeTracing")]
    public class LDRtblTypeTracing
    {
        [Key]
        [Index(IsUnique = true)]
        public int id { get; set; }
        public string description { get; set; }
    }
}
