﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ACPermanence.DataBases.NEXO.LDR
{
    [Table("LDR.tblBookTracing")]
    public class LDRtblBookTracing
    {
        [Key]
        [Index(IsUnique = true)]
        public int id { get; set; }
        public int type_tracing_id { get; set; }
        public int book_id { get; set; }
        public string personal_id { get; set; }
        public DateTime create_date { get; set; }
    }
}
