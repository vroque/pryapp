﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.LDR
{
    [Table("LDR.tblSatisfactionSurveyClaim")]
    public class LDRtblSatisfactionSurveyClaim
    {
        [Key]
        [Column(Order = 1)]
        public int survey_id { get; set; }
        public int page_id { get; set; }
        public int? survey_answer { get; set; }
        public string survey_state { get; set; }
        public DateTime survey_date_create { get; set; }
        public DateTime? survey_date_resolved { get; set; }
    }
}
