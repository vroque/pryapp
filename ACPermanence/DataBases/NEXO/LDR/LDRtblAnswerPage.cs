﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.LDR
{
    [Table("LDR.tblAnswerPage")]
    public class LDRtblAnswerPage
    {
        [Key]
        [Column(Order = 1)]
        public int answer_id { get; set; }
        public int page_id { get; set; }
        public int? derivate_id { get; set; }
        public string answer_person_creator_pidm { get; set; }
        public string answer_person_validate_pidm { get; set; }
        public string answer_note { get; set; }
        public string answer_state { get; set; }
        public DateTime answer_date_created { get; set; }
        public DateTime? answer_date_validate { get; set; }
    }
}
