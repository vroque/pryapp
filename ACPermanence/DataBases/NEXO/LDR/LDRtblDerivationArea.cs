﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.LDR
{
    [Table("LDR.tblDerivationArea")]
    public class LDRtblDerivationArea
    {
        [Key]
        [Column(Order = 1)]
        public int derivate_id { get; set; }
        public int page_id { get; set; }
        public string derivate_person_creator_pidm { get; set; }
        public string derivate_funtional_unity { get; set; }
        public string derivate_mail_recipient { get; set; }
        public string derivate_type { get; set; }
        public string derivate_state { get; set; }
        public string derivate_note_disabled { get; set; }
        //public string derivate_answer { get; set; }
        //public string derivate_person_answer_pidm { get; set; }
        public DateTime derivate_date_created { get; set; }
        public DateTime? derivate_date_modify { get; set; }
    }
}
