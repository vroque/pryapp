﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.LDR
{
    [Table("LDR.tblStaffInvolved")]
    public class LDRtblStaffInvolved
    {
        [Key]
        [Column(Order = 1)]
        public int staff_involved_id { get; set; }
        public int page_id { get; set; }
        public int derivate_id { get; set; }
        public string staff_involved_pidm { get; set; }
        public string staff_position_company { get; set; }
        public string staff_involved_note { get; set; }
        public string staff_involved_type { get; set; }
        public string staff_derivate_state { get; set; }
        public DateTime staff_date_created { get; set; }
        public DateTime? staff_date_modify { get; set; }
    }
}
