﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ACPermanence.DataBases.NEXO.LDR
{
    [Table("LDR.tblFile")]
    public class LDRtblFile
    {
        [Key]
        [Column(Order = 1)]
        public int file_id { get; set; }
        public int page_id { get; set; }
        public int? derivate_id { get; set; }
        public int? answer_id { get; set; }
        public string file_name { get; set; }
        public string file_route { get; set; }
        public string file_type { get; set; }
        public string user_pidm { get; set; }
        public DateTime date_registry { get; set; }
        public string file_state { get; set; }
    }
}
