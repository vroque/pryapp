﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.LDR
{
    [Table("LDR.tblPageClaimBook")]
    public class LDRtblPageClaimBook
    {
        [Key]
        [Column(Order = 1)]
        public int page_id { get; set; }
        public string page_div { get; set; }
        public string page_origin { get; set; }
        public int page_year { get; set; }
        public string page_campus { get; set; }
        public string page_bussines_unity { get; set; }
        public int page_numeration { get; set; }
        public string page_email { get; set; }
        public string page_first_name { get; set; }
        public string page_last_name { get; set; }
        public string page_identification { get; set; }
        public string page_phone { get; set; }
        public string page_home { get; set; }
        public string page_legal_guardian { get; set; }
        public string page_type_well { get; set; }
        public string page_description_well { get; set; }
        public string page_type_claim { get; set; }
        public string page_description_claim { get; set; }
        public string page_state { get; set; }
        public string page_register_user_pidm { get; set; }
        public string page_note_dissmised { get; set; }
        public DateTime page_date_create { get; set; }
        public DateTime? page_date_finalized { get; set; }
        public string page_person_claim { get; set; }
        public bool? page_avoid { get; set; }
        public string page_avoid_description { get; set; }

    }
}
