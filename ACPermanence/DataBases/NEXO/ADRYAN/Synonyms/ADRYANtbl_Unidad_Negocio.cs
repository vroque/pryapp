using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.ADRYAN.Synonyms
{
    [Table("ADRYAN.tbl_Unidad_Negocio")]
    public class ADRYANtbl_Unidad_Negocio
    {
        [Key]
        [Column(Order = 1)]
        public int idUnidadNegocio { get; set; }
        public string nombre { get; set; }
        public string nombreCorto { get; set; }
    }
}
