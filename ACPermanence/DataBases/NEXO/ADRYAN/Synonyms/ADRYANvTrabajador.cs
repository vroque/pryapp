using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.ADRYAN.Synonyms
{
    [Table("ADRYAN.vTrabajador")]
    public class ADRYANvTrabajador
    {
        [Key]
        [Column(Order = 1)]
        public string codigo_unico { get; set; }

        public string compania { get; set; }
        public string numero_documento { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }
        public string nombre { get; set; }
        public DateTime? fecha_retiro { get; set; }
        public string iddependencia { get; set; }

        /// <summary>
        /// Nombre de unidad funcional
        /// </summary>
        public string nombre_unidad_funcional { get; set; }

        public string descripcion_ubicacion_fisica { get; set; }

        /// <summary>
        /// Código de unidad funcional
        /// </summary>
        public string unidad_funcional_organica { get; set; }

        public string puesto_organica { get; set; }
    }
}
