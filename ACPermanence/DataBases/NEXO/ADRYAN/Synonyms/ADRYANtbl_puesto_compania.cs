using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.ADRYAN.Synonyms
{
    [Table("ADRYAN.tbl_puesto_compania")]
    public class ADRYANtbl_puesto_compania
    {
        [Key]
        [Column(Order = 1)]
        public string puesto { get; set; }

        [Key]
        [Column(Order = 2)]
        public string compania { get; set; }

        public string descripcion_puesto { get; set; }
        public string situacion_registro { get; set; }
    }
}
