using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.ADRYAN.Synonyms
{
    [Table("ADRYAN.tbl_unidad_funcional")]
    public class ADRYANtbl_unidad_funcional
    {
        [Key]
        [Column(Order = 1)]
        public string unidad_funcional { get; set; }

        /// <summary>
        /// - 01 : UC
        /// - 02 : IC
        /// </summary>
        [Key]
        [Column(Order = 2)]
        public string compania { get; set; }

        public string codigo_sucursal { get; set; }
        public string centro_costo { get; set; }
        public string nombre_unidad_funcional { get; set; }
        public string unidad_funcional_superior { get; set; }
    }
}
