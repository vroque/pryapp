using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    /// <summary>
    /// Información de estudiantes UC matriculados en un determinado período.
    /// La data se llena a demanda desde BANNER. 
    /// </summary>
    [Table("CAU.tblStudentCICUC")]
    public class CAUtblStudentCICUC
    {
        [Key, Column(Order = 1)]
        public decimal PIDM { get; set; }
        public string CODIGO { get; set; }
        public string NOMBRE { get; set; }
        public string DESCCOLLEGE { get; set; }
        [Key, Column(Order = 2)]
        public string PROGRAM { get; set; }
        public string DESCPROGRAM { get; set; }
        public string CAMPUS { get; set; }
        public string DEPARTAMENT { get; set; }
        public string TERMCATALOG { get; set; }
        public decimal? TOTALCREDIT { get; set; }
        public string ENROLLEMENTCYCLE { get; set; }
        public DateTime? UPDATEDATE { get; set; }
        public string TERMCATALOGREAL { get; set; }
    }
}
