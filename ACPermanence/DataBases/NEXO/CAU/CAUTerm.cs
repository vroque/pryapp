﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    /// <summary>
    /// Fecha de períodos académicos UC para uso de la aplicación
    /// </summary>
    [Table("CAU.Term")]
    public class CAUTerm
    {
        [Key]
        [Column("Term")]
        public string Term { get; set; }

        public string TermDescription { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}