﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.PollQuestion")]
    public class CAUPollQuestion
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public long PollId { get; set; }
        public string QuestionTitle { get; set; }
        public short QuestionOrder { get; set; }
        public bool IsComment { get; set; }
    }
}
