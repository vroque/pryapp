﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    /// <summary>
    /// Tabla con información sobre encuestas que debe de completar una persona
    /// </summary>
    [Table("CAU.Surveyed")]
    public class CAUSurveyed
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        public decimal Pidm { get; set; }
        public int SurveyId { get; set; }
        public bool Completed { get; set; }
    }
}