﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    /// <summary>
    /// Parentesco
    /// </summary>
    [Table("CAU.Kinship")]
    public class CAUKinship
    {
        [Key]
        [Column(Order = 1)]
        public int KinshipID { get; set; }
        public string KinshipName { get; set; }
        public int KinshipTypeID { get; set; }
        public int KinshipDegreeID { get; set; }
    }
}