﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.Poll")]
    public class CAUPoll
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime PublicationDate { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsRequest { get; set; }
        public short? RequestState { get; set; }
        public bool Active { get; set; }
    }
}
