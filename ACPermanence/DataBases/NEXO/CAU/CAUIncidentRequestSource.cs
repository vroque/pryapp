using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.IncidentRequestSource")]
    public class CAUIncidentRequestSource
    {
        [Key, Column(Order = 1)]
        public int IncidentRequestSourceId { get; set; }
        public string IncidentRequestSourceName { get; set; }
        public string IncidentRequestSourceDescription { get; set; }
    }
}
