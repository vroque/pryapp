﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.TblRequestAttachment")]
    public class CAUTblRequestAttachment
    {
        [Key]
        public int id { get; set; }
        public int requestID { get; set; }
        public int requerimentID { get; set; }
        public bool attached { get; set; }
        public int number { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public DateTime updateDate { get; set; }
    }
}
