﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    /// <summary>
    /// Tipo de configuración
    /// </summary>
    [Table("CAU.SettingsType")]
    public class CAUSettingsType
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}