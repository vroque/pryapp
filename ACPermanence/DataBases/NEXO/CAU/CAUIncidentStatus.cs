using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.IncidentStatus")]
    public class CAUIncidentStatus
    {
        [Key]
        [Column(Order = 1)]
        public int IncidentStatusId { get; set; }

        public string IncidentStatusName { get; set; }
        public string IncidentStatusDescription { get; set; }
    }
}
