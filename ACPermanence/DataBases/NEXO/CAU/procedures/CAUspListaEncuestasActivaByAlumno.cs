namespace ACPermanence.DataBases.NEXO.CAU.procedures
{
    /// <summary>
    /// Datos que devuelve el procedimiento: CED.sp_ListaEncuestasActiva_by_alumno
    /// CAU.spListaEncuestasActivaByAlumno es un sinónimo a BDCED.CED.sp_ListaEncuestasActiva_by_alumno
    /// </summary>
    public class CAUspListaEncuestasActivaByAlumno
    {
        public int Id { get; set; }
        public string NombreEncuesta { get; set; }
        public string RutaEncuesta { get; set; }
    }
}