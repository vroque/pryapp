﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace ACPermanence.DataBases.NEXO.CAU.views
{
    [Table("CAU.v_Graduate")]
    public class CAUv_Graduate
    {
        [Key]
        [Column(Order = 1)]
        public string divs { set; get; }
        [Key]
        [Column(Order = 2)]
        public string levl { set; get; }
        [Column(Order = 3)]
        public string college { set; get; }
        [Column(Order = 4)]
        public string departament { set; get; }
        [Key]
        [Column(Order = 5)]
        public string program { set; get; }
        [Key]
        [Column(Order = 6)]
        public decimal pidm { set; get; }
        public string periodGraduate { set; get; }
        public string periodRegistred { set; get; }
        public decimal creditsRequired { set; get; }
        public string electiveCredits { set; get; }
        public string compulsoryCredits { set; get; }
        public string realCredits { set; get; }
        public string egresado { set; get; }
        public string bachiller { set; get; }
        public string titulado { set; get; }
        public string practicasProf { set; get; }
        public string proySocial { set; get; }
        [Column(name: "2doidioma")]
        public string idioma { set; get; }
}
}
