﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    /// <summary>
    /// Términos y condiciones aceptados
    /// </summary>
    [Table("CAU.AcceptedTerms")]
    public class CAUAcceptedTerms
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        public decimal Pidm { get; set; }
        public int TermsId { get; set; }
        public bool Accepted { get; set; }
        public DateTime AcceptedDate { get; set; }
    }
}