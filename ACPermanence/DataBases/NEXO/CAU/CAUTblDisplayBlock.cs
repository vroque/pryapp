﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.TblDisplayBlock")]
    public class CAUTblDisplayBlock
    {
        [Key]
        [Column(Order = 1)]
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public System.DateTime startFech { get; set; }
        public System.DateTime endFech { get; set; }
        public bool active { get; set; }
    }
}
