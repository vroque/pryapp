﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    /// <summary>
    /// Tabla CAU.PersonApoderado
    /// </summary>
    [Table("CAU.PersonApoderado")]
    public class CAUPersonApoderado
    {
        [Key]
        [Column(Order = 1)]
        public int ID { get; set; }
        public decimal PIDM { get; set; }
        public decimal StudentPIDM { get; set; }
        public int KinshipID { get; set; }
        public bool Active { get; set; }
        public DateTime ActivationDate { get; set; }
        public decimal ActivationUserPIDM { get; set; }
        public DateTime? LastModificationDate { get; set; }
        public string LastModificationComment { get; set; }
        public decimal LastModificationUserPIDM { get; set; }
    }
}