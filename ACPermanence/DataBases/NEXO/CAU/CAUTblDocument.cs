﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.TblDocument")]
    public class CAUTblDocument
    {
        [Key]
        public int document { set; get; }
        public string documentName { set; get; }
        public DateTime updateDate { set; get; }
    }
}
