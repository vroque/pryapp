using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.IncidentSubCategoryFunctionalUnit")]
    public class CAUIncidentSubCategoryFunctionalUnit
    {
        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }
        public int IncidentSubCategoryId { get; set; }
        public string IncidentSubCategoryName { get; set; }
        public string FunctionalUnitId { get; set; }
        public string FunctionalUnitName { get; set; }
    }
}
