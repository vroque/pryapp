﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.TblDocumentState")]
    public class CAUTblDocumentState
    {
        [Key]
        [Column(Order=1)]
        public int documentID { set; get; }
        [Key]
        [Column(Order = 2)]
        public int state { set; get; }
        [Key]
        [Column(Order = 3)]
        public int order { set; get; }
        [Key]
        [Column(Order = 4)]
        public int nextState { set; get; }
        public int officeID { set; get; }
    }
}
