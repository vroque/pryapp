﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    /// <summary>
    /// Configuración para la aplicación
    /// </summary>
    [Table("CAU.Settings")]
    public class CAUSettings
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        public string Abbr { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SettingsTypeId { get; set; }
        public bool Active { get; set; }
        public bool HasDetail { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}