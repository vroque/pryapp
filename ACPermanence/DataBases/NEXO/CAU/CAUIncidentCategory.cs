using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.IncidentCategory")]
    public class CAUIncidentCategory
    {
        [Key]
        [Column(Order = 1)]
        public int IncidentCategoryId { get; set; }

        public int? IncidentParentCategoryId { get; set; }
        public string IncidentCategoryName { get; set; }
        public string IncidentCategoryDescription { get; set; }
    }
}
