﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.TblRequestLog")]
    public class CAUTblRequestLog
    {
        [Key]
        [Column(Order = 1)]
        public int id { set; get; }
        public int requestID { set; get; }
        public int oldState { set; get; }
        public int newState { set; get; }
        public string observation { set; get; }
        public string userID { set; get; }
        public int? officeUserID { set; get; }
        public int? officeID { set; get; }
        public string officeName { set; get; }
        public string approved { set; get; }
        public DateTime updateDate { set; get; }
    }
}
