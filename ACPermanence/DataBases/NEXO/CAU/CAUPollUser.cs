﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.PollUser")]
    public class CAUPollUser
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public long PollId { get; set; }
        public decimal Pidm { get; set; }
        public int RequestId { get; set; }
        public short Order { get; set; }
        public DateTime PollDate { get; set; }
        public bool Omitted { get; set; }
        public bool Completed { get; set; }
    }
}
