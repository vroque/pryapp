﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.TblRequest")]
    public class CAUTblRequest
    {
        [Key]
        [Column(Order = 1)]
        public string divs { set; get; }
        [Key]
        [Column(Order = 2)]
        public string campus { set; get; }
        [Key]
        [Column(Order = 3)]
        public string program { set; get; }
        [Key]
        [Column(Order = 4)]
        public decimal pidm { set; get; }
        [Key]
        [Column(Order = 5)]
        public int requestID { set; get; }
        public DateTime requestDate { set; get; }
        public int documentNumber { set; get; }
        public int documentID { set; get; }
        public int relationshipID { set; get; }
        public string applicantName { set; get; }
        public DateTime deliveryDate { set; get; }
        public int requestStateID { set; get; }
        public string requestDescription { set; get; }
        public int officeUserID { set; get; }
        public double amount { set; get; }
        public string conceptID { set; get; }
        public string receiverName { set; get; }
        public string receiverDocument { set; get; }
        public int officeID { set; get; }
        public string term { set; get; }
        public bool aproved { set; get; }
        public DateTime updateDate { set; get; }
        public string onlinePaymentID { set; get; }
        
    }
}
