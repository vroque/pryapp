﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.tblDocumentRequirement")]
    public class CAUtblDocumentRequirement
    {
        [Key]
        public int id { get; set; }
        public int documentID { get; set; }
        public string requirementDescription { get; set; }
        public int requirementOrder { get; set; }
        public string requirementCategory { get; set; }
    }
}
