using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table(("CAU.Incident"))]
    public class CAUIncident
    {
        [Key]
        [Column(Order = 1)]
        public long IncidentId { get; set; }

        public decimal AuthorPidm { get; set; }
        public decimal ReporterPidm { get; set; }
        public string ReporterName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime PublicationDate { get; set; }
        public int? IncidentCategoryId { get; set; }
        public int? IncidentSubCategoryId { get; set; }
        public int IncidentTypeId { get; set; }
        public int IncidentStatusId { get; set; }
        public decimal? AssignedToPidm { get; set; }
        public string AssignedToTeam { get; set; }
        public int RequestSource { get; set; }
    }
}
