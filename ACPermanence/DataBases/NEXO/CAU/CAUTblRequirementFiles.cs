﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("[CAU].[tblRequirementFiles]")]
    public class CAUTblRequirementFiles
    {
        public int id { get; set; }
        public int requirementID { get; set; }
        public int requestID { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public DateTime createDate { get; set; }
        public DateTime updateDate { get; set; }
    }
}
