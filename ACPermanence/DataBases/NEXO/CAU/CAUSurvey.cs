﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    /// <summary>
    /// Tabla con información de encuestas
    /// </summary>
    [Table("CAU.Survey")]
    public class CAUSurvey
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Active { get; set; }
    }
}