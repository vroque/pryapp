﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("[CAU].[tblRequirement]")]
    public class CAUTblRequirement
    {
        [Key]
        public int id { get; set; }
        public int documentID { get; set; }
        public int categoryID { get; set; }
        public string description { get; set; }
        public int order { get; set; }
        public int minFiles { get; set; }
        public int maxFiles { get; set; }
        public DateTime createDate { get; set; }
        public DateTime updateDate { get; set; }
    }
}
