﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("[CAU].[TblTermDocument]")]
    public class CAUTblTermDocument
    {
        [Key]
        [Column(Order = 1)]
        public string term { get; set; }
        [Key]
        [Column(Order = 2)]
        public string partTerm { get; set; }
        [Key]
        [Column(Order = 3)]
        public string campus { get; set; }
        [Key]
        [Column(Order = 4)]
        public string department { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public DateTime startClassesDate { get; set; }
        public DateTime endClassesDate { get; set; }
        public DateTime startEnrollDate { get; set; }
        public DateTime endEnrollDate { get; set; }
        public DateTime? startSubstituteExamDate { get; set; }
        public DateTime? endSubstituteExamDate { get; set; }
        public DateTime updateDate { get; set; }
    }
}
