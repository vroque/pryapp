﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("[CAU].[tblRequirementCategory]")]
    public class CAUTblRequirementCategory
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
    }
}
