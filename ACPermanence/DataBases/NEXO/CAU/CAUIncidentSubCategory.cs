using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.IncidentSubCategory")]
    public class CAUIncidentSubCategory
    {
        [Key]
        [Column(Order = 1)]
        public int IncidentSubCategoryId { get; set; }

        public int IncidentCategoryId { get; set; }
        public string IncidentSubCategoryName { get; set; }
        public string IncidentSubCategoryDescription { get; set; }
    }
}
