﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("[CAU].[TblSignatory]")]
    public class CAUTblSignatory
    {
        [Key]
        public int signatoryID { get; set; }
        public decimal pidm { get; set; }
        public string name { get; set; }
        public byte[] signature { get; set; }
        public byte[] postSignature { get; set; }
        public byte[] seal { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public DateTime updateDate { get; set; }
        public bool fullSignature { get; set; }
    }
}
