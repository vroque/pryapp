﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("[CAU].[TBLDOCUMENTCOST]")]
    public class CAUTblDdocumentCost
    {
        [Key]
        [Column(Order = 1)]
        public int documentID { get; set; }
        [Key]
        [Column(Order = 2)]
        public string divs { get; set; }
        [Key]
        [Column(Order = 3)]
        public string department { get; set; }
        [Key]
        [Column(Order = 4)]
        public string program { get; set; }
        [Key]
        [Column(Order = 5)]
        public double cost { get; set; }
        [Key]
        [Column(Order = 6)]
        public string account { get; set; }
        [Key]
        [Column(Order = 7)]
        public string costcenter { get; set; }
        [Key]
        [Column(Order = 8)]
        public string service { get; set; }
        [Key]
        [Column(Order = 9)]
        public double processcost { get; set; }
    }
}
