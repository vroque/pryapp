﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.PollChoiceDetail")]
    public class CAUPollChoiceDetail
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public long PollChoiceId { get; set; }
        public decimal Pidm { get; set; }
        public int RequestId { get; set; }
        public string Comment { get; set; }
        public DateTime VoteDate { get; set; }
    }
}
