﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.PollChoice")]
    public class CAUPollChoice
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        public long PollQuestionId { get; set; }
        public short OrderChoice { get; set; }
        public string ChoiceText { get; set; }
        public int Votes { get; set; }
        public bool IsComment { get; set; }
    }
}
