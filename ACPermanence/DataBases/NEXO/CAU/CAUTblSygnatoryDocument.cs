﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("[CAU].[TblSignatoryDocument]")]
    public class CAUTblSignatoryDocument
    {
        [Key]
        [Column(Order = 1)]
        public int signatoryID { get; set; }
        [Key]
        [Column(Order = 2)]
        public int documentID { get; set; }
        public string rol { get; set; }
        public string college { get; set; }
        public int order { get; set; }
    }
}
