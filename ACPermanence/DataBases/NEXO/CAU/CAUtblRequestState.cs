﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table("CAU.TblRequestState")]
    public class CAUtblRequestState
    {
        [Key]
        public int state { get; set; }
        public string stateName { get; set; }
        public DateTime updateDate { get; set; }
    }
}
