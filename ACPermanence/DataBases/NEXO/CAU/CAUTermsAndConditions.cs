﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    /// <summary>
    /// Tabla con información de términos y condiciones
    /// </summary>
    [Table("CAU.TermsAndConditions")]
    public class CAUTermsAndConditions
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public bool IsFile { get; set; }
        public string Detail { get; set; }
        public string Url { get; set; }
        public DateTime PublicationDate { get; set; }
        public bool Active { get; set; }
    }
}