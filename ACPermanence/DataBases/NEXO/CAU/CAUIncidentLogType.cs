using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table(("CAU.IncidentLogType"))]
    public class CAUIncidentLogType
    {
        [Key]
        [Column(Order = 1)]
        public int IncidentLogTypeId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
    }
}