using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table(("CAU.IncidentType"))]
    public class CAUIncidentType
    {
        [Key]
        [Column(Order = 1)]
        public int IncidentTypeId { get; set; }

        public string IncidentTypeName { get; set; }
        public string IncidentTypeDescription { get; set; }
    }
}
