using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.CAU
{
    [Table(("CAU.IncidentLog"))]
    public class CAUIncidentLog
    {
        [Key]
        [Column(Order = 1)]
        public long IncidentLogId { get; set; }

        public long IncidentId { get; set; }
        public int IncidentLogTypeId { get; set; }
        public decimal IncidentLogAuthorPidm { get; set; }
        public string IncidentLogAuthorName { get; set; }
        public string IncidentLogTeamId { get; set; }
        public string IncidentLogTeamName { get; set; }
        public string IncidentLogComment { get; set; }
        public DateTime IncidentLogPublicationDate { get; set; }
        public bool IncidentLogIsPublic { get; set; }
    }
}
