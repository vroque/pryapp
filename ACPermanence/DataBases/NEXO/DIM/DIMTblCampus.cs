﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TblCampus")]
    public class DIMTblCampus
    {
        [Key]
        [Column(Order = 1)]
        public string campus { set; get; }
        public string campusDescription { set; get; }
        public string codePlace { set; get; }
        public DateTime updateDate { set; get; }   
    }
}
