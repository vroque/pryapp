﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("[DIM].[tblAcademicProgress]")]
    public class DIMTblAcademicProgress
    {
        [Key]
        [Column(Order = 1)]
        public string divs { get; set; }
        [Key]
        [Column(Order = 2)]
        public string levl { get; set; }
        [Key]
        [Column(Order = 3)]
        public string program { get; set; }
        [Key]
        [Column(Order = 4)]
        public string term { get; set; }
        [Key]
        [Column(Order = 5)]
        public decimal pidm { get; set; }
        [Key]
        [Column(Order = 6)]
        public string departament { get; set; }
        [Key]
        [Column(Order = 7)]
        public string nrc { get; set; }
        [Key]
        [Column(Order = 8)]
        public string subject { get; set; }
        [Key]
        [Column(Order = 9)]
        public string course { get; set; }
        public string nameCourse { get; set; }
        public int credit { get; set; }
        public double score { get; set; }
        public string elective { get; set; }
        public string cycle { get; set; }
        public decimal nonAttendancePercentage { get; set; }
        public DateTime dateFinalScore { get; set; }
        public string aproved { get; set; }
    }
}
