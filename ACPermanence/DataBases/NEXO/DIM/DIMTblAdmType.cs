﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("[DIM].[tblAdmType]")]
    public class DIMTblAdmType
    {
        [Key]
        public string admType { get; set; }
        public string admTypeDescription { get; set; }
        public bool admPronabec { get; set; }
        public DateTime updateDate { get; set; }
    }
}
