﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TblSubjectCourse")]
    public class DIMTblSubjectCourse
    {
        [Key]
        public string subject { get; set; }
        public string course { get; set; }
        public string term { get; set; }
        public string college { get; set; }
        public string divs { get; set; }
        public string department { get; set; }
        public string program { get; set; }
        public string courseDescription { get; set; }
        public int credits { get; set; }
        public int elective { get; set; }
        public DateTime updateDate { get; set; }
    }
}
