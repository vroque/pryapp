﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("[DIM].[tblStudentEnrollmentByPeriod]")]
    public class DIMTblStudentEnrollmentByPeriod
    {
        [Key]
        [Column(Order = 1)]
        public string divs { get; set; }
        [Key]
        [Column(Order = 2)]
        public string levl { get; set; }
        [Key]
        [Column(Order = 3)]
        public string program { get; set; }
        [Key]
        [Column(Order = 4)]
        public decimal pidm { get; set; }
        [Key]
        [Column(Order = 5)]
        public string term { get; set; }
        public string departament { get; set; }
        public decimal realCredits { get; set; }
        [Column("thirdFiftTenthTop")] // FIXME T_T
        public string thirdFithTenthTop { get; set; }
        public decimal PGA { get; set; }
        public string termCatalog { get; set; }
        [Column("nominalCycle")] //FIXME
        public string actualCycle { get; set; }
        public DateTime updateDate { get; set; }
    }
}
