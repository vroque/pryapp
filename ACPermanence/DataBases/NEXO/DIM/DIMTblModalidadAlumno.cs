﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TBLModalidadAlumno")]
    public class DIMTblModalidadAlumno
    {
        [Key]
        [Column(Order = 1)]
        public decimal pidm { get; set; }
        [Key]
        [Column(Order = 2)]
        public string departament { get; set; }
        [Key]
        [Column(Order = 3)]
        public string program { get; set; }
        [Key]
        [Column(Order = 4)]
        public string college { get; set; }
        public string divs { get; set; }
        public string campus { get; set; }
        public string term { get; set; }
        public string partterm { get; set; }
        public string admitted { get; set; }
        public string levl { get; set; }
        public string subject { get; set; }

    }
}
