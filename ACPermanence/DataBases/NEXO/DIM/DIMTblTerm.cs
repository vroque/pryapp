﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TBLTERM")]
    public class DIMTblTerm
    {
        [Key]
        [Column(Order = 1)]
        public string term { set; get; }
        public string termDescription { set; get; }
        public DateTime startDate { set; get; }
        public DateTime endDate { set; get; }
        public DateTime updateDate { set; get; }   
    }
}
