﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.tblPostulant")]
    public class DIMTblPostulant
    {
        [Key]
        [Column(Order = 1)]
        public string divs { get; set; }
        [Key]
        [Column(Order = 2)]
        public string campus { get; set; }
        [Key]
        [Column(Order = 3)]
        public string term { get; set; }
        [Key]
        [Column(Order = 4)]
        public string levl { get; set; }
        [Key]
        [Column(Order = 5)]
        public string college { get; set; }
        [Key]
        [Column(Order = 6)]
        public string program { get; set; }
        [Key]
        [Column(Order = 7)]
        public string departament { get; set; }
        [Key]
        [Column(Order = 8)]
        public string admType { get; set; }
        [Key]
        [Column(Order = 9)]
        public decimal pidm { get; set; }
        public DateTime registrationDate { get; set; }
        public string status { get; set; }
        public string currentState { get; set; }
        public DateTime updateDate { get; set; }
    }
}
