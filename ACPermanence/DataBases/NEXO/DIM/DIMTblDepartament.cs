﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TblDepartament")]
    public class DIMTblDepartament
    {
        [Key]
        [Column(Order = 1)]
        public string departament { set; get; }
        public string departamentDescription { set; get; }
        public DateTime updateDate { set; get; }
        /// <summary>
        /// Indicador de vigencia del registro (1 y 0)
        /// </summary>
        public string campana { get; set; }
    }
}
