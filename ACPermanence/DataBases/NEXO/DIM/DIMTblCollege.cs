﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TBLCOLLEGE")]
    public class DIMTblCollege
    {
        [Key]
        public string college { set; get; }
        [Column("collegeDesc")]
        public string collegeDescription { set; get; }
        public DateTime updateDate { set; get; } 
    }
}
