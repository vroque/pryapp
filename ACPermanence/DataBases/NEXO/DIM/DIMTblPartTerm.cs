﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TblPartTerm")]
    public class DIMTblPartTerm
    {
        [Key]
        [Column(Order = 1)]
        public string partTerm { set; get; }
        public string partTermDescription { set; get; }
        public DateTime updateDate { set; get; }   
    }
}
