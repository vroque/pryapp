﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TBLSUBJECT")]
    public class DIMTblSubject
    {
        [Key]
        [Column(Order = 1)]
        public string subject { set; get; }
        public string subjectDescription { set; get; }
        public DateTime updateDate { set; get; }   
    }
}
