﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TblMatriculadosPeriodo")]
    public class DIMTblMatriculadosPeriodo
    {
        [Key]
        [Column(Order = 1)]
        public string divs { set; get; }
        [Key]
        [Column(Order = 2)]
        public string campus { set; get; }
        [Key]
        [Column(Order = 3)]
        public string term { set; get; }
        [Key]
        [Column(Order = 4)]
        public string departament { set; get; }
        [Key]
        [Column(Order = 5)]
        public string program { set; get; }
        [Key]
        [Column(Order = 6)]
        public string college { set; get; }
        [Key]
        [Column(Order = 7)]
        public string levl { set; get; }
        public string partTerm { set; get; }
        public int nrc { set; get; }
        public string section { set; get; }
        public string subject { set; get; }
        public string course { set; get; }
        public decimal pidm { set; get; }
        public decimal finalScore { set; get; }
        public DateTime enrollmentDate { set; get; }
        public DateTime updateDate { set; get; }   
    }
}
