﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TBLDIVS")]
    public class DIMTblDivs
    {
        [Key]
        [Column(Order = 1)]
        public string divs { set; get; }
        public string divsDescription { set; get; }
        public DateTime updateDate { set; get; }   
    }
}
