﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TBLRequisitoAlumno")]
    public class DIMTblRequirementStudent
    {
        [Key]
        [Column(Order = 1)]
        public string divs { set; get; }
        [Key]
        [Column(Order = 2)]
        public string campus { set; get; }
        [Key]
        [Column(Order = 3)]
        public string term { set; get; }
        [Key]
        [Column(Order = 4)]
        public string departament { set; get; }
        [Key]
        [Column(Order = 5)]
        public string program { set; get; }
        [Key]
        [Column(Order = 6)]
        public string college { set; get; }
        [Key]
        [Column(Order = 7)]
        public string levl { set; get; }
        [Key]
        [Column(Order = 8)]
        public string partTerm { set; get; }
        [Key]
        [Column(Order = 9)]
        public decimal pidm { set; get; }
        [Key]
        [Column(Order = 10)]
        public string codeRequirement { set; get; }
        public string delivered { set; get; }    
        public DateTime updateDate { set; get; }   
    }
}
