﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TBLLEVL")]
    public class DIMTblLevl
    {
        [Key]
        [Column(Order = 1)]
        public string levl { set; get; }
        public string levlDescription { set; get; }
        public DateTime updateDate { set; get; }   
    }
}
