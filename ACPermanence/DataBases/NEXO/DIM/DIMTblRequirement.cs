﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TblRequisito")]
    public class DIMTblRequirement
    {
        [Key]
        [Column(Order = 1)]
        public string codeRequirement { set; get; }
        public string requirementDescription { set; get; }
        public DateTime updateDate { set; get; }   
    }
}
