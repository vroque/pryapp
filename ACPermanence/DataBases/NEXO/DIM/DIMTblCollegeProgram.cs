﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("[DIM].[tblCollegeProgram]")]
    public class DIMTblCollegeProgram
    {
        [Key]
        [Column(Order = 1)]
        public string divs { get; set; }
        [Key]
        [Column(Order = 2)]
        public string program { get; set; }
        [Key]
        [Column(Order = 3)]
        public string college { get; set; }
        [Key]
        [Column(Order = 4)]
        public string levl { get; set; }
        public string active { get; set; }
        public DateTime updateDate { get; set; }
    }
}
