﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TblAlumnoEstado")]
    public class DIMTblAlumnoEstado
    {
        [Key]
        [Column(Order=1)]
        public string divs { get; set; }
        [Key]
        [Column(Order = 2)]
        public string campus { get; set; }
        [Key]
        [Column(Order = 3)]
        public string term { get; set; }
        [Key]
        [Column(Order = 4)]
        public string levl { get; set; }
        [Key]
        [Column(Order = 5)]
        public string college { get; set; }
        [Key]
        [Column(Order = 6)]
        public string program { get; set; }
        [Key]
        [Column(Order = 7)]
        public string departament { get; set; }
        [Key]
        [Column(Order = 8)]
        public string admType { get; set; }
        [Key]
        [Column(Order = 9)]
        public decimal pidm { get; set; }
        public int approvedCredits { get; set; }
        public DateTime registrationDate { get; set; }
        //retirado por Henry en el proceso de paso a preproducción
        //public string currentCycle { get; set; }
        public DateTime updateDate { get; set; }
    }
}
