﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.tblTeachingHoursTerm")]
    public class DIMtblTeachingHoursTerm
    {
        [Key]
        [Column(Order = 1)]
        public string divs { get; set; }
        [Key]
        [Column(Order = 2)]
        public string levl { get; set; }
        [Key]
        [Column(Order = 3)]
        public string term { get; set; }
        [Key]
        [Column(Order = 4)]
        public string program { get; set; }
        [Key]
        [Column(Order = 5)]
        public string course { get; set; }
        [Key]
        [Column(Order = 6)]
        public string nrc { get; set; }
        [Key]
        [Column(Order = 7)]
        public decimal pidmTeacher { get; set; }
        [Key]
        [Column(Order = 8)]
        public string beginTime { get; set; }
        [Key]
        [Column(Order = 9)]
        public string endTime { get; set; }
        [Key]
        [Column(Order = 10)]
        public string sunday { get; set; }
        [Key]
        [Column(Order = 11)]
        public string monday { get; set; }
        [Key]
        [Column(Order = 12)]
        public string tuesday { get; set; }
        [Key]
        [Column(Order = 13)]
        public string wednesday { get; set; }
        [Key]
        [Column(Order = 14)]
        public string thursday { get; set; }
        [Key]
        [Column(Order = 15)]
        public string friday { get; set; }
        [Key]
        [Column(Order = 16)]
        public string saturday { get; set; }
        [Key]
        [Column(Order = 17)]
        public string type { get; set; }
        [Key]
        [Column(Order = 18)]
        public string courseName { get; set; }
        [Key]
        [Column(Order = 19)]
        public string nameTeacher { get; set; }
        [Key]
        [Column(Order = 20)]
        public string hallClass { get; set; }
        [Key]
        [Column(Order = 21)]
        public string classroom { get; set; }
        [Key]
        [Column(Order = 22)]
        public DateTime updateDate { get; set; }
    }
}
