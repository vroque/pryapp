﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Table("DIM.TblPerson")]
    public class DIMTblPerson
    {
        [Key]
        public decimal pidm { set; get; }
        public string documentNumber { set; get; }
        public string documentType { set; get; }
        public string firstLastName { set; get; }
        public string secondLastName { set; get; }
        public string firstName { set; get; }
        public string fullName { set; get; }      
        public DateTime birthdate { set; get; }
        public string gender { set; get; }
        public string addressHome { set; get; }
        public string ubigeo { set; get; }
        public string personaEmail { set; get; }       
        public DateTime updateDate { set; get; }

    }
}
