﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.DIM
{
    [Obsolete("Use SZVMAJR instead", true)]
    [Table("DIM.TblProgram")]
    public class DIMTblProgram
    {
        [Key]
        [Column(Order = 1)]
        public string program { set; get; }
        public string programDescription { set; get; }
        public DateTime updateDate { set; get; }   
    }
}
