﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.TCU
{
    [Table("TCU.tblCarnet")]
    public class TCUtblCarnet
    {
        [Key]
        [Column(Order = 1)]
        public string divs { get; set; }
        [Key]
        [Column(Order = 2)]
        public string campus { get; set; }
        [Key]
        [Column(Order = 3)]
        public string term { get; set; }
        [Key]
        [Column(Order = 4)]
        public string departament { get; set; }
        [Key]
        [Column(Order = 5)]
        public string program { get; set; }
        [Key]
        [Column(Order = 6)]
        public string college { get; set; }
        [Key]
        [Column(Order = 7)]
        public string levl { get; set; }
        [Key]
        [Column(Order = 8)]
        public string partTerm { get; set; }
        [Key]
        [Column(Order = 9)]
        public decimal? nrc { get; set; }
        [Key]
        [Column(Order = 10)]
        public string section { get; set; }
        [Key]
        [Column(Order = 11)]
        public decimal pidm { get; set; }
        public string idYear { get; set; }
        public string arrived { get; set; }
        public DateTime? arrivalDate { get; set; }
        public string delivered { get; set; }
        public DateTime? deliveryDate { get; set; }
        public string paymentStatus { get; set; }
        public int? idSchedule { get; set; }
        public string observation { get; set; }
        public string cardStatus { get; set; }
        public string validated { get; set; }
        public string quantityValidated { get; set; }
        public string documentNumber { get; set; }
        public string fotoObservacion { get; set; }
        public DateTime? observationDate { get; set; }
        public string userObservation { get; set; }
        public string status_2 { get; set; }
        public string statusDate { get; set; }
        public string userUpdate { get; set; }
        public DateTime? updateDate { get; set; }
        public long? correlative { get; set; }
        public string statusProcessed { get; set; }
        public DateTime? procedureDate { get; set; }
        public string observationProcedure { get; set; }
        public string deliveredTo { get; set; }
        public DateTime? expirationDate { get; set; }
    }
}
