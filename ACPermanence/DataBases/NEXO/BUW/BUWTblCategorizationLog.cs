﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.BUW
{
    [Table("[BUW].[tblCategorizationLog]")]
    public class BUWTblCategorizationLog
    {
        [Key]
        public int id { get; set; }
        public string div { get; set; }
        public string levl { get; set; }
        public string department { get; set; }
        public string program { get; set; }
        public string term { get; set; }
        public decimal pidm { get; set; }
        public string category { get; set; }
        public string user { get; set; }
        public DateTime updateDate { get; set; }
    }
}
