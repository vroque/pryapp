﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.BUW
{
    [Table("[BUW].[tblCategorizeParameter]")]
    public class BUWTblCategorizeParameter
    {
        [Key]
        [Column(Order = 1)]
        public string div { get; set; }
        [Key]
        [Column(Order = 2)]
        public string levl { get; set; }
        [Key]
        [Column(Order = 3)]
        public string department { get; set; }
        [Key]
        [Column(Order = 4)]
        public string program { get; set; }
        [Key]
        [Column(Order = 5)]
        public string term { get; set; }
        [Key]
        [Column(Order = 6)]
        public string name { get; set; }
        public decimal value { get; set; }
        public DateTime updateDate { get; set; }
    }
}
