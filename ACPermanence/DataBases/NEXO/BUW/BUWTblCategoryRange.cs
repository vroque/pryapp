﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.BUW
{
    [Table("[BUW].[tblCategoryRange]")]
    public class BUWTblCategoryRange
    {
        [Key]
        [Column(Order = 1)]
        public string div { get; set; }
        [Key]
        [Column(Order = 2)]
        public string levl { get; set; }
        [Key]
        [Column(Order = 3)]
        public string department { get; set; }
        [Key]
        [Column(Order = 4)]
        public string program { get; set; }
        [Key]
        [Column(Order = 5)]
        public string term { get; set; }
        [Key]
        [Column(Order = 6)]
        public string category { get; set; }
        public decimal ammountMin { get; set; }
        public decimal ammountMax { get; set; }
        public DateTime updateDate { get; set; }
    }
}
