﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.UBIGEO
{
    [Table("[UBIGEO].[tblCountry]")]
    public class UBIGEOTblCountry
    {
        [Key]
        public string idCountry { get; set; }
        public string country { get; set; }
        public string codeCountry { get; set; }
    }
}
