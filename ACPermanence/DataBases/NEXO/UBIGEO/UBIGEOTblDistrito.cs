﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.UBIGEO
{
    [Table("[UBIGEO].[tblDistrito]")]
    public class UBIGEOTblDistrito
    {
        [Key]
        public string idDistrict { get; set; }
        public string idDepartament { get; set; }
        public string idProvince { get; set; }
        public string district { get; set; }
    }
}
