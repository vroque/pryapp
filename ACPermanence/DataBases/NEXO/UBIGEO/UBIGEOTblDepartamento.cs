﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.UBIGEO
{
    [Table("[UBIGEO].[tblDepartamento]")]
    public class UBIGEOTblDepartamento
    {
        [Key]
        public string idDepartament { get; set; }
        public string departament { get; set; }
    }
}
