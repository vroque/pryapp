﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.UBIGEO
{
    [Table("[UBIGEO].[tblProvincia]")]
    public class UBIGEOTblProvincia
    {
        [Key]
        public string idProvince { get; set; }
        public string idDepartament { get; set; }
        public string province { get; set; }
    }
}
