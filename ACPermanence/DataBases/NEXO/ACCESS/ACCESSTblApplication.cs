﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.ACCESS
{
    [Table("ACCESS.tblApplication")]
    public class ACCESSTblApplication
    {
		[Key]
		public int applicationID { get; set; }
        public string applicationName { get; set; }
        public string applicationDescription { get; set; }
        public DateTime updateDate { get; set; }
    }
}
