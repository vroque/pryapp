﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.ACCESS
{

    [Table("ACCESS.TblGroup")]
    public class ACCESSTblGroup
    {
        [Key]
        [Column(Order = 1)]
        public int groupID { get; set; }
        public string groupName { get; set; }
        public DateTime updateDate { get; set; }
    }
}