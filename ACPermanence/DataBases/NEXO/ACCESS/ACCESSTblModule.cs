﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACPermanence.DataBases.NEXO.ACCESS
{
    [Table("ACCESS.TblModule")]
    public class ACCESSTblModule
    {
        [Key]
        public int moduleID { get; set; }
        public int applicationID { get; set; }
        public string moduleDescription { get; set; }
        public string moduleName { get; set; }
        public DateTime updateDate { get; set; }
        public string abbr { get; set; }
    }
}
