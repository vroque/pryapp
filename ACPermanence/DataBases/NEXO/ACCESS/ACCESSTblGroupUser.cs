﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.ACCESS
{
    [Table("ACCESS.TBLGroupUser")]
    public class ACCESSTblGroupUser
    {
        [Key]
        [Column(Order = 1)]
        public int groupID { get; set; }
        [Key]
        [Column(Order = 2)]
        public string userID { get; set; }
        public DateTime updateDate { get; set; }
    }

}
