﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.ACCESS
{
    [Table("ACCESS.TblGroupModule")]
    public class ACCESSTblGroupModule
    {
        [Key]
        [Column(Order = 1)]
        public int groupID{ get; set; }
        [Key]
        [Column(Order = 2)]
        public int moduleID { get; set; }
        public string campus { get; set; }
        public DateTime updateDate { get; set; }
    }
}
