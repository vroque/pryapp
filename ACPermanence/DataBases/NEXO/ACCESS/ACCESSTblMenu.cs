﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.ACCESS
{
    [Table("ACCESS.TblMenu")]
    public class ACCESSTblMenu
    {
        [Key] public int menuID { get; set; }
        public int moduleID { get; set; }
        public string position { get; set; }
        public string driver { get; set; }
        public string name { get; set; }
        public string icon { get; set; }
        public string uri { get; set; }
        public string cssStyle { get; set; }
        public string status { get; set; }
        public int? parentID { get; set; }
        public DateTime updateDate { get; set; }
        public string description { get; set; }
        public string cardStyle { get; set; }
        public string cardIcoStyle { get; set; }
        public int? order { get; set; }
        public bool ExternalLink { get; set; }
    }
}