﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACPermanence.DataBases.NEXO.ACCESS
{
    [Table("ACCESS.TblUser")]
    public class ACCESSTblUser
    {
        [Key]
        public string userID { get; set; }
        public decimal pidm { get; set; }
        public string userState { get; set; }
        public DateTime updateDate { get; set; }
    }
}