﻿using System.Collections.Generic;

namespace ACTools.Constants
{
    public static class InstitucionalConstants
    {
        public const string UC = "UCCI";
        public const string IC = "IESC";
        public const string CIC = "CIC";

        public static Dictionary<string, string> DEPARTMENT_DOCS_NAMES =
            new Dictionary<string, string>
            {
                {"UREG", "Presencial"},
                {"UPGT", "Semipresencial (Gente que trabaja)"},
                {"UVIR", "Semipresencial (A distancia)"},
                {"UCIC", "UC-CENTRO DE IDIOMAS"},
                {"UPOS", "UC-POSTGRADO"},
                {"ICEC", "UC-INST-EDUC-CONTINUA"},
                {"UCEC", "UC-CENTRO DE EDUCACION CONTIN"},
                {"ITEC", "UC-TECNICO"}
            };
        public const int OFFICE_WEB = 0;
        public const int OFFICE_CAU = 1;
        public const int OFFICE_ADM = 2;

        /// <summary>
        /// Registros Académicos
        /// </summary>
        public const int OFFICE_RA = 3;

        public const int OFFICE_DFI = 4;
        public const int OFFICE_DFCE = 5;
        public const int OFFICE_COU = 8;
        public const int OFFICE_DIRESC = 9;
        public const int OFFICE_PRS = 10;
        public const int OFFICE_OPP = 11;
        public const int OFFICE_SFU = 12;
        public const int OFFICE_CAJ = 13;
        public const int OFFICE_OEA = 14;
        public const int OFFICE_OFG = 15;

        public static readonly Dictionary<int, string> OFFICES_NAMES =
            new Dictionary<int, string>
            {
                {OFFICE_WEB, "PAgina Web" },
                {OFFICE_CAU, "Centro de atención"},
                {OFFICE_ADM, "Admisión"},
                {OFFICE_RA, "Registros academicos"},
                {OFFICE_DFI, "DFI"},
                {OFFICE_DFCE, "DFCE"},
                {OFFICE_COU, "Convalidaciones"},
                {OFFICE_DIRESC, "Directores de escuela"},
                {OFFICE_PRS, "Proyección social"},
                {OFFICE_OPP, "Practica profesionales"},
                {OFFICE_SFU, "SFU"},
                {OFFICE_CAJ, "Caja"},
                {OFFICE_OEA, "Evaluación del aprendizaje"},
                {OFFICE_OFG, "Gestión docente"},
            };

        public static readonly Dictionary<int, string> OFFICES_NAMES_MODULE =
            new Dictionary<int, string>
            {
                {OFFICE_CAU, "bo_cau-personal"},
                {OFFICE_RA, "bo_rau-personal"},
                {OFFICE_OEA, "bo_learning_assessment"},
                {OFFICE_OFG, "bo_teacher_management"},
            };

        public static List<string> PRONABEC_ADM_TYPES =
            new List<string>
            {
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "63",
                "67",
                "71"
            };

        public const string PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA_OLD = "101";
        public const string PROGRAM_INGENIERIA_ELECTRONICA_OLD = "102";
        public const string PROGRAM_INGENIERIA_DE_SISTEMAS_E_INFORMATICA = "103";
        public const string PROGRAM_INGENIERIA_EN_AGRONEGOCIOS = "104";
        public const string PROGRAM_INGENIERIA_CIVIL = "105";
        public const string PROGRAM_ARQUITECTURA = "106";
        public const string PROGRAM_INGENIERIA_AMBIENTAL = "107";
        public const string PROGRAM_INGENIERIA_INDUSTRIAL = "108";
        public const string PROGRAM_INGENIERIA_ELECTRICA = "109";
        public const string PROGRAM_INGENIERIA_DE_MINAS = "110";
        public const string PROGRAM_INGENIERIA_MECANICA = "111";
        public const string PROGRAM_INGENIERIA_MECATRONICA = "112";
        public const string PROGRAM_INGENIERIA_ELECTRONICA = "113";
        public const string PROGRAM_INGENIERIA_EMPRESARIAL = "114";
        public const string PROGRAM_ADMINISTRACION = "308";
        public const string PROGRAM_ADMINISTRACION_MARKETING_Y_NEGOCIOS_INTERNACIONALES = "309";
        public const string PROGRAM_CONTABILIDAD_Y_FINANZAS = "310";
        public const string PROGRAM_CONTABILIDAD_Y_AUDITORIA = "311";
        public const string PROGRAM_DERECHO = "312";
        public const string PROGRAM_ECONOMIA = "313";
        public const string PROGRAM_CIENCIAS_Y_TECNOLOGIAS_DE_LA_COMUNICACION = "314";
        public const string PROGRAM_ADMINISTRACION_Y_NEGOCIOS_INTERNACIONALES = "315";
        public const string PROGRAM_ADMINISTRACION_Y_FINANZAS = "316";
        public const string PROGRAM_ADMINISTRACION_Y_RECURSOS_HUMANOS = "317";
        public const string PROGRAM_ADMINISTRACION_Y_MARKETING = "318";
        public const string PROGRAM_ADMINISTRACION_Y_GESTION_PUBLICA = "319";
        public const string PROGRAM_PSICOLOGIA = "501";
        public const string PROGRAM_MEDICINA_HUMANA = "502";
        public const string PROGRAM_ODONTOLOGIA = "503";
        public const string PROGRAM_ENFERMERIA = "504";
        public const string PROGRAM_OBSTETRICIA = "505";
        public const string PROGRAM_SEGUNDA_ESPECIALIDAD_ORTODONCIA_Y_ORTOPEDIA_MAXILAR = "506";
        public const string PROGRAM_TERAPIA_FISICA_Y_REHABILITACION = "507";
        public const string PROGRAM_LABORATORIO_CLÍNICO_Y_ANATOMÍA_PATOLÓGICA = "508";
        public const string PROGRAM_RADIOLOGIA = "509";
        public const string PROGRAM_SEGUNDA_ESPECIALIDAD_ODONTOPEDIATRÍA = "510";
        public const string PROGRAM_EDUCACION = "601";

        public static readonly Dictionary<string, string> PROGRAMS = new Dictionary<string, string>
        {
            {"101", "INGENIERIA DE SISTEMAS E INFORMATICA"},
            {"102", "INGENIERIA ELECTRONICA"},
            {"103", "INGENIERIA DE SISTEMAS E INFORMATICA"},
            {"104", "INGENIERIA EN AGRONEGOCIOS"},
            {"105", "INGENIERIA CIVIL"},
            {"106", "ARQUITECTURA"},
            {"107", "INGENIERIA AMBIENTAL"},
            {"108", "INGENIERIA INDUSTRIAL"},
            {"109", "INGENIERIA ELECTRICA"},
            {"110", "INGENIERIA DE MINAS"},
            {"111", "INGENIERIA MECANICA"},
            {"112", "INGENIERIA MECATRONICA"},
            {"113", "INGENIERIA ELECTRONICA"},
            {"114", "INGENIERIA EMPRESARIAL"},
            {"308", "ADMINISTRACION"},
            {"309", "ADMINISTRACION: MARKETING Y NEGOCIOS INTERNACIONALES"},
            {"310", "CONTABILIDAD Y FINANZAS"},
            {"311", "CONTABILIDAD Y AUDITORIA"},
            {"312", "DERECHO"},
            {"313", "ECONOMIA"},
            {"314", "CIENCIAS Y TECNOLOGIAS DE LA COMUNICACION"},
            {"315", "ADMINISTRACION Y NEGOCIOS INTERNACIONALES"},
            {"316", "ADMINISTRACION Y FINANZAS"},
            {"317", "ADMINISTRACION Y RECURSOS HUMANOS"},
            {"318", "ADMINISTRACION Y MARKETING"},
            {"319", "ADMINISTRACION Y GESTION PUBLICA"},
            {"501", "PSICOLOGIA"},
            {"502", "MEDICINA HUMANA"},
            {"503", "ODONTOLOGIA"},
            {"504", "ENFERMERIA"},
            {"505", "OBSTETRICIA"},
            {"506", "SEGUNDA ESPECIALIDAD ORTODONCIA Y ORTOPEDIA MAXILAR"},
            {"507", "TERAPIA FISICA Y REHABILITACION"},
            {"508", "LABORATORIO CLÍNICO Y ANATOMÍA PATOLÓGICA"},
            {"509", "RADIOLOGIA"},
            {"510", "SEGUNDA ESPECIALIDAD ODONTOPEDIATRÍA"},
            {"601", "EDUCACION"}
        };

        public static readonly Dictionary<string, string> CAMPUS_NAME = new Dictionary<string, string>
        {
            {"S01", "Huancayo"},
            {"F01", "Arequipta"},
            {"F02", "Lima"},
            {"F03", "Cusco"},
            {"V00", "Virtual"},
        };

        /// <summary>
        /// Escuelas Académicas UC
        /// </summary>
        public static readonly List<string> PROGRAM_LIST = new List<string>(PROGRAMS.Keys);
    }
}