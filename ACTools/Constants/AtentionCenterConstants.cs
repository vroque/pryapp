﻿using System.Collections.Generic;

namespace ACTools.Constants
{
    public static class AtentionCenterConstants
    {
        /*********************************************
         * PERÍODOS IMPORTANTES PARA CONSULTA DE DATOS
         *********************************************/

        /// <summary>
        /// Período académico de migración de datos hacia BANNER
        /// </summary>
        public const int PeriodOfDataMigrationToBanner = 201710;

        /**************************
         * ESTADOS DE UNA SOLICITUD
         **************************/

        public const int STATE_CANCEL = 0;
        public const int STATE_INITIAL = 1;
        public const int STATE_PAYMENT_PENDING = 2;
        public const int STATE_ATENTION = 3;
        public const int STATE_DELIVERY_PENDING = 4;
        public const int STATE_APROVAL_PENDING = 5;
        public const int STATE_PROCESS_PENDING = 6;
        public const int STATE_DOCUMENT_RECEPTION = 7;
        public const int STATE_DOCUMENT_VERIFICATION = 8;
        public const int STATE_CONVALIDATION = 9;
        public const int STATE_CONVALIDATION_VERIFICATION = 10;
        public const int STATE_CONVALIDATION_ACTION = 11; // aqui cau cou convalida
        public const int STATE_CAP_PENDING = 12;
        public const int STATE_RESOLUTION_PENDING = 13;
        public const int STATE_FINAL = 100;

        public static readonly Dictionary<int, string> STATES_NAMES =
            new Dictionary<int, string>
            {
                {STATE_CANCEL, "CANCELADO"},
                {STATE_INITIAL, "REGISTRADO"},
                {STATE_PAYMENT_PENDING, "PENDIENTE DE PAGO"},
                {STATE_ATENTION, "EN EL CENTRO DE ATENCIÓN"},
                {STATE_DELIVERY_PENDING, "PENDIENTE DE ENTREGA"},
                {STATE_APROVAL_PENDING, "PENDIENTE DE APROBACIÓN"},
                {STATE_PROCESS_PENDING, "PENDIENTE DE PROCESAR"},
                {STATE_DOCUMENT_RECEPTION, "EN RECEPCION DE DOCUMENTOS"},
                {STATE_DOCUMENT_VERIFICATION, "VERIFICACIÓN DE DOCUMENTOS"},
                {STATE_CONVALIDATION, "EN CONVALIDACIÓN"},
                {STATE_CONVALIDATION_VERIFICATION, "VERIFICACIÓN DE CONVALIDACIÓN"},
                {STATE_CONVALIDATION_ACTION, "PENDIENTE DE CONVALIDACIÓN"},
                {STATE_CAP_PENDING, "PENDIENTE EJECUCIÓN DE CAP"},
                {STATE_RESOLUTION_PENDING, "PENDIENTE GENERACIÓN DE RESOLUCIÓN"},
                {STATE_FINAL, "FINALIZADA"},
            };

        /*********************
         * TIPOS DE DOCUMENTOS
         *********************/

        public const int DOC_STUDIES_CERTIFICATE = 1;
        public const int DOC_STUDIES_PROOF = 2; // constancia de estudios general, historica y por periodo
        public const int DOC_ACADEMIC_HISTORY = 3;
        public const int DOC_ENROLLMENT_CONSOLIDATE = 5;
        public const int DOC_ENTRY_PROOF = 6;
        public const int DOC_ENROLLMENT_PROOF = 7;
        public const int DOC_SPECIAL_PROOF = 8;
        public const int DOC_3510SUPERIOR_PROOF = 9;
        public const int DOC_REPORT_CARD = 12;
        public const int DOC_STUDENT_CARD = 14;
        public const int DOC_PROMOTIONAL_3510SUPERIOR_PROOF = 22;
        public const int DOC_PROMOTIONAL_STUDIES_CERTIFICATE = 23;
        public const int DOC_AVERAGE_STUDIES_PROOF = 24; // constancia de estudios con promeido acumulado
        public const int DOC_CUSTOM_STUDIES_PROOF = 25; // constancia de estudios personalizada
        public const int DOC_CULM_CAREER_STUDIES_PROOF = 26; // constancia de estudios carrera culmidada

        /// <summary>
        /// Constancia de Matrícula PRONABEC
        /// </summary>
        public const int DOC_PROOF_OF_ENROLLMENT_PRONABEC = 32;

        public const int DOC_CONVALIDATION = 33;

        /// <summary>
        /// Constancia de Matrícula Promocional
        /// </summary>
        public const int DOC_PROOF_OF_ENROLLMENT_PROMOTIONAL = 44;

        /// <summary>
        /// Constancia de Egresado
        /// </summary>
        public const int DOC_GRADUATION_PROOF = 49;

        public const int DOC_ENROLLMENT_PROOF_WITH_DATE = 51;
        public const int DOC_STUDIES_PROOF_WITH_DATE = 52;
        public const int DOC_CONVALIDATION_EXTENSION = 54;

        /// <summary>
        /// CIC: Constancia de matrícula
        /// </summary>
        public const int DOC_CIC_PROOF_OF_ENROLLMENT = 55; // TODO: Usar el ID correcto antes de pasar a producción

        /// <summary>
        /// CIC: Certificado de estudios
        /// </summary>
        public const int DOC_CIC_STUDIES_CERTIFICATE = 56; // TODO: Usar el ID correcto antes de pasar a producción

        /// <summary>
        /// CIC: Diploma
        /// </summary>
        public const int DOC_CIC_DIPLOMA = 57; // TODO: Usar el ID correcto antes de pasar a producción

        /// <summary>
        /// CIC: Constancia de Idioma Extranjero - Uso interno UC
        /// </summary>
        public const int DOC_CIC_CAIE = 58; // TODO: Usar el ID correcto antes de pasar a producción

        /// <summary>
        /// CIC: Constancia de Idioma Extranjero - Uso Externo
        /// </summary>
        public const int DOC_CIC_CAIE_EXT = 59; // TODO: Usar el ID correcto antes de pasar a producción

        /// <summary>
        /// CIC: Constancia de Idioma Extranjero - Instituto
        /// </summary>
        public const int DOC_CIC_CAIE_INST = 60; // TODO: Usar el ID correcto antes de pasar a producción

        /// <summary>
        /// vuc: Constancia de Vida Universitaria.
        /// </summary>
        public const int DOC_UNIVERSITY_LIFE_CERTIFICATE = 61;

        /// <summary>
        /// Duplicado de constancia de egresado
        /// </summary>

        public const int DOC_DUPLICATE_PROOF_GRADUATE = 62;

        /************************
         * TIPOS DE CONVALIDACIÓN
         ************************/

        public const int CONVA_TYPE_FIRST = 1;
        public const int CONVA_TYPE_SECOND = 2;
        public const int CONVA_TYPE_EXTENSION = 3;

        /*******************
         * CONCEPTOS DE PAGO
         *******************/

        public const string BACHILLER_CONCEPT_ID = "C07";
    }
}