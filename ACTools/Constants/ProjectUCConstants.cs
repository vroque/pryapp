﻿using System.Collections.Generic;

namespace ACTools.Constants
{
    public class ProjectUCConstants
    {
        #region Tipos de integrantes de un proyecto

        public const int MemberTypeOwner = 1;
        public const int MemberTypeLeader = 2;
        public const int MemberTypeMember = 3;

        public static readonly Dictionary<int, string> MemberTypesName =
            new Dictionary<int, string>
            {
                {MemberTypeOwner, "Dueño" },
                {MemberTypeLeader, "Responsable"},
                {MemberTypeMember, "Integrante"}
            };

        #endregion
    }
}
