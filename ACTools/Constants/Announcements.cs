﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACTools.Constants
{
    class Announcements
    {
        public static Dictionary<string, string> TYPES_STUDIES_PLANS =
            new Dictionary<string, string>()
                {
                    {"ADM","PGR"},
                    {"ADG","PGT"},
                    {"ADE","PGE"},
                    {"ADV","PGR"},
                    {"ADP","PGR"}
                };
    }
}
