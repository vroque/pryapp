﻿using System.Collections.Generic;

namespace ACTools.Constants
{
    public static class PersonalConstants
    {
        // versión actual de la ficha socieconomico
        public static int SOC_REC_VERSION = 1;

        /**
         * PARENTESCO: CAU.Kinship
         */

        public const int Padres = 1;
        public const int Hijos = 2;
        public const int Conyuges = 3;
        public const int Suegros = 4;
        public const int YernoNuera = 5;
        public const int Abuelos = 6;
        public const int Nietos = 7;
        public const int Hermanos = 8;
        public const int Cunados = 9;
        public const int Bisabuelos = 10;
        public const int Bisnietos = 11;
        public const int Tios = 12;
        public const int Sobrinos = 13;
        public const int TiosAbuelos = 14;
        public const int Primos = 15;
        public const int Apoderado = 16;


        public static readonly Dictionary<int, string> KinshipDictionary =
            new Dictionary<int, string>
            {
                {Padres, "Padres"},
                {Hijos, "Hijos"},
                {Conyuges, "Cónyuges"},
                {Suegros, "Suegros"},
                {YernoNuera, "Yerno/Nuera"},
                {Abuelos, "Abuelos"},
                {Nietos, "Nietos"},
                {Hermanos, "Hermanos"},
                {Cunados, "Cuñados"},
                {Bisabuelos, "Bisabuelos"},
                {Bisnietos, "Bisnietos"},
                {Tios, "Tíos"},
                {Sobrinos, "Sobrinos"},
                {TiosAbuelos, "Tíos abuelos"},
                {Primos, "Primos"},
                {Apoderado, "Apoderado"}
            };
    }
}