﻿using System.Collections.Generic;

namespace ACTools.Constants
{
    public class ClaimsBookConstants
    {

        public const string LDR_ID_COMPANY_UCCI = "01";

        /// <summary>
        /// CONSTANTES DE ESTADO DE RECLAMO
        /// </summary>
        public const string LDR_STATE_PAGE_DISMISSED = "0";
        public const string LDR_STATE_PAGE_PENDING = "1";
        public const string LDR_STATE_PAGE_DERIVATED = "2";
        public const string LDR_STATE_PAGE_ANSWERED = "4"; // -> Esta en area legal
        public const string LDR_STATE_PAGE_RESOLVED = "99";
        public const string LDR_STATE_PAGE_FINALIZED = "100";

        /// <summary>
        /// CONSTANTES DE TIPO DE ORIGEN DE RECLAMO
        /// </summary>
        public const string LDR_TYPE_ORIGIN_PAGE_DIGITAL = "0";
        public const string LDR_TYPE_ORIGIN_PAGE_PHYSICAL = "1";

        /// <summary>
        /// CONSTANTES DE ESTADO DE RESPUESTA DE RECLAMO
        /// </summary>
        public const string LDR_STATE_ANSWER_REJECTED = "0";
        public const string LDR_STATE_ANSWER_WAIT = "1";
        public const string LDR_STATE_ANSWER_ACCEPTED = "99";
        public const string LDR_STATE_ANSWER_ACCEPTED_FINISH= "100";


        /// <summary>
        /// CONSTANTES DE TIPO DE ARCHIVO
        /// </summary>
        public const string LDR_STATE_FILE_CREATE = "1";
        public const string LDR_STATE_FILE_DERIVATED = "2";
        public const string LDR_STATE_FILE_DISMISSED = "3";
        public const string LDR_STATE_FILE_ANSWER = "4";
        public const string LDR_STATE_FILE_RESOLVED = "99";
        public const string LDR_STATE_FILE_FINALIZED = "100";

        /// <summary>
        /// CONSTANTES DE ESTADO DE DERIVACION 
        /// </summary>
        public const string LDR_STATE_DERIVATE_DISMISSED = "0";
        public const string LDR_STATE_DERIVATE_PENDING = "1";
        //public const string LDR_STATE_DERIVATE_DERIVATED = "2";
        public const string LDR_STATE_DERIVATE_RESOLVED = "99";
        public const string LDR_STATE_DERIVATE_FINALIZED = "100";

        public const string LDR_STATE_DERIVATE_RECHAZED_ANSWER = "200"; // ESTADO FANTASMA PARA RECONOCER QUE SE ESTA RECHAZANDO LA RESPUESTA DE LA DERIVACIÓN

        /// <summary>
        /// CONSTANTES, TIPO DE DERIVACIÓN, AREA INVOLUCRADA Y AREA LEGAL
        /// </summary>
        public const string LDR_TYPE_DERIVATE_INVOLVED_AREA = "0";
        public const string LDR_TYPE_DERIVATE_LEGAL_AREA = "1";

        /// <summary>
        /// CONSTANTES PARA SABER SI UN JEFE DERIVO A UN COLABORADOR 1-> FUE DERIVADO; 0-> AUN NO FUE DERIVADO
        /// SOLO JEFES PUEDEN DERIVAR
        /// </summary>
        public const string LDR_STATE_STAFF_DERIVATED = "1";
        public const string LDR_STATE_STAFF_NOT_DERIVATED = "0";

        /// <summary>
        /// CONSTANTES DE TIPO DE COLABORADOR JEFE->1 ; COLABORADOR->2
        /// </summary>
        public const string LDR_TYPE_INVOLVED_BOSS = "1";
        public const string LDR_TYPE_INVOLVED_COLLABORATOR = "2";

        /// <summary>
        /// CONSTANTES PARA TIPO DE USUARIO
        /// </summary>
        public const string LDR_TYPE_USER_AD = "0";
        public const string LDR_TYPE_USER_INVOLVED_AREA = "1";
        public const string LDR_TYPE_USER_LEGAL_AREA = "2";

        /// <summary>
        /// CONSTANTES PARA ESTADO DE ENCUESTA DE SATISFACIÓN
        /// </summary>
        public const string LDR_STATE_SATISFACTION_SURVEY_PENDING = "1";
        public const string LDR_STATE_SATISFACTION_SURVEY_RESOLVED = "99";



        public const string LDR_CODE_FUNTIONAL_UNITY_LEGAL_AREA = "00000501";
        public const string LDR_PIDM_BOOS_UNITY_LEGAL_AREA = "62195";



        /// <summary>
        /// DICCIONARIO DE ESTADO DE RECLAMO
        /// </summary>
        public static Dictionary<string, string> LDR_PAGE_STATUS =
        new Dictionary<string, string>()
        {
            {LDR_STATE_PAGE_PENDING , "PENDING"},
            {LDR_STATE_PAGE_DERIVATED , "DERIVATED"},
            {LDR_STATE_PAGE_DISMISSED, "DISMISSED"},
            {LDR_STATE_PAGE_RESOLVED, "RESOLVED"},
            {LDR_STATE_PAGE_FINALIZED, "FINALIZED"}
        };

        /// <summary>
        /// DICCIONARIO DE TIPO DE ORIGEN DE RECLAMO
        /// </summary>
        public static Dictionary<string, string> LDR_PAGE_TYPE_ORIGIN =
        new Dictionary<string, string>()
        {
            {LDR_TYPE_ORIGIN_PAGE_DIGITAL , "DIGITAL"},
            {LDR_TYPE_ORIGIN_PAGE_PHYSICAL , "FISICO"}
        };

        /// <summary>
        /// DICCIONARIO DE TIPO DE ARCHIVO
        /// </summary>
        public static Dictionary<string, string> LDR_FILE_STATUS =
        new Dictionary<string, string>()
        {
            {LDR_STATE_FILE_CREATE , "CREATE"},
            {LDR_STATE_FILE_DERIVATED , "DERIVATED"},
            {LDR_STATE_FILE_ANSWER , "ANSWER"},
            {LDR_STATE_FILE_RESOLVED , "RESOLVED"},
            {LDR_STATE_FILE_FINALIZED, "FINALIZED"}
        };
}
}
