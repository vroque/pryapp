﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACTools.Constants
{
    public class LearningAssessmentConstants
    {
        /// <summary>
        /// PRIMER CARACTER DEL PARTE PERIODO INDICA LA MODALIDAD
        /// </summary>
        public const string PART_PERIOD_MODALITY_UREG = "R";
        public const string PART_PERIOD_MODALITY_UPGT = "W";
        public const string PART_PERIOD_MODALITY_UVIR = "V";

        /// <summary>
        /// SEGUNDO CARACTER DEL PARTE PERIODO INDICA LA SEDE
        /// </summary>
        public const string PART_PERIOD_CAMPUS_HYO = "H";
        public const string PART_PERIOD_CAMPUS_ARQP = "A";
        public const string PART_PERIOD_CAMPUS_CUS = "C";
        public const string PART_PERIOD_CAMPUS_LIMA = "L";
        public const string PART_PERIOD_CAMPUS_VIR = "V";

        public static readonly Dictionary<string, string> CAMPUS_VALUES = new Dictionary<string, string> {
            {PART_PERIOD_CAMPUS_HYO, "S01"},
            {PART_PERIOD_CAMPUS_ARQP, "F01"},
            {PART_PERIOD_CAMPUS_LIMA, "F02"},
            {PART_PERIOD_CAMPUS_CUS, "F03"},
            {PART_PERIOD_CAMPUS_VIR, "V00"},
        };

        public static readonly Dictionary<string, string> DEPARTMENT_VALUES= new Dictionary<string, string> {
            {PART_PERIOD_MODALITY_UREG, _acad.DEPA_REG},
            {PART_PERIOD_MODALITY_UPGT, _acad.DEPA_PGT},
            {PART_PERIOD_MODALITY_UVIR, _acad.DEPA_VIR},
        };

        /// <summary>
        /// Valores para reclamo de notas
        /// </summary>
        public const string OEA_TYPE_NOTE_CLAIM_REQUALIFICATION = "RECALIFICACION";
        public const string OEA_TYPE_NOTE_CLAIM_RECTIFICATION = "RECTIFICACION";

        public static readonly Dictionary<string, string> OEA_TYPES_NOTE_CLAIM = new Dictionary<string, string>
        {
            {OEA_TYPE_NOTE_CLAIM_REQUALIFICATION, "Recalificación"},
            {OEA_TYPE_NOTE_CLAIM_RECTIFICATION, "Rectificación"}
        };

        // Estados de reclamo de nota
        public const int OEA_STATUS_NOTE_CLAIM_REGISTERED = 1;
        public const int OEA_STATUS_NOTE_CLAIM_CAS = 2;
        public const int OEA_STATUS_NOTE_CLAIM_TEACHER = 3;
        public const int OEA_STATUS_NOTE_CLAIM_INVOLVED_AREA = 4;
        public const int OEA_STATUS_NOTE_CLAIM_RAU = 5;
        public const int OEA_STATUS_NOTE_CLAIM_FINALIZED = 6;

        // Estados de reporte
        public const int OEA_STATUS_NOTE_CLAIM_REPORT_PENDING_REVISION = 7;
        public const int OEA_STATUS_NOTE_CLAIM_REPORT_APPROVED = 8;
        public const int OEA_STATUS_NOTE_CLAIM_REPORT_DISAPPROVED = 9;

        public static readonly Dictionary<int, string> OEA_STATUS_NOTE_CLAIM = new Dictionary<int, string>
        {
            {OEA_STATUS_NOTE_CLAIM_REGISTERED, "Registrado"},
            {OEA_STATUS_NOTE_CLAIM_CAS, "Pendiente de validación CAS"},
            {OEA_STATUS_NOTE_CLAIM_TEACHER, "Pendiente de informe docente"},
            {OEA_STATUS_NOTE_CLAIM_INVOLVED_AREA, "Pendiente de validar informe"},
            {OEA_STATUS_NOTE_CLAIM_RAU, "Pendiente de Registros Académicos"},
            {OEA_STATUS_NOTE_CLAIM_FINALIZED, "Finalizado"},
        };


        public static readonly Dictionary<int, string> OEA_STATUS_NOTE_CLAIM_REPORT = new Dictionary<int, string>
        {
            {OEA_STATUS_NOTE_CLAIM_REPORT_PENDING_REVISION, "Pendiente de revisión"},
            {OEA_STATUS_NOTE_CLAIM_REPORT_APPROVED, "Aprobado"},
            {OEA_STATUS_NOTE_CLAIM_REPORT_DISAPPROVED, "No Aprobado"},
        };


        /// <summary>
        /// Obtener modalidad de parte periodo
        /// </summary>
        /// <param name="parterm">Parte periodo</param>
        /// <returns></returns>
        public static string GetModalityByPartTerm(string parterm) {
            return (DEPARTMENT_VALUES[parterm.Substring(0, 1)]);
        }

        /// <summary>
        /// Obtener sede de parte periodo
        /// </summary>
        /// <param name="parterm">Parte periodo</param>
        /// <returns></returns>
        public static string GetCampusByPartTerm(string parterm)
        {
            return (CAMPUS_VALUES[parterm.Substring(1, 1)]);
        }

        /// <summary>
        /// Obtener Modulo de parte periodo
        /// </summary>
        /// <param name="parterm">Parte periodo</param>
        /// <returns></returns>
        public static string GetModuleByPartTerm(string parterm)
        {
            return (parterm.Substring(2, 1));
        }

        /*public static string modalityByPartTerm(this string parterm) {
            return (parterm.Substring(2, 1));
        }*/
    }
}
