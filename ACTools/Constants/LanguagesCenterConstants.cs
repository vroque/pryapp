﻿using System;
using System.Collections.Generic;
using _cau = ACTools.Constants.AtentionCenterConstants;

namespace ACTools.Constants
{
    public static class LanguagesCenterConstants
    {
        public const string APP_NAME = "CAU WEBAPP";

        private const int Zero = 0;

        /// <summary>
        /// Activo
        /// </summary>
        public const int ACTIVE = 1;

        /// <summary>
        /// Inactivo
        /// </summary>
        public const int INACTIVE = Zero;

        /// <summary>
        /// Guardado con éxito
        /// </summary>
        public const int SAVE_SUCCESSFUL = 1;

        /// <summary>
        /// Error al guardar
        /// </summary>
        public const int SAVE_FAILED = Zero;

        /// <summary>
        /// No ha realizado el pago por el trámite de bachiller
        /// </summary>
        public const int NOT_PAID = Zero;

        /// <summary>
        /// El estudiante realizó el pago por el trámite de bachiller
        /// </summary>
        public const int PAID = 1;

        /// <summary>
        /// El estudiante no cumple las reglas del CIC para tramitar la Constancia de Acreditación de Idioma Extranjero
        /// </summary>
        public const int FAILS_CAIE_RULES = 2;

        /// <summary>
        /// Código de estudiante no encontrado
        /// </summary>
        public const int STUDENT_NOT_FOUND = 3;

        public const string MALE = "1";
        public const string FEMALE = "0";


        /***********
         * Idiomas *
         ***********/

        public const string INGLES = "I01";
        public const string PORTUGUES = "I02";
        public const string ITALIANO = "I03";
        public const string FRANCES = "I04";
        public const string QUECHUA = "I05";
        public const string ALEMAN = "I06";

        public static readonly Dictionary<string, string> LANGUAGES = new Dictionary<string, string>
        {
            {INGLES, "Inglés"},
            {PORTUGUES, "Portugués"},
            {ITALIANO, "Italiano"},
            {FRANCES, "Francés"},
            {QUECHUA, "Quechua"},
            {ALEMAN, "Alemán"}
        };

        public static readonly List<string> ACTIVE_LANGUAGES_LIST = new List<string>
        {
            INGLES,
            PORTUGUES,
            ITALIANO,
            FRANCES,
            QUECHUA,
            ALEMAN
        };

        /****************************
         * Nota mínima para aprobar *
         ****************************/

        /// <summary>
        /// Nota mínima para aprobar exámenes de tipo 'X', 'U' y 'C'
        /// </summary>
        public const int MinimumMarkToPassExam = 11;

        /// <summary>
        /// Nueva nota mínima para aprobar exámenes de tipo "U" a partir del 2019/01/01
        /// </summary>
        public const int NewMinimumMarkToPassU = 14;
        public static readonly DateTime DateToApplyNewMinimumMarkToPassU = new DateTime(2019, 1, 1);

        /// <summary>
        /// Nota mínima para aprobar ciclos cursados en el CIC
        /// </summary>
        public const int MinimumMarkToPassCycle = 12;

        /**********************
         * Inglés profesional *
         **********************/

        /// <summary>
        /// Ciclo mínimo aprobado para poder matricularse en inglés profesional en la UC
        /// </summary>
        public const int MinimumCycleToEnrollInProfessionalEnglish = 7;

        /***************
         * Modalidades *
         ***************/

        public const string M_SUPER_INTENSIVO = "S";
        public const string M_INTENSIVO = "I";
        public const string M_REGULAR = "R";
        public const string M_PRIMARIA = "P";
        public const string M_EXAMEN = "X";
        public const string M_SUFICIENCIA = "U";
        public const string M_KIDS = "K";
        public const string M_SUFICIENCIA_EXT = "F";
        public const string M_SUPER_INTENSIVE_ONLINE = "L";
        public const string M_CONVALIDACION = "C";

        public static readonly Dictionary<string, string> MODALITIES_NAME = new Dictionary<string, string>
        {
            {M_SUPER_INTENSIVO, "Super Intensivo"},
            {M_INTENSIVO, "Intensivo"},
            {M_REGULAR, "Regular"},
            {M_PRIMARIA, "Primaria"},
            {M_EXAMEN, "Examen"},
            {M_SUFICIENCIA, "Suficiencia"},
            {M_KIDS, "Kids"},
            {M_SUFICIENCIA_EXT, "Suficiencia Externa"},
            {M_SUPER_INTENSIVE_ONLINE, "Super Intensivo OL"},
            {M_CONVALIDACION, "Convalidación"},
        };


        public static readonly List<string> ACTIVE_LANGUAGES_MODALITIES = new List<string>
        {
            M_SUPER_INTENSIVO,
            M_INTENSIVO,
            M_REGULAR,
            M_PRIMARIA,
            M_EXAMEN,
            M_SUFICIENCIA,
            M_KIDS,
            M_SUFICIENCIA_EXT,
            M_SUPER_INTENSIVE_ONLINE,
            M_CONVALIDACION
        };

        /// <summary>
        /// Modalidades a las que puede acceder un estudiante de la U
        /// </summary>
        public static readonly List<string> UC_ACTIVE_LANGUAGES_MODALITIES = new List<string>
        {
            M_SUPER_INTENSIVO,
            M_INTENSIVO,
            M_REGULAR,
            M_SUPER_INTENSIVE_ONLINE,
        };

        /***********************
         * REPO DOCUMENTOS CIC *
         ***********************/

        /// <summary>
        /// PDF: Documentos CIC en General
        /// </summary>
        public const string SRC_CIC_PDF = "output-cic";

        /// <summary>
        /// PDF: Constancia de Acreditación de Idioma Extranjero
        /// </summary>
        public const string SRC_CAIE_PDF = "output-cic-caie";

        /// <summary>
        /// URI: Constancia de Acreditación de Idioma Extranjero
        /// </summary>
        public const string URI_CAIE_PDF = "output-cic-caie-client";

        /// <summary>
        /// URI: Documentos CIC en General
        /// </summary>
        public const string URI_CIC_PDF = "output-cic-client";


        /*********************************************************************
         * Concepto (IDConcepto) de pago por los documentos que emite el CIC *
         *********************************************************************/

        public const string BACHILLER_CONCEPT = "C07";
        public const string CAIE_EN_CONCEPT = "C05";
        public const string CAIE_PT_CONCEPT = "C09";
        public const string CAIE_IT_CONCEPT = "C08";

        public static readonly List<string> CAIE_CONCEPTS_LIST = new List<string>
        {
            CAIE_EN_CONCEPT,
            CAIE_PT_CONCEPT,
            CAIE_IT_CONCEPT,
            BACHILLER_CONCEPT
        };

        public static readonly Dictionary<string, string> CAIE_CONCEPTS_BY_LANGUAGE_ID = new Dictionary<string, string>
        {
            {INGLES, CAIE_EN_CONCEPT},
            {PORTUGUES, CAIE_PT_CONCEPT},
            {ITALIANO, CAIE_IT_CONCEPT},
            {FRANCES, string.Empty},
            {QUECHUA, string.Empty},
            {ALEMAN, string.Empty}
        };


        /*******************************************
         * NIVELES DE IDIOMAS: CIC.LanguageProgram *
         *******************************************/

        public const int PROG_UNKNOWN = Zero;
        public const int PROG_REGULAR = 1;
        public const int PROG_SCHOOLCHILD = 2;
        public const int PROG_VIRTUAL = 3;

        public static readonly Dictionary<int, string> LANGUAGE_PROGRAM = new Dictionary<int, string>
        {
            {PROG_REGULAR, "Regular"},
            {PROG_SCHOOLCHILD, "Escolar"},
            {PROG_VIRTUAL, "Virtual"}
        };

        /***********************************
         * CIC SETTINGS - CIC.SettingsType *
         ***********************************/

        /// <summary>
        /// Modalidades de Idiomas
        /// </summary>
        public const int LanguageModalities = 1;

        /// <summary>
        /// Nivel de Idioma con el que se imprimirá la Constancia de Idioma Extranjero
        /// </summary>
        public const int CaieLanguageLevel = 2;

        /*************************************************************
         * MODALIDADES DE IDIOMAS: 5to caracter del campo IDSeccionC *
         * CIC.Settings: SettingTypeId = 1                           *
         *************************************************************/

        public const string MOD_INTENSIVO = "I";
        public const string MOD_SUPERINTENSIVO = "S";
        public const string MOD_REGULAR = "R";
        public const string MOD_PRIMARIA = "P";

        /// <summary>
        /// Examen de clasificación que un estudiante realiza para estudiar en el CIC
        /// </summary>
        public const string MOD_EXAMEN_CLASIFICACION = "X";

        /// <summary>
        /// Examen de suficiencia para acreditar dominio de idioma,
        /// solo es válido para CAIE, Ingles Intermedio (Inglés Profesional) e Segundo Idioma
        /// </summary>
        public const string MOD_EXAMEN_SUFICIENCIA = "U";

        public const string MOD_KIDS = "K";
        public const string MOD_SUFICIENCIA_EXTERNA = "F";
        public const string MOD_SUPER_INTENSIVO_ONLINE = "L";
        public const string MOD_CONVALIDACION = "C";
        public const string MOD_SIMULACRO_Y_O_EXAMEN_CAMBRIDGE = "O";

        /// <summary>
        /// Secciones asignadas al dictado de clases
        /// </summary>
        public static readonly List<string> STUDY_CLASSROOM_LIST = new List<string>
        {
            MOD_INTENSIVO,
            MOD_SUPERINTENSIVO,
            MOD_REGULAR,
            MOD_PRIMARIA,
            MOD_KIDS,
            MOD_SUPER_INTENSIVO_ONLINE
        };

        /// <summary>
        /// Secciones asignadas a exámenes
        /// </summary>
        public static readonly List<string> EXAM_CLASSROOM_LIST = new List<string>
        {
            MOD_EXAMEN_CLASIFICACION,
            MOD_EXAMEN_SUFICIENCIA,
            MOD_SUFICIENCIA_EXTERNA,
            MOD_CONVALIDACION
        };

        /// <summary>
        /// Nombre Corto de Modalidades CIC
        /// </summary>
        public static readonly Dictionary<string, string> ModalityShortNameList = new Dictionary<string, string>
        {
            {MOD_INTENSIVO, "Intensivo"},
            {MOD_SUPERINTENSIVO, "S. Intensivo"},
            {MOD_REGULAR, "Regular"},
            {MOD_PRIMARIA, "Primaria"},
            {MOD_EXAMEN_CLASIFICACION, "Examen"},
            {MOD_EXAMEN_SUFICIENCIA, "E. Suficiencia"},
            {MOD_KIDS, "Kids"},
            {MOD_SUFICIENCIA_EXTERNA, "Suf. Ext."},
            {MOD_SUPER_INTENSIVO_ONLINE, "S. Int. Online"},
            {MOD_CONVALIDACION, "Convalidación"}
        };

        /************************************
         * DOCUMENTOS DEL CENTRO DE IDIOMAS *
         ************************************/
        public static readonly List<int> LanguagesCenterDocumentList = new List<int>
        {
            _cau.DOC_CIC_PROOF_OF_ENROLLMENT,
            _cau.DOC_CIC_STUDIES_CERTIFICATE,
            _cau.DOC_CIC_DIPLOMA,
            _cau.DOC_CIC_CAIE
        };

        /***********************************************
         * TIPOS DE DOCUMENTOS DEL CIC : CIC.Documents *
         ***********************************************/

        /// <summary>
        /// Constancia de Acreditación de Idioma Extranjero
        /// </summary>
        public const string DOC_CAIE = "CAIE";

        /// <summary>
        /// Constancia de Matrícula
        /// </summary>
        public const string DOC_CONS_MAT = "CMAT";

        /// <summary>
        /// Certificado de Estudios
        /// </summary>
        public const string DOC_CERT_EST = "CEST";

        /// <summary>
        /// Diploma CIC
        /// </summary>
        public const string DOC_DIPL_CIC = "DIPL";
    }
}
