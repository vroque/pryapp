namespace ACTools.Constants
{
    public static class AdryanConstants
    {
        /**********************************
         * Companies: ADRYAN.TBL_COMPANIA *    
         * 01 : UNIVERSIDAD CONTINENTAL   *
         * 02 : CORPORACION APEC          *
         **********************************/

        /// <summary>
        /// 01 : UNIVERSIDAD CONTINENTAL
        /// </summary>
        public const string ContinentalUniversityCompany = "01";

        /// <summary>
        /// 02 : CORPORACION APEC
        /// </summary>
        public const string ApecCorporationCompany = "02";

        /*****************************************************
         * Root functional unit: ADRYAN.TBL_UNIDAD_FUNCIONAL *
         * 00000001 : IC_LIMA_PRESIDENCIA                    *
         * 00000005 : UC PRESIDENCIA                         *
         *****************************************************/

        /// <summary>
        /// 00000001 : IC_LIMA_PRESIDENCIA
        /// </summary>
        public const string ContinentalInstituteRootFunctionalUnit = "00000001";

        /// <summary>
        /// 00000005 : UC PRESIDENCIA 
        /// </summary>
        public const string ContinentalUniversityRootFunctionalUnit = "00000005";

        public const string NoFunctionalUnit = "00000000";
    }
}
