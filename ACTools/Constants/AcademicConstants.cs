﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACTools.Constants
{
    public class AcademicConstants
    {
        public const string DEFAULT_DIV = "UCCI";
        public const string DEFAULT_LEVL = "PG";

        public const string LEVL_PREGRADO= "PG";
        public const string LEVL_TECNICO = "TT";
        public const string LEVL_MAESTRIA = "MA";
        public const string LEVL_IDIOMAS = "ID";
        public const string LEVL_POSTGRADO = "GR";
        public const string LEVL_EDUCACION_CONTINUA = "EC";
        public const string LEVL_DOCTORADO = "DO";

        public const string DEPA_REG = "UREG";
        public const string DEPA_VIR = "UVIR";
        public const string DEPA_PGT = "UPGT";

        public const string DEPA_REG_APEC = "ADM";
        public const string DEPA_VIR_APEC = "ADV";
        public const string DEPA_PGT_APEC = "ADG";

        public const int STATE_POSTULANT = 1; //"POSTULANTE";
        public const int STATE_ENROLMENT = 2; //"INGRESANTE";
        public const int STATE_STUDENT   = 3; //"MATRICULADO";
        public const int STATE_GRADUATE  = 4; //"EGRESADO";
        public const int STATE_BACHELOR  = 5; //"BACHILLER";
        public const int STATE_DEGREE    = 6; //"TITULADO";
        //public const int STATE_NO_ENROLMENT = 7; //"TITULADO";
        //public const int STATE_NO_STUDENT = 8; //"TITULADO";


        public static Dictionary<int, string> STUDENT_STATES =
        new Dictionary<int, string>()
        {
            {STATE_POSTULANT, "POSTULANTE"},
            {STATE_ENROLMENT, "INGRESANTE"},
            {STATE_STUDENT, "ESTUDIANTE"},
            {STATE_GRADUATE, "EGRESADO"},
            {STATE_BACHELOR, "BACHILLER"},
            {STATE_DEGREE, "TITULADO"},
            //{STATE_NO_ENROLMENT, "NO INGRESANTE"},
            //{STATE_NO_STUDENT, "NO ESTUDIANTE"},
        };
        public static Dictionary<int, string> STUDENT_MODULES =
        new Dictionary<int, string>()
        {
            {STATE_POSTULANT, "fd_postulante"},
            {STATE_ENROLMENT, "fd_ingresante"},
            {STATE_STUDENT, "fd_estudiante"},
            {STATE_GRADUATE, "fd_egresado"},
            {STATE_BACHELOR, "fd_egresado"},
            {STATE_DEGREE, "fd_egresado"},
            //{STATE_NO_ENROLMENT, "NO INGRESANTE"},
            //{STATE_NO_STUDENT, "NO ESTUDIANTE"},
        };
        public static Dictionary<string, string> TYPES_STUDIES_PLANS = 
        new Dictionary<string, string>()
        {
            {"ADM", "PGR"},
            {"ADG", "PGT"},
            {"ADE", "PGE"},
            {"ADV", "PGR"},
            {"ADP", "PGR"}
        };
        public static Dictionary<string, string> HEAD_FACULTAD_BY_CODE =
        new Dictionary<string, string>()
        {
            {"01", "FI"},  // FACULTAD INGENIERIA
            {"02", "FCE"}, // FACULTAD CC EMPRESA
            {"03", "FCS"}, // FACULTAD CC SALUD
            {"04", "FD"},  // FACULTAD DERECHO
            {"05", "FH"}   // FACULTAD HUMANIDADES
        };
        public static Dictionary<string, string> HEAD_FACULTAD_ID =
        new Dictionary<string, string>()
        {
            {"ING", "FI"},
            {"CE", "FCE"},
            {"CS", "FCS"}, // o FH
            {"DE", "FD"}
        };

        public static Dictionary<string, string> DEPARTMENT_NAMES_CAU =
        new Dictionary<string, string>()
        {
            {"UPGT", "Gente que trabaja"},
            {"UREG", "Regular"},
            {"UVIR", "Virtual"},
            {"UCEC", "Centro de Educación Continua"}
        };

        public static Dictionary<string, string> BANNER_SCORE_CONCEPT_SUST =
        new Dictionary<string, string>()
        {
            {"06", "2-C1"}, //MODIFICACIÓN AL C1
            {"07", "3-EP"}, //MODIFICACIÓN AL EP
            {"08", "4-C2"}, //MODIFICACIÓN AL C2
            {"09", "5-EF"}  //MODIFICACIÓN AL EF
        };
    }
}