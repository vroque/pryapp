﻿namespace ACTools.Constants
{
    public static class ExtracurricularActivitiesConstants
    {
        /***********************
        * REPO DOCUMENTOS VUC *
        ***********************/
        /// <summary>
        /// PDF: Documentos VUC en General
        /// </summary>
        public const string SRC_VUC_PDF = "output-vuc";

        /// <summary>
        /// URI: Documentos VUC en General
        /// </summary>
        public const string URI_VUC_PDF = "output-vuc-client";

        /***********************
        * REPO IMAGÉNES VUC *
        ***********************/
        /// <summary>
        /// Images de ejes
        /// </summary>
        public const string SRC_VUC_IMAGE_AXI = "output-vuc-axi";

        /// <summary>
        /// URI: Imágenes de ejes
        /// </summary>
        public const string URI_VUC_IMAGE_AXI = "output-vuc-axi-client";

        /// <summary>
        /// Images de actividades
        /// </summary>
        public const string SRC_VUC_IMAGE_ACTIVITY = "output-vuc-activity";

        /// <summary>
        /// URI: Imágenes de actividades
        /// </summary>
        public const string URI_VUC_IMAGE_ACTIVITY = "output-vuc-activity-client";

        /// <summary>
        /// URI: Consstante para la URI de las imagenes de actividades en el frontdesk
        /// </summary>
        public const string URI_FRONT_VUC_IMAGE_ACTIVITY = "output-front-vuc-activity-client";

        /// <summary>
        /// URI: Consstante para la URI de las imagenes de ejes en el frontdesk
        /// </summary>
        public const string URI_FRONT_VUC_IMAGE_AXI = "output-front-vuc-axi-client";

        /***********************
        * REPO FILES VUC *
        ***********************/
        /// <summary>
        /// Evidencias de los reconocimientos
        /// </summary>
        public const string SRC_VUC_FILE = "output-vuc-recog";

        /// <summary>
        /// URI: Evidencias de los reconocimientos
        /// </summary>
        public const string URI_VUC_FILE = "output-vuc-recog-client";
    }
}
