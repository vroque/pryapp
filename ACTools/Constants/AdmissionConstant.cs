using System.Collections.Generic;

namespace ACTools.Constants
{
    public static class AdmissionConstant
    {
        /// <summary>
        /// Postulante Ingresante a la UC
        /// </summary>
        public const string INGRESANTE = "1";

        /// <summary>
        /// Postulante NO Ingresante a la UC
        /// </summary>
        public const string NO_INGRESANTE = "0";

        /// <summary>
        /// Estado de Postulante UC
        /// </summary>
        public static readonly Dictionary<string, string> APPLICANT_STATUS = new Dictionary<string, string>
        {
            {INGRESANTE, "INGRESANTE"},
            {NO_INGRESANTE, "NO INGRESANTE"}
        };

        /// <summary>
        /// Modalidad de postulación "Sin Descripción"
        /// </summary>
        public const decimal NO_ID_MODALIDAD_POSTU = -1;
        public const string NO_ID_MODALIDAD_POSTU_BANNER = "42";

        /// <summary>
        /// Postulante Renuncia a la Carrera Ingresada
        /// </summary>
        public const string RENUNCIA_CARRERA = "1";

        /// <summary>
        /// Postulante NO Renuncia a la Carrera Ingresada
        /// </summary>
        public const string NO_RENUNCIA_CARRERA = "0";

        // Códigos PRONABEC según BDUCCI.dbo.TblModalidadPostu
        /*
        public const decimal PronabecOrdinaria = 36;
        public const decimal PronabecRepared = 37;
        public const decimal PronabecVraem = 38;
        public const decimal PronabecInternacional = 39;
        public const decimal PronabecAlbergue = 41;
        public const decimal PronabecHuallaga = 42;
        public const decimal PronabecServicioM = 43;
        public const decimal PronabecBilingue = 46; 
        public const decimal PronabecFfAa =  47;
        */

        // Códigos PRONABEC en BDINTBANNER según la conversión BDUCCI.dbo.tblModalidadPostuRecruiter

        public const string PRONABEC_ORDINARIA = "15";
        public const string PRONABEC_REPARED = "16";
        public const string PRONABEC_VRAEM = "17";
        public const string PRONABEC_INTERNACIONAL = "18";
        public const string PRONABEC_ALBERGUE = "19";
        public const string PRONABEC_HUALLAGA = "20";
        public const string PRONABEC_FFAA = "21";

        public static readonly Dictionary<string, string> PRONABEC = new Dictionary<string, string>
        {
            {PRONABEC_ORDINARIA, "Ordinaria"},
            {PRONABEC_REPARED, "REPARED"},
            {PRONABEC_VRAEM, "VRAEM"},
            {PRONABEC_INTERNACIONAL, "Internacional"},
            {PRONABEC_ALBERGUE, "Albergue"},
            {PRONABEC_HUALLAGA, "HUALLAGA"},
            {PRONABEC_FFAA, "FF.AA"}
        };

        public static readonly List<string> PRONABEC_LIST = new List<string>
        {
            PRONABEC_ORDINARIA,
            PRONABEC_REPARED,
            PRONABEC_VRAEM,
            PRONABEC_INTERNACIONAL,
            PRONABEC_ALBERGUE,
            PRONABEC_HUALLAGA,
            PRONABEC_FFAA,
        };

        /// <summary>
        /// Modalidad de Admisión UC
        /// </summary>
        public static readonly Dictionary<string, string> MODALITY_OF_ADMISSION = new Dictionary<string, string>
        {
            // SELECT DISTINCT(IDEscuelaADM) FROM tblPostulante WHERE IDDependencia = 'UCCI' AND Ingresante = 1
            {"ADE", ""},
            {"ADG", "Gente Que Trabaja"},
            {"ADM", "Regular"},
            {"ADP", "Posgrado"},
            {"ADV", "Virtual"},
            {"MGP", "Maestría Gestión Pública"},
            {"UPGT", "Gente Que Trabaja"},
            {"UREG", "Regular"},
            {"UVIR", "Virtual"},
        };

        /// <summary>
        /// Modalidad de Estudios
        /// </summary>
        public static readonly Dictionary<string, string> STUDY_MODE = new Dictionary<string, string>
        {
            {"ADM", "Presencial"},
            {"ADG", "Semipresencial"},
            {"ADV", "Semipresencial"},
            {"UPGT", "Semipresencial"},
            {"UREG", "Presencial"},
            {"UVIR", "Semipresencial"},
        };
    }
}