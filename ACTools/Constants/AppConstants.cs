﻿namespace ACTools.Constants
{
    public static class AppConstants
    {
        public const string TYPE_STUDENT = "student"; // si es un estudiante
        public const string TYPE_EXECUTIVE = "executive"; // si es un administrativo
        public const string TYPE_PARENT = "parent"; // si es un padre, apoderado u otro
        public const int CAUAPP_ID = 5;
        public const string ANONYMOUS_MODULE = "ANONYMOUS";
        public const string DEFAULT_CAMPUS = "S01";
        public const string BACKOFFICE_PREFIX = "bo_";

        /************
         * SETTINGS *
         ************/
        
        /// <summary>
        /// Bloqueo por términos y condiciones
        /// </summary>
        public const int SETTINGS_TERMS_CONDITIONS = 1;

        /// <summary>
        /// Bloqueo por encuestas
        /// </summary>
        public const int SETTINGS_SURVEY = 2;
    }
}