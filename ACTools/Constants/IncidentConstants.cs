using System.Collections.Generic;

namespace ACTools.Constants
{
    public class IncidentConstants
    {
        public const int LogCreate = 1;
        public const int LogAssignDerive = 2;
        public const int LogCategorize = 3;
        public const int LogTask = 4;
        public const int LogComment = 5;
        public const int LogSolution = 6;
        public const int LogClose = 7;

        public static readonly Dictionary<int, string> LogType = new Dictionary<int, string>
        {
            {LogCreate, "creado"},
            {LogAssignDerive, "asignado/derivado"},
            {LogCategorize, "categorizado"},
            {LogTask, "tarea"},
            {LogComment, "comentario"},
            {LogSolution, "solución"},
            {LogClose, "cerrado"}
        };

        public const int StatusOpen = 1;
        public const int StatusInProgress = 2;
        public const int StatusSolved = 3;
        public const int StatusRejected = 4;
        public const int StatusCancelled = 5;
        public const int StatusClosed = 6;
    }
}
