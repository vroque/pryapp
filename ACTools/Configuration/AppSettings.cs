﻿using System.Collections.Generic;
using System.Web.Configuration;

namespace ACTools.Configuration
{
    public class AppSettings
    {
        public static string app_env = WebConfigurationManager.AppSettings["app-env"];

        public static string app_title = WebConfigurationManager.AppSettings["app-title"];

        public static bool MaintenanceMode = WebConfigurationManager.AppSettings["MaintenanceMode"] == "true";

        public static string path_root = WebConfigurationManager.AppSettings["app-root"];

        public static string version = WebConfigurationManager.AppSettings["app-static-version"];

        public static string ip_local = WebConfigurationManager.AppSettings["app-ip-local"];

        public static string time_out = WebConfigurationManager.AppSettings["app-time-out"];

        public static string dependencia_current = WebConfigurationManager.AppSettings["app-dependencia"];
        public static string sede_current = WebConfigurationManager.AppSettings["app-sede"];
        public static string periodo_current = WebConfigurationManager.AppSettings["app-periodo"];
        public static string payment_gateway = WebConfigurationManager.AppSettings["payment-gateway"];

        public static string id_analytics = WebConfigurationManager.AppSettings["id-analytics"];

        public static readonly Dictionary<string, string> mail = new Dictionary<string, string>()
        {
            {"endpoint", WebConfigurationManager.AppSettings["mail-endpoint"]},
            {"email", WebConfigurationManager.AppSettings["mail-email"]},
            {"password", WebConfigurationManager.AppSettings["mail-password"]},
            {"port", WebConfigurationManager.AppSettings["mail-port"]},
            {"replyTo", WebConfigurationManager.AppSettings["mail-reply-to"]},
            {"dev", WebConfigurationManager.AppSettings["dev_email"]}
        };

        public static readonly Dictionary<string, string> reports = new Dictionary<string, string>()
        {
            {"output", WebConfigurationManager.AppSettings["doc-output"]},
            {"output-cnv-req", $"{WebConfigurationManager.AppSettings["doc-output"]}{@"\REQUISITOS\"}"},
            {"output-cnv-res", $"{WebConfigurationManager.AppSettings["doc-output"]}{@"\RESOLUCIONES\"}"},
            {"output-cic", $"{WebConfigurationManager.AppSettings["doc-output"]}{@"CENTRO-IDIOMAS\"}"},
            {"output-cic-caie", $"{WebConfigurationManager.AppSettings["doc-output"]}{@"PROYECCION\"}"},
            {"output-prs", $"{WebConfigurationManager.AppSettings["doc-output"]}{@"\PROYECCION\"}"},
            {"output-opp", $"{WebConfigurationManager.AppSettings["doc-output"]}{@"\PROYECCION\"}"},
            {"output-vi", $"{WebConfigurationManager.AppSettings["doc-output"]}{@"\CONVENIOS\"}"},
            {"output-student", $"{WebConfigurationManager.AppSettings["doc-output"]}{@"\RESOLUCIONES\"}"},
            {"output-conint", $"{WebConfigurationManager.AppSettings["doc-output-vi"]}{@"\FORMATO\"}"},
            {"output-vuc", $"{WebConfigurationManager.AppSettings["doc-output-vuc"]}{@"\CONSTANCIAS\"}"},
            {"output-vuc-client", $"{path_root}backoffice/media/actividades-extracurriculares/pdf/"},
            {"output-client", $"{path_root}backoffice/get-documents/"},
            {"output-cic-client", $"{path_root}backoffice/centro-idiomas/pdf/"},
            {"output-cic-caie-client", $"{path_root}backoffice/centro-idiomas/pdf/"},
            {"output-excel", "~/Content/media/output/"},
            {"output-ldr", string.Format("{0}{1}", WebConfigurationManager.AppSettings["doc-output"], @"\LDR\")},
            {"output-NoteClaim", string.Format("{0}{1}", WebConfigurationManager.AppSettings["doc-output"], @"\RECLAMONOTAS\")},
        };

        public static readonly Dictionary<string, string> images = new Dictionary<string, string>()
        {
            {"output-vuc-activity", $"{WebConfigurationManager.AppSettings["doc-output-vuc"]}{@"\ACTIVIDADES\"}"},
            {"output-vuc-axi", $"{WebConfigurationManager.AppSettings["doc-output-vuc"]}{@"\EJES\"}"},
            {"output-vuc-activity-client", $"{path_root}backoffice/media/actividades-extracurriculares/images/activities/"},
            {"output-front-vuc-activity-client", $"{path_root}frontdesk/media/actividades-extracurriculares/images/activities/"},
            {"output-vuc-axi-client", $"{path_root}backoffice/media/actividades-extracurriculares/images/axis/"},
            {"output-front-vuc-axi-client", $"{path_root}frontdesk/media/actividades-extracurriculares/images/axis/"},
        };

        public static readonly Dictionary<string, string> files = new Dictionary<string, string>()
        {
            {"output-vuc-recog", $"{WebConfigurationManager.AppSettings["doc-output-vuc"]}{@"\RECONOCIMIENTOS\"}"},
            {"output-vuc-recog-client", $"{path_root}backoffice/media/actividades-extracurriculares/recognitions/"},
        };

        /// <summary>
        /// Create New Email API Token
        /// </summary>
        public static readonly string app_token =
            WebConfigurationManager.AppSettings["app-token"];

        /// <summary>
        /// Create New Email API
        /// </summary>
        public static readonly string create_email_api =
            $"{WebConfigurationManager.AppSettings["queque-api"]}/mail/create";
    }
}