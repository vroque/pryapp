﻿using ACTools.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace ACTools.Mail
{

    public class Mail
    {
        #region properties
        string email { get; set; }
        string password { get; set; }
        int port { get; set; }
        string endpoint { get; set; }
        #endregion properties

        #region methods
        public Mail()
        {
            this.email = AppSettings.mail["email"]; // System.Configuration.ConfigurationManager.AppSettings.Get("mail -email").ToString();
            this.password = AppSettings.mail["password"]; //  System.Configuration.ConfigurationManager.AppSettings.Get("mail-password").ToString();
            this.endpoint = AppSettings.mail["endpoint"]; // System.Configuration.ConfigurationManager.AppSettings.Get("mail-endpoint").ToString();
            this.port = int.Parse(AppSettings.mail["port"]); //System.Configuration.ConfigurationManager.AppSettings.Get("mail-port").ToString()
        }
        public bool send(List<string> to, string subject, string body, List<string> files, List<string> replyTo, List<string> cc, List<string> cco)
        {
            string env = AppSettings.app_env;
            if (env.Equals("dev") || env.Equals("test"))
            {
                string emails = "";
                to.ForEach(f => emails = $" {emails} - to: {f}");
                cc.ForEach( f => emails = $" {emails} - cc: {f}");
                cco.ForEach(f => emails = $"{emails} - cco:  {f}");
                to = new List<string>();
                cc = new List<string>();
                cco = new List<string>();
                subject = $"[{emails}]: {subject}";
                to.Add(AppSettings.mail["dev"]);
            }

            MailMessage message = new MailMessage();
            message.From = new MailAddress(this.email);
            if (to == null || to.Count == 0)
                throw new Exception("requiere un destinatario");
            foreach(String item in to)
            {
                message.To.Add(item);
            }

            foreach (var item in replyTo)
            {
                message.ReplyToList.Add(item);
            }

            foreach (String item in cc)
            {
                message.CC.Add(item);
            }
            foreach (String item in cco)
            {
                message.Bcc.Add(item);
            }
           
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            foreach(string file in files)
                message.Attachments.Add(new Attachment(file));

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.UseDefaultCredentials = true;

            smtpClient.Host = this.endpoint; ;
            smtpClient.Port = this.port;
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = 
                new System.Net.NetworkCredential(this.email, this.password);
            smtpClient.Send(message);
            return true;
        }
        public static bool ValidateEmail(string strMailAddress)
        {
            return Regex.IsMatch(strMailAddress, 
                @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))"      
                + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }
        #endregion methods

    }
}
