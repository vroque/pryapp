﻿using System;
using System.Collections.Generic;
using ACTools.Configuration;

namespace ACTools.Mail
{
    public class MailSender : Mail
    {
        new List<string> to { get; set; }
        string subject { get; set; }
        string body { get; set; }
        List<string> replyTo { get; set; }
        List<string> cc { get; set; }
        List<string> cco { get; set; }
        List<string> files { get; set; }

        public MailSender()
        {
            this.to = new List<string>();
            this.files = new List<string>();
            replyTo = new List<string>();
            this.cc = new List<string>();
            this.cco = new List<string>();
        }

        public MailSender compose(string type, Object parameters)
        {
            this.body = MailTemplates.render(type, parameters);
            return this;
        }

        public MailSender destination(string to, string subject)
        {
            this.to.Add(to);
            this.subject = subject;
            replyTo.Add(AppSettings.mail["replyTo"]);
            return this;
        }
        public MailSender destination(List<string> tos, string subject)
        {
            this.to = tos;
            this.subject = subject;
            return this;
        }
        /// <summary>
        /// Enviar a copia oculta
        /// </summary>
        /// <param name="cco"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        public MailSender destinationCCO(List<string> cco, string subject)
        {
            this.cco = cco;
            this.subject = subject;
            return this;
        }
        public MailSender addDestinate(string to)
        {
            this.to.Add(to);
            return this;
        }
        public MailSender addCC(string to)
        {
            this.cc.Add(to);
            return this;
        }

        /// <summary>
        /// Agregar a copia oculta
        /// </summary>
        /// <param name="to">Emails</param>
        /// <returns></returns>
        public MailSender addCCO(string to)
        {
            this.cco.Add(to);
            return this;
        }
        /// <summary>
        /// Agregar lista con copia oculta
        /// </summary>
        /// <param name="to">Lista</param>
        /// <returns></returns>
        public MailSender addCCO(List<string> to)
        {
            this.cco.AddRange(to);
            return this;
        }
        public MailSender addFile(string path)
        {
            this.files.Add(path);
            return this;
        }
        public MailSender send()
        {
            bool a = false;
            a = base.send(this.to, this.subject, this.body, this.files, replyTo, this.cc, this.cco);
            if (!a)
                throw new Exception("No se puedo enviar el correo");
            return this;
        }
    }
}
