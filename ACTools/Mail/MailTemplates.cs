﻿using Mustache;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACTools.Mail
{
    public class MailTemplates
    {
        
        public static string render(string tpl, Object parameters)
        {
            Dictionary<String, String> tpls = new Dictionary<string, string>();
            tpls.Add("info_convalidacion", @"\App_Data\Templates\Mail\COU\infoConvalidacion.html");
            tpls.Add("reserva_ambiente", @"\App_Data\Templates\Mail\BUW\infoConvalidacion.html");
            tpls.Add("request_set", @"\App_Data\Templates\Mail\CAU\acuseSolicitud.html");
            tpls.Add("request_send", @"\App_Data\Templates\Mail\CAU\sendDocument.html");
            tpls.Add("request_refuse", @"\App_Data\Templates\Mail\CAU\refuseDocument.html");
            tpls.Add("end_transfer", @"\App_Data\Templates\Mail\CAC\endTransfer.html");
            tpls.Add("register_activity", @"\App_Data\Templates\Mail\CAU\registerActivity.html");

            // Vida continental mails
            tpls.Add("accepted_program", @"\App_Data\Templates\Mail\VUC\acceptedProgram.html");
            tpls.Add("generate_debt", @"\App_Data\Templates\Mail\VUC\generateDebt.html");
            tpls.Add("accredited", @"\App_Data\Templates\Mail\VUC\accredited.html");
            tpls.Add("not_accepted_program", @"\App_Data\Templates\Mail\VUC\notAcceptedProgram.html");
            tpls.Add("enroll_activity", @"\App_Data\Templates\Mail\VUC\registerActivity.html");
            // FIn Vida continental mails

            // PRY
            tpls.Add("pry_generic_template", @"\App_Data\Templates\Mail\PRY\genericTemplate.html");

            tpls.Add("ldr_create_claim", @"\App_Data\Templates\Mail\LDR\registerClaim.html");
            tpls.Add("ldr_derivate_claim", @"\App_Data\Templates\Mail\LDR\derivedClaim.html");
            tpls.Add("ldr_disable_derivation", @"\App_Data\Templates\Mail\LDR\DisableDerivation.html");
            tpls.Add("ldr_finalized_claim", @"\App_Data\Templates\Mail\LDR\finalizedClaim.html");
            tpls.Add("ldr_dismissed_claim", @"\App_Data\Templates\Mail\LDR\DismissedClaim.html");

            tpls.Add("oea_save_substitute_exam", @"\App_Data\Templates\Mail\OEA\sustituteExam.html");
            // Reclamo de notas
            tpls.Add("oea_note_claim_derivate_teacher", @"\App_Data\Templates\Mail\OEA\noteClaimDerivatedTeacher.html");
            tpls.Add("oea_note_claim_validate_report", @"\App_Data\Templates\Mail\OEA\noteClaimValidateReport.html");
            tpls.Add("oea_note_claim_answer_student", @"\App_Data\Templates\Mail\OEA\noteClaimAnswerToStudent.html");
            tpls.Add("oea_note_claim_email_teacher", @"\App_Data\Templates\Mail\OEA\noteClaimEmailTeacher.html");

            var path = Path.GetDirectoryName(
                System.IO.Path.GetDirectoryName(
                  System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase));
            
            if( !tpls.ContainsKey(tpl) )
            {
                throw new Exception("No existe la plantilla");
            }
            path = path + tpls[tpl];
            path = path.Substring(6, path.Length - 6); // remove file:\
            string text = File.ReadAllText(path);

            //Mail mail_obj = new Mail();
            FormatCompiler compiler = new FormatCompiler();
            Generator generator = compiler.Compile(text);
            string body_str = generator.Render(parameters);
            return body_str;

            
        }
    }
}
