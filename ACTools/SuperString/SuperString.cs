﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace ACTools.SuperString
{
    public static class SuperString
    {
        /// <summary>
        /// Convierte una frase a minusculas con la primera letra en mayusculas
        /// </summary>
        /// <param name="value">frase</param>
        /// <returns>frase capitalizada</returns>
        public static string toEachCapitalize(this string value)
        {
            if (value == null)
            {
                return null;
            }
            else
            {
                value = value.ToLower();
                char[] array = value.ToCharArray();
                // Handle the first letter in the string.
                if (array.Length >= 1)
                {
                    if (char.IsLower(array[0]))
                    {
                        array[0] = char.ToUpper(array[0]);
                    }
                }
                // Scan through the letters, checking for spaces.
                // ... Uppercase the lowercase letters following spaces.
                for (int i = 1; i < array.Length; i++)
                {
                    if (array[i - 1] == ' ')
                    {
                        if (char.IsLower(array[i]))
                        {
                            array[i] = char.ToUpper(array[i]);
                        }
                    }
                }
                return new string(array);
            }
            
        }

        /// <summary>
        /// Convertir la palabras a mayusculas y quitar las tildes
        /// </summary>
        /// <param name="value"> Palabra</param>
        /// <returns> Palabra transformada</returns>
        public static string toEachUpperClean(this string value)
        {
            value = value.ToUpper();
            char[] array = value.ToCharArray();

            // quitamos la opcion de limpiar tildes
            // buscar carateres con tilde y quitarles la tilde
            //for (int i = 1; i < array.Length; i++)
            //{
            //    switch (array[i])
            //    {
            //        case ('Á'):
            //            array[i] = 'A';
            //            break;
            //        case ('É'):
            //            array[i] = 'E';
            //            break;
            //        case ('Í'):
            //            array[i] = 'I';
            //            break;
            //        case ('Ó'):
            //            array[i] = 'O';
            //            break;
            //        case ('Ú'):
            //            array[i] = 'U';
            //            break;
            //        default:
            //            break;
            //    }
            //}
            return new string(array);
        }
       
        /// <summary>
        /// Convierte una palabra a minusculas con la primera letra en mayusculas
        /// </summary>
        /// <param name="value">palbra</param>
        /// <returns>palabra capitalizada</returns>
        public static string toCapitalize(this string value)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            value = value.ToLower();
            // Return char and concat substring.
            return char.ToUpper(value[0]) + value.Substring(1);
        }
        
        /// <summary>
        /// metodo para completar zeros a la izquiera
        /// hasta completar un lognitud
        /// </summary>
        /// <param name="value">Cadena a completar</param>
        /// <param name="cant">Cantidad maxima a ser completada</param>
        /// <returns></returns>
        public static string completeZeros(this string value, int cant)
        {
            if (value.Length >= cant) return value;
            int length = cant - value.Length;
            string res = "";
            for (int i = 0; i < length; i++)
            {
                res += "0";
            }
            res += value;
            return res;
        }

        /// <summary>
        /// Convierte los 20 primeros numeros a su contraparte en letras
        /// en español
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Numero en letras</returns>
        public static string numberToLetters(this int number) {            
            return NUMBER_TO_LETTERS[number];
        }

        /// <summary>
        /// Convierte los 20 primeros numeros a su contraparte ordina
        /// en letras
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Orden del numero en letras</returns>
        public static string numberToOrdinal(this int number)
        {
            return NUMBER_TO_ORDINALS[number];
        }

        /// <summary>
        /// Convierte los 20 primeos numeros a su contraparte
        /// en numeros romanos
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Numero en Romanos</returns>
        public static string numberToRoman(this int number)
        {
            if (number < 1)
                number = 1;
            else if (number > 20)
                number = 20;
            return NUMBER_TO_ROMAN[number];
        }

        public static Dictionary<int, string> NUMBER_TO_LETTERS =
            new Dictionary<int, string>()
            {
                {0, "CERO"},
                {1, "UNO"},
                {2, "DOS"},
                {3, "TRES"},
                {4, "CUATRO"},
                {5, "CINCO"},
                {6, "SEIS"},
                {7, "SIETE"},
                {8, "OCHO"},
                {9, "NUEVO"},
                {10, "DIEZ"},
                {11, "ONCE"},
                {12, "DOCE"},
                {13, "TRECE"},
                {14, "CATORCE"},
                {15, "QUINCE"},
                {16, "DIECISÉIS"},
                {17, "DIECISIETE"},
                {18, "DIECIOCHO"},
                {19, "DIECINUEVE"},
                {20, "VEINTE"},
        };
        public static Dictionary<int, string> NUMBER_TO_ORDINALS =
            new Dictionary<int, string>()
            {
                //{0, "CERO"},
                {1, "PRIMER"},
                {2, "SEGUNDO"},
                {3, "TERCERO"},
                {4, "CUARTO"},
                {5, "QUINTO"},
                {6, "SEXTO"},
                {7, "SEPTIMO"},
                {8, "OCTAVO"},
                {9, "NOVENO"},
                {10, "DÉCIMO"},
                {11, "DÉCIMO PRIMER"},
                {12, "DÉCIMO SEGUNDO"},
                {13, "DÉCIMO TERCER"},
                {14, "DÉCIMO CUARTO"},
                {15, "DÉCIMO QUINTO"},
                {16, "DÉCIMO SEXTO"},
                {17, "DÉCIMO SÉPTIMO"},
                {18, "DÉCIMO OCTAVO"},
                {19, "DÉCIMO NOVENO"},
                {20, "VIGÉSIMO "},
        };
        public static Dictionary<int, string> NUMBER_TO_ROMAN =
            new Dictionary<int, string>()
            {
                //{0, "CERO"},
                {1, "I"},
                {2, "II"},
                {3, "III"},
                {4, "IV"},
                {5, "V"},
                {6, "VI"},
                {7, "VII"},
                {8, "VIII"},
                {9, "IX"},
                {10, "X"},
                {11, "XI"},
                {12, "XII"},
                {13, "XIII"},
                {14, "XIV"},
                {15, "XV"},
                {16, "XVI"},
                {17, "XVII"},
                {18, "XVIII"},
                {19, "XIX"},
                {20, "XX"},
        };

        public static string getGenderPrefix(string gender)
        {
            if (gender == "0")
            {
                return "la Srta.";
            }
            else
            {
                return "el Sr.";
            }

        }
        public static string getGenderRequestPrefix(string gender)
        {
            if (gender == "0")
            {
                return "de la interesada";
            }
            else
            {
                return "del interesado";
            }

        }

        public static string GenerateSlug(this string phrase)
        {
            string str = phrase.RemoveAccent().ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        public static string RemoveAccent(this string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        /// <summary>
        /// Agrega a una cadena de texto la extension del correo electroico uc
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToEmailUc(this string value)
        {
            return $"{value.ToLower()}@continental.edu.pe";
        }
    }
}
