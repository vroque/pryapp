﻿namespace ACTools.Util
{
    /// <summary>
    /// Repuesta del servidor cuando se realiza una operación.
    /// </summary>
    public class MessageServer
    {
        /// <summary>
        /// Status de la operación (warning, danger, success).
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Título del mensaje.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Mensaje de la operación.
        /// </summary>
        public string Message { get; set; }
    }
}
