﻿namespace ACTools.Util
{
    /// <summary>
    /// Utilidades para operaciones con números
    /// </summary>
    public class Number
    {
        /// <summary>
        /// Cardinal to Ordinal
        /// </summary>
        /// <param name="number">number</param>
        /// <returns></returns>
        public static string AddOrdinal(int number)
        {
            if (number < 0) return number.ToString();

            if (number % 100 == 11 || number % 100 == 12 || number % 100 == 13) return number + "st";

            switch (number % 10)
            {
                case 1:
                    return number + "st";
                case 2:
                    return number + "nd";
                case 3:
                    return number + "rd";
                case 4:
                    return number + "th";
                default:
                    return number.ToString();
            }
        }
    }
}