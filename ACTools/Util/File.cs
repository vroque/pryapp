﻿using System;
using System.IO;
using System.Linq;
using ACTools.PDF;
using iTextSharp.text;

namespace ACTools.Util
{
    /// <summary>
    /// Utilidades relacionadas a archivos
    /// </summary>
    public static class File
    {
        public const string PdfExtension = ".pdf";
        public const string JpegExtension = ".jpeg";
        public const string ZipExtension = ".zip";

        /// <summary>
        /// Verifica si un archivo ha sido generado o no.
        /// </summary>
        /// <param name="path">Ruta de acceso absoluta o relativa al directorio que se va a buscar</param>
        /// <param name="fileName">Nombre de archivo</param>
        /// <param name="extension">Extensión del archivo</param>
        /// <returns></returns>
        public static bool Exist(string path, string fileName, string extension)
        {
            string[] files;

            try
            {
                files = GetFiles(path, fileName, extension);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ApplicationException("No se pudo acceder al directorio.");
            }

            return files.Any();
        }

        /// <summary>
        /// Devuelve los nombres de los archivos (con sus rutas de acceso) que coinciden con el patrón de búsqueda
        /// especificado en el directorio especificado.
        /// </summary>
        /// <param name="path">Ruta de acceso absoluta o relativa al directorio que se va a buscar</param>
        /// <param name="fileName">Nombre de archivo</param>
        /// <param name="extension">Extensión del archivo</param>
        /// <returns>Una matriz de nombres completos (con sus rutas de acceso) para los archivos del directorio
        /// especificado que coinciden con el patrón de búsqueda especificado, o una matriz vacía si no se encuentra
        /// ningún archivo.</returns>
        /// <exception cref="ApplicationException"></exception>
        public static string[] GetFiles(string path, string fileName, string extension)
        {
            var file = $"{fileName}{extension}";
            string[] files;

            try
            {
                files = Directory.GetFiles(path, file);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ApplicationException("No se pudo acceder al directorio.");
            }

            return files;
        }

        /// <summary>
        /// PDF Utils
        /// </summary>
        public static class Pdf
        {
            public static class PdfPCell
            {
                /// <summary>
                /// Formato para la creación de celdas de una tabla.
                /// </summary>
                /// <param name="borderTop"></param>
                /// <param name="borderRight"></param>
                /// <param name="borderBottom"></param>
                /// <param name="borderLeft"></param>
                /// <param name="alignCenter"></param>
                /// <param name="paddingTop"></param>
                /// <param name="paddingRight"></param>
                /// <param name="paddingBottom"></param>
                /// <param name="paddingLeft"></param>
                /// <returns></returns>
                public static iTextSharp.text.pdf.PdfPCell New(float borderTop = 0, float borderRight = 0,
                    float borderBottom = 0, float borderLeft = 0, bool alignCenter = false,
                    float paddingTop = 2, float paddingRight = 2, float paddingBottom = 2, float paddingLeft = 2)
                {
                    var pdfPCell = new iTextSharp.text.pdf.PdfPCell
                    {
                        BorderWidthTop = borderTop,
                        BorderWidthRight = borderRight,
                        BorderWidthBottom = borderBottom,
                        BorderWidthLeft = borderLeft,
                        BorderColorBottom = PDFGenerator.getColor("black"),
                        PaddingTop = paddingTop,
                        PaddingRight = paddingRight,
                        PaddingBottom = paddingBottom,
                        PaddingLeft = paddingLeft
                    };

                    if (!alignCenter) return pdfPCell;

                    pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                    return pdfPCell;
                }
            }
        }
    }
}