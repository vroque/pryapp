﻿using ACTools.Configuration;
using System;
using System.Linq;
using _vuc = ACTools.Constants.ExtracurricularActivitiesConstants;

namespace ACTools.Util
{
    /// <summary>
    /// Utilidades para Vida Universitaria
    /// </summary>
    public static class ExtracurricularActivities
    {
        /// <summary>
        /// Verifica si ya se ha creado o no el documento.
        /// archivos generales
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public static bool ExistDocument(string fileName)
        {
            var repoPath = AppSettings.reports[_vuc.SRC_VUC_PDF];
            return File.Exist(repoPath, fileName, File.PdfExtension);
        }

        /// <summary>
        /// Verifica si ya se ha creado o no la imagen de ejes.
        /// archivos generales
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public static bool ExistAxiImage(string fileName)
        {
            var repoPath = AppSettings.images[_vuc.SRC_VUC_IMAGE_AXI];
            return File.Exist(repoPath, fileName, File.JpegExtension);
        }

        /// <summary>
        /// Verifica si ya se ha creado o no la imagen de actividades.
        /// archivos generales
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public static bool ExistActivityImage(string fileName)
        {
            var repoPath = AppSettings.images[_vuc.SRC_VUC_IMAGE_ACTIVITY];
            return File.Exist(repoPath, fileName, File.JpegExtension);
        }

        /// <summary>
        /// Verifica si ya se ha creado o no la imagen de actividades.
        /// archivos generales
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public static bool ExistFiles(string fileName)
        {
            var repoPath = AppSettings.files[_vuc.SRC_VUC_FILE];
            return File.Exist(repoPath, fileName, File.ZipExtension);
        }

        /// <summary>
        /// Obtiene la ruta del archivo de una solicitud de actividades extracurriculares desde el repositorio.
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">Excepción para nombres de archivo menor a 10</exception>
        public static string GetDocumentPath(string fileName)
        {
            var repoPath = AppSettings.reports[_vuc.SRC_VUC_PDF];

            if (fileName.Length <= 10)
            {
                throw new ApplicationException(
                    "No se puede realizar una búsqueda de documentos con menos de 10 caracteres.");
            }

            return File.GetFiles(repoPath, fileName, File.PdfExtension).FirstOrDefault();
        }

        /// <summary>
        /// Obtiene la ruta de las imágenes de ejes de actividades extracurriculares
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">Excepción para nombres de archivo menor a 10</exception>
        public static string GetAxiImagePath(string fileName)
        {
            var repoPath = AppSettings.images[_vuc.SRC_VUC_IMAGE_AXI];

            if (fileName.Length <= 32)
            {
                throw new ApplicationException(
                    "No se puede realizar una búsqueda de imágenes con menos de 10 caracteres.");
            }

            return File.GetFiles(repoPath, fileName, File.JpegExtension).FirstOrDefault();
        }

        /// <summary>
        /// Obtiene la ruta de las imágenes de Vida Universitaria desde el repositorio.
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">Excepción para nombres de archivo menor a 10</exception>
        public static string GetActivityImagePath(string fileName)
        {
            var repoPath = AppSettings.images[_vuc.SRC_VUC_IMAGE_ACTIVITY];

            if (fileName.Length <= 32)
            {
                throw new ApplicationException(
                    "No se puede realizar una búsqueda de imágenes con menos de 10 caracteres.");
            }

            return File.GetFiles(repoPath, fileName, File.JpegExtension).FirstOrDefault();
        }

        /// <summary>
        /// Obtiene la ruta de los archivos de reconocimientos de actividades extracurriculares.
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">Excepción para nombres de archivo menor a 10</exception>
        public static string GetFilesPath(string fileName)
        {
            var repoPath = AppSettings.files[_vuc.SRC_VUC_FILE];

            if (fileName.Length <= 32)
            {
                throw new ApplicationException(
                    "No se puede realizar una búsqueda de archivos con menos de 32 caracteres.");
            }

            return File.GetFiles(repoPath, fileName, File.ZipExtension).FirstOrDefault();
        }
    }
}
