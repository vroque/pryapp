﻿using System;
using System.Linq;
using ACTools.Configuration;
using _cic = ACTools.Constants.LanguagesCenterConstants;

namespace ACTools.Util
{
    /// <summary>
    /// Utilidades para el Centro de Idiomas
    /// </summary>
    public static class LanguagesCenter
    {
        /// <summary>
        /// Verifica si el archivo de la Constancia de Idioma Extranjero ya ha sido creado o no.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool ExistCaieDocument(string fileName)
        {
            var repoPath = AppSettings.reports[_cic.SRC_CAIE_PDF];
            return File.Exist(repoPath, fileName, File.PdfExtension);
        }

        /// <summary>
        /// Verifica si el archivo (que no sea Constancia de Idioma Extranjero)
        /// de una solicitud del Centro de Idiomas ya ha sido creado o no.
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public static bool ExistDocument(string fileName)
        {
            var repoPath = AppSettings.reports[_cic.SRC_CIC_PDF];
            return File.Exist(repoPath, fileName, File.PdfExtension);
        }

        /// <summary>
        /// Verifica la existencia de cualquier archivo
        /// (documentos generales o constancia de idioma extranjero).
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public static bool ExistAnyDocument(string fileName)
        {
            var repoPath = AppSettings.reports[_cic.SRC_CIC_PDF];
            var repoCaiePath = AppSettings.reports[_cic.SRC_CAIE_PDF];
            
            return File.Exist(File.Exist(repoCaiePath, fileName, File.PdfExtension)
                ? repoCaiePath
                : repoPath, fileName, File.PdfExtension);
        }

        /// <summary>
        /// Obtiene la ruta del archivo de una solicitud del Centro de Idiomas desde el repositorio.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">Excepción para nombres de archivo menor a 10</exception>
        public static string GetDocumentPath(string fileName)
        {
            var repoPath = AppSettings.reports[_cic.SRC_CIC_PDF];
            var repoCaiePath = AppSettings.reports[_cic.SRC_CAIE_PDF];

            if (fileName.Length <= 10)
            {
                throw new ApplicationException(
                    "No se puede realizar una búsqueda de documentos con menos de 10 caracteres.");
            }

            return File.Exist(repoCaiePath, fileName, File.PdfExtension)
                ? File.GetFiles(repoCaiePath, fileName, File.PdfExtension).FirstOrDefault()
                : File.GetFiles(repoPath, fileName, File.PdfExtension).FirstOrDefault();
        }
    }
}