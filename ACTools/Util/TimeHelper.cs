﻿namespace ACTools.Util
{
    public static class TimeHelper
    {
        /// <summary>
        /// Convierte una cadena a horas.
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static int ConvertTimeToMinutes(string time)
        {
            var hours = int.Parse(time.Substring(0, 2));
            var minutes = int.Parse(time.Substring(3, 2));
            return hours * 60 + minutes;
        }
    }
}
