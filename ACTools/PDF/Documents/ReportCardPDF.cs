﻿using ACTools.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections.Generic;
using ACTools.SuperString;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Platilla PDF para Boleta de Notas
    /// </summary>
    public class ReportCardPDF
    {
        #region properties
        public ReportCardPDF.Data data { get; set; }
        #endregion properties

        #region constructor
        public ReportCardPDF()
        {
            data = new ReportCardPDF.Data();
        }
        #endregion constructor

        #region methods
        /// <summary>
        /// Genera la Boleta de Notas
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public string generate()
        {
            

            Paragraph white_space = new Paragraph();
            white_space.Add(new Phrase("\n"));
            PDFGenerator pdf_get = new PDFGenerator();

            PdfPTable head = PDFGenerator.Table.create(2);
            head.WidthPercentage = 100;

            PdfPTable head_l = PDFGenerator.Table.transparent(2); // no borders
            Paragraph head_l_p_1 = new Paragraph();
            head_l_p_1.Add(new Phrase("Facultad: ", pdf_get.getFontDoc("doc_body")));
            head_l_p_1.Add(new Phrase(data.college_name.ToUpper(), pdf_get.getFontDoc("doc_body")));
            head_l.AddCell(head_l_p_1);

            Paragraph head_l_p_2 = new Paragraph();
            head_l_p_2.Add(new Phrase("E.A.P: ", pdf_get.getFontDoc("doc_body")));
            head_l_p_2.Add(new Phrase(data.program_name.ToUpper(), pdf_get.getFontDoc("doc_body")));
            head_l.AddCell(head_l_p_2);

            PdfPCell head_l_cell_1 = PDFGenerator.Cell.transparent(2);

            Paragraph head_l_cell_1_p_1 = new Paragraph();
            head_l_cell_1_p_1.Add(new Phrase("Código: ", pdf_get.getFontDoc("doc_body")));
            head_l_cell_1_p_1.Add(new Phrase(data.student_id, pdf_get.getFontDoc("doc_body")));
            head_l_cell_1.AddElement(head_l_cell_1_p_1);

            Paragraph head_l_cell_1_p_2 = new Paragraph();
            head_l_cell_1_p_2.Add(new Phrase("Estudiante: ", pdf_get.getFontDoc("doc_body")));
            head_l_cell_1_p_2.Add(new Phrase(data.student_names.ToUpper(), pdf_get.getFontDoc("doc_body")));
            head_l_cell_1.AddElement(head_l_cell_1_p_2);

            head_l.AddCell(head_l_cell_1);

            PdfPTable head_r = PDFGenerator.Table.outline(2, false);
            head_r.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            head_r.DefaultCell.Padding = 5f;

            PdfPCell head_r_cell_1 = new PdfPCell(new Paragraph("Boleta de notas", pdf_get.getFontDoc("doc_body")));
            head_r_cell_1.BorderWidthTop = 0f;
            head_r_cell_1.BorderWidthLeft = 0f;
            head_r_cell_1.BorderWidthBottom = 0.5f;
            head_r_cell_1.BorderWidthRight = 0.5f;
            head_r_cell_1.HorizontalAlignment = Element.ALIGN_CENTER;
            head_r_cell_1.Padding = 5f;
            PdfPCell head_r_cell_2 = new PdfPCell(new Paragraph(data.term, pdf_get.getFontDoc("doc_body")));
            head_r_cell_2.BorderWidthBottom = 0.5f;
            head_r_cell_2.BorderWidthTop = 0f;
            head_r_cell_2.BorderWidthLeft = 0f;
            head_r_cell_2.BorderWidthRight = 0f;
            head_r_cell_2.HorizontalAlignment = Element.ALIGN_CENTER;
            head_r_cell_2.Padding = 5f;
            PdfPCell head_r_cell_3 = new PdfPCell(
                new Paragraph("C: Consolidado\nEP: Examen parcial\n EF: Examen final", pdf_get.getFontDoc("doc_body")));
            head_r_cell_3.BorderWidthRight = 0.5f;
            head_r_cell_3.BorderWidthTop = 0f;
            head_r_cell_3.BorderWidthLeft = 0f;
            head_r_cell_3.BorderWidthBottom = 0f;
            head_r_cell_3.Padding = 5f;
            head_r_cell_3.HorizontalAlignment = Element.ALIGN_CENTER;


            head_r.AddCell(head_r_cell_1);
            head_r.AddCell(head_r_cell_2);
            head_r.AddCell(head_r_cell_3);
            head_r.AddCell(new Paragraph("Nota mínima: 0 \n Nota aprobatoria: 11", pdf_get.getFontDoc("doc_body")));

            head.AddCell(head_l);
            head.AddCell(head_r);



            //document.Add(new Paragraph("\n"));

            // Iniciamos con el cuerpo

            PdfPTable body = PDFGenerator.Table.create(9);
            body.WidthPercentage = 100;
            body.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            body.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            body.DefaultCell.Padding = 5f;
            body.DefaultCell.BorderWidthBottom = 0.5f;
            body.DefaultCell.BorderWidthLeft = Rectangle.NO_BORDER;
            body.DefaultCell.BorderWidthRight = Rectangle.NO_BORDER;
            body.DefaultCell.BorderWidthTop = Rectangle.NO_BORDER;
            body.DefaultCell.BorderWidthBottom = Rectangle.NO_BORDER;

            float[] widths = new float[] { 6f, 3f, 5f, 15f, 2f, 2f, 2f, 2f, 3f };
            body.SetWidths(widths);

            PdfPCell body_cell_l = new PdfPCell(new Phrase("Cod. Asig.", pdf_get.getFontDoc("doc_body_bold")));
            body_cell_l.Border = Rectangle.NO_BORDER;
            body_cell_l.HorizontalAlignment = Element.ALIGN_CENTER;
            body_cell_l.VerticalAlignment = Element.ALIGN_MIDDLE;
            body_cell_l.BorderWidthLeft = 0.5f;
            body_cell_l.BorderWidthBottom = 0.5f;
            body_cell_l.BorderWidthTop = 0.5f;
            body.AddCell(body_cell_l);
            foreach (string option in new string[] { "Créditos", "Sección", "Asignatura", "C1", "EP", "C2", "EF" })
            {
                //PdfPCell body_cell_h_tmp = Helpers.Reports.PDF.Cell.centered();
                //body_cell_h_tmp.AddElement(new Phrase(option, pdf_get.getFontDoc("times_12_blue_sky")));
                PdfPCell body_cell_h_tmp = new PdfPCell(new Phrase(option, pdf_get.getFontDoc("doc_body_bold")));
                body_cell_h_tmp.Border = Rectangle.NO_BORDER;
                if (option == "Asignatura")
                    body_cell_h_tmp.HorizontalAlignment = Element.ALIGN_LEFT;
                else body_cell_h_tmp.HorizontalAlignment = Element.ALIGN_CENTER;
                body_cell_h_tmp.VerticalAlignment = Element.ALIGN_MIDDLE;

                //body_cell_h_tmp.Padding = 5f;
                body_cell_h_tmp.BorderWidthTop = 0.5f;
                body_cell_h_tmp.BorderWidthBottom = 0.5f;
                body.AddCell(body_cell_h_tmp);
            }

            PdfPCell body_cell_r = new PdfPCell(new Phrase("PROM.", pdf_get.getFontDoc("doc_body_bold")));
            body_cell_r.Border = Rectangle.NO_BORDER;
            body_cell_r.HorizontalAlignment = Element.ALIGN_CENTER;
            body_cell_r.VerticalAlignment = Element.ALIGN_MIDDLE;
            body_cell_r.BorderWidthRight = 0.5f;
            body_cell_r.BorderWidthBottom = 0.5f;
            body_cell_r.BorderWidthTop = 0.5f;
            body.AddCell(body_cell_r);

            ///* Aqui inician los datos */

            foreach (ReportCardPDF.Score score in data.scores)
            {
                PdfPCell body_cell_l_tmp = PDFGenerator.Cell.centered();
                body_cell_l_tmp.AddElement(new Phrase(score.course_id, pdf_get.getFontDoc("doc_body")));
                body_cell_l_tmp.BorderWidthLeft = 0.5f;
                body.AddCell(body_cell_l_tmp);

                body.AddCell(new Phrase(score.credits.ToString(), pdf_get.getFontDoc("doc_body")));
                body.AddCell(new Phrase(score.section, pdf_get.getFontDoc("doc_body")));
                //body.AddCell(new Phrase(score.course_name, pdf_get.getFontDoc("times_12")));
                PdfPCell course_cell = new PdfPCell();
                course_cell.Border = Rectangle.NO_BORDER;
                Paragraph course_paragrap = new Paragraph();
                course_paragrap.Alignment = Element.ALIGN_LEFT;
                course_paragrap.Add(new Phrase(score.course_name, pdf_get.getFontDoc("doc_body")));
                course_cell.AddElement(course_paragrap);
                body.AddCell(course_cell);

                body.AddCell(new Phrase(score.cl1.ToString(), pdf_get.getFontDoc("doc_body")));
                body.AddCell(new Phrase(score.ep.ToString(), pdf_get.getFontDoc("doc_body")));
                body.AddCell(new Phrase(score.cl2.ToString(), pdf_get.getFontDoc("doc_body")));
                body.AddCell(new Phrase(score.ef.ToString(), pdf_get.getFontDoc("doc_body")));

                PdfPCell body_cell_r_tmp = PDFGenerator.Cell.centered();
                Paragraph course_prom_paragrap = new Paragraph();
                course_prom_paragrap.Alignment = Element.ALIGN_CENTER;
                course_prom_paragrap.Add(new Phrase(score.average.ToString(), pdf_get.getFontDoc("doc_body")));
                body_cell_r_tmp.AddElement(course_prom_paragrap);
                body_cell_r_tmp.BorderWidthRight = 0.5f;
                body.AddCell(body_cell_r_tmp);
                
            }


            ///* END Aqui inician los datos */

            PdfPTable footer = PDFGenerator.Table.transparent(3);
            footer.WidthPercentage = 100;
            footer.DefaultCell.BorderWidthTop = 0.5f;
            footer.SetWidths(new float[] { 10f, 5f, 5f });

            footer.AddCell(new Phrase("* Oficina de Registros Académicos ", pdf_get.getFontDoc("doc_body")));

            PdfPCell footer_cell_r_1 = new PdfPCell();
            footer_cell_r_1.AddElement(new Paragraph("Promedio ponderado", pdf_get.getFontDoc("doc_body")));
            footer.AddCell(footer_cell_r_1);

            PdfPCell footer_cell_r_2 = new PdfPCell();
            Paragraph footer_ponderado = new Paragraph();
            footer_ponderado.Alignment = Element.ALIGN_RIGHT;
            footer_cell_r_2.BorderWidthLeft = 0f;
            footer_cell_r_2.PaddingRight = 10f;

            if (data.scores.Count > 0)
            {
                double pga = data.pga==null? data.final_average(): (double)data.pga;
                footer_ponderado.Add(new Phrase(pga.ToString("0.##"), pdf_get.getFontDoc("doc_body")));
            }
            else
                footer_ponderado.Add(new Phrase("0.0", pdf_get.getFontDoc("doc_body")));

            footer_cell_r_2.AddElement(footer_ponderado);
            footer.AddCell(footer_cell_r_2);

            // Crear documento
            Document document = PDFEvents.createSimple(data: data, type_doc: "half_A4",rotate: true);
            //document.SetPageSize(PageSize.A4.Rotate());
            document.Add(white_space);

            document.Add(head);
            document.Add(body);
            document.Add(footer);  
                  
            try
            {
                document.Close();

            }
            catch
            {
                //Debug.Print("salto error" + ex.Message);
                return null;
            }
            return string.Format("{0}{1}/pdf/", AppSettings.reports["output-client"], data.name_file);
        }

        #endregion methods

        #region extra classes
        /// <summary>
        /// Información para construir la boleta
        /// </summary>
        public class Data: PDFData
        {
            public Data()
            {
                this.scores = new List<Score>();
            }
            public string term { get; set; }
            public int index { get; set; }
            public List<ReportCardPDF.Score> scores { get; set; }
            public double? pga { get; set; }

            public double final_average()
            {
                double avg = 0;
                int credits = 0;
                foreach(Score score in scores)
                {
                    avg += score.average * score.credits;
                    credits += score.credits;
                }
                if (avg > 0 && credits > 0)
                    avg = avg / credits;
                return avg;
            } 
        }

        /// <summary>
        /// Detalles de las notas de un curso
        /// </summary>
        public class Score
        {
            public string course_id { get; set; }
            public int credits { get; set; }
            public string course_name { get; set; }
            public string section { get; set; }
            /// <summary>
            /// Consolidado 1 -> primera unidad
            /// </summary>
            public string cl1 { get; set; }
            /// <summary>
            /// Consolidado 2 -> segunda unidad
            /// </summary>
            public string cl2 { get; set; }
            public string ta1 { get; set; }
            public string ta2 { get; set; }
            /// <summary>
            /// Examen Parcial
            /// </summary>
            public string ep { get; set; }
            /// <summary>
            /// Examen final
            /// </summary>
            public string ef { get; set; }
            /// <summary>
            /// este promedio no se calcula porque tiene diferentes formas
            /// dependeindo de muchos factores 
            /// </summary>
            public double average { get; set; }
        }
        #endregion extra classes
    }
}
