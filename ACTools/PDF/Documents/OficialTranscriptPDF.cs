﻿using ACTools.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using ACTools.SuperString;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Platilla PDF para Historial Académico (A.K.A Record Académico)
    /// </summary>
    public class OficialTranscriptPDF
    {
        #region methods
        /// <summary>
        /// Genera la Constancia y devuelve la ruta
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string generate(OficialTranscriptPDF.Data data)
        {
            PDFGenerator pdf_gen = new PDFGenerator();
            Paragraph titulo = new Paragraph();
            titulo.Add(new Phrase("Historial académico", pdf_gen.getFontDoc("doc_title")));
            titulo.Add(new Phrase("\nDocumento válido solo para trámites internos\n\n", pdf_gen.getFontDoc("doc_body")));
            titulo.Alignment = Element.ALIGN_CENTER;

            PdfPTable head = PDFGenerator.Table.create(4);
            head.DefaultCell.BorderColor = PDFGenerator.getColor("black");
            head.DefaultCell.BackgroundColor = PDFGenerator.getColor("white");
            head.SetWidths(new float[] { 10f, 20f, 10f, 10 });
            head.WidthPercentage = 100;
            head.DefaultCell.VerticalAlignment = Element.ALIGN_CENTER;
            head.DefaultCell.HorizontalAlignment = Element.ALIGN_MIDDLE;
            head.DefaultCell.PaddingBottom = 5f;
            head.DefaultCell.PaddingLeft = 2f;

            head.AddCell(new Phrase("Estudiante:", pdf_gen.getFontDoc("doc_body")));
            head.AddCell(new Phrase(data.student_names.ToUpper(), pdf_gen.getFontDoc("doc_body")));

            head.AddCell(new Phrase("Código:", pdf_gen.getFontDoc("doc_body")));
            head.AddCell(new Phrase(data.student_id, pdf_gen.getFontDoc("doc_body")));

            head.AddCell(new Phrase("Especialidad:", pdf_gen.getFontDoc("doc_body")));
            head.AddCell(new Phrase(string.Format("{0} \nFacultad de {1}", data.program_name.ToUpper(), data.college_name.ToUpper()), pdf_gen.getFontDoc("doc_body")));

            head.AddCell(new Phrase("Fecha:", pdf_gen.getFontDoc("doc_body")));
            head.AddCell(new Phrase(DateTime.Now.ToShortDateString(), pdf_gen.getFontDoc("doc_body")));


            PdfPTable body = PDFGenerator.Table.transparent(8);
            body.DefaultCell.BorderColor = PDFGenerator.getColor("black");
            body.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            body.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            body.WidthPercentage = 100;

            float[] widths = new float[] { 8f, 25f, 10f, 3f, 3f, 3f, 8f, 8f };
            body.SetWidths(widths);

            PdfPCell body_cell_c = new PdfPCell(new Phrase("Código", pdf_gen.getFontDoc("tbl_footer")));
            body_cell_c.Border = Rectangle.NO_BORDER;
            body_cell_c.BorderColor = PDFGenerator.getColor("black");
            body_cell_c.HorizontalAlignment = Element.ALIGN_CENTER;
            body_cell_c.VerticalAlignment = Element.ALIGN_MIDDLE;
            body_cell_c.BorderWidthLeft = 0.5f;
            body_cell_c.BorderWidthBottom = 0.5f;
            body_cell_c.BorderWidthTop = 0.5f;
            body_cell_c.PaddingBottom = 5f;
            body.AddCell(body_cell_c);

            PdfPCell body_cell_l = new PdfPCell(new Phrase("Asignatura", pdf_gen.getFontDoc("tbl_footer")));
            body_cell_l.Border = Rectangle.NO_BORDER;
            body_cell_l.BorderColor = PDFGenerator.getColor("black");
            body_cell_l.HorizontalAlignment = Element.ALIGN_CENTER;
            body_cell_l.VerticalAlignment = Element.ALIGN_MIDDLE;
            body_cell_l.BorderWidthLeft = 0.5f;
            body_cell_l.BorderWidthBottom = 0.5f;
            body_cell_l.BorderWidthTop = 0.5f;
            body_cell_l.PaddingBottom = 5f;
            body.AddCell(body_cell_l);

            // foreach (string option in new string[] { "UNIVERSIDAD", "PAIS", "RESOLUCIÓN", "PROM", "CRED", "TIPO", "COND", "ESTADO", "SECCIÓN" })
            foreach (string option in new string[] { "Observación", "Prom", "Créd", "Tipo", "Estado" })
            {
                PdfPCell body_cell_h_tmp = new PdfPCell(new Phrase(option, pdf_gen.getFontDoc("tbl_footer")));
                body_cell_h_tmp.Border = Rectangle.NO_BORDER;
                body_cell_h_tmp.HorizontalAlignment = Element.ALIGN_CENTER;
                body_cell_h_tmp.VerticalAlignment = Element.ALIGN_MIDDLE;
                body_cell_h_tmp.PaddingLeft = 0f;
                body_cell_h_tmp.PaddingRight = 0f;
                body_cell_h_tmp.PaddingBottom = 5f;
                body_cell_h_tmp.BorderWidthTop = 0.5f;
                body_cell_h_tmp.BorderWidthBottom = 0.5f;
                body.AddCell(body_cell_h_tmp);
            }

            PdfPCell body_cell_r = new PdfPCell(new Phrase("período", pdf_gen.getFontDoc("tbl_footer")));
            body_cell_r.Border = Rectangle.NO_BORDER;
            body_cell_r.BorderColor = PDFGenerator.getColor("black");
            body_cell_r.HorizontalAlignment = Element.ALIGN_CENTER;
            body_cell_r.VerticalAlignment = Element.ALIGN_MIDDLE;
            body_cell_r.BorderWidthRight = 0.5f;
            body_cell_r.BorderWidthBottom = 0.5f;
            body_cell_r.BorderWidthTop = 0.5f;
            body_cell_r.PaddingBottom = 5f;
            body.AddCell(body_cell_r);

            int as_obl_apro = 0;
            int as_ele_apro = 0;
            int cr_obl_apro = 0;
            int cr_ele_apro = 0;
            int as_obl_desapro = 0;
            int as_ele_desapro = 0;
            int cr_obl_desapro = 0;
            int cr_ele_desapro = 0;
            int as_obl_desapro_inactiv = 0;
            int cr_obl_desapro_inactiv = 0;

            foreach (CourseDetail course in data.courses.OrderBy(f => f.term))
            {

                if (course.is_aproved)
                {
                    if (course.type == "O")
                    {
                        as_obl_apro++;
                        cr_obl_apro += course.credits;
                    }
                    else
                    {
                        as_ele_apro++;
                        cr_ele_apro += course.credits;
                    }
                }
                //else
                //{
                //    // un curso  desaprobado no se cuenta si el mismo curso fue aprovado posteriormente
                //    //verificacion si se aprobo posteriomente
                //    int aproves = data.courses.Where(f => f.id == course.id && f.is_aproved == true).Count();
                //    if (aproves < 1)
                //    {
                //        if (course.type == "O")
                //        {
                //            as_obl_desapro++;
                //            cr_obl_desapro += course.credits;
                //        }
                //        //else
                //        //{
                //        //    as_ele_desapro++;
                //        //    cr_ele_desapro += course.credits;
                //        //}
                //    }

                //}
                
                // codigo
                PdfPCell body_cell_c_tmp = PDFGenerator.Cell.centered();
                body_cell_c_tmp.BorderWidthBottom = 0.5f;
                body_cell_c_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                //Paragraph body_cell_l_tmp_parr = new Paragraph()
                body_cell_c_tmp.AddElement(new Phrase(course.id, pdf_gen.getFontDoc("table_body")));
                body_cell_c_tmp.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                //body_cell_l_tmp.VerticalAlignment = Element.ALIGN_MIDDLE;
                body_cell_c_tmp.BorderWidthLeft = 0.5f;
                body_cell_c_tmp.PaddingLeft = 0.5f;
                body_cell_c_tmp.PaddingRight = 0f;
                body.AddCell(body_cell_c_tmp);

                // asignatura
                PdfPCell body_cell_l_tmp = PDFGenerator.Cell.centered();
                body_cell_l_tmp.BorderWidthBottom = 0.5f;
                body_cell_l_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                //Paragraph body_cell_l_tmp_parr = new Paragraph()
                body_cell_l_tmp.AddElement(new Phrase(course.name, pdf_gen.getFontDoc("table_body")));
                body_cell_l_tmp.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                //body_cell_l_tmp.VerticalAlignment = Element.ALIGN_MIDDLE;
                body_cell_l_tmp.PaddingLeft = 0f;
                body_cell_l_tmp.PaddingRight = 0f;
                body.AddCell(body_cell_l_tmp);

                // observacion
                PdfPCell body_cell_c_0_1_tmp = PDFGenerator.Cell.centered();
                body_cell_c_0_1_tmp.AddElement(new Phrase(course.observation, pdf_gen.getFontDoc("table_body")));
                body_cell_c_0_1_tmp.BorderWidthBottom = 0.5f;
                body_cell_c_0_1_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                body_cell_c_0_1_tmp.PaddingLeft = 5f;
                body_cell_c_0_1_tmp.PaddingRight = 0f;
                body.AddCell(body_cell_c_0_1_tmp);

                //PROM
                PdfPCell body_cell_c_1_tmp = PDFGenerator.Cell.centered();
                body_cell_c_1_tmp.AddElement(new Phrase(course.score.ToString(), pdf_gen.getFontDoc("table_body")));
                body_cell_c_1_tmp.BorderWidthBottom = 0.5f;
                body_cell_c_1_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                body_cell_c_1_tmp.PaddingLeft = 5f;
                body_cell_c_1_tmp.PaddingRight = 0f;
                body.AddCell(body_cell_c_1_tmp);

                //CRED
                PdfPCell body_cell_c_2_tmp = PDFGenerator.Cell.centered();
                body_cell_c_2_tmp.AddElement(new Phrase(course.credits.ToString(), pdf_gen.getFontDoc("table_body")));
                body_cell_c_2_tmp.BorderWidthBottom = 0.5f;
                body_cell_c_2_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                body_cell_c_2_tmp.PaddingLeft = 10f;
                body_cell_c_2_tmp.PaddingRight = 0f;
                body.AddCell(body_cell_c_2_tmp);

                //TIPO
                PdfPCell body_cell_c_3_tmp = PDFGenerator.Cell.centered();
                body_cell_c_3_tmp.AddElement(new Phrase(course.type, pdf_gen.getFontDoc("table_body")));
                body_cell_c_3_tmp.BorderWidthBottom = 0.5f;
                body_cell_c_3_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                body_cell_c_3_tmp.PaddingLeft = 10f;
                body_cell_c_3_tmp.PaddingRight = 0f;
                body.AddCell(body_cell_c_3_tmp);

                //ESTADO
                PdfPCell body_cell_c_4_tmp = PDFGenerator.Cell.centered();
                body_cell_c_4_tmp.AddElement(new Phrase(course.state, pdf_gen.getFontDoc("table_body")));
                body_cell_c_4_tmp.BorderWidthBottom = 0.5f;
                body_cell_c_4_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                body_cell_c_4_tmp.PaddingLeft = 10f;
                body_cell_c_4_tmp.PaddingRight = 0f;
                body.AddCell(body_cell_c_4_tmp);

                //PERIODO         
                PdfPCell body_cell_r_tmp = PDFGenerator.Cell.centered();
                body_cell_r_tmp.BorderWidthBottom = 0.5f;
                body_cell_r_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                body_cell_r_tmp.HorizontalAlignment = Element.ALIGN_CENTER;
                body_cell_r_tmp.VerticalAlignment = Element.ALIGN_MIDDLE;
                
                body_cell_r_tmp.AddElement(new Phrase(course.term, pdf_gen.getFontDoc("table_body")));
                body_cell_r_tmp.BorderWidthRight = 0.5f;
                body_cell_r_tmp.PaddingLeft = 10f;
                body.AddCell(body_cell_r_tmp);

            }
   

            //resumen
            
            /*PdfPCell body_cell_summary_tmp_line = new PdfPCell();
            body_cell_summary_tmp_line.Colspan = 8;
            //body_cell_summary_tmp_line.AddElement(new Phrase("==========================================================", pdf_gen.getFontDoc("table_body")));
            body_cell_summary_tmp_line.BorderWidthBottom = 0f;
            body_cell_summary_tmp_line.BorderWidthTop = 0f;
            body_cell_summary_tmp_line.BorderWidthLeft = 0.5f;
            body_cell_summary_tmp_line.BorderWidthRight = 0.5f;
            body_cell_summary_tmp_line.PaddingLeft = 30f;

            body.AddCell(body_cell_summary_tmp_line);

            PdfPCell body_cell_summary_tmp_line_2 = new PdfPCell();
            body_cell_summary_tmp_line_2.Colspan = 8;
            body_cell_summary_tmp_line_2.AddElement(new Phrase("(*)==========================================================", pdf_gen.getFontDoc("table_body")));
            body_cell_summary_tmp_line_2.BorderWidthBottom = 0f;
            body_cell_summary_tmp_line_2.BorderWidthTop = 0f;
            body_cell_summary_tmp_line_2.BorderWidthLeft = 0.5f;
            body_cell_summary_tmp_line_2.BorderWidthRight = 0.5f;
            body_cell_summary_tmp_line_2.PaddingLeft = 30f;

            PdfPCell body_cell_summary_tmp_line_3 = new PdfPCell();
            body_cell_summary_tmp_line_3.Colspan = 8;
            body_cell_summary_tmp_line_3.AddElement(new Phrase("==========================================================", pdf_gen.getFontDoc("table_body")));
            body_cell_summary_tmp_line_3.BorderWidthBottom = 0f;
            body_cell_summary_tmp_line_3.BorderWidthTop = 0f;
            body_cell_summary_tmp_line_3.BorderWidthLeft = 0.5f;
            body_cell_summary_tmp_line_3.BorderWidthRight = 0.5f;
            body_cell_summary_tmp_line_3.PaddingLeft = 50f;
            body_cell_summary_tmp_line_3.PaddingTop = 0f;
            body_cell_summary_tmp_line_3.PaddingBottom = 0f;*/

            PdfPCell body_cell_summary_tmp_1 = new PdfPCell();
            body_cell_summary_tmp_1.Colspan = 2;
            body_cell_summary_tmp_1.BorderWidthBottom = 0f;
            body_cell_summary_tmp_1.BorderWidthTop = 0f;
            body_cell_summary_tmp_1.BorderWidthLeft = 0.5f;
            body_cell_summary_tmp_1.BorderWidthRight = 0f;
            body_cell_summary_tmp_1.PaddingTop = 0f;
            body_cell_summary_tmp_1.PaddingBottom = 0f;

            PdfPCell body_cell_summary_tmp_2 = new PdfPCell();
            body_cell_summary_tmp_2.Colspan = 6;
            body_cell_summary_tmp_2.BorderWidthBottom = 0f;
            body_cell_summary_tmp_2.BorderWidthTop = 0f;
            body_cell_summary_tmp_2.BorderWidthLeft = 0f;
            body_cell_summary_tmp_2.BorderWidthRight = 0.5f;
            body_cell_summary_tmp_2.PaddingTop = 0f;
            body_cell_summary_tmp_2.PaddingBottom = 0f;

            PdfPCell body_cell_summary_tmp_3 = new PdfPCell();
            body_cell_summary_tmp_3.Colspan = 4;
            body_cell_summary_tmp_3.BorderWidthBottom = 0f;
            body_cell_summary_tmp_3.BorderWidthTop = 0f;
            body_cell_summary_tmp_3.BorderWidthLeft = 0.5f;
            body_cell_summary_tmp_3.BorderWidthRight = 0f;
            body_cell_summary_tmp_3.PaddingTop = 0f;
            body_cell_summary_tmp_3.PaddingBottom = 0f;

            PdfPCell body_cell_summary_tmp_4 = new PdfPCell();
            body_cell_summary_tmp_4.Colspan = 4;
            body_cell_summary_tmp_4.BorderWidthBottom = 0f;
            body_cell_summary_tmp_4.BorderWidthTop = 0f;
            body_cell_summary_tmp_4.BorderWidthLeft = 0f;
            body_cell_summary_tmp_4.BorderWidthRight = 0.5f;
            body_cell_summary_tmp_4.PaddingTop = 0f;
            body_cell_summary_tmp_4.PaddingBottom = 10f;

            PdfPCell body_cell_summary_tmp_5 = new PdfPCell();
            body_cell_summary_tmp_5.Colspan = 8;
            //body_cell_summary_tmp_5.AddElement(new Phrase("(A) - Asignatura aprobada por adicional o exámen final rezagado.", pdf_gen.getFontDoc("table_body")));
            body_cell_summary_tmp_5.BorderWidthBottom = 0.5f;
            body_cell_summary_tmp_5.BorderWidthTop = 0f;
            body_cell_summary_tmp_5.BorderWidthLeft = 0.5f;
            body_cell_summary_tmp_5.BorderWidthRight = 0.5f;
            body_cell_summary_tmp_5.PaddingLeft = 50f;
            body_cell_summary_tmp_5.PaddingTop = 0f;
            body_cell_summary_tmp_5.PaddingBottom = 10f;

            /*
               int as_obl_apro = 0;
                int as_ele_apro = 0;
                int cr_obl_apro = 0;
                int cr_ele_apro = 0;
                int as_obl_desapro = 0;
                int as_ele_desapro = 0;
                int cr_obl_desapro = 0;
                int cr_ele_desapro = 0;
             */

            /*********************************************************************/
            // SUMAR CURSOS Y CREDITOS DESAPROBADOS, busca los no aprobados y activos
            /*********************************************************************/

            var disapprove_courses = data.courses.Where(f => !f.is_aproved).Select(f => f.id).Distinct();

            foreach(var item in disapprove_courses)
            {

                var course = data.courses.FirstOrDefault(f => f.id == item);
                int aproves = data.courses.Where(f => f.id == course.id && f.is_aproved == true).Count();
                if (aproves < 1)
                {
                    if (course.type == "O")
                    {
                        if (course.is_active == "1") //obligatorio y activo
                        {
                            as_obl_desapro++;
                            cr_obl_desapro += course.credits;
                        }
                        else //obligatorio e activo
                        {
                            as_obl_desapro_inactiv++;
                            cr_obl_desapro_inactiv += course.credits;
                        }
                    }
                    else
                    {
                        as_ele_desapro++;
                        cr_ele_desapro += course.credits;
                    }
                }
            }
            /*********************************************************************/
            /*********************************************************************/

            Paragraph body_cell_summary_tmp_1_p = new Paragraph();
            body_cell_summary_tmp_1_p.Add(new Phrase(
                string.Format(
                    "Asignaturas Obligatorias Aprobadas: {0}\nAsignaturas Obligatorias Desaprobadas: {1}\nAsignaturas Electivas Aprobadas: {2}\nAsignaturas Electivas Desaprobadas: {3}\nAsignaturas Obligatorias Desaprobadas Inactivas: {4}",
                        as_obl_apro,
                        as_obl_desapro,
                        as_ele_apro,
                        as_ele_desapro,
                        as_obl_desapro_inactiv
                    ),
                    pdf_gen.getFontDoc("table_body")));
            body_cell_summary_tmp_1_p.Alignment = Element.ALIGN_CENTER;
            body_cell_summary_tmp_1.AddElement(body_cell_summary_tmp_1_p);

            body.AddCell(body_cell_summary_tmp_1);

            Paragraph body_cell_summary_tmp_2_p = new Paragraph();
            body_cell_summary_tmp_2_p.Add(new Phrase(
                string.Format(
                    "Créditos Obligatorios Aprobados: {0}\nCréditos Obligatorios Desaprobados: {1}\nCréditos Electivos Aprobados: {2}\nCréditos Electivos Desaprobados: {3}\nCréditos Obligatorias Desaprobadas Inactivas: {4}",
                    cr_obl_apro,
                    cr_obl_desapro,
                    cr_ele_apro,
                    cr_ele_desapro,
                    cr_obl_desapro_inactiv
                ),
                pdf_gen.getFontDoc("table_body")));
            body_cell_summary_tmp_2_p.Alignment = Element.ALIGN_CENTER;
            body_cell_summary_tmp_2.PaddingBottom = 10f;
            body_cell_summary_tmp_2.AddElement(body_cell_summary_tmp_2_p);

            body.AddCell(body_cell_summary_tmp_2);

            body.AddCell(body_cell_summary_tmp_5);


            Document document = PDFEvents.createSimple(data, header_pages: "HISTORIAL ACADEMICO");
            document.Add(titulo);
            document.Add(head);
            document.Add(new Paragraph("\n"));
            document.Add(body);

            try
            {
                document.Close();

                return string.Format("{0}{1}/pdf/", AppSettings.reports["output-client"], data.name_file);
            }
            catch
            {
                //Debug.Print("salto error" + ex.Message);
                return null;
            }
        }
        public class Data : PDFData
        {
            public Data()
            {
                this.courses = new List<CourseDetail>();
            }
            public List<CourseDetail> courses { get; set; }
        }
        public class CourseDetail
        {
            public int cycle { get; set; }
            public string type { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public string university { get; set; }
            public string country { get; set; }
            public string resolution { get; set; }
            public string observation { get; set; }
            public int score { get; set; }
            public int credits { get; set; }
            public string condition { get; set; }
            public string state { get; set; }
            public string section { get; set; }
            public string is_active { get; set; }
            public string term { get; set; }
            public DateTime date { get; set; }
            public bool is_aproved { get; set; }

        }

        #endregion methods
    }
}
