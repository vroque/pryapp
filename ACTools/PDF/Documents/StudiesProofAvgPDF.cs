﻿using ACTools.Configuration;
using System;
using ACTools.SuperString;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Generar PDF de Constancia de Ingreso
    /// </summary>
    public class StudiesProofAvgPDF
    {
        /// <summary>
        /// Genera la Constancia y devuelve la ruta
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string generate(StudiesProofAvgPDF.Data data)
        {
            iTextSharp.text.Document document = PDFEvents.createSimple(
                (PDFData)data,
                "El jefe(e) de la Oficina de Registros Académicos de la\nUniversidad Continental otorga:");

            PDFGenerator pdf_gen = new PDFGenerator();

            iTextSharp.text.Paragraph body_parrafo = new iTextSharp.text.Paragraph();

            iTextSharp.text.Paragraph titulo = new iTextSharp.text.Paragraph("Constancia", pdf_gen.getFontDoc("doc_title"));
            titulo.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            body_parrafo.Add(titulo);

            iTextSharp.text.Paragraph content = new iTextSharp.text.Paragraph();
            content.Add(new iTextSharp.text.Phrase("\n"));
            content.Add(new iTextSharp.text.Phrase("         A ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new iTextSharp.text.Phrase(string.Format("{0}, ", data.student_names.ToUpper()), pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new iTextSharp.text.Phrase(" identificado(a) con código de matrícula N° ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new iTextSharp.text.Phrase(data.student_id, pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new iTextSharp.text.Phrase(", de la ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new iTextSharp.text.Phrase($"Facultad de {data.college_name.ToUpper()}", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new iTextSharp.text.Phrase($", Escuela Académico Profesional de {data.program_name.ToUpper()};", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new iTextSharp.text.Phrase(" quien estuvo cursando el ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new iTextSharp.text.Phrase($"{data.last_term_period} periodo", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new iTextSharp.text.Phrase(" habiendo obtenido como promedio acumulado ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new iTextSharp.text.Phrase(string.Format("{0} ", data.avg.ToString("0.00")), pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new iTextSharp.text.Phrase("al ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new iTextSharp.text.Phrase(string.Format("{0}", data.last_term_end_date), pdf_gen.getFontDoc("doc_body")));
            content.Add(new iTextSharp.text.Phrase(", de acuerdo a las calificaciones que obran en los archivos de la Oficina de Registros Académicos a los que me remito en caso de ser necesario.", pdf_gen.getFontDoc("doc_body")));
            content.Add(new iTextSharp.text.Phrase("\n"));
            content.Add(new iTextSharp.text.Phrase("          Se expide la presente a solicitud del interesado para los fines que considere conveniente.", pdf_gen.getFontDoc("doc_body")));
            content.Alignment = iTextSharp.text.Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);
            body_parrafo.Add(content);

            iTextSharp.text.Paragraph fecha_parrafo = new iTextSharp.text.Paragraph(
                string.Format(
                    "Expedido el {0} de {1} de {2}",
                    DateTime.Now.ToString("dd"),
                    DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES")),
                    DateTime.Now.ToString("yyyy")),
                    pdf_gen.getFontDoc("doc_body")
                );
            fecha_parrafo.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            body_parrafo.Add(fecha_parrafo);

            document.Add(body_parrafo);
            try
            {
                document.Close();
                return string.Format("{0}{1}/pdf/", AppSettings.reports["output-client"], data.name_file);
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// Objesto de colleccion de datos para generar el documento
        /// </summary>
        public class Data : PDFData
        {
            public string last_term { get; set; }
            public string last_term_end_date { get; set; }
            public string last_term_period { get; set; }
            public float avg { get; set; }



        }
    }
}
