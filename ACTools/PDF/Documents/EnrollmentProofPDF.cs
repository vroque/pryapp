﻿using ACTools.Configuration;
using iTextSharp.text;
using System;
using ACTools.SuperString;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Genera PDF para Constancia de Matrícula
    /// </summary>
    public class EnrollmentProofPDF
    {
        /// <summary>
        /// Genera la Constancia y devuelve la ruta
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string generate(Data data)
        {
            var pdfGen = new PDFGenerator();
            
            var titulo = new Paragraph("Constancia de Matrícula", pdfGen.getFontDoc("doc_title"))
            {
                Alignment = Element.ALIGN_CENTER
            };

            var content = new Paragraph
            {
                new Phrase("\n"),
                new Phrase("         A ", pdfGen.getFontDoc("doc_body")),
                new Phrase($"{data.student_names.ToUpper()}, ", pdfGen.getFontDoc("doc_body_bold")),
                new Phrase(" identificado(a) con código de matrícula N° ", pdfGen.getFontDoc("doc_body")),
                new Phrase(data.student_id, pdfGen.getFontDoc("doc_body_bold")),
                new Phrase(", estudiante de la ", pdfGen.getFontDoc("doc_body")),
                new Phrase($"Facultad de {data.college_name.ToUpper()}", pdfGen.getFontDoc("doc_body_bold")),
                new Phrase("", pdfGen.getFontDoc("doc_body")),
                new Phrase($", Escuela Académico Profesional de {data.program_name.ToUpper()};", pdfGen.getFontDoc("doc_body_bold")),
                new Phrase(" quien está matriculado(a) en el ", pdfGen.getFontDoc("doc_body")),
                new Phrase($"{data.period} periodo", pdfGen.getFontDoc("doc_body_bold")),
                new Phrase(" en el ", pdfGen.getFontDoc("doc_body")),
                new Phrase(data.term, pdfGen.getFontDoc("doc_body_bold")),
                new Phrase(
                    ", tal como consta en los archivos que obran en esta dependencia a los que me remito en caso de ser necesario."
                    , pdfGen.getFontDoc("doc_body")),
                new Phrase("\n"),
                new Phrase(
                    "          Se expide la presente a solicitud del interesado para los fines que considere conveniente.",
                    pdfGen.getFontDoc("doc_body"))
            };
            content.Alignment = Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);

            var fechaParrafo = new Paragraph(
                $"Expedido el {DateTime.Now:dd} de {DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES"))} de {DateTime.Now:yyyy}",
                pdfGen.getFontDoc("doc_body")
            ) {Alignment = Element.ALIGN_RIGHT};

            var expirationText = new Paragraph
            {
                new Phrase("\n"),
                new Phrase("Este documento tiene vigencia de dos meses desde la fecha de emisión.",
                    pdfGen.getFontDoc("doc_body"))
            };
            expirationText.Alignment = Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);

            var bodyParrafo = new Paragraph {titulo, content, fechaParrafo, expirationText};

            // create document
            var document = PDFEvents.createSimple(data,
                "El jefe(e) de la Oficina de Registros Académicos de la\nUniversidad Continental otorga:");
            document.Add(bodyParrafo);
            try
            {
                document.Close();
                return $"{AppSettings.reports["output-client"]}{data.name_file}/pdf/";
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Objeto de colleccion de datos para generar el documento
        /// </summary>
        public class Data : PDFData
        {
            /// <summary>
            /// Período académico
            /// </summary>
            public string term { get; set; }

            /// <summary>
            /// Ciclo matriculado
            /// </summary>
            public string period { get; set; }
        }
    }
}
