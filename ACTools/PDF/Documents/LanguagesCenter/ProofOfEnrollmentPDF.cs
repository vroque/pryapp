﻿using System;
using System.Collections.Generic;
using ACTools.Configuration;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _util = ACTools.Util;

namespace ACTools.PDF.Documents.LanguagesCenter
{
    public class ProofOfEnrollmentPDF
    {
        /// <summary>
        /// Creación del PDF Constancia de Matrícula
        /// </summary>
        /// <param name="data">Datos del documento</param>
        /// <returns>URI del documento generado</returns>
        public static string Generate(Data data)
        {
            var pdfGenerator = new PDFGenerator();

            var document = PDFEvents.LanguagesCenter(data.Source, data,
                "El Director del Centro de Idiomas de la Universidad Continental, deja:");

            var bodyParagraph = new iTextSharp.text.Paragraph();

            var title = new iTextSharp.text.Paragraph("Constancia de Matrícula", pdfGenerator.getFontDoc("doc_title"))
            {
                Alignment = iTextSharp.text.Element.ALIGN_CENTER
            };

            bodyParagraph.Add(title);

            var content = new iTextSharp.text.Paragraph
            {
                new iTextSharp.text.Phrase("\n"),
                new iTextSharp.text.Phrase("        "),
                new iTextSharp.text.Phrase($"Que {data.GenderData["civil_status"]} ",
                    pdfGenerator.getFontDoc("doc_body")),
                new iTextSharp.text.Phrase(data.student_names, pdfGenerator.getFontDoc("doc_body_bold")),
                new iTextSharp.text.Phrase(", con Código de Matrícula N° ", pdfGenerator.getFontDoc("doc_body")),
                new iTextSharp.text.Phrase($"{data.student_id}", pdfGenerator.getFontDoc("doc_body_bold")),
                new iTextSharp.text.Phrase($", se encuentra {data.GenderData["enrolled"]} en el ",
                    pdfGenerator.getFontDoc("doc_body")),
                new iTextSharp.text.Phrase($"Ciclo {data.LanguageCycle} ", pdfGenerator.getFontDoc("doc_body_bold")),
                new iTextSharp.text.Phrase("del ", pdfGenerator.getFontDoc("doc_body")),
                new iTextSharp.text.Phrase($"Nivel {data.LanguageLevel} ", pdfGenerator.getFontDoc("doc_body_bold")),
                new iTextSharp.text.Phrase("del ", pdfGenerator.getFontDoc("doc_body")),
                new iTextSharp.text.Phrase($"Idioma {data.LanguageName}", pdfGenerator.getFontDoc("doc_body_bold")),
                new iTextSharp.text.Phrase(", que se desarrollará del ", pdfGenerator.getFontDoc("doc_body")),
                new iTextSharp.text.Phrase($"{data.StartDate:dd} de {data.StartDate:MMMM} de {data.StartDate:yyyy}",
                    pdfGenerator.getFontDoc("doc_body_bold")),
                new iTextSharp.text.Phrase(" hasta el ", pdfGenerator.getFontDoc("doc_body")),
                new iTextSharp.text.Phrase($"{data.EndDate:dd} de {data.EndDate:MMMM} de {data.EndDate:yyyy}",
                    pdfGenerator.getFontDoc("doc_body_bold")),
//                new iTextSharp.text.Phrase(", en el horario ", pdfGenerator.getFontDoc("doc_body")),
//                new iTextSharp.text.Phrase($"{data.Schedule} ", pdfGenerator.getFontDoc("doc_body_bold")),
                new iTextSharp.text.Phrase(".\n\n", pdfGenerator.getFontDoc("doc_body")),
                new iTextSharp.text.Phrase("        ", pdfGenerator.getFontDoc("doc_body")),
                new iTextSharp.text.Phrase($"Se expide la presente a solicitud {data.GenderData["concerned"]} ",
                    pdfGenerator.getFontDoc("doc_body")),
                new iTextSharp.text.Phrase("para los fines pertinentes.\n\n", pdfGenerator.getFontDoc("doc_body"))
            };

            content.Alignment = iTextSharp.text.Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);

            bodyParagraph.Add(content);

            var dateParagraph =
                new iTextSharp.text.Paragraph(
                    $"Expedido el {DateTime.Now:dd} de {DateTime.Now:MMMM} de {DateTime.Now:yyyy}",
                    pdfGenerator.getFontDoc("doc_body")) {Alignment = iTextSharp.text.Element.ALIGN_RIGHT};

            bodyParagraph.Add(dateParagraph);

            document.Add(bodyParagraph);
            try
            {
                document.Close();
                return $"{AppSettings.reports[_cic.URI_CIC_PDF]}{data.name_file}/";
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Retorna URI de Constancia de Matrícula.
        /// * Verifica si existe PDF generado para retornar su URI
        /// * Sino existe el PDF, lo genera y luego retorna su URI
        /// </summary>
        /// <param name="data">Datos del documento</param>
        /// <returns>URI del documento</returns>
        /// <exception cref="ApplicationException"></exception>
        public static string GetUri(Data data)
        {
            if (data.name_file.Length <= 10)
            {
                throw new ApplicationException(
                    "No se puede realizar una búsqueda de documentos con menos de 10 caracteres.");
            }

            return _util.LanguagesCenter.ExistDocument(data.name_file)
                ? $"{AppSettings.reports[_cic.URI_CIC_PDF]}{data.name_file}"
                : Generate(data);
        }

        /// <summary>
        /// Datos básicos para la Constancia de Matrícula
        /// </summary>
        public class Data : PDFData
        {
            public Dictionary<string, string> GenderData { get; set; }
            public string LanguageCycle { get; set; }
            public string LanguageLevel { get; set; }
            public string LanguageName { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public string Schedule { get; set; }
            public string Source { get; set; }
        }
    }
}