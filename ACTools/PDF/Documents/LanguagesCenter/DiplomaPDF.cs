﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ACTools.Configuration;
using iTextSharp.text;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _util = ACTools.Util;

namespace ACTools.PDF.Documents.LanguagesCenter
{
    /// <summary>
    /// Diploma CIC
    /// </summary>
    public class DiplomaPDF
    {
        private static readonly Dictionary<string, List<string>> DiplomaText =
            new Dictionary<string, List<string>>
            {
                {_cic.INGLES, new List<string> {EnglishHeaderText, EnglishBodyText}},
                {_cic.PORTUGUES, new List<string> {PortugueseHeaderText, PortugueseBodyText}},
                {_cic.ITALIANO, new List<string> {ItalianHeaderText, ItalianBodyTExt}}
            };

        private const string EnglishHeaderText = "El Centro de Idiomas Continental awards this Diploma to:";

        private const string EnglishBodyText =
            "For having completed satisfactory the English Course in the {0} Level, with {1} hours of tuition.";

        private const string PortugueseHeaderText = "El Centro de Idiomas Continental confere esta Diploma a:";

        private const string PortugueseBodyText =
            "Por haver concluído e aprovado nos níveis {0} o Idioma Português com {1} horas de capacitação.";

        private const string ItalianHeaderText = "El Centro de Idiomas Continental questo Diploma a:";

        private const string ItalianBodyTExt =
            "Per avere finito soddisfacentemente il corso di Lingua Italiana nei liveli {0} con {1} ore di lezione.";

        /// <summary>
        /// Creación del PDF de Diploma
        /// </summary>
        /// <param name="data">Datos del documento/param>
        /// <returns></returns>
        public static string Generate(Data data)
        {
            var pdfGenerator = new PDFGenerator();
            var docTitleFont = pdfGenerator.getFontDoc("doc_title");
            var docBodyFont = pdfGenerator.getFontDoc("doc_body");
            var docBodyBoldFont = pdfGenerator.getFontDoc("doc_body_bold");

            var document = PDFEvents.LanguagesCenter(data.Source, data, typeDoc: "cicDiploma", rotate: true);

            var bodyParagraph = new Paragraph();

            var title = new Paragraph("Diploma", docTitleFont) {Alignment = Element.ALIGN_CENTER};

            bodyParagraph.Add(title);

            var headerText = new Paragraph {new Phrase(DiplomaText[data.LanguageId][0], docBodyFont)};
            headerText.Alignment = Element.ALIGN_CENTER;
            headerText.SetLeading(1.0f, 2.0f);
            bodyParagraph.Add(headerText);

            var studentParagraph = new Paragraph
            {
                new Phrase("\n\n"),
                new Phrase($"{data.student_names}", docTitleFont),
            };
            studentParagraph.Alignment = Element.ALIGN_CENTER;
            studentParagraph.SetLeading(1.0f, 2.0f);
            bodyParagraph.Add(studentParagraph);

            var totalHours = data.LanguageId == _cic.INGLES ? data.Cycle * 40 : data.Cycle * 30;

            var content = new Paragraph
            {
                new Phrase("\n"),
                new Phrase(string.Format(DiplomaText[data.LanguageId][1], data.LanguageLevelName, totalHours),
                    docBodyFont),
                new Phrase("\n\n"),
            };

            content.Alignment = Element.ALIGN_LEFT;
            content.SetLeading(1.0f, 2.0f);

            bodyParagraph.Add(content);

            Paragraph dateParagraph;

            switch (data.LanguageId)
            {
                case _cic.INGLES:
                    dateParagraph = new Paragraph(
                        DateTime.Now.ToString("MMMM ", CultureInfo.GetCultureInfo("en-US")) +
                        $"{_util.Number.AddOrdinal(DateTime.Now.Day)}, " +
                        $"{DateTime.Now.Year} in Huancayo, Peru",
                        docBodyFont) {Alignment = Element.ALIGN_RIGHT};
                    break;
                case _cic.PORTUGUES:
                    dateParagraph = new Paragraph(
                        "Outorgado o dia " +
                        $"{_util.Number.AddOrdinal(DateTime.Now.Day)} de " +
                        DateTime.Now.ToString("MMMM ", CultureInfo.GetCultureInfo("pt-BR")) +
                        $"{DateTime.Now.Year} em Huancayo, Peru",
                        docBodyFont) {Alignment = Element.ALIGN_RIGHT};
                    break;
                case _cic.ITALIANO:
                    dateParagraph = new Paragraph(
                        "Concesso il" +
                        $"{_util.Number.AddOrdinal(DateTime.Now.Day)}, " +
                        DateTime.Now.ToString("MMMM ", CultureInfo.GetCultureInfo("it-IT")) +
                        $"{DateTime.Now.Year} a Huancayo, Perú",
                        docBodyFont) {Alignment = Element.ALIGN_RIGHT};
                    break;
                default:
                    throw new NullReferenceException("Idioma desconocido.");
            }

            bodyParagraph.Add(dateParagraph);
            document.Add(bodyParagraph);

            try
            {
                document.Close();
                return $"{AppSettings.reports[_cic.URI_CIC_PDF]}{data.name_file}/";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        /// <summary>
        /// Retorna URL de Diploma.
        /// * Verifica si existe PDF generado para retornar su URL
        /// * Sino existe el PDF, lo genera y luego retorna su URL
        /// </summary>
        /// <param name="data">Datos del documento</param>
        /// <returns>URL del documento</returns>
        /// <exception cref="ApplicationException"></exception>
        public static string GetUri(Data data)
        {
            if (data.name_file.Length <= 10)
            {
                throw new ApplicationException(
                    "No se puede realizar una búsqueda de documentos con menos de 10 caracteres.");
            }

            return _util.LanguagesCenter.ExistDocument(data.name_file)
                ? $"{AppSettings.reports[_cic.URI_CIC_PDF]}{data.name_file}"
                : Generate(data);
        }

        public class Data : PDFData
        {
            public int Cycle { get; set; }
            public string LanguageId { get; set; }
            public string LanguageLevelName { get; set; }
            public string Source { get; set; }
        }
    }
}