﻿using System;
using System.Collections.Generic;
using ACTools.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _util = ACTools.Util;

namespace ACTools.PDF.Documents.LanguagesCenter
{
    public class StudiesCertificatePDF
    {
        /// <summary>
        /// Genera Certificado de Estudios
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string Generate(Data data)
        {
            var pdfGenerator = new PDFGenerator();
            var docTitleFont = pdfGenerator.getFontDoc("doc_title");
            var docBodyFont = pdfGenerator.getFontDoc("doc_body");
            var docBodyBoldFont = pdfGenerator.getFontDoc("doc_body_bold");
            var tableHeaderFont = pdfGenerator.getFontDoc("tbl_header");
            var tableTitleFont = pdfGenerator.getFontDoc("tbl_title");
            var tableContentFont = pdfGenerator.getFontDoc("tbl_content");
            var tableFooterFont = pdfGenerator.getFontDoc("tbl_footer");
            var blackColor = PDFGenerator.getColor("black");

            var document =
                PDFEvents.LanguagesCenterWithPhoto(data.Source, data, headerPages: "Certificado de Estudios".ToUpper());

            var title = new Paragraph("Certificado de estudios", docTitleFont)
            {
                Alignment = Element.ALIGN_CENTER
            };
            document.Add(title);

            var subTitle = new Paragraph
            {
                new Phrase("El Director del Centro de Idiomas Continental, certifica:\n\n", docBodyFont),
            };
            subTitle.Alignment = Element.ALIGN_CENTER;
            subTitle.SetLeading(1.0f, 2.0f);
            document.Add(subTitle);

            var preface = new Paragraph
            {
                new Phrase("Que ", docBodyFont),
                new Phrase(data.student_names, docBodyBoldFont),
                new Phrase(", con código de matrícula ", docBodyFont),
                new Phrase(data.student_id, docBodyBoldFont),
                new Phrase(", ha cursado el idioma ", docBodyFont),
                new Phrase(data.LanguageName, docBodyBoldFont),
                new Phrase(", obteniendo las siguientes calificaciones:", docBodyFont),
                new Phrase("\n\n")
            };
            preface.Alignment = Element.ALIGN_JUSTIFIED;
            preface.SetLeading(1.0f, 2.0f);
            document.Add(preface);

            // Table
            var contentTable = new PdfPTable(22)
            {
                WidthPercentage = 100,
                HorizontalAlignment = 0
            };
            contentTable.SetWidths(new[]
            {
                40, 25, 55, 20, 20, 20, 20, 20, 20, 20,
                20, 20, 20, 20, 20, 20, 20, 20, 25, 40,
                40, 55
            });

            // header
            string[] tableHeadText =
            {
                "Período", "Ciclo", "Sección", "P1", "P2", "P3", "P4", "P5", "P6", "PP1",
                "WB", "Lab", "FT", "PP2", "EP", "EF", "PP3", "PP4", "Prom", "Inicio",
                "Fin", "Modalidad"
            };
            foreach (var head in tableHeadText)
            {
                var cell = PDFGenerator.Cell.centered();
                var paragraph = new Paragraph(head, tableHeaderFont) {Alignment = Element.ALIGN_CENTER};
                cell.AddElement(paragraph);
                cell.Border = Rectangle.BOX;
                cell.PaddingTop = 2f;
                cell.PaddingBottom = 7f;
                cell.BorderColor = blackColor;
                contentTable.AddCell(cell);
            }

            // scores
            foreach (var score in data.Scores)
            {
                var bodyTermCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, borderLeft: 0.5f,
                    paddingLeft: 5f, alignCenter: true);
                bodyTermCell.AddElement(new Phrase(score.Term, tableContentFont));
                contentTable.AddCell(bodyTermCell);

                var bodyCycleCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 5f,
                    alignCenter: true);
                bodyCycleCell.AddElement(new Phrase(score.Cycle, tableContentFont));
                contentTable.AddCell(bodyCycleCell);

                var bodyClassroomCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f,
                    alignCenter: true);
                bodyClassroomCell.AddElement(new Phrase(score.Classroom, tableContentFont));
                contentTable.AddCell(bodyClassroomCell);

                var bodyP1Cell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyP1Cell.AddElement(new Phrase(score.P1.ToString(), tableContentFont));
                contentTable.AddCell(bodyP1Cell);

                var bodyP2Cell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyP2Cell.AddElement(new Phrase(score.P2.ToString(), tableContentFont));
                contentTable.AddCell(bodyP2Cell);

                var bodyP3Cell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyP3Cell.AddElement(new Phrase(score.P3.ToString(), tableContentFont));
                contentTable.AddCell(bodyP3Cell);

                var bodyP4Cell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyP4Cell.AddElement(new Phrase(score.P4.ToString(), tableContentFont));
                contentTable.AddCell(bodyP4Cell);

                var bodyP5Cell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyP5Cell.AddElement(new Phrase(score.P5.ToString(), tableContentFont));
                contentTable.AddCell(bodyP5Cell);

                var bodyP6Cell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyP6Cell.AddElement(new Phrase(score.P6.ToString(), tableContentFont));
                contentTable.AddCell(bodyP6Cell);

                var bodyPp1Cell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyPp1Cell.AddElement(new Phrase(score.Pp1.ToString(), tableContentFont));
                contentTable.AddCell(bodyPp1Cell);

                var bodyWbCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyWbCell.AddElement(new Phrase(score.Wb.ToString(), tableContentFont));
                contentTable.AddCell(bodyWbCell);

                var bodyLabCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyLabCell.AddElement(new Phrase(score.Lab.ToString(), tableContentFont));
                contentTable.AddCell(bodyLabCell);

                var bodyFtCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyFtCell.AddElement(new Phrase(score.Ft.ToString(), tableContentFont));
                contentTable.AddCell(bodyFtCell);

                var bodyPp2Cell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyPp2Cell.AddElement(new Phrase(score.Pp2.ToString(), tableContentFont));
                contentTable.AddCell(bodyPp2Cell);

                var bodyEpCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyEpCell.AddElement(new Phrase(score.Ep.ToString(), tableContentFont));
                contentTable.AddCell(bodyEpCell);

                var bodyEfCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyEfCell.AddElement(new Phrase(score.Ef.ToString(), tableContentFont));
                contentTable.AddCell(bodyEfCell);

                var bodyPp3Cell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyPp3Cell.AddElement(new Phrase(score.Pp3.ToString(), tableContentFont));
                contentTable.AddCell(bodyPp3Cell);

                var bodyPp4Cell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 3f,
                    alignCenter: true);
                bodyPp4Cell.AddElement(new Phrase(score.Pp4.ToString(), tableContentFont));
                contentTable.AddCell(bodyPp4Cell);

                var bodyPromCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f, paddingLeft: 5f,
                    alignCenter: true);
                bodyPromCell.AddElement(new Phrase(score.Prom.ToString(), tableContentFont));
                contentTable.AddCell(bodyPromCell);

                var bodyBeginCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f,
                    paddingLeft: 1, alignCenter: true);
                bodyBeginCell.AddElement(new Phrase($"{score.Begin:dd/MM/yy}", tableContentFont));
                contentTable.AddCell(bodyBeginCell);

                var bodyEndCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f,
                    paddingLeft: 1, alignCenter: true);
                bodyEndCell.AddElement(new Phrase($"{score.End:dd/MM/yy}", tableContentFont));
                contentTable.AddCell(bodyEndCell);

                var bodyModalityCell = _util.File.Pdf.PdfPCell.New(borderRight: 0.5f, borderBottom: 0.5f,
                    paddingLeft: 3f, alignCenter: true);
                bodyModalityCell.AddElement(new Phrase(
                    score.Modality.Length > 14 ? score.Modality.Substring(0, 14) : score.Modality, tableContentFont));
                contentTable.AddCell(bodyModalityCell);
            }

            // line
            var endCell = new PdfPCell
            {
                Colspan = 22,
                PaddingLeft = 0,
                BorderWidthTop = 0,
                BorderWidthRight = 0.5f,
                BorderWidthBottom = 0,
                BorderWidthLeft = 0.5f,
                BorderColorBottom = blackColor
            };
            contentTable.AddCell(endCell);

            // Descripción abreviaturas
            var leftSummaryCell = new PdfPCell
            {
                Colspan = 8,
                BorderWidthTop = 0,
                BorderWidthRight = 0,
                BorderWidthBottom = 0,
                BorderWidthLeft = 0,
                PaddingTop = 0,
                PaddingBottom = 3,
                PaddingLeft = 0
            };
            var rightSummaryCell = new PdfPCell
            {
                Colspan = 14,
                BorderWidthTop = 0,
                BorderWidthRight = 0,
                BorderWidthBottom = 0,
                BorderWidthLeft = 0,
                PaddingTop = 0,
                PaddingRight = 40,
                PaddingBottom = 3
            };

            var leftSummaryBodyCell = new Paragraph
            {
                new Phrase("P1: Promedio Expresión Escrita\n", tableFooterFont),
                new Phrase("P2: Promedio Expresión Oral\n", tableFooterFont),
                new Phrase("P3: Promedio Comprensión Escrita\n", tableFooterFont),
                new Phrase("P4: Promedio Comprensión Oral\n", tableFooterFont),
                new Phrase("P5: Promedio Adicional 1\n", tableFooterFont),
                new Phrase("P6: Promedio Adicional 2\n", tableFooterFont),
                new Phrase("PR o PP1: Promedio Parcial\n", tableFooterFont),
                new Phrase("WB: Workbook online\n", tableFooterFont)
            };
            leftSummaryBodyCell.Alignment = Element.ALIGN_LEFT;
            leftSummaryCell.AddElement(leftSummaryBodyCell);
            contentTable.AddCell(leftSummaryCell);

            var rightSummaryBodyCell = new Paragraph
            {
                new Phrase("LAB: Laboratorio y/o Proyecto\n", tableFooterFont),
                new Phrase("FT: Fast Test\n", tableFooterFont),
                new Phrase("PP2: Workbook online y Laboratorio\n", tableFooterFont),
                new Phrase("EP: Examen Parcial\n", tableFooterFont),
                new Phrase("EF: Examen Final\n", tableFooterFont),
                new Phrase("PP3: Promedio Parcial y Examen Final; Examen Parcial y Examen Final\n", tableFooterFont),
                new Phrase("PP4 o EE: PP1 y PP2 o Esfuerzo del Estudiante\n", tableFooterFont),
                new Phrase("PROM: Promedio Final\n", tableFooterFont)
            };
            rightSummaryBodyCell.Alignment = Element.ALIGN_LEFT;
            rightSummaryCell.AddElement(rightSummaryBodyCell);
            contentTable.AddCell(rightSummaryCell);

            var disclaimerCell = new PdfPCell
            {
                Colspan = 22,
                BorderWidthTop = 0,
                BorderWidthRight = 0,
                BorderWidthBottom = 0,
                BorderWidthLeft = 0,
                PaddingLeft = 0,
                PaddingBottom = 3
            };
            disclaimerCell.AddElement(
                new Phrase("Los promedios que aparecen en blanco no son obligatorios y no afectan al promedio final.",
                    tableFooterFont));
            contentTable.AddCell(disclaimerCell);

            document.Add(contentTable);
            try
            {
                document.Close();
                return $"{AppSettings.reports[_cic.URI_CIC_PDF]}{data.name_file}";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        /// <summary>
        /// Retorna URL de Diploma.
        /// * Verifica si existe PDF generado para retornar su URL
        /// * Sino existe el PDF, lo genera y luego retorna su URL
        /// </summary>
        /// <param name="data">Datos del documento</param>
        /// <returns>URL del documento</returns>
        /// <exception cref="ApplicationException"></exception>
        public static string GetUri(Data data)
        {
            if (data.name_file.Length <= 10)
            {
                throw new ApplicationException(
                    "No se puede realizar una búsqueda de documentos con menos de 10 caracteres.");
            }

            return _util.LanguagesCenter.ExistDocument(data.name_file)
                ? $"{AppSettings.reports[_cic.URI_CIC_PDF]}{data.name_file}"
                : Generate(data);
        }

        public class Data : PDFData
        {
            public string LanguageId { get; set; }
            public string LanguageName { get; set; }
            public string Source { get; set; }
            public List<Score> Scores { get; set; }
        }

        public class Score
        {
            public string Term { get; set; }
            public string Cycle { get; set; }
            public string Classroom { get; set; }
            public int? P1 { get; set; }
            public int? P2 { get; set; }
            public int? P3 { get; set; }
            public int? P4 { get; set; }
            public int? P5 { get; set; }
            public int? P6 { get; set; }
            public int? Pp1 { get; set; }
            public int? Pr { get; set; }
            public int? Pp { get; set; }
            public int? Wb { get; set; }
            public int? Lab { get; set; }
            public int? Ft { get; set; }
            public int? Pp2 { get; set; }
            public int? Ep { get; set; }
            public int? Ef { get; set; }
            public int? Pp3 { get; set; }
            public int? Pp4 { get; set; }
            public int? Ee { get; set; }
            public int? Prom { get; set; }
            public DateTime? Begin { get; set; }
            public DateTime? End { get; set; }
            public string Modality { get; set; }
            public int? Style { get; set; }
        }
    }
}