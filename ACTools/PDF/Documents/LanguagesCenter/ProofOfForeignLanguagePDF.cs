﻿using System;
using System.Collections.Generic;
using ACTools.Configuration;
using ACTools.SuperString;
using iTextSharp.text;
using _cau = ACTools.Constants.AtentionCenterConstants;
using _cic = ACTools.Constants.LanguagesCenterConstants;
using _util = ACTools.Util;

namespace ACTools.PDF.Documents.LanguagesCenter
{
    public class ProofOfForeignLanguagePDF
    {
        /// <summary>
        /// Creación PDF Constancia de Acreditación de Idioma Extranjero
        /// </summary>
        /// <param name="data">Datos del documento</param>
        /// <returns>URI del documento generado</returns>
        public static string Generate(Data data)
        {
            var pdfGenerator = new PDFGenerator();
            var docTitleFont = pdfGenerator.getFontDoc("doc_title");
            var docBodyFont = pdfGenerator.getFontDoc("doc_body");
            var docBodyBoldFont = pdfGenerator.getFontDoc("doc_body_bold");

            var document = PDFEvents.LanguagesCenter(data.Source, data,
                "El Director del Centro de Idiomas de la Universidad Continental, deja:");

            var bodyParagraph = new Paragraph();

            var title =
                new Paragraph("Constancia".ToUpper(), docTitleFont)
                {
                    Alignment = Element.ALIGN_CENTER
                };

            bodyParagraph.Add(title);

            var content = new Paragraph
            {
                new Phrase("\n"),
                new Phrase("        "),
                new Phrase($"Que {data.GenderData["civil_status"]} ", docBodyFont),
                new Phrase(data.student_names, docBodyBoldFont),
                new Phrase(", con Código de Matrícula N° ", docBodyFont),
                new Phrase($"{data.student_id}", docBodyBoldFont)
            };


            switch (data.DocumentType)
            {
                case _cau.DOC_CIC_CAIE:
                    content.Add(new Phrase(", de la facultad ", docBodyFont));
                    content.Add(new Phrase($"{data.college_name.toEachCapitalize()}", docBodyBoldFont));
                    content.Add(new Phrase(", EAP ", docBodyFont));
                    content.Add(new Phrase($"{data.program_name.toEachCapitalize()}", docBodyBoldFont));
                    content.Add(new Phrase(", en la modalidad ", docBodyFont));
                    content.Add(new Phrase($"{data.GraduationModality}", docBodyBoldFont));
                    content.Add(new Phrase(", ha terminado sus estudios en el Nivel ", docBodyFont));
                    content.Add(new Phrase($"{data.LanguageLevelName} ", docBodyBoldFont));
                    content.Add(new Phrase("del Idioma ", docBodyFont));
                    content.Add(new Phrase($"{data.LanguageName}", docBodyBoldFont));
                    content.Add(new Phrase(", lo cual le permite acreditar el conocimiento de ", docBodyFont));
                    content.Add(new Phrase("un idioma extranjero de acuerdo al Reglamento de la ", docBodyFont));
                    content.Add(new Phrase("Universidad según su plan de estudios.", docBodyFont));
                    content.Add(new Phrase("\n\n"));
                    content.Add(new Phrase("        ", docBodyFont));
                    content.Add(new Phrase($"Se expide la presente en solicitud ", docBodyFont));
                    content.Add(new Phrase($"{data.GenderData["concerned"]} ", docBodyFont));
                    content.Add(new Phrase("para los trámites de la constancia de egresado.\n\n", docBodyFont));
                    break;
                case _cau.DOC_CIC_CAIE_EXT:
                case _cau.DOC_CIC_CAIE_INST:
                    content.Add(new Phrase(", ha culminado el Nivel ", docBodyFont));
                    content.Add(new Phrase($"{data.LanguageLevelName} ", docBodyBoldFont));
                    content.Add(new Phrase("del Idioma ", docBodyFont));
                    content.Add(new Phrase($"{data.LanguageName}", docBodyBoldFont));
                    content.Add(new Phrase(", con calificación de ", docBodyFont));
                    content.Add(new Phrase($"{data.Avg}/20", docBodyBoldFont));
                    content.Add(new Phrase(", con una duración de ", docBodyFont));
                    content.Add(new Phrase($"{data.TotalHours} ", docBodyBoldFont));
                    content.Add(new Phrase("horas lectivas, desarrolladas del ", docBodyFont));
                    content.Add(new Phrase($"{data.StartDate:dd} de {data.StartDate:MMMM} ", docBodyBoldFont));
                    content.Add(new Phrase($"de {data.StartDate:yyyy} ", docBodyBoldFont));
                    content.Add(new Phrase("al ", docBodyFont));
                    content.Add(new Phrase($"{data.EndDate:dd} de {data.EndDate:MMMM} ", docBodyBoldFont));
                    content.Add(new Phrase($"de {data.EndDate:yyyy}", docBodyBoldFont));
                    content.Add(new Phrase(".", docBodyFont));
                    content.Add(new Phrase("\n\n"));
                    content.Add(new Phrase("        ", docBodyFont));
                    content.Add(new Phrase($"Se expide la presente en solicitud {data.GenderData["concerned"]} ",
                        docBodyFont));
                    content.Add(new Phrase("para los fines pertinentes.\n\n", docBodyFont));
                    break;
                default:
                    throw new SystemException($"Tipo de documento desconocido {data.DocumentType}");
            }

            content.Alignment = Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);

            bodyParagraph.Add(content);

            var dateParagraph =
                new Paragraph($"Expedido el {DateTime.Now:dd} de {DateTime.Now:MMMM} de {DateTime.Now:yyyy}", docBodyFont)
                {
                    Alignment = Element.ALIGN_RIGHT
                };

            bodyParagraph.Add(dateParagraph);

            document.Add(bodyParagraph);
            try
            {
                document.Close();
                return data.Source == _cic.SRC_CAIE_PDF
                    ? $"{AppSettings.reports[_cic.URI_CAIE_PDF]}{data.name_file}/"
                    : $"{AppSettings.reports[_cic.URI_CIC_PDF]}{data.name_file}/";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        /// <summary>
        /// Retorna URI de Constancia de Acreditación de Idioma Extranjero.
        /// * Verifica si existe PDF generado para retornar su URI
        /// * Sino existe el PDF, lo genera y luego retorna su URI
        /// </summary>
        /// <param name="data">Datos del documento</param>
        /// <returns>URI del documento</returns>
        /// <exception cref="ApplicationException"></exception>
        public static string GetUri(Data data)
        {
            if (data.name_file.Length <= 10)
            {
                throw new ApplicationException(
                    "No se puede realizar una búsqueda de documentos con menos de 10 caracteres.");
            }

            return _util.LanguagesCenter.ExistCaieDocument(data.name_file)
                ? $"{AppSettings.reports[_cic.URI_CAIE_PDF]}{data.name_file}"
                : Generate(data);
        }

        /// <summary>
        /// Datos básicos para la Constancia de Acreditación de Idioma Extranjero
        /// </summary>
        public class Data : PDFData
        {
            public Dictionary<string, string> GenderData { get; set; }
            public string LanguageLevelName { get; set; }
            public string LanguageName { get; set; }
            public decimal? FinalAverage { get; set; }
            public decimal? TotalHours { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string GraduationModality { get; set; }
            public int DocumentType { get; set; }
            public double? Avg { get; set; }
            public string Source { get; set; }
        }
    }
}