﻿using ACTools.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using static ACTools.PDF.PDFGenerator;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Genera PDF para Constancia de Matrícula PRONABEC
    /// </summary>
    public abstract class PronabecProofOfEnrollmentPdf
    {
        /// <summary>
        /// Genera la Constancia y devuelve la ruta
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string Generate(Data data)
        {
            var pdfGen = new PDFGenerator();

            var titulo =
                new Paragraph("Constancia de matrícula", pdfGen.getFontDoc("doc_title"))
                {
                    Alignment = Element.ALIGN_CENTER
                };


            var content = new Paragraph
            {
                new Phrase("\n"),
                new Phrase("         A ", pdfGen.getFontDoc("doc_body")),
                new Phrase($"{data.student_names}, ", pdfGen.getFontDoc("doc_body_bold")),
                new Phrase(" identificado(a) con código de Matrícula N° ", pdfGen.getFontDoc("doc_body")),
                new Phrase(data.student_id, pdfGen.getFontDoc("doc_body_bold")),
                new Phrase(", estudiante de la Facultad de ", pdfGen.getFontDoc("doc_body")),
                new Phrase($"{data.college_name}, Escuela Académico Profesional de {data.program_name};"
                    , pdfGen.getFontDoc("doc_body_bold")),
                new Phrase(" quien está matriculado en el ", pdfGen.getFontDoc("doc_body")),
                new Phrase($"{data.Period} periodo", pdfGen.getFontDoc("doc_body_bold")),
                new Phrase(" en el ", pdfGen.getFontDoc("doc_body")),
                new Phrase(data.Term, pdfGen.getFontDoc("doc_body_bold")),
                new Phrase(", en las siguientes asignaturas:", pdfGen.getFontDoc("doc_body")),
                new Phrase("\n\n")
            };
            //start table
            content.Alignment = Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);


            /*====================================*/

            var table = Table.create(6);
            table.WidthPercentage = 100;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.DefaultCell.Padding = 5f;
            table.DefaultCell.BorderWidthLeft = 0.5f;
            table.DefaultCell.BorderWidthRight = 0.5f;
            table.DefaultCell.BorderWidthTop = 0.5f;
            table.DefaultCell.BorderWidthBottom = 0.5f;

            var widths = new[] {10f, 15f, 5f, 4f, 4f, 4f};
            table.SetWidths(widths);

            var bodyCellL =
                new PdfPCell(new Phrase("CÓDIGO", pdfGen.getFontDoc("table_body_strong")))
                {
                    BackgroundColor = getColor("gray_sky"),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_MIDDLE
                };
            table.AddCell(bodyCellL);
            
            foreach (var option in new[] {"ASIGNATURAS", "TIPO", "H.T.", "H.P."})
            {
                var bodyCellHTmp =
                    new PdfPCell(new Phrase(option, pdfGen.getFontDoc("table_body_strong")))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        BackgroundColor = getColor("gray_sky")
                    };

                table.AddCell(bodyCellHTmp);
            }

            var bodyCellR =
                new PdfPCell(new Phrase("Créditos", pdfGen.getFontDoc("table_body_strong")))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    BackgroundColor = getColor("gray_sky")
                };
            table.AddCell(bodyCellR);

            foreach (var course in data.Courses)
            {
                var bodyCellBTmp =
                    new PdfPCell(new Phrase(course.Code, pdfGen.getFontDoc("table_body")))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                var bodyCellBTmp1 =
                    new PdfPCell(new Phrase(course.Name, pdfGen.getFontDoc("table_body")))
                    {
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                var bodyCellBTmp2 =
                    new PdfPCell(new Phrase(course.Type, pdfGen.getFontDoc("table_body")))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                var bodyCellBTmp3 =
                    new PdfPCell(new Phrase(course.Ht.ToString(), pdfGen.getFontDoc("table_body")))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                var bodyCellBTmp4 =
                    new PdfPCell(new Phrase(course.Hp.ToString(), pdfGen.getFontDoc("table_body")))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                var bodyCellBTmp5 =
                    new PdfPCell(new Phrase(course.Credits.ToString(), pdfGen.getFontDoc("table_body")))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };

                table.AddCell(bodyCellBTmp);
                table.AddCell(bodyCellBTmp1);
                table.AddCell(bodyCellBTmp2);
                table.AddCell(bodyCellBTmp3);
                table.AddCell(bodyCellBTmp4);
                table.AddCell(bodyCellBTmp5);
            }


            //end table
            var content2 = new Paragraph
            {
                new Phrase(
                    "          Se expide la presente a solicitud del interesado para los fines que considere conveniente.",
                    pdfGen.getFontDoc("doc_body"))
            };
            content2.Alignment = Element.ALIGN_JUSTIFIED;
            content2.SetLeading(1.0f, 2.0f);


            var fechaParrafo = new Paragraph(
                $"Expedido el {DateTime.Now:dd} de {DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES"))} de {DateTime.Now:yyyy}",
                pdfGen.getFontDoc("doc_body_bold")
            ) {Alignment = Element.ALIGN_RIGHT};

            var bodyParrafo = new Paragraph {titulo, content, table, content2, fechaParrafo};

            //create document
            var document = PDFEvents.createSimple(data,
                "EL JEFE(E) DE LA OFICINA DE REGISTROS ACADÉMICOS DE LA UNIVERSIDAD CONTINENTAL OTORGA: ");

            document.Add(bodyParrafo);
            try
            {
                document.Close();
                return $"{AppSettings.reports["output-client"]}{data.name_file}/pdf/";
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Objesto de colleccion de datos para generar el documento
        /// </summary>
        public class Data : PDFData
        {
            public string Term { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public string Period { get; set; }
            public List<Course> Courses { get; set; }
        }

        public class Course
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public int Ht { get; set; }
            public int Hp { get; set; }
            public int Credits { get; set; }
        }
    }
}