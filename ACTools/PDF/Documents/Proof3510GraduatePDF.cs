﻿using ACTools.Configuration;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using ACTools.SuperString;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Generar PDF de Constancia de estudios
    /// </summary>
    public class Proof3510GraduatePDF
    {
        /// <summary>
        /// Genera la Constancia y devuelve la ruta
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string generate(Proof3510GraduatePDF.Data data)
        {
            PDFGenerator pdf_gen = new PDFGenerator();

            Paragraph titulo = new Paragraph("Constancia", pdf_gen.getFontDoc("doc_title"));
            titulo.Alignment = Element.ALIGN_CENTER;

            Paragraph content = new Paragraph();
            content.Add(new Phrase("\n"));
            content.Add(new Phrase("         A ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase(string.Format("{0}, ", data.student_names.ToUpper()), pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(" identificado(a) con código de matrícula N° ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase(data.student_id, pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(", de la ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase($"Facultad de {data.college_name.ToUpper()}", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase($", Escuela Académico Profesional de {data.program_name.ToUpper()}", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase("; quien ha cursado el ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase($"{data.last_term_period} periodo", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(" y pertenece al ", pdf_gen.getFontDoc("doc_body")));
            string s3510 = "";
            if (data.prom_3510.Count == 1)
                s3510 = data.prom_3510[0];
            else if (data.prom_3510.Count == 2)
                s3510 = string.Join(" y ", data.prom_3510);
            else if (data.prom_3510.Count == 3)
                s3510 = $"{data.prom_3510[0]}, {data.prom_3510[1]} y {data.prom_3510[2]}";

            content.Add(new Phrase(string.Format("{0} promocional ", string.Join(",", s3510.ToLower())), pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase("de su promoción en el ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase(string.Format("{0}", data.last_term), pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(", con un promedio ponderado de ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase(string.Format("{0},", data.last_term_avg), pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(" de acuerdo a las calificaciones que obran en esta dependencia a los que me remito en caso de ser necesario.", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase("\n"));
            content.Add(new Phrase("          Se expide la presente a solicitud del interesado(a) para los fines que considere conveniente.", pdf_gen.getFontDoc("doc_body")));
            content.Alignment = Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);

            Paragraph fecha_parrafo = new Paragraph(
                string.Format(
                    "Expedido el {0} de {1} de {2}",
                    DateTime.Now.ToString("dd"),
                    DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES")),
                    DateTime.Now.ToString("yyyy")),
                    pdf_gen.getFontDoc("doc_body")
                );
            fecha_parrafo.Alignment = Element.ALIGN_RIGHT;

            Paragraph body_parrafo = new Paragraph();
            body_parrafo.Add(titulo);
            body_parrafo.Add(content);
            body_parrafo.Add(fecha_parrafo);

            //create document
            Document document = PDFEvents.createSimple(
                (PDFData)data,
                "El jefe(e) de la Oficina de Registros Académicos de la\nUniversidad Continental otorga:");

            document.Add(body_parrafo);
            try
            {
                document.Close();
                return string.Format("{0}{1}/pdf/", AppSettings.reports["output-client"], data.name_file);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        /// <summary>
        /// Objesto de colleccion de datos para generar el documento
        /// </summary>
        public class Data : PDFData
        {
            public string last_term { get; set; }
            public string last_term_avg { get; set; }
            public List<string> prom_3510 { get; set; }
            public float score { get; set; }
            public string last_term_period { get; set; }
        }
    }
}
