﻿using ACTools.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Platilla PDF para Boleta de Notas
    /// </summary>
    public class ReportCardAPECPDF
    {
        #region properties
        public ReportCardPDF.Data data { get; set; }
        #endregion properties

        public ReportCardAPECPDF(ReportCardPDF.Data pdf)
        {
            //copiar los datos
            data = pdf;
        }
        public ReportCardAPECPDF()
        {
            //copiar los datos
            data = new ReportCardPDF.Data();
        }

        #region methods
        /// <summary>
        /// Genera la Boleta de Notas
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public string generate()
        {
            iTextSharp.text.Document document = PDFEvents.createSimple(data: data, type_doc: "half_A4", rotate: true);

            iTextSharp.text.Paragraph white_space = new iTextSharp.text.Paragraph();
            white_space.Add(new iTextSharp.text.Phrase("\n"));


            iTextSharp.text.pdf.PdfPTable head = PDFGenerator.Table.create(2);
            head.WidthPercentage = 100;

            iTextSharp.text.pdf.PdfPTable head_l = PDFGenerator.Table.transparent(2); // no borders
            iTextSharp.text.Paragraph head_l_p_1 = new iTextSharp.text.Paragraph();
            head_l_p_1.Add(new iTextSharp.text.Phrase("Facultad: ", PDFGenerator.getFont("times_12_blue_sky")));
            head_l_p_1.Add(new iTextSharp.text.Phrase(data.college_name, PDFGenerator.getFont("times_12")));
            head_l.AddCell(head_l_p_1);

            iTextSharp.text.Paragraph head_l_p_2 = new iTextSharp.text.Paragraph();
            head_l_p_2.Add(new iTextSharp.text.Phrase("E.A.P: ", PDFGenerator.getFont("times_12_blue_sky")));
            head_l_p_2.Add(new iTextSharp.text.Phrase(data.program_name, PDFGenerator.getFont("times_12")));
            head_l.AddCell(head_l_p_2);

            iTextSharp.text.pdf.PdfPCell head_l_cell_1 = PDFGenerator.Cell.transparent(2);

            iTextSharp.text.Paragraph head_l_cell_1_p_1 = new iTextSharp.text.Paragraph();
            head_l_cell_1_p_1.Add(new iTextSharp.text.Phrase("Código: ", PDFGenerator.getFont("times_12_blue_sky")));
            head_l_cell_1_p_1.Add(new iTextSharp.text.Phrase(data.student_id, PDFGenerator.getFont("times_12")));
            head_l_cell_1.AddElement(head_l_cell_1_p_1);

            iTextSharp.text.Paragraph head_l_cell_1_p_2 = new iTextSharp.text.Paragraph();
            head_l_cell_1_p_2.Add(new iTextSharp.text.Phrase("Alumno: ", PDFGenerator.getFont("times_12_blue_sky")));
            head_l_cell_1_p_2.Add(new iTextSharp.text.Phrase(data.student_names, PDFGenerator.getFont("times_12")));
            head_l_cell_1.AddElement(head_l_cell_1_p_2);

            head_l.AddCell(head_l_cell_1);

            iTextSharp.text.pdf.PdfPTable head_r = PDFGenerator.Table.outline(2, false);
            head_r.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            head_r.DefaultCell.Padding = 5f;

            iTextSharp.text.pdf.PdfPCell head_r_cell_1 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Paragraph("BOLETA DE NOTAS", PDFGenerator.getFont("times_14_blue_sky")));
            head_r_cell_1.BorderWidthTop = 0f;
            head_r_cell_1.BorderWidthLeft = 0f;
            head_r_cell_1.BorderWidthBottom = 0.5f;
            head_r_cell_1.BorderWidthRight = 0.5f;
            head_r_cell_1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            head_r_cell_1.Padding = 5f;
            iTextSharp.text.pdf.PdfPCell head_r_cell_2 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Paragraph(data.term, PDFGenerator.getFont("times_14")));
            head_r_cell_2.BorderWidthBottom = 0.5f;
            head_r_cell_2.BorderWidthTop = 0f;
            head_r_cell_2.BorderWidthLeft = 0f;
            head_r_cell_2.BorderWidthRight = 0f;
            head_r_cell_2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            head_r_cell_2.Padding = 5f;
            iTextSharp.text.pdf.PdfPCell head_r_cell_3 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Paragraph("CL: Control de Lectura \n TA: Tarea Académica", PDFGenerator.getFont("times_14_blue_sky")));
            head_r_cell_3.BorderWidthRight = 0.5f;
            head_r_cell_3.BorderWidthTop = 0f;
            head_r_cell_3.BorderWidthLeft = 0f;
            head_r_cell_3.BorderWidthBottom = 0f;
            head_r_cell_3.Padding = 5f;
            head_r_cell_3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;


            head_r.AddCell(head_r_cell_1);
            head_r.AddCell(head_r_cell_2);
            head_r.AddCell(head_r_cell_3);
            head_r.AddCell(new iTextSharp.text.Paragraph("CL Nota mínima: 00 \n Nota aprobatoria: 11", PDFGenerator.getFont("times_14_blue_sky")));

            head.AddCell(head_l);
            head.AddCell(head_r);



            //document.Add(new iTextSharp.text.Paragraph("\n"));

            // Iniciamos con el cuerpo

            iTextSharp.text.pdf.PdfPTable body = PDFGenerator.Table.create(11);
            body.WidthPercentage = 100;
            body.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            body.DefaultCell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            body.DefaultCell.Padding = 5f;
            body.DefaultCell.BorderWidthBottom = 0.5f;
            body.DefaultCell.BorderWidthLeft = iTextSharp.text.Rectangle.NO_BORDER;
            body.DefaultCell.BorderWidthRight = iTextSharp.text.Rectangle.NO_BORDER;
            body.DefaultCell.BorderWidthTop = iTextSharp.text.Rectangle.NO_BORDER;
            body.DefaultCell.BorderWidthBottom = iTextSharp.text.Rectangle.NO_BORDER;

            float[] widths = new float[] { 6f, 3f, 5f, 15f, 2f, 2f, 2f, 2f, 2f, 2f, 3f };
            body.SetWidths(widths);

            iTextSharp.text.pdf.PdfPCell body_cell_l = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Cod. Asig.", PDFGenerator.getFont("times_12_blue_sky")));
            body_cell_l.Border = iTextSharp.text.Rectangle.NO_BORDER;
            body_cell_l.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            body_cell_l.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            body_cell_l.BorderWidthLeft = 0.5f;
            body_cell_l.BorderWidthBottom = 0.5f;
            body_cell_l.BorderWidthTop = 0.5f;
            body.AddCell(body_cell_l);
            foreach (string option in new string[] { "Créditos", "Sección", "Asignatura", "CL1", "TA1", "EP", "CL2", "TA2", "EF" })
            {
                //iTextSharp.text.pdf.PdfPCell body_cell_h_tmp = Helpers.Reports.PDF.Cell.centered();
                //body_cell_h_tmp.AddElement(new iTextSharp.text.Phrase(option, PDFGenerator.getFont("times_12_blue_sky")));
                iTextSharp.text.pdf.PdfPCell body_cell_h_tmp = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(option, PDFGenerator.getFont("times_12_blue_sky")));
                body_cell_h_tmp.Border = iTextSharp.text.Rectangle.NO_BORDER;
                if (option == "Asignatura")
                    body_cell_h_tmp.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                else body_cell_h_tmp.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                body_cell_h_tmp.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;

                //body_cell_h_tmp.Padding = 5f;
                body_cell_h_tmp.BorderWidthTop = 0.5f;
                body_cell_h_tmp.BorderWidthBottom = 0.5f;
                body.AddCell(body_cell_h_tmp);
            }

            iTextSharp.text.pdf.PdfPCell body_cell_r = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("PROM.", PDFGenerator.getFont("times_12_blue_sky")));
            body_cell_r.Border = iTextSharp.text.Rectangle.NO_BORDER;
            body_cell_r.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            body_cell_r.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            body_cell_r.BorderWidthRight = 0.5f;
            body_cell_r.BorderWidthBottom = 0.5f;
            body_cell_r.BorderWidthTop = 0.5f;
            body.AddCell(body_cell_r);

            ///* Aqui inician los datos */

            foreach (ReportCardPDF.Score score in data.scores)
            {
                iTextSharp.text.pdf.PdfPCell body_cell_l_tmp = PDFGenerator.Cell.centered();
                body_cell_l_tmp.AddElement(new iTextSharp.text.Phrase(score.course_id, PDFGenerator.getFont("times_12")));
                body_cell_l_tmp.BorderWidthLeft = 0.5f;
                body.AddCell(body_cell_l_tmp);

                body.AddCell(new iTextSharp.text.Phrase(score.credits.ToString(), PDFGenerator.getFont("times_12")));
                body.AddCell(new iTextSharp.text.Phrase(score.section, PDFGenerator.getFont("times_12")));
                //body.AddCell(new iTextSharp.text.Phrase(score.course_name, PDFGenerator.getFont("times_12")));
                iTextSharp.text.pdf.PdfPCell course_cell = new iTextSharp.text.pdf.PdfPCell();
                course_cell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                iTextSharp.text.Paragraph course_paragrap = new iTextSharp.text.Paragraph();
                course_paragrap.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                course_paragrap.Add(new iTextSharp.text.Phrase(score.course_name, PDFGenerator.getFont("times_12")));
                course_cell.AddElement(course_paragrap);
                body.AddCell(course_cell);

                body.AddCell(new iTextSharp.text.Phrase(score.cl1.ToString(), PDFGenerator.getFont("times_12")));
                body.AddCell(new iTextSharp.text.Phrase(score.ta1.ToString(), PDFGenerator.getFont("times_12")));
                body.AddCell(new iTextSharp.text.Phrase(score.ep.ToString(), PDFGenerator.getFont("times_12")));
                body.AddCell(new iTextSharp.text.Phrase(score.cl2.ToString(), PDFGenerator.getFont("times_12")));
                body.AddCell(new iTextSharp.text.Phrase(score.ta2.ToString(), PDFGenerator.getFont("times_12")));
                body.AddCell(new iTextSharp.text.Phrase(score.ef.ToString(), PDFGenerator.getFont("times_12")));

                iTextSharp.text.pdf.PdfPCell body_cell_r_tmp = PDFGenerator.Cell.centered();
                body_cell_r_tmp.AddElement(new iTextSharp.text.Phrase(score.average.ToString(), PDFGenerator.getFont("times_12")));
                body_cell_r_tmp.BorderWidthRight = 0.5f;
                body.AddCell(body_cell_r_tmp);
            }


            ///* END Aqui inician los datos */

            iTextSharp.text.pdf.PdfPTable footer = PDFGenerator.Table.transparent(3);
            footer.WidthPercentage = 100;
            footer.DefaultCell.BorderWidthTop = 0.5f;
            footer.SetWidths(new float[] { 10f, 5f, 5f });

            footer.AddCell(new iTextSharp.text.Phrase("* OFICINA DE REGISTROS ACADEMICOS", PDFGenerator.getFont("times_12_blue_sky")));

            iTextSharp.text.pdf.PdfPCell footer_cell_r_1 = new iTextSharp.text.pdf.PdfPCell();
            footer_cell_r_1.AddElement(new iTextSharp.text.Phrase("Promedio Ponderado", PDFGenerator.getFont("times_12")));
            footer.AddCell(footer_cell_r_1);

            iTextSharp.text.pdf.PdfPCell footer_cell_r_2 = new iTextSharp.text.pdf.PdfPCell();
            iTextSharp.text.Paragraph footer_ponderado = new iTextSharp.text.Paragraph();
            footer_ponderado.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            footer_cell_r_2.BorderWidthLeft = 0f;
            footer_cell_r_2.PaddingRight = 10f;

            if (data.scores.Count > 0)
            {
                footer_ponderado.Add(new iTextSharp.text.Phrase(data.final_average().ToString("0.##"), PDFGenerator.getFont("times_12")));
                footer_cell_r_2.AddElement(footer_ponderado);
            }
            else
            {
                footer_ponderado.Add(new iTextSharp.text.Phrase("0.0", PDFGenerator.getFont("times_12")));
                footer_cell_r_2.AddElement(footer_ponderado);
            }
            footer.AddCell(footer_cell_r_2);

            document.Add(white_space);

            document.Add(head);
            document.Add(body);
            document.Add(footer);



            try
            {
                document.Close();

            }
            catch
            {
                //Debug.Print("salto error" + ex.Message);
                return null;
            }
            return string.Format("{0}{1}/pdf/", AppSettings.reports["output-client"], data.name_file);

        }

        #endregion methods 
    }
}
