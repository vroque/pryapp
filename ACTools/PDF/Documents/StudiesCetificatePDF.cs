﻿using System;
using System.Collections.Generic;
using System.Linq;
using iTextSharp.text;
using ACTools.Configuration;
using ACTools.SuperString;
using iTextSharp.text.pdf;

namespace ACTools.PDF.Documents
{
    public class StudiesCetificatePDF
    {
        #region methods
        /// <summary>
        /// Genera la Constancia y devuelve la ruta
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string generate(StudiesCetificatePDF.Data data)
        {
            PDFGenerator pdf_gen = new PDFGenerator();

            int n_creditos_o = 0;
            int n_cussos_o = 0;
            int n_creditos_e = 0;
            int n_cussos_e = 0;
            int n_cursos_apro = 0;
            int n_cursos_desa = 0;
            int n_creditos_apro = 0;
            int n_creditos_desa = 0;

            Document document = PDFEvents.createWithPhoto(data, header_pages: "CERTIFICADO DE ESTUDIOS");
            //subtitle
            Paragraph titulo = new Paragraph("Certificado de estudios", pdf_gen.getFontDoc("doc_title"));
            titulo.Alignment = Element.ALIGN_CENTER;

            Paragraph subtitulo = new Paragraph();
            subtitulo.Add(new Phrase("\nFacultad de ", pdf_gen.getFontDoc("doc_body")));
            subtitulo.Add(new Phrase(data.college_name.ToUpper(), pdf_gen.getFontDoc("doc_body_bold")));
            subtitulo.Add(new Phrase("\nE.A.P. de ", pdf_gen.getFontDoc("doc_body")));
            subtitulo.Add(new Phrase(data.program_name.ToUpper(), pdf_gen.getFontDoc("doc_body_bold")));
            subtitulo.Alignment = Element.ALIGN_CENTER;
            //text
            Paragraph prefacio = new Paragraph();
            prefacio.Add(new Phrase("\nLa autoridad universitaria que suscribe, certifica que ", pdf_gen.getFontDoc("doc_body")));
            prefacio.Add(new Phrase(data.student_names, pdf_gen.getFontDoc("doc_body_bold")));
            prefacio.Add(new Phrase(", con código ", pdf_gen.getFontDoc("doc_body")));
            prefacio.Add(new Phrase(data.student_id, pdf_gen.getFontDoc("doc_body_bold")));
            prefacio.Add(new Phrase(", ha cursado las asignaturas que se indican a continuación con los siguientes resultados:", pdf_gen.getFontDoc("doc_body")));
            prefacio.Add((new Paragraph("\n")));
            prefacio.Add((new Paragraph("\n")));

            document.Add(titulo);
            document.Add(subtitulo);
            document.Add(prefacio);

            // add Table
            PdfPTable content_table = new PdfPTable(6);
            content_table.WidthPercentage = 100;
            //content_table.TotalWidth = 500f;
            content_table.HorizontalAlignment = 0;
            // content_table.SpacingAfter = 10;
            // content_table.SetWidths(new float[] { 55f, 220, 65, 100f, 65, 65, 65, 65, 70f });
            content_table.SetWidths(new float[] { 55f, 220, 100f, 60, 60, 65f });

            // foreach (string option in new string[] { "CÓDIGO", "ASIGNATURA", "UNIVERSIDAD", "PAÍS", "RESOLUCIÓN", "OBSERVACIÓN", "CALIFICATIVO", "CRÉDITO", "FECHA" })
            foreach (string option in new string[] { "Código", "Asignatura", "Observación", "Calificativo", "Crédito", "Fecha" })
            {
                PdfPCell _cell = PDFGenerator.Cell.centered();
                Paragraph _parrafo = new Paragraph(option, pdf_gen.getFontDoc("tbl_header"));
                _parrafo.Alignment = Element.ALIGN_CENTER;
                _cell.AddElement(_parrafo);
                _cell.Border = Rectangle.BOX;
                _cell.PaddingTop = 2f;
                _cell.PaddingBottom = 7f;
                _cell.BorderColor = PDFGenerator.getColor("black");
                content_table.AddCell(_cell);
            }
            int[] cycles = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
            foreach (int option in cycles)
            {
                List<StudiesCetificatePDF.CourseDetail> obj_tmp = data.courses.Where(f => f.cycle == option).ToList();
                if (obj_tmp.Count > 0)
                {
                    PdfPCell celda_ciclo = new PdfPCell();
                    celda_ciclo.PaddingLeft = 50f;
                    celda_ciclo.Colspan = 9;
                    celda_ciclo.BorderWidthBottom = 0.5f;
                    celda_ciclo.BorderWidthTop = 0;
                    celda_ciclo.BorderWidthLeft = 0.5f;
                    celda_ciclo.BorderWidthRight = 0.5f;
                    celda_ciclo.BorderColorBottom = PDFGenerator.getColor("black");
                    Paragraph text_ciclo = new Paragraph();
                    text_ciclo.Add(new Phrase(
                        string.Format("{0} periodo", obj_tmp.First().cycle.numberToOrdinal().toCapitalize())
                        , pdf_gen.getFontDoc("tbl_title"))); //convertir a texto
                    /*celda_ciclo.HorizontalAlignment = Element.ALIGN_CENTER;
                    celda_ciclo.VerticalAlignment = Element.ALIGN_MIDDLE;
                    text_ciclo.Alignment = Element.ALIGN_CENTER;
                    celda_ciclo.BackgroundColor = PDFGenerator.getColor("gray_sky");*/
                    celda_ciclo.AddElement(text_ciclo);
                    content_table.AddCell(celda_ciclo);

                    // CURSOS DEL CICLO
                    foreach (StudiesCetificatePDF.CourseDetail obj in obj_tmp)
                    {
                        if (!obj.is_aproved) // ignorar desaprobados
                        {
                            int aproved_course = data.courses
                                .Where(f => f.id == obj.id && f.is_aproved)
                                .Count();
                            if (aproved_course == 0)
                            {
                                n_cursos_desa += 1;
                                n_creditos_desa += obj.credits;
                            }
                            continue;
                        }
                        else
                        {
                            n_cursos_apro += 1;
                            n_creditos_apro += obj.credits;
                        }
                        //FIX N CREDITOS Y CURSOS
                        if (obj.type == "E")
                        {
                            n_cussos_e++;
                            n_creditos_e += obj.credits;
                        }
                        else
                        {
                            n_cussos_o++;
                            n_creditos_o += obj.credits;
                        }
                        PdfPCell body_cell_l_tmp = PDFGenerator.Cell.centered();
                        body_cell_l_tmp.BorderWidthBottom = 0.5f;
                        body_cell_l_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                        //Paragraph body_cell_l_tmp_parr = new Paragraph()
                        body_cell_l_tmp.AddElement(new Phrase(obj.id, pdf_gen.getFontDoc("tbl_header")));
                        body_cell_l_tmp.PaddingLeft = 5f;
                        body_cell_l_tmp.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        //body_cell_l_tmp.VerticalAlignment = Element.ALIGN_MIDDLE;
                        body_cell_l_tmp.BorderWidthLeft = 0.5f;
                        body_cell_l_tmp.PaddingRight = 0f;
                        content_table.AddCell(body_cell_l_tmp);


                        PdfPCell body_cell_c_1_tmp = PDFGenerator.Cell.centered();
                        body_cell_c_1_tmp.AddElement(new Phrase(obj.name, pdf_gen.getFontDoc("tbl_content")));
                        body_cell_c_1_tmp.BorderWidthBottom = 0.5f;
                        body_cell_c_1_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                        body_cell_c_1_tmp.PaddingLeft = 5f;
                        body_cell_c_1_tmp.PaddingRight = 0f;
                        content_table.AddCell(body_cell_c_1_tmp);

                        PdfPCell body_cell_c_1_5_tmp = PDFGenerator.Cell.centered();
                        body_cell_c_1_5_tmp.AddElement(new Phrase(obj.observation, pdf_gen.getFontDoc("tbl_content")));
                        body_cell_c_1_5_tmp.BorderWidthBottom = 0.5f;
                        body_cell_c_1_5_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                        body_cell_c_1_5_tmp.PaddingLeft = 5f;
                        body_cell_c_1_5_tmp.PaddingRight = 0f;
                        content_table.AddCell(body_cell_c_1_5_tmp);

                        PdfPCell body_cell_c_2_tmp = PDFGenerator.Cell.centered();
                        body_cell_c_2_tmp.AddElement(new Phrase( obj.score.numberToLetters(), pdf_gen.getFontDoc("tbl_content")));//convertir
                        body_cell_c_2_tmp.BorderWidthBottom = 0.5f;
                        body_cell_c_2_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                        body_cell_c_2_tmp.PaddingLeft = 5f;
                        body_cell_c_2_tmp.PaddingRight = 0f;
                        content_table.AddCell(body_cell_c_2_tmp);

                        PdfPCell body_cell_c_3_tmp = PDFGenerator.Cell.centered();
                        body_cell_c_3_tmp.AddElement(new Phrase(obj.credits.ToString(), pdf_gen.getFontDoc("tbl_content")));
                        body_cell_c_3_tmp.BorderWidthBottom = 0.5f;
                        body_cell_c_3_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                        body_cell_c_3_tmp.PaddingLeft = 5f;
                        body_cell_c_3_tmp.PaddingRight = 0f;
                        body_cell_c_3_tmp.HorizontalAlignment = Element.ALIGN_CENTER;
                        body_cell_c_3_tmp.VerticalAlignment = Element.ALIGN_MIDDLE;
                        content_table.AddCell(body_cell_c_3_tmp);

                        PdfPCell body_cell_r_tmp = PDFGenerator.Cell.centered();
                        body_cell_r_tmp.BorderWidthBottom = 0.5f;
                        body_cell_r_tmp.BorderColorBottom = PDFGenerator.getColor("black");
                        body_cell_r_tmp.HorizontalAlignment = Element.ALIGN_CENTER;
                        body_cell_r_tmp.VerticalAlignment = Element.ALIGN_MIDDLE;
                        body_cell_r_tmp.AddElement(new Phrase(string.Format("{0:dd/MM/yyyy}", obj.date), pdf_gen.getFontDoc("tbl_content")));
                        body_cell_r_tmp.BorderWidthRight = 0.5f;
                        body_cell_r_tmp.PaddingLeft = 10f;
                        content_table.AddCell(body_cell_r_tmp);
                    }
                }
            }


            //Linea 
            PdfPCell celda_end = new PdfPCell();
            celda_end.PaddingLeft = 0f;
            celda_end.Colspan = 6;
            celda_end.BorderWidthBottom = 0f;
            celda_end.BorderWidthTop = 0;
            celda_end.BorderWidthLeft = 0.5f;
            celda_end.BorderWidthRight = 0.5f;
            celda_end.BorderColorBottom = PDFGenerator.getColor("black");
            //celda_end.AddElement(new Phrase("__________________________________________________________________________________________________________", pdf_gen.getFontDoc("tbl_footer_bold")));
            content_table.AddCell(celda_end);


            //resumen
            PdfPCell celda_summary_left = new PdfPCell();

            celda_summary_left.Colspan = 2;
            celda_summary_left.BorderWidthBottom = 0.5f;
            celda_summary_left.BorderWidthTop = 0f;
            celda_summary_left.BorderWidthLeft = 0.5f;
            celda_summary_left.BorderWidthRight = 0f;
            celda_summary_left.PaddingTop = 0f;
            celda_summary_left.PaddingBottom = 10f;

            PdfPCell celda_summary_right = new PdfPCell();

            celda_summary_right.Colspan = 4;
            celda_summary_right.BorderWidthBottom = 0.5f;
            celda_summary_right.BorderWidthTop = 0f;
            celda_summary_right.BorderWidthLeft = 0f;
            celda_summary_right.BorderWidthRight = 0.5f;
            celda_summary_right.PaddingTop = 0f;
            celda_summary_right.PaddingBottom = 10f;
            celda_summary_right.PaddingRight = 40f;

            Paragraph body_cell_summary_tmp_1_p = new Paragraph();
            body_cell_summary_tmp_1_p.Alignment = Element.ALIGN_RIGHT;
            body_cell_summary_tmp_1_p.Add(new Phrase(string.Format("Asignaturas obligatorias aprobadas: {0}\nAsignaturas electivas aprobadas: {1}", n_cussos_o, n_cussos_e), pdf_gen.getFontDoc("tbl_footer")));
            //body_cell_summary_tmp_1_p.Add(new Phrase(string.Format("\nTotal Asignaturas Aprobadas: {0}\nTotal Asignaturas Desaprobadas: {1}", n_cursos_apro, n_cursos_desa), pdf_gen.getFontDoc("verdana_bold_6")));
            celda_summary_left.AddElement(body_cell_summary_tmp_1_p);



            Paragraph body_cell_summary_tmp_2_p = new Paragraph();
            body_cell_summary_tmp_2_p.Alignment = Element.ALIGN_RIGHT;
            body_cell_summary_tmp_2_p.Add(new Phrase(string.Format("Créditos obligatorios aprobados: {0}\nCréditos electivos aprobados: {1}", n_creditos_o, n_creditos_e), pdf_gen.getFontDoc("tbl_footer")));
            //body_cell_summary_tmp_2_p.Add(new Phrase(string.Format("\nTotal Créditos Aprobados: {0}\nTotal Créditos Desaprobados: {1}", n_creditos_apro, n_creditos_desa), pdf_gen.getFontDoc("verdana_bold_6")));
            celda_summary_right.AddElement(body_cell_summary_tmp_2_p);


            content_table.AddCell(celda_summary_left);
            content_table.AddCell(celda_summary_right);

            document.Add(content_table);
            try
            {
                document.Close();
                return string.Format("{0}{1}/pdf/", AppSettings.reports["output-client"], data.name_file);
            }
            catch
            {
                return null;
            }
        }
        public class Data : PDFData
        {
            public Data()
            {
                this.courses = new List<CourseDetail>();
            }
            public List<CourseDetail> courses { get; set; }
        }
        public class CourseDetail
        {
            public int cycle { get; set; }
            public string type { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public string university { get; set; }
            public string country { get; set; }
            public string resolution { get; set; }
            public string observation { get; set; }
            public int score { get; set; }
            public int credits { get; set; }
            public DateTime? date { get; set; }
            public bool is_aproved{ get; set; }

        }

        #endregion methods
    }
}
