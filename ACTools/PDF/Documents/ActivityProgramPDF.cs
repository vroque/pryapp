﻿using ACTools.Configuration;
using iTextSharp.text;
using System;
using System.IO;
using _vuc = ACTools.Constants.ExtracurricularActivitiesConstants;

namespace ACTools.PDF.Documents
{
    public class ActivityProgramPDF
    {
        /// <summary>
        /// Genera la Constancia y devuelve la ruta
        /// </summary>
        /// <param name="data">Información a insertar en el pDF</param>
        /// <param name="activityType">tipo de actividad</param>
        /// <returns>url del documento PDF</returns>
        public static string Generate(Data data, int activityType)
        {
            PDFGenerator pdf = new PDFGenerator();

            Paragraph title = new Paragraph("Certifica", pdf.getFontDoc("doc_title"))
            {
                Alignment = Element.ALIGN_CENTER
            };

            Paragraph content = new Paragraph
            {
                new Phrase("\n"),
                new Phrase("         A ", pdf.getFontDoc("doc_body")),
                new Phrase(data.student_names.ToUpper(), pdf.getFontDoc("doc_body_bold")),
                new Phrase(", identificado(a) con código de matrícula N° ", pdf.getFontDoc("doc_body")),
                new Phrase(data.student_id, pdf.getFontDoc("doc_body_bold")),
                new Phrase(", estudiante de la ", pdf.getFontDoc("doc_body")),
                new Phrase($"Facultad de {data.college_name.ToUpper()}", pdf.getFontDoc("doc_body_bold")),
                new Phrase($", Escuela Académico Profesional de {data.program_name.ToUpper()};", 
                            pdf.getFontDoc("doc_body_bold")),
                new Phrase(" por haber culminado satisfactoriamente el", pdf.getFontDoc("doc_body"))
            };

            if (activityType == 1)
            {
            content.Add(new Phrase(" programa", pdf.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(" denominado", pdf.getFontDoc("doc_body")));
            content.Add(new Phrase($" {data.NameActivity},", pdf.getFontDoc("doc_body_bold")));
            }
            else
            {
                content.Add(new Phrase(" taller", pdf.getFontDoc("doc_body_bold")));
                content.Add(new Phrase(" denominado", pdf.getFontDoc("doc_body")));
                content.Add(new Phrase($" {data.NameActivity},", pdf.getFontDoc("doc_body_bold")));
                content.Add(new Phrase(" logrando obtener ", pdf.getFontDoc("doc_body")));
                content.Add(new Phrase($" 0{data.Credit}", pdf.getFontDoc("doc_body_bold")));
                content.Add(new Phrase($" crédito{(data.Credit>1? "s": string.Empty)} extracurricular{(data.Credit>1 ? "es": string.Empty)} ", pdf.getFontDoc("doc_body_bold")));
            }

            content.Add(new Phrase(" en el período", pdf.getFontDoc("doc_body")));
            content.Add(new Phrase($" {data.Term}", pdf.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(".", pdf.getFontDoc("doc_body")));

            // para constancia
            content.Add(new Phrase("\n"));
            content.Add(new Phrase("          Se extiende la presente a solicitud del interesado(a) para los fines que considere conveniente.", pdf.getFontDoc("doc_body")));
            content.Alignment = Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);

            Paragraph paragraphDate = new Paragraph(string.Format("Expedido el {0} de {1} de {2}", DateTime.Now.ToString("dd"), DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES")), DateTime.Now.ToString("yyyy")), pdf.getFontDoc("doc_body"));
            paragraphDate.Alignment = Element.ALIGN_RIGHT;

            Paragraph paragraphBody = new Paragraph();
            paragraphBody.Add(title);
            paragraphBody.Add(content);
            paragraphBody.Add(paragraphDate);

            var path = AppSettings.reports[_vuc.SRC_VUC_PDF]; 
            // Verificar si existe la carpeta
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // create document
            Document document = PDFEvents.ExtracurricularActivityCreate(_vuc.SRC_VUC_PDF, data, "El director de la Oficina de Vida Universitaria Continental:");
            document.Add(paragraphBody);
            try
            {
                document.Close();
                return $"{AppSettings.reports[_vuc.URI_VUC_PDF]}{data.name_file}/";
            }
            catch
            {
                return null;
            }
        }

        public class Data : PDFData
        {
            public Data(PDFData pdf)
            {
                name_file = pdf.name_file;
                signs = pdf.signs;
                student_id = pdf.student_id;
                student_names = pdf.student_names;
                student_gender = pdf.student_gender;
                college_name = pdf.college_name;
                program_name = pdf.program_name;
                request_id = pdf.request_id;
                document_number = pdf.document_number;
                user = pdf.user;
            }
            public string NameActivity { get; set; }
            public byte Credit { get; set; }
            public string Term { get; set; }
            public int ActivityType { get; set; }
        }
    }
}
