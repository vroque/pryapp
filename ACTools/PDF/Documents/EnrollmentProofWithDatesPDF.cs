﻿using ACTools.Configuration;
using iTextSharp.text;
using System;
using ACTools.SuperString;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Genera PDF para Constancia de Matricula
    /// </summary>
    public class EnrollmentProofWithDatesPDF
    {
        /// <summary>
        /// Genera la Constancia y devuelve la ruta
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string generate(EnrollmentProofWithDatesPDF.Data data)
        {
            PDFGenerator pdf_gen = new PDFGenerator();


            Paragraph titulo = new Paragraph("Constancia de matrícula", pdf_gen.getFontDoc("doc_title"));
            titulo.Alignment = Element.ALIGN_CENTER;


            Paragraph content = new Paragraph();
            content.Add(new Phrase("\n"));
            content.Add(new Phrase("         A ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase($"{data.student_names.ToUpper()}, ", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(" identificado(a) con código de matrícula N° ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase(data.student_id, pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(", estudiante de la ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase($"Facultad de {data.college_name.ToUpper()}", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase($", Escuela Académico Profesional de {data.program_name.ToUpper()};", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(" quien está matriculado(a) en el ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase($"{data.period} periodo", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(" en el ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase(data.term, pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase($", fecha de inicio de periodo el {data.start_date:dd/MM/yyyy} y finaliza el {data.end_date:dd/MM/yyyy} tal como consta en los archivos que obran en esta dependencia a los que me remito en caso de ser necesario.", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase("\n"));
            content.Add(new Phrase("          Se expide la presente a solicitud del interesado para los fines que considere conveniente.", pdf_gen.getFontDoc("doc_body")));
            content.Alignment = Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);


            Paragraph fecha_parrafo = new Paragraph(
                string.Format(
                    "Expedido el {0} de {1} de {2}",
                    DateTime.Now.ToString("dd"),
                    DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES")),
                    DateTime.Now.ToString("yyyy")),
                    pdf_gen.getFontDoc("doc_body")
                );
            fecha_parrafo.Alignment = Element.ALIGN_RIGHT;


            Paragraph body_parrafo = new Paragraph();
            body_parrafo.Add(titulo);
            body_parrafo.Add(content);
            body_parrafo.Add(fecha_parrafo);

            //create document
            Document document = PDFEvents.createSimple(
                (PDFData)data,
                "El jefe(e) de la Oficina de Registros Académicos de la \nUniversidad Continental otorga:");

            document.Add(body_parrafo);
            try
            {
                document.Close();
                return string.Format("{0}{1}/pdf/", AppSettings.reports["output-client"], data.name_file);
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// Objesto de colleccion de datos para generar el documento
        /// </summary>
        public class Data : PDFData
        {
            public string term { get; set; }
            public DateTime start_date { get; set; }
            public DateTime end_date { get; set; }
            public string period { get; set; }
        }
    }
}
