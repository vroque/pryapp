﻿using ACTools.Configuration;
using ACTools.SuperString;
using iTextSharp.text;
using System;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Generar PDF de Constancia de estudios
    /// </summary>
    public class StudiesProofPDF
    {
        /// <summary>
        /// Genera la Constancia y devuelve la ruta
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string generate(StudiesProofPDF.Data data)
        {
            PDFGenerator pdf_gen = new PDFGenerator();

            Paragraph titulo = new Paragraph("Constancia de estudios", pdf_gen.getFontDoc("doc_title"));
            titulo.Alignment = Element.ALIGN_CENTER;
            
            Paragraph content = new Paragraph();
            content.Add(new Phrase("\n"));
            content.Add(new Phrase("         A ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase(data.student_names.ToUpper(), pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(", identificado(a) con código de matrícula N° ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase(data.student_id, pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(", estudiante de la ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase($"Facultad de {data.college_name.ToUpper()}", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase($", Escuela Académico Profesional de {data.program_name.ToUpper()};", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(" quien está matriculado(a) y cursa el ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase($"{data.last_term_period} periodo", pdf_gen.getFontDoc("doc_body_bold")));
            content.Add(new Phrase(" en el ", pdf_gen.getFontDoc("doc_body")));
            content.Add(new Phrase(data.last_term, pdf_gen.getFontDoc("doc_body_bold")));

            content.Add(new Phrase(", tal como consta en los archivos que obran en esta dependencia a los que me remito en caso de ser necesario.", 
                                                    pdf_gen.getFontDoc("doc_body")));

            content.Add(new Phrase("\n"));
            content.Add(new Phrase("          Se expide la presente a solicitud del interesado(a) para los fines que considere conveniente.", pdf_gen.getFontDoc("doc_body")));
            content.Alignment = Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);
            
            Paragraph fecha_parrafo = new Paragraph(
                string.Format(
                    "Expedido el {0} de {1} de {2}",
                    DateTime.Now.ToString("dd"),
                    DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES")),
                    DateTime.Now.ToString("yyyy")),
                    pdf_gen.getFontDoc("doc_body")
                );
            fecha_parrafo.Alignment = Element.ALIGN_RIGHT;
            
           
            Paragraph expiration_text= new Paragraph();
            expiration_text.Add(new Phrase("\n"));
            expiration_text.Add(new Phrase("Este documento tiene vigencia de tres meses desde la fecha de emisión.", pdf_gen.getFontDoc("doc_body")));
            expiration_text.Alignment = Element.ALIGN_JUSTIFIED;
            expiration_text.SetLeading(1.0f, 2.0f);         
            
            Paragraph body_parrafo = new Paragraph();
            body_parrafo.Add(titulo);
            body_parrafo.Add(content);
            body_parrafo.Add(fecha_parrafo);
            body_parrafo.Add(expiration_text);


            // create document 
            Document document = PDFEvents.createSimple(
                (PDFData)data,
                "El jefe(e) de la Oficina de Registros Académicos de la \nUniversidad Continental otorga:");

            document.Add(body_parrafo);
            try
            {
                document.Close();
                return string.Format("{0}{1}/pdf/", AppSettings.reports["output-client"], data.name_file);
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// Objesto de colleccion de datos para generar el documento
        /// </summary>
        public class Data : PDFData
        {
            public Data(PDFData to_copy)
            {
                this.name_file = to_copy.name_file;
                this.signs = to_copy.signs;
                this.student_id = to_copy.student_id;
                this.student_names = to_copy.student_names;
                this.student_gender = to_copy.student_gender;
                this.college_name = to_copy.college_name;
                this.program_name = to_copy.program_name;
                this.request_id = to_copy.request_id;
                this.document_number = to_copy.document_number;
                this.user = to_copy.user;
            }
            public string last_term { get; set; }
            public string last_term_period { get; set; }
        }
    }
}
