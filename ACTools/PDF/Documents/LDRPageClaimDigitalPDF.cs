﻿using ACTools.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using static ACTools.PDF.PDFGenerator;

namespace ACTools.PDF.Documents
{
    public class LDRPageClaimDigitalPDF
    {
        public static string generate(LDRPageClaimDigitalPDF.Data data, string source)
        {
            PDFGenerator pdf_gen = new PDFGenerator();
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            table.SetWidths(new float[] { 35, 220 });

            //PdfPCell cell;
            PdfPCell cell = new PdfPCell(new Paragraph());
            iTextSharp.text.Paragraph _texto;

            _texto = new iTextSharp.text.Paragraph($"Libro de Reclamaciones {data.numeration} - {data.year}", FontFactory.GetFont("times_16", 10, Font.BOLD, getColor("white")));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            //cell = new PdfPCell(new Paragraph($"Libro de Reclamaciones {data.numeration} - {data.year}"));
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = getColor("black");
            cell.BorderColor = getColor("white");
            table.AddCell(cell);

            _texto = new iTextSharp.text.Paragraph(data.date_creation.ToString(), FontFactory.GetFont("times_16", 10, Font.BOLD, getColor("white")));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            //cell = new PdfPCell(new Paragraph(data.date_creation.ToString()));
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = getColor("black");
            cell.BorderColor = getColor("white");
            table.AddCell(cell);
            
            _texto = new iTextSharp.text.Paragraph("Cliente", FontFactory.GetFont("times_16", 10, Font.BOLD, getColor("white")));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            //cell = new PdfPCell(new Paragraph("Cliente"));
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = getColor("black");
            cell.BorderColor = getColor("white");
            table.AddCell(cell);




            _texto = new iTextSharp.text.Paragraph("Nombre", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);

            _texto = new iTextSharp.text.Paragraph($": {data.names}", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);


            _texto = new iTextSharp.text.Paragraph($"Documento de Identidad", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);

            _texto = new iTextSharp.text.Paragraph($": {data.identification}", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;  
            table.AddCell(cell);


            _texto = new iTextSharp.text.Paragraph($"E-mail", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);

            _texto = new iTextSharp.text.Paragraph($": {data.email}", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);


            _texto = new iTextSharp.text.Paragraph($"Teléfono", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);

            _texto = new iTextSharp.text.Paragraph($": {data.phone}", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);


            _texto = new iTextSharp.text.Paragraph($"Domicilio Apoderado", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);

            _texto = new iTextSharp.text.Paragraph($": {data.home}", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);

            /*table.AddCell("Nombre");
            table.AddCell(data.names);

            table.AddCell("Documento de Identidad");
            table.AddCell(data.identification);

            table.AddCell("E-mail");
            table.AddCell(data.email);

            table.AddCell("Teléfono");
            table.AddCell(data.phone);

            table.AddCell("Domicilio Apoderado");
            table.AddCell(data.home);*/
            
            _texto = new iTextSharp.text.Paragraph("Servicio", FontFactory.GetFont("times_16", 10, Font.BOLD, getColor("white")));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            //cell = new PdfPCell(new Paragraph("Servicio"));
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = getColor("black");
            cell.BorderColor = getColor("white");
            table.AddCell(cell);



            _texto = new iTextSharp.text.Paragraph($"Tipo Bien Contratado", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);

            _texto = new iTextSharp.text.Paragraph($": {data.type_well}", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);



            _texto = new iTextSharp.text.Paragraph($"Tipo de Incidente", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);

            _texto = new iTextSharp.text.Paragraph($": {data.type_claim}", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);



            _texto = new iTextSharp.text.Paragraph($"Detalle de Servicio", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);

            _texto = new iTextSharp.text.Paragraph($": {data.description_well}", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);



            _texto = new iTextSharp.text.Paragraph($"Detalle de Incidente", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);

            _texto = new iTextSharp.text.Paragraph($": {data.description_claim}", PDFGenerator.getFont("times_12"));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = getColor("white");
            cell.Border = 0;
            table.AddCell(cell);



            /*table.AddCell("Tipo Bien Contratado");
            table.AddCell(data.type_well);

            table.AddCell("Tipo de Incidente");
            table.AddCell(data.type_claim);

            table.AddCell("Detalle de Servicio");
            table.AddCell(data.description_well);

            table.AddCell("Detalle de Incidente");
            table.AddCell(data.description_claim);*/

            _texto = new iTextSharp.text.Paragraph("", FontFactory.GetFont("times_16", 10, Font.BOLD, getColor("white")));
            _texto.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell = new PdfPCell();
            cell.AddElement(_texto);
            //cell = new PdfPCell(new Paragraph(""));
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = getColor("black");
            cell.BorderColorBottom = getColor("white");
            cell.BorderColor = getColor("white");
            table.AddCell(cell);

            Paragraph body_parrafo = new Paragraph();
            body_parrafo.Add(table);
            Document document = PDFTemplate.LDRsimpleProof((PDFLDRclaim)data, source);
            document.Add(body_parrafo);

            try
            {
                document.Close();
                //return string.Format("{0}{1}/pdf/", AppSettings.reports["output-client"], data.name_file);
                return string.Format("{0}/{1}.pdf", source, data.name_file);
            }
            catch (Exception e)
            {
                return null;
            }
           
        }

        public class Data : PDFLDRclaim
        {
            public string type { get; set; }
            public string year { get; set; }
            public string numeration { get; set; }
            public DateTime date_creation { get; set; }
            public string names { get; set; }
            public string identification { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string home { get; set; }
            public string legal_guardian { get; set; }
            public string origin { get; set; }
            public string campus { get; set; }
            public string type_well { get; set; }
            public string type_claim { get; set; }
            public string description_well { get; set; }
            public string description_claim { get; set; }
        }
    }
}
