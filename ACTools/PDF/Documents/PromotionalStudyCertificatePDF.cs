﻿using ACTools.Configuration;
using ACTools.SuperString;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ACTools.PDF.Documents
{
    public class PromotionalStudyCertificatePdf
    {
        #region methods

        /// <summary>
        /// Genera la Constancia y devuelve la ruta
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string Generate(Data data)
        {
            var nCreditosO = 0;
            var nCussosO = 0;
            var nCreditosE = 0;
            var nCursosE = 0;
            var nCursosApro = 0;
            var nCursosDesa = 0;
            var nCreditosApro = 0;
            var nCreditosDesa = 0;

            var pdfGen = new PDFGenerator();
            //subtitle
            var titulo =
                new Paragraph("CERTIFICADO DE ESTUDIOS", pdfGen.getFontDoc("times_20_blue_sky"))
                {
                    Alignment = Element.ALIGN_CENTER
                };

            var subtitulo = new Paragraph
            {
                new Phrase("\nFACULTAD DE: ", pdfGen.getFontDoc("times_16")),
                new Phrase(data.college_name, pdfGen.getFontDoc("times_bold_italic_16")),
                new Phrase("\nE.A.P: ", pdfGen.getFontDoc("times_16")),
                new Phrase(data.program_name, pdfGen.getFontDoc("times_bold_italic_16"))
            };
            subtitulo.Alignment = Element.ALIGN_CENTER;
            //text
            var prefacio = new Paragraph
            {
                new Paragraph("\n"),
                new Phrase("        "),
                new Phrase("La Autoridad Universitaria que suscribe, ", pdfGen.getFontDoc("times_12")),
                new Phrase($"certifica que {data.GenderData["civil_status"]} ",pdfGen.getFontDoc("times_12")),
                new Phrase(data.student_names, pdfGen.getFontDoc("times_bold_italic_12")),
                new Phrase(", con código ", pdfGen.getFontDoc("times_12")),
                new Phrase(data.student_id, pdfGen.getFontDoc("times_bold_italic_12")),
                new Phrase(", ha cursado las siguientes asignaturas que se indican ", pdfGen.getFontDoc("times_12")),
                new Phrase("con los siguientes resultados:", pdfGen.getFontDoc("times_12")),
                new Paragraph("\n"),
                new Paragraph("\n")
            };
            prefacio.Alignment = Element.ALIGN_JUSTIFIED;


            // add Table
            var contentTable = new PdfPTable(6)
            {
                WidthPercentage = 100,
                HorizontalAlignment = 0
            };
            // content_table.TotalWidth = 500f;
            // content_table.SpacingAfter = 10;
            // content_table.SetWidths(new float[] { 55f, 220, 65, 100f, 65, 65, 65, 65, 70f });
            contentTable.SetWidths(new[] {55f, 220, 100f, 60, 60, 65f});

            // foreach (string option in new string[] { "CÓDIGO", "ASIGNATURA", "UNIVERSIDAD", "PAÍS", "RESOLUCIÓN", "OBSERVACIÓN", "CALIFICATIVO", "CRÉDITO", "FECHA" })
            foreach (var option in new[] {"CÓDIGO", "ASIGNATURA", "OBSERVACIÓN", "CALIFICATIVO", "CRÉDITO", "FECHA"})
            {
                var cell = PDFGenerator.Cell.centered();
                var parrafo =
                    new Paragraph(option, pdfGen.getFontDoc("table_body_strong")) {Alignment = Element.ALIGN_CENTER};
                cell.AddElement(parrafo);
                cell.Border = Rectangle.BOX;
                cell.PaddingTop = 2f;
                cell.PaddingBottom = 7f;
                cell.BorderColor = PDFGenerator.getColor("blue_sky");
                contentTable.AddCell(cell);
            }
            int[] cycles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
            foreach (var option in cycles)
            {
                var objTmp = data.Courses.Where(f => f.Cycle == option).ToList();
                if (objTmp.Count <= 0) continue;

                var celdaCiclo = new PdfPCell
                {
                    PaddingLeft = 50f,
                    Colspan = 9,
                    BorderWidthBottom = 0.5f,
                    BorderWidthTop = 0,
                    BorderWidthLeft = 0.5f,
                    BorderWidthRight = 0.5f,
                    BorderColorBottom = PDFGenerator.getColor("blue_sky_light")
                };
                var textCiclo = new Paragraph
                {
                    new Phrase($"{objTmp.First().Cycle.numberToOrdinal()} PERIODO",
                        pdfGen.getFontDoc("resolution_body_bold"))
                };
                //convertir a texto
                celdaCiclo.AddElement(textCiclo);
                contentTable.AddCell(celdaCiclo);

                // CURSOS DEL CICLO
                foreach (var obj in objTmp)
                {
                    if (!obj.IsAproved) // ignorar desaprobados
                    {
                        var aprovedCourse = data.Courses.Count(f => f.Id == obj.Id && f.IsAproved);
                        if (aprovedCourse == 0)
                        {
                            nCursosDesa += 1;
                            nCreditosDesa += obj.Credits;
                        }
                        continue;
                    }
                    nCursosApro += 1;
                    nCreditosApro += obj.Credits;
                    // FIX N CREDITOS Y CURSOS
                    if (obj.Type == "E")
                    {
                        nCursosE++;
                        nCreditosE += obj.Credits;
                    }
                    else
                    {
                        nCussosO++;
                        nCreditosO += obj.Credits;
                    }
                    var bodyCellLTmp = PDFGenerator.Cell.centered();
                    bodyCellLTmp.BorderWidthBottom = 0.5f;
                    bodyCellLTmp.BorderColorBottom = PDFGenerator.getColor("blue_sky_light");
                    // Paragraph body_cell_l_tmp_parr = new Paragraph()
                    bodyCellLTmp.AddElement(new Phrase(obj.Id, pdfGen.getFontDoc("table_body")));
                    bodyCellLTmp.PaddingLeft = 5f;
                    bodyCellLTmp.HorizontalAlignment = Element.ALIGN_CENTER;
                    // body_cell_l_tmp.VerticalAlignment = Element.ALIGN_MIDDLE;
                    bodyCellLTmp.BorderWidthLeft = 0.5f;
                    bodyCellLTmp.PaddingRight = 0f;
                    contentTable.AddCell(bodyCellLTmp);


                    var bodyCellC1Tmp = PDFGenerator.Cell.centered();
                    bodyCellC1Tmp.AddElement(new Phrase(obj.Name, pdfGen.getFontDoc("table_body")));
                    bodyCellC1Tmp.BorderWidthBottom = 0.5f;
                    bodyCellC1Tmp.BorderColorBottom = PDFGenerator.getColor("blue_sky_light");
                    bodyCellC1Tmp.PaddingLeft = 5f;
                    bodyCellC1Tmp.PaddingRight = 0f;
                    contentTable.AddCell(bodyCellC1Tmp);

                    var bodyCellC15Tmp = PDFGenerator.Cell.centered();
                    bodyCellC15Tmp.AddElement(new Phrase(obj.Observation, pdfGen.getFontDoc("table_body")));
                    bodyCellC15Tmp.BorderWidthBottom = 0.5f;
                    bodyCellC15Tmp.BorderColorBottom = PDFGenerator.getColor("blue_sky_light");
                    bodyCellC15Tmp.PaddingLeft = 5f;
                    bodyCellC15Tmp.PaddingRight = 0f;
                    contentTable.AddCell(bodyCellC15Tmp);

                    var bodyCellC2Tmp = PDFGenerator.Cell.centered();
                    bodyCellC2Tmp.AddElement(new Phrase(obj.Score.numberToLetters(),
                        pdfGen.getFontDoc("table_body"))); //convertir
                    bodyCellC2Tmp.BorderWidthBottom = 0.5f;
                    bodyCellC2Tmp.BorderColorBottom = PDFGenerator.getColor("blue_sky_light");
                    bodyCellC2Tmp.PaddingLeft = 5f;
                    bodyCellC2Tmp.PaddingRight = 0f;
                    contentTable.AddCell(bodyCellC2Tmp);

                    var bodyCellC3Tmp = PDFGenerator.Cell.centered();
                    bodyCellC3Tmp.AddElement(new Phrase(obj.Credits.ToString(),
                        pdfGen.getFontDoc("table_body")));
                    bodyCellC3Tmp.BorderWidthBottom = 0.5f;
                    bodyCellC3Tmp.BorderColorBottom = PDFGenerator.getColor("blue_sky_light");
                    bodyCellC3Tmp.PaddingLeft = 5f;
                    bodyCellC3Tmp.PaddingRight = 0f;
                    bodyCellC3Tmp.HorizontalAlignment = Element.ALIGN_CENTER;
                    bodyCellC3Tmp.VerticalAlignment = Element.ALIGN_MIDDLE;
                    contentTable.AddCell(bodyCellC3Tmp);

                    var bodyCellRTmp = PDFGenerator.Cell.centered();
                    bodyCellRTmp.BorderWidthBottom = 0.5f;
                    bodyCellRTmp.BorderColorBottom = PDFGenerator.getColor("blue_sky_light");
                    bodyCellRTmp.HorizontalAlignment = Element.ALIGN_CENTER;
                    bodyCellRTmp.VerticalAlignment = Element.ALIGN_MIDDLE;
                    bodyCellRTmp.AddElement(new Phrase($"{obj.Date:dd/MM/yyyy}",
                        pdfGen.getFontDoc("table_body")));
                    bodyCellRTmp.BorderWidthRight = 0.5f;
                    bodyCellRTmp.PaddingLeft = 10f;
                    contentTable.AddCell(bodyCellRTmp);
                }
            }


            //Linea 
            var celdaEnd = new PdfPCell
            {
                PaddingLeft = 0f,
                Colspan = 6,
                BorderWidthBottom = 0f,
                BorderWidthTop = 0,
                BorderWidthLeft = 0.5f,
                BorderWidthRight = 0.5f,
                BorderColorBottom = PDFGenerator.getColor("blue_sky_light")
            };
            celdaEnd.AddElement(new Phrase(
                "==================================================================================================",
                pdfGen.getFontDoc("times_bold_italic_8")));
            contentTable.AddCell(celdaEnd);


            //resumen
            var celdaSummaryLeft = new PdfPCell
            {
                Colspan = 2,
                BorderWidthBottom = 0.5f,
                BorderWidthTop = 0f,
                BorderWidthLeft = 0.5f,
                BorderWidthRight = 0f,
                PaddingTop = 0f,
                PaddingBottom = 10f
            };


            var celdaSummaryRight = new PdfPCell
            {
                Colspan = 4,
                BorderWidthBottom = 0.5f,
                BorderWidthTop = 0f,
                BorderWidthLeft = 0f,
                BorderWidthRight = 0.5f,
                PaddingTop = 0f,
                PaddingBottom = 10f,
                PaddingRight = 40f
            };


            var bodyCellSummaryTmp1P = new Paragraph {Alignment = Element.ALIGN_RIGHT};
            bodyCellSummaryTmp1P.Add(new Phrase(
                $"Asignaturas Obligatorias Aprobadas: {nCussosO}\nAsignaturas Electivas Aprobadas: {nCursosE}",
                pdfGen.getFontDoc("table_body")));
            bodyCellSummaryTmp1P.Add(new Phrase(
                $"\nTotal Asignaturas Aprobadas: {nCursosApro}\nTotal Asignaturas Desaprobadas: {nCursosDesa}",
                pdfGen.getFontDoc("table_body_bold")));
            celdaSummaryLeft.AddElement(bodyCellSummaryTmp1P);


            var bodyCellSummaryTmp2P = new Paragraph {Alignment = Element.ALIGN_RIGHT};
            bodyCellSummaryTmp2P.Add(new Phrase(
                $"Créditos Obligatorios Aprobados: {nCreditosO}\nCréditos Electivos Aprobados: {nCreditosE}",
                pdfGen.getFontDoc("table_body")));
            bodyCellSummaryTmp2P.Add(new Phrase(
                $"\nTotal Créditos Aprobados: {nCreditosApro}\nTotal Créditos Desaprobados: {nCreditosDesa}",
                pdfGen.getFontDoc("table_body_bold")));
            celdaSummaryRight.AddElement(bodyCellSummaryTmp2P);


            contentTable.AddCell(celdaSummaryLeft);
            contentTable.AddCell(celdaSummaryRight);

            // create document
            var document = PDFEvents.createWithPhoto(data, "");
            document.Add(titulo);
            document.Add(subtitulo);
            document.Add(prefacio);
            document.Add(contentTable);
            try
            {
                document.Close();
                return $"{AppSettings.reports["output-client"]}{data.name_file}/pdf/";
            }
            catch
            {
                return null;
            }
        }

        public class Data : PDFData
        {
            public Data()
            {
                Courses = new List<CourseDetail>();
            }

            public List<CourseDetail> Courses { get; set; }
            public Dictionary<string, string> GenderData { get; set; }
        }

        public class CourseDetail
        {
            public int Cycle { get; set; }
            public string Type { get; set; }
            public string Id { get; set; }
            public string Name { get; set; }
            public string University { get; set; }
            public string Country { get; set; }
            public string Resolution { get; set; }
            public string Observation { get; set; }
            public int Score { get; set; }
            public int Credits { get; set; }
            public DateTime? Date { get; set; }
            public bool IsAproved { get; set; }
        }

        #endregion methods
    }
}