﻿using ACTools.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Clase para imprimir la resolución de Convalidación
    /// </summary>
    public class ConvalidationPDF
    {
        /// <summary>
        /// Genera la Constancia de Ingreso y devuelve la ruta del PDF generado
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string generate(Data data)
        {
            //Document document = PDFTemplate.simpleProof(data, " ");
            
            PDFGenerator pdf_gen = new PDFGenerator();
            
            // doc_sub_title_little
            Paragraph titulo = new Paragraph(
                $"FACULTAD DE {data.college_name.ToUpper()}\n", pdf_gen.getFontDoc("doc_title_little"));
            titulo.Alignment = Element.ALIGN_CENTER;

            Paragraph subtitulo = new Paragraph(
                $"RESOLUCIÓN DECANAL N° {data.resolution_code}-{data.resolution_year}-{data.college_code}-UC"
                , pdf_gen.getFontDoc("doc_sub_title_little"));
            subtitulo.Alignment = Element.ALIGN_CENTER;

            Paragraph espacio_en_blanco = new Paragraph("\n");
           
            Paragraph fecha_parrafo = new Paragraph(
                $"Expedido el {data.resolution_date: dd} de {data.resolution_date: MMMM} de {data.resolution_date: yyyy}"
                , pdf_gen.getFontDoc("doc_body"));

            fecha_parrafo.Alignment = Element.ALIGN_RIGHT;

            //Crear varios parrafos :)
            Paragraph visto = new Paragraph(
               "\n       VISTO:\n\n", pdf_gen.getFontDoc("doc_sub_title_little"));
            

            Paragraph visto_body = new Paragraph(
                $"El expediente presentado por el(la) estudiante {data.student_names.ToUpper()}, con código de matrícula N° {data.student_id}, quien solicita convalidación de asignaturas.\n"
                , pdf_gen.getFontDoc("doc_body"));
            visto_body.Alignment = Element.ALIGN_JUSTIFIED;

            Paragraph considerando = new Paragraph(
               "\n       CONSIDERANDO:\n\n", pdf_gen.getFontDoc("doc_sub_title_little"));

            string considerando1;
            if (data.is_conti)
                considerando1 = $"Que, el(la) estudiante ingresó a la Escuela Académico Profesional de {data.program_name} de la Universidad Continental, en el proceso de admisión del periodo académico {data.term}, quien con anterioridad cursó estudios en la misma Universidad.\n\n";
            else
                considerando1 = $"Que, el(la) estudiante ingresó a la Escuela Académico Profesional de {data.program_name} de la Universidad Continental, en el proceso de admisión del periodo académico {data.term}, quien con anterioridad cursó estudios en el(la) {data.proc_institution}.\n\n";
            Paragraph considerando_body = new Paragraph(considerando1, pdf_gen.getFontDoc("doc_body"));
            considerando_body.Alignment = Element.ALIGN_JUSTIFIED;

            Paragraph considerando_body_2 = new Paragraph(
                "Que, mediante Resolución N° 032-2007-CU/UCCI, de fecha 06 de agosto de 2007, se aprobó el Reglamento de Convalidación de la Universidad Continental, que establece los requisitos y procedimiento de la convalidación de asignaturas.\n\n"
                , pdf_gen.getFontDoc("doc_body"));
            considerando_body_2.Alignment = Element.ALIGN_JUSTIFIED;

            Paragraph considerando_body_3 = new Paragraph(
               "Que, se ha revisado y analizado la documentación presentada por el (la) estudiante, y la misma cumple con los requisitos establecidos para aprobar la convalidación de asignaturas, por lo que;\n\n"
               , pdf_gen.getFontDoc("doc_body"));
            considerando_body_3.Alignment = Element.ALIGN_JUSTIFIED;

            Paragraph considerando_body_4 = new Paragraph(
               "El Decano en uso de sus atribuciones,\n"
               , pdf_gen.getFontDoc("doc_body"));
            considerando_body_4.Alignment = Element.ALIGN_JUSTIFIED;

            Paragraph resuelve = new Paragraph(
               "\n       RESUELVE:\n\n", pdf_gen.getFontDoc("doc_sub_title_little"));

            Paragraph contenido_resuelve = new Paragraph();

            Paragraph aprobar = new Paragraph();
            aprobar.Add(new Phrase("1° APROBAR ", pdf_gen.getFontDoc("doc_sub_title_little")));
            aprobar.Add(new Phrase(
                $"la convalidación de la(s) asignatura(s) de el(la) estudiante {data.student_names.ToUpper()}, perteneciente a la Escuela Académico Profesional de {data.program_name} de la Universidad Continental, bajo el siguiente detalle:"
                , pdf_gen.getFontDoc("doc_body")));
            contenido_resuelve.Alignment = Element.ALIGN_JUSTIFIED;
            //aprobar.FirstLineIndent = 25;

            /// Tabla de convalidación de cursos
            PdfPTable cursos = PDFGenerator.Table.transparent(6);
            int[] widths = new int[] { 100, 13, 13, 100, 13, 13 };
            cursos.SetWidths(widths);
            cursos.WidthPercentage = 100;
            string sumila1_str;
            if (data.is_conti)
                sumila1_str = "ASIGNATURAS APROBADAS EN LA UNIV CONTINENTAL";
            else sumila1_str = $"ASIGNATURAS APROBADAS EN EL(LA) {data.proc_institution}\n\n";
            PdfPCell sumilla_cursos_1 = new PdfPCell(
                new Paragraph(sumila1_str
                    , pdf_gen.getFontDoc("tbl_footer_bold")));

            sumilla_cursos_1.Border = Rectangle.NO_BORDER;
            sumilla_cursos_1.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            sumilla_cursos_1.Colspan = 3;
            cursos.AddCell(sumilla_cursos_1);
            //cursos.AddCell(new PdfPCell(new Phrase("R2C2-5")) { Colspan = 4 });
            PdfPCell sumilla_cursos_2 = new PdfPCell(
                new Paragraph(
                    string.Format("ASIGNATURAS DEL PLAN DE ESTUDIOS DE LA ESCUELA ACADEMICO PROFESIONAL DE {0} DE LA UNIVERSIDAD CONTINENTAL\n\n"
                    , data.program_name.ToUpper())
                    , pdf_gen.getFontDoc("tbl_footer_bold")));

            sumilla_cursos_2.Border = Rectangle.NO_BORDER;
            sumilla_cursos_2.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            sumilla_cursos_2.Colspan = 3;
            cursos.AddCell(sumilla_cursos_2);
            //cabecera 2
            for (int i = 0; i < 2; i++)
            {
                PdfPCell cabecera_l = new PdfPCell(
                    new Paragraph("ASIGNATURAS", pdf_gen.getFontDoc("tbl_title")));
                cabecera_l.BackgroundColor = PDFGenerator.getColor("gray_sky");
                cabecera_l.HorizontalAlignment = Element.ALIGN_CENTER;
                cabecera_l.VerticalAlignment = Element.ALIGN_CENTER;
                cabecera_l.BorderWidth = 0.5f;
                PdfPCell cabecera_c = new PdfPCell(
                    new Paragraph("CR", pdf_gen.getFontDoc("tbl_title")));
                cabecera_c.BackgroundColor = PDFGenerator.getColor("gray_sky");
                cabecera_c.HorizontalAlignment = Element.ALIGN_CENTER;
                cabecera_c.VerticalAlignment = Element.ALIGN_CENTER;
                cabecera_c.BorderWidth = 0.5f;
                PdfPCell cabecera_r = new PdfPCell(
                    new Paragraph("Nota", pdf_gen.getFontDoc("tbl_title"))); 
                cabecera_r.BackgroundColor = PDFGenerator.getColor("gray_sky");
                cabecera_r.HorizontalAlignment = Element.ALIGN_CENTER;
                cabecera_r.VerticalAlignment = Element.ALIGN_CENTER;
                cabecera_r.BorderWidth = 0.5f;

                cursos.AddCell(cabecera_l);
                cursos.AddCell(cabecera_c);
                cursos.AddCell(cabecera_r);
            }
            // cuerpo de la tabla
            foreach (Course course in data.courses)
            {
                // Esto es el primer registro
                PdfPCell asignatura = PDFGenerator.Cell.centered();
                asignatura.AddElement(new Phrase(course.proc_course_name, pdf_gen.getFontDoc("table_body")));
                asignatura.Border = Rectangle.NO_BORDER;
                asignatura.BorderWidthLeft = 0.5f;
                asignatura.BorderWidthTop = 0.25f;
                asignatura.BorderWidthBottom = 0.25f;
                asignatura.UseBorderPadding = false;

                //credito
                PdfPCell credito = PDFGenerator.Cell.centered();
                credito.AddElement(new Phrase(course.proc_course_credits.ToString(), pdf_gen.getFontDoc("table_body")));
                credito.Border = Rectangle.NO_BORDER;
                credito.BorderWidthLeft = 0.5f;
                credito.BorderWidthRight = 0.5f;
                credito.BorderWidthTop = 0.25f;
                credito.BorderWidthBottom = 0.25f;
                credito.UseBorderPadding = false;

                //nota
                PdfPCell nota = PDFGenerator.Cell.centered();
                nota.AddElement(new Phrase(course.proc_course_score, pdf_gen.getFontDoc("table_body")));
                nota.Border = Rectangle.NO_BORDER;
                nota.BorderWidthLeft = 0.5f;
                nota.BorderWidthRight = 0.5f;
                nota.BorderWidthTop = 0.25f;
                nota.BorderWidthBottom = 0.25f;
                nota.UseBorderPadding = false;

                cursos.AddCell(asignatura);
                cursos.AddCell(credito);
                cursos.AddCell(nota);

                //Curso equivalente
                if (course.uc_rowspan > 0)
                {
                    PdfPCell uc_asignatura = PDFGenerator.Cell.centered();
                    uc_asignatura.Rowspan = course.uc_rowspan;
                    uc_asignatura.AddElement(new Phrase(course.uc_course_name, pdf_gen.getFontDoc("table_body")));
                    uc_asignatura.Border = Rectangle.NO_BORDER;
                    uc_asignatura.BorderWidthLeft = 0.5f;
                    uc_asignatura.BorderWidthTop = 0.25f;
                    uc_asignatura.BorderWidthBottom = 0.25f;
                    uc_asignatura.UseBorderPadding = false;

                    //credito
                    PdfPCell uc_credito = PDFGenerator.Cell.centered();
                    uc_credito.Rowspan = course.uc_rowspan;
                    uc_credito.AddElement(new Phrase(course.uc_course_credits.ToString(), pdf_gen.getFontDoc("table_body")));
                    uc_credito.Border = Rectangle.NO_BORDER;
                    uc_credito.BorderWidthLeft = 0.5f;
                    uc_credito.BorderWidthRight = 0.5f;
                    uc_credito.BorderWidthTop = 0.25f;
                    uc_credito.BorderWidthBottom = 0.25f;
                    uc_credito.UseBorderPadding = false;

                    //nota
                    PdfPCell uc_nota = PDFGenerator.Cell.centered();
                    uc_nota.Rowspan = course.uc_rowspan;
                    uc_nota.AddElement(new Phrase(course.uc_course_score, pdf_gen.getFontDoc("table_body")));
                    uc_nota.Border = Rectangle.NO_BORDER;
                    uc_nota.BorderWidthLeft = 0.5f;
                    uc_nota.BorderWidthRight = 1f;
                    uc_nota.BorderWidthTop = 0.25f;
                    uc_nota.BorderWidthBottom = 0.25f;
                    uc_nota.UseBorderPadding = false;

                    cursos.AddCell(uc_asignatura);
                    cursos.AddCell(uc_credito);
                    cursos.AddCell(uc_nota);
                }
                
            }
            


            Paragraph wrapp = new Paragraph("\n");




            Paragraph transcribir = new Paragraph();
            transcribir.Add(new Phrase("2° TRANSCRIBIR: ", pdf_gen.getFontDoc("doc_sub_title_little")));
            transcribir.Add( new Phrase(
                $" la presente resolución a las instancias correspondientes para su conocimiento y fines de ley.\n"
                , pdf_gen.getFontDoc("doc_body")));
            //transcribir.FirstLineIndent = 25;
            transcribir.Alignment = Element.ALIGN_JUSTIFIED;




            Paragraph sumilla_1 = new Paragraph("Regístrese, comuníquese y archívese.\n\n\n", pdf_gen.getFontDoc("doc_body"));
            sumilla_1.Alignment = Element.ALIGN_JUSTIFIED;
            

            Paragraph sumilla_2 = new Paragraph(
                $"c.c: Decano de la facultad de {data.college_name} (1)\nRegistros Académicos (1)\nInteresado (1)\nArchivo (1)"
                , pdf_gen.getFontDoc("table_body"));
            sumilla_2.Alignment = Element.ALIGN_JUSTIFIED;

            // create document
            Document document = PDFEvents.createSimple(data, header_pages: "RESOLUCIÓN DE CONVALIDACIÓN");

            document.Add(titulo);
            document.Add(subtitulo);
            document.Add(espacio_en_blanco);
            document.Add(fecha_parrafo);
            document.Add(visto);
            document.Add(visto_body);
            document.Add(considerando);
            document.Add(considerando_body);
            document.Add(considerando_body_2);
            document.Add(considerando_body_3);
            document.Add(considerando_body_4);
            document.Add(resuelve);
            document.Add(aprobar);
            document.NewPage();
            document.Add(cursos);
            document.Add(wrapp);
            document.Add(transcribir);
            document.Add(sumilla_1);
            document.Add(sumilla_2);

            try
            {
                document.Close();
                return $"{AppSettings.reports["output-client"]}{data.name_file}/pdf/";
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// Objetos de colección de datos para generar el documento
        /// </summary>
        public class Data : PDFData
        {
            public string college_code { get; set; }
            public string resolution_code { get; set; }
            public string resolution_year { get; set; }
            public DateTime resolution_date { get; set; }
            public string term { get; set; }
            public string proc_institution { get; set; }
            public bool is_conti { get; set; }
            public string adm_type_name { get; set; }
            public string modality_name { get; set; }
            public string date_of_admission { get; set; }
            public DateTime adm_date { get; set; }
            public List<Course> courses { get; set; }
        }
        public class Course
        {
            public string proc_course_name { get; set; }
            public string proc_course_credits { get; set; }
            public string proc_course_score { get; set; }
            public string uc_course_name { get; set; }
            public int uc_course_credits { get; set; }
            public string uc_course_score { get; set; }
            public int uc_rowspan { get; set; }
        }
    }
}


