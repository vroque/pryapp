﻿using System;
using System.Collections.Generic;
using ACTools.Configuration;
using iTextSharp.text;
using ACTools.SuperString;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Constancia de Matrícula Promocional (SUNEDU)
    /// </summary>
    public class PromotionalProofOfEnrollmentPDF
    {
        public static string Generate(Data data)
        {
            PDFGenerator pdf_gen = new PDFGenerator();

            Document document = PDFEvents.createWithPhoto(data);
            
            Paragraph bodyParagraph = new Paragraph();
            
            Paragraph office = new Paragraph(" ", pdf_gen.getFontDoc("doc_body"))
            {
                Alignment = Element.ALIGN_CENTER
            };
            office.Add(new Phrase("\n\n"));
            bodyParagraph.Add(office);
            
            Paragraph title =
                new Paragraph("Constancia de matrícula", pdf_gen.getFontDoc("doc_title"))
                {
                    Alignment = Element.ALIGN_CENTER
                };
            bodyParagraph.Add(title);
            
            Paragraph contentParagraph = new Paragraph
            {
                new Phrase("\n"),
                new Phrase("        "),
                new Phrase("El jefe(e) de Registros Académicos de la Universidad Continental deja constancia que ", pdf_gen.getFontDoc("doc_body")),
                new Phrase(data.student_names.ToUpper(), pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(", con código de matrícula N° ", pdf_gen.getFontDoc("doc_body")),
                new Phrase(data.StudentDni, pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(", estudiante de la ", pdf_gen.getFontDoc("doc_body")),
                new Phrase($"Facultad de {data.college_name}, ", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase($"Escuela Académico Profesional de {data.program_name}; ", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase("registra matrícula en el ", pdf_gen.getFontDoc("doc_body")),
                new Phrase($"{data.Period} período ", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase($"del {data.Term},", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase($" el {data.DateTermEnrolled:dd} de {data.DateTermEnrolled:MMMM} de {data.DateTermEnrolled:yyyy}, en la ", pdf_gen.getFontDoc("doc_body")),
                new Phrase($"Modalidad {data.StudyMode}", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(".", pdf_gen.getFontDoc("doc_body")),
                new Phrase("\n\n"),
                new Phrase("        "),
                new Phrase($"Se expide la presente constancia a solicitud ", pdf_gen.getFontDoc("doc_body")),
                new Phrase($"{data.GenderData["concerned"]}.", pdf_gen.getFontDoc("doc_body"))
            };

            contentParagraph.Alignment = Element.ALIGN_JUSTIFIED;
            contentParagraph.SetLeading(1.0f, 2.0f);

            bodyParagraph.Add(contentParagraph);

            Paragraph dateParagraph =
                new Paragraph($"\n\nExpedido el {DateTime.Now:dd} de {DateTime.Now:MMMM} de {DateTime.Now:yyyy}",
                    pdf_gen.getFontDoc("doc_body")) {Alignment = Element.ALIGN_RIGHT};

            bodyParagraph.Add(dateParagraph);

            document.Add(bodyParagraph);
            try
            {
                document.Close();
                return $"{AppSettings.reports["output-client"]}{data.name_file}/pdf/";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

        }
        
        /// <summary>
        /// Objeto de datos para generar el documento
        /// </summary>
        public class Data: PDFData
        {
            /// <summary>
            /// Período académico
            /// </summary>
            public string Term { get; set; }
            /// <summary>
            /// Ciclo matriculado
            /// </summary>
            public string Period { get; set; }
            /// <summary>
            /// Fecha de matrícula
            /// </summary>
            public DateTime DateTermEnrolled { get; set; }
            public string StudentDni { get; set; }
            public string StudyMode { get; set; }
            public Dictionary<string, string> GenderData { get; set; }
        }
    }
}