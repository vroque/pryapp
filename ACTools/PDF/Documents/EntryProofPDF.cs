﻿using ACTools.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Generar PDF de Constancia de Ingreso
    /// </summary>
    public class EntryProofPDF
    {
        /// <summary>
        /// Genera la Constancia y devuelve la ruta
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string generate(EntryProofPDF.Data data)
        {
            iTextSharp.text.Document document = PDFTemplate.simpleProof(
                (PDFData)data, 
                "COMISIÓN PERMANENTE DE ADMISIÓN");

            iTextSharp.text.Paragraph body_parrafo = new iTextSharp.text.Paragraph();

            iTextSharp.text.Paragraph titulo = new iTextSharp.text.Paragraph("Constancia de Ingreso", 
                PDFGenerator.getFont("lucida_italic_28"));
            titulo.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            

            iTextSharp.text.Paragraph content = new iTextSharp.text.Paragraph();
            content.Add(new iTextSharp.text.Phrase("\n"));
            content.Add(new iTextSharp.text.Phrase("         El Presidente de la Comisión de Admisión hace constar que ", PDFGenerator.getFont("lucida_italic_12")));
            content.Add(new iTextSharp.text.Phrase(string.Format("{0}, ", data.student_names), PDFGenerator.getFont("times_bold_italic_12")));
            content.Add(new iTextSharp.text.Phrase(" identificado(a) con código de Matrícula N° ", PDFGenerator.getFont("lucida_italic_12")));
            content.Add(new iTextSharp.text.Phrase(data.student_id, PDFGenerator.getFont("times_bold_italic_12")));
            //content.Add(new iTextSharp.text.Phrase(", ingresó a esta Casa Superior de Estudios en el semestre ", PDFGenerator.getFont("lucida_italic_12")));
            content.Add(new iTextSharp.text.Phrase(", ingresó por ", PDFGenerator.getFont("lucida_italic_12")));
            // modalidad de ingreso
            content.Add(new iTextSharp.text.Phrase(string.Format("{0}", data.adm_type), PDFGenerator.getFont("lucida_bold_italic_12")));
            // periodo de ingreso
            content.Add(new iTextSharp.text.Phrase(" a esta casa superior de estudios en el periodo ", PDFGenerator.getFont("lucida_italic_12")));
            content.Add(new iTextSharp.text.Phrase(data.adm_term, PDFGenerator.getFont("times_bold_italic_12")));
            content.Add(new iTextSharp.text.Phrase(" el ", PDFGenerator.getFont("lucida_italic_12")));
            // fecha de ingreso
            content.Add(new iTextSharp.text.Phrase(
                string.Format(
                    "{0} de {1} de {2}",
                    data.adm_date.ToString("dd"),
                    data.adm_date.ToString("MMMM", new System.Globalization.CultureInfo("es-ES")),
                    data.adm_date.ToString("yyyy")),
                    PDFGenerator.getFont("lucida_bold_italic_12")
                )
            );

            // carrera
            content.Add(new iTextSharp.text.Phrase(string.Format(" a la Escuela Académico Profesional de {0}", data.program_name)
                                                    , PDFGenerator.getFont("times_bold_italic_12")));

            content.Add(new iTextSharp.text.Phrase(" a la modalidad ", PDFGenerator.getFont("lucida_italic_12")));
            // modalidad de ingreso
            content.Add(new iTextSharp.text.Phrase(data.adm_modality, PDFGenerator.getFont("times_bold_italic_12")));
            content.Alignment = iTextSharp.text.Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);

            // fin tabla
            //fin tabla
            iTextSharp.text.Paragraph content_post = new iTextSharp.text.Paragraph();
            content_post.Add(new iTextSharp.text.Phrase("\n"));
            content_post.Add(new iTextSharp.text.Phrase("          Se expide la presente a solicitud del interesado para los fines que considere conveniente.", PDFGenerator.getFont("lucida_italic_12")));
            content_post.Alignment = iTextSharp.text.Element.ALIGN_JUSTIFIED;
            content_post.SetLeading(1.0f, 2.0f);


            iTextSharp.text.Paragraph fecha_parrafo = new iTextSharp.text.Paragraph(
                string.Format(
                    "Huancayo, {0} de {1} de {2}.",
                    DateTime.Now.ToString("dd"),
                    DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES")),
                    DateTime.Now.ToString("yyyy")),
                    PDFGenerator.getFont("lucida_bold_italic_12")
                );
            fecha_parrafo.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;

            body_parrafo.Add(titulo);
            body_parrafo.Add(content);
            body_parrafo.Add(content_post);
            body_parrafo.Add(fecha_parrafo);

            document.Add(body_parrafo);
            try
            {
                document.Close();
                return string.Format("{0}{1}/pdf/", AppSettings.reports["output-client"], data.name_file);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        /// <summary>
        /// Objesto de colleccion de datos para generar el documento
        /// </summary>
        public class Data : PDFData
        {
            public string adm_term { get; set; }
            public string adm_type { get; set; }
            public string program { get; set; }
            public string adm_modality { get; set; }
            public DateTime adm_date { get; set; }
        }
    }
}
