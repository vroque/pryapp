﻿using ACTools.Configuration;
using iTextSharp.text;
using System;
using ACTools.SuperString;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Generación de PDF de Constancia de Ingreso
    /// </summary>
    public class ProofOfEntryPDF
    {
        /// <summary>
        /// Genera la Constancia de Egresado y devuelve la ruta del PDF generado
        /// </summary>
        /// <param name="data"></param>
        /// <returns>url del documento PDF</returns>
        public static string generate(Data data)
        {
            PDFGenerator pdf_gen = new PDFGenerator();
            
            Paragraph body_paragraph = new Paragraph();

            Paragraph title =
                new Paragraph("Constancia de ingreso", pdf_gen.getFontDoc("doc_title"));
            title.Alignment = Element.ALIGN_CENTER;
            body_paragraph.Add(title);

            Paragraph content = new Paragraph
            {
                new Phrase("\n"),
                new Phrase("        "),
                new Phrase("El presidente de la Comisión de Admisión hace constar que ", pdf_gen.getFontDoc("doc_body")),
                new Phrase(data.student_names.ToUpper(), pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase($", con código de matrícula N° ", pdf_gen.getFontDoc("doc_body")),
                new Phrase($"{data.student_id}", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(", ingresó por ", pdf_gen.getFontDoc("doc_body")),
                new Phrase(data.adm_type_name.ToUpper(), pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(" a esta casa superior de estudios en el período ", pdf_gen.getFontDoc("doc_body")),
                new Phrase(data.adm_term, pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(", el ", pdf_gen.getFontDoc("doc_body")),
                new Phrase(data.date_of_admission, pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(", a la ", pdf_gen.getFontDoc("doc_body")),
                new Phrase($"Escuela Académico Profesional de {data.program_name.ToUpper()} ", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase("en la ", pdf_gen.getFontDoc("doc_body")),
                new Phrase($"Modalidad {data.study_mode}.", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase("\n"),
                new Phrase("        "),
                new Phrase($"Se expide la presente a solicitud {data.gender_concerned} ", pdf_gen.getFontDoc("doc_body")),
                new Phrase("para los fines que considere conveniente.", pdf_gen.getFontDoc("doc_body"))
            };
            content.Alignment = Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);
            body_paragraph.Add(content);

            Paragraph date_paragraph =
                new Paragraph(
                    $"\nExpedido el {DateTime.Now:dd} de {DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES"))} de {DateTime.Now:yyyy}",
                    pdf_gen.getFontDoc("doc_body")
                ) {Alignment = Element.ALIGN_RIGHT};
            body_paragraph.Add(date_paragraph);

            // create document
            Document document = PDFEvents.createSimple(data, " ");

            document.Add(body_paragraph);
            try
            {
                document.Close();
                return $"{AppSettings.reports["output-client"]}{data.name_file}/pdf/";
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// Objetos de colección de datos para generar el documento
        /// </summary>
        public class Data : PDFData
        {
            public string adm_term { get; set; }
            public string adm_type_name { get; set; } 
            public string study_mode { get; set; }
            public string date_of_admission { get; set; }
            public DateTime adm_date { get; set; }
            public string gender_civil_status { get; set; }
            public string gender_concerned { get; set; }
            public string gender_identified { get; set; }
            public string student_dni { get; set; }

        }
    }
}
