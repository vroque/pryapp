﻿using System;
using ACTools.Configuration;
using iTextSharp.text;
using ACTools.SuperString;

namespace ACTools.PDF.Documents
{
    /// <summary>
    /// Constancia de Egresado
    /// </summary>
    public class ProofOfGraduationPDF
    {
        public static string Generate(Data data)
        {
            PDFGenerator pdf_gen = new PDFGenerator();
            //Document document = PDFTemplate.simpleProof(data, " ");
            Document document = PDFEvents.createWithPhoto(data);

            Paragraph body_paragraph = new Paragraph();

            Paragraph office =
                new Paragraph("Secretaría General", pdf_gen.getFontDoc("doc_staff"))
                {
                    Alignment = Element.ALIGN_CENTER
                };
            office.Add(new Phrase("\n"));
            body_paragraph.Add(office);

            Paragraph title =
                new Paragraph("Constancia de egresado", pdf_gen.getFontDoc("doc_title"))
                {
                    Alignment = Element.ALIGN_CENTER
                };
            body_paragraph.Add(title);

            Paragraph content = new Paragraph
            {
                new Phrase("\n"),
                new Phrase("        "),
                new Phrase("El secretario general de la Universidad Continental deja constancia que, ",pdf_gen.getFontDoc("doc_body")),
                //new Phrase($"{data.gender_civil_status} ", pdf_gen.getFontDoc("doc_body")),
                new Phrase($"{data.student_names.ToUpper()},", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(" identificado(a) con código de matrícula N° ", pdf_gen.getFontDoc("doc_body")),
                new Phrase(data.student_dni.toEachCapitalize(), pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(", ha culminado sus estudios universitarios en la ", pdf_gen.getFontDoc("doc_body")),
                new Phrase($"Escuela Académico Profesional de {data.program_name.ToUpper()},",pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase($" Facultad de {data.college_name.ToUpper()},",pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(" habiendo aprobado satisfactoriamente las asignaturas del plan curricular vigente, cumpliendo con los ", pdf_gen.getFontDoc("doc_body")),
                new Phrase($"{data.credits_o + data.credits_e} ", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase("créditos entre obligatorios y electivos en la ",pdf_gen.getFontDoc("doc_body")),
                new Phrase($"Modalidad {data.study_mode}", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(".", pdf_gen.getFontDoc("doc_body")),
                new Phrase("\n"),
                new Phrase("        "),
                new Phrase($"Así mismo, {data.gender_indicated} ha concluido con sus actividades ", pdf_gen.getFontDoc("doc_body")),
                new Phrase("extracurriculares de prácticas preprofesionales, idioma extranjero y ", pdf_gen.getFontDoc("doc_body")),
                new Phrase("proyección social, cumpliendo con los requisitos del Reglamento de ", pdf_gen.getFontDoc("doc_body")),
                new Phrase("Grados y Títulos para ser considerado como ", pdf_gen.getFontDoc("doc_body")),
                new Phrase(data.gender_graduated.ToUpper(), pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(" el ", pdf_gen.getFontDoc("doc_body")),
                new Phrase($"{data.graduation_date:dd} de {data.graduation_date:MMMM} de {data.graduation_date:yyyy}", pdf_gen.getFontDoc("doc_body_bold")),
                new Phrase(".", pdf_gen.getFontDoc("doc_body")),
                new Phrase("\n"),
                new Phrase("        "),
                new Phrase($"Se expide la presente constancia a solicitud {data.gender_concerned}.", pdf_gen.getFontDoc("doc_body"))
            };

            content.Alignment = Element.ALIGN_JUSTIFIED;
            content.SetLeading(1.0f, 2.0f);

            body_paragraph.Add(content);

            Paragraph date_paragraph =
                new Paragraph($"\nExpedido el {DateTime.Now:dd} de {DateTime.Now:MMMM} de {DateTime.Now:yyyy}",
                    pdf_gen.getFontDoc("doc_body")) {Alignment = Element.ALIGN_RIGHT};

            body_paragraph.Add(date_paragraph);

            document.Add(body_paragraph);
            try
            {
                document.Close();
                return $"{AppSettings.reports["output-client"]}{data.name_file}/pdf/";
            }
            catch
            {
                return null;
            }
        }
       
        /// <summary>
        /// Objeto de datos para generar eldocumento
        /// </summary>
        public class Data: PDFData
        {
            public string study_mode { get; set; }
            public int credits_o { get; set; }
            public int credits_e { get; set; }
            public DateTime graduation_date { get; set; }
            public string gender_civil_status { get; set; }
            public string gender_concerned { get; set; }
            public string gender_indicated { get; set; }
            public string gender_graduated { get; set; }
            public string gender_identified { get; set; }
            public string student_dni { get; set; }
        }
        
    }
}