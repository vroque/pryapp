﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACTools.PDF
{
    public class PDFData
    {
        public string name_file { get; set; }
        public List<PDFSign> signs { get; set; }
        public string student_id { get; set; }
        public string student_names { get; set; }
        public string student_gender { get; set; }
        public string college_name { get; set; }
        public string program_name { get; set; }
        public int request_id { get; set; }
        public int document_number { get; set; }
        public string user { get; set; }
        public byte[] photo_bytes { get; set; }
    }

    public class PDFSign
    {
        public byte[] sign { get; set; }
        public byte[] post_sign { get; set; }
        public byte[] seal { get; set; }
    }

    public class PDFLDRclaim
    {
        public string name_file { get; set; }
    }
}

