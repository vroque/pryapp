﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using ACTools.SuperString;

namespace ACTools.PDF
{
    public class PDFEvents : iTextSharp.text.pdf.PdfPageEventHelper
    {
        #region properties
        public string header_type { get; set; }
        public string type { get; set; } //footer
        public Boolean is_first_page { get; set; }
        public Boolean pagination { get; set; }

        public List<PDFSign> signs { get; set; }
        public string student_id { get; set; }
        public string student_names { get; set; }
        public string document_name { get; set; }
        public string user;
        public int npages = 0;
        public int num_subjects { get; set; }

        PdfContentByte cb;
        PdfTemplate template;
        #endregion

        #region constructors
        public PDFEvents()
        {
            this.is_first_page = true;
            this.pagination = true;
        }
        #endregion constructors
        #region methods
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            if (this.pagination)
            {
                cb = writer.DirectContent;
                template = cb.CreateTemplate(50, 50);
            }
        }

        public override void OnStartPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            
            if (!this.is_first_page)
                {
                    this.addHeaderSimple(writer, document);
                }
            else
                {
                    this.is_first_page = false;
                }

        }
        public override void OnCloseDocument(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            if (this.pagination)
            {
                template.BeginText();
                template.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED), 8f);
                template.SetTextMatrix(0, 0);
                template.ShowText("" + writer.PageNumber);
                template.EndText();
            }
        }
        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document doc)
        {
            this.npages += 1;
            switch (this.type)
            {
                case "half_A4":
                    this.half_A4(writer, doc);
                    break;
                case "membrete":
                case "studies-certificate":
                case "conva-resolution":
                case "simple":
                case "withPhoto":
                    this.addFootDocument(writer, doc);
                    break;
                case "cicDiploma":
                    AddDocumentFootWithoutFooterText(writer, doc, true);
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region otros
        public void half_A4(PdfWriter writer, Document doc)
        {
            PdfPTable footer = new PdfPTable(1);
            footer.DefaultCell.Border = Rectangle.NO_BORDER;
            footer.TotalWidth = doc.PageSize.Width - 140;

            /* IMAGENES */
            drawSigns(ref doc, 60f);

            /* END IMAGENES */

            PdfPCell cell_firmas = new PdfPCell();
            cell_firmas.Border = Rectangle.NO_BORDER;
            cell_firmas.AddElement(new Phrase("\n\n\n\n"));
            footer.AddCell(cell_firmas);

            Paragraph footer_text = new Paragraph();
            footer_text.Add(new Phrase("El presente documento y las firmas consignadas en él han sido emitidos a través de medios digitales, en el amparo de lo dispuesto en el artículo 141-A del Código Civil: “Artículo 141-A. – Formalidad En los casos que la ley establezca que la manifestación de voluntad deba hacerse a través de alguna formalidad expresa o requerida de firma, ésta podrá ser generada o comunicada a través de medios electrónicos, ópticos o cualquier otro análogo. Tratándose de instrumentos públicos, la autoridad competente deberá dejar constancia del medio empleado y conservar una versión íntegra para su ulterior consulta.” La verificación de esta constancia podrá hacerse en la página web: http://www.universidad.continental.edu.pe/certificaciones.\nDocumento emitido por: ", PDFGenerator.getFont("verdana_8")));
            footer_text.Add(new Phrase(string.Format("{0} a las {1}", user, DateTime.Now.ToString()), PDFGenerator.getFont("verdana_bold_8")));
            footer_text.Alignment = Element.ALIGN_JUSTIFIED;
            footer_text.SetLeading(1.0f, 1.0f);

            PdfPCell cell = new PdfPCell();
            cell.Border = Rectangle.BOX;
            cell.AddElement(footer_text);

            footer.AddCell(cell);

            footer.AddCell(new Phrase(string.Format("Página: {0} de {1}", writer.PageNumber, this.npages), PDFGenerator.getFont("verdana_8")));

            footer.WriteSelectedRows(0, -1, 72f, (doc.BottomMargin - 12), writer.DirectContent);
        }
  
        public void addHeaderSimple(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document doc)
        {
            PDFGenerator pdf_gen = new PDFGenerator();

            PdfPTable header = new PdfPTable(1);
            header.WidthPercentage = 100;
            Paragraph header_text = new Paragraph();
            header_text.Add(new Phrase(10f, this.document_name, pdf_gen.getFontDoc("head_name")));

            header_text.Add(new Phrase(10f, "\n", pdf_gen.getFontDoc("doc_head")));

            header_text.Add(new Phrase(10f, "Código: ", pdf_gen.getFontDoc("doc_head")));

            header_text.Add(new Phrase(10f, string.Format("{0} - ", this.student_id), pdf_gen.getFontDoc("doc_head")));

            header_text.Add(new Phrase(10f, "Nombre: ", pdf_gen.getFontDoc("doc_head")));

            header_text.Add(new Phrase(10f, string.Format("{0}", this.student_names.toEachCapitalize()), pdf_gen.getFontDoc("doc_head")));
            header_text.SetLeading(1.0f, 1.0f);
            header_text.Alignment = Element.ALIGN_CENTER;

            PdfPCell cell = new PdfPCell();
            cell.Border = Rectangle.BOTTOM_BORDER;
            cell.PaddingBottom = 10f;
            cell.AddElement(header_text);
            header.AddCell(cell);
            doc.Add(header);

        }

        public static Document createSimple(PDFData data, string header_text = "", string type_doc = "simple", bool rotate = false, string header_pages = null) {
            PDFGenerator pdf_gen = new PDFGenerator();
            Document document;
            System.IO.FileStream filestram_obj = PDFGenerator.create(data.name_file);
            if (filestram_obj != null)
            {
                document = new Document(PageSize.A4, 72f, 72f, 40f, 194f);
                if (rotate)
                {
                    document = new Document(PageSize.A4.Rotate(), 72f, 72f, 40f, 180f);
                }
                PdfWriter _tmp = PdfWriter.GetInstance(document, filestram_obj);
                PDFEvents events = new PDFEvents();
                events.user = data.user;
                events.type = type_doc;
                events.signs = data.signs;
                events.student_id = data.student_id;
                events.student_names = data.student_names;
                if (header_pages != null)
                    events.document_name = header_pages;

                _tmp.PageEvent = events;
            }
            else
            {
                throw new Exception("El en filestream");
            }

            Paragraph request_code = new Paragraph(new Paragraph(
                string.Format("N° {0}-{1}", data.request_id, data.document_number),
                pdf_gen.getFontDoc("doc_id")));
            request_code.Alignment = Element.ALIGN_RIGHT;


            Image logo = PDFGenerator.logo();
            logo.Alignment = Element.ALIGN_LEFT;
            logo.ScaleToFit(180, 91);

            PdfPTable tbl_head = new PdfPTable(2);
            tbl_head.WidthPercentage = 100;
            tbl_head.SetWidths(new float[] { 50f, 50f });

            PdfPCell _cell_logo = PDFGenerator.Cell.centered();
            _cell_logo.AddElement(logo);
            _cell_logo.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell_logo.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell _cell_code = PDFGenerator.Cell.centered();
            _cell_code.AddElement(request_code);
            _cell_code.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell_code.HorizontalAlignment = Element.ALIGN_RIGHT;


            tbl_head.AddCell(_cell_logo);
            tbl_head.AddCell(_cell_code);

            document.Open();
            document.Add(tbl_head);
            document.Add(new Paragraph("\n"));
            if (header_text.Length > 0)
            {
                Paragraph cabecera;
                cabecera = new Paragraph(header_text, pdf_gen.getFontDoc("doc_staff"));
                cabecera.Alignment = Element.ALIGN_CENTER;
                document.Add(cabecera);
                document.Add(new Paragraph("\n"));
                document.Add(new Paragraph("\n"));
            }

            return document;
        }

        /// <summary>
        /// Actividades extracurriculares
        /// </summary>
        /// <param name="data"></param>
        /// <param name="header_text"></param>
        /// <param name="type_doc"></param>
        /// <param name="rotate"></param>
        /// <param name="header_pages"></param>
        /// <returns></returns>
        public static Document ExtracurricularActivityCreate(string source, PDFData data, string header_text = "", string type_doc = "simple", bool rotate = false, string header_pages = null)
        {
            PDFGenerator pdf_gen = new PDFGenerator();
            Document document;
            System.IO.FileStream filestram_obj = PDFGenerator.create(source, data.name_file);
            if (filestram_obj != null)
            {
                document = new Document(PageSize.A4, 72f, 72f, 40f, 194f);
                if (rotate)
                {
                    document = new Document(PageSize.A4.Rotate(), 72f, 72f, 40f, 180f);
                }
                PdfWriter _tmp = PdfWriter.GetInstance(document, filestram_obj);
                PDFEvents events = new PDFEvents();
                events.user = data.user;
                events.type = type_doc;
                events.signs = data.signs;
                events.student_id = data.student_id;
                events.student_names = data.student_names;
                if (header_pages != null)
                    events.document_name = header_pages;

                _tmp.PageEvent = events;
            }
            else
            {
                throw new Exception("El en filestream");
            }

            Paragraph request_code = new Paragraph(new Paragraph(
                string.Format("N° {0}-{1}", data.request_id, data.document_number),
                pdf_gen.getFontDoc("doc_id")));
            request_code.Alignment = Element.ALIGN_RIGHT;


            Image logo = PDFGenerator.logo();
            logo.Alignment = Element.ALIGN_LEFT;
            logo.ScaleToFit(180, 91);

            PdfPTable tbl_head = new PdfPTable(2);
            tbl_head.WidthPercentage = 100;
            tbl_head.SetWidths(new float[] { 50f, 50f });

            PdfPCell _cell_logo = PDFGenerator.Cell.centered();
            _cell_logo.AddElement(logo);
            _cell_logo.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell_logo.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell _cell_code = PDFGenerator.Cell.centered();
            _cell_code.AddElement(request_code);
            _cell_code.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell_code.HorizontalAlignment = Element.ALIGN_RIGHT;


            tbl_head.AddCell(_cell_logo);
            tbl_head.AddCell(_cell_code);

            document.Open();
            document.Add(tbl_head);
            document.Add(new Paragraph("\n"));
            if (header_text.Length > 0)
            {
                Paragraph cabecera;
                cabecera = new Paragraph(header_text, pdf_gen.getFontDoc("doc_staff"));
                cabecera.Alignment = Element.ALIGN_CENTER;
                document.Add(cabecera);
                document.Add(new Paragraph("\n"));
                document.Add(new Paragraph("\n"));
            }

            return document;
        }

        /// <summary>
        /// Centro de Idiomas
        /// </summary>
        /// <param name="source"></param>
        /// <param name="data"></param>
        /// <param name="headerText"></param>
        /// <param name="typeDoc"></param>
        /// <param name="rotate"></param>
        /// <param name="headerPages"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static Document LanguagesCenter(string source, PDFData data, string headerText = "",
            string typeDoc = "simple", bool rotate = false, string headerPages = null)
        {
            var pdfGenerator = new PDFGenerator();
            Document document;

            var filestramObj = source.Any()
                ? PDFGenerator.create(source, data.name_file)
                : PDFGenerator.create(data.name_file);
            
            if (filestramObj != null)
            {
                document = new Document(PageSize.A4, 72f, 72f, 40f, 194f);
                
                if (rotate) document = new Document(PageSize.A4.Rotate(), 72f, 72f, 40f, 180f);
                
                var tmp = PdfWriter.GetInstance(document, filestramObj);
                var events = new PDFEvents
                {
                    user = data.user,
                    type = typeDoc,
                    signs = data.signs,
                    student_id = data.student_id,
                    student_names = data.student_names,
                    pagination = false
                };
                
                if (headerPages != null) events.document_name = headerPages;

                tmp.PageEvent = events;
            }
            else
            {
                throw new Exception("No se pudo generar el archivo :(");
            }

            var requestCode = new Paragraph(new Paragraph($"N° {data.request_id}-{data.document_number}",
                pdfGenerator.getFontDoc("doc_id"))) {Alignment = Element.ALIGN_RIGHT};

            var logo = PDFGenerator.logo();
            logo.Alignment = Element.ALIGN_LEFT;
            logo.ScaleToFit(180, 91);

            var tblHead = new PdfPTable(2) {WidthPercentage = 100};
            tblHead.SetWidths(new float[] { 50f, 50f });

            var cellLogo = PDFGenerator.Cell.centered();
            cellLogo.AddElement(logo);
            cellLogo.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellLogo.HorizontalAlignment = Element.ALIGN_LEFT;

            var cellCode = PDFGenerator.Cell.centered();
            cellCode.AddElement(requestCode);
            cellCode.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellCode.HorizontalAlignment = Element.ALIGN_RIGHT;

            tblHead.AddCell(cellLogo);
            tblHead.AddCell(cellCode);

            document.Open();
            document.Add(tblHead);
            document.Add(new Paragraph("\n"));
            
            if (headerText.Length <= 0) return document;
            
            var cabecera =
                new Paragraph(headerText, pdfGenerator.getFontDoc("doc_staff")) {Alignment = Element.ALIGN_CENTER};
            document.Add(cabecera);
            document.Add(new Paragraph("\n"));
            document.Add(new Paragraph("\n"));

            return document;
        }

        /// <summary>
        /// Centro de Idiomas con foto
        /// </summary>
        /// <param name="source"></param>
        /// <param name="data"></param>
        /// <param name="headerText"></param>
        /// <param name="headerPages"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static Document LanguagesCenterWithPhoto(string source, PDFData data, string headerText = "",
            string headerPages = null)
        {
            var pdfGen = new PDFGenerator();
            Document document;
            
            var filestramObj = source.Any()
                ? PDFGenerator.create(source, data.name_file)
                : PDFGenerator.create(data.name_file);

            if (filestramObj != null)
            {
                document = new Document(PageSize.A4, 72f, 72f, 40f, 194f);
                var tmp = PdfWriter.GetInstance(document, filestramObj);
                var events = new PDFEvents
                {
                    user = data.user,
                    type = "simple",
                    signs = data.signs,
                    student_id = data.student_id,
                    student_names = data.student_names
                };

                if (headerPages != null)
                    events.document_name = headerPages;

                tmp.PageEvent = events;
            }
            else
            {
                throw new Exception("No se pudo generar el archivo :(");
            }

            var requestCode = new Paragraph(new Paragraph(
                $"N° {data.request_id}-{data.document_number}",
                pdfGen.getFontDoc("doc_id"))) {Alignment = Element.ALIGN_LEFT};


            var logo = PDFGenerator.logo();
            logo.Alignment = Element.ALIGN_LEFT;
            logo.ScaleToFit(180, 91);

            
            if (data.photo_bytes == null) throw new Exception("El alumno no cuenta con foto.");

            var photo = Image.GetInstance(data.photo_bytes);
            photo.Alignment = Element.ALIGN_RIGHT;
            //photo.SetAbsolutePosition(450, 720);
            photo.ScaleToFit(100, 100);

            var tblHead = new PdfPTable(2) {WidthPercentage = 100};
            tblHead.SetWidths(new float[] { 50f, 50f });

            var cellLogo = PDFGenerator.Cell.centered();
            cellLogo.AddElement(logo);
            cellLogo.AddElement(requestCode);
            cellLogo.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellLogo.HorizontalAlignment = Element.ALIGN_LEFT;

            var cellCode = PDFGenerator.Cell.centered();
            cellCode.AddElement(photo);

            //_cell_code.AddElement(request_code);
            cellCode.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellCode.HorizontalAlignment = Element.ALIGN_RIGHT;

            tblHead.AddCell(cellLogo);
            tblHead.AddCell(cellCode);

            document.Open();
            document.Add(tblHead);

            if (headerText.Length <= 0) return document;

            var cabecera = new Paragraph(headerText, pdfGen.getFontDoc("doc_staff")) {Alignment = Element.ALIGN_CENTER};
            document.Add(cabecera);

            return document;
        }

        public static Document createWithPhoto(PDFData data, string header_text = "", string header_pages = null)
        {
            PDFGenerator pdf_gen = new PDFGenerator();
            Document document;
            System.IO.FileStream filestram_obj = PDFGenerator.create(data.name_file);
            if (filestram_obj != null)
            {
                document = new Document(PageSize.A4, 72f, 72f, 40f, 194f);
                PdfWriter _tmp = PdfWriter.GetInstance(document, filestram_obj);
                PDFEvents events = new PDFEvents();
                events.user = data.user;
                events.type = "simple";
                events.signs = data.signs;
                events.student_id = data.student_id;
                events.student_names = data.student_names;

                if (header_pages != null)
                    events.document_name = header_pages;

                _tmp.PageEvent = events;
            }
            else
            {
                throw new Exception("El en filestream");
            }

            Paragraph request_code = new Paragraph(new Paragraph(
                string.Format("                        N° {0}-{1}", data.request_id, data.document_number),
                pdf_gen.getFontDoc("doc_id")));
            request_code.Alignment = Element.ALIGN_LEFT;


            Image logo = PDFGenerator.logo();
            logo.Alignment = Element.ALIGN_LEFT;
            logo.ScaleToFit(180, 91);

            
            if (data.photo_bytes == null)
            {
                throw new Exception("El alumno no cuenta con foto.");
            }

            Image photo = Image.GetInstance(data.photo_bytes);
            photo.Alignment = Element.ALIGN_RIGHT;
            //photo.SetAbsolutePosition(450, 720);
            photo.ScaleToFit(100, 100);

            PdfPTable tbl_head = new PdfPTable(2);
            tbl_head.WidthPercentage = 100;
            tbl_head.SetWidths(new float[] { 50f, 50f });

            PdfPCell _cell_logo = PDFGenerator.Cell.centered();
            _cell_logo.AddElement(logo);
            _cell_logo.AddElement(request_code);
            _cell_logo.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell_logo.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell _cell_code = PDFGenerator.Cell.centered();
            _cell_code.AddElement(photo);

            //_cell_code.AddElement(request_code);
            _cell_code.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell_code.HorizontalAlignment = Element.ALIGN_RIGHT;


            tbl_head.AddCell(_cell_logo);
            tbl_head.AddCell(_cell_code);

            document.Open();
            document.Add(tbl_head);
            if (header_text.Length > 0)
            {
                Paragraph cabecera;
                cabecera = new Paragraph(header_text, pdf_gen.getFontDoc("doc_staff"));
                cabecera.Alignment = Element.ALIGN_CENTER;
                document.Add(cabecera);
            }

            return document;
        }

        public void addFootDocument(PdfWriter writer, Document doc)
        {
            PDFGenerator pdf_gen = new PDFGenerator();
            PdfPTable footer = new PdfPTable(1);
            footer.DefaultCell.Border = Rectangle.NO_BORDER;
            footer.TotalWidth = doc.PageSize.Width - 140;
            float[] values = new float[12];

            /* IMAGENES */

            drawSigns(ref doc);

            /* END IMAGENES */

            PdfPCell cell_firmas = new PdfPCell();
            cell_firmas.Border = Rectangle.NO_BORDER;
            cell_firmas.AddElement(new Phrase("\n\n\n\n"));
            footer.AddCell(cell_firmas);

            Paragraph footer_text = new Paragraph();
            footer_text.Add(new Phrase("Este documento y las firmas consignadas en él han sido emitidos a través de medios digitales, al amparo de lo dispuesto en el artículo 141-A del Código Civil: “Artículo 141-A. – Formalidad En los casos que la ley establezca que la manifestación de voluntad deba hacerse a través de alguna formalidad expresa o requerida de firma, esta podrá ser generada o comunicada a través de medios electrónicos, ópticos o cualquier otro tipo análogo. Tratándose de instrumentos públicos, la autoridad competente deberá dejar constancia del medio empleado y conservar una versión íntegra para su ulterior consulta”. La verificación de esta constancia podrá hacerse en la página web:  http://www.universidad.continental.edu.pe/certificaciones.\nDocumento emitido por: ", pdf_gen.getFontDoc("foot_body")));
            footer_text.Add(new Phrase(string.Format("{0} a las {1}", user, DateTime.Now.ToString()), pdf_gen.getFontDoc("foot_body_bold")));
            footer_text.Alignment = Element.ALIGN_JUSTIFIED;
            footer_text.SetLeading(1.0f, 1.0f);

            PdfPCell cell = new PdfPCell();
            cell.Border = Rectangle.BOX;
            cell.AddElement(footer_text);
            footer.AddCell(cell);

            footer.WriteSelectedRows(0, -1, 72f, (doc.BottomMargin - 12), writer.DirectContent);

            if (this.pagination)
            {
                //contenido para la numeracion de pagina
                int pageN = writer.PageNumber;
                String text = string.Format("Página: {0} de ", pageN.ToString());
                Rectangle pageSize = doc.PageSize;
                cb.SetRGBColorFill(100, 100, 100);
                cb.BeginText();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED), 8f);
                cb.SetTextMatrix(75, 37f);
                cb.ShowText(text);
                cb.EndText();
                cb.AddTemplate(template, 125, 37f);
            }
        }

        /// <summary>
        /// Añade detalle de pie de página para documentos que no utilizan firmas digitalizadas.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="document"></param>
        /// <param name="horizontal"></param>
        public void AddDocumentFootWithoutFooterText(PdfWriter writer, Document document, bool horizontal = false)
        {
            var footer = new PdfPTable(1);
            footer.DefaultCell.Border = Rectangle.NO_BORDER;
            footer.TotalWidth = document.PageSize.Width - 140;

            if (horizontal) DrawSignsForHorizontalDocuments(ref document);
            else drawSigns(ref document);

            var cellSigns = new PdfPCell {Border = Rectangle.NO_BORDER};
            cellSigns.AddElement(new Phrase("\n\n\n\n"));
            footer.AddCell(cellSigns);

            footer.WriteSelectedRows(0, -1, 72, (document.BottomMargin - 12), writer.DirectContent);

            if (!pagination) return;
            //contenido para la numeracion de pagina
            var pageN = writer.PageNumber;
            var text = $"Página: {pageN} de ";
            var pageSize = document.PageSize;
            cb.SetRGBColorFill(100, 100, 100);
            cb.BeginText();
            cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED), 8f);
            cb.SetTextMatrix(75, 37f);
            cb.ShowText(text);
            cb.EndText();
            cb.AddTemplate(template, 125, 37f);
        }
        
        /// <summary>
        /// Dibuja las firmas 
        /// </summary>
        /// <param name="doc"></param>
        public void drawSigns(ref Document doc, Single h_base = 100f)
        {
            switch (this.signs.Count)
            {
                
                case 1:
                    Image sello_img = Image.GetInstance(this.signs[0].seal);
                    sello_img.SetAbsolutePosition(190, h_base + 30f);
                    sello_img.ScaleToFit(76f, 76f);
                    doc.Add(sello_img);

                    Image postfirma_img = Image.GetInstance(this.signs[0].post_sign);
                    postfirma_img.SetAbsolutePosition(250, h_base + 20f);
                    postfirma_img.ScaleToFit(160f, 36f);
                    doc.Add(postfirma_img);

                    Image firma_img = Image.GetInstance(this.signs[0].sign);
                    firma_img.SetAbsolutePosition(270, h_base + 35f);
                    firma_img.ScaleToFit(160f, 60f);
                    doc.Add(firma_img);

                    break;
                case 2:
                    // primera firma
                    Image sello1_img = Image.GetInstance(this.signs[0].seal);
                    sello1_img.ScaleToFit(76f, 76f);
                    sello1_img.SetAbsolutePosition(70, h_base + 30f);
                    doc.Add(sello1_img);
                   
                    Image postfirma1_img = Image.GetInstance(this.signs[0].post_sign);
                    postfirma1_img.SetAbsolutePosition(130, h_base + 10f);
                    postfirma1_img.ScaleToFit(160f, 36f);
                    doc.Add(postfirma1_img);

                    Image firma1_img = Image.GetInstance(this.signs[0].sign);
                    firma1_img.SetAbsolutePosition(130, h_base + 25f);
                    firma1_img.ScaleToFit(160f, 60f);
                    doc.Add(firma1_img);

                    // segunda firma
                    Image sello2_img = Image.GetInstance(this.signs[1].seal);
                    sello2_img.ScaleToFit(76f, 76f);
                    sello2_img.SetAbsolutePosition(320, h_base + 30f);
                    doc.Add(sello2_img);

                    Image postfirma2_img = Image.GetInstance(this.signs[1].post_sign);
                    postfirma2_img.SetAbsolutePosition(380, h_base + 10f);
                    postfirma2_img.ScaleToFit(140f, 36f);
                    doc.Add(postfirma2_img);

                    Image firma2_img = Image.GetInstance(this.signs[1].sign);
                    firma2_img.SetAbsolutePosition(380, h_base + 30f);
                    firma2_img.ScaleToFit(160f, 60f);
                    doc.Add(firma2_img);

                    break;
                case 3:
                    break;
                case 0:
                    throw new Exception("No hay firmas para este documento");
                default:
                    break;

            }
        }

        /// <summary>
        /// Firma para documentos con hoja en posición horizontal
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="height"></param>
        /// <exception cref="NullReferenceException"></exception>
        public void DrawSignsForHorizontalDocuments(ref Document doc, float height = 100f)
        {
            switch (signs.Count)
            {
                case 1:
                    var selloImg = Image.GetInstance(this.signs[0].seal);
                    selloImg.SetAbsolutePosition(390, height + 30f);
                    selloImg.ScaleToFit(76f, 76f);
                    doc.Add(selloImg);

                    var postfirmaImg = Image.GetInstance(this.signs[0].post_sign);
                    postfirmaImg.SetAbsolutePosition(450, height + 20f);
                    postfirmaImg.ScaleToFit(160f, 36f);
                    doc.Add(postfirmaImg);

                    var firmaImg = Image.GetInstance(this.signs[0].sign);
                    firmaImg.SetAbsolutePosition(470, height + 35f);
                    firmaImg.ScaleToFit(160f, 60f);
                    doc.Add(firmaImg);
                    
                    break;
                case 2:
                    // primera firma
                    var sello1Img = Image.GetInstance(this.signs[0].seal);
                    sello1Img.ScaleToFit(76f, 76f);
                    sello1Img.SetAbsolutePosition(140, height + 30f);
                    doc.Add(sello1Img);

                    var postfirma1Img = Image.GetInstance(this.signs[0].post_sign);
                    postfirma1Img.SetAbsolutePosition(200, height + 10f);
                    postfirma1Img.ScaleToFit(160f, 36f);
                    doc.Add(postfirma1Img);

                    var firma1Img = Image.GetInstance(this.signs[0].sign);
                    firma1Img.SetAbsolutePosition(200, height + 25f);
                    firma1Img.ScaleToFit(160f, 60f);
                    doc.Add(firma1Img);

                    // segunda firma
                    var sello2Img = Image.GetInstance(this.signs[1].seal);
                    sello2Img.ScaleToFit(76f, 76f);
                    sello2Img.SetAbsolutePosition(450, height + 30f);
                    doc.Add(sello2Img);

                    var postfirma2Img = Image.GetInstance(this.signs[1].post_sign);
                    postfirma2Img.SetAbsolutePosition(510, height + 10f);
                    postfirma2Img.ScaleToFit(140f, 36f);
                    doc.Add(postfirma2Img);

                    var firma2Img = Image.GetInstance(this.signs[1].sign);
                    firma2Img.SetAbsolutePosition(510, height + 30f);
                    firma2Img.ScaleToFit(160f, 60f);
                    doc.Add(firma2Img);
                    
                    break;
                case 0:
                    throw new NullReferenceException("No hay firmas para este documento");
                default:
                    break;
            }
        }
        
        #endregion otros
    }
}
