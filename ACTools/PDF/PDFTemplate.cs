﻿using ACTools.PDF.Documents;
using System;
using System.IO;
using System.Linq;
using ACTools.SuperString;
using iTextSharp.text;

namespace ACTools.PDF
{
    class PDFTemplate
    {
        #region properties
        #endregion properties

        #region methods


        public static iTextSharp.text.Document LDRsimpleProof(PDFLDRclaim data, string source)
        {
            iTextSharp.text.Document document;
            System.IO.FileStream filestram_obj = PDFGenerator.createSource(source,data.name_file);

            if (filestram_obj != null)
            {
                document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 50f, 50f, 30f, 30f);
                iTextSharp.text.pdf.PdfWriter _tmp = iTextSharp.text.pdf.PdfWriter.GetInstance(document, filestram_obj);
                PDFEvents events = new PDFEvents();
                //events.type = "membrete"; // no quiero membrete
                _tmp.PageEvent = events;
            }
            else
            {
                return null;
            }
            document.Open();
            iTextSharp.text.Image logo = PDFGenerator.logo();
            logo.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            logo.ScaleToFit(180, 91);
            document.Add(logo);
            document.Add(new iTextSharp.text.Paragraph("\n"));
            document.Add(new iTextSharp.text.Paragraph("\n"));
            return document;
        }
        
        /// <summary>
        /// Template para documentos del Centro de Idiomas
        /// </summary>
        /// <param name="source">¿En qué directorio se guardará el documento?</param>
        /// <param name="data">data básica para usar en el template</param>
        /// <param name="header_text">Texto de cabecera</param>
        /// <returns></returns>
        public static iTextSharp.text.Document languagesCenter(string source, PDFData data, string header_text)
        {
            iTextSharp.text.Document document;
            FileStream file_stream = source.Any()
                ? PDFGenerator.create(source, data.name_file)
                : PDFGenerator.create(data.name_file);

            if (file_stream != null)
            {
                document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 72f, 72f, 40f, 194f);
                iTextSharp.text.pdf.PdfWriter tmp = iTextSharp.text.pdf.PdfWriter.GetInstance(document, file_stream);
                PDFEvents events = new PDFEvents
                {
                    user = data.user,
                    type = "membrete",
                    signs = data.signs
                };
                tmp.PageEvent = events;
            }
            else
            {
                throw new Exception("No se pudo generar el archivo :(");
            }
            document.Open();
            iTextSharp.text.Paragraph codigo_parrafo = new iTextSharp.text.Paragraph(new iTextSharp.text.Paragraph(
                $"N° {data.document_number}-CIC", 
                PDFGenerator.getFont("times_14_red")));
            codigo_parrafo.SetLeading(0f, 0f);
            document.Add(codigo_parrafo);
            iTextSharp.text.Image logo = PDFGenerator.logo();
            logo.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            logo.ScaleToFit(180, 91);
            document.Add(logo);
            document.Add(new iTextSharp.text.Paragraph("\n"));
            iTextSharp.text.Paragraph cabecera;
            if (header_text.Length > 0)
            {
                cabecera = new iTextSharp.text.Paragraph(header_text, PDFGenerator.getFont("times_14"));
            }
            else
            {
                cabecera = new iTextSharp.text.Paragraph(
                    "El Director del Centro de Idiomas de la Universidad Continental deja:".ToUpper(), 
                    PDFGenerator.getFont("times_bold_italic_12"));
            }
            cabecera.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            document.Add(cabecera);
            document.Add(new iTextSharp.text.Paragraph("\n"));
            document.Add(new iTextSharp.text.Paragraph("\n"));
            return document;
        }

        #endregion methods
    }
}
