﻿using ACTools.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace ACTools.PDF
{
    public class PDFGenerator
    {
        public PDFGenerator()
        {
            //register fonts
            if (!FontFactory.IsRegistered("ac_lucida"))
                FontFactory.Register(_ST_PATH + @"\Fonts\LCALLIG.TTF", "ac_lucida");
            if (!FontFactory.IsRegistered("ac_verdana"))
                FontFactory.Register(_ST_PATH + @"\Fonts\VERDANA.TTF", "ac_verdana");
            if (!FontFactory.IsRegistered("ac_verdanab"))
                FontFactory.Register(_ST_PATH + @"\Fonts\VERDANAB.TTF", "ac_verdanab");
            if (!FontFactory.IsRegistered("ac_austin"))
                FontFactory.Register(_ST_PATH + @"\Fonts\Austin-Extrabold.otf", "ac_austin");
            if (!FontFactory.IsRegistered("ac_gilroy"))
                FontFactory.Register(_ST_PATH + @"\Fonts\gilroy-regular.otf", "ac_gilroy");
            if (!FontFactory.IsRegistered("ac_gilroyb"))
                FontFactory.Register(_ST_PATH + @"\Fonts\gilroy-bold", "ac_gilroyb");
        }

        #region properties

        private static string _ST_PATH
        {
            get
            {
                var path = Path.GetDirectoryName(
                    Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase));
                path = path + @"\App_Data\PDF";
                path = path.Substring(6, path.Length - 6); // remove file:\
                return path;
            }
        }

        private static string _REPO_PATH => AppSettings.reports["output"];

        //private const string _OUTPUT_PATH = AppSettings.reports["output"].ToString();

        #endregion properties

        #region static methods 

        /// <summary>
        /// Creación del PDF en el directorio por default
        /// </summary>
        /// <param name="fileName">Nombre del archivo (sin extensión)</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static FileStream create(string fileName)
        {
            try
            {
                //Mi modo solo mi modo
                return new FileStream($@"{AppSettings.reports["output"]}{fileName}.pdf", FileMode.Create);
            }
            catch (Exception ex)
            {
                Debug.Print("error:" + ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Creación del PDF en un directorio en específico
        /// </summary>
        /// <param name="source">¿En qué directorio se generará el PDF?</param>
        /// <param name="fileName">Nombre del archivo (sin extensión)</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static FileStream create(string source, string fileName)
        {
            try
            {
                //Mi modo solo mi modo
                return new FileStream($@"{AppSettings.reports[source]}{fileName}.pdf", FileMode.Create);
            }
            catch (Exception ex)
            {
                Debug.Print("error:" + ex.Message);
            }
            return null;
        }

        public static FileStream createSource(string source, string fileName)
        {
            try
            {
                if (!Directory.Exists(source))
                {
                    var di = Directory.CreateDirectory(source);
                }
                return new FileStream($@"{source}/{fileName}.pdf", FileMode.Create);
            }
            catch (Exception ex)
            {
                Debug.Print("error:" + ex.Message);
            }
            return null;
        }


        public static Image logo()
        {
            var logo = Image.GetInstance(_ST_PATH + @"\Images\logo.png");
            logo.Alignment = Element.ALIGN_CENTER;
            logo.ScaleToFit(180, 91);
            return logo;
        }

        public static Image logo(string size)
        {
            Image logo = null;

            if (size != "min") return logo;

            logo = Image.GetInstance(_ST_PATH + @"\Images\logo.png");
            logo.Alignment = Element.ALIGN_CENTER;
            logo.ScaleToFit(150, 60);

            return logo;
        }

        /// <summary>
        /// Verifica si el archivo pdf la solicitud ya ah sido creadado
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool exists(string fileName)
        {
            fileName = $"{fileName}.pdf";
            string[] files = null;
            try
            {
                files = Directory.GetFiles(_REPO_PATH, fileName);
            }
            catch
            {
                // Error con permisos
                return false;
            }
            return files.Length > 0;
        }

        /// <summary>
        /// Obtiene la ruta del archivo desde el repositorio
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string getRepoDocumentPath(string fileName)
        {
            if (fileName.Length <= 10)
            {
                throw new ApplicationException(
                    "No se puede realizar una busqueda de documentos con menos de 10 caracteres.");
            }

            fileName = $"{fileName}.pdf";
            var files = Directory.GetFiles(_REPO_PATH, fileName);
            return files.Length > 0 ? files.First() : null;
        }

        /// <summary>
        /// Obtiene la ruta del archivo desde el repositorio
        /// </summary>
        /// <param name="pathName"></param>
        /// <returns></returns>
        public static string[] listDocumentInPath(string pathName)
        {
            var count = AppSettings.reports.Count(f => f.Key == pathName);

            if (count <= 0) throw new Exception("no existe el destino en la configuración");

            var path = AppSettings.reports[pathName];
            const string name = "*.pdf";
            var files = Directory.GetFiles(path, name);
            return files.Length > 0 ? files : null;
        }

        #endregion

        #region inner clasess

        public class Cell : PdfPCell
        {
            public static PdfPCell transparent()
            {
                return new PdfPCell {Border = NO_BORDER};
            }

            public static PdfPCell centered()
            {
                var cell = transparent();
                cell.HorizontalAlignment = ALIGN_CENTER;
                cell.VerticalAlignment = ALIGN_MIDDLE;
                return cell;
            }

            public static PdfPCell transparent(int colspan = 0, int rowspan = 0)
            {
                var cell = transparent();
                if (colspan > 0)
                {
                    cell.Colspan = colspan;
                }
                if (rowspan > 0)
                {
                    cell.Rowspan = rowspan;
                }
                return cell;
            }
        }

        public class Table // : PdfPTable
        {
            public static PdfPTable transparent(int cols)
            {
                var table = new PdfPTable(cols);
                table.DefaultCell.Border = Rectangle.NO_BORDER;
                return table;
            }

            public static PdfPTable create(int cols)
            {
                return new PdfPTable(cols);
            }

            public static PdfPTable outline(int cols, bool outlineBool)
            {
                var table = new PdfPTable(cols);
                if (outlineBool)
                {
                }
                else
                {
                    table.DefaultCell.BorderWidthBottom = 0f;
                    table.DefaultCell.BorderWidthTop = 0f;
                    table.DefaultCell.BorderWidthLeft = 0f;
                    table.DefaultCell.BorderWidthRight = 0f;
                }
                return table;
            }
        }

        #endregion

        #region methods

        public static Font getFont(string name)
        {
            Dictionary<string, Font> fontList = new Dictionary<string, Font>
            {
                {"doc_title", FontFactory.GetFont("ac_lucida", "Cp1254", BaseFont.EMBEDDED, 28)},
                {
                    "doc_body",
                    new Font(BaseFont.CreateFont(_ST_PATH + @"\Fonts\LCALLIG.TTF", "Cp1254", BaseFont.EMBEDDED), 12)
                },
                {
                    "doc_body_bold", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\LCALLIG.TTF", "Cp1254", BaseFont.EMBEDDED), 12,
                        Font.BOLD)
                },
                {"doc_alt_body", FontFactory.GetFont(FontFactory.TIMES, 12, Font.ITALIC)},
                {"doc_alt_body_bold", FontFactory.GetFont(FontFactory.TIMES_BOLD, 12, Font.ITALIC)},
                /*********************************************
                 * fuentes de la nueva identidad grafica de UC
                 *********************************************/
                {
                    "austin_bold_28", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\Austin-Extrabold.otf", "Cp1254", BaseFont.EMBEDDED),
                        28)
                },
                {
                    "gilroy-regular", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\gilroy-regular.otf", "Cp1254", BaseFont.EMBEDDED),
                        12)
                },
                {
                    "gilroy-bold",
                    new Font(BaseFont.CreateFont(_ST_PATH + @"\Fonts\gilroy-bold.otf", "Cp1254", BaseFont.EMBEDDED), 12)
                },
                /*********************************************/
                {
                    "lucida_italic_12", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\LCALLIG.TTF", "Cp1254", BaseFont.EMBEDDED), 12)
                },
                {
                    "lucida_bold_italic_12", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\LCALLIG.TTF", "Cp1254", BaseFont.EMBEDDED), 12,
                        Font.BOLD)
                },
                {
                    "lucida_italic_28", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\LCALLIG.TTF", "Cp1254", BaseFont.EMBEDDED), 28)
                },
                {
                    "verdana_6", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANA.TTF", "Cp1254", BaseFont.EMBEDDED), 6)
                },
                {
                    "verdana_bold_6", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANAB.TTF", "Cp1254", BaseFont.EMBEDDED), 6)
                },
                {
                    "verdana_6_red", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANA.TTF", /*BaseFont.CP1250,*/"Cp1254",
                            BaseFont.EMBEDDED), 6, Font.NORMAL, BaseColor.RED)
                },
                {
                    "verdana_8", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANA.TTF", "Cp1254", BaseFont.EMBEDDED), 8)
                },
                {
                    "verdana_10", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANA.TTF", "Cp1254", BaseFont.EMBEDDED), 10)
                },
                {
                    "verdana_bold_10", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANAB.TTF", "Cp1254", BaseFont.EMBEDDED), 10)
                },
                {
                    "verdana_bold_28", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANAB.TTF", "Cp1254", BaseFont.EMBEDDED), 28)
                },
                {
                    "verdana_12", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANA.TTF", "Cp1254", BaseFont.EMBEDDED), 12)
                },
                {
                    "verdana_14", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANA.TTF", "Cp1254", BaseFont.EMBEDDED), 14)
                },
                {
                    "verdana_20", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANA.TTF", "Cp1254", BaseFont.EMBEDDED), 20)
                },
                {
                    "verdana_bold_14", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANAB.TTF", "Cp1254", BaseFont.EMBEDDED), 14)
                },
                {
                    "verdana_bold_12", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANAB.TTF", "Cp1254", BaseFont.EMBEDDED), 12)
                },
                {
                    "verdana_bold_8", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANAB.TTF", "Cp1254", BaseFont.EMBEDDED), 8)
                },
                {
                    "verdana_bold_40", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\VERDANAB.TTF", "Cp1254", BaseFont.EMBEDDED), 40)
                },
                {"times_6", FontFactory.GetFont(FontFactory.TIMES, 6, Font.NORMAL)},
                {"times_8", FontFactory.GetFont(FontFactory.TIMES, 8, Font.NORMAL)},
                {"times_10", FontFactory.GetFont(FontFactory.TIMES, 10, Font.NORMAL)},
                {"times_12", FontFactory.GetFont(FontFactory.TIMES, 12, Font.NORMAL)},
                {"times_bold_10", FontFactory.GetFont(FontFactory.TIMES, 10, Font.BOLD)},
                {"times_italic_12", FontFactory.GetFont(FontFactory.TIMES, 12, Font.ITALIC)},
                {"times_bold_italic_8", FontFactory.GetFont(FontFactory.TIMES_BOLD, 8, Font.ITALIC)},
                {"times_bold_italic_12", FontFactory.GetFont(FontFactory.TIMES_BOLD, 12, Font.ITALIC)},
                {"times_bold_italic_16", FontFactory.GetFont(FontFactory.TIMES_BOLD, 16, Font.ITALIC)},
                {"times_14", FontFactory.GetFont(FontFactory.TIMES, 14, Font.NORMAL)},
                {"times_16", FontFactory.GetFont(FontFactory.TIMES, 16, Font.NORMAL)},
                {"times_18", FontFactory.GetFont(FontFactory.TIMES, 18, Font.NORMAL)},
                {"times_20", FontFactory.GetFont(FontFactory.TIMES, 20, Font.NORMAL)},
                {"times_14_red", FontFactory.GetFont(FontFactory.TIMES, 14, Font.NORMAL, BaseColor.RED)},
                {"times_12_red", FontFactory.GetFont(FontFactory.TIMES, 12, Font.NORMAL, BaseColor.RED)},
                {"times_6_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 6, getColor("blue_sky"))},
                {"times_8_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 8, getColor("blue_sky"))},
                {"times_10_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 10, getColor("blue_sky"))},
                {"times_12_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 12, Font.NORMAL, getColor("blue_sky"))},
                {"times_14_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 14, getColor("blue_sky"))},
                {"times_16_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 16, getColor("blue_sky"))},
                {"times_20_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 20, getColor("blue_sky"))},
                {"times_24_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 24, getColor("blue_sky"))}
            };

            return fontList.ContainsKey(name) ? fontList[name] : null;
        }

        public Font getFontDoc(string name)
        {
            var fontList = new Dictionary<string, Font>
            {
                /*==================================Tratando estandarizar documentos==================================*/
                #region documentos
                {"doc_id", FontFactory.GetFont("ac_gilroy", "Cp1254", BaseFont.EMBEDDED, 12, Font.NORMAL, BaseColor.RED)},
                {"doc_staff", FontFactory.GetFont("ac_gilroy", "Cp1254", BaseFont.EMBEDDED, 14)},
                {"doc_title", FontFactory.GetFont("ac_austin", "Cp1254", BaseFont.EMBEDDED, 24)},
                {"doc_title_little", FontFactory.GetFont("ac_gilroyb", "Cp1254", BaseFont.EMBEDDED, 12, Font.BOLD)},
                {"doc_sub_title_little", FontFactory.GetFont("ac_gilroyb", "Cp1254", BaseFont.EMBEDDED, 10, Font.BOLD)},
                {"doc_body", FontFactory.GetFont("ac_gilroy", "Cp1254", BaseFont.EMBEDDED, 11)},
                {"doc_body_bold", FontFactory.GetFont("ac_gilroyb", "Cp1254", BaseFont.EMBEDDED, 11, Font.BOLD)},
                {"doc_head", FontFactory.GetFont("ac_gilroy", "Cp1254", BaseFont.EMBEDDED, 8)},
                #endregion

                #region tablas
                {"tbl_header", FontFactory.GetFont("ac_gilroyb", "Cp1254", BaseFont.EMBEDDED, 6, Font.BOLD)},
                //{"tbl_title", FontFactory.GetFont("ac_gilroyb", "Cp1254", BaseFont.EMBEDDED, 9, Font.BOLD)},
                {"tbl_title", FontFactory.GetFont("ac_gilroyb", "Cp1254", BaseFont.EMBEDDED, 6, Font.BOLD)},
                {"tbl_content", FontFactory.GetFont("ac_gilroy", "Cp1254", BaseFont.EMBEDDED, 6)},
                {"tbl_footer", FontFactory.GetFont("ac_gilroy", "Cp1254", BaseFont.EMBEDDED, 8)},
                {"tbl_footer_bold", FontFactory.GetFont("ac_gilroy", "Cp1254", BaseFont.EMBEDDED, 8, Font.BOLD)},
                #endregion

                #region cabecera
                {"head_nro_doc", FontFactory.GetFont("ac_gilroy", "Cp1254", BaseFont.EMBEDDED, 12)},
                {"head_name", FontFactory.GetFont("ac_gilroyb", "Cp1254", BaseFont.EMBEDDED, 10, Font.BOLD)},
                #endregion

                #region pie
                {"foot_body", FontFactory.GetFont("ac_gilroy", "Cp1254", BaseFont.EMBEDDED, 7)},
                {"foot_body_bold", FontFactory.GetFont("ac_gilroyb", "Cp1254", BaseFont.EMBEDDED, 7, Font.BOLD)},
                {"foot_staff", FontFactory.GetFont("ac_gilroyb", "Cp1254", BaseFont.EMBEDDED, 7, Font.BOLD)},
                {"foot_page_numeration", FontFactory.GetFont("ac_gilroy", "Cp1254", BaseFont.EMBEDDED, 8)},

                #endregion


                /*====================================================================================================*/



                // Titles
                //{"doc_title", FontFactory.GetFont("ac_lucida", "Cp1254", BaseFont.EMBEDDED, 28)},
                {"resolution_title", FontFactory.GetFont("ac_verdanab", "Cp1254", BaseFont.EMBEDDED, 12)},
                {"certificate_title", FontFactory.GetFont("ac_lucida", "Cp1254", BaseFont.EMBEDDED, 16)},
                // Subtitles
                {"resolution_subtitle", FontFactory.GetFont("ac_verdanab", "Cp1254", BaseFont.EMBEDDED, 10)},
                // body
                /*{
                    "doc_body",
                    new Font(BaseFont.CreateFont(_ST_PATH + @"\Fonts\LCALLIG.TTF", "Cp1254", BaseFont.EMBEDDED), 12)
                },
                {
                    "doc_body_bold", new Font(
                        BaseFont.CreateFont(_ST_PATH + @"\Fonts\LCALLIG.TTF", "Cp1254", BaseFont.EMBEDDED), 12,
                        Font.BOLD)
                },*/
                {"doc_alt_body", FontFactory.GetFont(FontFactory.TIMES, 12, Font.ITALIC)},
                {"doc_alt_body_bold", FontFactory.GetFont(FontFactory.TIMES_BOLD, 12, Font.ITALIC)},
                {"resolution_body", FontFactory.GetFont("ac_verdana", "Cp1254", BaseFont.EMBEDDED, 9)},
                {"resolution_body_bold", FontFactory.GetFont("ac_verdanab", "Cp1254", BaseFont.EMBEDDED, 9)},
                {"resolution_body_medium", FontFactory.GetFont("ac_verdana", "Cp1254", BaseFont.EMBEDDED, 11)},
                {"resolution_body_medium_bold", FontFactory.GetFont("ac_verdanab", "Cp1254", BaseFont.EMBEDDED, 11)},
                {"table_body", FontFactory.GetFont("ac_verdana", "Cp1254", BaseFont.EMBEDDED, 6)},
                {"table_body_bold", FontFactory.GetFont("ac_verdanab", "Cp1254", BaseFont.EMBEDDED, 6)},
                {"table_body_strong", FontFactory.GetFont("ac_verdanab", "Cp1254", BaseFont.EMBEDDED, 8)},
                {"official_transcript_body", FontFactory.GetFont(FontFactory.TIMES, 8, getColor("blue_sky"))},
                {"times_6", FontFactory.GetFont(FontFactory.TIMES, 6, Font.NORMAL)},
                {"times_8", FontFactory.GetFont(FontFactory.TIMES, 8, Font.NORMAL)},
                {"times_10", FontFactory.GetFont(FontFactory.TIMES, 10, Font.NORMAL)},
                {"times_12", FontFactory.GetFont(FontFactory.TIMES, 12, Font.NORMAL)},
                {"times_bold_10", FontFactory.GetFont(FontFactory.TIMES, 10, Font.BOLD)},
                {"times_italic_12", FontFactory.GetFont(FontFactory.TIMES, 12, Font.ITALIC)},
                {"times_bold_italic_8", FontFactory.GetFont(FontFactory.TIMES_BOLD, 8, Font.ITALIC)},
                {"times_bold_italic_12", FontFactory.GetFont(FontFactory.TIMES_BOLD, 12, Font.ITALIC)},
                {"times_bold_italic_16", FontFactory.GetFont(FontFactory.TIMES_BOLD, 16, Font.ITALIC)},
                {"times_14", FontFactory.GetFont(FontFactory.TIMES, 14, Font.NORMAL)},
                {"times_16", FontFactory.GetFont(FontFactory.TIMES, 16, Font.NORMAL)},
                {"times_14_red", FontFactory.GetFont(FontFactory.TIMES, 14, Font.NORMAL, BaseColor.RED)},
                {"times_12_red", FontFactory.GetFont(FontFactory.TIMES, 12, Font.NORMAL, BaseColor.RED)},
                {"times_6_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 6, getColor("blue_sky"))},
                {"times_8_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 8, getColor("blue_sky"))},
                {"times_10_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 10, getColor("blue_sky"))},
                {"times_12_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 12, Font.NORMAL, getColor("blue_sky"))},
                {"times_14_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 14, getColor("blue_sky"))},
                {"times_16_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 16, getColor("blue_sky"))},
                {"times_20_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 20, getColor("blue_sky"))},
                {"times_24_blue_sky", FontFactory.GetFont(FontFactory.TIMES, 24, getColor("blue_sky"))}
            };

            if (fontList.ContainsKey(name)) return fontList[name];

            throw new Exception("Font doesn't exist");
        }

        public static BaseColor getColor(string name)
        {
            var colorList = new Dictionary<string, BaseColor>
            {
                {"blue_sky", new BaseColor(88, 96, 135)},
                {"blue_sky_light", new BaseColor(224, 226, 235)},
                {"gray_sky", new BaseColor(204, 204, 204)},
                {"black", new BaseColor(58, 58, 58)},
                {"white", new BaseColor(255, 255, 255)}
            };
            return colorList.ContainsKey(name) ? colorList[name] : null;
        }

        #endregion methods
    }
}