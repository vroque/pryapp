﻿using ACTools.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ACTools.PDF
{
    public class PDFActions
    {
        /// <summary>
        /// Verifica si el archivo pdf de la solicitud ya ha sido creado
        /// </summary>
        /// <param name="file_name">Nombre del archivo</param>
        /// <returns></returns>
        public static bool exists(string file_name)
        {
            string repo_path = AppSettings.reports["output"];

            file_name = $"{file_name}.pdf";
            List<string> list = new List<string>();
            try
            {
                string[] files = Directory.GetFiles(repo_path, file_name);
                list.AddRange(files.ToList());
            }
            catch
            {
                // Error con permisos
                return false;
            }
            return list.Count > 0;
        }

        /// <summary>
        /// Verifica si el archivo pdf la solicitud ya ha sido creado
        /// </summary>
        /// <param name="name_file"></param>
        /// <returns></returns>
        public static List<string> existsGYT(string student_id)
        {
            student_id = string.Format("*{0}*.pdf", student_id);
            string _REPO_PATH_PRS = AppSettings.reports["output-prs"];

            string[] files = new string[] {};
            
            List<string> list = new List<string>();

            try
            {
                files = Directory.GetFiles(_REPO_PATH_PRS, student_id);

            }
            catch
            {
                // Error con permisos
                throw new Exception("No se pudo acceder a la carpeta");
            }
            foreach(string item in files)
            {
                string path = "";
                string[] full_path = item.Split('\\');// windows file separator
                foreach(string term in full_path)
                {
                    if(term.IndexOf(".pdf") > 0)
                    {
                        path = term.Substring(0, term.IndexOf(".pdf"));
                    }
                }
                if(path != "")
                    list.Add(path);
            }
            return list;
        }

        /// <summary>
        /// Obtiene la ruta del archivo desde el repositorio
        /// </summary>
        /// <param name="file_name">Nombre del archivo</param>
        /// <returns></returns>
        public static string getRepoDocumentPath(string file_name)
        {
            string repo_path = AppSettings.reports["output"];
            string pdf_file = $"{file_name}.pdf";

            if (file_name.Length > 10)
            {
                List<string> list = new List<string>();
                string[] files = Directory.GetFiles(repo_path, pdf_file);

                list.AddRange(files.ToList());

                return list.Count > 0 ? $"{AppSettings.reports["output-client"]}{file_name}/pdf/" : null;
            }
            else
            {
                throw new ApplicationException(
                    "No se puede realizar una busqueda de documentos con menos de 10 caracteres.");
            }
        }

        /// <summary>
        /// Obtiene la ruta del archivo desde el repositorio
        /// </summary>
        /// <param name="name_file">Nombre del archivo</param>
        /// <returns></returns>
        public static string getRepoDocumentPathGyt(string name_file)
        {
            string _REPO_PATH_PRS = AppSettings.reports["output-prs"];
            name_file = $"{name_file}.pdf";

            List<string> list = new List<string>();              
            string[] files = Directory.GetFiles(_REPO_PATH_PRS, name_file);
            //var arr = Queryable.Concat(files, files2).ToArray();
            list.AddRange(files.ToList());
            if (list.Count > 0)
            {
                return list.First().ToString();
            }
            else
            {
                throw new ApplicationException(
                    "No se puede realizar una busqueda de documentos con menos de 10 caracteres.");
            }
        }
    }
}
