﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACTools.strings
{
    public class Errors
    {
        public const string NO_PERSON = "La persona no existe";
        public const string NO_STUDENT = "El estudiante no existe";
        
        public const string NO_INFO_IN_PERIOD = "No hay datos de {0} del alumno para el período {1}";

        public const string HTTP_CLIENT = "Error {0} al conectar con {1}";

    }
}
