﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ACTools.Configuration;
using System.Drawing;

namespace ACTools.Request
{
    public class Requests
    {
        private WebRequest request;
        private Stream dataStream;
        private string status;
        public String Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        public string responseText;
        
        /// <summary>
        /// For sending HTTP requests and receiving HTTP responses from a resource identified by a URI. 
        /// </summary>
        private static readonly HttpClient Client = new HttpClient();

        public Requests(string url)
        {
            // Create a request using a URL that can receive a post.

            request = WebRequest.Create(url);
        }

        public Requests(string url, string method)
            : this(url)
        {



            if (method.Equals("GET") || method.Equals("POST"))
            {
                // Set the Method property of the request to POST.
                request.Method = method;
            }
            else
            {
                throw new Exception("Invalid Method Type");
            }
        }

        public Requests(string url, string method, string data)
            : this(url, method)
        {

            // Create POST data and convert it to a byte array.
            string postData = data;
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";

            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;

            // Get the request stream.
            dataStream = request.GetRequestStream();

            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);

            // Close the Stream object.
            dataStream.Close();

        }

        public string GetResponse()
        {
            // Get the original response.
            WebResponse response = null;
            try
            {
                response = request.GetResponse();
                this.Status = ((HttpWebResponse)response).StatusDescription;
            }
            catch
            {
                return null;
            }


            // Get the stream containing all content returned by the requested server.
            dataStream = response.GetResponseStream();

            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);

            // Read the content fully up to the end.
            string responseFromServer = reader.ReadToEnd();

            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();
            this.responseText = responseFromServer;

            return responseFromServer;
        }

        public JObject ToJson()
        {
            try
            {
                JObject data = JObject.Parse(this.responseText);
                return data;
            }
            catch
            {

            }
            return null;
        }

        /// <summary>
        /// Registro de nuevo email
        /// </summary>
        /// <param name="formUrlEncodedContent">Datos de la nueva cuenta a crear</param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> CreateNewEmailAsync(FormUrlEncodedContent formUrlEncodedContent)
        {
            Client.DefaultRequestHeaders.TryAddWithoutValidation("x-token", AppSettings.app_token);
            return await Client.PostAsync(AppSettings.create_email_api, formUrlEncodedContent);
        }
        
    }

}
