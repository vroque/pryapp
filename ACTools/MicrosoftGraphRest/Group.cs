﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ACTools.SuperString;
using Microsoft.Graph;

namespace ACTools.MicrosoftGraphRest
{
    public class Group
    {
        /// <summary>
        /// Crea un grupo
        /// </summary>
        /// <param name="groupName">Nombre del grupo</param>
        /// <param name="idProject">Id del proyecto</param>
        /// <param name="groupDescription">Descripción del grupo</param>
        /// <param name="ownersUri">Arreglo de los URI de los dueños</param>
        /// <param name="members">Arreglo de los URI de los integrantes</param>
        /// <returns></returns>
        public static async Task<string> Create(string groupName, int idProject, string groupDescription, string[] ownersUri, string[] members = null)
        {
            var graphClient = new GraphServiceClient(AuthProvider.Get());
            var mailNickname = $"proyectosuc-{groupName.GenerateSlug()}-{idProject.ToString()}";
            var group = new Microsoft.Graph.Group
            {
                Description = groupDescription,
                DisplayName = groupName,
                GroupTypes = new List<string>() { "Unified" },
                MailEnabled = true,
                MailNickname = mailNickname,
                SecurityEnabled = false,
                AdditionalData = new Dictionary<string, object>
                {
                    {"members@odata.bind", members },
                    {"owners@odata.bind", ownersUri}
                }
            };

            // Send the POST request to create group 
            group = await graphClient.Groups.Request().AddAsync(group);
            return group.Id;
        }
    }
}
