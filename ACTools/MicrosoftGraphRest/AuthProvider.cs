﻿using System.Web.Configuration;
using Microsoft.Graph.Auth;
using Microsoft.Identity.Client;

namespace ACTools.MicrosoftGraphRest
{
    public class AuthProvider
    {
        public static ClientCredentialProvider Get()
        {
            IConfidentialClientApplication confidentialClientApplication = ConfidentialClientApplicationBuilder
                .Create(WebConfigurationManager.AppSettings["mg-clientid"])
                .WithTenantId(WebConfigurationManager.AppSettings["mg-tenantid"])
                .WithClientSecret(WebConfigurationManager.AppSettings["mg-clientsecret"])
                .Build();

            return new ClientCredentialProvider(confidentialClientApplication);
        }
    }
}
