﻿using System;
using System.Threading.Tasks;
using ACTools.SuperString;
using Microsoft.Graph;

namespace ACTools.MicrosoftGraphRest
{
    public class User
    {
        public static async Task<string> GetUriUser(string userPrincipalName)
        {
            var graphClient = new GraphServiceClient(AuthProvider.Get());
            var user = await graphClient.Users[userPrincipalName].Request().GetAsync();
            return $"https://graph.microsoft.com/v1.0/users/{user.Id}";
        }

        public static async Task<bool> AddUser(string userPrincipalName, string groupId)
        {
            try
            {
                var graphClient = new GraphServiceClient(AuthProvider.Get());
                var userToAdd = await graphClient.Users[userPrincipalName.ToEmailUc()].Request().GetAsync();

                await graphClient.Groups[groupId].Members.References.Request().AddAsync(userToAdd);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            
        }
    }
}
