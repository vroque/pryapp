﻿using System.Threading.Tasks;
using Microsoft.Graph;

namespace ACTools.MicrosoftGraphRest
{
    public class Teams
    {
        /// <summary>
        /// Crea un teams asociado a un grupo
        /// </summary>
        /// <param name="groupId">Id del grupo</param>
        /// <returns></returns>
        public static async Task<string> Create(string groupId)
        {
            var graphClient = new GraphServiceClient(AuthProvider.Get());
            var team = new Team
            {
                MemberSettings = new TeamMemberSettings
                {
                    AllowCreateUpdateChannels = true,
                    AllowAddRemoveApps = true,
                    AllowCreateUpdateRemoveTabs = true,
                    AllowCreateUpdateRemoveConnectors = true,
                    ODataType = null
                },
                MessagingSettings = new TeamMessagingSettings
                {
                    AllowUserEditMessages = true,
                    AllowUserDeleteMessages = true,
                    AllowOwnerDeleteMessages = true,
                    ODataType = null
                },
                FunSettings = new TeamFunSettings
                {
                    AllowGiphy = true,
                    GiphyContentRating = GiphyRatingType.Strict,
                    ODataType = null
                },
                ODataType = null
            };

            var res = await graphClient.Groups[groupId].Team
                .Request()
                .PutAsync(team);
            return res.Id;
        }
    }
}
