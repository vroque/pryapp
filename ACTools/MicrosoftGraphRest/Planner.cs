﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graph;

namespace ACTools.MicrosoftGraphRest
{
    public class Planner
    {
        /// <summary>
        /// Crea un planner
        /// </summary>
        /// <param name="groupId">Id del grupo</param>
        /// <param name="title">Titulo del planner</param>
        /// <returns></returns>
        public static async Task<string> Create(string groupId, string title)
        {
            var graphClient = new GraphServiceClient(AuthProvider.Get());

            var plannerPlan = new PlannerPlan
            {
                Owner = groupId,
                Title = title
            };

            try
            {
                var plan = await graphClient.Planner.Plans
                    .Request()
                    .AddAsync(plannerPlan);
                return plan.Id;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
