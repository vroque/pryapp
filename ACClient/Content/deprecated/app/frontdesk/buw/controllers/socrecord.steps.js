'use strict';
/* jshint -W097 */
/* global angular, $, _, Promise, document, window */

angular.module('BUWModule')
  /**
    * Controllador para manejar los pasos
    * de la ficha socioeconomica v 3
    * @author Arturo Bolaños
    * @date 18/06/2016
  */
  .controller('socRecordStepCtrl',
  ['$scope', '$state', '$stateParams', 'CountryModel',
   'SRUbicationModel','SRAcademicModel','SRWorkingModel',
   'SRIncomeModel', 'SRExpensesModel','SRHomeModel',
   'SRComodityModel', 'SRHealthModel', 'SRPersonalModel',
   'SRFamiliarModel', 'SRFamiliarRelationModel',
  function ($scope, $state, $stateParams, CountryModel,
            SRUbicationModel, SRAcademicModel, SRWorkingModel,
            SRIncomeModel, SRExpensesModel, SRHomeModel,
            SRComodityModel, SRHealthModel, SRPersonalModel,
            SRFamiliarModel, SRFamiliarRelationModel)
    {

    if($scope.$parent.soc_record === undefined || !$scope.$parent.profile){
      $state.go('^');
      return 0;
    }
    $scope.mode = 'read';
    $scope.current_step = $stateParams.step;
    $scope.$parent.current_step = $stateParams.step;
    $scope.person_id = $scope.$parent.soc_record.personal.id;
    $scope.student_id = $scope.$parent.profile.student_id;
    $scope.current_year= new Date().getFullYear();

    $scope.step1 = {
      person: $scope.$parent.soc_record.personal,
      init: function(){
        $scope.o_person = this.person;
        $scope.o_person.country = $scope.o_person.country || '170'; // Perú
        $scope.o_person.country = ($scope.o_person.country === '' || $scope.o_person.country === '0')? '170':$scope.o_person.country; // Perú
        $scope.person = angular.copy($scope.o_person);


        CountryModel.query().$promise
        .then(
          function(data){
            $scope.countries = data.objects;
          }, function(err){}
        );
      },
      save: function(person){
        var data = new SRPersonalModel(person);
        // data.id = $scope.person_id;
        data.$save()
        .then(function(res)  {
          $scope.o_person = angular.copy($scope.person);
          $scope.$parent.soc_record.personal = person;
          $scope.mode = 'read';
        });
        //.catch(function(req) { console.log('error saving obj', req); })
        //.finally(function()  { console.log('always called'); });
      },
      cancel: function(person){
        $scope.person = angular.copy($scope.o_person);
        $scope.mode = 'read';
      }
    };
    $scope.step2 = {
      ubication: $scope.$parent.soc_record.ubication,
      init: function(){
        $scope.o_ubication = this.ubication;

        $scope.o_ubication.email = $scope.$parent.soc_record.personal.document_number + '@continental.edu.pe';
        $scope.o_ubication.origin_country = $scope.o_ubication.origin_country || '170'; // Perú
        $scope.o_ubication.recurrent_country = $scope.o_ubication.recurrent_country || '170'; // Perú
        $scope.ubication = angular.copy($scope.o_ubication);

        CountryModel.query().$promise
        .then(
          function(data){
            $scope.countries = data.objects;
          }, function(err){}
        );
      },
      save: function(ubic){
        var data = new SRUbicationModel(ubic);
        data.id = $scope.person_id;
        data.$save()
        .then(function(res)  {
          $scope.o_ubication = angular.copy($scope.ubication);
          $scope.$parent.soc_record.ubication = ubic;
          $scope.mode = 'read';
          $scope.$parent.updateAllPercent();
        });
        //.catch(function(req) { console.log('error saving obj', req); })
        //.finally(function()  { console.log('always called'); });

      },
      cancel: function(person){
        $scope.ubication = angular.copy($scope.o_ubication);
        $scope.mode = 'read';
      }
    };
    $scope.step3 = {
      academic: $scope.$parent.soc_record.academic,
      init: function(){
        $scope.o_academic = this.academic;
        $scope.academic = angular.copy($scope.o_academic);
        CountryModel.query().$promise
        .then(
          function(data){
            $scope.countries = data.objects;
          }, function(err){}
        );
      },
      save: function(acad){
        var data = new SRAcademicModel(acad);
        data.id = $scope.person_id;
        data.$save()
        .then(function(res)  {
          $scope.o_academic = angular.copy($scope.academic);
          $scope.$parent.soc_record.academic = acad;
          $scope.mode = 'read';
          $scope.$parent.updateAllPercent();
        });
      },
      cancel: function(person){
        $scope.academic = angular.copy($scope.o_academic);
        $scope.mode = 'read';
      }
    };
    $scope.step4 = {
      working: $scope.$parent.soc_record.working,
      init: function(){
        $scope.o_laboral = this.working;
        if($scope.o_laboral.category_dep == '1') $scope.o_laboral.category = '1';
        if($scope.o_laboral.category_ind == '1') $scope.o_laboral.category = '2';
        if($scope.o_laboral.category_cj == '1') $scope.o_laboral.category = '3';
        $scope.laboral = angular.copy($scope.o_laboral);
        CountryModel.query().$promise
        .then(
          function(data){
            $scope.countries = data.objects;
          }, function(err){}
        );
      },
      save: function(work){
        var data = new SRWorkingModel(work);
        data.id = $scope.person_id;
        data.$save()
        .then(function(res)  {
          $scope.o_laboral = angular.copy($scope.laboral);
          $scope.$parent.soc_record.working = work;
          $scope.mode = 'read';
          $scope.$parent.updateAllPercent();
        });
      },
      cancel: function(person){
        $scope.laboral = angular.copy($scope.o_laboral);
        $scope.mode = 'read';
      },
      chCatgory: function(cat){
        //solo puedo elegir uno
        $scope.laboral.category_dep = '0';
        $scope.laboral.category_ind = '0';
        $scope.laboral.category_cj = '0';
        switch (cat) {
          case '1':
            $scope.laboral.category_dep = '1';
            break;
          case '2':
            $scope.laboral.category_ind = '1';
            break;
          case '3':
            $scope.laboral.category_cj = '1';
            break;
        }
      }
    };
    $scope.step5 = {
      familiar: $scope.$parent.soc_record.familiar,
      relations: $scope.$parent.soc_record.relations,
      init: function(){
        $scope.o_familiar = this.familiar;
        if($scope.profile.modality != 'PGR'){
          $scope.o_familiar.father = '1';
          $scope.o_familiar.mother = '1';
        }
        $scope.familiar = angular.copy($scope.o_familiar);

        $scope.lives_with_list=['Solo','Padre','Madre','Cónyuge','Hermano(s) ', 'Hijo(s)','Otro familiar'];
        $scope.lives_with=[];
        for(var i in $scope.lives_with_list){
          if(this.inLivesWith($scope.lives_with_list[i]))
            $scope.lives_with.push($scope.lives_with_list[i]);
          else $scope.lives_with.push('0');
        }
      },
      save: function(familiar){
        var data = new SRFamiliarModel(familiar);
        data.lives_with = [];
        for(var i in $scope.lives_with){
          if($scope.lives_with[i] != '0')
            data.lives_with.push($scope.lives_with[i]);
        }
        data.id = $scope.person_id;
        data.$save()
        .then(function(res)  {
          $scope.o_familiar = angular.copy($scope.familiar);
          //this.familiar = familiar;
          familiar.lives_with=data.lives_with;
          $scope.$parent.soc_record.familiar = familiar;
          $scope.mode = 'read';
          $scope.$parent.updateAllPercent();
        });
        //.catch(function(req) { console.log('error saving obj', req); })
        //.finally(function()  { console.log('always called'); });
      },
      cancel: function(){
        $scope.familiar = angular.copy($scope.o_familiar);
        $scope.mode = 'read';
      },
      addFamily: function(family){
        var state = (family.situation == 'A')? 'socRecord.contributor': 'socRecord.dependent';
        $state.go(state,{familyid:'0000', relation: family.relation});
      },
      hasFamily: function(relation){
        for (var i in this.relations) {
          if(this.relations[i].relation == relation){
            return true;
          }
        }
        return false;
      },
      remFamily: function(family){
        for(var i in this.relations){
          if(this.relations[i].person_id == family.person_id){
            delete this.relations[i];
            family.id =  $scope.person_id;
            SRFamiliarRelationModel.remove({id: family.person_id}, function(d){
              $scope.$parent.updateAllPercent();
            });
            break;
          }
        }
      },
      inLivesWith: function(item){
        for(var k in $scope.familiar.lives_with){
          if($scope.familiar.lives_with[k] == item)
            return true;
        }
        return false;
      },
      checkLivesWith:function(index,item){
        if($scope.lives_with[index]==item && item == 'Solo'){
          $scope.lives_with=[];
          for(var i in $scope.lives_with_list){
            if($scope.lives_with_list[i]=='Solo')
              $scope.lives_with.push($scope.lives_with_list[i]);
            else $scope.lives_with.push('0');
          }
        }
        else if ($scope.lives_with[index]==item && item != 'Solo'){
          for(var j in $scope.lives_with_list){
            if($scope.lives_with_list[j]=='Solo')
              $scope.lives_with[j]='0';
          }

        }

      }

    };
    $scope.step6 = {
      init: function(){
        $scope.relation = $stateParams.relation;
      },
      save: function(){},
      cancel: function(){}
    };
    $scope.step7 = {
      familiar: $scope.$parent.soc_record.familiar,
      init: function(){

        //$scope.familiar=[{visibility:1}]
        //$scope.o_familiar = this.familiar;
        //$scope.familiar = angular.copy($scope.o_familiar);
        //console.log('init step7',$scope.familiar);
      },
      save: function(){
        //$scope.familiar.forEach(function(item){item.visibility=1});
        $scope.mode = 'read';
      },
      cancel: function(){
        $scope.mode = 'read';
      },
      edit:function () {
        //console.log('edit');
        $scope.mode = 'write';
        //console.log($scope.mode);
        //$scope.familiar.forEach(function(item){item.visibility=0});
        //$scope.familiar[$scope.dependents.length-1].visibility=1;
      }/*,
      newDependent:function(){
        $scope.dependents.forEach(function(item){item.visibility=0});
        $scope.dependents.push({visibility:1});
      },
      changeVisibility:function(index){
        $scope.dependents.forEach(function(item){item.visibility=0});
        $scope.dependents[index].visibility=1;
      }*/
    };
    $scope.step8 = {
      income: $scope.$parent.soc_record.income,
      init: function(){
        $scope.o_income = this.income;
        $scope.income = angular.copy($scope.o_income);
        for(var key in $scope.income){
          if($scope.income[key] === 0)
            $scope.income[key] = null;
        }
      },
      save: function(income){
        var data = new SRIncomeModel(income);
        data.id = $scope.person_id;
        data.$save()
        .then(function(res)  {
          $scope.o_income = angular.copy($scope.income);
          $scope.$parent.soc_record.income = income;
          $scope.mode = 'read';
          $scope.$parent.updateAllPercent();
        });
      },
      cancel: function(person){
        $scope.income = angular.copy($scope.o_income);
        $scope.mode = 'read';
      },
      total: function(income){
        var total = 0;
        for(var key in income){
          total += parseInt(income[key]) || 0;
        }
        return total;
      }
    };
    $scope.step9 = {
      expenses: $scope.$parent.soc_record.expenses,
      init: function(){
        $scope.o_expenses = this.expenses;
        $scope.expenses = angular.copy($scope.o_expenses);
        for(var key in $scope.expenses){
          if($scope.expenses[key] === 0)
            $scope.expenses[key] = null;
        }
        $scope.total_income = $scope.step8.total($scope.step8.income);
      },
      save: function(expenses){
        var data = new SRExpensesModel(expenses);
        data.id = $scope.person_id;
        data.$save()
        .then(function(res)  {
          $scope.o_expenses = angular.copy($scope.expenses);
          $scope.$parent.soc_record.expenses = expenses;
          $scope.mode = 'read';
          $scope.$parent.updateAllPercent();
        });
      },
      cancel: function(person){
        $scope.expenses = angular.copy($scope.o_expenses);
        $scope.mode = 'read';
      },
      total: function(expenses){
        var total = 0;
        for(var key in expenses){
          total += parseInt(expenses[key]) || 0;
        }
        return total;
      }
    };
    $scope.step10 = {
      home: $scope.$parent.soc_record.home,
      init: function(){
        $scope.o_home = this.home;
        $scope.home = angular.copy($scope.o_home);
      },
      save: function(home){
        var data = new SRHomeModel(home);
        data.id = $scope.person_id;
        data.$save()
        .then(function(res)  {
          $scope.o_home = angular.copy($scope.home);
          $scope.$parent.soc_record.home = home;
          $scope.mode = 'read';
          $scope.$parent.updateAllPercent();
        });

      },
      cancel: function(person){
        $scope.home = angular.copy($scope.o_home);
        $scope.mode = 'read';
      }
    };
    $scope.step11 = {
      comodity: $scope.$parent.soc_record.comodity,
      init: function(){
        $scope.o_comodity = this.comodity;
        $scope.comodity = angular.copy($scope.o_comodity);
        $scope.services_list = ['Agua','Desagüe', 'Luz', 'Teléfono', 'Internet', 'cable'];
        $scope.appliances_list = ['Cocina','Refrigeradora', 'Televisión', 'Equipo de sonido', 'DVD / Blu-ray', 'Celular / Teléfono móvil', 'Lavadora','Computadora / laptop', 'Aspiradora', 'Horno microondas', 'Servicio doméstico'];
        $scope.services = [] ;
        for(var i in $scope.services_list){
          if(this.inServices($scope.services_list[i]))
            $scope.services.push($scope.services_list[i]);
          else $scope.services.push('0');
        }
        $scope.appliances = [];
        for(var j in $scope.appliances_list){
          if(this.inAppliances($scope.appliances_list[j]))
            $scope.appliances.push($scope.appliances_list[j]);
          else $scope.appliances.push('0');
        }
      },
      save: function(comodity){
        var data = new SRComodityModel();
        data.appliances = [];
        data.services = [];
        for(var i in $scope.appliances){
          if($scope.appliances[i] != '0')
            data.appliances.push($scope.appliances[i]);
        }
        for(var k in $scope.services){
          if($scope.services[k] != '0')
            data.services.push($scope.services[k]);
        }
        data.id = $scope.person_id;
        data.$save()
        .then(function(res)  {
          $scope.o_comodity = data;
          $scope.comodity = data;
          $scope.$parent.soc_record.comodity = data;
          $scope.mode = 'read';
          $scope.$parent.updateAllPercent();
        });
      },
      cancel: function(person){
        $scope.comodity = angular.copy($scope.o_comodity);
        $scope.mode = 'read';
      },
      inAppliances: function(item){
        for(var k in $scope.comodity.appliances){
          if($scope.comodity.appliances[k] == item)
            return true;
        }
        return false;
      },
      inServices: function(item){
        for(var k in $scope.comodity.services){
          if($scope.comodity.services[k] == item)
            return true;
        }
        return false;
      },
      countServices: function(){
        var c = 0;
        for(var k in $scope.services){
          if($scope.services[k] != '0')
            c++;
        }
        return c;
      },
      countAppliances: function(){
        var c = 0;
        for(var k in $scope.appliances){
          if($scope.appliances[k] != '0')
            c++;
        }
        return c;
      }
    };
    $scope.step12 = {
      health: $scope.$parent.soc_record.health,
      init: function(){
        $scope.o_health = this.health;
        $scope.o_health.familiar_disease = ($scope.o_health.familiar_disease === null)?[]:$scope.o_health.familiar_disease;
        $scope.health = angular.copy($scope.o_health);
        $scope.familiars = $scope.$parent.soc_record.relations;
      },
      save: function (health) {
        var data = new SRHealthModel(health);
        data.id = $scope.person_id;
        data.$save()
        .then(function(res)  {
          // $scope.health = data;
          $scope.$parent.soc_record.health = health;
          $scope.mode = 'read';
          $scope.$parent.updateAllPercent();
        });
      },
      cancel: function(person){
        $scope.health = angular.copy($scope.o_health);
        $scope.mode = 'read';
      },
      addFam: function (fam) {
        var k = 0;
        $scope.health.familiar_disease = $scope.health.familiar_disease || [];
        for(var j in $scope.health.familiar_disease){
          if ($scope.health.familiar_disease[j].person_id == fam.person_id) {
              k++;
              $scope.health.familiar_disease[j].disease = fam.disease;
          }
        }
        if(k === 0 ){
          $scope.health.familiar_disease.push(angular.copy(fam));
        }
        this.save($scope.health);
      },
      remFam: function(fam){
        var k = -1;
        for(var j in $scope.health.familiar_disease){
          if($scope.health.familiar_disease[j].person_id == fam.person_id ){
            k = j;
            break;
          }
        }
        if(k >= 0)
          $scope.health.familiar_disease.splice(k,1);
        this.save($scope.health);
      },
      infoFam: function(fam){
        for(var i in $scope.familiars){
          if($scope.familiars[i].person_id == fam.person_id){
            return $scope.familiars[i].father_name + " " + $scope.familiars[i].father_name + ", "+ $scope.familiars[i].first_name;
          }
        }
        return 'Nombre no asignado';
      }
    };

  }])
  /**
    * Controllador para manejar el paso 6
    * de la ficha socioeconomica v 3
    * @author Arturo Bolaños
    * @date 18/06/2016
  */
  .controller('socRecordContribCtrl',
  ['$scope', '$state', '$stateParams', 'SRFamiliarRelationModel', 'CountryModel',
  function ($scope, $state, $stateParams, SRFamiliarRelationModel, CountryModel) {
    if(!$scope.$parent.soc_record || !$scope.$parent.profile){
      $state.go('^');
      return 0;
    }

    $scope.person_id = $scope.$parent.soc_record.personal.id;
    $scope.student_id = $scope.$parent.profile.student_id;
    $scope.mode = 'read';
    $scope.relation = $stateParams.relation;
    $scope.init = function(){
      document.getElementById("aportant-btn-edit").focus();
      $scope.mode = 'read';
      $scope.o_aportant = {};
      $scope.aportant = {};
      $scope.load($stateParams.familyid)
      .then(function(data){
        $scope.o_aportant = data;
        $scope.o_aportant.birth_country = $scope.o_aportant.birth_country || '170'; // Perú
        $scope.o_aportant.birth_country = ($scope.o_aportant.birth_country === '')? '170':$scope.o_aportant.birth_country; // Perú
        if($scope.o_aportant.category_dep == '1') $scope.o_aportant.category = '1';
        if($scope.o_aportant.category_ind == '1') $scope.o_aportant.category = '2';
        if($scope.o_aportant.category_cj == '1') $scope.o_aportant.category = '3';
        $scope.aportant = angular.copy($scope.o_aportant);

        $scope.$apply();
      });
      CountryModel.query().$promise
      .then(
        function(data){
          $scope.countries = data.objects;
        }, function(err){}
      );
    };
    $scope.save = function(familiar){
      var data = new SRFamiliarRelationModel(familiar);
      data.id = $scope.person_id;
      data.$save()
      .then(function(res)  {
        familiar.person_id = res.person_id;
        $scope.o_aportant = angular.copy($scope.aportant);
        $scope.updateList(familiar);
        $scope.mode = 'read';
        $scope.$parent.updateAllPercent();
      });
    };
    $scope.cancel = function(){
      $scope.aportant = angular.copy($scope.o_aportant);
      $scope.mode = 'read';
    };
    $scope.chCatgory = function(cat){
      $scope.aportant.category_dep = '0';
      $scope.aportant.category_ind = '0';
      $scope.aportant.category_cj = '0';
      switch (cat) {
        case '1':
          $scope.aportant.category_dep = '1';break;
        case '2':
          $scope.aportant.category_ind = '1';break;
        case '3':
          $scope.aportant.category_cj = '1';break;
      }
    };
    $scope.updateList = function(familiar){
      for(var key in $scope.$parent.soc_record.relations){
        if($scope.$parent.soc_record.relations[key].person_id == familiar.person_id){
          $scope.$parent.soc_record.relations[key] = familiar;
          return 1;
        }
      }
      $scope.$parent.soc_record.relations.push(familiar);
      return 1;
    };
    $scope.load = function(familyid) {
      // Return a new promise.
      return new Promise(function(resolve, reject) {
        if(familyid === '0000'){
          resolve({ relation: $stateParams.relation, economic_type: 'A' });
        }else{
          SRFamiliarRelationModel.get({id:$stateParams.familyid }).$promise
          .then(function(data){
            resolve(data);
            $scope.$apply();
          })
          .catch(function(err){
            reject(err);
          });
        }

      });
    };
    $scope.goBack = function(){
      window.history.back();
    };
  }])
  /**
    * Controllador para manejar el paso 7
    * de la ficha socioeconomica v 3
    * @author Arturo Bolaños
    * @date 18/06/2016
  */
  .controller('socRecordDepenCtrl',
  ['$scope', '$state', '$stateParams', 'SRFamiliarRelationModel',
  function ($scope, $state, $stateParams, SRFamiliarRelationModel) {
    if(!$scope.$parent.soc_record || !$scope.$parent.profile){
      $state.go('^');
      return 0;
    }
    $scope.mode = 'read';
    $scope.person_id = $scope.$parent.soc_record.personal.id;
    // $scope.student_id = $scope.$parent.profile.student_id;
    $scope.init = function(){
      document.getElementById("dependent-btn-edit").focus();
      if($stateParams.familyid === '0000'){
        $scope.o_dependent = { relation: $stateParams.relation, economic_type: 'D' };
        $scope.dependent = angular.copy($scope.o_dependent);
      }
      else{
        SRFamiliarRelationModel.get({id:$stateParams.familyid }).$promise
        .then(function(data){
          $scope.o_dependent = data;
          $scope.dependent = angular.copy($scope.o_dependent);
        });
      }
    };
    $scope.cancel = function(){
      $scope.dependent = angular.copy($scope.o_dependent);
      $scope.mode = 'read';
    };
    $scope.save = function(familiar){
      var data = new SRFamiliarRelationModel(familiar);
      data.id = $scope.person_id;
      data.$save()
      .then(function(res)  {
        familiar.person_id = res.person_id;
        $scope.o_dependent = angular.copy($scope.dependent);
        $scope.updateList(familiar);
        $scope.mode = 'read';
        $scope.$parent.updateAllPercent();
      });
      //.catch(function(req) { console.log('error saving obj', req); })
      //.finally(function()  { console.log('always called'); });
    };
    $scope.updateList = function(familiar){
      for(var key in $scope.$parent.soc_record.relations){
        if($scope.$parent.soc_record.relations[key].person_id == familiar.person_id){
          $scope.$parent.soc_record.relations[key] = familiar;
          return 1;
        }
      }
      $scope.$parent.soc_record.relations.push(familiar);
      return 1;
    };
    $scope.goBack = function(){
      window.history.back();
    };
  }]);
