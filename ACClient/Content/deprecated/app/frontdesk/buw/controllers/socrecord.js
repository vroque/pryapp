'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('BUWModule')
  /**
    * Controllador para manejar
    * la ficha socioeconomica v 3
    * @author Arturo Bolaños
    * @date 25/05/2016
  */
  .controller('socRecordCtrl',
  ['$scope', '$state', '$stateParams', 'SocioeconomicRecordModel', 'AcademicProfileModel', 'CountryModel', 'SRAbstractRelationModel',
  function ($scope, $state, $stateParams, SocioeconomicRecordModel, AcademicProfileModel, CountryModel, SRAbstractRelationModel) {
      console.log('run buw module');
      $scope.init = function () {
          $scope.current_step = 0;
          $scope.steps = [
            { step: 1, name: 'Datos de Identificación', short_name: 'personal', icon: 'user', percent: 0, errors: [] },
            { step: 2, name: 'Datos de Lugar de Residencia Actual', short_name: 'residencia', icon: 'map-signs', percent: 0, errors: [] },
            { step: 3, name: 'Datos Académicos', short_name: 'académico', icon: 'graduation-cap', percent: 0, errors: [] },
            { step: 4, name: 'Datos Laborales', short_name: 'laboral', icon: 'briefcase', percent: 0, errors: [] },
            { step: 5, name: 'Composición Familiar', short_name: 'familiar', icon: 'users', percent: 0, errors: [] },
            /*{step: 6, name: '', short_name: '', icon: '', percent: 0, errors: [] },
            {step: 7, name: '', short_name: '', icon: '', percent: 0, errors: [] },*/
            { step: 8, name: 'Otros Ingresos Mensuales', short_name: 'ingresos', icon: 'eur', percent: 0, errors: [] },
            { step: 9, name: 'Egresos Mensuales Familiares', short_name: 'egresos', icon: 'cc-visa', percent: 0, errors: [] },
            { step: 10, name: 'Datos de Vivienda', short_name: 'vivienda', icon: 'home', percent: 0, errors: [] },
            { step: 11, name: 'Comodidades del Hogar ', short_name: 'servicios', icon: 'television', percent: 0, errors: [] },
            { step: 12, name: 'Datos de Salud Familiar', short_name: 'salud', icon: 'heartbeat', percent: 0, errors: [] },
          ];
          $scope.initNav(); // inicializando nav
          $scope.profile = window.profile;
          switch ($scope.profile.department) {
              case ("UPGT"): $scope.profile.modality = "PGT"; break;
              case ("UVIR"): $scope.profile.modality = "VIR"; break;
              case ("UREG"):
              default: $scope.profile.modality = "PGR";
          }

          SocioeconomicRecordModel.get().$promise
          .then(
            function (data) {
                $scope.soc_record = data;
                $scope.soc_record.personal.id = $scope.soc_record.personal.id;
            },
            function (error) { }
          );
      };

      $scope.goStep = function (step) {
          $state.go('.step', { 'step': step });
      };

      $scope.initNav = function () {
          // manejando ficha navigation
          var con_w = angular.element('#ficha-nav .ficha-res-container').width();
          var list_w = angular.element('#ficha-nav .ficha-res-container ul.ficha-res').width();
          //flecha derecha
          if (list_w - con_w > 0) {
              angular.element('#ficha-nav .anext > span').css('transform', 'rotate(45deg)');
          }
          //eventos de direccionales
          angular.element('#ficha-nav .abefore').on('click', function (e) {
              var left = angular.element('#ficha-nav .ficha-res-container ul.ficha-res').css('left');
              left = parseInt(left);
              left += 70;
              if (left >= 0) {
                  left = 0;
                  angular.element('#ficha-nav .abefore > span').css('transform', 'rotate(0deg)');
              }
              else {
                  angular.element('#ficha-nav .anext > span').css('transform', 'rotate(45deg)');
              }
              angular.element('#ficha-nav .ficha-res-container ul.ficha-res').css('left', left + 'px');
          });
          angular.element('#ficha-nav .anext').on('click', function (e) {
              var left = angular.element('#ficha-nav .ficha-res-container ul.ficha-res').css('left');
              left = parseInt(left);
              left -= 70;
              if (left <= con_w - list_w) {
                  left = (Math.floor((con_w - list_w) / 70)) * 70;
                  angular.element('#ficha-nav .anext > span').css('transform', 'rotate(0deg)');
              }
              else {
                  angular.element('#ficha-nav .abefore > span').css('transform', 'rotate(45deg)');
              }
              angular.element('#ficha-nav .ficha-res-container ul.ficha-res').css('left', left + 'px');
          });
      };
      $scope.nextStep = function (step) {
          var n = parseInt(step);
          if (n === 5) return 8;
          n++;
          return n;
      };
      $scope.beforeStep = function (step) {
          var n = parseInt(step);
          if (n === 8) return 5;
          n--;
          return n;
      };
      $scope.iconOf = function (step) {
          for (var i in $scope.steps) {
              if ($scope.steps[i].step == step) {
                  return $scope.steps[i].icon;
              }
          }
          return 'home';
      };
      $scope.verifyStep = function (step) {
          var percent = 0;
          var errors = [];
          switch (step) {
              case 1:
                  var personal = $scope.soc_record.personal;
                  percent = 50; //empieza avanzado por la data actual
                  if (personal.gender === '' || personal.gender === null)
                      errors.push('Genero');
                  if (personal.birthdate === '' || personal.birthdate === null)
                      errors.push('Fecha de Nacimiento');
                  // Virtual/Regular
                  if ($scope.profile.modality != 'PGR') {
                      if (personal.country === '' || personal.country === null)
                          errors.push('Pais');
                  }
                  else {
                      if (personal.ubigeo === '' || personal.ubigeo === '0' || personal.ubigeo.length < '6' || personal.ubigeo === null)
                          errors.push('Lugar de Nacimiento');
                  }
                  break;
              case 2:
                  var ubication = $scope.soc_record.ubication;
                  if (ubication.origin_address === '' || ubication.origin_address === null)
                      errors.push('origin_address');
                  if (ubication.referency === '' || ubication.referency === null)
                      errors.push('referencyr');
                  if (ubication.reference_phone === '' || ubication.reference_phone === null)
                      errors.push('reference_phone');
                  if (ubication.reference_person === '' || ubication.reference_person === null)
                      errors.push('reference_person');
                  if (ubication.celphone === '' || ubication.celphone === null)
                      errors.push('celphoneor');
                  if (ubication.email === '' || ubication.email === null)
                      errors.push('email');
                      //if(ubication.has_recurrent === '' || ubication.has_recurrent === null)
                      //  errors.push('has_recurrent');
                      // Campos opcionales segun condicion
                  else if (ubication.has_recurrent === '1') {
                      if (ubication.recurrent_country === '' || ubication.recurrent_country === null)
                          errors.push('recurrent_country');
                      if (ubication.recurrent_ubigeo === '' || ubication.recurrent_ubigeo === null)
                          errors.push('recurrent_ubigeo');
                      if (ubication.recurrent_address === '' || ubication.recurrent_address === null)
                          errors.push('recurrent_address');
                      if (ubication.recurrent_referency === '' || ubication.recurrent_referency === null)
                          errors.push('recurrent_referency');
                  }
                  // para virtual
                  if ($scope.profile.modality === 'VIR') {
                      if (ubication.origin_country === '' || ubication.origin_country === null)
                          errors.push('origin_country');
                  }
                  else {
                      if (ubication.origin_ubigeo === '' || ubication.origin_ubigeo === null)
                          errors.push('origin_ubigeo');
                  }
                  break;
              case 3:
                  var academic = $scope.soc_record.academic;
                  if (academic.end_year === 0 || academic.end_year === null)
                      errors.push('end_year');
                  if (academic.last_grade === '' || academic.last_grade === null)
                      errors.push('last_grade');
                  
                  if ($scope.profile.modality === 'PGR') {
                      if (academic.first_in_college === '' || academic.first_in_college === null)
                          errors.push('first_in_college');
                      if (!(academic.last_grade === '1' || academic.last_grade === '5')) {
                          if (academic.college_mode === '' || academic.college_mode === null)
                              errors.push('college_mode');
                          if (academic.college_pension === null)
                              errors.push('college_pension');
                          if (academic.college_name === '' || academic.college_name === null)
                              errors.push('college_name');
                          if (academic.college_mode === '' || academic.college_mode === null)
                              errors.push('college_mode');
                          if (academic.college_career === '' || academic.college_career === null)
                              errors.push('college_career');
                          if (academic.college_duration === 0 || academic.college_duration === null)
                              errors.push('college_duration');
                      }
                  }
                  break;
              case 4:
                  var working = $scope.soc_record.working;
                  if (working.has === '' || working.has === null)
                      errors.push('has');
                      // si tiene trabajo
                  else if (working.has === '1') {
                      //if is dependent
                      if (working.category_dep === '' || working.category_dep === null)
                          errors.push('category_dep');
                      else if (working.category_dep === '1') {
                          if (working.dep_company === '' || working.dep_company === null)
                              errors.push('dep_company');
                          if (working.dep_type === '' || working.dep_type === null)
                              errors.push('dep_type');
                          if (working.dep_activity === '' || working.dep_activity === null)
                              errors.push('dep_activity');
                          if (working.dep_service_time === '' || working.dep_service_time === null)
                              errors.push('dep_service_time');
                          if (working.dep_position === '' || working.dep_position === null)
                              errors.push('dep_position');
                      }
                      // if is independet
                      if (working.category_ind === '' || working.category_ind === null)
                          errors.push('category_ind');
                      else if (working.category_ind === '1') {
                          if (working.ind_actual_work === '' || working.ind_actual_work === null)
                              errors.push('ind_actual_work');
                          if (working.ind_activity === '' || working.ind_activity === null)
                              errors.push('ind_activity');
                          if (working.ind_has_ruc === '' || working.ind_has_ruc === null)
                              errors.push('ind_has_ruc');
                          if (working.ind_has_ruc === '1') {
                              if (working.ind_ruc === '' || working.ind_ruc === null)
                                  errors.push('ind_ruc');
                          }
                          if (working.ind_workers === '' || working.ind_workers === null)
                              errors.push('ind_workers');
                      }
                      // if is cesante o jubilado
                      if (working.category_cj === '' || working.category_cj === null)
                          errors.push('category_cj');
                      else if (working.category_cj === '1') {
                          //if( working.cj_cesante === '' || working.cj_cesante === null)
                          //  errors.push('cj_cesante');
                          if (working.cj_company === '' || working.cj_company === null)
                              errors.push('cj_company');
                          if (working.cj_service_time === '' || working.cj_service_time === null)
                              errors.push('cj_service_time');
                      }
                  }
                  //if has other work
                  if (working.has_complementary === '' || working.has_complementary === null)
                      errors.push('has_complementary');
                  else if (working.has_complementary === '1') {
                      if (working.com_name === '' || working.com_name === null)
                          errors.push('com_name');
                  }
                  break;
              case 5:
                  var familiar = $scope.soc_record.familiar;
                  //errors.push('not yet implement');

                  if (familiar.lives_with === '' || familiar.lives_with === null)
                      errors.push('lives_with');

                  if ($scope.profile.modality === 'PGR') {
                      if (familiar.father === '1'  || familiar.mother === '1')
                          if (familiar.parent_status === '' || familiar.parent_status === null)
                              errors.push('parent_status');
                      if (familiar.head_family === '' || familiar.head_family === null)
                          errors.push('head_family');
                      else if (familiar.head_family === '6') {
                          if (familiar.head_family_other === '' || familiar.head_family_other === null)
                              errors.push('head_family_other');
                      }
                      if (familiar.economic_status === '' || familiar.economic_status === null)
                          errors.push('economic_status');
                      else if (familiar.economic_status === '2') {
                          if (familiar.independet_support === '' || familiar.independet_support === null)
                              errors.push('independet_support');
                      }
                  }
                  // Familiares
                  if (familiar.independet_support == '1' || familiar.dependent_support == '1' || familiar.economic_status == '1') {
                      //verificar por lo menos un familiar
                      if ($scope.soc_record.relations.length <= 0) {
                          errors.push('familiar relations');
                      }
                  }
                  break;
              case 8:
                  var income = $scope.soc_record.income;
                  var s = 0;
                  for (var i in income) {
                      if (income[i] !== null)
                          s += income[i];
                  }
                  if (s <= 0) errors.push('por lo menos rellenar 1');
                  break;
              case 9:
                  var expenses = $scope.soc_record.expenses;
                  if (expenses.food <= 0 || expenses.food === null) errors.push('food');
                  if (expenses.services <= 0 || expenses.services === null) errors.push('services');
                  if (expenses.conti <= 0 || expenses.conti === null) errors.push('conti');
                  if (expenses.materials <= 0 || expenses.materials === null) errors.push('materials');
                  break;
              case 10:
                  var home = $scope.soc_record.home;
                  if (home.home_type === '' || home.home_type === null) errors.push('home_type');
                  if (home.home_status === '' || home.home_status === null) errors.push('home_status');
                  if (home.wall_material === '' || home.wall_material === null) errors.push('wall_material');
                  if (home.roof_material === '' || home.roof_material === null) errors.push('roof_material');
                  if (home.floor_material === '' || home.floor_material === null) errors.push('floor_material');
                  break;
              case 11:
                  var comodity = $scope.soc_record.comodity;
                  if (comodity.appliances === null || comodity.appliances.length <= 0) errors.push('appliances');
                  if (comodity.services === null || comodity.services.length <= 0) errors.push('services');
                  break;
              case 12:
                  var health = $scope.soc_record.health;
                  if (health.hospital === '' || health.hospital === null)
                      errors.push('hospital');
                  else if (health.hospital === '5') {
                      if (health.other === '' || health.other === null)
                          errors.push('other');
                  }
                  if (health.disability === '' || health.disability === null)
                      errors.push('disability');
                  else if (health.disability === '1') {
                      if (health.disability_type === '' || health.disability_type === null)
                          errors.push('disability_type');
                  }
                  if (health.relative_disabled === '' || health.relative_disabled === null)
                      errors.push('relative_disabled');
                  break;
              default:
                  errors.push('modulo invalido de la ficha');
                  percent = 0;
          }

          //update and return
          if (errors.length === 0)
              percent = 100;
          $scope.updateStepPercent(step, percent);

          return errors;
      };
      $scope.updateStepPercent = function (step, percent) {
          for (var i in $scope.steps) {
              if ($scope.steps[i].step == step) {
                  $scope.steps[i].percent = percent;
              }
          }
      };
      $scope.verifyTotal = function () {
          var s = 0;
          for (var i in $scope.steps) {
              s += $scope.steps[i].percent;
          }
          s = s / 10.0;

          return s.toFixed(2);
      };
      $scope.updateAllPercent = function () {
          //alert('update all percent');
          if ($scope.soc_record !== undefined) {
              for (var i in $scope.steps) {
                  $scope.steps[i].errors = $scope.verifyStep($scope.steps[i].step);
              }
          }
          var total = $scope.verifyTotal();
          if (total > 0) {
              var abs = new SRAbstractRelationModel({
                  total_percent: parseInt(total),
                  completes_module: '',
                  missing_modules: ''
              });
              abs.$save();
          }
      };
      /**
       * Supervisar cambios en soc record para modificar
       * los porcentajes de avance
       */
      $scope.$watch("soc_record", function (value) {
          $scope.updateAllPercent();
      });
  }
  ])
.directive('postRender', ['$timeout', function ($timeout) {
    var def = {
        restrict: 'A',
        terminal: true,
        transclude: true,
        link: function (scope, element, attrs) {
            $timeout(scope.updateStepPercent, 0);  //Calling a scoped method
        }
    };
    return def;
}]);
