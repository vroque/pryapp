'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('BUWModule')
  /*
    * Controllador principal para buw
    * @author Arturo Bolaños
    * @date 08/09/2016
  */
  .controller('buwCtrl', ['$scope',
    function ($scope) {
      $scope.init = function () {
        //console.log('init ficha');
      };
    }
  ]);
