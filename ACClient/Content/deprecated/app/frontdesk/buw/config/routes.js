'use strict';
/* jshint -W097 */
/* global angular, require, window */

angular.module('centro-atencion')
.config(['$ocLazyLoadProvider', '$stateProvider', '$urlRouterProvider',
  function ($ocLazyLoadProvider, $stateProvider, $urlRouterProvider) {
    console.log('startt route (2)');
    $ocLazyLoadProvider.config({ asyncLoader: require });
    $urlRouterProvider.otherwise('/ficha-socieconomica');
    $stateProvider
    .state('socRecord', {
      url: '/ficha-socieconomica',
      controller: 'socRecordCtrl',
      templateUrl: window.app_base + 'buw/views/socrecord.html',
      resolve: {
        load: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'BUWModule',
            serie: true, // used for synchronous load chart scripts
            insertBefore: '#ngInsertBefore',
            files: [
              window.app_base + 'buw/module.js',
              window.app_base + 'buw/controllers/socrecord.js',
              window.app_base + 'buw/controllers/socrecord.steps.js',
              window.app_base + 'buw/services/profile.js',
              window.app_base + 'buw/services/socieconomic.js',
            ]
          });
        }]
      }
    })
    .state('socRecord.step', {
      url: '/paso/:step/',
      controller: 'socRecordStepCtrl',
      //templateUrl: window.app_base + 'buw/views/socrecord/step1.html'
      templateUrl: function ($stateParams){
        return window.app_base + 'buw/views/socrecord/step' + $stateParams.step + '.html';
      }
    })
    .state('socRecord.contributor', {
      url: '/aportante/:relation/:familyid/',
      controller: 'socRecordContribCtrl',
      //templateUrl: window.app_base + 'buw/views/socrecord/step1.html'
      templateUrl: function ($stateParams){
        return window.app_base + 'buw/views/socrecord/step6.html';
      }
    })
    .state('socRecord.dependent', {
      url: '/dependiente/:familyid/:relation/',
      controller: 'socRecordDepenCtrl',
      templateUrl: window.app_base + 'buw/views/socrecord/step7.html'
    });

  }
]);
