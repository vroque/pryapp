'use strict';
/* jshint -W097 */
/* global angular,  window */

angular.module('BUWModule')
/**
* Modelo para Ficha socieconomica
* Obtiene toda la ficha socieconomica
* del alumno logidado
* o del codigo de estudiante que se envie
* @author Arturo Bolaños
* @date 17/06/2016
* @version 1.0
* @param id string ID del alumno del cual obtener la ficha
*/
.factory('SocioeconomicRecordModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SocioeconomicRecord/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de Datos personales
 * @author Arturo Bolaños
 * @date 28/06/2016
 * @version 1.0
*/
.factory('SRPersonalModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRPersonal/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de Datos de ubicación
 * @author Arturo Bolaños
 * @date 17/06/2016
 * @version 1.0
*/
.factory('SRUbicationModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRUbication/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de Datos de ACademico
 * @author Arturo Bolaños
 * @date 17/06/2016
 * @version 1.0
*/
.factory('SRAcademicModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRAcademic/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de Datos laborales
 * @author Arturo Bolaños
 * @date 20/06/2016
 * @version 1.0
*/
.factory('SRWorkingModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRWorking/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de Datos para Ingresos familiares
 * @author Arturo Bolaños
 * @date 20/06/2016
 * @version 1.0
*/
.factory('SRIncomeModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRIncome/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de Datos para Egresos familiares
 * @author Arturo Bolaños
 * @date 21/06/2016
 * @version 1.0
*/
.factory('SRExpensesModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRExpenses/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de Datos para Viviensa
 * @author Arturo Bolaños
 * @date 21/06/2016
 * @version 1.0
*/
.factory('SRHomeModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRHome/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de Datos para Comodidaes del hogar
 * @author Arturo Bolaños
 * @date 21/06/2016
 * @version 1.0
*/
.factory('SRComodityModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRComodity/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de Datos de salud
 * @author Arturo Bolaños
 * @date 20/06/2016
 * @version 1.0
*/
.factory('SRHealthModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRHealth/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de Datos de familiares
 * @author Arturo Bolaños
 * @date 20/06/2016
 * @version 1.0
*/
.factory('SRFamiliarModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRFamiliar/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de Datos de relaciones familiares
 * @author Arturo Bolaños
 * @date 20/06/2016
 * @version 1.0
*/
.factory('SRFamiliarRelationModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRFamiliarRelation/:id', {id: '@id'});
  }
])
/**
 * Modelo para Ficha socieconomica
 * modulo de resumen de ficha
 * @author Arturo Bolaños
 * @date 15/08/2016
 * @version 1.0
*/
.factory('SRAbstractRelationModel', ['$resource',
  function ($resource) {
    return $resource(window.url_base + 'api/personal/SRAbstract/:id', {id: '@id'});
  }
]);
