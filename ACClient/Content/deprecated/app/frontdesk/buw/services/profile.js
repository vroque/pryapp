'use strict';
/* jshint -W097 */
/* global angular, api_base, window */

angular.module('BUWModule')
.factory('AcademicProfileModel', ['$resource', '$rootScope',
  function ($resource, $rootScope) {
    return $resource(
      window.url_base + 'api/academic/profile/:id', {id: '@id'}
    );
  }
]);
