﻿'use strict';
/* jshint -W097 */
/* global angular, require, window */
angular.module('centro-atencion')
.config(['$ocLazyLoadProvider', '$stateProvider', '$urlRouterProvider',
  function ($ocLazyLoadProvider, $stateProvider, $urlRouterProvider) {
  	$ocLazyLoadProvider.config({ asyncLoader: require });
  	$urlRouterProvider.otherwise('../');
  	$stateProvider
    .state('ReportApplicationForStudent', {
    	url: '/report/applications/for/student/',
    	controller: 'reportageCtrl',
    	templateUrl: window.app_base + 'views/reportage.html',
    	resolve: {
    		load: function ($ocLazyLoad) {
    			return $ocLazyLoad.load({
    				name: 'CAUModule',
    				files: [
					  window.app_base + 'module.js',
					  window.app_base + 'controllers/reportage.js'
    				]
    			});
    		}
    	}
    })
  }
]);