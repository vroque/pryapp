'use strict';
/* jshint -W097 */
/* global angular, window, _ */

angular.module('centro-atencion')
  .controller('authCtrl',['$scope',
  function($scope){

  }
])
.controller('sidebarCtrl',['$scope',
  function($scope){
  	$scope.init = function() {
  	    $scope.$parent.menu_compresed = false;
  	    $scope.$parent.url_base = window.url_base;
  	};

  	$scope.loadMenuItem = function(menu_id) {
  		_.each($scope.menu_list.top, function(menu) {
  			if (!menu.status) {
  				menu.status = (menu.id == menu_id) ? true : false;
  			}
  			else {
  				menu.status = (menu.id == menu_id) ? false : true;
  			}
  		});
    };
    $scope.compresedMenu = function() {
    	$scope.$parent.menu_compresed = (!$scope.$parent.menu_compresed) ? true : false;
    	//angular.element('');
    	//agregar una clase con media querys!!!
    };
  }
])
.controller('indexCtrl', ['$scope',
  function($scope){
  }
]);
