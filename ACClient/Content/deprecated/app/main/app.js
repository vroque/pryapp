'use strict';
/* jshint -W097 */
/* global angular, window */
/*
  Modulo Principal de Angular
*/
angular.module('centro-atencion',
  [
    'ui.router',
    'ngResource',
    'ngMessages',
    'ngInputModified',
    'oc.lazyLoad',
    'ui.bootstrap',
    'uiModule',
    'ServicesModule'
  ]
)
.run(['$rootScope',
  function($rootScope)
  {
    console.log('Run angular(1)');
  }
]);
