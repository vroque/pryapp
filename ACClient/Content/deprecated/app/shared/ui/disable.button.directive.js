﻿'use-strict';
/* global angular, api_base */
angular.module('uiModule')
/*
  * Directiva para dibujar
  * un control de botones si/no
  * con valores '0' '1'
  * @author Arturo Bolaños
  * @date 11/06/2016
*/
.directive('disBtn', [function () {
    return {
        scope: {
            value: '=',
            mode: '='
        },
        link: function (scope, element, attrs) {
            angular.element(element).bind('click', function (e) {
                angular.element(element).prop('disabled', true);
                setTimeout(function () {
                    angular.element(element).prop('disabled', false);
                }, 5000);
            });
        }
    };
}]);
