'use-strict';
/* global angular, api_base */
angular.module('uiModule')
.directive('datelabel', ['$timeout', function ($timeout) {
    return {
        scope: {
            fecha: '='
        },
        template: function (element, attrs) {
            switch (attrs.skin) {
                case 'pretty':
                    return '<span class="font-xl no-lineheight">{{fecha | date:"dd"}}</span><br>\
          <span class="font-nm no-lineheight">{{fecha | date:"MMM"}}</span><br>\
          <span class="font-nm ">{{fecha | date:"yyyy"}}</span>'
                    break;
                default:
                    return '<span class="font-nm no-lineheight ">{{fecha | date:"dd"}}</span>\
          <span class="font-nm " >{{fecha | date:"MMM"}}</span>\
          <span class="font-nm ">{{fecha | date:"yyyy"}}</span>'
                    break;
            }
        },
        link: function (scope, element, attrs) {
            var color = attrs.color;
            if (attrs.color == undefined || attrs.color.length == 0 || attrs.color == null) {
                color = '#000';
            }
            element.css('color', attrs.color);
        }
    };
}]);
