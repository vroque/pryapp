'use-strict';
/* global angular */
angular.module('uiModule')
/*
  * Directiva para dibujar
  * un control de botones si/no
  * con valores '0' '1'
  * @author Arturo Bolaños
  * @date 11/06/2016
*/
.directive('cdYesNo', [function(){
  return {
    restrict: 'AE',
    template: '<div class="btn-group btn-group-circle">'+
                '<button ng-if="mode == \'write\'" type="button" ng-click="ch_opt(1)" ng-class="(value == \'1\')?\'btn-primary\':\'btn-default\'" class="btn btn-xs">Si</button>'+
                '<button ng-if="mode == \'write\'" type="button" ng-click="ch_opt(0)" ng-class="(value == \'1\')?\'btn-default\':\'btn-primary\'" class="btn btn-xs">No</button>'+
                '<button ng-if="mode == \'read\'" type="button" ng-class="(value == \'1\')?\'btn-primary\':\'btn-default\'" class="btn btn-xs" disabled>Si</button>'+
                '<button ng-if="mode == \'read\'" type="button" ng-class="(value == \'1\')?\'btn-default\':\'btn-primary\'" class="btn btn-xs" disabled>No</button>'+
              '</div>',
    scope: {
      value: '=',
      mode: '='
    },
    link: function(scope, element, attr){
      if(scope.value === undefined || scope.value === null){
        scope.value = '0';
      }
      scope.ch_opt = function(val){
        if( scope.mode === 'write'){
          scope.value = val;
        }
      };
      //binding Change
      scope.$watch('value', function(o,n){
        var cdpChange = attr.cdpChange || false;
        if(cdpChange && o != n)
          scope.$parent.$eval(cdpChange);
      });
    }
  };
}]);
