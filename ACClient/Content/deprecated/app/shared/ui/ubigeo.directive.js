'use-strict';
/* global angular, api_base */
angular.module('uiModule')
/*
  * Directiva para dibujar
  * un control de Departamento
  * con la facultad de trabajar
  * con los controllers de
  * distrito y provincia
  * @author Arturo Bolaños
  * @date 11/06/2016
*/
.directive('cdUbigeoDepartamento', ['ServiceDepartamentoModel',
  function(ServiceDepartamentoModel){
    return {
      restrict: 'AE',
      replace: 'true',
      template: '<div> <span ng-show="mode == \'read\'"> <span ng-repeat="departamento in departamentos | filter:{id:value}:true">{{departamento.name}}</span></span>'+
                '<select ng-change="ch_opt(depa)" ng-show="mode == \'write\'" class="form-control" name="{{name}}" ng-required="req" ng-model="depa">'+
                '	<option ng-repeat="departamento in departamentos" ng-selected="value == departamento.id" value="{{departamento.id}}">{{departamento.name}}</option>'+
                '</select> </div>',
      scope: {
        ubigeo: '=',
        mode: '='
      },
      link: function(scope, element, attrs){
        scope.name = attrs.name;
        scope.req = (attrs.required === undefined)? false: true;
        console.log('depa: ', scope.ubigeo);
        if(scope.ubigeo === undefined || scope.ubigeo === null || !scope.ubigeo)
        {
          scope.ubigeo = '000000';
        }
        scope.value = scope.ubigeo.substring(0,2);
        scope.depa = (scope.value === '00')?'':scope.value;
        ServiceDepartamentoModel.query(
          function(data){
            scope.departamentos = data;
          }, function(err){
            //console.log('dep success', err);
        });
        scope.ch_opt = function(depa){
          scope.ubigeo = depa + '0000';
          scope.value = depa;
          scope.depa = (scope.value === '00')?'':scope.value;
        };
      }
    };
  }
])
/*
  * Directiva para dibujar
  * un control de provincia
  * con la facultad de trabajar
  * con los controllers de
  * distrito t departamento
  * @author Arturo Bolaños
  * @date 11/06/2016
*/
.directive('cdUbigeoProvincia', ['ServiceProvinciaModel',
  function(ServiceProvinciaModel){
    return {
      restrict: 'AE',
      replace: 'true',
      template: '<div> <span ng-show="mode == \'read\'"> <span ng-repeat="provincia in provincias | filter:{id:value}:true">{{provincia.name}}</span></span>'+
                '<select ng-change="ch_opt(prov)" ng-show="mode == \'write\'" class="form-control" name="{{name}}" ng-required="req" ng-model="prov">'+
                '	<option ng-repeat="provincia in provincias" ng-selected="value == provincia.id" value="{{provincia.id}}">{{provincia.name}}</option>'+
                '</select> </div>',
      scope: {
        ubigeo: '=',
        mode: '='
      },
      link: function(scope, element, attrs){
        scope.name = attrs.name;
        scope.req = (attrs.required === undefined)? false: true;
        scope.ch_opt = function(prov){
          scope.ubigeo = prov + '00';
          scope.value = prov;
        };
        scope.set_provincia = function(newp, old){
          var depa = newp.substring(0,2);
          var depa_old = old.substring(0,2);
          scope.value = newp.substring(0,4);
          scope.prov = (scope.value === depa+'00')?'':scope.value;
          if(depa != depa_old){
            ServiceProvinciaModel.query(
              {'id': depa},
              function(data){
                scope.provincias = data;
              }, function(err){
                //console.log('prov success', err);
              }
            );
          }
        };
        scope.$watch('ubigeo', function(value, old){
          scope.set_provincia(value, old);
        });
        if(scope.ubigeo === undefined || scope.ubigeo === null || !scope.ubigeo)
        {
          scope.ubigeo = '000000';
          //scope.prov = (scope.value === depa+'00')?'':scope.value;
        }
        scope.set_provincia(scope.ubigeo, '000000');
      }
    };
  }
])
/*
  * Directiva para dibujar
  * un control de distrito
  * con la facultad de trabajar
  * con los controllers de
  * departamento y provincia
  * @author Arturo Bolaños
  * @date 11/06/2016
*/
.directive('cdUbigeoDistrito', ['ServiceDistritoModel',
  function(ServiceDistritoModel){
    return {
      restrict: 'AE',
      replace: 'true',
      template: '<div> <span ng-show="mode == \'read\'"> <span ng-repeat="distrito in distritos | filter:{id:value}:true">{{distrito.name}}</span></span>'+
                '<select ng-change="ch_opt(dist)" ng-show="mode == \'write\'" class="form-control" name="{{name}}" ng-required="req" ng-model="dist">'+
                '	<option ng-repeat="distrito in distritos" ng-selected="value == distrito.id" value="{{distrito.id}}">{{distrito.name}}</option>'+
                '</select> </div>',
      scope: {
        ubigeo: '=',
        mode: '='
      },
      link: function(scope, element, attrs){
        scope.name = attrs.name;
        scope.req = (attrs.required === undefined)? false: true;
        //scope.dist =
        scope.ch_opt = function(dist){
          scope.ubigeo = dist;
          scope.value = dist;
        };
        scope.set_distrito = function(newd, old){
          var prov = newd.substring(0,4);
          var prov_old = old.substring(0,4);
          scope.value = newd;
          scope.dist = (scope.value === prov+'00')?'':scope.value;
          if(prov != prov_old){
            ServiceDistritoModel.query(
              {'id': prov},
              function(data){
                scope.distritos = data;
                //console.log('prov success', data);
              }, function(err){
                //console.log('prov success', err);
              }
            );
          }
        };
        scope.$watch('ubigeo', function(value, old){
          scope.set_distrito(value, old);
        });
        if(scope.ubigeo === undefined || scope.ubigeo === null || !scope.ubigeo)
        {
          scope.ubigeo = '000000';
        }
        scope.set_distrito(scope.ubigeo, '000000');
      }
    };
  }
]);
