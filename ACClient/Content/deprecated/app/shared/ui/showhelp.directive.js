'use-strict';
/* global angular, api_base */
angular.module('uiModule')
/*
  * Directiva para mostrar mensaje de ayuda
  * en formulario, trabaja con mensaje de ayuda
  * @author Arturo Bolaños
  * @date 11/06/2016
*/
.directive('cdShowHelp',[function(){
  return {
    restrict: 'A',
    link: function(scope, element, attrs){
      var name= attrs.name;
      angular.element(element).on('focus', function(e){
        angular.element('div.help-message[name="'+name+'"]').removeClass('hidden');
      });
      angular.element(element).on('blur', function(e){
        angular.element('div.help-message[name="'+name+'"]').addClass('hidden');
      });
    }
  };
}]);
