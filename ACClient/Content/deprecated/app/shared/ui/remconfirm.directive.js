'use-strict';
/* jshint -W097 */
/* global angular */

angular.module('uiModule')
.directive('remconfirm', [ function () {
  return {
    restrict: 'AE',
    templateUrl: window.url_base+ 'Content/deprecated/app/shared/ui/views/remBotton.html',
    link: function (scope, element, attr) {
      scope.confirm = false;
      scope.msg = attr.msg;
      scope.bindAction = function(e){
        var bindingAction = attr.bindingAction;
        scope.$eval(bindingAction);
      };
    }
  };
}]);
angular.module('uiModule')
.directive('cdRemConfirm', [ function () {
  return {
    restrict: 'AE',
    templateUrl: window.url_base + 'Content/deprecated/app/shared/ui/views/remButton.html',
    link: function (scope, element, attr) {
      scope.confirm = false;
      scope.msg = attr.msg;
      scope.bindAction = function(e){
        var bindingAction = attr.cdpAction;
        scope.$eval(bindingAction);
      };
    }
  };
}]);
