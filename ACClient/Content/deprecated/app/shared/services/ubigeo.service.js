'use strict';
/* jshint -W097 */
/* global angular, window */

angular.module('ServicesModule')
/*
  * Servicio para obtener
  * los Departamento
  * @author Arturo Bolaños
  * @date 11/06/2016
*/
.factory('ServiceDepartamentoModel', ['$resource',
  function($resource) {
    return $resource(window.url_base + 'api/ubigeo/department/:id', { id: '@id' }, {});
  }
])
/*
  * Servicio para obtener
  * los Provincias
  * @author Arturo Bolaños
  * @date 11/06/2016
*/
.factory('ServiceProvinciaModel', ['$resource',
  function($resource) {
    return $resource(window.url_base + 'api/ubigeo/province/:id', { id: '@id' }, {});
  }
])
/*
  * Servicio para obtener
  * los Distritos
  * @author Arturo Bolaños
  * @date 11/06/2016
*/
.factory('ServiceDistritoModel', ['$resource',
  function($resource) {
    return $resource(window.url_base + 'api/ubigeo/district/:id', { id: '@id' }, {});
  }
]);
