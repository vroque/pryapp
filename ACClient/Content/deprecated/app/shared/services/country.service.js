'use strict';
/* jshint -W097 */
/* global angular, window */

angular.module('ServicesModule')
/*
  * Servicio para obtener
  * los Paises
  * @author Arturo Bolaños
  * @date 11/06/2016
*/
.factory('CountryModel', ['$resource',
  function($resource) {
    return $resource(window.url_base +  'api/ubigeo/country/:id', { id : '@id'}, {});
  }
]);
