'use strict';
/* jshint -W097 */
/* global angular */

angular.module('ServicesModule')
  .service('PersonModel', ['$resource', '$rootScope',
    function ($resource, $rootScope) {
      return $resource(
        $rootScope.url_base + 'backoffice/api/person/:id',
        {id: '@id'}
      );
    }
  ]);
