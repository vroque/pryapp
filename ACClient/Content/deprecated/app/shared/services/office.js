﻿'use strict';
/* jshint -W097 */
/* global angular, api_base */

angular.module('ServicesModule')
.service('OfficeModel', ['$resource', '$rootScope',
  function ($resource, $rootScope) {
      return $resource($rootScope.url_base + 'backoffice/api/oficina/');
  }
]);
