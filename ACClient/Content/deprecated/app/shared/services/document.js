'use strict';
/* jshint -W097 */
/* global angular, api_base */

angular.module('ServicesModule')
.service('DocumentModel', ['$resource', '$rootScope',
  function ($resource, $rootScope) {
      return $resource($rootScope.url_base + 'backoffice/api/documento/');
  }
]);
