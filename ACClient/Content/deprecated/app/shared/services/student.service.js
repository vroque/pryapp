'use strict';
/* jshint -W097 */
/* global angular, window */

angular.module('ServicesModule')
  .factory('StudentModel', ['$resource', '$rootScope',
    function ($resource, $rootScope) {
      return $resource(
        $rootScope.url_base + 'api/academic/student/:id',
        {id: '@id'}
      );
    }
  ])
  .factory('AcademicProfileModel', ['$resource',
    function ($resource) {
      return $resource(
        window.url_base + 'api/academic/profile/:id', {id: '@id'}
      );
    }
  ])
  .factory('AcademicProgressModel', ['$resource',
    function ($resource) {
      return $resource(
        window.url_base + 'api/academic/AcademicProgress/:id', {id: '@id'}
      );
    }
  ])
  .factory('CourseTrackModel', ['$resource',
    function ($resource) {
      return $resource(
        window.url_base + 'api/academic/CourseTrack/:id', {id: '@id'}
      );
    }
  ])
  .factory('GraduatedRuleModel', ['$resource',
    function ($resource) {
      return $resource(
        window.url_base + 'api/academic/GraduatedRule/:id', {id: '@id'}
      );
    }
  ])
  //Estos deben ir en Convalidaciones
  .factory('ValidationEquivalencyModel', ['$resource',
    function ($resource) {
      return $resource(
        window.url_base + 'api/convalidation/ValidationEquivalency/:id', {id: '@id'}
      );
    }
  ])
  .factory('EquivalencyTableModel', ['$resource',
    function ($resource) {
      return $resource(
        window.url_base + 'api/convalidation/EquivalencyTable/:id', {id: '@id'}
      );
    }
  ]);
