'use strict';
/* jshint -W097 */
/* global angular, require, window */
angular.module('centro-atencion')
.config(['$ocLazyLoadProvider', '$stateProvider', '$urlRouterProvider',
  function ($ocLazyLoadProvider, $stateProvider, $urlRouterProvider)
  {
    $ocLazyLoadProvider.config({ asyncLoader: require });
    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('home', {
      url: '/reportage',
      controller: 'reportageCtrl',
      templateUrl: window.app_base + 'views/reportage.html',
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'CAUModule',
            serie: true, // used for synchronous load chart scripts
            insertBefore: '#ngInsertBefore',
            files: [
              window.app_base + 'module.js',
              window.app_base + 'controllers/reportage.js'
            ]
          });
        }
      }
    })
    .state('documentsPromotionalCertificate', {
        url: '/DocumentosPromocionalesCertificado',
        controller: 'certificateCtrl',
        templateUrl: window.app_base + 'views/documents/documentsPromotionalCertificate.html',
        resolve: {
            load: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'CAUModule',
                    serie: true, // used for synchronous load chart scripts
                    insertBefore: '#ngInsertBefore',
                    files: [
                    window.app_base + 'module.js',
                    window.app_base + 'controllers/documents/documents.promotional.certificate.js',
                    window.app_base + 'services/promotional.certificate.js',
                    window.app_base + 'filters/custom.filters.js'
                    ]
                });
            }
        }
    })
    .state('documentsPromotionalCertificate.step1', {
        url: '/Paso1',

        views:{
            '@':{
                templateUrl: window.app_base + 'views/documents/documentsPromotionalCertificatePaso1.html',
                controller: 'certificateCtrl',
                }
        },

        resolve: {
            load: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'CAUModule',
                    serie: true, // used for synchronous load chart scripts
                    insertBefore: '#ngInsertBefore',
                    files: [
                    window.app_base + 'module.js',
                    window.app_base + 'controllers/documents/documents.promotional.certificate.js',
                    window.app_base + 'services/promotional.certificate.js',
                    window.app_base + 'filters/custom.filters.js'
                    ]
                });
            }
        }
    })
    .state('documentsPromotionalCertificate.step2', {
        url: '/Paso2',
        views:{
            '@':{
                templateUrl: window.app_base + 'views/documents/documentsPromotionalCertificatePaso2.html',
                controller: 'certificateCtrl',
                 }
        },
        resolve: {
            load: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'CAUModule',
                    serie: true, // used for synchronous load chart scripts
                    insertBefore: '#ngInsertBefore',
                    files: [
                    window.app_base + 'module.js',
                    window.app_base + 'controllers/documents/documents.promotional.certificate.js',
                    window.app_base + 'services/promotional.certificate.js',
                    window.app_base + 'filters/custom.filters.js'
                    ]
                });
            }
        }
    })
    .state('documentsPromotionalConstancy', {
        url: '/DocumentosPromocionalesConstancias',
        controller: 'constancyCtrl',
        templateUrl: window.app_base + 'views/documents/documentsPromotionalConstancy.html',
        resolve: {
            load: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'CAUModule',
                    serie: true, // used for synchronous load chart scripts
                    insertBefore: '#ngInsertBefore',
                    files: [
                    window.app_base + 'module.js',
                    window.app_base + 'controllers/documents/documents.promotional.constancy.js',
                    window.app_base + 'services/promotional.constancy.js',
                    window.app_base + 'filters/custom.filters.js'
                    ]
                });
            }
        }
    })
    .state('documentsPromotionalConstancy.grupal', {
        url: '/Grupal',

        views:{
            '@':{
                templateUrl: window.app_base + 'views/documents/documentsPromotionalConstancyGrupal.html',
                controller: 'constancyCtrl',
            }
        },

        resolve: {
            load: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'CAUModule',
                    serie: true, // used for synchronous load chart scripts
                    insertBefore: '#ngInsertBefore',
                    files: [
                    window.app_base + 'module.js',
                    window.app_base + 'controllers/documents/documents.promotional.constancy.js',
                    window.app_base + 'services/promotional.constancy.js',
                    window.app_base + 'filters/custom.filters.js'
                    ]
                });
            }
        }
    })
    .state('documentsPromotionalConstancy.individual', {
        url: '/Individual',
        views:{
            '@':{
                templateUrl: window.app_base + 'views/documents/documentsPromotionalConstancyIndividual.html',
                controller: 'constancyCtrl',
                 }
        },
        resolve: {
            load: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'CAUModule',
                    serie: true, // used for synchronous load chart scripts
                    insertBefore: '#ngInsertBefore',
                    files: [
                    window.app_base + 'module.js',
                    window.app_base + 'controllers/documents/documents.promotional.constancy.js',
                    window.app_base + 'services/promotional.constancy.js',
                    window.app_base + 'filters/custom.filters.js'
                    ]
                });
            }
        }
    })
    //student card duplicate
    .state('duplicateLicense', {
        url: '/list',
        views:{
            '@':{
                templateUrl: window.app_base + 'views/duplicate/list.html',
                controller: 'duplicateCtrl',
                 }
        },
        resolve: {
            load:['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'CAUModule',
                    serie: true, // used for synchronous load chart scripts
                    insertBefore: '#ngInsertBefore',
                    files: [
                    window.app_base + 'module.js',
                    window.app_base + 'controllers/duplicate/list.js',
                    window.app_base + 'services/duplicate/duplicateLicenseList.js',
                    window.app_base + 'services/shared/application.states.js',
                    window.app_base + 'services/duplicate/servicesC5.js',
                    ]
                });
            }]
        }
    })
    //new report: amount of solicitudes by student and request
    .state('reportApplicationByStudent', {
        url: '/report/application/by/student',
        views:{
            '@':{
                templateUrl: window.app_base + 'views/report/applicationByStudent.html',
                controller: 'reportApplicationByStudentCtrl',
                 }
        },
        resolve: {
            load:['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'CAUModule',
                    serie: true, // used for synchronous load chart scripts
                    insertBefore: '#ngInsertBefore',
                    files: [
                    window.app_base + 'module.js',
                    window.app_base + 'controllers/report/application.by.student.js',
                    window.app_base + 'services/shared/application.states.js',
                    window.app_base + 'services/shared/application.document.js',
                    window.app_base + 'services/report/application.by.student.js',
                    window.app_base + '../../../v1/backoffice/js/lib/jquery.table2excel.min.js'
                    ]
                });
            }]
        }
    })
    //generate gradutes' records
    .state('graduateRecord', {
        url: '/constancia/egresado',
        views:{
            '@':{
                templateUrl: window.app_base + 'views/graduateRecord/generate.html',
                controller: 'generateGraduteRecordCtrl',
                 }
        },
        resolve: {
            load:['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'CAUModule',
                    serie: true, // used for synchronous load chart scripts
                    insertBefore: '#ngInsertBefore',
                    files: [
                    window.app_base + 'module.js',
                    window.app_base + 'controllers/graduateRecord/generate.js',
                    window.app_base + '../../../v1/backoffice/js/lib/jquery.table2excel.min.js',
                    window.app_base + 'directives/graduateRecord/listStudentId.js',
                    window.app_base + 'services/graduateRecord/graduates.js',
                    window.app_base + 'filters/graduateRecord/filters.js'
                    ]
                });
            }]
        }
    });
  }
]);
