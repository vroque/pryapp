﻿
'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('CAUModule')
    /*
     * Controller para manejar el cambio de estado de solicitudes
     * @author GNUstavo Huarcaya
     * @date 02/05/2016
     */
    .controller('toolsCtrl', ['$scope', '$http', 'solicitudesEstudianteModel',
                'estadoSolicitudModel',
        function($scope, $http, solicitudesEstudianteModel,
                 estadoSolicitudModel) {
            $scope.moreSearchOption = false;
            $scope.searchSolicitud = false
            $scope.estudiante = "";
            $scope.errorMsg = '';
            $scope.resultados = true;

            $scope.moreSearchOptions = function() {
                $scope.moreSearchOption = !$scope.moreSearchOption;
            };

            $scope.limpiar = function() {
                $scope.moreSearchOption = false;
                $scope.estudiante = "";
                $scope.searchSolicitud = false;
            };

            $scope.myEnter = function(keyEvent) {
              if (keyEvent.which === 13) {
                $scope.searchSolicitudes($scope.estudiante);
              }
            };

            $scope.searchSolicitudes = function(alumno_id) {
                $scope.searchSolicitud = true;

                herramientasModel.query({
                        alumno_id: alumno_id
                    }).$promise
                    .then(function(data) {
                        if (data.length > 0) {
                            $scope.solicitudes = data;
                            $scope.resultados = true;
                        } else {
                            $scope.errorMsg = "nada que mostrar";
                            $scope.resultados = false;
                        }
                    }, function(err) {
                        $scope.errorMsg = "arrrg!";
                        $scope.resultados = false;
                    });
            };

            $scope.editarEstadoSolicitud = function(sol) {
                sol.estado = 'write';
                $scope.editable = !$scope.editable;
                $scope.guardar = !$scope.guardar;

                estadoSolicitudModel.query().$promise
                    .then(function (data) {
                        if (data.length > 0) {
                            $scope.estados = data;
                        } else {
                            // pass
                        }
                    }, function (err) {
                      // pass
                    });

            };

            $scope.guardarEstadoSolicitud = function(sol) {
                sol.estado = 'read';
                $scope.guardar = !$scope.guardar;
                $scope.editable = false;

                herramientasModel.save({
                    alumno_id: sol.alumno_id,
                    id: sol.id,
                    estado_id: sol.estado_id
                }, function(data) {
                    console.log(data);
                });

                $scope.searchSolicitudes(sol.alumno_id);
            };

        }
    ]);
