
'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('CAUModule')
    /*
     * Controller: duplicate licenses
     * @author wanshi reymundo
     * @date 28/06/2016
     */
    .controller('duplicateCtrl', ['$scope', '$http','DuplicateListModel','ApplicationStateModel','SendToC5Model','CleanC5Model','ChangeStateModel','MailModel',
        function($scope, $http,DuplicateListModel,ApplicationStateModel,SendToC5Model,CleanC5Model,ChangeStateModel,MailModel ) {
          $scope.init=function(){
            console.log("init");
            //change for for productions
            $scope.state_after_paying=1
            //$scope.state_after_paying=6
            $scope.state_search=$scope.state_after_paying;
            $scope.searchApplications($scope.state_search);
            ApplicationStateModel.get().$promise.then(
              function(data){$scope.application_states=data.objects},
              function(error){console.log(error);}
            )

          }
        $scope.searchApplications=function(state){
          $scope.initWork();
          DuplicateListModel.query({state:state}).$promise.then(
            function(data){
              $scope.actual_state=state;
              $scope.msg=data.length+' resultado(s).'
              data.forEach(function(item){item.active=0});
              data.active=function(){
                /*
                0:all 0
                -1:incomplete
                1:all 1
                */
                var count=0;
                this.forEach(function(item){count+=item.active});
                if(count!=0){
                  count=count==this.length?1:-1;
                }
                return count;
              }
              $scope.duplicate_list=data;
              console.log($scope.duplicate_list);
              $scope.endWork();
            },
            function(error){console.log(error);}
          );
        };
        $scope.checkAll=function(){
          var all=0;
          $scope.duplicate_list.forEach(function(item){all=item.active==0?1:all;})
          $scope.duplicate_list.forEach(function(item){item.active=all})
        };
        $scope.checkUncheck=function(id){
          $scope.duplicate_list.forEach(function(item){
            if(item.id==id){
              item.active=item.active==1?0:1;
            }
          })
        };

        $scope.sendToC5=function(){
          $scope.initWork();
          console.log("sendToC5");
          var block_duplicates=[];
          var temp;
          $scope.duplicate_list.forEach(
            function(item){
              if(item.active){
              temp={
                 id_dependency:item.dependencia_id,
                 id_headquarters:item.sede_id,
                 id_per_acad:item.periodo_id,
                id_student:item.alumno_id
              }
              block_duplicates.push(temp);
            }
            }
          );
          console.log(block_duplicates);
          var x = new SendToC5Model({list_duplicates:block_duplicates});
          x.$save().then(
            function(data){
              $scope.msg=data.list_duplicates.length +' enviado(s) a C5.';
              console.log(data);
              $scope.endWork();
            },
            function(error){console.log(error);}
          );
        };
        $scope.cleanC5=function(){
          console.log("cleanC5");
          var x =
          CleanC5Model.delete().$promise.then(
            function(data){console.log(data);
              $scope.msg=' C5 Limpio.';
            },
            function(error){console.log(error);}
          )
        };
        $scope.changeState=function(new_state){
          $scope.initWork();
          console.log("change state:"+new_state);
          var list_applications=[];
          var temp;
          $scope.duplicate_list.forEach(function(item){
            if(item.active){
              temp={dependencia_id:item.dependencia_id,
                sede_id:item.sede_id,
                id:item.id,
                alumno_id:item.alumno_id
              }
              list_applications.push(temp);
            }
          });
          console.log(list_applications);
          var x=new ChangeStateModel({state:new_state,list_applications,list_applications});
          x.$save().then(
            function(data){console.log(data);            
            },
            function(error){console.log(error);}

          );
          $scope.searchApplications($scope.actual_state);
        };
    /*
        $scope.sendMail=function() {
          console.log('sending mail');
          var list_mail_data=[];
          var temp;
          $scope.duplicate_list.forEach(function(item){
            if(item.active){
              temp={
                application_id:item.id,
                student_id:item.alumno_id,
                state_srt:item.estado_id_str,
                date_application_str:item.fechasolicitud_str,
                description_str:item.descripcion_str,
                application_text_str:item.documento_text_str,
                subject:'subject: studetn card ready!'
              }
              list_mail_data.push(temp);
            }
          });
          console.log(list_mail_data);
          var x = new  MailModel({mail_type:'duplicate_student_card',list_mail_data:list_mail_data});
          x.$save().then(
            function(data){
              $scope.msg='Correo(s) enviado(s).'
              console.log(data);},
            function(error){console.log(error);}
          );
        };
*/
        $scope.sendMail=function() {
          $scope.initWork();
          console.log('sending mail');
          var list_student=[];
          var temp;
          $scope.duplicate_list.forEach(function(item){
            if(item.active){
              temp={
                id:item.alumno_id
              }
              list_student.push(temp);
            }
          });
          console.log(list_student);
          var x = new  MailModel({mail_type:'duplicate_student_card',list_student:list_student});
          x.$save().then(
            function(data){
              $scope.msg='Correo(s) enviado(s).'
              console.log(data);
              $scope.endWork();
            },
            function(error){console.log(error);}
          );
        };
        $scope.initWork=function(){
          $scope.working=true;
          NProgress.configure({ parent: "#bar-loading-alumnos" });
          NProgress.start();
        };
        $scope.endWork=function(){
          $scope.working=false;
          NProgress.done();
        };


        }
    ]);
