'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('CAUModule')
    /*
     * Controller: report, apllication by student
     * @author wanshi reymundo
     * @date 07/07/2016
     */
    .controller('reportApplicationByStudentCtrl', ['$scope', '$http','ApplicationStateModel','ApplicationDocumentModel','ApplicationByStudentModel',
        function($scope, $http,ApplicationStateModel,ApplicationDocumentModel,ApplicationByStudentModel ) {
          $scope.init=function(){
            $scope.msg='Esperando datos para la busqueda.';
            ApplicationStateModel.get().$promise.then(
              function(data){
                data.objects.push({id:0,descripcion_str:''});
                $scope.application_states=data.objects .sort(function (a, b) {
                    if (a.descripcion_str > b.descripcion_str) {return 1;}
                    if (a.descripcion_str < b.descripcion_str) {return -1;}
                    return 0;
                    })},
              function(error){console.log(error);}
            )
            ApplicationDocumentModel.get().$promise.then(
              function(data){
                data.objects.push({id:0,nombre_str:''});
                $scope.application_documents=data.objects.sort(function (a, b) {
                    if (a.nombre_str > b.nombre_str) {return 1;}
                    if (a.nombre_str < b.nombre_str) {return -1;}
                    return 0;
                    })},
              function(error){console.log(error);}
            )
          };
          $scope.searchApplications=function(id_student='',id_document=0,id_state=0){
            $scope.msg='Buscando...';


            ApplicationByStudentModel.query({ id_student : id_student, id_document : id_document, id_state:id_state}).$promise.then(
              function(data){
                $scope.msg="Se encontraron "+data.length+" registro(s).";
                ;$scope.applicationsByStudent=data},
              function(error){$scope.msg='Ha ocurrido un problema, recarge la pagina por favor.';console.log(error);}
            )
          };
          $scope.openApplication= function(id_student,id_application){
            var url=$scope.url_base + 'backoffice/centro/de/atencion/solicitudes/'+id_application+'/'+id_student+'/';
            window.open(url);
          };
          $scope.table2excel=function(){
            $("#table-applications-by-student").table2excel({
              exclude: ".excludeThisClass",
              name: "ApplicationsByStudent",
              filename: "Solicitudes por estudiante" //do not include extension
            });
          };
        }
    ]);
