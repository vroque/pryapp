'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('CAUModule')
  /*
    * Controllador para manejar relaciones familiares
    * de la ficha socioeconomica
    * @author Arturo Bolaños
    * @date 07/03/2016
  */
.controller('reportageCtrl', ['$scope', 'RequestModel','RequestStateModel','OfficeModel','DocumentModel',
  function ($scope,RequestModel,RequestStateModel,OfficeModel,DocumentModel) {
      $scope.init = function () {
        $scope.filter_form = {
          request_status_id : 1
        };
        $scope.request_found_loaded = [];
        $scope.request_found = [];
        $scope.action_document = [
          {
            id: 2,
            active: 'active',
            name: 'Derivaciones',
            icon: 'send'
          },
          {
            id: 4,
            active: '',
            name: 'Emisiones',
            icon: 'paperclip'
          }
        ];
        OfficeModel.query().$promise.then(
          function(data) {
            $scope.offices = data;
          },
          function(error) {
            console.log('error_office',error);
          }
        );
        RequestStateModel.query().$promise.then(
          function(data) {
            $scope.request_state = data;
          },
          function(error) {
            console.log('error_request_state',error);
          }
        );
        DocumentModel.query().$promise.then(
          function(data) {
            $scope.documents = data;
          },
          function(error) {
            console.log('error_documents',error);
          }
        );

      };

      $scope.selectActionSection = function(action_id) {
        _.each($scope.action_document, function(action) {
          action.active = (action.id==action_id) ? 'active' : '';
          $scope.filter_form.request_status_id = action_id;
        });
      };

      $scope.searchRequest = function(request_id,creation_date) {
        console.log("eentre");
        $scope.request_found = [];
        if (request_id != undefined && creation_date == undefined) {
          if (!_.contains(_.pluck($scope.request_found_loaded,'id'),request_id)) {
            RequestModel.get({id: request_id}).$promise.then(
              function(data) {
                $scope.request_found_loaded.push(data);
                $scope.request_found.push(data);
              },
              function(error) {
                console.log("No se encontro");
              }
            );
          }
          else {
            $scope.request_found.push(_.findWhere($scope.request_found_loaded,{id:request_id}));
          }
        }
        else if (request_id == undefined && creation_date != undefined) {
          var _date_exact = creation_date.split('/');
          if (!isNaN(new Date(_date_exact[2],_date_exact[1]-1,_date_exact[0]))) {
            creation_date = new Date(_date_exact[2],_date_exact[1]-1,_date_exact[0])
            console.log("vaaa");
            RequestModel.query({creation_date: creation_date}).$promise.then(
              function(data) {
                _.each(data, function(request) {
                  if (!_.contains(_.pluck($scope.request_found_loaded,'id'),request.id)) {
                    $scope.request_found_loaded.push(request);
                  }
                  $scope.request_found.push(request);
                });

              },
              function(error) {
                console.log('No se encontro');
              }
            );
          }
        }
      };
    }
  ]
);
