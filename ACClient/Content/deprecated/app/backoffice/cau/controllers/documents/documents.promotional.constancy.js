﻿'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('CAUModule')
  /*
    * Controllador para manejar relaciones familiares
    * de la ficha socioeconomica
    * @author Arturo Bolaños
    * @date 07/03/2016
  */
.controller('constancyCtrl', ['$scope', 'RequestModel', 'RequestStateModel', 'EscuelasModel', 'AlumnosEgresadosModel', 'ConstanciasModel', 'EgresadoIndividualModel',
  function ($scope, RequestModel, RequestStateModel, EscuelasModel, AlumnosEgresadosModel,ConstanciasModel,EgresadoIndividualModel) {
    $scope.init = function () {
      $scope.alumnosSeleccionados=[];
      $scope.escuelas=[];
      $scope.alumnos=[];
      $scope.errores=[];
      $scope.mostrarError=false;

      EscuelasModel
        .query()
        .$promise.then(
        function (data) {
          $scope.escuelas=[];

          angular.forEach(data,function(value,key)
              {
                value.name=value.id+' - '+value.name;
                this.push(value);
              }
              ,$scope.escuelas);
        },
        function (error) {
            console.log('escuelas_model_office', error);
        }
      );

    };


    $scope.iniciarBusqueda=function(formdata){
      if(formdata.$invalid){
        return true;
      }

      NProgress.configure({ parent: "#bar-loading-alumnos" });
      NProgress.start();
      var button = document.getElementById("button-buscar");
      button.value="Buscando";
      button.disabled="disabled";

      AlumnosEgresadosModel
        .query({id : formdata.escuela.$viewValue})
        .$promise.then(
        function (data) {
          $scope.alumnos= [];
          angular.forEach(data,function(value,key)
              {
                value["habilitado"] = true;
                value["status"] = 0;
                value["datos_completos"] = false;
                value["ruta_archivo"] = "";
                value["periodo_str_anterior"] = "";
                value["periodo_str_proximo"] = "";
                this.push(value);
              }
              ,$scope.alumnos);

              NProgress.done();
              button.value="Buscar";
              button.removeAttribute("disabled");
        },
        function (error) {
            console.log('escuelas_model_office', error);
        }
      );
    }
    $scope.iniciarBusquedaIndividual=function(formdata){
      if(formdata.$invalid){
        return true;
      }

      NProgress.configure({ parent: "#bar-loading-alumnos" });
      NProgress.start();
      var button = document.getElementById("button-buscar");
      button.value="Buscando";
      button.disabled="disabled";

      EgresadoIndividualModel
        .query({id : formdata.IDAlumno.$viewValue})
        .$promise.then(
        function (data) {
          $scope.alumnos= [];
          angular.forEach(data,function(value,key)
              {
                value["habilitado"] = true;
                value["status"] = 0;
                value["datos_completos"] = false;
                value["ruta_archivo"] = "";
                value["periodo_str_anterior"] = "";
                value["periodo_str_proximo"] = "";
                this.push(value);
              }
              ,$scope.alumnos);

              NProgress.done();
              button.value="Buscar";
              button.removeAttribute("disabled");
        },
        function (error) {
            console.log('escuelas_model_office', error);
        }
      );
    }

    $scope.procesarConstanciaIndividual=function(alumno){
      NProgress.start();

      if(alumno.status===2){
        $scope.periodo=alumno.periodo_str_anterior;
      }
      if(alumno.status===3){
        $scope.periodo=alumno.periodo_str_proximo;
      }
      $scope.listAlumnos=[];
      $scope.listAlumnos.push(alumno)

      ConstanciasModel
        .procesarConstancias({periodo : $scope.periodo,
                              listAlumnos: $scope.listAlumnos})
        .$promise.then(
        function (data) {
          angular.forEach(data,function(value,key){
            $scope.alumnosSeleccionados.filter(
              function(obj)
              {
                if(obj.IDAlumno===value.id_alumno){
                  obj.datos_completos = value.datos_completos;
                  obj.periodo_str_anterior = value.periodo_str_anterior;
                  obj.periodo_str_proximo = value.periodo_str_proximo;
                  obj.ruta_archivo = value.ruta_archivo;

                  if(value.datos_completos){
                    obj.status = 1;
                  }
                  else
                  {
                    if(value.periodo_str_anterior!==null){
                      obj.status = 2;
                    }
                    else
                    {
                      obj.status = 3;
                    }
                  }
                  NProgress.done();
                  return obj;
                }
              }
            );

          });
          NProgress.done();
        },
        function (error) {
            console.log('escuelas_model_office', error);
        }
      );
    }

    $scope.procesarConstancias=function(frm){
      //errores
      $scope.mostrarError = false;
      $scope.errores=[];

      if(frm.$invalid){
        return true;
      }

      if($scope.alumnosSeleccionados.length===0)
        {
          var er=new Object();
          er["idError"]=1;
          er["descripcion"]="No se seleccionaron los alumnos";
          $scope.errores.push(er);
          $scope.mostrarError = true;
          return true;
        }

      //progress
      NProgress.configure({ parent: "#bar-loading-constancias" });
      NProgress.start();
      var button = document.getElementById("button-procesar");
      button.value="Procesando";
      button.disabled="disabled";
      $scope.alumnosSeleccionados.filter(
                function(obj)
                {
                    obj.datos_completos = false;
                    obj.periodo_str_anterior = "";
                    obj.periodo_str_proximo = "";
                    obj.ruta_archivo = "";
                    obj.status = 0;
                }
              );




      $scope.periodo=frm.periodoAcademico.$viewValue;
      $scope.listAlumnos=[];
        angular.forEach($scope.alumnosSeleccionados,
          function(value,key){
            this.push(value);
          }
          ,$scope.listAlumnos);

      ConstanciasModel
        .procesarConstancias({periodo : $scope.periodo,listAlumnos: $scope.listAlumnos})
        .$promise.then(
        function (data) {
          console.log(data);
          if("idError" in data[0])
          {
            $scope.mostrarError = true;
            angular.forEach(data,
              function(value,key){
                this.push(value);
              }
            ,$scope.errores);
          }
          else
          {
            angular.forEach(data,function(value,key){
              $scope.alumnosSeleccionados.filter(
                function(obj)
                {
                  if(obj.IDAlumno===value.id_alumno){
                    obj.datos_completos = value.datos_completos;
                    obj.periodo_str_anterior = value.periodo_str_anterior;
                    obj.periodo_str_proximo = value.periodo_str_proximo;
                    obj.ruta_archivo = value.ruta_archivo;

                    if(value.datos_completos){
                      obj.status = 1;
                    }
                    else
                    {
                      if(value.periodo_str_anterior!==null){
                        obj.status = 2;
                      }
                      else
                      {
                        obj.status = 3;
                      }
                    }
                    NProgress.done();
                    button.value="Procesar";
                    button.removeAttribute("disabled");
                    return obj;
                  }
                }
              );
            });
          }
          NProgress.done();
          button.value="Procesar";
          button.removeAttribute("disabled");
        },
        function (error) {
            console.log('escuelas_model_office', error);
        }
      );
    }

    $scope.passToList=function(alumno){
      alumno["habilitado"]=false;
      $scope.alumnosSeleccionados.push(alumno);
    }
    $scope.removeFromSelected=function(alumno){
      alumno["habilitado"]=true;
      $scope.alumnosSeleccionados=$scope.alumnosSeleccionados.filter(function(obj){if(obj.IDAlumno!==alumno.IDAlumno){return obj;}});
    }

    var periodoSeleccionado = null;
    $scope.selectPeriodoFinalizo = function (periodo) {
      periodoSeleccionado = ( periodo !== "" ? periodo : null);
    }
    $scope.periodoFinalizoFilterFn = function (alumno) {
      return periodoSeleccionado == null || alumno.IDPerAcad == periodoSeleccionado;
    }
  }
]);
