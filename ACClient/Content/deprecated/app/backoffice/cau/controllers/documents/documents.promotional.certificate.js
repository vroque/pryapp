'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('CAUModule')
  /*
    * Controllador para manejar relaciones familiares
    * de la ficha socioeconomica
    * @author Arturo Bolaños 
    * @date 07/03/2016
  */
.controller('certificateCtrl', ['$scope', 'RequestModel', 'RequestStateModel', 'EscuelasModel', 'AlumnosEgresadosModel', 'CertificateModel','PrintCertificateModel',
  function ($scope, RequestModel, RequestStateModel, EscuelasModel, AlumnosEgresadosModel, CertificateModel, PrintCertificateModel) {
      $scope.init = function () {
          $scope.alumnosSeleccionados = [];
          $scope.solicitudesSeleccionadas = [];
          $scope.escuelas = [];
          $scope.alumnos = [];
          $scope.errores = [];
          $scope.mostrarError = false;

          EscuelasModel
            .query()
            .$promise.then(
            function (data) {
                $scope.escuelas = [];

                angular.forEach(data, function (value, key) {
                    value.name = value.id + ' - ' + value.name;
                    this.push(value);
                }
                    , $scope.escuelas);
            },
            function (error) {
                console.log('escuelas_model_office', error);
            }
          );

      };

      /*enventos paso 1*/

      $scope.iniciarBusquedaPaso1 = function (formdata) {
          if (formdata.$invalid) {
              return true;
          }

          NProgress.configure({ parent: "#bar-loading-alumnos" });
          NProgress.start();
          var button = document.getElementById("button-buscar");
          button.value = "Buscando";
          button.disabled = "disabled";

          AlumnosEgresadosModel
            .query({ id: formdata.escuela.$viewValue })
            .$promise.then(
            function (data) {
                $scope.alumnos = [];
                angular.forEach(data, function (value, key) {
                    value["habilitado"] = true;
                    value["estado_id"] = 0;
                    value["estado_descripcion"] = "Por Generar";
                    value["id_solicitud"] = "--";
                    this.push(value);
                }
                    , $scope.alumnos);

                NProgress.done();
                button.value = "Buscar";
                button.removeAttribute("disabled");
            },
            function (error) {
                console.log('escuelas_model_office', error);
            }
          );
      }

      $scope.procesarCertificados = function (frm) {
          //errores
          $scope.mostrarError = false;
          $scope.errores = [];

          if (frm.$invalid) {
              return true;
          }

          if ($scope.alumnosSeleccionados.length === 0) {
              var er = new Object();
              er["idError"] = 1;
              er["descripcion"] = "No se seleccionaron los alumnos";
              $scope.errores.push(er);
              $scope.mostrarError = true;
              return true;
          }

          //progress
          NProgress.configure({ parent: "#bar-loading-constancias" });
          NProgress.start();

          var button = document.getElementById("button-procesar");
          button.value = "Generando ...";
          button.disabled = "disabled";
          $scope.alumnosSeleccionados.filter(
                    function (obj) {
                        obj.datos_completos = false;
                        obj.status = 0;
                    }
                  );
          $scope.listAlumnos = [];
          angular.forEach($scope.alumnosSeleccionados,
            function (value, key) {
                this.push(value);
            }
            , $scope.listAlumnos);

          CertificateModel
            .procesarCertificados({
              listAlumnos: $scope.listAlumnos
            })
            .$promise.then(
            function (data) {
                if ("idError" in data[0]) {
                    $scope.mostrarError = true;
                    angular.forEach(data,
                      function (value, key) {
                          this.push(value);
                      }
                    , $scope.errores);
                }
                else {
                    angular.forEach(data, function (value, key) {
                        $scope.alumnosSeleccionados.filter(
                          function (obj) {
                              if (obj.IDAlumno === value.id_alumno) {
                                  obj.estado_id=value.estado_id;
                                  obj.estado_descripcion=value.estado_descripcion;
                                  obj.id_solicitud=value.id_solicitud;

                                  NProgress.done();
                                  button.value = "Generar Solicitudes";
                                  button.removeAttribute("disabled");
                                  return obj;
                              }
                          }
                        );
                    });
                }
                NProgress.done();
                button.value = "Generar Solicitudes";
                button.removeAttribute("disabled");
            },
            function (error) {
                console.log('escuelas_model_office', error);
            }
          );
      }

      $scope.passToList = function (alumno) {
          alumno["habilitado"] = false;
          $scope.alumnosSeleccionados.push(alumno);
      }
      $scope.removeFromSelected = function (alumno) {
          alumno["habilitado"] = true;
          $scope.alumnosSeleccionados = $scope.alumnosSeleccionados.filter(function (obj) { if (obj.IDAlumno !== alumno.IDAlumno) { return obj; } });
      }

      /*eventos del paso 2*/

      $scope.iniciarBusquedaPaso2 = function (formdata) {
          if (formdata.$invalid) {
              return true;
          }
          
          var fechaIni = new Date(formdata.fechaDesde.$viewValue.replace( /(\d{2})\/(\d{2})\/(\d{4})/, "$3/$2/$1"));
          var fechaFin = new Date(formdata.fechaHasta.$viewValue.replace( /(\d{2})\/(\d{2})\/(\d{4})/, "$3/$2/$1"));
          

          //verificando las fechas

          fechaIni.setUTCHours(0);
          fechaFin.setUTCHours(0);

          NProgress.configure({ parent: "#bar-loading-alumnos" });
          NProgress.start();
          var button = document.getElementById("button-buscar");
          button.value = "Buscando Solicitudes";
          button.disabled = "disabled";

          PrintCertificateModel
            .imprimirCertificados({
              escualaId: formdata.escuela.$viewValue,
              fechaInicio: fechaIni,
              fechaFin: fechaFin
            })
            .$promise.then(
            function (data) {
                $scope.solicitudesDerivadas = [];
                angular.forEach(data, function (value, key) {
                    value["habilitado"] = true;
                    value["estado_id"] = 0;
                    value["estado_descripcion"] = "Por Generar";
                    this.push(value);
                }
                , $scope.solicitudesDerivadas);

                NProgress.done();
                button.value = "Buscar Solicitudes";
                button.removeAttribute("disabled");
            },
            function (error) {
                console.log('escuelas_model_office', error);
            }
          );
      }
    
    $scope.passToListPaso2 = function (solicitud) {
        solicitud["habilitado"] = false;
        $scope.solicitudesSeleccionadas.push(solicitud);
      }
    
    $scope.removeFromSelectedPaso2 = function (solicitud) {
        solicitud["habilitado"] = true;
        $scope.solicitudesSeleccionadas = $scope.solicitudesSeleccionadas.filter(function (obj) { if (obj.IdSolicitud !== solicitud.IdSolicitud) { return obj; } });
      }
    $scope.imprimirCertificados = function (frm) {
          //errores
          $scope.mostrarError = false;
          $scope.errores = [];

          if (frm.$invalid) {
              return true;
          }

          if ($scope.solicitudesSeleccionadas.length === 0) {
              var er = new Object();
              er["idError"] = 1;
              er["descripcion"] = "No se seleccionaron los solicitudes";
              $scope.errores.push(er);
              $scope.mostrarError = true;
              return true;
          }

          var button = document.getElementById("button-procesar");
          button.value = "Generando Certificados ...";
          button.disabled = "disabled";
          $scope.solicitudesSeleccionadas.filter(
                    function (obj) {
                        obj.datos_completos = false;
                        obj.status = 0;
                    }
                  );
          $scope.listSolicitudesSeleccionadas = [];

          angular.forEach($scope.solicitudesSeleccionadas,
            function (value, key) {
              var sol = new Object();
              sol["IDAlumno"] = value.IdAlumno;
              sol["IDEscuela"] = value.IdEscuela;
              sol["IDDependencia"] = value.IdDependencia;
              sol["IDSede"] = value.IdSede;
              sol["IdSolicitud"] = value.IdSolicitud;
              this.push(sol);
            },
          $scope.listSolicitudesSeleccionadas);

          var i=0;
          function EnviarCertificado(){
            //progress
            NProgress.configure({ parent: "#bar-loading-constancias" });
            NProgress.start();

            PrintCertificateModel
            .imprimirCertificados({
              IDAlumno : $scope.listSolicitudesSeleccionadas[i].IDAlumno,
              IDEscuela : $scope.listSolicitudesSeleccionadas[i].IDEscuela,
              IDDependencia : $scope.listSolicitudesSeleccionadas[i].IDDependencia,
              IDSede : $scope.listSolicitudesSeleccionadas[i].IDSede,
              IdSolicitud : $scope.listSolicitudesSeleccionadas[i].IdSolicitud,
            })
            .$promise.then(
            function (data) {
                console.log(data);
                if ("idError" in data[0]) {
                    $scope.mostrarError = true;
                    angular.forEach(data,
                      function (value, key) {
                          this.push(value);
                      }
                    , $scope.errores);
                }
                else
                {
                    angular.forEach(data, function (value, key) {
                        $scope.solicitudesSeleccionadas.filter(
                          function (obj) {
                              if (obj.IdAlumno === value.id_alumno) {
                                  obj.estado_id=value.estado_id;
                                  obj.estado_descripcion=value.estado_descripcion;
                                  obj.IdSolicitud=value.id_solicitud;
                                  obj.ruta_archivo=value.ruta_archivo;

                                  NProgress.done();
                                  button.value = "Generar Certificados";
                                  button.removeAttribute("disabled");
                                  return obj;
                              }
                          }
                        );
                    });
                }
                NProgress.done();
                button.value = "Generar Certificados";
                button.removeAttribute("disabled");
            },
            function (error) {
                console.log('escuelas_model_office', error);
            })
            .then(function(){
              if( i < $scope.listSolicitudesSeleccionadas.length-1)
              {
                i=i+1;
                EnviarCertificado();
              }
            });
          }

        EnviarCertificado();
      }
  }
]);