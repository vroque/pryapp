'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('CAUModule')
  /*
    * Controllador para generar constancia de egresados
    * @author WReymundo
    * @date 19/08/2016
  */
.controller('generateGraduteRecordCtrl', ['$scope','AlumnosEgresadosModel','ConstanciasEgresadosModel',
  function ($scope,AlumnosEgresadosModel,ConstanciasEgresadosModel) {
    $scope.init = function () {

    };
    $scope.changeStudentArray = function(student_str){
			$scope.student_id_array=student_str
			.split(' ')
			.filter(function (id){return id != ''})
			.filter(function(id,position,self){return self.indexOf(id)==position});
		};
    $scope.iniciarBusqueda=function(formdata){
      if(formdata.$invalid){
        return true;
      }

      NProgress.configure({ parent: "#bar-loading-alumnos" });
      NProgress.start();
      var button = document.getElementById("button-buscar");
      button.value="Buscando";
      button.disabled="disabled";

      AlumnosEgresadosModel
        .query({student_id_list: $scope.search.data_student_id_list,mentirita:'jaja'})
        .$promise.then(
        function (data) {
          $scope.alumnos= [];
          angular.forEach(data,function(value,key)
              {
                value["language"]=value["language"]=='1'?true:value["language"];
                value["language"]=value["language"]=='0'?false:value["language"];
                value["social_projection"]=value["social_projection"]=='1'?true:value["social_projection"];
                value["social_projection"]=value["social_projection"]=='0'?false:value["social_projection"];
                value["practice"]=value["practice"]=='1'?true:value["practice"];
                value["practice"]=value["practice"]=='0'?false:value["practice"];
                value["bachiller"]=value["bachiller"]=='1'?true:value["bachiller"];
                value["bachiller"]=value["bachiller"]=='0'?false:value["bachiller"];
                value["degree"]=value["degree"]=='1'?true:value["degree"];
                value["degree"]=value["degree"]=='0'?false:value["degree"];

                value["habilitado"] = true;
                value["status"] = 0;
                //value["datos_completos"] = false;
                value["ruta_archivo"] = "";
                //value["periodo_str_anterior"] = "";
                //value["periodo_str_proximo"] = "";

                value["success"]=value["language"]  && value["practice"] && value["social_projection"] && value["bachiller"] != true && value["degree"] != true;
                value["info"]= value["language"]  && value["practice"] && value["social_projection"]  && value["bachiller"];
                value["warning"]= !value["language"]  || !value["practice"]  || !value["social_projection"];
                value["danger"]=value["language"] === null && value["practice"] ===null && value["social_projection"] ===null;
                this.push(value);
              }
              ,$scope.alumnos);
              $scope.alumnos.forEach(function(item,index){
                item.success=function(){return item.language  && item.practice && item.social_projection && item.bachiller != true && item.degree != true;}
                item.info=function(){return item.language  && item.practice && item.social_projection  && item.bachiller;}
                item.warning=function(){return !item.language  || !item.practice  || !item.social_projection;}
                item.danger=function(){return item.language === null && item.practice ===null && item.social_projection ===null;}
              });
              $scope.alumnosSeleccionados=[];
              NProgress.done();
              button.value="Buscar";
              button.removeAttribute("disabled");
        },
        function (error) {
            console.log('escuelas_model_office', error);
        }
      );
    };
    $scope.passToList=function(alumno){
      alumno["habilitado"]=false;
      $scope.alumnosSeleccionados.push(alumno);
    }
    $scope.removeFromSelected=function(alumno){
      alumno["habilitado"]=true;
      $scope.alumnosSeleccionados=$scope.alumnosSeleccionados.filter(function(obj){if(obj.student_id!==alumno.student_id){return obj;}});
    };
    $scope.updateDates=function(fechas){
      $scope.alumnosSeleccionados.forEach(function(item,index){
        item.fecha=fechas
      });
    };
    $scope.procesarConstancias=function(frm){
      //console.log(frm);

      //progress
      NProgress.configure({ parent: "#bar-loading-constancias" });
      NProgress.start();
      var button = document.getElementById("button-procesar");
      button.value="Procesando";
      button.disabled="disabled";

      $scope.alumnosToSend=[];
      $scope.alumnosSeleccionados.forEach(function(item,index){
        $scope.alumnosToSend.push({student_id:item.student_id,graduate_date:frm["fecha-"+item.student_id]['$viewValue']})
      });
      //console.log($scope.alumnosToSend);

      ConstanciasEgresadosModel
        .procesarConstancias($scope.alumnosToSend)
        .$promise.then(
        function (data) {
          console.log(data);
          NProgress.done();
          button.value="Procesar";
          button.removeAttribute("disabled");
          $scope.alumnosSeleccionados.forEach(function(item){
            data.forEach(function(it){
              if(it.student_id==item.student_id){
                item.has_date=it.has_date;
                item.has_photo=it.has_photo;
                if(!(it.has_photo==false || it.has_date==false))
                {
                  item.file=it.file;
                }
                else{
                  item.file='';
                }
              }
            })
          }),
          console.log($scope.alumnosSeleccionados);
        },
        function (error) {
            console.log('escuelas_model_office', error);
        }
      );

    }

  }
]);
