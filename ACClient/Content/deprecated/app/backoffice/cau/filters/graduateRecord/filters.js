'use strict';
/* jshint -W097 */
/* global angular, window */

/* global angular, api_base */
angular.module('CAUModule')
.filter('statusToTextGraduate',function(){
  return function(status){

    var status_text={
                     0 : "Por verificar",
                     1 : "Finalizado",
                    }
    return status_text[status];
  }
});
