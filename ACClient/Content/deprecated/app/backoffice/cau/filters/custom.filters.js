'use strict';
/* jshint -W097 */
/* global angular, window */

/* global angular, api_base */
angular.module('CAUModule')
.filter('statusToTextConstancy',function(){
  return function(status){

    var status_text={
                     0 : "Por verificar",
                     1 : "Finalizado",
                     2 : "Periodo Anterior", //cuando se encontro un periodo anterior
                     3 : "Periodo Proximo" //cuando  no se encontro un periodo anterior solo posteriores
                    }

    return status_text[status];
  }
})
.filter('statusToTextCertificate',function(){
  return function(status){
    var status_text={
                     0 : "Por verificar",
                     3 : "Pendiente de Entrega",
                     1 : "Pendiente de Entrega",
                     2 : "Rechazado"
                    }

    return status_text[status];
  }
})
.filter('unique',function(){
  return function (data, propertyName) {
    if (angular.isArray(data) && angular.isString(propertyName)) {
      var results = [];
      var keys = {};
      for (var i = 0; i < data.length; i++) {
      var val = data[i][propertyName];
        if (angular.isUndefined(keys[val])) {
          keys[val] = true;
          results.push(val);
        }
      }
      return results;
    } 
    else 
    {
      return data;
    }
  }
});