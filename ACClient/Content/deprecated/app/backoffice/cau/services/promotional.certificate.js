﻿'use strict';
/* jshint -W097 */
/* global angular, window */

/* global angular, api_base */
angular.module('CAUModule')
.factory('AlumnosEgresadosModel', ['$resource',
    function ($resource) {
        return $resource(window.url_base + 'backoffice/api/centro/atencion/alumnos/egresados/:id', { id: '@id' });
    }
])
.factory('EscuelasModel', ['$resource',
    function ($resource) {
        return $resource(window.url_base + 'backoffice/api/centro/atencion/escuelas/academicas/', {});
    }
])
.factory('CertificateModel', ['$resource',
    function ($resource) {
        return $resource(
          window.url_base + 'backoffice/api/centro/atencion/documents/certificado/promocional/sin/deuda/', {},
          { 'procesarCertificados': { method: 'POST', isArray: true } });
    }
])
.factory('PrintCertificateModel', ['$resource',
    function ($resource) {
        return $resource(
          window.url_base + 'backoffice/api/centro/atencion/documents/certificado/promocional/sin/deuda/impresion/:escualaId', {escualaId:'@escualaId'},
          { 'imprimirCertificados': { method: 'POST', isArray: true } });
    }
]);
