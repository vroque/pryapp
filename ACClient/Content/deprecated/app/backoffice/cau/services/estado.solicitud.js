'use strict';
/* jshint -W097 */
/* global angular, api_base */

angular.module('CAUModule')
    .factory('estadoSolicitudModel', ['$resource', '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
            $rootScope.api_base + 'solicitud-estado/'
            );
        }]);
﻿
