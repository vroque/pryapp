'use strict';
/* jshint -W097 */
/* global angular, api_base */


/*
  * factories for C5
  * @author Wanshi R.C.
  * @date 1/07/2016
*/
angular.module('CAUModule')
.factory('SendToC5Model', ['$resource','$rootScope',
// send applications' data to C5 tables in db
    function ($resource,$rootScope) {
        return $resource($rootScope.url_base  + 'api/documentary/c5/');
    }])
.factory('CleanC5Model', ['$resource','$rootScope',
//update data in C5 tables
        function ($resource,$rootScope) {
            return $resource($rootScope.url_base  + 'api/documentary/c5/');
        }
]);
