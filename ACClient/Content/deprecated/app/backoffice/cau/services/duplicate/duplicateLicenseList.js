'use strict';
/* jshint -W097 */
/* global angular, api_base */

/*
  * factories for the duplicate student card
  * @author Wanshi R.C.
  * @date 26/05/2016
*/
angular.module('CAUModule')
.factory('DuplicateListModel', ['$resource','$rootScope',
/*list of duplicate student card applications for state */
    function ($resource,$rootScope) {
        return $resource($rootScope.api_base + 'duplicado/carne/list/',{state:'@state'});
    }
])
.factory('MailModel', ['$resource','$rootScope',
/*factory for sending mails*/
    function ($resource,$rootScope) {
        return $resource($rootScope.url_base + 'api/documentary/sendMail');
    }
])
;
