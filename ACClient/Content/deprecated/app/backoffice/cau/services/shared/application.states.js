'use strict';
/* jshint -W097 */
/* global angular, api_base */

/*
  * factories for application states
  * @author Wanshi R.C.
  * @date 26/05/2016
*/
angular.module('CAUModule')
.factory('ApplicationStateModel', ['$resource','$rootScope',
/*bring all states*/
    function ($resource,$rootScope) {
        return $resource($rootScope.url_base + 'backoffice/centro/de/atencion/get/solicitudes/estado/');
    }
])
.factory('ChangeStateModel', ['$resource','$rootScope',
/*change states in a group of applications*/
    function ($resource,$rootScope) {
        return $resource($rootScope.url_base  + 'api/documentary/applicationsCAU/');
    }
])
;
