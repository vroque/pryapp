'use strict';
/* jshint -W097 */
/* global angular, api_base */

/*
  * factories for application states
  * @author Wanshi R.C.
  * @date 26/05/2016
*/
angular.module('CAUModule')
.factory('ApplicationDocumentModel', ['$resource','$rootScope',
/*bring all documents*/
    function ($resource,$rootScope) {
        return $resource($rootScope.url_base + 'backoffice/centro/de/atencion/get/documento/');
    }
])
;
