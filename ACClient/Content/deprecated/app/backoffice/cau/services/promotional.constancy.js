﻿'use strict';
/* jshint -W097 */
/* global angular, window */

/* global angular, api_base */
angular.module('CAUModule')
.factory('AlumnosEgresadosModel', ['$resource',
    function ($resource) {
        return $resource(window.url_base + 'backoffice/api/centro/atencion/alumnos/egresados/:id', { id: '@id' });
    }
])
.factory('EgresadoIndividualModel', ['$resource',
    function ($resource) {
        return $resource(window.url_base + 'backoffice/api/centro/atencion/alumnos/egresado/individual/:id', { id: '@id' });
    }
])
.factory('EscuelasModel', ['$resource',
    function ($resource) {
        return $resource(window.url_base + 'backoffice/api/centro/atencion/escuelas/academicas/', {});
    }
])
.factory('ConstanciasModel', ['$resource',
    function ($resource) {
        return $resource(
          window.url_base + 'backoffice/api/centro/atencion/documents/constancias/promocionales/',
          {},
          {'procesarConstancias':  {method:'POST',isArray:true}});
    }
]);
