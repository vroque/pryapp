﻿'use strict';
/* jshint -W097 */
/* global angular, window */

/* global angular, api_base */
angular.module('CAUModule')
.factory('AlumnosEgresadosModel', ['$resource',
    function ($resource) {
        return $resource(window.url_base + 'backoffice/api/centro/atencion/alumnos/egresados/:id', { id: '@id' });
    }
])
.factory('ConstanciasEgresadosModel', ['$resource',
    function ($resource) {
        return $resource(window.url_base + 'backoffice/api/centro/atencion/documents/constancias/egresados/',
          {},
          {'procesarConstancias':  {method:'POST',isArray:true}});
    }
]);
