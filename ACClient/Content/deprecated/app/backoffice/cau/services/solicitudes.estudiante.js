﻿'use strict';
/* jshint -W097 */
/* global angular, api_base */

angular.module('CAUModule')
    .factory('solicitudesEstudianteModel', ['$resource', '$rootScope',
        function ($resource, $rootScope) {
            return $resource(
            $rootScope.api_base + 'solicitud/:alumno_id',
            { alumno_id: '@alumno_id' }
            );
        }]);
