'use strict';
/* jshint -W097 */
/* global angular, api_base */


/*
  * factories for C5
  * @author Wanshi R.C.
  * @date 1/07/2016
*/
angular.module('CAUModule')
.factory('ApplicationByStudentModel', ['$resource','$rootScope',
/*search for student's applications*/
    function ($resource,$rootScope) {
        return $resource($rootScope.url_base  + 'api/documentary/ReportApplicationsByStudent/');
    }
])
;
