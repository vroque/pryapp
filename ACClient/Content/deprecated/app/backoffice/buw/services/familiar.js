'use strict';
/* jshint -W097 */
/* global window, angular, api_base */

angular.module('BUWModule')
/**
 * DEPRECATED
 * Modelo relaciones familiares del Estudiante
 * @author Arturo Bolaños
 * @date 17/04/2016
 * @version 1.0
*/
.factory('FamiliarModel', ['$resource', '$rootScope',
  function ($resource, $rootScope) {
    return $resource(
      $rootScope.api_base + 'relacion/familiar/:student_id/:who/:relation_type',
      {student_id: '@student_id', who: '@who', relation_type: '@relation_type'}
    );
  }
]);
