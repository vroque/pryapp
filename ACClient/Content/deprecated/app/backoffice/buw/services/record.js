'use strict';
/* jshint -W097 */
/* global angular,  window */

angular.module('BUWModule')
  /**
   * Modelo para Ficha socieconomica
   * Obtiene toda la ficha socieconomica
   * del alumno logiado o del alumno cuyo codigo se envie
   * o del codigo de estudiante que se envie
   * @author Arturo Bolaños
   * @date 17/06/2016
   * @version 1.0
   * @param id string codigo del alumno del cual obtener la ficha
  */
  .factory('VerificationRecordModel', ['$resource',
    function ($resource) {
      return $resource(window.url_base + 'api/personal/VerificationRecord/:id', {id: '@id'});
    }
  ])
  /**
   * Modelo de dato unico de la ficha
   * Trae o guarda un dato unico de la ficha
   * @author Arturo Bolaños
   * @date 17/06/2016
   * @version 1.0
   * @param id string codigo del alumno
  */
  .factory('VRDataModel', ['$resource',
    function ($resource) {
      return $resource(window.url_base + 'api/personal/VRData/:id', {id: '@id'});
    }
  ])
  /**
   * Modelo de Observaciones para la verificacion
   * de la ficha socieconomica
   * @author Arturo Bolaños
   * @date 17/06/2016
   * @version 1.0
   * @param id string codigo del alumno
  */
  .factory('VRObservationModel', ['$resource',
    function ($resource) {
      return $resource(window.url_base + 'api/personal/VRObservation/:id', {id: '@id'});
    }
  ])
  /**
   * Modelo de perfil academico del estudiante
   * Trae la informacion academica de un estudiante
   * @author Arturo Bolaños
   * @date 17/06/2016
   * @version 1.0
   * @param id string codigo del alumno
  */
  .factory('AcademicProfileModel', ['$resource', '$rootScope',
    function ($resource, $rootScope) {
      return $resource(
        window.url_base + 'api/academic/profile/:id', {id: '@id'}
      );
    }
  ])
  /**
   * Modelo para Ficha socieconomica
   * modulo de Datos de relaciones familiares
   * @author Arturo Bolaños
   * @date 20/06/2016
   * @version 1.0
  */
  .factory('VRFamiliarRelationModel', ['$resource',
    function ($resource) {
      return $resource(window.url_base + 'api/personal/VRFamiliarRelation/:id', {id: '@id'});
    }
  ]);
