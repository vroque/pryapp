﻿'use strict';
/* jshint -W097 */
/* global angular, require, window */
angular.module('centro-atencion')
.config(['$ocLazyLoadProvider', '$stateProvider', '$urlRouterProvider',
  function ($ocLazyLoadProvider, $stateProvider, $urlRouterProvider) {
    $ocLazyLoadProvider.config({ asyncLoader: require });
    $urlRouterProvider.otherwise('/bienestar/ficha');
    $stateProvider
    .state('buw', {
      url: '/bienestar',
      controller: 'buwCtrl',
      //templateUrl: window.app_base + 'views/familiarRelation.html',
       resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'BUWModule',
            serie: true, // used for synchronous load chart scripts
            insertBefore: '#ngInsertBefore',
            files: [
              window.app_base + 'buw/module.js',
              window.app_base + 'buw/controllers/buw.js',
            ]
          });
        }
      }
    })
    .state('buw.record', {
      url: '/ficha',
      views: { '@':
        {
          controller: 'buwRecordCtrl',
          templateUrl: window.app_base + 'buw/views/record.choseStudend.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'BUWModule',
            serie: true, // used for synchronous load chart scripts
            insertBefore: '#ngInsertBefore',
            files: [
              window.app_base + 'buw/module.js',
              window.app_base + 'buw/services/record.js',
              window.app_base + 'buw/controllers/record.js'
            ]
          });
        }
      }
    })
    .state('buw.record.student', {
      url: '/:student_id',
      views: { '@':
        {
          controller: 'buwRecordStudentCtrl',
          templateUrl: window.app_base + 'buw/views/record.full.html'
        }
      },
    });

  }
]);
