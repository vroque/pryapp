'use strict';
/* jshint -W097 */
/* global window, angular, $, _ */

angular.module('BUWModule')
  /*
    * Controllador para la entrada a la ficha de asesor de bienestar
    * Busca alumno par mostrar su ficha
    * @author Arturo Bolaños
    * @date 08/09/2016
  */
  .controller('buwRecordCtrl', ['$scope', 'StudentModel',
    function ($scope, StudentModel) {
      $scope.init = function(){};
      /*
       * Buscamos estudiantes para ver su ficha
      */
      $scope.searchStudent = function (student_id) {
        StudentModel.get(
          {id: student_id, by: 'student_id'}
        ).$promise.then(function(data){
          $scope.alumno = data;
        }, function(err){
          $scope.result = -1;
          //$scope.show_errors(err, 'no encontrado');
        });
      };
    }
  ])
  .controller('buwRecordStudentCtrl', [
    '$scope',
    '$stateParams',
    'AcademicProfileModel',
    'VerificationRecordModel',
    'VRDataModel',
    'VRObservationModel',
    function ($scope, $stateParams,
              AcademicProfileModel,
              VerificationRecordModel,
              VRDataModel, VRObservationModel) {
      var aportant_tpl = window.url_base +
                        'Content/app/backoffice/buw/views/aportant.html';
      var dependent_tpl = window.url_base +
                          'Content/app/backoffice/buw/views/dependent.html';
      $scope.observation = 'sss';

      $scope.init = function(){
        $scope.mode = 'read';
        $scope.observations = [];
        $scope.observation = '';
        $scope.tpl = '';

        AcademicProfileModel.get({id: $stateParams.student_id}).$promise
        .then(
          function(data){
            $scope.profiles = [data];
            $scope.profile = data;
          }, function(err){ }
        )
        .finally(function(){
          VerificationRecordModel.get({id: $stateParams.student_id}).$promise
          .then(
            function(data){
              $scope.soc_record = data;
              $scope.initialize();
            },
            function(error){}
          );
        });
      };

      $scope.save = function(level,key, value, form){
        form = form || {};
        form.saved = false;
        var data = new VRDataModel({
          person_id:  $scope.soc_record.personal.id,
          level:  level,
          key:  key,
          value: value
        });
        if(form.$invalid) return  0;
        data.$save(function(data){
          form.saved = true;
          form.update_at = data.time;
        }, function(err){
          form.falied = true;
        });
      };

      $scope.calcTotal =  function(list){
        var total = 0;
        for(var key in list){
          total += parseInt(list[key]) || 0;
        }
        return total;
      };

      $scope.workinChCategory = function(cat, form){
        $scope.soc_record.working.category_dep = '0';
        $scope.soc_record.working.category_ind = '0';
        $scope.soc_record.working.category_cj = '0';
        switch (cat) {
          case '1':
            $scope.soc_record.working.category_dep = '1';
            $scope.save('working', 'category_dep', 1, form);
            $scope.save('working', 'category_ind', 0);
            $scope.save('working', 'category_cj', 0);
            break;
          case '2':
            $scope.soc_record.working.category_ind = '1';
            $scope.save('working', 'category_ind', 1, form);
            $scope.save('working', 'category_dep', 0);
            $scope.save('working', 'category_cj', 0);
            break;
          case '3':
            $scope.soc_record.working.category_cj = '1';
            $scope.save('working', 'category_cj', 1, form);
            $scope.save('working', 'category_dep', 0);
            $scope.save('working', 'category_ind', 0);
            break;
        }
      };

      $scope.addObservation = function(obs){
        var obj = new VRObservationModel({observation: obs});
        obj.person_id = $scope.soc_record.personal.id;
        obj.date = new Date();
        obj.$save(
          function(data){
            $scope.soc_record.observations.unshift(data);
            $scope.soc_record.observation = '';
          },function(err){

          }
        );
      };

      $scope.closeRecord = function(){
        $scope.mode = 'read';
        $scope.addObservation('REVISION DE FICHA CERRADA.');
      };

      $scope.openRecord = function(){
        $scope.mode = 'write';
        $scope.addObservation('REAPERTURA DE LA FICHA.');
      };

      $scope.showFamiliar = function(familiar){
        $scope.show_familiar = true;
        if(familiar.economic_type ===  'D'){
          $scope.showDependent(familiar);
        }
        else if(familiar.economic_type ===  'A'){
          $scope.showAportant(familiar);
        }
      };

      $scope.showAportant = function(person){
        $scope.aportant = person;
        $scope.tpl = aportant_tpl;
      };

      $scope.showDependent = function(person){
        $scope.dependent = person;
        $scope.tpl = dependent_tpl;
      };

      $scope.closeFamiliar = function(){
        $scope.show_familiar = false;
      };

      $scope.initialize = function(){
        if($scope.soc_record.status){
          $scope.mode = 'write';
        }
        //income and expenses
        for(var i in $scope.soc_record.income)
          if ($scope.soc_record.income[i] === 0) $scope.soc_record.income[i] = null;

        for(var j in $scope.soc_record.expenses)
          if ($scope.soc_record.expenses[j] === 0) $scope.soc_record.expenses[j] = null;
        //working
        if($scope.soc_record.working.category_dep == '1') $scope.soc_record.working.category = '1';
        if($scope.soc_record.working.category_ind == '1') $scope.soc_record.working.category = '2';
        if($scope.soc_record.working.category_cj == '1') $scope.soc_record.working.category = '3';
        //sort observations
        $scope.soc_record.observations =  _.sortBy($scope.soc_record.observations, function(obj){ return obj.date; }).reverse();
        //ini observation variable
        $scope.soc_record.observation = '';
      };

    }
  ])
  .controller('dependentVerificationCtrl', ['$scope', '$state', '$stateParams', 'VRFamiliarRelationModel',
  function ($scope, $state, $stateParams, VRFamiliarRelationModel) {
      $scope.$parent.$watch('dependent', function(value){
        $scope.o_dependent = $scope.$parent.dependent;
        $scope.dependent = angular.copy($scope.o_dependent);
      });
      $scope.person_id = $scope.$parent.soc_record.personal.id;
      // $scope.student_id = $scope.$parent.profile.student_id;
      $scope.init = function(){
        $scope.o_dependent = $scope.$parent.dependent;
        $scope.dependent = angular.copy($scope.o_dependent);

      };
      $scope.cancel = function(){
        $scope.dependent = angular.copy($scope.o_dependent);
        $scope.mode = 'read';
      };
      $scope.save = function(familiar){
        var data = new VRFamiliarRelationModel(familiar);
        data.id = $scope.person_id;
        data.$save()
        .then(function(res)  {
          familiar.person_id = res.person_id;
          $scope.o_dependent = angular.copy($scope.dependent);
          $scope.updateList(familiar);
        });
        //.catch(function(req) { console.log('error saving obj', req); })
        //.finally(function()  { console.log('always called'); });
      };
      $scope.updateList = function(familiar){
        for(var key in $scope.$parent.soc_record.relations){
          if($scope.$parent.soc_record.relations[key].person_id == familiar.person_id){
            $scope.$parent.soc_record.relations[key] = familiar;
            return 1;
          }
        }
        $scope.$parent.soc_record.relations.push(familiar);
        return 1;
      };

    }
  ])

  .controller('aportantVerificationCtrl', ['$scope', '$state', '$stateParams', 'VRFamiliarRelationModel',
    function ($scope, $state, $stateParams, VRFamiliarRelationModel) {
        $scope.$parent.$watch('dependent', function(value){
          $scope.o_dependent = $scope.$parent.dependent;
          $scope.dependent = angular.copy($scope.o_dependent);
        });
        $scope.person_id = $scope.$parent.soc_record.personal.id;
        $scope.student_id = $scope.$parent.profile.student_id;

        $scope.init = function(){
            $scope.o_aportant = $scope.$parent.aportant;
            $scope.o_aportant.birth_country = $scope.o_aportant.birth_country || '170'; // Perú
            $scope.o_aportant.birth_country = ($scope.o_aportant.birth_country === '')? '170':$scope.o_aportant.birth_country; // Perú
            if($scope.o_aportant.category_dep == '1') $scope.o_aportant.category = '1';
            if($scope.o_aportant.category_ind == '1') $scope.o_aportant.category = '2';
            if($scope.o_aportant.category_cj == '1') $scope.o_aportant.category = '3';
            $scope.aportant = angular.copy($scope.o_aportant);
        };
        $scope.save = function(familiar){
          var data = new VRFamiliarRelationModel(familiar);
          data.id = $scope.person_id;
          data.$save()
          .then(function(res)  {
            familiar.person_id = res.person_id;
            $scope.o_aportant = angular.copy($scope.aportant);
            $scope.updateList(familiar);
            $scope.mode = 'read';
            $scope.$parent.updateAllPercent();
          });
        };
        $scope.cancel = function(){
          $scope.aportant = angular.copy($scope.o_aportant);
          $scope.mode = 'read';
        };
        $scope.chCatgory = function(cat){
          $scope.aportant.category_dep = '0';
          $scope.aportant.category_ind = '0';
          $scope.aportant.category_cj = '0';
          switch (cat) {
            case '1':
              $scope.aportant.category_dep = '1';break;
            case '2':
              $scope.aportant.category_ind = '1';break;
            case '3':
              $scope.aportant.category_cj = '1';break;
          }
        };
        $scope.updateList = function(familiar){
          for(var key in $scope.$parent.soc_record.relations){
            if($scope.$parent.soc_record.relations[key].person_id == familiar.person_id){
              $scope.$parent.soc_record.relations[key] = familiar;
              return 1;
            }
          }
          $scope.$parent.soc_record.relations.push(familiar);
          return 1;
        };
      }
    ]);
