'use strict';
/* jshint -W097 */
/* global angular */

angular.module('BUWModule')
/*
  * Controllador principal para buw
  * @author Arturo Bolaños
  * @date 08/09/2016
*/
  .controller('buwCtrl', ['$scope',
    function ($scope) {
      console.log('LOAD BUW');
    }
  ]);
