'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('BUWModule')
  /*
    * Controllador para manejar relaciones familiares
    * de la ficha socioeconomica
    * @author Arturo Bolaños
    * @date 07/03/2016
  */
.controller('familiarCtrl', ['$scope', 'FamiliarModel', 'StudentModel', 'PersonModel',
  function ($scope, FamiliarModel, StudentModel, PersonModel) {
    $scope.init = function(){
      $scope.date = new Date();
    };
    /*
      Busqueda de estudiante por codigo
    */
    $scope.searchStudent = function(student_id){
      StudentModel.get(
        {id: student_id}
      ).$promise.then(function(data){
        $scope.alumno = data;
          $scope.searchFamiliar(student_id);
      }, function(err){
        $scope.result = -1;
        $scope.show_errors(err, 'no encontrado');
      });
    };

    $scope.searchFamiliar = function(student_id){
      FamiliarModel.query({student_id: student_id}).$promise
      .then(function(data){
        if(data.length > 0){
          $scope.setFamiliars(data);
          $scope.result = 1;
        }
        else {
          $scope.result = 0;
        }
      }, function(err){
        $scope.result = -1;
        $scope.show_errors(err, 'no encontrado');
      });
    };
    /*
     * Trae 1 a 1 los datos de los familiares
     * esto se hace por la falta de performas de la tabla de personas en la DB
    */
    $scope.setFamiliars = function(data){
      $scope.familiars = data;
      for(var i = 0; i < data.length; i++){
        $scope.familiars[i].person = PersonModel.get(
          {id: data[i].who.trim()}
        );
      } // end for
    };

    /*
     * eliminar relacion falimiar
    */
    $scope.removeFamiliar = function(relation){
      var rel = angular.copy(relation);
      delete(rel.person);
      FamiliarModel.remove(rel).$promise
      .then(
        function(data){
          $scope.familiars = _.without($scope.familiars, relation);
        },
        function(err){
          $scope.show_errors(err, 'no eliminado');
        }
      );
    };

    /**/
    $scope.clean = function(){
      $scope.alumno = false;
      $scope.familiars = null;
      $scope.result = 1;
    };
  }
]);
