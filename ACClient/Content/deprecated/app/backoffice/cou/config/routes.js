﻿'use strict';
/* jshint -W097 */
/* global angular, require, window */

/**
 * Rutas para Convalidaciones (solo version en angular)
**/
angular.module('centro-atencion')
.config(['$ocLazyLoadProvider', '$stateProvider', '$urlRouterProvider',
  function ($ocLazyLoadProvider, $stateProvider, $urlRouterProvider) {
    $ocLazyLoadProvider.config({ asyncLoader: require });
    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('simulation', {
      url: '/simulacion',
      controller: 'simulationCtrl',
      templateUrl: window.url_base + 'Content/app/backoffice/cou/views/simulation.html',
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'COUModule',
            serie: true, // used for synchronous load chart scripts
            insertBefore: '#ngInsertBefore',
            files: [
              window.url_base + 'Content/app/backoffice/cou/module.js',
              window.url_base + 'Content/app/backoffice/cou/controllers/simulation.js'
            ]
          });
        }
      }
    })
    .state('simulation.table',{
      url: '/tabla/:student_id',
      controller: 'simulationTableCtrl',
      templateUrl: window.url_base + 'Content/app/backoffice/cou/views/simulationTable.html',
    });
  }
]);
