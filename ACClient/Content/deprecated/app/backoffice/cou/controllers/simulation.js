﻿'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('COUModule')
  /*
    * Controllador para manejar relaciones familiares
    * de la ficha socioeconomica
    * @author Arturo Bolaños
    * @date 07/03/2016
  */
.controller('simulationCtrl', ['$scope', 'StudentModel',
  function ($scope, StudentModel) {
    $scope.init = function(){};
    $scope.searchStudent = function(student_id){
      StudentModel.get(
        {id: student_id}
      ).$promise.then(function(data){
        $scope.alumno = data;
      }, function(err){
        $scope.result = -1;
        //$scope.show_errors(err, 'no encontrado');
      });
    };
  }

]);

/*
    $scope.init = function() {
      $scope.date = new Date();
      $scope.ciclos = [1,2,3,4,5,6,7,8,9,10];
      $scope.ciclos_letras = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
    };
    $scope.findStudent = function(code){
      AcademicProfileModel.query({id: code}).$promise
      .then(
        function(data){
          $scope.profiles = data;
          $scope.equivalencyTable(code, data[0].academic_school.id, data[0].study_plan.id, "B002.01");
          $scope.academicProgress(code, data[0].study_plan.id, data[0].academic_school.id);
          //$scope.newPlanProgress(code, data[0].academic_school.id);
          $scope.coruseTrack(data[0].study_plan.id, data[0].academic_school.id, function(track){
            $scope.p1_track = track;
          });
          $scope.coruseTrack("B002.01", data[0].academic_school.id, function(track){
            $scope.p2_track = track;
          });
        },
        function(err){}
      );

    };
    $scope.academicProgress = function(code, study_plan, academic_school){
      AcademicProgressModel.get(
        {
          student_id: code,
          study_plan: study_plan,
          academic_school: academic_school
        }
      )
      .$promise
      .then(
        function(data){
          $scope.academic_progress = data.courses;
        },
        function(err){}
      );
    };
    $scope.coruseTrack = function(study_plan, academic_school, callback){
      CourseTrackModel.get({study_plan: study_plan, academic_school: academic_school})
      .$promise
      .then(
        callback,
        function(err){}
      );
    };
    $scope.sumCredits = function(courses){
      var cant = 0;
      for(var i in courses) {
        if( courses[i].score >= 11)
          cant += courses[i].credits;
      }
      return cant;
    };
    $scope.validationEquivalency = function(schoo_id, study_plan_id, destination_study_plan_id){
      ValidationEquivalencyModel.get({
        schoo_id: schoo_id,
        study_plan_id: study_plan_id,
        destination_study_plan_id: destination_study_plan_id
      }).$promise.then(
        function(data){
          $scope.equivalency_table = data;
        },
        function(err){}
      );
    };
    $scope.equivalencyTable = function(student_id,school_id,study_plan,new_study_plan){
      EquivalencyTableModel.query({student_id:student_id, school_id:school_id,  study_plan:study_plan, new_study_plan:new_study_plan }).$promise
      .then(
        function(data){
          $scope.equivalency_table = data;
        }, function(err){}
      );
    };
  }])
  .controller('simulationTableCtrl', ['$scope',
    function($scope) {
      $scope.init = function () {
        $scope.date = new Date();
      };
    }]);
*/
