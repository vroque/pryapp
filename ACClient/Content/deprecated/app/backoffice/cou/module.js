﻿'use strict';
/* jshint -W097 */
/* global angular, window */

/**
 * Modulo para COnvalidaciones
**/
angular.module('COUModule', [])
.run(['$rootScope', function ($rootScope) {
    $rootScope.url_base = window.url_base;
}]);
