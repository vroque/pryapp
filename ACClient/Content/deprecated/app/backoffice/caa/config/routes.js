﻿'use strict';
/* jshint -W097 */
/* global angular, require, window */
angular.module('centro-atencion')
.config(['$ocLazyLoadProvider', '$stateProvider', '$urlRouterProvider',
  function ($ocLazyLoadProvider, $stateProvider, $urlRouterProvider) {
    $ocLazyLoadProvider.config({ asyncLoader: require });
    $urlRouterProvider.otherwise('/panel');
    $stateProvider
    .state('panel', {
      url: '/panel',
      controller: 'panelCtrl',
      templateUrl: window.app_base + 'views/panel.html',
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'CAAModule',
            serie: true, // used for synchronous load chart scripts
            insertBefore: '#ngInsertBefore',
            files: [
              window.app_base + 'module.js',
              window.app_base + 'controllers/panel.js'
            ]
          });
        }
      }
    });
  }
]);
