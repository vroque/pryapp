'use strict';
/* jshint -W097 */
/* global angular */

angular.module('CAAModule')
  /*
    * Controllador de lla parte de Fechas de Horarios
    * para el proyecto de televisores
    * @author Arturo Bolaños
    * @date 27/04/2016
  */
  .controller('panelCtrl', ['$scope',
    function($scope){
      $scope.init = function(){
        var days = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
        var now = new Date();
        $scope.day = days[now.getDay()];

      };
    }
  ]);
