﻿'use strict';
/* jshint -W097 */
/* global angular, require, window */
angular.module('centro-atencion')
.config(['$ocLazyLoadProvider', '$stateProvider', '$urlRouterProvider',
  function ($ocLazyLoadProvider, $stateProvider, $urlRouterProvider) {
      $ocLazyLoadProvider.config({ asyncLoader: require });
      $urlRouterProvider.otherwise('/');
      $stateProvider
      .state('consolidated', {
          url: '/consolidado',
          controller: 'consolidatedCtrl',
          templateUrl: window.app_base + 'views/consolidated.html',
          resolve: {
              load: function ($ocLazyLoad) {
                  return $ocLazyLoad.load({
                      name: 'CEUModule',
                      serie: true, // used for synchronous load chart scripts
                      insertBefore: '#ngInsertBefore',
                      files: [
                        window.app_base + 'module.js',
                        window.app_base + '../../../v1/backoffice/js/lib/jquery.table2excel.min.js',
                        window.app_base + 'services/schools.js',
                        window.app_base + 'services/periods.js',
                        window.app_base + 'services/consolidated.js',
                        window.app_base + 'filters/lastSemesterSUNEDU.js',
                        window.app_base + 'controllers/consolidatedController.js'
                      ]
                  });
              }
          }
      })
  }
]);
