'use strict';
/* jshint -W097 */
/* global angular, api_base */


//cambiar ruta
angular.module('CEUModule')
.factory('SchoolsModel', ['$resource',
/*
  * factory for the stadistic center
  * bring schools_id,faculty_id,schools_name
  * @author Wanshi R.C.
  * @date 26/05/2016
*/
    function ($resource) {
        return $resource(window.url_base + 'backoffice/api/centro/atencion/escuelas/academicas/', {});
    }
]);
