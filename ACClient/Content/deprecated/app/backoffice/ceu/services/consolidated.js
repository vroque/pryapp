'use strict';
/* jshint -W097 */
/* global angular, api_base */


//cambiar ruta
angular.module('CEUModule')
.factory('ConsolidatedModel', ['$resource', '$rootScope',
/*
  * factory for the stadistic center
  * bring data from tblAlumnoEgresado
  * @author Wanshi R.C.
  * @date 26/05/2016
*/
  function ($resource, $rootScope) {
    return $resource(
      $rootScope.api_base + 'egresados/consolidado/', {}
    );
  }
]);
