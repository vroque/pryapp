'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('CEUModule')
  /*
    * controller for the stadistic center
    * report: consolidated graduates
    * @author Wanshi R.C.
    * @date 26/05/2016
  */
.controller('consolidatedCtrl', ['$scope','PeriodsModel', 'ConsolidatedModel','SchoolsModel','SemesterSuneduFilter',
  function ($scope,PeriodsModel, ConsolidatedModel,SchooolsModel,SemesterSuneduFilter) {
    $scope.init = function(){
      PeriodsModel.query().$promise.then(function(data){
        $scope.periods_list=data;
        $scope.search_option_last_period=$scope.periods_list[0];
        console.log($scope.periods_list)
        console.log($scope.search_option_last_period);
      },function(err){$scope.result = -1;});
      //$scope.periods_list=PeriodsModel;


      $scope.search_options={
        Bachiller:{v:true,x:false,name:'Bachiller'},
        Degree:{v:true,x:false,name:'Título'},
        Projection:{v:true,x:false,name:'Proyección'},
        Language:{v:true,x:false,name:'Idioma'},
        Practice:{v:true,x:false,name:'Práctica'}
      };
    };
    $scope.changeSearchOption= function(option,type){
      $scope.search_options[option][type]=!$scope.search_options[option][type];
    };
    $scope.table2excel=function(){
      $("#table-consolidated").table2excel({
        exclude: ".excludeThisClass",
        name: "Consolidate",
        filename: "consolidado" //do not include extension
      });
    }
    $scope.searchEgresados = function(){
      $scope.result=0;
      //
      var params={period:$scope.search_option_last_period.id};
      for(var index in  $scope.search_options){
        params[index.toLowerCase()+'_completed']=$scope.search_options[index].v?1:0;
        params[index.toLowerCase()+'_incompleted']=$scope.search_options[index].x?1:0;
      }
      //cargar egresados
    ConsolidatedModel.query(params)
    .$promise.then(function(data){
        $scope.students=data;
        $scope.result=1;
        $scope.cant_results=$scope.students.length;
        if($scope.cant_results > 0){
        SchooolsModel.query().$promise.then(
          function(data) {
            console.log(data);
            $scope.schools=data;
            for (var i = 0; i < $scope.students.length; i++) {
              for(var j = 0; j < $scope.schools.length; j++) {
                if($scope.students[i].school==$scope.schools[j].id){
                  $scope.students[i].faculty_id=$scope.schools[j].faculty_id;
                  $scope.students[i].school_name=$scope.schools[j].name;
                  break;
                }
              }
            }
            console.log($scope.students)
          },
          function(error) {
            $scope.result = -1;
          }
        );}
      }, function(err){
        $scope.result = -1;
      });
    };
  }
]);
