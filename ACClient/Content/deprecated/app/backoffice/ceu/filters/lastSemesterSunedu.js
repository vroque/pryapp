'use strict';
/* jshint -W097 */
/* global angular, api_base */



angular.module('CEUModule')
.filter('SemesterSunedu', function () {
  /*
    * filter for the stadistic center
    * change a real semester to a sunedu semester(only two semester a year)
    * @author Wanshi R.C.
    * @date 26/05/2016
  */
    return function (input) {
    //return input.substring(1,1);
    var  r=(input.substring(input.length-1,input.length)<='1')?'1':'2';
    return input.substring(0,input.length-1)+r;
    }
  }
);
