﻿'use strict';
/* jshint -W097 */
/* global angular, require, window */
angular.module('centro-atencion')
.config(['$ocLazyLoadProvider', '$stateProvider', '$urlRouterProvider',
  function ($ocLazyLoadProvider, $stateProvider, $urlRouterProvider) {
    $ocLazyLoadProvider.config({ asyncLoader: require });
    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('home', {
      url: '/',
      controller: 'changeCtrl',
      templateUrl: window.app_base + 'views/change.student.html',
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load({
              name: 'GNRModule',
              serie: true, // used for synchronous load chart scripts
              insertBefore: '#ngInsertBefore',

            files: [
              window.app_base + 'module.js',
              window.app_base + 'services/student.js',
              window.app_base + 'controllers/change.student.js'
            ]
          });
        }
      }
    });
  }
]);
