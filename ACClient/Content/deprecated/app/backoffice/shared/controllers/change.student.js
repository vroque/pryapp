'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('GNRModule')
  /*
    * Controllador para ...
    * @autho
    * @date
  */
.controller('changeCtrl', ['$scope', 'StudentModel',
  function ($scope,StudentModel) {
      $scope.init = function(){
        $scope.init_search = false;
        $scope.student_found_loaded = [];
      };

      $scope.searchStudent = function(student_id) {
        $scope.student_found = undefined;
        $scope.init_search = false;
        //console.log(student_id);
        if (!_.contains(_.pluck($scope.student_found_loaded,'id'),student_id)) {
          StudentModel.get({alumno_id: student_id}).$promise.then(
            function(data) {
              //console.log(data);
              $scope.student_found_loaded.push(data);
              $scope.student_found = data;
              $scope.init_search = true;
            },
            function(error) {
              $scope.student_found = undefined;
              $scope.init_search = true;
            }
          );
        }
        else {
          $scope.student_found = _.findWhere($scope.student_found_loaded,{id:student_id});
          $scope.init_search = true;
        }

      };
    }
  ]
);
