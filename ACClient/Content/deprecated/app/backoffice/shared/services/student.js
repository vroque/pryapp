﻿'use strict';
/* jshint -W097 */
/* global angular, api_base */

angular.module('GNRModule')
.factory('StudentModel', ['$resource', '$rootScope',
  function ($resource, $rootScope) {
      return $resource($rootScope.url_base + 'backoffice/api/alumno/');
  }
]);
