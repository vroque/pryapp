'use strict';
/* jshint -W097 */
/* global angular, require, window */
angular.module('centro-atencion')
.config(['$ocLazyLoadProvider', '$stateProvider', '$urlRouterProvider',
  function ($ocLazyLoadProvider, $stateProvider, $urlRouterProvider) {
    $ocLazyLoadProvider.config({ asyncLoader: require });
    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('home', {
      url: '/administracion-constancias/',
      controller: 'administracionConstanciasCtrl',
      templateUrl: window.app_base + 'views/administracionConstancia.html',
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load({
              name: 'CICModule',
              serie: true, // used for synchronous load chart scripts
              insertBefore: '#ngInsertBefore',
            files: [
              window.app_base + 'module.js',
              window.app_base + 'services/periodos.js',
              window.app_base + 'services/periodos.egresados.js',
              window.app_base + 'services/idiomas.js',
              window.app_base + 'services/caie.js',
              window.app_base + 'services/modalidades.pregrado.js',
              window.app_base + 'services/sedes.js',
              window.app_base + 'services/departament.js',
              window.app_base + 'services/academic.schools.js',
              window.app_base + 'services/niveles.js',
              window.app_base + 'services/matriculadosucic.js',
              window.app_base + 'controllers/administracion.constancias.js',
              window.app_base + 'directives/list.student.id.js',
              window.app_base + 'directives/constancias/const.acred.idiom.ext.js',
              window.app_base + 'directives/constancias/const.acred.idiom.ext.batch.js'
            ]
          });
        }
      }
    });
  }
]);
