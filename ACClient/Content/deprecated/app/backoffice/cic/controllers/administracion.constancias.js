'use strict';
/* jshint -W097 */
/* global angular, $, _ */

angular.module('CICModule')
    /*
     * Controller para la administración de Constancias y Reportes del CIC
     * @author GNUstavo Huarcaya
     * @date 10/05/2016
     */
    .controller('administracionConstanciasCtrl', ['$scope', '$http',
        'periodosEgresadosModel', 'periodosModel', 'idiomasModel', 'caieModel',
        'modalidadesPregradoModel', 'sedesModel', 'departamentModel',
        'academicSchoolsModel', 'nivelesModel', 'matriculadosucicModel',
        function($scope, $http, periodosEgresadosModel, periodosModel,
            idiomasModel, caieModel, modalidadesPregradoModel, sedesModel,
            departamentModel, academicSchoolsModel, nivelesModel,
            matriculadosucicModel) {
            $scope.estadoAdmConst = "active";
            $scope.estadoAdmConstBatch = "";
            $scope.estadoReportes = "";
            $scope.activoAdmConst = true;
            $scope.activoAdmConstBatch = false;
            $scope.activoReportes = false;

            $scope.isConstancia = false;
            $scope.isCertDip = false;

            $scope.isConstanciaMatricula = false;
            $scope.isConstAcredIdiomExt = false;
            $scope.isCertiEstudio = false;
            $scope.isDiploma = false;

            $scope.isConstanciaBatch = false;

            $scope.isConstAcredIdiomExtBatch = false;

            $scope.isMatriculados = false;
            $scope.isMatriculadosDG = false;
            $scope.isEmisionConst = false;

            $scope.isRepMatriUC = false;

            $scope.tipoDocumento = null;
            $scope.tipoDocumentoBatch = null;

            $scope.buscando = false;
            $scope.existSearchResult = true;


            var topQuery = 11;
            var topQueryPlus = topQuery;
            $scope.showLimit = 10;
            $scope.msgError = null;

            $scope.documentos = {
                disponibles: [{
                    id: "cdm",
                    name: "Constancia de Matrícula"
                }, {
                    id: "cai",
                    name: "Constancia de Acreditación de Idioma Extranjero"
                }, {
                    id: "cde",
                    name: "Certificados de Estudios"
                }, {
                    id: "dip",
                    name: "Diplomas"
                }],
                reportes: [{
                    id: "mat",
                    name: "Matriculados"
                },{
                    id: "mdg",
                    name: "Matriculados Detalle Global"
                }, {
                    id: "edc",
                    name: "Emisión de Constancias"
                }],
            };

            $scope.documentosBatch = {
                disponibles: [{
                    id: "cai2",
                    name: "Constancia de Acreditación de Idioma Extranjero"
                }],
            };

            /*******************************************************************
            ** MANEJO DE TABS E INPUT DE CADA OPCION
            *******************************************************************/

            /**
             * Este tab se encuentra activo por default
             * Activa el tab "Administración de Constancias"
             * y desactiva el tab "Reportes" y
             * "Administración de Constancias por Lotes"
             * @return {boolean} Activa el tab "Administración de Constancias"
             */
            $scope.admConst = function() {
                $scope.estadoAdmConst = "active";
                $scope.estadoAdmConstBatch = "";
                $scope.estadoReportes = "";
                $scope.activoAdmConst = true;
                $scope.activoAdmConstBatch = false;
                $scope.activoReportes = false;
                $scope.buscando = false;
                noReporte();
                noConstanciaBatch();
            };

            /**
             * Activa el tab "Administración de Constancias por Lotes"
             * y desactiva el tab "Reportes" y "Administración de Constancias"
             * @return {[type]} [description]
             */
            $scope.admConstBatch = function() {
                $scope.estadoAdmConstBatch = "active";
                $scope.estadoAdmConst = "";
                $scope.estadoReportes = "";
                $scope.activoAdmConst = false;
                $scope.activoAdmConstBatch = true;
                $scope.activoReportes = false;
                $scope.buscando = false;
                noConstancia();
                noCertDip();
                noReporte();
            };

            /**
             * Activa el tab "Reportes" y desactiva
             * el tab "Administración de Constancias" y
             * "Administración de Constancias por Lotes"
             * @return {boolean} Activa el tab "Reportes"
             */
            $scope.reportes = function() {
                $scope.estadoReportes = "active";
                $scope.estadoAdmConst = "";
                $scope.estadoAdmConstBatch = "";
                $scope.activoAdmConst = false;
                $scope.activoAdmConstBatch = false;
                $scope.activoReportes = true;
                $scope.buscando = false;
                noConstancia();
                noCertDip();
                noConstanciaBatch();
            };

            /**
             * Oculta los filtros de búsqueda de "Constancia de Matricula" y
             * "Constancia de Acreditación de Idioma Extranjero" del tab
             * "Administración de Constancias". También oculta el panel de
             * resultados de búsqueda
             * @return {boolean} Oculta filtros
             */
            function noConstancia() {
                $scope.isConstancia = false;
                $scope.isConstanciaMatricula = false;
                $scope.isConstAcredIdiomExt = false;
                $scope.buscando = false;
            };

            /**
             * Oculta los filtros de búsqueda de "Certificados de Estudio" y
             * "Diplomas" del tab "Administración de Constancias".
             * También oculta el panel de resultados de búsqueda
             * @return {boolean} Oculta filtros
             */
            function noCertDip() {
                $scope.isCertDip = false;
                $scope.isCertiEstudio = false;
                $scope.isDiploma = false;
                $scope.buscando = false;
            };

            /**
             * Oculta los filtros de búsqueda del tab "Administración de
             * Constancias" y "Reportes"
             * @return {boolean} false
             */
            function noConstanciaBatch() {
                $scope.isConstanciaBatch = false;
                $scope.isConstAcredIdiomExtBatch = false;
                $scope.buscando = false;
            };

            /**
             * Oculta los filtros de búsqueda de "Matriculados" y "Emisión de
             * Constancias" del tab "Reportes".
             * También oculta el panel de resultados de búsqueda
             * @return {boolean} Oculta filtros
             */
            function noReporte() {
                $scope.isMatriculados = false;
                $scope.isMatriculadosDG = false;
                $scope.isEmisionConst = false;
                $scope.isRepMatriUC = false;
                $scope.buscando = false;
            }

            /*******************************************************************
            ** CONSUMO DE APIS
            *******************************************************************/

            /**
             * Uso del service "periodoModel" para consumir el API de
             * periodos academicos
             * @return {JSON} Archivo JSON con los periodos academicos
             */
            function periods() {
                periodosModel.query().$promise
                    .then(function(data) {
                        if (data.length > 0) {
                            $scope.periodos = data;
                        } else {
                            // pass
                        }
                    }, function(err) {
                        // pass
                    });
            };
            periods();

            /**
             * Uso del service "periodosEgresadosModel" para consumir el API de
             * periodos academicos en los que egresaron alumnos de la UCCI
             * @return {JSON} Archivo JSON con los periodos academicos
             */
            function periodsE() {
                periodosEgresadosModel.query().$promise
                    .then(function(data) {
                        if (data.length > 0) {
                            $scope.periodosE = data;
                        } else {
                            // pass
                        }
                    }, function(err) {
                        // pass
                    });
            };
            periodsE();

            /**
             * Uso del service "idiomasModel" para consumir el API de idiomas
             * @return {JSON} Archivo JSON con los idiomas disponibles
             */
            function languages() {
                idiomasModel.query().$promise
                    .then(function(data) {
                        if (data.length > 0) {
                            $scope.idiomas = data;
                        } else {
                            // pass
                        }
                    }, function(err) {
                        //pass
                    });
            };
            languages();

            function getDistinct(o, attr) {
                var answer = {};
                $.each(o, function(index, record) {
                    answer[index[attr]] = answer[index[attr]] || [];
                    answer[index[attr]].push(record);
                });
                return answer; //return an object that has an entry for each unique value of attr in o as key, values will be an array of all the records that had this particular attr.
            };

            /**
             * Uso del service "nivelesModel" para consumir el API de nivel de
             * idiomas CIC
             * @param  {string} args id_escuela idioma
             * @param  {string} argv modalidad CIC
             * @return {JSON}        Archivo JSON con los niveles disponibles
             */
            function levels(args, argv) {
                nivelesModel.query({
                        id_escuela: args,
                        modalidad: argv
                    }).$promise
                    .then(function(data) {
                        if (data.length > 0) {
                          var unicos = alasql("SELECT DISTINCT nivel_idioma, modalidad_cic, id_escuela, id_nivel FROM ? GROUP BY modalidad_cic, nivel_idioma, id_escuela, id_nivel ORDER BY id_nivel", [data]);
                            $scope.niveles = unicos;
                        } else {
                            //pass
                        }
                    }, function(error) {
                        // pas
                    });
            };

            /**
             * Uso del service "modalidadesPregradoModel" para consumir el API
             * "Modalidades Pregrado"
             * @return {JSON} Modalidades pregrado existentes
             */
            function modalities() {
                modalidadesPregradoModel.query().$promise
                    .then(function(data) {
                        if (data.length > 0) {
                            $scope.modalidadesP = data;
                        } else {
                            // pass
                        }
                    }, function(err) {
                        // pass
                    });
            };
            modalities();

            /**
             * Uso del service "sedesModel" para consumir el API "Sedes"
             * @return {JSON} Sedes de la UCCI
             */
            function headquarters() {
                sedesModel.query().$promise
                    .then(function(data) {
                        if (data.length > 0) {
                            $scope.sedes = data;
                        } else {
                            // pass
                        }
                    }, function(error) {
                        // pass
                    })
            };
            headquarters();

            /**
             * Uso del service genérico "departamentModel" para consumir el API
             * "Facultades"
             * @return {JSON} Facultades UCCI
             */
            function departaments() {
                departamentModel.query().$promise
                    .then(function(data) {
                        if (data.length > 0) {
                            $scope.facultades = data;
                        } else {
                            // pass
                        }
                    }, function(error) {
                        // pass
                    })
            };
            departaments();

            /**
             * Uso del service "academiSchools" para consumir el API
             * "Escuelas Pregrado" y filtrar según una determinada facultad
             * @param  {string} args Identificador de facultad UCCI
             * @return {JSON}        Escuelas pregrado que pertenecen a la
             *                       facultad "args"
             */
            function academicSchools(args) {
                academicSchoolsModel.query({
                        facultad_id: args
                    }).$promise
                    .then(function(data) {
                        if (data.length > 0) {
                            $scope.escuelas = data;
                        } else {
                            // pass
                        }
                    }, function(error) {
                        // pass
                    })
            };

            /**
             * Usa service "caieModel" para consumir el API "Constancias de
             * Acreditación de Idioma Extranjero" y devolver un
             * determinado número de estudiantes que tienen dicha constancia
             * @param  {string} args Periodo que el alumno egresó de ls UCCI
             * @param  {string} argv Código de Idioma
             * @param  {int} argz    Número de estudiantes que se mostrarán (TOP)
             * @return {JSON}        Lista de 'argz' alumnos que tienen
             *                       "Constancia de Acreditación de Idioma
             *                       Extranjero"
             */
            function caie(args, argv, argz) {
                caieModel.query({
                        periodo_egreso: args,
                        idioma_id: argv,
                        top: argz
                    }).$promise
                    .then(
                        function(data) {
                            //console.log(data);
                            $scope.existSearchResult = true;
                            $scope.constanciasAIE = data;
                            verifyButtons('constAcredIdiomExt');
                        },
                        function(error) {
                            $scope.existSearchResult = false;
                            $scope.msgError = "No se encontraron resultados para el tipo de búsqueda seleccionado";
                            $scope.constanciasAIE = undefined;
                        }
                    );
            };

            /**
             * Usa el service caieModel para consumir el API de "Constancias de
             * Acreditación de Idioma Extranjero" para devolver todos los
             * estudiantes que poseen dicha certificación
             * @param  {string} args Período que el estudiante egresó de la UCCI
             * @param  {string} argv Código de Idioma
             * @return {JSON}        Estudiantes que tienen "Constancia de
             *                       Acreditación de Idioma Extranjero"
             */
            function caieT(args, argv) {
                NProgress.configure({ parent: '#adm_const' });
                NProgress.start();
                caieModel.query({
                        periodo_egreso: args,
                        idioma_id: argv
                    }).$promise
                    .then(
                        function(data) {
                            //console.log(data);
                            $scope.existSearchResult = true;
                            $scope.constanciasAIET = data;
                            $scope.nResultadosT = data.length;
                            $scope.showLimit = 10;
                            if ($scope.nResultadosT > 10) {
                                $scope.showLimit = 10;
                            } else if ($scope.nResultadosT <= $scope.showLimit) {
                                $scope.showLimit = $scope.nResultadosT;
                            }
                            verifyButtons('constAcredIdiomExt');
                            NProgress.done();
                        },
                        function(error) {
                            $scope.existSearchResult = false;
                            $scope.msgError = "No se encontraron resultados para el tipo de búsqueda seleccionado";
                            $scope.constanciasAIET = undefined;
                            NProgress.done();
                        }
                    );
            };

            /**
             * Usa el service caieModel para consumir el API de "Constancia de
             * Acreditación de Idioma Extranjero" y poder devolver todos los
             * estudiantes que poseen dicha certificación de acuerdo a los
             * código de alumnos ingresados (Búsqueda por Lote)
             * @param  {string} args Código de estudiantes
             * @return {JSON}        Estudiantes que tienen "Constancia de
             *                       Acreditación de Idioma Extranjero"
             */
            function caieBatch(args) {
                NProgress.configure({ parent: '#adm_const' });
                NProgress.start();
                caieModel.query({
                    estudiantes: args
                }).$promise
                .then(
                    function(data) {
                        $scope.existSearchResult = true;
                        $scope.constanciasAIEBatch = data;
                        //console.log(data);
                        NProgress.done();
                    },
                    function(error) {
                      $scope.existSearchResult = false;
                      $scope.msgError = "No se encontraron resultados para el tipo de búsqueda";
                      $scope.constanciasAIEBatch = undefined;
                      NProgress.done();
                    }
                );
            };


            /**
             * Usa el service matriculadosucicModel para consumir el API de
             * "Estudiantes UC Matriculados en el CIC"
             * @return {JSON} matriUCIC Estudiantes UC Matriculados y NO
             *                          Matriculados en el CIC
             */
            function matriculadosUCIC() {
                NProgress.configure({ parent: '#adm_const' });
                NProgress.start();
                console.log("Estudiantes UC");
                matriculadosucicModel.query().$promise
                    .then(function(data) {
                        if (data.length > 0) {
                            $scope.matriUCIC = data;
                            console.log($scope.matriUCIC.length);
                            NProgress.done();
                            $scope.isRepMatriUC = true;
                        } else {
                            // pass
                        }
                    }, function(err) {
                        //pass
                        console.log("Ok, Houston, we've had a problem here");
                        NProgress.done();
                    });
            };


            /*******************************************************************
            **
            *******************************************************************/

            /**
             * Lectura de los códigos ingresados para procesar "Constancias de
             * Idioma Extranjero" por lotes.
             * @param  {string} student_str Códigos de estudiantes
             * @return {array}              Arreglo de código de estudiantes
             */
            $scope.changeStudentArray = function(student_str){
        			$scope.estudiantes = student_str
        			.split(' ')
        			.filter(function (id){return id != ''})
        			.filter(function(id,position,self){return self.indexOf(id)==position});
        		};


            /**
             * Verifica si existe más data para mostrar
             * Activa el botón "Cargar Más"
             * @param  {string} args Tipo busqueda para la carga de más data
             * @return {boolean}     Activa el respectivo boton de "Cargar Más"
             */
            function verifyButtons(args) {
                if (args == 'constAcredIdiomExt') {
                    $scope.showMoreButton = ($scope.nResultadosT >
                        $scope.showLimit) ? true : false;
                    topQueryPlus += 10;
                }
            };


            /**
             * Muestra una opción de filtro para la búsqueda de constancias
             * de acuerdo a las opciones seleccionadas en el select
             * "Selecciona Tipo de Documento" del tab "Administración de
             * Constancias"
             * @param  {string} args Tipo de documento a filtrar
             * @return {boolean}      Activa los input del tipo documento
             */
            $scope.seleccionTipoDoc = function(args) {
                switch (args) {
                    case "cdm":
                        $scope.isConstancia = true;
                        $scope.isConstanciaMatricula = true;
                        noCertDip();
                        noConstanciaBatch();
                        $scope.isConstAcredIdiomExt = false;
                        break;
                    case "cai":
                        $scope.isConstancia = true;
                        $scope.isConstAcredIdiomExt = true;
                        noCertDip();
                        noConstanciaBatch();
                        $scope.isConstanciaMatricula = false;
                        break;
                    case "cde":
                        $scope.isCertDip = true;
                        $scope.isCertiEstudio = true;
                        noConstancia();
                        noConstanciaBatch();
                        $scope.isDiploma = false;
                        break;
                    case "dip":
                        $scope.isCertDip = true;
                        $scope.isDiploma = true;
                        noConstancia();
                        noConstanciaBatch();
                        $scope.isCertiEstudio = false;
                        break;
                    default:
                        noConstancia();
                        noCertDip();
                        noConstanciaBatch();
                        break;
                }
            };

            $scope.seleccionTipoDocBatch = function(args) {
                switch (args) {
                    case "cai2":
                        $scope.isConstanciaBatch = true;
                        $scope.isConstAcredIdiomExtBatch = true;
                        break;
                    default:
                        noConstancia();
                        noCertDip();
                        noConstanciaBatch();
                        break;
                }
            };


            /**
             * Muestra una opción de filtro para la generación de reportes
             * de acuerdo a las opciones seleccionadas en el select
             * "Selecciona Tipo de Reporte" del tab "Reportes"
             * @param  {string} args Tipo de reporte a filtrar
             * @return {boolean}      Activa los input del tipo reporte
             */
            $scope.seleccionTipoRep = function(args) {
                switch (args) {
                    case "mat":
                        $scope.isMatriculados = true;
                        $scope.isMatriculadosDG = false;
                        $scope.isEmisionConst = false;
                        break;
                    case "mdg":
                        $scope.isMatriculadosDG = true;
                        $scope.isMatriculados = false;
                        $scope.isEmisionConst = false;
                        $scope.isRepMatriUC = false;
                        break;
                    case "edc":
                        $scope.isEmisionConst = true;
                        $scope.isMatriculados = false;
                        $scope.isMatriculadosDG = false;
                        $scope.isRepMatriUC = false;
                        break;
                    default:
                        $scope.isMatriculados = false;
                        $scope.isMatriculadosDG = false;
                        $scope.isEmisionConst = false;
                        $scope.isRepMatriUC = false;
                        break;
                }
            };

            /**
             * Busca alumnos egresados que tengan un determinado número de
             * ciclos aprobados en el CIC, según el año que egresaron
             * @param  {string} args Periodo académico en el que se egresó
             * @param  {string} argv Idioma que estudió en el CIC
             * @return {JSON}        Lista de alumnos que cumplen los requisitos
             */
            $scope.searchConstAcredIdiomExt = function(args, argv) {
                $scope.buscando = true;
                //NProgress.configure({ parent: '#xyz' });
                //NProgress.start();
                caieT(args, argv);
                //NProgress.done();
            };

            /**
             * Busca alumnos egresados que tengan un determinado número de
             * ciclos aprobados en el CIC, según los código de alumnos
             * ingresados en la vista (Búsqueda por Lote)
             * @param  {string} args Código de alumnos a buscar
             * @return {JSON}        Lista de alumnos que cumplen con
             *                       los requisitos
             */
            $scope.searchConstAcredIdiomExtBatch = function(args) {
                $scope.buscando = true;
                caieBatch(args);
            };

            /**
             * Renderiza más resultados y muestra el número de resultados que se
             * estan renderizando al usuario.
             * Activa|Desactiva el boton de "Cargar Más"
             * @return {boolean} Carga más resultados y Activa|Desactiva el
             *                   boton "Cargar Más"
             */
            $scope.cargarMas = function() {
                if ($scope.nResultadosT > $scope.showLimit) {
                    $scope.showLimit += 10;
                    if ($scope.nResultadosT <= $scope.showLimit) {
                        $scope.showLimit = $scope.nResultadosT;
                        $scope.showMoreButton = false;
                    }
                } else if ($scope.nResultadosT <= $scope.showLimit) {
                    $scope.showLimit = $scope.nResultadosT;
                    $scope.showMoreButton = false;
                }
            };

            /**
             * Carga la lista de "Escuelas Pregrado" de una respectiva Facultad
             * "args"
             * @param  {string} args Facultad UCCI
             * @return {JSON}        Escuelas Pregrado de correspondientes a una
             *                       Facultad "args"
             */
            $scope.seleccionTipoFacultad = function(args) {
                academicSchools(args);
            };

            /**
             * Carga los niveles de Idiomas CIC de acuerdo a la modalidad
             * "args" elegida (Adultos, Escolares, Virtuales).
             * @param  {string} args id_escuela correspondiente al idioma CIC
             * @param  {string} argv Modalidad CIC
             * @return {JSON}        Niveles de Idiomas CIC
             */
            $scope.seleccionTipoIdioma = function(args, argv) {
                levels(args, argv);
            };

            /**
             * Limpia el campo de resultados de búsqueda cuando se selecciona
             * un filtro diferente.
             * Reinicia el contador de mostrar los "n" primeros resultados
             * @return {boolean} Limpia el panel de resultados de búsqueda
             */
            $scope.seleccionReset = function() {
                if ($scope.buscando) {
                    $scope.constanciasAIET = undefined;
                    $scope.showLimit = 10;
                    if ($scope.nResultadosT > 10) {
                        $scope.showLimit = 10;
                    } else if ($scope.nResultadosT <= $scope.showLimit) {
                        $scope.showLimit = $scope.nResultadosT;
                    }
                    $scope.buscando = false;
                }
            };

            $scope.procesarConstanciaIndividual = function(alumno) {
                NProgress.configure({ parent: 'body' });
                NProgress.start();
                alumno.viewBotones = true;
                $scope.listAlumnos = [];
                $scope.listAlumnos.push(alumno);
                caieModel
                    .procesarConstancia({
                        listAlumnos: $scope.listAlumnos
                    })
                    .$promise.then(
                        function(data) {
                            angular.forEach(data, function(value, key) {
                                $scope.constanciasAIET.filter(
                                    function(obj) {
                                        if (obj.IDAlumno === value.idAlumno) {
                                            obj.rutaArchivo = value.rutaArchivo;
                                            return obj;
                                        }
                                    }
                                );
                            });
                        },
                        function(error) {
                            console.log('error', error);
                        }
                    )
                NProgress.done();
            };

            $scope.procesarConstanciaIndividualBatch = function(alumno) {
                NProgress.configure({ parent: 'body' });
                NProgress.start();
                alumno.viewBotones = true;
                $scope.listAlumnos = [];
                $scope.listAlumnos.push(alumno);
                caieModel
                    .procesarConstancia({
                        listAlumnos: $scope.listAlumnos
                    })
                    .$promise.then(
                        function(data) {
                            angular.forEach(data, function(value, key) {
                                $scope.constanciasAIEBatch.filter(
                                    function(obj) {
                                        if (obj.IDAlumno === value.idAlumno) {
                                            obj.rutaArchivo = value.rutaArchivo;
                                            return obj;
                                        }
                                    }
                                );
                            });
                        },
                        function(error) {
                            console.log('error', error);
                        }
                    )
                NProgress.done();
            };

            $scope.procesarConstanciaX = function(alumno) {
                $scope.listAlumnos = [];

                angular.forEach($scope.constanciasAIET,
                    function(value, key) {
                        this.push(value);
                    }, $scope.listAlumnos);

                caieModel
                    .procesarConstancia({
                        listAlumnos: $scope.listAlumnos
                    })
                    .$promise.then(
                        function(data) {
                            angular.forEach(data, function(value, key) {
                                $scope.constanciasAIET.filter(
                                    function(obj) {
                                        if (obj.IDAlumno === value.idAlumno) {
                                            obj.rutaArchivo = value.rutaArchivo;
                                            return obj;
                                        }
                                    }
                                );
                            });
                        },
                        function(error) {
                            console.log('error', error);
                        }
                    )
            };

            /******************************************************************
            ** REPORTES
            *******************************************************************/

            /**
             * Lista de Estudiantes UC Matriculados y NO Matriculados en el CIC
             */
            $scope.getMatriculadosUCIC  = function() {
                matriculadosUCIC();
            };

            // Estilo para los reportes en .xls
            var estilo = {
                headers: true,
                column: {style: {Font: {Bold: "1"}}},
                rows: {1: {style: {Font: {Color: "#000000"}}}},
                cells: {1: {1: {style: {Font: {Color: "#000000"}}}}}
            };


            /**
             * Reporte de Estudiantes UC Matricuados en el Centro de Idiomas
             * @return {file} xls Fichero en formato MS Excel con data de
             *                    Estudiantes UC Matriculados en el CIC
             */
            $scope.getRepMatriculadosUCIC = function() {
                alasql('SELECT id_alumno AS codigoAlumno, nombres AS Nombres, facultad AS Facultad, id_escuela AS IDEscuela, nombre_escuela AS NombreEscuela, sede AS Sede, modalidad AS Modalidad, plan_estudio AS PlanEstudio, creditos_uc_aprobados AS CreditosUCAprobados, ciclo_uc_matriculado AS CicloUCMatriculado, idioma AS Idioma, ciclo_cic_matriculado AS UltimoCicloCICMatriculado, nivel AS UltimoCicloAprobado INTO XLS("reporte-matriculados_uc-cic.xls",?) FROM ? WHERE matriculado_cic = 1 ORDER BY nombres', [estilo, $scope.matriUCIC]);
            }

            /**
             * Reporte de Estudiantes UC Matricuados en el Centro de Idiomas
             * @return {file} xls Fichero en formato MS Excel con data de
             *                    Estudiantes UC Matriculados en el CIC
             */
            $scope.getRepNoMatriculadosUCIC = function() {
                alasql('SELECT id_alumno AS codigoAlumno, nombres AS Nombres, facultad AS Facultad, id_escuela AS IDEscuela, nombre_escuela AS NombreEscuela, sede AS Sede, modalidad AS Modalidad, plan_estudio AS PlanEstudio, creditos_uc_aprobados AS CreditosUCAprobados, ciclo_uc_matriculado AS CicloUCMatriculado, idioma AS Idioma, nivel AS UltimoCicloAprobado INTO XLS("reporte-no-matriculados_uc-cic.xls",?) FROM ? WHERE matriculado_cic = 0 ORDER BY nombres', [estilo, $scope.matriUCIC]);
            }

        }
    ]);
