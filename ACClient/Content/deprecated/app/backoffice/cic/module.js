'use strict';
/* jshint -W097 */
/* global angular, window */

angular.module('CICModule', [])
    .run(['$rootScope', function($rootScope) {
        $rootScope.app_base = window.app_base;
        $rootScope.api_base = window.api_base;
        $rootScope.url_base = window.url_base;
    }]);
