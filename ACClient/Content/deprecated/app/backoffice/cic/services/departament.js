'use strict';
/* jshint -W097 */
/* global angular, api_base */
angular.module('CICModule')
    .factory('departamentModel', ['$resource', '$rootScope',
        function($resource, $rootScope) {
            return $resource(
                $rootScope.url_base + 'backoffice/api/facultades/'
            );
        }
    ]);
