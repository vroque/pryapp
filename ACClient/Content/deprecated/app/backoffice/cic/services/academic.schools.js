'use strict';
/* jshint -W097 */
/* global angular, api_base */
angular.module('CICModule')
    .factory('academicSchoolsModel', ['$resource', '$rootScope',
        function($resource, $rootScope) {
            return $resource(
                $rootScope.url_base + 'backoffice/api/escuelas-pregrado/:facultad_id/', {
                  facultad_id: '@facultad_id'
                }
            );
        }
    ]);
