'use strict';
/* jshint -W097 */
/* global angular, api_base */
angular.module('CICModule')
    .factory('periodosEgresadosModel', ['$resource', '$rootScope',
        function($resource, $rootScope) {
            return $resource(
                $rootScope.api_base + 'periodos-alumnos-egresados/'
            );
        }
    ]);
