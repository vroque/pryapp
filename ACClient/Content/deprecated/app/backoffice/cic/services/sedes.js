'use strict';
/* jshint -W097 */
/* global angular, api_base */
angular.module('CICModule')
    .factory('sedesModel', ['$resource', '$rootScope',
        function($resource, $rootScope) {
            return $resource(
                //$rootScope.app_base + 'sedes/'
                $rootScope.url_base + 'backoffice/api/sedes/'
            );
        }
    ]);
