﻿'use strict';
/* jshint -W097 */
/* global angular, api_base */
angular.module('CICModule')
    .factory('caieModel', ['$resource', '$rootScope',
        function($resource, $rootScope) {
            return $resource(
                $rootScope.api_base + 'constancia-acreditacion-idioma-extranjero/:periodo_egreso/:idioma_id/:top/', {
                    periodo_egreso: '@periodo_egreso',
                    idioma_id: '@idioma_id',
                    top: '@top'
                }, {
                  'procesarConstancia': {method: 'POST', isArray: true}
                }
            );
        }
    ]);
