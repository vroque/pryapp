'use strict';
/* jshint -W097 */
/* global angular, api_base */
angular.module('CICModule')
    .factory('nivelesModel', ['$resource', '$rootScope',
        function($resource, $rootScope) {
            return $resource(
                $rootScope.url_base + 'backoffice/api/cic/niveles/:id_escuela/:modalidad/', {
                  id_escuela: '@id_escuela',
                  modalidad: '@modalidad'
                }
            );
        }
    ]);
