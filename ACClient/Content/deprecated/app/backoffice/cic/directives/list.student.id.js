'use strict';
/* jshint -W097 */
/* global angular, api_base */

angular.module('CICModule')
    .directive('cdListStudentId', ['$rootScope', function($rootScope) {
        return {
            templateUrl: window.app_base + 'views/listStudentId.html'
        }
    }]);
