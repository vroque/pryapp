'use strict';
/* jshint -W097 */
/* global angular, api_base */

angular.module('CICModule')
    .directive('constAcredIdiomExtBatchDirective', ['$rootScope', function($rootScope) {
        return {
            templateUrl: window.app_base + 'views/constancias/constAcredIdiomExtBatch.html'
        }
    }]);
