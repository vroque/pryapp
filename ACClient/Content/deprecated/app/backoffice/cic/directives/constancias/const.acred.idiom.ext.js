'use strict';
/* jshint -W097 */
/* global angular, api_base */

angular.module('CICModule')
    .directive('constAcredIdiomExtDirective', ['$rootScope', function($rootScope) {
        return {
            templateUrl: window.app_base + 'views/constancias/constAcredIdiomExt.html'
        }
    }]);
