﻿using System.Web.Mvc;

namespace ACClient.Areas.backoffice
{
    public class backofficeAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "backoffice"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "backoffice",
                "backoffice",
                new {controller = "Generals", action = "Index"},
                new[] {"ACClient.Areas.backoffice.Controllers"}
            );
            context.MapRoute(
                "backoffice_set_profile",
                "backoffice/set-profile",
                new {controller = "Generals", action = "setProfile"},
                new[] {"ACClient.Areas.backoffice.Controllers"}
            );
            context.MapRoute(
                "backoffice_go_backoffice",
                "go/backoffice",
                new {controller = "Generals", action = "GoToBackOffice"},
                new[] {"ACClient.Areas.backoffice.Controllers"}
            );
            context.MapRoute(
                "backoffice_chage_to_student",
                "backoffice/cambiar-a-estudiante/{student_id}",
                new {controller = "Generals", action = "changeToStudent",},
                new[] {"ACClient.Areas.backoffice.Controllers"}
            );
            context.MapRoute(
                "backoffice_get_pdf_documents",
                "backoffice/get-documents/{pdf}/pdf/",
                new {controller = "AtentionCenter", action = "getPDF",},
                new[] {"ACClient.Areas.backoffice.Controllers"}
            );
            context.MapRoute(
                "backoffice_get_pdf_documents_gyt",
                "backoffice/get-documents/gyt/{pdf}/pdf/",
                new {controller = "Home", action = "getGytPDF", pdf = UrlParameter.Optional},
                new[] {"ACClient.Areas.backoffice.Controllers"}
            );
            // Languages Center PDF documents route
            context.MapRoute("backoffice_get.cic_pdf_documents", "backoffice/centro-idiomas/pdf/{fileName}/",
                new
                {
                    controller = "LanguagesCenter", action = "GetLanguagesCenterPdf"
                },
                new[] {"ACClient.Areas.backoffice.Controllers"});

            // Extracurricular Activities PDF documents route
            context.MapRoute("backoffice_get.vuc_pdf_documents", "backoffice/media/actividades-extracurriculares/pdf/{fileName}/",
                new { controller = "ExtracurricularActivities", action = "GetExtracurricularActivitiesPdf" },
                new[] { "ACClient.Areas.backoffice.Controllers" });

            // Extracurricular Activities images activities route
            context.MapRoute("backoffice_get.vuc_images_activities", "backoffice/media/actividades-extracurriculares/images/activities/{fileName}/",
                new { controller = "ExtracurricularActivities", action = "GetExtracurricularActivitiesImagesActivities" },
                new[] { "ACClient.Areas.backoffice.Controllers" });

            // Extracurricular Activities images axis route
            context.MapRoute("backoffice_get.vuc_images_axis", "backoffice/media/actividades-extracurriculares/images/axis/{fileName}/",
                new { controller = "ExtracurricularActivities", action = "GetExtracurricularActivitiesImagesAxis" },
                new[] { "ACClient.Areas.backoffice.Controllers" });

            // Extracurricular Activities recognition route
            context.MapRoute("backoffice_get.vuc_files_recognition", "backoffice/media/actividades-extracurriculares/recognitions/{fileName}/",
                new { controller = "ExtracurricularActivities", action = "GetExtracurricularActivitiesFiles" },
                new[] { "ACClient.Areas.backoffice.Controllers" });

            // Deprecated Exceptions
            context.MapRoute(
                "backoffice_socieconomic_record",
                "backoffice/buw-personal/verificacion",
                new {controller = "BUW", action = "Index"},
                new[] {"ACClient.Areas.backoffice.Controllers"}
            );
            // SPA routes
            context.MapRoute(
                "backoffice_extended",
                "backoffice/{*ext}",
                new {controller = "Generals", action = "Index"},
                new[] {"ACClient.Areas.backoffice.Controllers"}
            );
        }
    }
}