﻿using ACAccess.Authorization;
using ACClient.Helpers.ActionFilters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACClient.Areas.backoffice.Controllers
{
    public class BUWController : Controller
    {
        /// <url>/backoffice/buw-personal/verificacion</url>
        /// <summary>
        /// Pagina de verificación de la  ficha socieconomica
        /// </summary>
        /// <returns></returns> 
        [HasSession("AuthBackoffice")]
        public ActionResult Index()
        {

            ACAuth auth_obj = (ACAuth)Session["AuthBackoffice"];
            List<string> js_list = new List<string>();
            js_list.Add("backoffice/buw/config/routes.js");
            //js_list.Add("frontdesk/buw/controller/socrecord.js");
            ViewBag.js_list = js_list;
            ViewBag.seccion = "vida-academica";
            /** INCICIO DE CIERRE DE FICHA POR PERMISOS */
            // bool has_permission = true; //FichaSocioeconomica.has_access(auth_obj.usuario_id);
                                        //Verifica si tienes permiso de administraivo

            Menu menu_obj = new Menu();

            Menu menu = new Menu();
            ViewBag.app = "backoffice";
            ViewBag.menus = menu.getFromSedeUser(auth_obj.campus.code, auth_obj.id);
            ViewBag.person = auth_obj;
            ViewBag.profile = auth_obj.profile;
            ViewBag.title_seccion = "Mi vida universitaria";
            return View();
        }
    }
}