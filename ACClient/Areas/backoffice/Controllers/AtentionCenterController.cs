﻿using ACClient.Helpers.ActionFilters;
using System.Web.Mvc;
using ACTools.PDF;

namespace ACClient.Areas.backoffice.Controllers
{
    public class AtentionCenterController : Controller
    {
        /// <summary>
        /// End point para la vista de PDF
        /// </summary>
        /// <param name="pdf"></param>
        /// <returns></returns>
        [HasSessionOrSession("Auth", "AuthBackoffice")]
        public ActionResult getPDF(string pdf)
        {
            if (PDFGenerator.exists(pdf))
                return base.File(PDFGenerator.getRepoDocumentPath(pdf), "application/pdf");
            return HttpNotFound();
        }

        public ActionResult getGytPdf(string pdf, string path)
        {
            if (PDFActions.existsGYT(pdf).Count > 0)
                return base.File(PDFActions.getRepoDocumentPathGyt(pdf), "application/pdf");

            return HttpNotFound();
        }
    }
}