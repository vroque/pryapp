﻿using ACClient.Helpers.ActionFilters;
using System.Web.Mvc;
using _util = ACTools.Util;

namespace ACClient.Areas.backoffice.Controllers
{
    [HasSession("AuthBackoffice")]
    public class ExtracurricularActivitiesController : Controller
    {
        /// <summary>
        /// Endpoint para la vista PDF de los documentos de extracurriculares                                          
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public ActionResult GetExtracurricularActivitiesPdf(string fileName)
        {
            if (_util.ExtracurricularActivities.ExistDocument(fileName))
            {
                return File(_util.ExtracurricularActivities.GetDocumentPath(fileName), "application/pdf");
            }

            return HttpNotFound();
        }

        /// <summary>
        /// Imágenes de actividades
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public ActionResult GetExtracurricularActivitiesImagesActivities(string fileName)
        {
            if (_util.ExtracurricularActivities.ExistActivityImage(fileName))
            {
                return File(_util.ExtracurricularActivities.GetActivityImagePath(fileName), "image/jpeg");
            }

            return HttpNotFound();
        }

        /// <summary>
        /// Imágenes de ejes
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public ActionResult GetExtracurricularActivitiesImagesAxis(string fileName)
        {
            if (_util.ExtracurricularActivities.ExistAxiImage(fileName))
            {
                return File(_util.ExtracurricularActivities.GetAxiImagePath(fileName), "image/jpeg");
            }

            return HttpNotFound();
        }

        /// <summary>
        /// Archivos de reconocimientos
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public ActionResult GetExtracurricularActivitiesFiles(string fileName)
        {
            if (_util.ExtracurricularActivities.ExistFiles(fileName))
            {
                return File(_util.ExtracurricularActivities.GetFilesPath(fileName), "application/zip");
            }

            return HttpNotFound();
        }

    }
}