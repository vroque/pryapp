﻿using System.Collections.Generic;
using ACAccess.Authentication.Login;
using ACAccess.Authorization;
using ACClient.Helpers.ActionFilters;
using Newtonsoft.Json;
using System.Linq;
using System.Web.Mvc;
using ACBusiness.Institutional;
using _app = ACTools.Constants.AppConstants;

namespace ACClient.Areas.backoffice.Controllers
{
    /// <summary>
    /// Controlador principal para BackOffice
    /// * todo lo que entra a partir de /backoffice/* para por aqui
    /// </summary>
    public class GeneralsController : Controller
    {
        /// <url> /backoffice </url>
        /// <summary>
        /// Pagina inicio BackOffice
        /// </summary>
        /// <returns></returns>
        [HasSession("AuthBackoffice")]
        public ActionResult Index()
        {
            var auth = (ACAuth) Session["AuthBackoffice"];
            var mod = new Module();
            //ViewBag.campus_list = campus_list;
            ViewBag.campus = auth.campus;
            if (ViewBag.campus == null)
            {
                List<Campus> campus_list = mod.listExecutiveCampus(auth.id);
                if (campus_list.Count == 0)
                    return RedirectToRoute("General_notauthorized");
                if (campus_list.Count > 1)
                    return RedirectToRoute("backoffice_set_profile");
                auth.campus = campus_list.First();
                auth.module_list = new Module().getByUser(auth.id, auth.campus.id);
            }

            var menu = new Menu();
            List<Menu> menus = menu.getFromSedeUser(auth.campus.id, auth.id);
            ViewBag.menus = menus;
            ViewBag.choices = JsonConvert.SerializeObject(menus, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }
            );
            ViewBag.person = auth;
            ViewBag.sidebar = "profileExecutive";

            // Terms and conditions
            var terms = new TermsAndConditions().GetTermsJsonConverted();
            ViewBag.terms = terms;
            // Surveys
            var surveys = new Survey().GetSurveysJsonConverted();
            ViewBag.surveys = surveys;

            return View();
        }

        [HasSession("AuthBackoffice")]
        public ActionResult setProfile()
        {
            var auth = (ACAuth) Session["AuthBackoffice"];
            var mod = new Module();
            List<Campus> campus_list = mod.listExecutiveCampus(auth.id);
            ViewBag.campus_list = campus_list;
            ViewBag.person = auth;
            return View();
        }

        [HasSession("AuthBackoffice")]
        public ActionResult changeToStudent(string student_id)
        {
            var bridge = new BridgeLogin();
            student_id = $"{student_id}@continental.edu.pe";
            var user = bridge.login(student_id, "");
            if (user != null)
            {
                var auth_obj = ACAuth.getAsValid(user);
                if (auth_obj != null)
                {
                    if (user.type.Contains(_app.TYPE_STUDENT))
                    {
                        Session.Add("Auth", auth_obj);
                        return RedirectToRoute("frontdesk");
                    }
                    else
                    {
                        return RedirectToRoute("backoffice");
                    }
                }
            }

            return RedirectToRoute("backoffice");
        }

        /// <summary>
        /// Return to backoffice from frontdesk
        /// </summary>
        /// <returns></returns>
        [HasSession("AuthBackoffice")]
        public ActionResult GoToBackOffice()
        {
            Session.Remove("Auth");
            return RedirectToRoute("backoffice");
        }
    }
}