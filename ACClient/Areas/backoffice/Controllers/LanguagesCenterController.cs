﻿using System.Web.Mvc;
using ACClient.Helpers.ActionFilters;
using ACTools.PDF;
using _util = ACTools.Util;

namespace ACClient.Areas.backoffice.Controllers
{
    public class LanguagesCenterController : Controller
    {
        /// <summary>
        /// Endpoint para la vista PDF de los documentos del Centro de Idiomas
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [HasSession("AuthBackoffice")]
        public ActionResult GetLanguagesCenterPdf(string fileName)
        {
            if (_util.LanguagesCenter.ExistAnyDocument(fileName))
            {
                return File(_util.LanguagesCenter.GetDocumentPath(fileName), "application/pdf");
            }

            return HttpNotFound();
        }
    }
}