﻿using System.Web.Mvc;

namespace ACClient.Areas.parent
{
    public class parentAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "parent";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "parent",
                "parent/",
                new { controller = "Generals", action = "Index" },
                new[] { "ACClient.Areas.parent.Controllers" }
            );
            context.MapRoute(
                "parent_select_profile",
                "parent/select-profile/",
                new { controller = "Generals", action = "selectProfile" },
                new[] { "ACClient.Areas.parent.Controllers" }
            );
            context.MapRoute(
                "parent_select_student",
                "parent/select-student/",
                new { controller = "Generals", action = "selectStudent" },
                new[] { "ACClient.Areas.parent.Controllers" }
            );
            // SPA routes
            context.MapRoute(
                "parent_extend",
                "Parent/{*ext}",
                new { controller = "Generals", action = "Index" },
                new[] { "ACClient.Areas.Parent.Controllers" }
            );
        }
    }
}