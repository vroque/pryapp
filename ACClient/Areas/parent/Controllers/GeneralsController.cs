﻿using System.Collections.Generic;
using ACAccess.Authorization;
using ACBusiness.Parent;
using ACClient.Helpers.ActionFilters;
using System.Web.Mvc;
using _app = ACTools.Constants.AppConstants;
using ACBusiness.Personal;
using System.Threading.Tasks;
using ACBusiness.Academic;
using Newtonsoft.Json;

namespace ACClient.Areas.Parent.Controllers
{
    public class GeneralsController : Controller
    {
        /// <url>/frontdesk</url>
        /// <summary>
        /// Pagina de inico de Parent
        /// </summary>
        /// <returns></returns>
        [HasSession("AuthParent")]
        //public async Task<ActionResult> Index()
        public ActionResult Index()
        {
            var parent_auth = (ACAuth) Session["AuthParent"];
            var person_obj = new Person();
            var person_student = person_obj.getByStudentCode(parent_auth.id);
            var person = new Relationship();

            // obtenemos el perfil de CHILD ya seleccionado
            var student_auth = (ACAuth) Session["AuthChild"];
            if (student_auth == null) // si no selecciono ningun CHILD
            {
                return person_student == null
                    ? RedirectToRoute("parent_select_student")
                    : RedirectToRoute("parent_select_profile", new {entonces = Request.Url.AbsoluteUri});
            }

            // si ya selecciono un CHILD 
            var menu_obj = new Menu();

            var menus = menu_obj.getFromParentChild();
            ViewBag.choices = JsonConvert.SerializeObject(menus, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }
            );

            //List<Relationship> list_students = await new Relationship().listChilds(parent_auth.person_id);

            ViewBag.student = student_auth;
            ViewBag.person = parent_auth;
            ViewBag.sidebar = "profileParent";
            //ViewBag.number_students = list_students.Count;

            // Terms and conditions
            var terms = new TermsAndConditions().GetTermsJsonConverted();
            ViewBag.terms = terms;
            // Surveys
            var surveys = new Survey().GetSurveysJsonConverted();
            ViewBag.surveys = surveys;

            return View();
        }

        [HasSession("AuthParent")]
        public ActionResult selectProfile()
        {
            var student = (ACAuth) Session["Auth"];
            var parent = (ACAuth) Session["AuthParent"];
            if (student == null)
                return RedirectToRoute("parent_select_student");
            
            ViewBag.person = new Person(parent.person_id);
            return View();
        }

        public async Task<ActionResult> selectStudent()
        {
            ACAuth auth = (ACAuth)Session["AuthParent"];
            Person parent = new Person(auth.person_id);
            List<Relationship> list_students = await new Relationship().listChilds(auth.person_id);
            foreach (Relationship student in list_students)
            {
                var student_profile = AcademicProfile.listProfiles(student.child.id);
                student.profile = student_profile;
            }

            // verificamos si tiene o no una cuenta de estudiante asociada, siendo esta mixta
            ViewBag.mix = false;
            var student_obj = (ACAuth) Session["Auth"];

            if (student_obj != null) ViewBag.mix = true;

            ViewBag.lista_students = list_students;
            ViewBag.person = parent;
            return View();
        }
    }
}