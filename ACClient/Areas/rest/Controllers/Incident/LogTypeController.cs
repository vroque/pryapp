using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Incident;

namespace ACClient.Areas.rest.Controllers.Incident
{
    public class LogTypeController : ApiController
    {
        // GET: api/Incident/LogType
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await new IncidentLogType().GetAllAsync());
        }

        // GET: api/Incident/LogType/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await IncidentLogType.GetByIdAsync(huk));
        }

        // POST: api/Incident/LogType
        public async Task<IHttpActionResult> Post(IncidentLogType incidentLogType)
        {
            return Ok(await new IncidentLogType(incidentLogType).CreateAsync());
        }

        // PUT: api/Incident/LogType
        public async Task<IHttpActionResult> Put(IncidentLogType incidentLogType)
        {
            return Ok(await new IncidentLogType(incidentLogType).UpdateAsync());
        }

        // DELETE: api/Incident/LogType
        public async Task<IHttpActionResult> Delete(string huk)
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
