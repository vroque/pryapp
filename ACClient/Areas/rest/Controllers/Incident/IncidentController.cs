using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.Incident;
using _incident = ACTools.Constants.IncidentConstants;

namespace ACClient.Areas.rest.Controllers.Incident
{
    public class IncidentController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // GET: api/Incident/Incident
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await new ACBusiness.Incident.Incident().GetAllAsync());
        }

        // GET: api/Incident/Incident/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await ACBusiness.Incident.Incident.GetByIdAsync(huk));
        }

        // GET: api/Incident/Incident/:huk?view=:view
        public async Task<IHttpActionResult> Get(int huk, string view)
        {
            var incident = await ACBusiness.Incident.Incident.GetByIdAsync(huk);

            if (incident == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }

            if (view.Equals("log"))
            {
                var studentAuth = (ACAuth) _httpSessionState["Auth"];

                if (studentAuth == null) return Ok(await IncidentLog.GetAllByIncidentId(huk));
                
                List<IncidentLog> logs = await IncidentLog.GetAllByIncidentId(huk);
                var publicTasks = new List<int> {_incident.LogCreate, _incident.LogSolution, _incident.LogClose};
                return Ok(logs.Where(w => w.IsPublic || publicTasks.Contains(w.LogTypeId))
                    .OrderBy(o => o.PublicationDate).ToList());

            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Incident/Incident
        public async Task<IHttpActionResult> Post(ACBusiness.Incident.Incident incident)
        {
            var studentAuth = (ACAuth) _httpSessionState["Auth"];
            var backOfficeAuth = (ACAuth) _httpSessionState["AuthBackoffice"];

            if (studentAuth == null && backOfficeAuth == null)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            incident.AuthorPidm = backOfficeAuth?.person_id ?? studentAuth.person_id;

            if (incident.ReporterPidm < 1)
            {
                incident.ReporterPidm = studentAuth?.person_id ?? 0;
            }

            return Ok(await new ACBusiness.Incident.Incident(incident).CreateAsync());
        }

        // PUT: api/Incident/Incident
        public async Task<IHttpActionResult> Put(ACBusiness.Incident.Incident incident)
        {
            var backOfficeAuth = (ACAuth) _httpSessionState["AuthBackoffice"];
            if (backOfficeAuth == null)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            return Ok(await new ACBusiness.Incident.Incident(incident).UpdateAsync(backOfficeAuth.person_id));
        }

        // DELETE: api/Incident/Incident/:huk
        public async Task<IHttpActionResult> Delete(string huk)
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
