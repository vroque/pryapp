using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Incident;

namespace ACClient.Areas.rest.Controllers.Incident
{
    public class SubCategoryFunctionalUnitController : ApiController
    {
        // GET: api/Incident/SubCategoryFunctionalUnit
        public async Task<IHttpActionResult> Get()
        {
            return StatusCode(HttpStatusCode.Forbidden);
        }

        // GET: api/Incident/SubCategoryFunctionalUnit/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await SubCategoryFunctionalUnit.GetByIdAsync(huk));
        }

        // POST: api/Incident/SubCategoryFunctionalUnit
        public async Task<IHttpActionResult> Post(SubCategoryFunctionalUnit subCategoryFunctionalUnit)
        {
            return Ok(await new SubCategoryFunctionalUnit(subCategoryFunctionalUnit).CreateAsync());
        }

        // PUT: api/Incident/SubCategoryFunctionalUnit
        public async Task<IHttpActionResult> Put(SubCategoryFunctionalUnit subCategoryFunctionalUnit)
        {
            return Ok(await new SubCategoryFunctionalUnit(subCategoryFunctionalUnit).UpdateAsync());
        }

        // DELETE: api/Incident/SubCategoryFunctionalUnit/:huk
        public async Task<IHttpActionResult> Delete(int huk)
        {
            var reg = await SubCategoryFunctionalUnit.GetByIdAsync(huk);
            
            if (reg == null) return StatusCode(HttpStatusCode.NotFound);
            
            return Ok(await new SubCategoryFunctionalUnit(reg).DeleteAsync());
        }
    }
}
