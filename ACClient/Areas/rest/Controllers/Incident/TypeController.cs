using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Incident;

namespace ACClient.Areas.rest.Controllers.Incident
{
    public class TypeController : ApiController
    {
        // GET: api/Incident/Type
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await new IncidentType().GetAllAsync());
        }

        // GET: api/Incident/Type/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await IncidentType.GetByIdAsync(huk));
        }

        // POST: api/Incident/Type
        public async Task<IHttpActionResult> Post(IncidentType incidentType)
        {
            return Ok(await new IncidentType(incidentType).CreateAsync());
        }

        // PUT: api/Incident/Type
        public async Task<IHttpActionResult> Put(IncidentType incidentType)
        {
            return Ok(await new IncidentType(incidentType).UpdateAsync());
        }

        // DELETE: api/Incident/Type/:huk
        public async Task<IHttpActionResult> Delete(string huk)
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
