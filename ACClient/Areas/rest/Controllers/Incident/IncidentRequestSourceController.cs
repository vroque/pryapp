using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Incident;

namespace ACClient.Areas.rest.Controllers.Incident
{
    public class IncidentRequestSourceController : ApiController
    {
        // GET: api/Incident/IncidentRequestSource
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await IncidentRequestSource.GetAllAsync());
        }

        // GET: api/Incident/IncidentRequestSource/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await IncidentRequestSource.GetByIdAsync(huk));
        }

        // POST: api/Incident/IncidentRequestSource
        public async Task<IHttpActionResult> Post(IncidentRequestSource incidentRequestSource)
        {
            return Ok(await new IncidentRequestSource(incidentRequestSource).CreateAsync());
        }

        // PUT: api/Incident/IncidentRequestSource
        public async Task<IHttpActionResult> Put(IncidentRequestSource incidentRequestSource)
        {
            return Ok(await new IncidentRequestSource(incidentRequestSource).UpdateAsync());
        }

        // DELETE: api/Incident/IncidentRequestSource/:huk
        public async Task<IHttpActionResult> Delete(string huk)
        {
            return Ok(HttpStatusCode.NotImplemented);
        }
    }
}
