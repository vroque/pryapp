using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Incident;

namespace ACClient.Areas.rest.Controllers.Incident
{
    public class SubCategoryController : ApiController
    {
        // GET: api/Incident/SubCategory
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await SubCategory.GetAllAsync());
        }

        // GET: api/Incident/SubCategory/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await SubCategory.GetByIdAsync(huk));
        }

        // GET: api/Incident/SubCategory/:huk?view=:view
        public async Task<IHttpActionResult> Get(int huk, string view)
        {
            var subcategory = await SubCategory.GetByIdAsync(huk);

            if (subcategory == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }

            if (view.Equals("team"))
            {
                return Ok(await SubCategoryFunctionalUnit.GetAllBySubcategoryIdAsync(huk));
            }

            return StatusCode(HttpStatusCode.Forbidden);
        }

        // POST: api/Incident/SubCategory
        public async Task<IHttpActionResult> Post(SubCategory subCategory)
        {
            return Ok(await new SubCategory(subCategory).CreateAsync());
        }

        // PUT: api/Incident/SubCategory
        public async Task<IHttpActionResult> Put(SubCategory subCategory)
        {
            return Ok(await new SubCategory(subCategory).UpdateAsync());
        }

        // DELETE: api/Incident/SubCategory/:huk
        public async Task<IHttpActionResult> Delete(string huk)
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
