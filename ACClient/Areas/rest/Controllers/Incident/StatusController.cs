using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Incident;

namespace ACClient.Areas.rest.Controllers.Incident
{
    public class StatusController : ApiController
    {
        // GET: api/Incident/Status
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await new IncidentStatus().GetAllAsync());
        }

        // GET: api/Incident/Status/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await IncidentStatus.GetByIdAsync(huk));
        }

        // POST: api/Incident/Status
        public async Task<IHttpActionResult> Post(IncidentStatus incidentStatus)
        {
            return Ok(await new IncidentStatus(incidentStatus).CreateAsync());
        }

        // PUT: api/Incident/Status
        public async Task<IHttpActionResult> Put(IncidentStatus incidentStatus)
        {
            return Ok(await new IncidentStatus(incidentStatus).UpdateAsync());
        }

        // DELETE: api/Incident/Status/:huk
        public async Task<IHttpActionResult> Delete(string huk)
        {
            return Ok(HttpStatusCode.NotImplemented);
        }
    }
}
