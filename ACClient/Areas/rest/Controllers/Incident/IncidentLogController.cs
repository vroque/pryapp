using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.Adryan;
using ACBusiness.Incident;

namespace ACClient.Areas.rest.Controllers.Incident
{
    public class IncidentLogController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // GET: api/Incident/IncidentLog
        public async Task<IHttpActionResult> Get()
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // GET: api/Incident/IncidentLog/:huk
        public async Task<IHttpActionResult> Get(long huk)
        {
            return Ok(await IncidentLog.GetByIdAsync(huk));
        }

        // POST: api/Incident/IncidentLog
        public async Task<IHttpActionResult> Post(IncidentLog incidentLog)
        {
            var backOfficeAuth = (ACAuth) _httpSessionState["AuthBackoffice"];

            if (backOfficeAuth == null)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            var employee = await Employee.GetByPidmAndCompanyIdAsync(backOfficeAuth.person_id);
            
            if (employee == null)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }
            
            incidentLog.AuthorPidm = backOfficeAuth.person_id;
            incidentLog.TeamId = employee.unidad_funcional_organica;
            incidentLog.TeamName = employee.nombre_unidad_funcional;
            
            return Ok(await new IncidentLog(incidentLog).CreateAsync());
        }

        // PUT: api/Incident/IncidentLog
        public async Task<IHttpActionResult> Put(IncidentLog incidentLog)
        {
            return Ok(await new IncidentLog(incidentLog).UpdateAsync());
        }

        // DELETE: api/Incident/IncidentLog/:huk
        public async Task<IHttpActionResult> Delete(long huk)
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
