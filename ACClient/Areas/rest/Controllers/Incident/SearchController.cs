using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;

namespace ACClient.Areas.rest.Controllers.Incident
{
    public class SearchController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // GET: api/Incident/Search
        public async Task<IHttpActionResult> Get()
        {
            var studentAuth = (ACAuth) _httpSessionState["Auth"];

            if (studentAuth == null) return StatusCode(HttpStatusCode.Forbidden);

            return Ok(await ACBusiness.Incident.Incident.GetAllByStudentPidm(studentAuth.person_id));
        }

        // GET: api/Incident/Search?reporter=:reporter&author=:author&category=:category&type=:type&status=:status&owner=:owner&team=:team
        public async Task<IHttpActionResult> Get(int reporter, int author = 0, int category = 0, int type = 0,
            int status = 0, int owner = 0, string team = "all", decimal diff = 0)
        {
            return Ok(await ACBusiness.Incident.Incident.SearchAsync(reporter, author, category, type, status, owner,
                team, diff));
        }
    }
}
