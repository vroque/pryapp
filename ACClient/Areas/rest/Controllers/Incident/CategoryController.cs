using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Incident;

namespace ACClient.Areas.rest.Controllers.Incident
{
    public class CategoryController : ApiController
    {
        // GET: api/Incident/Category
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await new Category().GetAllAsync());
        }

        // GET: api/Incident/Category?type=:type
        public async Task<IHttpActionResult> Get(string type)
        {
            if (type.Equals("parents"))
            {
                return Ok(await new Category().GetAllWithoutParentCategoryAsync());
            }

            if (type.Equals("children"))
            {
                return Ok(await new Category().GetAllWithParentCategoryAsync());
            }

            return Content(HttpStatusCode.NoContent, string.Empty);
        }

        // GET: api/Incident/Category/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await Category.GetByIdAsync(huk));
        }

        // GET: api/Incident/Category/:huk?view=:view
        public async Task<IHttpActionResult> Get(int huk, string view)
        {
            var category = await Category.GetByIdAsync(huk);

            if (category == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }

            if (view.Equals("parent"))
            {
                return Ok(await new Category(category).GetParentCategoryAsync());
            }

            if (view.Equals("children"))
            {
                return Ok(await new Category(category).GetChildCategoriesAsync());
            }
            
            if (view.Equals("sub"))
            {
                return Ok(await SubCategory.GetAllByCategoryIdAsync(huk));
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        // POST: api/Incident/Category
        public async Task<IHttpActionResult> Post(Category category)
        {
            return Ok(await new Category(category).CreateAsync());
        }

        // PUT: api/Incident/Category
        public async Task<IHttpActionResult> Put(Category category)
        {
            return Ok(await new Category(category).UpdateAsync());
        }

        // DELETE: api/Incident/Category/:huk
        public async Task<IHttpActionResult> Delete(string huk)
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
