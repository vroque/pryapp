﻿using ACBusiness.Documentary;

using ACClient.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACClient.Helpers.ActionFilters;
//using ACClientBusiness;

namespace ACClient.Areas.rest.Controllers.documentary
{
    [HasAuth("AuthBackoffice")]
    public class ReportApplicationsByStudentController : ApiController
    {
        private IHttpSessionState session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public IHttpActionResult Get(string id_student = "", int id_document = 0, int id_state = 0)
        {
            Application reports = new Application();
            ACAuth auth_obj = (ACAuth)session["AuthBackoffice"];

            return Ok(reports.reportNumberOfTimes(auth_obj.div, auth_obj.campus.id,id_student, id_document, id_state));
        }
    }
}