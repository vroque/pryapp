﻿using ACBusiness.Accounts;
using System.Web.Http;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.enrollment
{
    [HasAuth("Auth")]
    public class InscriptionGroupController : ApiController
    {
        /// <summary>
        /// Devuelve el grupo de inscripción de un estudiante por periodo y pidm
        /// </summary>
        /// <param name="pidm">pidm del estudiante</param>
        /// <param name="term">periodo de consulta</param>
        /// <returns></returns>
        public IHttpActionResult Get([FromUri]decimal pidm, [FromUri]string term)
        {
            InscriptionGroup inscriptionGroup = new InscriptionGroup().getInscriptionGroup(pidm, term);
            return Ok(inscriptionGroup);
        }
    }
}