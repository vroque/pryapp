﻿using ACBusiness.Accounts;
using System.Web.Http;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
namespace ACClient.Areas.rest.Controllers.enrollment
{
    /// <summary>
    /// Api para ver deudas economicas
    /// </summary>
    [HasAuth("Auth")]
    public class RetentionController : ApiController
    {
        /// <summary>
        /// Devuelve el periodo actual de matrícula para un estudiante
        /// en base a la fecha actual y su campus y departament
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get([FromUri]decimal pidm)
        {
            List<Retention> retentions = new Retention().getRetentionsByPidm(pidm);
            return Ok(retentions);
        }
    }
}