﻿using ACBusiness.Accounts;
using System.Web.Http;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.enrollment
{
    [HasAuth("Auth")]
    public class StatusStudentController : ApiController
    {
        /// <summary>
        /// Devuelve el estado del estudiante en un periodo solicitado
        /// </summary>
        /// <param name="pidm">pidm del estudiante</param>
        /// <param name="term">periodo de consulta</param>
        /// <returns></returns>
        public IHttpActionResult Get([FromUri]decimal pidm)
        {
            StatusStudent status = new StatusStudent().getStatusByStudent(pidm);
            return Ok(status);
        }
    }
}