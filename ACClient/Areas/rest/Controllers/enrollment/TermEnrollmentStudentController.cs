﻿using ACBusiness.enrollment;
using ACClient.Helpers.ActionFilters;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.enrollment
{
    [HasAuth("Auth")]
    public class TermEnrollmentStudentController : ApiController
    {
        public IHttpActionResult Get([FromUri]decimal pidm)
        {
            TermEnrollmentStudent termEnrollmentStudent = new TermEnrollmentStudent().getTermEnrollmentByStudent(pidm);
            return Ok(termEnrollmentStudent);
        }
    }
}