﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.Accounts;
using ACClient.Helpers.ActionFilters;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.accounting
{
    /// <inheritdoc />
    /// <url> /api/accounting/PaymentSimulator </url>
    /// <summary>
    /// Controlador para el simulador de pagos
    /// </summary>
    [HasAuth("Auth")]
    public class PaymentSimulatorController : ApiController
    {
        private readonly IHttpSessionState _httpSession =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Obtiene los datos necesarios para el simulador en base a 
        /// el perfil de estudiante logeado (en sesión)
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get()
        {
            //obtener los datos del alumno logeado
            var paymentSimulator = new PaymentSimulator();
            var acAuth = (ACAuth) _httpSession["Auth"];

            var student = new Student(acAuth.id) {profile = acAuth.profile};
            var ps = paymentSimulator.Get(student);

            return Ok(ps);
        }
    }
}