﻿using ACBusiness.Accounts;
using System.Threading.Tasks;
using System.Web.Http;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using ACBusiness.Academic;
using ACAccess.Authorization;
using System.Web;
using System.Web.SessionState;
namespace ACClient.Areas.rest.Controllers.accounting
{
    /// <summary>
    /// Api para ver deudas economicas
    /// </summary>
    [HasAuth("Auth", "AuthChild", "AuthBackoffice")]
    public class DebtController : ApiController
    {
        private IHttpSessionState session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> Get(string id)
        {
            //obtener los datos del alumno logeado
            decimal person_id;
            if (!decimal.TryParse(id, out person_id))
            {
                return NotFound();
            };
            Debt debt = new Debt();
            List<Debt> list = await debt.query(person_id);
            return Ok(await debt.query(person_id));
            
        }

        public async Task<IHttpActionResult> post([FromBody]Data data) {
            bool validate = false;
            return Ok(validate);
        }


        public class Data
        {
            public Student student { get; set; }
            public string term { get; set; }
        }
    }
}