﻿using ACBusiness.Accounts;
using ACBusiness.Institutional;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using _acad = ACTools.Constants.AcademicConstants;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.accounting
{
    [HasAuth("Auth", "AuthBackoffice", "AuthParent")]
    public class EconomicStatusController: ApiController
    {
        EconomicStatus economic_status = new EconomicStatus();
        public async Task<IHttpActionResult> Get([FromUri]string term, [FromUri]int pidm, [FromUri]string type)
        {
            term = Term.toAPEC(term);
            List<EconomicStatus> list = new List<EconomicStatus>();
            if (type == "debts")
                list = await economic_status.getGroupedDebts(_acad.DEFAULT_DIV, term, pidm);
            else if (type == "payments")
                list = await economic_status.getPaymets(_acad.DEFAULT_DIV, term, pidm);
            return Ok(list);
        }

        public async Task<IHttpActionResult> Post([FromBody]EconomicStatus debt) {
            EconomicStatus data = await economic_status.getPaymentOnlineID(debt);
            return Ok(data);
        }

        public async Task<IHttpActionResult> Get([FromUri]int pidm)
        {
            List<string> list = await economic_status.getTermsEconomicStatus(_acad.DEFAULT_DIV, pidm);
            return Ok(list);
        }
    }
}