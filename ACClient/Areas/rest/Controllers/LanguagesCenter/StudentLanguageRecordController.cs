using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.LanguagesCenter;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    public class StudentLanguageRecordController : ApiController
    {
        // GET: api/LanguagesCenter/StudentLanguageRecord
        public IHttpActionResult Get()
        {
            return Ok(HttpStatusCode.NotImplemented);
        }
        
        // GET: api/LanguagesCenter/StudentLanguageRecord/:huk
        public async Task<IHttpActionResult> Get(string huk)
        {
            return Ok(await StudentLanguageRecord.GetByStudentIdAsync(huk));
        }
    }
}