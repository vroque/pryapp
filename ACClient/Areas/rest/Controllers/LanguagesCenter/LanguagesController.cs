﻿using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.LanguagesCenter;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    [HasAuth("AuthBackoffice")]
    public class LanguagesController : ApiController
    {
        // GET: api/Languages
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Language.GetAllAsync());
        }

        // GET: api/Languages/<languageProgram>
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await Language.GetByLanguageProgramAsync(huk));
        }
    }
}