using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.LanguagesCenter;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    public class ProfessionalEnglishController : ApiController
    {
        // GET: api/LanguagesCenter/ProfessionalEnglish
        public async Task<IHttpActionResult> Get()
        {
            return Ok(HttpStatusCode.NotImplemented);
        }
        
        // GET: api/LanguagesCenter/ProfessionalEnglish/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await Student.QualifyForEnrollInProfessionalEnglish(huk));
        }
    }
}