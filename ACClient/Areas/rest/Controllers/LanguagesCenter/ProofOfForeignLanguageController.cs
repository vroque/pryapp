﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.LanguagesCenter;
using ACClient.Helpers.ActionFilters;
using _tools_doc_cic = ACTools.PDF.Documents.LanguagesCenter;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    [HasSession("Auth", "AuthBackoffice")]
    public class ProofOfForeignLanguageController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // GET: api/ProofOfForeignLanguage
        [HasSession("Auth", "AuthBackoffice")]
        public IHttpActionResult Get()
        {
            var studentAuth = (ACAuth) _httpSessionState["Auth"];
            var doc = new ACBusiness.LanguagesCenter.Documents.ProofOfForeignLanguage(studentAuth.profile);
            return Ok(doc);
        }

        // GET: api/ProofOfForeignLanguage/<students>
        public IHttpActionResult Get(string huk)
        {
            return Ok(HttpStatusCode.NotFound);
        }

        // GET: api/ProofOfForeignLanguage/<graduationPeriod>/<languageId>
        public IHttpActionResult Get(string huk, string iskay)
        {
            return Ok(StudentUC.GetStudentsWithCaie(huk, iskay));
        }

        // POST: api/ProofOfForeignLanguage
        [HasSession("AuthBackoffice")]
        public async Task<IHttpActionResult> Post(Data students)
        {
            var casAuth = (ACAuth) _httpSessionState["AuthBackoffice"];
            var studentAuth = (ACAuth) _httpSessionState["Auth"];

            var type = students.Type;
            string[] studentsArray = { };
            ProofOfForeignLanguage[] studentsObject = { };

            switch (type)
            {
                case "searchBatch":
                case "searchOnDemand":
                case "searchInstitute":
                    studentsArray = students.StudentsId.ToArray();
                    studentsArray = studentsArray.Distinct().ToArray();
                    studentsArray = studentsArray.Where(w => !string.IsNullOrEmpty(w)).ToArray();
                    break;
                case "caie":
                case "caieInstitute":
                    studentsObject = students.StudentsObject.ToArray();
                    studentsObject = studentsObject.Distinct().ToArray();
                    break;
                default:
                    studentsArray = null;
                    studentsObject = null;
                    break;
            }

            switch (type)
            {
                // Búsqueda por código de estudiantes para CAIE Batch
                case "searchBatch":
                    return Ok(StudentUC.GetStudentsWithCaie(studentsArray));
                // Búsqueda de código de estudiantes para Generación CAIE a demananda (casos especiales)
                // según una determinada población
                case "searchOnDemand":
                    return Ok(await StudentUC.GetStudentWithCaieOnDemand(studentsArray));
                // Búsqueda para estudiantes del Instituto
                case "searchInstitute":
                    return Ok(StudentIC.GetStudentsWithCaie(studentsArray));
                // Creación de Solicitud para CAIE Uso Externo (desde frontdesk)
                case "caieExtRequest":
                    var user = casAuth ?? studentAuth;
                    var doc = new ACBusiness.LanguagesCenter.Documents.ProofOfForeignLanguage(studentAuth.profile);
                    var student = new ACBusiness.Academic.Student(studentAuth.person_id);
                    var request = await doc.CreateRequestAsync(student, studentAuth.profile, students.LanguageId,
                        students.Cycle, students.Description, user.id);
                    return Ok(request);
                // Generación de CAIE
                case "caie":
                    return Ok(await ProofOfForeignLanguage.GenerateProofOfForeignLanguageAsync(studentsObject,
                        casAuth.id));
                // Generación de CAIE para Estudiantes del Instituto
                case "caieInstitute":
                    return Ok(await StudentIC.GenerateProofOfForeignLanguageAsync(studentsObject, casAuth.id));
                // Generación de CAIE interno a demanda (con info personalizada)
                case "caieOnDemandInternal":
                    return Ok(await ProofOfForeignLanguage.GenerateCustomInternalProofOfForeignLanguageAsync(
                        students.Custom, students.StudentsObject[0], casAuth.id));
                // Generación de CAIE externo a demanda (con info personalizada)
                case "caieOnDemandExternal":
                    return Ok(await ProofOfForeignLanguage.GenerateCustomExternalProofOfForeignLanguageAsync(
                        students.Custom, students.StudentsObject[0], casAuth.id));
                default:
                    return Ok(HttpStatusCode.NotFound);
            }
        }

        public class Data
        {
            public string Type { get; set; }
            public string[] StudentsId { get; set; }
            public ProofOfForeignLanguage[] StudentsObject { get; set; }
            public string LanguageId { get; set; }
            public string Cycle { get; set; }
            public string Description { get; set; }
            public _tools_doc_cic.ProofOfForeignLanguagePDF.Data Custom { get; set; }
            public int RequestId { get; set; }
        }
    }
}