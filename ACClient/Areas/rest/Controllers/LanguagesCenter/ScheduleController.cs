﻿using ACBusiness.LanguagesCenter;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    [HasAuth("Auth")]
    public class ScheduleController : ApiController
    {

        // GET: api/LanguageLevel/2546
        public async Task<IHttpActionResult> Get(string huk)
        {
            decimal pidm = decimal.Parse(huk);
            CICSchedule obj = new CICSchedule();
            CICOfferSchedule data = await obj.getOfferSchedule(pidm);
            return Ok(data);
        }

        // GET: api/LanguageLevel/2546
        public async Task<IHttpActionResult> Get(string huk,string iskay)
        {
            decimal pidm = decimal.Parse(huk);
            CICSchedule obj = new CICSchedule();
            CICOfferSchedule data = await obj.getOfferScheduleLast(pidm);
            return Ok(data);
        }

        // POST: api/LanguageLevel
        public IHttpActionResult Post([FromBody] CICOffer offer)
        {
            CICSchedule obj = new CICSchedule();
            List<CICSchedule> list = obj.getSchedule(offer);
            return Ok(list);
        }
    }
}