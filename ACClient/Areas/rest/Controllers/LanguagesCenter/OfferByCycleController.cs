﻿using ACBusiness.LanguagesCenter;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    [HasSession("Auth")]
    public class OfferByCycleController : ApiController
    {
        // GET: api/Languages
        public async Task<IHttpActionResult> Get(
            [FromUri] string campus, 
            [FromUri] string language, 
            [FromUri] int cycle)
        {
            CICOffer obj = new CICOffer();
            List<CICOffer> offers =await obj.getActiveOfferByCycle(campus, language, cycle); // ingles
            if (offers == null) return NotFound();
            return Ok(offers);
        }

    }
}