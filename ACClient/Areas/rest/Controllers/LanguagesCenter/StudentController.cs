using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using ACBusiness.LanguagesCenter;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    public class StudentController : ApiController
    {
        // GET: api/LanguagesCenter/Student
        public IHttpActionResult Get()
        {
            return Ok(HttpStatusCode.NotImplemented);
        }

        // GET: api/LanguagesCenter/Student/:huk
        public IHttpActionResult Get(decimal huk)
        {
            return Ok(new Student(huk));
        }

        // POST: api/LanguagesCenter/Student
        public IHttpActionResult Post(Data huk)
        {
            switch (huk.Type)
            {
                case "student":
                    return Ok(Search.SearchStudentUC(huk.StudentIdList));
                case "studentUC":
                    return Ok(Search.SearchStudentUC(huk.StudentIdList));
                default:
                    return Ok(HttpStatusCode.NotFound);
            }
        }

        public class Data
        {
            public string Type { get; set; }
            public List<string> StudentIdList { get; set; }
        }
    }
}