using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.LanguagesCenter;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    public class ClassroomController : ApiController
    {
        // GET: api/LanguagesCenter/Classroom/:huk
        public async Task<IHttpActionResult> Get(string huk)
        {
            return Ok(new Cycle(huk));
        }
    }
}