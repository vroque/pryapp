﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.LanguagesCenter;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    public class LanguageLevelController : ApiController
    {
        // GET: api/LanguageLevel
        public IHttpActionResult Get()
        {
            return Content(HttpStatusCode.NoContent, "");
        }

        // GET: api/LanguageLevel/<string>
        public async Task<IHttpActionResult> Get(string huk)
        {
            var pidm = Student.GetStudentPidm(huk);
            if (pidm == 0) return Ok(HttpStatusCode.NotFound);

            return Ok(await Student.GetLanguageLevelAsync(pidm));
        }

        // GET: api/LanguageLevel/<languageId>/<languageProgram>
        public IEnumerable<LanguageLevel> Get(string huk, int iskay)
        {
            return LanguageLevel.GetByLanguageIdAndLanguageProgram(huk, iskay);
        }

    }
}