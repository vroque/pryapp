﻿using ACBusiness.LanguagesCenter;
using ACClient.Helpers.ActionFilters;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class ConstAcredIdiomExtController : ApiController
    {
        public IHttpActionResult Get([FromUri]int pidm) {
            ConstAcredIdiomExt req = new ConstAcredIdiomExt();
            return Ok(req.getConstAcredIdiomExtPIDM(pidm));
        }
    }
}