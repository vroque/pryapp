﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.AtentionCenter;
using ACBusiness.LanguagesCenter;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class StudiesCertificateController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        
        // GET: api/LanguagesCenter/StudiesCertificate
        public async Task<IHttpActionResult> Get()
        {
            var acAuth = (ACAuth) _httpSessionState["Auth"];
            var doc = new ACBusiness.LanguagesCenter.Documents.StudiesCertificate(acAuth.profile);
            return Ok(doc);
        }
        
        // POST: api/LanguagesCenter/StudiesCertificate
        public async Task<IHttpActionResult> Post(Data data)
        {
            var cauAuth = (ACAuth) _httpSessionState["AuthBackoffice"];
            var studentAuth = (ACAuth) _httpSessionState["Auth"];
            var user = cauAuth ?? studentAuth;
            var doc = new ACBusiness.LanguagesCenter.Documents.StudiesCertificate(studentAuth.profile);
            var student = new ACBusiness.Academic.Student(studentAuth.person_id);
            var request = await doc.CreateRequestAsync(student, studentAuth.profile, data.LanguageId, data.Cycles,
                data.Description, user.id);

            if (ACRequest.isNull(request)) return Ok(HttpStatusCode.InternalServerError);
            
            return Ok(request);
        }
        
        public class Data
        {
            public string LanguageId { get; set; }
            public List<StudentLanguageRecord> Cycles { get; set; }
            public string Description { get; set; }
        }
    }
}