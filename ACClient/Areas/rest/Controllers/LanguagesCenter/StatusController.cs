﻿using ACAccess.Authorization;
using ACBusiness.LanguagesCenter;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    /// <summary>
    /// Esado del estudiante en el CIC
    /// aka ultimo ciclo alncanzado
    /// </summary>
    [HasAuth("Auth")]
    public class StatusController : ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        // GET: api/LanguageLevel
        public async System.Threading.Tasks.Task<IHttpActionResult> Get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            CICStatus obj = new CICStatus();
            CICStatus s = await obj.getCICStatus(auth_obj.person_id);
            if (s == null) return NotFound();
            return Ok(s);
        }
    }
}