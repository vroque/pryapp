﻿using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    /// <inheritdoc />
    /// <summary>
    /// API: Solicitudes CIC
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class RequestController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        
        // GET: api/LanguagesCenter/Request
        public async Task<IHttpActionResult> Get()
        {
            var requests = await ACRequest.LanguagesCenterRequestListAsync();
            return Ok(requests);
        }
    }
}