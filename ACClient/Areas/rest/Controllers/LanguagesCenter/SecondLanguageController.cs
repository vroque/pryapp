using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.LanguagesCenter;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    public class SecondLanguageController : ApiController
    {
        // GET: api/LanguagesCenter/SecondLanguage
        public async Task<IHttpActionResult> Get()
        {
            return Ok(HttpStatusCode.NotImplemented);
        }

        // GET: api/LanguagesCenter/SecondLanguage/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await Student.HasSecondLanguageForProofOfForeignLanguageAsync(huk));
        }

        // GET: api/LanguagesCenter/SecondLanguage/:huk/:iskay
        public async Task<IHttpActionResult> Get(int huk, string iskay)
        {
            return Ok(await Student.HasSecondLanguageForProofOfForeignLanguageByProgramIdAsync(huk, iskay));
        }
    }
}