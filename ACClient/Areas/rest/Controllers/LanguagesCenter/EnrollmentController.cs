﻿using System.Collections.Generic;
using System.Web.Http;
using ACBusiness.LanguagesCenter;
using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Collections;
using ACPermanence.DataBases.DBUCCI.CIC;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    public class EnrollmentController: ApiController
    {

        public IHttpActionResult Post(CICEnrolment data)
        {
            Enrollment enr = new Enrollment();
            return Ok(enr.ejecutarMatricula(data.pidm,data.section));
        }

    }
}