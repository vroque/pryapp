﻿using System.Collections.Generic;
using System.Web.Http;
using ACBusiness.LanguagesCenter;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    public class PeriodsController : ApiController
    {
        // GET: api/Periods
        public IEnumerable<Periods> Get()
        {
            return Periods.getAll();
        }
    }
}