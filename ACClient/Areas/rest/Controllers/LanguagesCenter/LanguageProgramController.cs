﻿using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.LanguagesCenter;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    public class LanguageProgramController : ApiController
    {
        // GET: api/LanguagesCenter/LanguageProgram
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await LanguageProgram.GetAllAsync());
        }
    }
}
