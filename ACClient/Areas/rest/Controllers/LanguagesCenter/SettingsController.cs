﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACClient.Helpers.ActionFilters;
using _cic = ACBusiness.LanguagesCenter;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    /// <summary>
    /// Configuraciones para el Centro de Idiomas
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class SettingsController : ApiController
    {
        // GET: api/LanguagesCenter/Settings
        public async Task<IHttpActionResult> Get()
        {
            return Ok(HttpStatusCode.NotImplemented);
        }

        // GET: api/LanguagesCenter/Settings/:huk
        public async Task<IHttpActionResult> Get(string huk)
        {
            switch (huk)
            {
                case "caie":
                    return Ok(await _cic.Settings.GetCaieLanguageLevelName());
                default:
                    return Ok(HttpStatusCode.NotFound);
            }
        }

        // PUT: api/LanguagesCenter/Settings
        public async Task<IHttpActionResult> Put(Data data)
        {
            switch (data.Type)
            {
                case "caie":
                    return Ok(await _cic.Settings.SaveCaieLanguageLevelName(data.Settings));
                default:
                    return Ok(HttpStatusCode.BadRequest);
            }
        }

        public class Data
        {
            public _cic.Settings Settings { get; set; }
            public string Type { get; set; }
        }
    }
}