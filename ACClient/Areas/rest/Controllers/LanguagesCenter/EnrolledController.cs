﻿using System.Collections.Generic;
using System.Web.Http;
using ACBusiness.LanguagesCenter;

namespace ACClient.Areas.rest.Controllers.LanguagesCenter
{
    public class EnrolledController : ApiController
    {
        // GET: api/LanguageLevel
        public IEnumerable<Enrolled> Get()
        {
            return Enrolled.GetAll();
        }

        // GET: api/LanguageLevel/<languageId>/<languageProgram>/<languageLevelId>
        public IEnumerable<Enrolled> Get(string huk, string iskay, string kimsa)
        {
            return Enrolled.GetByLangAndLangProgramAndLangLevel(huk, iskay, kimsa);
        }
    }
}