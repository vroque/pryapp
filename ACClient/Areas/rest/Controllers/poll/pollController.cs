﻿using ACBusiness.Poll;
using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.poll
{
    [HasAuth("Auth")]
    public class pollController: ApiController
    {
        public async Task<IHttpActionResult> Get([FromUri]int requestState) {
            Poll objPoll = new Poll();
            objPoll = await objPoll.GetPollByRequestState(requestState);
            return Ok(objPoll);
        }
    }
}