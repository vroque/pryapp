﻿using ACAccess.Authorization;
using ACBusiness.Poll;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.poll
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class pollUserController: ApiController
    {
        PollUser objPollUser = new PollUser();
        private IHttpSessionState session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> Get([FromUri]int requestId)
        {
            bool validate = false;
            ACAuth AuthBackoffice = (ACAuth)session["AuthBackoffice"];
            if (AuthBackoffice == null )
            {
                validate = await objPollUser.ValidateMustPollRequest(requestId);
            } 
            return Ok(validate);
        }

        public async Task<IHttpActionResult> Post([FromBody] Data data)
        {
            objPollUser = await objPollUser.SavePollUser(data.data, data.dataPollChoiceDetail);
            return Ok(objPollUser);
        }


        public class Data
        {
            public PollUser data { get; set; }
            public List<PollChoiceDetail> dataPollChoiceDetail { get; set; }
        }
    }
}