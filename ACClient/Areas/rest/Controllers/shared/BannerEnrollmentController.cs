using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.Extra;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.shared
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class BannerEnrollmentController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // GET: api/AtentionCenter/Shared/BannerEnrollment
        public async Task<IHttpActionResult> Get()
        {
            var studentAuth = (ACAuth) _httpSessionState["Auth"];

            if (studentAuth == null) return Ok(new BannerEnrollment());

            var hasPendingTerms =
                await BannerEnrollment.HasPendingTermsAndConditions(studentAuth.person_id, studentAuth.profile);

            return Ok(hasPendingTerms);
        }
    }
}