﻿using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using ACTools.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using _ate = ACTools.Constants.AtentionCenterConstants;

namespace ACClient.Areas.rest.Controllers.shared
{
    [HasSession("AuthBackoffice")]
    public class mailRequestController : ApiController
    {
        /// <summary>
        ///     Envia un correo
        /// </summary>
        /// <param name="id">Codigo de solicitud</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> post(int id)
        {
            ACRequest req = await ACRequest.get(id);
            
            if(req.document_paths != null && req.document_paths.Count > 0)
            {
                Student st = new Student(req.person_id);
                MailSender mail = new MailSender();

                foreach (string item in req.document_paths)
                    mail.addFile(item);

                mail.compose("request_send",
                    new
                    {
                        id = req.id,
                        student_id = st.id,
                        first_name = st.first_name,
                        full_name = st.full_name,
                        document_name = ACDocument.get(req.document_id).name,
                        date = $"{req.creation_date:dd/MM/yyyy hh:mm tt}",
                        date_now = $"{DateTime.Now:dd/MM/yyyy hh:mm tt}"
                        // state_name = _ate.STATES_NAMES[req.state]
                    })
                    .destination($"{st.id}@continental.edu.pe", $"[CAU] Envio de Documentos - Solicitud #{req.id}")
                    .send();
                return Ok(new { ok = "ok"});
            }
            return InternalServerError();
            

            
        }
    }
}