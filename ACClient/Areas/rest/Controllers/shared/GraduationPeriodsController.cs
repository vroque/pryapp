﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.shared
{
    [HasAuth("AuthBackoffice")]
    public class GraduationPeriodsController : ApiController
    {
        // GET: api/GraduationPeriods
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await GraduationPeriods.GetAllAsync());
        }

        // GET: api/GraduationPeriods/5
        public IHttpActionResult Get(int id)
        {
            return Ok(HttpStatusCode.NotFound);
        }

        // POST: api/GraduationPeriods
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/GraduationPeriods/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/GraduationPeriods/5
        public void Delete(int id)
        {
        }
    }
}