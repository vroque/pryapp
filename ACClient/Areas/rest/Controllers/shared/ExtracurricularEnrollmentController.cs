﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.Extra;
using ACBusiness.TermsAndConditions;

namespace ACClient.Areas.rest.Controllers.shared
{
    public class ExtracurricularEnrollmentController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // GET: api/AtentionCenter/Shared/ExtracurricularEnrollment
        public async Task<IHttpActionResult> Get()
        {
            var studentAuth = (ACAuth)_httpSessionState["Auth"];
            if (studentAuth == null) return Ok(new ExtracurricularEnrollment());
            var hasPendingTerms =
                await ExtracurricularEnrollment.GetDetailExtracurricular(studentAuth.person_id, studentAuth.profile);

            return Ok(hasPendingTerms);
        }

        /// <summary>
        /// Verifica que el estudiante haya aceptado un determinado terminos y condiciones
        /// </summary>
        /// <param name="pidm">pidm del estudiante</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get(decimal pidm)
        {
            return Ok(await AcceptedTerms.Get(pidm));
        }
    }
}