using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACClient.Helpers.ActionFilters;
using _bSettings = ACBusiness.Settings;

namespace ACClient.Areas.rest.Controllers.shared
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class LockCardsController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        private const int SettingTypeForCardLock = 2;

        // GET: api/Shared/LockCards/:id
        public async Task<IHttpActionResult> Get(string id)
        {
            var studentAuth = (ACAuth) _httpSessionState["Auth"];
            var backOfficeAuth = (ACAuth) _httpSessionState["AuthBackoffice"];

            if (studentAuth == null && backOfficeAuth == null) return Ok(HttpStatusCode.Forbidden);

            List<_bSettings.Settings> isActive =
                await _bSettings.Settings.GetActiveSettingsByAbbrAndBySettingTypeId(id, SettingTypeForCardLock);
            return Ok(!isActive.Any());
        }
    }
}