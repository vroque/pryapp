﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACBusiness.Benefit;

namespace ACClient.Areas.rest.Controllers.shared
{
    public class mockController : ApiController
    {
        public IHttpActionResult Get()
        {
            return Ok();
        }
    }
}