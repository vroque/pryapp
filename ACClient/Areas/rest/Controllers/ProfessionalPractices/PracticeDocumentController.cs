﻿using ACBusiness.ProfessionalPractices;
using ACClient.Helpers.ActionFilters;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.ProfessionalPractices
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class PracticeDocumentController: ApiController
    {
        public IHttpActionResult Get([FromUri]int pidm, [FromUri]string idescuela)
        {
            PracticeDocument req = new PracticeDocument();
            return Ok(req.getPracticeDocumentPIDM(pidm, idescuela));
        }
    }
}