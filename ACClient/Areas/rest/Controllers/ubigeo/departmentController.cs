﻿using ACBusiness.Ubigeo;
using ACClient.Helpers.ActionFilters;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.ubigeo
{
    [HasSession("Auth", "AuthBackoffice")]
    public class departmentController : ApiController
    {
        public IHttpActionResult Get()
        {
            UbigeoDepartment department = new UbigeoDepartment();
            return Ok(department.query());
        }
    }
}