﻿using ACBusiness.Ubigeo;
using ACClient.Helpers.ActionFilters;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.ubigeo
{
    [HasSession("Auth", "AuthBackoffice")]
    public class provinceController : ApiController
    {
        public IHttpActionResult Get()
        {
            UbigeoProvince province = new UbigeoProvince();
            return Ok(province.query());
        }
        public IHttpActionResult Get(string id)
        {
            UbigeoProvince province = new UbigeoProvince();
            return Ok(province.filter(id));
        }
    }
}