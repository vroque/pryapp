﻿using ACBusiness.Ubigeo;
using ACClient.Helpers.ActionFilters;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.ubigeo
{
    [HasSession("Auth", "AuthBackoffice")]
    public class districtController : ApiController
    {
        public IHttpActionResult Get()
        {
            UbigeoDistrict district = new UbigeoDistrict();
            return Ok(district.query());
        }
        public IHttpActionResult Get(string id)
        {
            UbigeoDistrict district = new UbigeoDistrict();
            return Ok(district.filter(id));
        }
    }
}