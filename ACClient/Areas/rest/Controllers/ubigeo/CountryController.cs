﻿using System.Web.Http;
using ACBusiness.Ubigeo;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.ubigeo
{
    [HasSession("Auth", "AuthBackoffice")]
    public class countryController: ApiController
    {
        public IHttpActionResult Get()
        {
            UbigeoCountry country = new UbigeoCountry();
            return Ok(country.query());
        }
    }
}