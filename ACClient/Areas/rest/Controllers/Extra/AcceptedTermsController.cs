using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.TermsAndConditions;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.Extra
{
    [HasSession("Auth", "AuthBackoffice")]
    public class AcceptedTermsController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // POST: api/Extra/AcceptedTerms
        public async Task<IHttpActionResult> Post(AcceptedTerms acceptedTerms)
        {
            var studentAuth = (ACAuth) _httpSessionState["Auth"];

            if (studentAuth == null) return Ok(HttpStatusCode.BadRequest);
            acceptedTerms.Pidm = studentAuth.person_id;
            
            return Ok(await new AcceptedTerms(acceptedTerms).CreateAsync());
        }
    }
}