﻿using ACBusiness.Extra;
using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.Extra
{
    [HasAuth("Auth")]
    public class WifiController : ApiController
    {
        // GET: api/Extra/Wifi/:pidm
        public async Task<IHttpActionResult> Get(decimal huk)
        {
            return Ok(await new Wifi().GetAsync(huk));
        }
    }
}
