﻿using ACBusiness.Extra;
using ACClient.Helpers.ActionFilters;
using ACPermanence.DataBases.NEXO.CAU;
using System.Collections.Generic;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.Extra
{
    public class DisplayBlockController : ApiController
    {
        // GET: api/Extra/displayblock
        [HasAuth("Auth", "AuthBackoffice")]
        public IHttpActionResult Get([FromUri]int id, [FromUri]string type)
        {
            if (type == "one")
            {
                DisplayBlock display = new DisplayBlock().getRegister(id);
                return Ok(display);
            }
            else if (type == "all")
            {
                List<DisplayBlock> display = new DisplayBlock().getRegisters();
                return Ok(display);
            }

            return null;
        }

        [HasAuth("AuthBackoffice")]
        public IHttpActionResult POST([FromBody]CAUTblDisplayBlock name)
        {
            DisplayBlock display = new DisplayBlock().post(name);
            return Ok(display);
        }

        [HasAuth("AuthBackoffice")]
        public IHttpActionResult PUT([FromBody] CAUTblDisplayBlock register)
        {
            DisplayBlock update = new DisplayBlock().put(register);
            if (update != null)
                return Ok(update);
            return InternalServerError();
        }

    }
}
