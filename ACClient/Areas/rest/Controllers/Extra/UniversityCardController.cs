﻿using ACAccess.Authorization;
using ACBusiness.Extra;
using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.Extra
{
    [HasAuth("Auth")]
    public class UniversityCardController: ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        [HasModule("fd_estudiante")]
        public async Task<IHttpActionResult> Get()
        {
            ACAuth authObj = (ACAuth)session["Auth"];
            return Ok(new UniversityCard().get(authObj.person_id, authObj.profile.department, authObj.profile.campus.id, authObj.profile.program.id));
        }
    }
}