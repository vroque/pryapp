﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ContinentalProject;

namespace ACClient.Areas.rest.Controllers.ContinentalProject
{
    public class ProjectMembersController : ApiController
    {
        public async Task<IHttpActionResult> Get(int id)
        {
            return Ok(await ProjectMember.GetByIdProject(id));
        }
    }
}
