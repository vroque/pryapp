﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.ContinentalProject;

namespace ACClient.Areas.rest.Controllers.ContinentalProject
{
    public class ManageProjectsController : ApiController
    {
        // API: /api/ContinentalProject/ManageProjects
        private readonly IHttpSessionState _session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public async Task<IHttpActionResult> Put(EnrollProject project)
        {
            var auth = (ACAuth)_session["AuthBackoffice"];
            return Ok(await Project.Approve(project, auth.id));
        }
    }
}
