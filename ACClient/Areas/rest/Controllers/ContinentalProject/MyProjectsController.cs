﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.ContinentalProject;

namespace ACClient.Areas.rest.Controllers.ContinentalProject
{
    public class MyProjectsController : ApiController
    {
        // API: /api/ContinentalProject/MyProjects
        private readonly IHttpSessionState _session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> Get()
        {
            var auth = (ACAuth)_session["AuthBackoffice"];
            return Ok(await Project.GetByPidm(auth.person_id));
        }
    }
}
