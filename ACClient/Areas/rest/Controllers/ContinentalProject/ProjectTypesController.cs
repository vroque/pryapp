﻿using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ContinentalProject;

namespace ACClient.Areas.rest.Controllers.ContinentalProject
{
    public class ProjectTypesController : ApiController
    {
        // API: /api/ContinentalProject/ProjectTypes
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await ProjectType.GetAll());
        }
    }
}
