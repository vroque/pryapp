﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.ContinentalProject;

namespace ACClient.Areas.rest.Controllers.ContinentalProject
{
    public class EnrollmentsController : ApiController
    {
        // API: /api/ContinentalProject/Enrollments
        private readonly IHttpSessionState _session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> Get(int idProject)
        {
            var auth = (ACAuth)_session["AuthBackoffice"] ?? (ACAuth)_session["Auth"];
            return Ok(await EnrollProject.GetEnrollByIdProject(auth.person_id, idProject));
        }

        public async Task<IHttpActionResult> Post([FromBody]int idProfileProject)
        {
            var auth = (ACAuth)_session["AuthBackoffice"] ?? (ACAuth)_session["Auth"];
            return Ok(await EnrollProject.Enroll(idProfileProject, auth.id, auth.campus.id, auth.person_id, auth.profile));
        }

        public async Task<IHttpActionResult> Put(EnrollProject enrollProject)
        {
            var auth = (ACAuth)_session["AuthBackoffice"];
            return Ok(await EnrollProject.Approve(enrollProject, auth.id));
        }
    }
}
