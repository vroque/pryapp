﻿using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ContinentalProject;

namespace ACClient.Areas.rest.Controllers.ContinentalProject
{
    public class CategoriesController : ApiController
    {
        // API: /api/ContinentalProject/Categories
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Category.GetAll());
        }
    }
}
