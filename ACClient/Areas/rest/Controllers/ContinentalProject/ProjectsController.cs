﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.ContinentalProject;

namespace ACClient.Areas.rest.Controllers.ContinentalProject
{
    public class ProjectsController : ApiController
    {
        // API: /api/ContinentalProject/Projects
        private readonly IHttpSessionState _session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Project.GetAll());
        }

        public async Task<IHttpActionResult> Get(int id)
        {
            return Ok(await Project.GetById(id));
        }

        public async Task<IHttpActionResult> GetByCategoryId(int idCategory)
        {
            return Ok(await Project.GetByCategoryId(idCategory));
        }

        public async Task<IHttpActionResult> Post(Project project)
        {
            var auth = (ACAuth)_session["AuthBackoffice"];
            return Ok(await Project.Create(project, auth.campus.id, auth.person_id, auth.id));
        }

        public async Task<IHttpActionResult> Put(Project project)
        {
            var auth = (ACAuth)_session["AuthBackoffice"];
            return Ok(await Project.Update(project, auth.id));
        }
    }
}
