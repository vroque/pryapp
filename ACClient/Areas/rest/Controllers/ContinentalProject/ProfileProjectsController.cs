﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.ContinentalProject;

namespace ACClient.Areas.rest.Controllers.ContinentalProject
{
    public class ProfileProjectsController : ApiController
    {
        // API: /api/ContinentalProject/ProfileProjects
        private readonly IHttpSessionState _session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public async Task<IHttpActionResult> Post(ProfileProject profileProject)
        {
            var auth = (ACAuth)_session["AuthBackoffice"];
            return Ok(await ProfileProject.Create(profileProject, auth.id));
        }

        public async Task<IHttpActionResult> Put(ProfileProject profileProject)
        {
            var auth = (ACAuth)_session["AuthBackoffice"];
            return Ok(await ProfileProject.Update(profileProject, auth.id));
        }

        public async Task<IHttpActionResult> Delete(int idProfileProject)
        {
            var auth = (ACAuth)_session["AuthBackoffice"];
            return Ok(await ProfileProject.Delete(idProfileProject, auth.id));
        }
    }
}
