﻿using ACAccess.Authorization;
using ACBusiness.Personal.VerificationRecords;
using ACClient.Helpers.ActionFilters;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.personal
{
    /// <author>Arturo Bolaños</author>
    /// <date>16/06/2016</date>
    /// <url>/api/personal/SRAcademic/{id}</url>
    /// <summary>
    /// API para Academico
    /// </summary>
    [HasSession("Auth")]
    public class VRObservationController : ApiController
    {
        private IHttpSessionState session =
             SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> Get(decimal id)
        {
            return Ok(await VRObservation.query(id));
        }

        //save or update
        public async Task<IHttpActionResult> Post(VRObservation observation)
        {
            ACAuth auth_obj = (ACAuth)session["AuthBackoffice"];
            observation.date = DateTime.Now;
            observation.name = auth_obj.full_name;
            int r = await observation.save(auth_obj.id);
            if (r > 0) { return Ok(observation); }
            return Content(HttpStatusCode.NoContent, "No Hay Cambios");
        }
    }
}