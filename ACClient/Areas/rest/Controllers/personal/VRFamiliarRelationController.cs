﻿using ACAccess.Authorization;
using ACBusiness.Personal.VerificationRecords;
using ACClient.Helpers.ActionFilters;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.personal
{
    /// <author>Arturo Bolaños</author>
    /// <date>16/06/2016</date>
    /// <url>/api/personal/SRFamiliarRelation/{id}</url>
    /// <summary>
    /// API para relaciones familiares
    /// </summary>
    [HasSession("Auth")]
    public class VRFamiliarRelationController: ApiController
    {
        private IHttpSessionState session =
             SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> Get(decimal id)
        {
            return Ok(await VRFamiliarRelation.get(id, "1.1"));
        }

        public async Task<IHttpActionResult> Get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            return Ok(await VRFamiliarRelation.list(Decimal.Parse(auth_obj.id), "1.1"));
        }
        //save or update
        public async Task<IHttpActionResult> Post(string id, VRFamiliarRelation relation)
        {
            string r = await relation.save(id, "1.1");
            if (r != null) { return Ok(new { person_id = r }); }
            //return Content(HttpStatusCode.NoContent, "No Hay Cambios");
            return NotFound();
        }
        public IHttpActionResult Delete(string id)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            decimal r = VRFamiliarRelation.remove( Decimal.Parse(auth_obj.id), id );
            if (r != 0) { return Ok(new { person_id = r }); }
            //return Content(HttpStatusCode.NoContent, "No Hay Cambios");
            return NotFound();
        }
    }
}