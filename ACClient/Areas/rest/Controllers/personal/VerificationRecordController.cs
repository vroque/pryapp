﻿using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using System.Threading.Tasks;
using ACClient.Helpers.ActionFilters;
using ACBusiness.Personal;
using ACAccess.Authorization;

namespace ACClient.Areas.rest.Controllers.personal
{
    /// <author>Arturo Bolaños</author>
    /// <date>16/06/2016</date>
    /// <url>/api/personal/VerificationRecord</url>
    /// <summary>
    /// API para Verificación de Datos de la ficha
    /// </summary>
    [HasSession("AuthBackoffice")]
    public class VerificationRecordController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public async Task<IHttpActionResult> Get(string id)
        {
            ACAuth auth_obj = (ACAuth)session["AuthBackoffice"];
            VerificationRecord socrec = await VerificationRecord.getByStudentCode(id, auth_obj.id);
            return Ok(socrec);
        }
    }
}