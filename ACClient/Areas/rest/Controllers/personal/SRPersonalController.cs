﻿using ACBusiness.Personal;
using ACClient.Helpers.ActionFilters;
using System.Net;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.personal
{
    /// <author>Arturo Bolaños</author>
    /// <date>16/06/2016</date>
    /// <url>/api/personal/SRPersonal/{id}</url>
    /// <summary>
    /// API para Academico
    /// </summary>
    [HasSession("Auth")]
    public class SRPersonalController: ApiController
    {
       
        public IHttpActionResult Get(string id)
        {
            //return Ok(Person.get(id));
            return InternalServerError();
        }

        //save or update
        public IHttpActionResult Post(string id, Person person)
        {
            // no aseguramos que solo cambien los datos permitidos en la ficha
            Person obj = new Person(person.id);
            obj.gender = person.gender;
            obj.marital_status = person.marital_status;
            obj.country = person.country;
            obj.ubigeo = person.ubigeo;
            // guardamos con los cambios permitidos
            int r = obj.Update();
            if (r > 0) { return Ok(); }
            return Content(HttpStatusCode.NoContent, "No Hay Cambios");
        }
    }
}