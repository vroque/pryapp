﻿
using ACAccess.Authorization;
using ACBusiness.Personal.SocieconomicRecords;
using ACClient.Helpers.ActionFilters;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.personal
{
    /// <author>Arturo Bolaños</author>
    /// <date>16/06/2016</date>
    /// <url>/api/personal/SRAcademic/{id}</url>
    /// <summary>
    /// API para Academico
    /// </summary>
    [HasSession("Auth")]
    public class SRAbstractController : ApiController
    {
        private IHttpSessionState session =
             SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public IHttpActionResult Get(decimal id)
        {
            return Ok(SRAcademic.get(id, "1.0"));
        }

        //save or update
        public IHttpActionResult Post(SRAbstract academic)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            int r = academic.save(auth_obj.person_id, "1.0");
            if (r > 0) { return Ok(new {status="ok" }); }
            return Content(HttpStatusCode.NoContent, "No Hay Cambios");
        }
    }
}