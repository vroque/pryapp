﻿using ACClient.Helpers.ActionFilters;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACBusiness.Personal;
using ACAccess.Authorization;


namespace ACClient.Areas.rest.Controllers.personal
{
    /// <author>Arturo Bolaños</author>
    /// <date>16/06/2016</date>
    /// <url>/api/personal/SocioeconomicRecord</url>
    /// <summary>
    /// API para Academico
    /// </summary>
    [HasSession("Auth")]
    public class SocioeconomicRecordController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public IHttpActionResult Get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            SocioeconomicRecord socrec = SocioeconomicRecord.getByStudentCode(auth_obj.id);
            return Ok(socrec);
        }
    }
}