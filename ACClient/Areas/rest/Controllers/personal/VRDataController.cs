﻿using ACClient.Helpers.ActionFilters;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Personal;

namespace ACClient.Areas.rest.Controllers.personal
{
    /// <author>Arturo Bolaños</author>
    /// <date>17/06/2016</date>
    /// <url>/api/personal/VRData</url>
    /// <summary>
    /// API para Academico
    /// </summary>
    [HasSession("AuthBackoffice")]
    public class VRDataController : ApiController
    {
        public async Task<IHttpActionResult> post(Data data)
        {
            VerificationRecord vr = new VerificationRecord();
            bool sw = await vr.save_single(data.person_id, data.level, data.key, data.value);
            if(sw)
                return Ok(new {status = true, time = DateTime.Now });
            return NotFound();
        }
    }
    public class Data {
        public string person_id {get; set;}
        public string level {get; set;}
        public string key {get; set;}
        public string value {get; set;}
    }
}
