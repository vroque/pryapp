﻿using ACBusiness.Personal;
using ACClient.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.personal
{
    [HasAuth("Auth", "AuthBackoffice", "AuthParent")]
    public class PhotoController: ApiController
    {
        [CacheFilter(TimeDuration=100)]
        public IHttpActionResult Get(string id)
        {
            decimal person_id;
            if (!decimal.TryParse(id, out person_id))
            {
                return NotFound();
            };            
            
            Person p = new Person();
            byte[] photo = p.getPhoto(person_id);
            if(photo == null){
                string fullPath = System.Web.Hosting.HostingEnvironment.MapPath(@"~/Content/static/images/foto@default.png");
                return new FileResult(fullPath, "image/png");
            }else{
                return new FileResult(photo, "image/png");
            }
        }

    }
}

         