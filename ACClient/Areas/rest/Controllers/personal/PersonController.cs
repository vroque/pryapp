﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.Personal;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.personal
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class PersonController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // GET: api/Personal/Person
        [HasSession("Auth", "AuthBackoffice")]
        public async Task<IHttpActionResult> Get()
        {
            var studentAuth = (ACAuth) _httpSessionState["Auth"];
            return Ok(new Person(studentAuth.person_id));
        }

        // GET: api/Personal/Person/:huk
        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> Get(decimal id)
        {
            return Ok(new Person(id));
        }

        // GET: api/Personal/Person/:huk
        [HasAuth("AuthBackoffice")]
        public IHttpActionResult Get(string dni)
        {
            return Ok(new Person().getByDNI(dni));
        }

        // POST: api/Personal/Person
        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> Post(Data data)
        {
            var type = data.Type;

            var acAuth = (ACAuth) _httpSessionState["AuthBackoffice"];

            switch (type)
            {
                // Registro de nueva persona
                case "new":
                    return Ok(await Person.NewAsync(data.Person, acAuth.id));
                // Búsqueda de personas por número de documento (DNI o CE)
                case "search":
                    var documentNumber = data.People.ToArray();
                    return Ok(await Person.SearchByDocumentNumberAsync(documentNumber));
                default:
                    return Ok(HttpStatusCode.NoContent);
            }
        }

        public class Data
        {
            public string Type { get; set; }
            public decimal StudentPidm { get; set; }
            public string[] People { get; set; }
            public Person Person { get; set; }
        }
    }
}
