﻿using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Personal;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.personal
{
    /// <summary>
    /// API: Parentesco
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class KinshipController : ApiController
    {
        // GET: api/Personal/Kinship
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Kinship.GetTask());
        }
    }
}