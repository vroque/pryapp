﻿
using ACAccess.Authorization;
using ACBusiness.Personal.SocieconomicRecords;
using ACClient.Helpers.ActionFilters;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.personal
{
    /// <author>Arturo Bolaños</author>
    /// <date>16/06/2016</date>
    /// <url>/api/personal/SRFamiliarRelation/{id}</url>
    /// <summary>
    /// API para relaciones familiares
    /// </summary>
    [HasSession("Auth")]
    public class SRFamiliarRelationController: ApiController
    {
        private IHttpSessionState session =
             SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public IHttpActionResult Get(decimal id)
        {
            return Ok(SRFamiliarRelation.get(id, "1.0"));
        }

        public IHttpActionResult Get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            return Ok(SRFamiliarRelation.list(auth_obj.person_id, "1.0"));
        }
        //save or update
        public IHttpActionResult Post(decimal id, SRFamiliarRelation relation)
        {
            decimal r = relation.save(id, "1.0");
            if (r > 0) { return Ok(new { person_id = r }); }
            //return Content(HttpStatusCode.NoContent, "No Hay Cambios");
            return NotFound();
        }
        public IHttpActionResult Delete(decimal id)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            decimal r = SRFamiliarRelation.remove(auth_obj.person_id, id);
            if (r > 0) { return Ok(new { person_id = r }); }
            //return Content(HttpStatusCode.NoContent, "No Hay Cambios");
            return NotFound();
        }
    }
}