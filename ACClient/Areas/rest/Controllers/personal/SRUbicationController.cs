﻿using ACBusiness.Personal.SocieconomicRecords;
using ACClient.Helpers.ActionFilters;
using System.Net;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.personal
{
    /// <author>Arturo Bolaños</author>
    /// <date>16/06/2016</date>
    /// <url>/api/personal/SRPersonal/{id}</url>
    /// <summary>
    /// API para Ubicación
    /// </summary>
    [HasSession("Auth")]
    public class SRUbicationController : ApiController
    {
        public IHttpActionResult Get(decimal id)
        {
            return Ok(SRUbication.get(id, "1.0"));
        }

        //save or update
        public IHttpActionResult Post(decimal id, SRUbication ubication)
        {
            int r = ubication.save(id, "1.0");
            if (r > 0) { return Ok(); }
            return Content(HttpStatusCode.NoContent, "No Hay Cambios");
        }
    }
}