﻿using ACBusiness.Personal.SocieconomicRecords;
using ACClient.Helpers.ActionFilters;
using System.Net;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.personal
{
    /// <author>Arturo Bolaños</author>
    /// <date>16/06/2016</date>
    /// <url>/api/personal/SRIncome/{id}</url>
    /// <summary>
    /// API para Academico
    /// </summary>
    [HasSession("Auth")]
    public class SRIncomeController: ApiController
    {
       
        public IHttpActionResult Get(decimal id)
        {
            return Ok(SRIncome.get(id, "1.0"));
        }

        //save or update
        public IHttpActionResult Post(decimal id, SRIncome income)
        {
            int r = income.save(id, "1.0");
            if (r > 0) { return Ok(); }
            return Content(HttpStatusCode.NoContent, "No Hay Cambios");
        }
    }
}