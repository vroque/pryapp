﻿using ACBusiness.Institutional;
using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/Modalities/

    [HasAuth("AuthBackoffice")]
    public class ModalitiesController : ApiController
    {
        /// <summary>
        /// Obtiene las modalidades.
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Department.GetAllAsync());
        }
    }
}