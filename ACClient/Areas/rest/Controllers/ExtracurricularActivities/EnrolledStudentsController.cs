﻿using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ExtracurricularActivities;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API:  api/ExtracurricularActivities/EnrolledStudents/

    public class EnrolledStudentsController : ApiController
    {
        /// <summary>
        /// Retorna la lista de estudiante inscritos en un programa que ya fueron seleccionados, 
        /// con el fin de registrar su seguimiento.
        /// </summary>
        /// <param name="id">ID de la programación de la actividad</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get(int id)
        {
            return Ok(await EnrolledProgramActivity.GetEnrolledStudents(id));
        }

        /// <summary>
        /// Retorna la lista de estudiante inscritos en un programa, con el fin de descargar un excel.
        /// </summary>
        /// <param name="idProgramming">ID de la programación de la actividad</param>
        /// <param name="idActivityType">ID de tipo de actividad</param>
        /// <returns></returns>
       // [HttpGet]
        public async Task<IHttpActionResult> GetRelationStudent(int idProgramming, int idActivityType)
        {
            return Ok(await EnrolledProgramStudent.GetByEnrolled(idProgramming, idActivityType));
        }
        
        /// <summary>
        /// Retorna a los estudiantes matriculados en la actividad "Taller".
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get(string term)
        {
            return Ok(await EnrolledProgramStudent.GetByTerm(term));
        }

        /// <summary>
        /// Se registra como seleccionados a los estudiantes de un programa.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(SelectedStudent model)
        {
            return Ok(await EnrolledProgramActivity.UpdateStudentStatus(model));
        }

        /// <summary>
        /// Retorna a los estudiantes aprobados en la actividad taller.
        /// </summary>
        /// <param name="idProgrammingActivity">id de programación</param>
        /// <param name="term">periodo académico</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetStudent(int idProgrammingActivity, string term)
        {
            return Ok(await EnrolledProgramActivity.GetStudents(idProgrammingActivity, term));
        }
    }
}