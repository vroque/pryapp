﻿using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ExtracurricularActivities;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/ScheduleTypes/

    [HasAuth("AuthBackoffice")]
    public class ScheduleTypesController : ApiController
    {
        /// <summary>
        /// Obtiene los tipos de horarios (Descriptivo y No descriptivo).
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await ScheduleType.GetAll());
        }
    }
}