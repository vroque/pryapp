﻿using System;
using System.Threading.Tasks;
using System.Web;
using ACTools.Configuration;
using System.Web.Http;
using ACBusiness.ExtracurricularActivities;
using ACClient.Helpers;
using ACClient.Helpers.ActionFilters;
using _vuc = ACTools.Constants.ExtracurricularActivitiesConstants;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/Axis/

    public class AxisController : ApiController
    {
        private readonly string _axiFolder = "~/Content/static/images/vuc/axis/";

        /// <summary>
        /// Obtiene todos los los ejes activos con sus actividades.
        /// </summary>
        /// <returns></returns>
        [HasAuth("Auth", "AuthBackoffice")]
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Axi.GetAll());
        }

        /// <summary>
        /// Obtiene un eje por su ID.
        /// </summary>
        /// <param name="id">Id del eje</param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> Get(int? id)
        {
            if (id == null) return BadRequest();
            return Ok(await Axi.GetById((int)id));
        }

        /// <summary>
        /// Obtiene un eje por su nombre.
        /// </summary>
        /// <param name="name">Nombre del eje</param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> GetByName(string name)
        {
            if (string.IsNullOrEmpty(name)) return BadRequest();
            var data = await Axi.GetByName(name);
            return Ok(data);
        }

        /// <summary>
        /// Registra un nuevo eje.
        /// </summary>
        /// <param name="model">Eje</param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        [HttpPost]
        public async Task<IHttpActionResult> Post(Axi model)
        {
            if (model == null) return BadRequest();

            if (!string.IsNullOrEmpty(model.ImageEncode))
            {
                model.ImagePath = ImageBase64Upload.Save(AppSettings.images[_vuc.SRC_VUC_IMAGE_AXI], 
                    model.ImageEncode);
            }
            return Created(string.Empty, await Axi.Create(model));
        }

        /// <summary>
        /// Actualiza un eje.
        /// </summary>
        /// <param name="model">Eje</param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        [HttpPut]
        public async Task<IHttpActionResult> Put(Axi model)
        {
            if (model == null) return BadRequest();

            if (!string.IsNullOrEmpty(model.ImageEncode))
            {

                model.ImagePath = ImageBase64Upload.Save(AppSettings.images[_vuc.SRC_VUC_IMAGE_AXI], 
                    model.ImageEncode);
            }

            return Ok(await Axi.Update(model));
        }

        /// <summary>
        /// Elimina un eje (cambia su estado active a 0).
        /// </summary>
        /// <param name="id">Id del eje</param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int? id)
        {
            if (id == null) return BadRequest();
            return Ok(await Axi.Delete((int)id));
        }
    }
}
