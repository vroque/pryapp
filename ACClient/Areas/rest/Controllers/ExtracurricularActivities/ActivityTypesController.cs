﻿using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ExtracurricularActivities;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/ActivityTypes/

    [HasAuth("AuthBackoffice")]
    public class ActivityTypesController : ApiController
    {
        /// <summary>
        /// Retorna los tipos de actividades.
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await ActivityType.GetAll());
        }
    }
}