﻿using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ExtracurricularActivities;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/ProgrammingActivity/

    [HasAuth("AuthBackoffice")]
    public class ProgrammingActivityController : ApiController
    {
        /// <summary>
        /// Obtiene las programaciones de actividades en estado activo y sus tipos de planes correspondientes.
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> GetAll()
        {
            return Ok(await ProgrammingActivity.GetAllAsync());
        }

        /// <summary>
        /// Obtiene una programación de actividad por su ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get(int? id)
        {
            if (id == null) return BadRequest();
            return Ok(await ProgrammingActivity.GetById((int)id));
        }

        /// <summary>
        /// Registra una nueva programación.
        /// </summary>
        /// <param name="model">Modelo de la programación</param>
        /// <returns></returns>
        [HttpPost, ActionName("CreateActivityProgramming")]
        public async Task<IHttpActionResult> Post(ProgrammingActivity model)
        {
            if (model == null) return BadRequest();
            return Created(string.Empty, await ProgrammingActivity.Create(model));
        }

        /// <summary>
        /// Actualiza una actividad.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IHttpActionResult> Put(ProgrammingActivity model)
        {
            if (model == null) return BadRequest();
            return Ok(await ProgrammingActivity.Update(model));
        }

        /// <summary>
        /// Elimina una programación.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int? id)
        {
            if (id == null) return BadRequest();
            return Ok(await ProgrammingActivity.Delete((int)id));
        }

        /// <summary>
        /// Retorna las programaciones en base a un filtro.
        /// </summary>
        /// <param name="idActivityType">Id del tipo de actividad</param>
        /// <param name="idModality">Modalidad</param>
        /// <param name="idCampus">Id del campus</param>
        /// <param name="shortName">Nombre corto</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetProgrammingActivityBy(int idActivityType, string idModality, 
            string idCampus, string shortName)
        {
            return Ok(await ProgrammingActivity.GetProgrammingActivitiesBy(idActivityType, idModality, idCampus, 
                        shortName));
        }

        /// <summary>
        /// Retorna las programaciones en base a un filtro.
        /// </summary>
        /// <param name="idActivityType">Id del tipo de actividad</param>
        /// <param name="idModality">Id de modalidad</param>
        /// <param name="idCampus">Id de campus</param>
        /// <param name="term">Periodo académico</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetProgrammingActivitiesFilterBy(int idActivityType, string idModality, 
            string idCampus, string term)
        {
            return Ok(await ProgrammingActivity.GetProgrammingActivitiesFilterBy(idActivityType, idModality, idCampus, 
                term));
        }

        /// <summary>
        /// Obtiene todas las programaciones en un determinado periodo académico.
        /// </summary>
        /// <param name="term">Periodo academico</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetProgrammingActivityByTerm(string term)
        {
            return Ok(await ProgrammingActivity.GetProgrammingActivityByTerm(term));
        }

        /// <summary>
        /// Obtiene las programaciones de actividades activas de tipo taller.
        /// </summary>
        /// <param name="term">Periodo academico</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetProgrammingWorkshopActivity(string term)
        {
            return Ok(await ProgrammingActivity.GetProgrammingWorkshopActivity(term));
        }

        /// <summary>
        /// Cambiar el estado de replica de un conjunto de programaciones de actividad tipo taller.
        /// </summary>
        [HttpPost, ActionName("ReplicateActivitySchedules")]
        public async Task<IHttpActionResult> ReplicateActivitySchedules()
        {
            return Ok(await ProgrammingActivity.ReplicateActivitySchedules());
        }
    }
}