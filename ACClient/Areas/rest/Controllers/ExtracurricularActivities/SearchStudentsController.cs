﻿using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ExtracurricularActivities;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/SearchStudents/

    [HasAuth("AuthBackoffice")]
    public class SearchStudentsController: ApiController
    {
        /// <summary>
        /// Obtiene a estudiantes con información de reconocimientos.
        /// </summary>
        /// <param name="dnis">Arreglo de DNI de estudiantes separados por un espacio</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(string[] dnis)
        {
            if (dnis.Length < 1) return BadRequest();
            return Ok(await StudentSearch.GetStudentsByDni(dnis));
        }
    }
}