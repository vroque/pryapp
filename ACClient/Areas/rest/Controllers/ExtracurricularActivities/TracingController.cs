﻿using ACAccess.Authorization;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACBusiness.ExtracurricularActivities;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/Tracing/

    [HasAuth("AuthBackoffice")]
    public class TracingController : ApiController
    {
        private readonly IHttpSessionState _session = 
                                            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Obtiene la información para generar el reporte de un programa.
        /// </summary>
        /// <param name="id">Id de la programación de la actividad</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get(int id)
        {
            return Ok(await Tracing.GetReport(id));
        }

        /// <summary>
        /// Genera el request y PDF para el certificado de superación de una actividad extracurricular.
        /// </summary>
        /// <param name="pidm">Pidm del estudiante</param>
        /// <param name="idProgrammingActivity">Id de la programación de la actividad</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> GetCertificate(decimal pidm, int idProgrammingActivity)
        {
            var authBackoffice = (ACAuth)_session["AuthBackoffice"];

            return Ok(await Tracing.GetCertificatePath(pidm, authBackoffice.id, idProgrammingActivity));
        }

        /// <summary>
        /// Registra un seguimiento para un programa.
        /// </summary>
        /// <param name="model">Modelo del seguimiento</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(List<Tracing> model)
        {
            if (model.Count == 0) return BadRequest();
            return Ok(await Tracing.Insert(model));
        }
    }
}
