﻿using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ExtracurricularActivities;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    [HasAuth("AuthBackoffice")]
    public class PeriodsController : ApiController
    {
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Term.GetTerms());
        }
    }
}
