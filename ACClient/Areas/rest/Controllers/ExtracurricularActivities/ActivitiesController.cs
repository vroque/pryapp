﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACTools.Configuration;
using ACAccess.Authorization;
using ACBusiness.ExtracurricularActivities;
using ACClient.Helpers;
using ACClient.Helpers.ActionFilters;
using _vuc = ACTools.Constants.ExtracurricularActivitiesConstants;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/Activities/

    public class ActivitiesController : ApiController
    {
        private readonly IHttpSessionState _session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Retorna las actividades activas con sus respectivos tipos de convalidaciones.
        /// </summary>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Activity.GetAll());
        }

        /// <summary>
        /// Retorna una actividad con sus tipos de convalidaciones.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> Get(int? id)
        {
            if (id == null) return BadRequest();

            return Ok(await Activity.GetById((int)id));
        }

        /// <summary>
        /// Obtiene las programaciones de una actividad (usado en la vista de estudiante).
        /// </summary>
        /// <param name="idActivity">Id de la actividad</param>
        /// <returns></returns>
        [HasAuth("Auth", "AuthBackoffice")]
        public async Task<IHttpActionResult> GetProgrammingActivities(int? idActivity)
        {
            if (idActivity == null) return BadRequest();

            var auth = (ACAuth)_session["Auth"];

            return Ok(await Activity.GetProgrammingActivities(auth.profile, (int)idActivity));
        }

        /// <summary>
        /// Obtiene una actividad por su nombre.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> GetByName(string name)
        {
            if (string.IsNullOrEmpty(name)) return BadRequest();
            return Ok(await Activity.GetByName(name));
        }

        /// <summary>
        /// Obtiene una actividad por su nombre corto.
        /// </summary>
        /// <param name="shortname"></param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> GetByShortName(string shortname)
        {
            if (string.IsNullOrEmpty(shortname)) return BadRequest();
            return Ok(await Activity.GetByShortName(shortname));
        }

        /// <summary>
        /// Obtiene las actividades por su tipo de actividad.
        /// </summary>
        /// <param name="idActivityType">Id del tipo de actividad</param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        public IHttpActionResult GetByActivityType(int? idActivityType)
        {
            if (idActivityType == null) return BadRequest();
            return Ok(Activity.GetByActivityType((int)idActivityType));
        }

        /// <summary>
        /// Registra una actividad.
        /// </summary>
        /// <param name="model">modelo de la Actividad</param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        [HttpPost, ActionName("CreateActivity")]
        public async Task<IHttpActionResult> CreateActivity(Activity model)
        {
            if (model == null) return BadRequest();

            if (!string.IsNullOrEmpty(model.ImageEncode))
            {
                model.ImagePath = ImageBase64Upload.Save(AppSettings.images[_vuc.SRC_VUC_IMAGE_ACTIVITY],
                    model.ImageEncode);
            }

            return Created(string.Empty, await Activity.Create(model));
        }

        /// <summary>
        /// Actualiza una actividad.
        /// </summary>
        /// <param name="model">modelo de la Actividad</param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        [HttpPut]
        public async Task<IHttpActionResult> Put(Activity model)
        {
            if (model == null) return BadRequest();

            if (!string.IsNullOrEmpty(model.ImageEncode))
            {
                model.ImagePath = ImageBase64Upload.Save(AppSettings.images[_vuc.SRC_VUC_IMAGE_ACTIVITY],
                    model.ImageEncode);
            }

            return Ok(await Activity.Update(model));
        }

        /// <summary>
        /// Cambia la visibilidad de una actividad.
        /// </summary>
        /// <param name="id">Id de la actividad</param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        [HttpPost, ActionName("Desactivate")]
        public async Task<IHttpActionResult> Desactivate(int? id)
        {
            if (id == null) return BadRequest();
            return Ok(await Activity.Desactivate((int)id));
        }

        /// <summary>
        /// Elimina una actividad (eliminado lógico).
        /// </summary>
        /// <param name="id">Id de la actividad</param>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int? id)
        {
            if (id == null) return BadRequest();
            return Ok(await Activity.Delete((int)id));
        }

        /// <summary>
        /// Realiza el proceso de réplica de los talleres pendientes.
        /// Basicamente actualiza su campo IsReplicated a 1.
        /// </summary>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        [HttpPost, ActionName("ProcessIsReplicated")]
        public async Task<IHttpActionResult> ProcessIsReplicated()
        {
            return Ok(await Activity.ProcessIsReplicated());
        }

        /// <summary>
        /// Obtiene los talleres en estado activo.
        /// </summary>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> GetWorkshopActivities()
        {
            return Ok(await Activity.GetWorkshopActivities());
        }
    }
}