﻿using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ExtracurricularActivities;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/SearchTeacher/

    [HasAuth("AuthBackoffice")]
    public class SearchTeacherController : ApiController
    {
        /// <summary>
        /// Busca a un docente por:
        /// </summary>
        /// <param name="documentNumber">DNI del docente</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get(string documentNumber)
        {
            return Ok(await TeacherSearch.GetByDni(documentNumber));
        }
    }
}