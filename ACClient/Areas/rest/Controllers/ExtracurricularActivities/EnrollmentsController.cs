﻿using ACAccess.Authorization;
using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACBusiness.ExtracurricularActivities;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/Enrollments/

    [HasAuth("Auth")]
    public class EnrollmentsController : ApiController
    {
        private readonly IHttpSessionState _session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Obtiene la información de las matrículas de un estudiante en actividades extracurriculares en el perido
        /// actual.
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get()
        {
            var auth = (ACAuth)_session["Auth"];

            return Ok(await Enrollment.GetEnrollments(auth.profile));
        }

        /// <summary>
        /// Realiza el registro de la actividad en las tablas de BDINTBANNER asignando el concepto que esta cancelado
        /// a una matricula del estudiante.
        /// </summary>
        /// <param name="idProgrammingActivity">Id de la programación de la actividad</param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult Put([FromBody]int idProgrammingActivity)
        {
            var auth = (ACAuth)_session["Auth"];

            return Ok(EnrolledProgramActivity.EnrollStudent(auth.profile, idProgrammingActivity));
        }
    }
}
