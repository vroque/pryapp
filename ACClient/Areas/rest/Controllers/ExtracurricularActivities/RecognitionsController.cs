﻿using ACClient.Helpers;
using ACClient.Helpers.ActionFilters;
using System;
using ACTools.Configuration;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using ACBusiness.ExtracurricularActivities;
using _vuc = ACTools.Constants.ExtracurricularActivitiesConstants;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/Recognitions/

    [HasAuth("AuthBackoffice")]
    public class RecognitionsController : ApiController
    {
        // Ruta donde se almacenará las evidencias
        private readonly string _activitiesFolder = "~/Content/static/files/vuc/recognition/";

        /// <summary>
        /// Registra un reconocimiento.
        /// </summary>
        /// <param name="model">Modelo de reconocimiento</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(Recognition model)
        {
            if (model == null) return BadRequest();
            
            if (string.IsNullOrEmpty(model.PathCode)) return BadRequest();

            // Obtener la ruta de la aplicación
            model.PathEvidency = PathFile.Save(AppSettings.files[_vuc.SRC_VUC_FILE], model.PathCode);
            return Created(string.Empty, await Recognition.Create(model));
        }       
    }
}