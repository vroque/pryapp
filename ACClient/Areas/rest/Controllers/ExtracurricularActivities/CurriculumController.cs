﻿using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ExtracurricularActivities;

namespace ACClient.Areas.rest.Controllers.ExtracurricularActivities
{
    // API: api/ExtracurricularActivities/Curriculum

    [HasAuth("AuthBackoffice")]
    public class CurriculumController : ApiController
    {
        /// <summary>
        /// Obtiene los planes de estudio.
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Curriculum.GetAll());
        }
    }
}