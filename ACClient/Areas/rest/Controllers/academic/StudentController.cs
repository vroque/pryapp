﻿using System.Net;
using System.Threading.Tasks;
using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACPermanence.Contexts.BDUCCI;
using System.Linq;
using System.Collections.Generic;

namespace ACClient.Areas.rest.Controllers.academic
{
    public class StudentController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        private readonly DBOContext _dboContext = new DBOContext();

        /// <summary>
        /// Obtiene el alumno de la sesion
        /// </summary>
        /// <returns></returns>
        [HasAuth("Auth")]
        public IHttpActionResult Get()
        {
            //obtener los datos del alumno logeado
            ACAuth auth_obj = (ACAuth) _httpSessionState["Auth"];
            var student = new Student(auth_obj.id);
            student.profile = auth_obj.profile;
            return Ok(student);
        }

        /// <summary>
        /// Obtiene el alumno segun el ID
        /// </summary>
        /// <returns></returns>
        //[HasAuth("AuthBackoffice")]
        //[HasModule("bo_herramientas", "bo_cau-personal", "bo_buw-personal", "bo_vida-academica", "bo_convenios",
        //    "bo_beneficios", "bo_learning_assessment", "bo_cic-persona")]
        public IHttpActionResult Get(string id, [FromUri] string by)
        {
            var student = new Student();
            if (by == null || by == "student_id")
            {
                if (student.validateDNI(id))
                {
                    student = new Student(id);
                    if (student.profiles.Count == 0)
                    {
                        return NotFound();
                    }

                    var prg = Student.GetLastMovimientoEstudiante(Student.GetMovimientosEstudiante(student.person_id));
                    student.profile = student.profiles.FirstOrDefault(w => w.program.id == prg.CODIGO_PROGRAMA);
                }
                else
                {
                    return NotFound();
                }
            }
            else if (by == "person_id")
            {
                int person_id;
                bool t = int.TryParse(id, out person_id);
                if (t)
                {
                    student = new Student(person_id);
                }
                else
                    return InternalServerError();
            }
            else
                return NotFound();

            // verificar si tiene perfiles de estudiante
            if (student.profiles.Count == 0)
                return NotFound();

            return Ok(student);
        }

        // POST: api/Academic/Student
        [HasAuth("AuthBackoffice", "Auth")]
        public async Task<IHttpActionResult> Post(Data data)
        {
            var type = data.Type;

            switch (type)
            {
                case "searchPronabec":
                    return Ok(await new ScholarshipStudent().GetEntrantsAsync(data.Students));
                case "studentProfiles":
                    List<Student> foundStudents;
                    var student = new Student();
                    foundStudents = student.searchStudents(data.Students);
                    if (foundStudents.Count() > 0)
                    {
                        return Ok(foundStudents);
                    }
                    else
                    {
                        return NotFound();
                    }

                default:
                    return Ok(HttpStatusCode.NoContent);
            }
        }

        public class Data
        {
            public string Type { get; set; }
            public string[] Students { get; set; }
        }
    }
}