﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    /// <summary>
    /// 
    /// </summary>
    [HasAuth("Auth", "AuthBackoffice")]
    [HasModule("fd_academic", "bo_cac-personal", "bo_vida-academica")]
    public class DiplomaedController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public IHttpActionResult Get([FromUri] CurriculumData data)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            //obtener los datos del alumno logeado
            //Curriculum curriculum = new Curriculum().get(data.department, data.program, data.campus, data.term);
            List<Diplomaed> diplomaed = new Diplomaed().get(data.department, data.program, data.campus,data.term);
            return Ok(diplomaed);
        }
    }
    public class DiplomaedData
    {
        public string department { get; set; }
        public string program { get; set; }
        public string campus { get; set; }
        public string term { get; set; }
    }
}
    