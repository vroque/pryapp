﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("AuthBackoffice")]
    public class ScholarshipsController : ApiController
    {
        // GET: api/Academic/Scholarships
        public IHttpActionResult Get()
        {
            return Ok(HttpStatusCode.NoContent);
        }

        // GET: api/Academic/Scholarships/:id
        public async Task<IHttpActionResult> Get(string id)
        {
            if (id.ToLower() != "pronabec") return Ok(HttpStatusCode.NotFound);
            
            var scholarships = new Scholarships();
            return Ok(await scholarships.GetPronabecScholarshipsAsync());

        }
    }
}