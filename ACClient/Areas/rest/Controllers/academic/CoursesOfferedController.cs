﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class CoursesOfferedController : ApiController
    {
        private IHttpSessionState session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        // GET: rest/coursesoffered
        public IHttpActionResult Get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            List<CoursesOffered> list = new List<CoursesOffered>();

            ACAuth AuthBackoffice = (ACAuth)session["AuthBackoffice"];

            if (AuthBackoffice != null)
            {
                list = new CoursesOffered().getCourses(auth_obj.profile);
                //list = CoursesOffered.getCourses(auth_obj.profile);
                return Ok(list);
            }
            list = new CoursesOffered().getCoursesByRestriction(auth_obj.profile);

            return Ok(list);
        }
    }
}