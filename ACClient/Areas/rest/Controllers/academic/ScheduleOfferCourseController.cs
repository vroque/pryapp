﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class ScheduleOfferCourseController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        // GET: rest/scheduleoffercourse
        public IHttpActionResult Get([FromUri] string nrcpadre, [FromUri] string term, [FromUri] string subject, [FromUri] string course, [FromUri] string codliga)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            List<Schedule> list = new List<Schedule>();

            ACAuth AuthBackoffice = (ACAuth)session["AuthBackoffice"];

            if (AuthBackoffice != null)
            {
                list = new Schedule().getScheduleByNrcTerm(nrcpadre, term, subject, course);
                return Ok(list);
            }
            list = new Schedule().getScheduleByNrcTermByRestriction(nrcpadre, term, subject, course);

            return Ok(list);
        }
    }
}