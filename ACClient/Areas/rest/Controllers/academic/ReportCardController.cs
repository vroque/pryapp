﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    ///<url>/api/academic/ReportCard</url>
    /// <summary>
    /// 
    /// </summary>
    [HasAuth("Auth", "AuthChild")]
    [HasModule("fd_estudiante", "fd_egresado")]
    public class ReportCardController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Obtiene el alumno de la sesion
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get(string id)
        {
            //obtener los datos del alumno logeado
            ACAuth auth_obj = (ACAuth)session["AuthChild"];
            if(auth_obj == null) auth_obj = (ACAuth)session["Auth"];
            Student student = new Student(auth_obj.id);
            student.profile = auth_obj.profile;

            Course course = new Course();            
            return Ok(course.reportCardtByTerm(student.profile, id)); // FIXME!!!
        }
    }
}
