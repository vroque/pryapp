﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("Auth", "AuthChild")]
    public class AbsenceController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        // GET: rest/Absence
        public IHttpActionResult Get()
        {
            ACAuth auth_obj = (ACAuth)session["AuthChild"];

            if (auth_obj == null)
                auth_obj = (ACAuth)session["Auth"];

            List<Absence> absence = new Absence().getByStudent(auth_obj.profile);
            return Ok(absence);
        }
    }
}