﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("Auth", "AuthBackoffice", "AuthChild")]
    public class EnrollmentTermController : ApiController
    {

        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Obtiene las lista de todos los periodos matriculados
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get([FromUri] int type )
        {
            //obtener los datos del alumno logeado
            ACAuth auth_obj = (ACAuth)session["AuthChild"];
            if(auth_obj == null) auth_obj = (ACAuth)session["Auth"];
            Student student = new Student();
            if (auth_obj != null) {
                student = new Student(auth_obj.id);
                student.profile = auth_obj.profile;
            }
            List<string> terms = new List<string>();
            switch (type)
            {
                case 1:
                    terms = new Enrollment().getAllTermsEnrollment(student.profile);
                    break;
                case 2:
                    terms = new Enrollment().getOldsTermsEnrollment(student.profile);
                    break;
                case 3:
                    terms = new Enrollment().getAllEnrollmentPeriods();
                    break;
                default:
                    throw new System.Exception("No envio campo tipo.");
                    
            }
            return Ok(terms);
        }

        [HasModule("bo_learning_assessment")]
        public IHttpActionResult Post([FromBody] AcademicProfile profile)
        {
            return Ok(new Enrollment().getAllTermsEnrollment(profile));
        }


    }
}
