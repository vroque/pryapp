﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    /// <summary>
    /// GET Promedio global acumulado de un alumno
    /// </summary>
    [HasSession("Auth")]
    public class GPAController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <ulr>/api/academic/profile</ulr>
        /// <summary>
        ///  Obtiene los perfiles academico de un alumno segun su codigo
        ///  con el cual esta logeado
        /// </summary>
        /// <returns> Lista de perfiles</returns>
        public IHttpActionResult Get()
        {
            //obtener los datos del alumno logeado
            ACAuth auth_obj = (ACAuth)session["Auth"];
            float gpa = auth_obj.profile.getGPA();

            return Ok(new { gpa = gpa });
        }


    }
}
