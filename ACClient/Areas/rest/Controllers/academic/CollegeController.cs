﻿using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Web.Http;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class CollegeController : ApiController
    {
        /// <ulr>/api/academic/allprofiles</ulr>
        /// <summary>
        ///  Obtiene los colleges(facultades) y sus programs (carreras)
        ///  de pregrado
        /// </summary>
        /// <returns> Lista de colleges </returns>
        public IHttpActionResult Get()
        {
            List<College> list = College.listCollegeByLvlWithPrograms(_acad.LEVL_PREGRADO);
            return Ok(list);
        }

    }
}