﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    /// <summary>
    /// Cursos matriculados
    /// </summary>
    [HasAuth("Auth", "AuthBackoffice")]
    public class enrollmentCoursesController : ApiController
    {

        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Obtiene las lista de todos los cursos matriculados
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get([FromUri] AcademicProfile profile)
        {
            //obtener los datos del alumno logeado
            ACAuth auth_obj = (ACAuth)session["Auth"];
            List<Course> terms = new List<Course>();
            if (profile == null)
            {
                terms = new Course().listByCurrentPeriod(auth_obj.profile);
            }
            else
            {
                terms = new Course().listByCurrentPeriod(profile);
            }
            return Ok(terms);
        }
        public IHttpActionResult Post(AcademicProfile profile)
        {
            //obtener los datos del alumno logeado
            List<Course> terms = new Course().listByCurrentPeriod(profile);
            return Ok(terms);
        }
    }
}