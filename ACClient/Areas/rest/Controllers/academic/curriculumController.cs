﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    /// <summary>
    /// 
    /// </summary>
    [HasAuth("Auth", "AuthBackoffice")]
    [HasModule("fd_academic", "bo_cac-personal", "bo_vida-academica")]
    public class curriculumController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public IHttpActionResult Get([FromUri]string person_id, [FromUri]string program)
        {
            decimal pidm;
            decimal.TryParse(person_id,out pidm);
           
            Curriculum curriculum = new Curriculum();

            Student st = new Student(pidm);
            st.syncProfile(program);
            curriculum = curriculum.getByStudent(st.profile);
          
            
            return Ok(curriculum);
        }

        public IHttpActionResult Get([FromUri] CurriculumData data)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            //obtener los datos del alumno logeado
            Curriculum curriculum = new Curriculum();
            if(auth_obj != null)
            {
                curriculum = curriculum.getByStudent(auth_obj.profile);
            } else
            {
                curriculum = curriculum.get(data.department, data.program, data.campus, data.term);
            }
            return Ok(curriculum);
        }
    }
    public class CurriculumData
    {
        public string department { get; set; }
        public string program { get; set; }
        public string campus { get; set; }
        public string term { get; set; }
    }
    public class PersonData
    {
        public decimal person_id{ get; set; }
        public string program { get; set; }
    }
}
    