﻿using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("AuthBackoffice")]
    public class GraduationPeriodsController : ApiController
    {
        // GET: api/academic/GraduationPeriods
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await GraduationPeriods.GetAllAsync());
        }
    }
}