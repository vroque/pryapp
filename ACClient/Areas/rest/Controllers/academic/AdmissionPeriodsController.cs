﻿using System.Collections.Generic;
using System.Web.Http;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_cau-personal")]
    public class AdmissionPeriodsController : ApiController
    {
        // GET: api/academic/AdmissionPeriods
        public IEnumerable<AdmissionPeriods> Get()
        {
            return AdmissionPeriods.getAll();
        }
    }
}