﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{

    /// <summary>
    /// Api para el perfil academico de un alumno
    /// </summary>
    [HasSession("Auth")]
    public class ProfileController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <ulr>/api/academic/profile</ulr>
        /// <summary>
        ///  Obtiene los perfiles academico de un alumno segun su codigo
        ///  con el cual esta logeado
        /// </summary>
        /// <returns> Lista de perfiles</returns>
        public IHttpActionResult Get()
        {
            //obtener los datos del alumno logeado
            ACAuth auth_obj = (ACAuth)session["Auth"];
            Student student = new Student(auth_obj.id);
            return Ok(student.profiles);
        }

        /// <ulr>/api/academic/profile/123245678</ulr>
        /// <summary>
        ///  Obtiene los perfiles academico de un alumno segun su codigo
        ///  con el cual esta logeado
        /// </summary>
        /// <returns> Lista de perfiles</returns>
        public IHttpActionResult Get(string id)
        {
            //obtener los datos del alumno logeado
            Student student = new Student(id);
            return Ok(student);
        }

        public IHttpActionResult Post(Data data)
        {
            ACAuth auth = (ACAuth)session["Auth"];
            auth.syncProfile(data.div, data.department, data.program);
            auth.module_list = new Module().getByStudent(auth.profile);
            session["Auth"] = auth;
            return Ok(new { status = true });
        }

        public class Data
        {
            public string div { get; set; }
            public string department { get; set; }
            public string program { get; set; }
        }
    }
}
