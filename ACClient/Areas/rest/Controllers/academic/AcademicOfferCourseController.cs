﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class AcademicOfferCourseController : ApiController
    {
        private IHttpSessionState session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        // GET: rest/academicoffercourse
        public IHttpActionResult Get([FromUri] string term, [FromUri] string subject, [FromUri] string course)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            List<AcademicOfferCourses> list = new List<AcademicOfferCourses>();

            ACAuth AuthBackoffice = (ACAuth)session["AuthBackoffice"];

            if (AuthBackoffice != null )
            {
                list = new AcademicOfferCourses().getOfferCourses(auth_obj.profile, term, subject, course);
                return Ok(list);
            }
            list = new AcademicOfferCourses().getOfferCoursesByRestriction(auth_obj.profile, term, subject, course);

            return Ok(list);
        }
    }
}