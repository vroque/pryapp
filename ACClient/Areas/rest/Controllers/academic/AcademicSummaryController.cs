﻿using ACAccess.Authorization;
using ACClient.Helpers.ActionFilters;
using ACBusiness.Academic;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    public class AcademicSummaryController: ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        [HasAuth("Auth")]
        public IHttpActionResult Get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            AcademicSummary req = new AcademicSummary();
            return Ok(req.get(auth_obj.profile));
        }

        /// <summary>
        /// Llamada a los datos de un estudiante mediante un profile escogido
        /// </summary>
        /// <param profile="actionContext">profile del estudiante</param>
        [HasAuth("Auth","AuthBackoffice")]
        public IHttpActionResult Post( AcademicProfile profile )
        {
            AcademicSummary req = new AcademicSummary();
            return Ok(req.get(profile));
        }
    }
}