﻿using System.Collections.Generic;
using System.Web.Http;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_cau-personal")]
    public class EntrantController : ApiController
    {

        // GET: api/academic/Entrant/<schoolId>/<entryPeriod>
        public IEnumerable<Entrant> Get(string schoolId, string entryPeriod)
        {
            return Entrant.GetBySchoolAndEntryPeriod(schoolId, entryPeriod);
        }
        
        // POST: api/academic/Entrant
        public IEnumerable<Entrant> Post(string[] applicants)
        {
            return Entrant.GetByStudentId(applicants);
        }

    }
}