﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("Auth", "AuthBackoffice")]
    //[HasModule("fd_estudiante", "fd_egresado")]
    public class RemainingCoursesController : ApiController
    {
        private IHttpSessionState session =
          SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        
        public IHttpActionResult Get()
        {
            //obtener los datos del alumno logeado
            ACAuth auth_obj = (ACAuth)session["Auth"];
            List<Course> remaining_courses = new Course().ListRemaining(auth_obj.profile, true);
            

            return Ok(remaining_courses);
        }
        public IHttpActionResult Get([FromUri] decimal person_id, [FromUri] string program)
        {
            //obtener los datos del alumno logeado
            Student st = new Student(person_id);
            st.syncProfile(_acad.DEFAULT_DIV, _acad.DEFAULT_LEVL, program);
            List<Course> remaining_courses = new Course().ListRemaining(st.profile, true);


            return Ok(remaining_courses);
        }
    }
}