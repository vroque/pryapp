﻿using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.academic
{

    [HasAuth("AuthBackoffice")]
    [HasAuth("bo_buw-personal")]
    public class FreshmanController : ApiController
    {
        public IHttpActionResult Get([FromUri] Data data)
        {
            return Ok(Freshman.listByTerm(data.term, data.program, data.campus));
        }
    }
    public class Data
    {
        public string term { get; set; }
        public string program { get; set; }
        public string campus { get; set; }
    }
}