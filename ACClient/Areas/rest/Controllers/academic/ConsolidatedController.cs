﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    public class ConsolidatedController : ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// devolvemos el consolidado de un estudiante por carrera en un determinado período
        /// </summary>
        /// <param pidm="id"></param>
        /// <param term="periodo"></param>
        /// <param program="program"></param>
        /// <returns></returns>
        [HasAuth("Auth", "AuthBackoffice")]
        public IHttpActionResult Get([FromUri] string term)
        {
            if (Int32.Parse(term) >= 201710)
            {
                //obtener los datos del alumno logeado
                ACAuth auth_obj = (ACAuth)session["Auth"];
                Student student = new Student(auth_obj.id);
                student.profile = auth_obj.profile;

                consolidateData data = new consolidateData();
                data.courses = new Course().consolidatedByTerm(student.profile, term);
                data.closeEnrollment = new CloseEnrollment().closeEnrollmentByStudent(student.person_id, term);

                return Ok(data);
            }
            else
            {
                return null;
            }            
        }
        public class consolidateData
        {
            #region Properties
            public List<Course> courses { get; set; }
            public CloseEnrollment closeEnrollment { get; set; }
            #endregion Properties
        }
    }
}