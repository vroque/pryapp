﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    public class ScheduleController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
       
        public IHttpActionResult Get([FromUri] string term)
        {
            var authObj = (ACAuth)_httpSessionState["AuthChild"] ?? (ACAuth)_httpSessionState["Auth"];

            List<Schedule> scheduleList = new Schedule().GetByStudentAndTerm(authObj.profile, term);

            scheduleTerm scheduleTerm = new scheduleTerm();
            scheduleTerm.term = term;

            if ( scheduleList == null)
            {
                scheduleTerm.schedule = new List<Schedule>();
                Ok(scheduleTerm);
            }
            scheduleTerm.schedule = scheduleList;
            return Ok(scheduleTerm);
        }

        class scheduleTerm
        {
            public string term { get; set; }
            public List<Schedule> schedule { get; set; }
        }
    }
}
