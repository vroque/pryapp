﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class AcademicOfferCourseChildrenController : ApiController
    {
        private IHttpSessionState session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        // GET: rest/academicoffercoursechildren
        public IHttpActionResult Get([FromUri] string nrcpadre, [FromUri] string term, [FromUri] string subject, [FromUri] string course, [FromUri] string codliga)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            List<AcademicOfferCourses> list = new List<AcademicOfferCourses>();

            ACAuth AuthBackoffice = (ACAuth)session["AuthBackoffice"];

            if (AuthBackoffice != null)
            {
                list = new AcademicOfferCourses().getOfferCoursesChildren(auth_obj.profile, nrcpadre, term, subject, course, codliga);
                return Ok(list);
            }
            list = new AcademicOfferCourses().getOfferCoursesChildrenByRestriction(auth_obj.profile, nrcpadre, term, subject, course, codliga);

            return Ok(list);
        }
    }
}