﻿using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using _acad = ACTools.Constants.AcademicConstants;

namespace ACClient.Areas.rest.Controllers.academic
{
    public class progressController : ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="program"></param>
        /// <returns></returns>
        [HasAuth("Auth", "AuthBackoffice")]
        public IHttpActionResult Get(decimal id, [FromUri] string program)
        {
            //obtener los datos del alumno logeado
            Student student = new Student(id);
            student.syncProfile(_acad.DEFAULT_DIV, _acad.DEFAULT_LEVL, program);
            AcademicProgress progress = new AcademicProgress().get(student.profile);

            return Ok(progress);
        }
    }
}