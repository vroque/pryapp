﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("AuthBackoffice")]
    public class GraduatesController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // GET: api/Academic/Graduates
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Graduated.GetAllGraduatesAsync());
        }

        // GET: api/Academic/Graduates?:term=term
        public async Task<IHttpActionResult> Get(string term)
        {
            return Ok(await Graduated.GetByGraduationTermAsync(term));
        }

        // PUT: api/atentioncenter/Graduates
        [HasModule("bo_cumplimiento_egresados")]
        public async Task<IHttpActionResult> Put(Data data)
        {
            var casAuth = (ACAuth) _httpSessionState["AuthBackoffice"];
            return Ok(await Graduated.UpdateGraduatesAsync(data.Graduates, data.BachelorResolutionDate,
                data.TitleResolutionDate, casAuth.id));
        }

        // POST: api/atentioncenter/Graduates
        public async Task<IHttpActionResult> Post(Data data)
        {
            var type = data.Type;
            string[] students = data.Students?.ToArray();
            ACAuth acAuth = (ACAuth) _httpSessionState["AuthBackoffice"];

            switch (type)
            {
                // Búsqueda por escuela académica y período de graduación UC
                case "search":
                    return Ok(await Graduated.GetBySchoolAndGraduationPeriodAsync(data.SchoolId,
                        data.GraduationPeriod));
                // Búsqueda bacth por código de estudiante egresado
                case "searchBatch":
                    return Ok(await Graduated.GetByStudentIdAsync(students));
                // Búsqueda batch por código de estudiante egresado.
                // Añade todas la carreras (egresadas o no) asociadas al código, a las que haya ingresado
                case "searchAllEntrant":
                    return Ok(await Graduated.GetByStudentIdIncludedAllEntrySchoolAsync(students));
                default:
                    return Ok(Content(HttpStatusCode.NotFound, ""));
            }
        }

        public class Data
        {
            public string Type { get; set; }
            public string SchoolId { get; set; }
            public string GraduationPeriod { get; set; }
            public string[] Students { get; set; }
            public Graduated[] Graduates { get; set; }
            public DateTime? BachelorResolutionDate { get; set; }
            public DateTime? TitleResolutionDate { get; set; }
        }
    }
}