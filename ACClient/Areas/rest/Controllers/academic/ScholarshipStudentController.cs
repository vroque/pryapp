﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("AuthBackoffice")]
    public class ScholarshipStudentController : ApiController
    {
        // GET: api/Academic/ScholarshipStudent
        public IHttpActionResult Get()
        {
            return Ok(HttpStatusCode.NoContent);
        }

        // GET: api/Academic/ScholarshipStudent/:id
        public IHttpActionResult Get(string id)
        {
            return Ok(HttpStatusCode.NotFound);
        }

        // POST: api/Academic/ScholarshipStudent
        public async Task<IHttpActionResult> Post(Data data)
        {
            var type = data.Type;

            switch (type)
            {
                // Búsca estudiantes matriculados en la UC por tipo PRONABEC
                case "searchEnrolledByPronabecType":
                    return Ok(await new ScholarshipStudent().GetEnrolledByPronabecTypeAndEnrolledPerdiodAsync(
                        data.ScholarshipId, data.SchoolId, data.EnrolledPeriod));
                default:
                    return Ok(HttpStatusCode.NoContent);
            }
        }

        public class Data
        {
            public string Type { get; set; }
            public string ScholarshipId { get; set; }
            public string SchoolId { get; set; }
            public string EnrolledPeriod { get; set; }
        }
    }
}