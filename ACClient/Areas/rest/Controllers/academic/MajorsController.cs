using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.academic
{
    [HasAuth("AuthBackoffice")]
    public class MajorsController : ApiController
    {
        // GET: api/Academic/Programs
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Program.GetAllAsync());
        }
    }
}