﻿using ACBusiness.GyT;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.opp
{
    /// <summary>
    /// APi para Practicas pre profesionales
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class searchController: ApiController
    {
        /// <summary>
        /// Busqueda de alumnos que hayan terminado practicas en diversos
        /// tipos de filtros
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public IHttpActionResult get([FromUri] SearchFilter obj)
        {
            PPPractices practices_obj = new PPPractices();
            List<PPPractices> list = new List<PPPractices>();
            switch (obj.type)
            {
                case ("student_names"):
                    list = practices_obj.listByName(obj.name);
                    break;
                case ("student_code"):
                    PPPractices p = practices_obj.get(obj.code);
                    list.Add(p);
                    break;
                case ("date"):
                    list = practices_obj.listByDateRange(obj.date, obj.date);
                    break;
                case ("date_range"):
                    list = practices_obj.listByDateRange(obj.date, obj.date_end);
                    break;
                case ("code_list"):
                    list = practices_obj.listByCodes(obj.code_list);
                    break;
            }
            if (list.Count == 0)
                return NotFound();
            return Ok(list);
        }
        public class SearchFilter
        {
            public string type { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public DateTime date { get; set; }
            public DateTime date_end { get; set; }
            public string program { get; set; }
            public List<string> code_list { get; set; }
        }
    }
}