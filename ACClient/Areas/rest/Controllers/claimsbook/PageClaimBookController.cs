﻿using System.Threading.Tasks;
using System.Web.Http;
using ACClient.Helpers.ActionFilters;
using _acad = ACTools.Constants.AcademicConstants;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web;
using ACAccess.Authorization;
using _page_states = ACTools.Constants.ClaimsBookConstants;
using System;
using ACBusiness.ComplaintsBook;

namespace ACClient.Areas.rest.Controllers.claimsbook
{
    [HasAuth("AuthBackoffice")]
    public class PageClaimBookController : ApiController
    {
        private readonly IHttpSessionState _session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public async Task<IHttpActionResult> Get([FromUri] string type)
        {
            ACAuth authObj = (ACAuth) _session["AuthBackoffice"];
            var claimsBook = new PageClaimBook();
            List<PageClaimBook> list;
            if (type.Trim().ToUpper() == _page_states.LDR_TYPE_USER_AD)
            {
                list = await claimsBook.listPageClaimsBookCAU(_acad.DEFAULT_DIV, authObj.campus.id);
            }
            else if (type.Trim().ToUpper() == _page_states.LDR_TYPE_USER_INVOLVED_AREA)
            {
                list = await claimsBook.listPageClaimsInvolvedArea(_acad.DEFAULT_DIV, authObj.person_id.ToString());
            }
            else if (type.Trim().ToUpper() == _page_states.LDR_TYPE_USER_LEGAL_AREA)
            {
                list = await claimsBook.listPageClaimsLegalArea(_acad.DEFAULT_DIV, authObj.person_id.ToString());
            }
            else
            {
                throw new Exception("Tipo de usuario invalido");
            }

            return Ok(list);
        }

        public async Task<IHttpActionResult> Get(int id)
        {
            ACAuth authObj = (ACAuth) _session["AuthBackoffice"];
            var claimsBook = new PageClaimBook();
            var pageClaimBook = await claimsBook.getPageClaimsBook(id, authObj.person_id.ToString());
            return Ok(pageClaimBook);
        }

        /// <summary>
        /// Retorna lista de reclamos que compartan div, sede, y numeración 
        /// </summary>
        /// <param name="div">Dependencia</param>
        /// <param name="campus">Sede</param>
        /// <param name="numerate">Numero</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get([FromUri] string div, [FromUri] string campus, [FromUri] int numerate)
        {
            ACAuth auth_obj = (ACAuth) _session["AuthBackoffice"];
            div = _acad.DEFAULT_DIV;
            campus = auth_obj.campus.id;

            var claimsBook = new PageClaimBook();
            List<PageClaimBook> list = claimsBook.listPageClaimsBookNumeration(div, campus, numerate);
            return Ok(list);
        }

        [HasModule("bo_claims-book-adm")]
        public async Task<IHttpActionResult> put([FromBody] PageClaimBook page)
        {
            var pageState = page.page_state;
            if (pageState == _page_states.LDR_STATE_PAGE_DISMISSED)
            {
                var savePage = await page.dismissedPage();
                if (savePage)
                {
                    return Ok(new {page_id = page.page_id});
                }

                return InternalServerError();
            }

            return InternalServerError();
        }

        [HasModule("bo_claims-book-adm")]
        public async Task<IHttpActionResult> Post([FromBody] PageClaimBook page)
        {
            ACAuth auth_obj = (ACAuth) _session["AuthBackoffice"];
            page.page_div = _acad.DEFAULT_DIV;
            page.page_campus = auth_obj.campus.id;
            page.page_register_user_pidm = auth_obj.person_id.ToString();
            page.page_origin = _page_states.LDR_TYPE_ORIGIN_PAGE_PHYSICAL;

            var r = await page.savePage();
            if (r)
            {
                return Ok(new {page_id = page.page_id});
            }

            return InternalServerError();
        }

        [HasModule("bo_claims-book-adm")]
        public async Task<IHttpActionResult> options([FromUri] int page_id)
        {
            var page = new PageClaimBook();
            var r = await page.sendEmailCreate(page_id);
            return Ok(r);
        }

        [HasModule("bo_claims-book-adm")]
        public async Task<IHttpActionResult> patch([FromBody] PageClaimBook page)
        {
            var save = await page.updatePersonClaim();
            return Ok(save);
        }
    }
}
