﻿using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ClaimsBook;
using ACAccess.Authorization;
using System.Web.SessionState;
using System.Web;
using _ldr_constants = ACTools.Constants.ClaimsBookConstants;
using System;

namespace ACClient.Areas.rest.Controllers.claimsbook
{
    [HasAuth("AuthBackoffice")]
    public class DerivationAreaController : ApiController
    {
        private readonly IHttpSessionState _session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public async Task<IHttpActionResult> Get(int id)
        {
            ACAuth auth_obj = (ACAuth) _session["AuthBackoffice"];
            var derivation = new DerivationArea();
            DerivationArea derivate;
            derivate = await derivation.getDerivationClaim(id, auth_obj.person_id.ToString());
            return Ok(derivate);
        }

        public async Task<IHttpActionResult> Get([FromUri] string page_id, [FromUri] string type)
        {
            var derivationbook = new DerivationArea();
            if (type == _ldr_constants.LDR_TYPE_USER_INVOLVED_AREA)
            {
                List<DerivationArea> list =
                    await derivationbook.listDerivationInvolvedArea(_ldr_constants.LDR_ID_COMPANY_UCCI,
                        int.Parse(page_id));
                return Ok(list);
            }

            if (type == _ldr_constants.LDR_TYPE_USER_LEGAL_AREA)
            {
                DerivationArea list =
                    await derivationbook.listDerivationLegalArea(_ldr_constants.LDR_ID_COMPANY_UCCI,
                        int.Parse(page_id));
                return Ok(list);
            }

            throw new Exception("Tipo de usuario invalido");
        }

        public async Task<IHttpActionResult> Get([FromUri] int page_id, [FromUri] string funtional_unity)
        {
            var derivationbook = new DerivationArea();
            List<DerivationArea> list = await derivationbook.listDerivationFuntionalUnity(
                _ldr_constants.LDR_ID_COMPANY_UCCI, page_id,
                funtional_unity);
            return Ok(list);
        }

        public async Task<IHttpActionResult> Post([FromBody] DerivationArea derivation)
        {
            ACAuth authObj = (ACAuth) _session["AuthBackoffice"];
            derivation.derivate_person_creator_pidm = authObj.person_id.ToString();
            derivation.derivate_type = _ldr_constants.LDR_TYPE_DERIVATE_INVOLVED_AREA;
            var r = await derivation.saveDerivation();
            if (r)
            {
                return Ok(new {id = derivation.derivate_id});
            }

            return InternalServerError();
        }

        public async Task<IHttpActionResult> Put([FromBody] DerivationArea derivation)
        {
            var actionDerivate = derivation.action_derivate;
            ACAuth authObj = (ACAuth) _session["AuthBackoffice"];
            if (actionDerivate == _ldr_constants.LDR_STATE_DERIVATE_DISMISSED)
            {
                var save = await derivation.disableDerivation();
                if (save)
                {
                    return Ok(new {derivation.derivate_id});
                }

                return InternalServerError();
            }

            if (actionDerivate == _ldr_constants.LDR_STATE_DERIVATE_RESOLVED)
            {
                var save = await derivation.answerDerivation();
                if (save)
                {
                    return Ok(new {derivation.derivate_id});
                }

                return InternalServerError();
            }

            if (actionDerivate == _ldr_constants.LDR_STATE_DERIVATE_FINALIZED)
            {
                var save = await derivation.acceptAnswerDerivation(authObj.person_id.ToString());
                if (save)
                {
                    return Ok(new {derivation.derivate_id});
                }

                return InternalServerError();
            }

            return InternalServerError();
        }
    }
}
