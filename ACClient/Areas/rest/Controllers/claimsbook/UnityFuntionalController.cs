﻿using System.Web.Http;
using System.Threading.Tasks;
using ACBusiness.Adryan;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.claimsbook
{
    public class UnityFuntionalController : ApiController
    {
        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> Get([FromUri] string company)
        {
            return Ok(await FunctionalUnit.GetAllByCompanyIdAsync(company));
        }
    }
}
