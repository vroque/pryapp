﻿using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Adryan;
using ACBusiness.ComplaintsBook;
using _ldr_constants= ACTools.Constants.ClaimsBookConstants;
namespace ACClient.Areas.rest.Controllers.claimsbook
{
    [HasAuth("AuthBackoffice")]
    public class CollaboratorController: ApiController
    {
        
        public async Task<IHttpActionResult> Get([FromUri]string names)
        {
            Employee collaborator = new Employee();
            List<Employee> listcollaborator;
            listcollaborator = await collaborator.listCollaborators(_ldr_constants.LDR_ID_COMPANY_UCCI, names);
            return Ok(listcollaborator);
        }

        public async Task<IHttpActionResult> Get([FromUri]string funtional_unity, [FromUri]string xxx)
        {
            Employee collaborator = new Employee();
            List<Employee> listcollaborator;
            listcollaborator = await collaborator.listCollaboratorsFuntionalUnity(_ldr_constants.LDR_ID_COMPANY_UCCI, funtional_unity);
            return Ok(listcollaborator);
        }

        public async Task<IHttpActionResult> Get([FromUri]int pidm)
        {
            Employee collaborator = new Employee();
            Employee collaborator_details;
            collaborator_details = await collaborator.getColaboratosByPidm(_ldr_constants.LDR_ID_COMPANY_UCCI, pidm);
            return Ok(collaborator_details);
        }
    }
}