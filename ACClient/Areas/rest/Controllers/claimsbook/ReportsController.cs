﻿using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.ComplaintsBook;
using _ldr_constants = ACTools.Constants.ClaimsBookConstants;


namespace ACClient.Areas.rest.Controllers.claimsbook
{
    [HasModule("bo_claims-book-reports")]
    public class ReportsController : ApiController
    {
        public async Task<IHttpActionResult> Post([FromBody] Reports dataReport)
        {
            var reports = new Reports();
            List<Reports> report;
            dataReport.div = _ldr_constants.LDR_ID_COMPANY_UCCI;
            if (dataReport.number_report == 1)
            {
                report = await reports.getReport1(dataReport);
            }
            else if (dataReport.number_report == 2)
            {
                report = await reports.getReport2(dataReport);
            }
            else if (dataReport.number_report == 3)
            {
                report = await reports.getReport3(dataReport);
            }
            else if (dataReport.number_report == 4)
            {
                report = await reports.getReport4(dataReport.date_start, dataReport.date_end);
            }
            else if (dataReport.number_report == 5)
            {
                report = await reports.getReport5(dataReport.number_days);
            }
            else
            {
                return InternalServerError();
            }

            return Ok(report);
        }
    }
}
