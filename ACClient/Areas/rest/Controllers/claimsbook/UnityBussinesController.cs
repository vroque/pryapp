﻿using System.Collections.Generic;
using System.Web.Http;
using System.Threading.Tasks;
using ACBusiness.ComplaintsBook;

namespace ACClient.Areas.rest.Controllers.claimsbook
{
    public class UnityBussinesController : ApiController
    {
        public async Task<IHttpActionResult> Get()
        {
            var businessUnit = new BusinessUnit();
            List<BusinessUnit> businessUnitList = await businessUnit.GetAllBusinessUnit();
            return Ok(businessUnitList);
        }
    }
}
