﻿using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web.Http;
using _ldr_constants = ACTools.Constants.ClaimsBookConstants;
using ACBusiness.ComplaintsBook;

namespace ACClient.Areas.rest.Controllers.claimsbook
{
    [HasModule("bo_claims-book-adm")]
    public class SatisfactionSurveyController : ApiController
    {
        public async Task<IHttpActionResult> Get([FromUri] int page_id)
        {
            var survey = new SatisfactionSurvey {page_id = page_id};
            var saveSurvey = await survey.generateSurvey();
            if (saveSurvey)
            {
                return Ok(new {survey_id = survey.survey_id});
            }

            return InternalServerError();
        }
    }
}
