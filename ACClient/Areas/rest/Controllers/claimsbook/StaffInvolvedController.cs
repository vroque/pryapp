﻿using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.SessionState;
using System.Web;
using _ldr_constants = ACTools.Constants.ClaimsBookConstants;
using ACAccess.Authorization;
using ACBusiness.ComplaintsBook;

namespace ACClient.Areas.rest.Controllers.claimsbook
{
    public class StaffInvolvedController : ApiController
    {
        private readonly IHttpSessionState _session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> Get([FromUri] int page_id)
        {
            var staffInvolved = new StaffInvolved();
            var list = await staffInvolved.getStaffInvolvedPage(_ldr_constants.LDR_ID_COMPANY_UCCI, page_id);
            return Ok(list);
        }

        public async Task<IHttpActionResult> Get([FromUri] int page_id, [FromUri] int derivate_id)
        {
            ACAuth authObj = (ACAuth) _session["AuthBackoffice"];

            var staffInvolved = new StaffInvolved();
            var list = await staffInvolved.getStaffInvolvedDerivationPidm(_ldr_constants.LDR_ID_COMPANY_UCCI, page_id,
                derivate_id, authObj.person_id.ToString());
            return Ok(list);
        }

        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> Post([FromBody] StaffInvolved staff)
        {
            var r = await staff.saveStaffInvolved();
            if (r)
            {
                return Ok(new {id = staff.staff_involved_id});
            }

            return InternalServerError();
        }
    }
}
