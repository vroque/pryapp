﻿using ACAccess.Authorization;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACBusiness.ComplaintsBook;
using _ldr_constants = ACTools.Constants.ClaimsBookConstants;

namespace ACClient.Areas.rest.Controllers.claimsbook
{
    [HasAuth("AuthBackoffice")]
    public class AnswerPageController : ApiController
    {
        private readonly IHttpSessionState _session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        private readonly AnswerPage _answerPage = new AnswerPage();

        public async Task<IHttpActionResult> Get(int id)
        {
            var answer = await _answerPage.getAnswer(id);
            return Ok(answer);
        }

        public async Task<IHttpActionResult> Get([FromUri] string page_id)
        {
            var answer = await _answerPage.getAnswerPage(int.Parse(page_id));
            return Ok(answer);
        }

        public async Task<IHttpActionResult> Get([FromUri] int page_id, [FromUri] int derivate_id)
        {
            List<AnswerPage> answers = await _answerPage.getAnswersDerivation(page_id, derivate_id);
            return Ok(answers);
        }

        public async Task<IHttpActionResult> Post([FromBody] AnswerPage answer)
        {
            ACAuth authObj = (ACAuth) _session["AuthBackoffice"];
            answer.answer_person_creator_pidm = authObj.person_id.ToString();
            var saveAnswer = await answer.saveAnswer();
            if (saveAnswer)
            {
                return Ok(new {answer_id = answer.answer_id});
            }

            return InternalServerError();
        }

        public async Task<IHttpActionResult> Put([FromBody] AnswerPage answer)
        {
            ACAuth auth_obj = (ACAuth) _session["AuthBackoffice"];
            var answerState = answer.answer_state;
            answer.answer_person_validate_pidm = auth_obj.person_id.ToString();
            if (answerState == _ldr_constants.LDR_STATE_ANSWER_REJECTED)
            {
                var saveAnswer =
                    await answer.rejectAnswerAdministrator(answer.answer_id, answer.answer_person_validate_pidm);
                if (saveAnswer)
                {
                    return Ok(new {answer_id = answer.answer_id});
                }

                return InternalServerError();
            }

            if (answerState == _ldr_constants.LDR_STATE_ANSWER_ACCEPTED ||
                answerState == _ldr_constants.LDR_STATE_ANSWER_ACCEPTED_FINISH)
            {
                var saveAnswer = await answer.acceptAnswer();
                if (saveAnswer)
                {
                    return Ok(new {answer_id = answer.answer_id});
                }

                return InternalServerError();
            }

            return InternalServerError();
        }
    }
}
