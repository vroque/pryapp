﻿using System.Collections.Generic;
using System.Web.Http;
using System.Threading.Tasks;
using ACClient.Helpers.ActionFilters;
using ACAccess.Authorization;
using System.Web.SessionState;
using System.Web;
using System.Net;
using System.Net.Http;
using ACBusiness.ComplaintsBook;
using ACClient.Helpers;

namespace ACClient.Areas.rest.Controllers.claimsbook
{
    [HasAuth("AuthBackoffice")]
    public class FileController : ApiController
    {
        private readonly IHttpSessionState _session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Obtiene un archivo
        /// </summary>
        /// <param name="id">código de archivo</param>
        /// <returns></returns>
        public IHttpActionResult Get(string id)
        {
            int fileId;
            if (!int.TryParse(id, out fileId))
            {
                return NotFound();
            }

            ;
            var file = new FilePage();
            List<string> data = file.GetFileData(fileId);
            if (data == null)
            {
                return NotFound();
            }

            return new FileResult(data[0], data[1]);
        }

        public async Task<IHttpActionResult> Get([FromUri] int page_id)
        {
            var bookFile = new FilePage();
            List<FilePage> filePageList = await bookFile.ListFilePage(page_id);
            return Ok(filePageList);
        }

        /// <summary>
        /// Obtiene archivos de una derivación
        /// </summary>
        /// <param name="page_id">código de reclamo</param>
        /// <param name="derivation_id">código de derivación</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get([FromUri] int page_id, [FromUri] int derivation_id)
        {
            var bookFile = new FilePage();
            List<FilePage> filePageList = await bookFile.ListFileDerivation(derivation_id);
            return Ok(filePageList);
        }

        /// <summary>
        /// Obtener archivos de una respuesta de una reclamación
        /// </summary>
        /// <param name="page_id">Código de reclamo</param>
        /// <param name="answer_id">código de respuesta</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get([FromUri] int page_id, [FromUri] string answer_id)
        {
            var bookFile = new FilePage();
            List<FilePage> filePageList = await bookFile.ListFileAnswerClaim(int.Parse(answer_id));
            return Ok(filePageList);
        }

        /// <summary>
        /// Guarda ARchivos
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> PostFormData()
        {
            ACAuth authObj = (ACAuth) _session["AuthBackoffice"];
            var fileClaims = new FilePage();
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var request = HttpContext.Current.Request;
            string pageId = request.Form.GetValues("page_id")[0];
            string derivateId = request.Form.GetValues("derivate_id")[0];
            string answerId = request.Form.GetValues("answer_id")[0];
            string action = request.Form.GetValues("action")[0];
            string userPidm = authObj.person_id.ToString();

            bool saveFile;
            var successful = 0;
            foreach (string file in request.Files)
            {
                var postedFile = request.Files[file];
                saveFile = fileClaims.SaveFile(pageId, derivateId, answerId, userPidm, postedFile, action);
                if (saveFile)
                {
                    successful++;
                }
            }

            return Ok(new {ok = successful});
        }
    }
}
