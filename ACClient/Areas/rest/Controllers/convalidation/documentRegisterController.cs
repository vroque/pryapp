﻿using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.convalidation
{
    [HasAuth("AuthBackoffice")]
    public class documentRegisterController : ApiController
    {
        public async Task<IHttpActionResult> get(int id)
        {
            List<ACRequestAttachments> doc = await ACRequestAttachments.list(id);
            if (doc != null)
                return Ok(doc);
            return InternalServerError();
        }

        public IHttpActionResult post(int id)
        {
            
            return InternalServerError();
        }
    }
}