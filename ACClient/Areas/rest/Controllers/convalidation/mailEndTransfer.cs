﻿using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using ACTools.Mail;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using _ate = ACTools.Constants.AtentionCenterConstants;

namespace ACClient.Areas.rest.Controllers.convalidation
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_cac-personal")]
    public class mailEndTransferController : ApiController
    {
        /// <summary>
        ///     Envia un correo
        /// </summary>
        /// <param name="id">Codigo de solicitud</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> post(int id)
        {
            ACRequest req = await ACRequest.get(id);

            // valid document type
            JObject json_obj = JObject.Parse(req.description);
            string conva_type_str = json_obj["conva_type"].ToString();
            int conva_type = 0;
            int.TryParse(conva_type_str, out conva_type);
            if (conva_type <= 0)
                throw new Exception("error en el tipo de convalidación");
            bool has_debt = conva_type == _ate.CONVA_TYPE_FIRST ? true : false;
            string description = json_obj["description"].ToString();
            //enviar correo
            Student st = new Student(req.person_id);
            MailSender mail = new MailSender();
            // adjuntar archivo
            if (req.document_paths != null && req.document_paths.Count > 0)
                foreach (string item in req.document_paths)
                    mail.addFile(item);

            mail.compose("end_transfer",
                new
                {
                    id = req.id,
                    amount = string.Format(req.cost.ToString(), "##.##"),
                    has_debt = has_debt,
                    student_id = st.id,
                    first_name = st.first_name,
                    full_name = st.full_name,
                    document_name = ACDocument.get(req.document_id).name,
                    date = $"{req.creation_date:dd/MM/yyyy hh:mm tt}",
                    date_now = $"{DateTime.Now:dd/MM/yyyy hh:mm tt}",
                    description = description
                    //state_name = _ate.STATES_NAMES[req.state]
                })
                .destination($"{st.id}@continental.edu.pe", $"[CAU] Envio de Documentos - Solicitud #{req.id}")
                .send();
            return Ok(new { ok = "ok" });
        }
    }
}