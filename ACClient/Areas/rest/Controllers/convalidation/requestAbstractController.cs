﻿using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.convalidation
{
    [HasAuth("AuthBackoffice")]
    public class requestAbstractController: ApiController
    {
        // private IHttpSessionState session = SessionStateUtility
        //         .GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// inserta o actualiza el campo descriptcion para 
        /// agregar los detalles de la convalidación
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> update(int id, [FromBody] Data data)
        {
            ACRequest req = await ACRequest.get(id);
            JObject json_obj = JObject.Parse(req.description);
            json_obj["convalidation"] = JObject.FromObject(new
            {
                type = (data.type == null) ? "" : data.type,
                institution = (data.institution == null) ? "" : data.institution,
                program = (data.program == null) ? "" : data.program,
                campus = (data.campus == null) ? "" : data.campus
            });
            
            req.description = json_obj.ToString();
            int i = await req.update();
            if (i > 0)
                return Ok(new { description = req.description });
            return InternalServerError();
        }
    }

    public class Data
    {
        public string type { get; set; }
        public string institution { get; set; }
        public string program { get; set; }
        public string campus { get; set; }

    }

    
}