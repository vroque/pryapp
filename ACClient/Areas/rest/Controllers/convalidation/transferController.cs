﻿using ACBusiness.AtentionCenter.Documents;
using ACBusiness.Convalidations;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.convalidation
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_cac-personal", "bo_deu-personal", "bo_rau-personal")]
    public class transferController: ApiController
    {
        public IHttpActionResult get(decimal id)
        {
            List<Transfer> list = new Transfer().list(id, true);
            return Ok(list);
        }

        public IHttpActionResult get(decimal id, [FromUri] string term)
        {
            List<Transfer> list = new Transfer().listByTerm(id, term);
            return Ok(list);
        }
        public IHttpActionResult get(decimal id, 
            [FromUri] string term, [FromUri] int tseq, [FromUri] int seq)
        {
            Transfer transfer = new Transfer().get(
                id, term, tseq: tseq, seq:seq, with_coures:true);
            return Ok(transfer);
        }
    }
}