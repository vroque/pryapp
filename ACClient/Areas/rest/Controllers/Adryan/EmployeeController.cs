using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.Adryan;
using _adryan = ACTools.Constants.AdryanConstants;

namespace ACClient.Areas.rest.Controllers.Adryan
{
    public class EmployeeController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // GET: api/Adryan/Employee
        public async Task<IHttpActionResult> Get()
        {
            var user = (ACAuth) _httpSessionState["AuthBackoffice"];
            return Ok(await Employee.GetByPidmAndCompanyIdAndStatusAsync(user.person_id,
                _adryan.ContinentalUniversityCompany, "active"));
        }

        // GET: api/Adryan/Employee/:huk
        public async Task<IHttpActionResult> Get(decimal huk)
        {
            return Ok(await Employee.GetByPidmAndCompanyIdAsync(huk));
        }

        // GET: api/Adryan/Employee?s=:s&type=:type&view=:view&status=:status
        public async Task<IHttpActionResult> Get(string s, string type, string view, string status)
        {
            if (view.Equals("uc"))
            {
                if (status.Equals("active") || status.Equals("inactive") || status.Equals("all"))
                {
                    return Ok(await Employee.SearchAsync(s, type, _adryan.ContinentalUniversityCompany,
                        status));
                }

                return StatusCode(HttpStatusCode.NotImplemented);
            }

            if (view.Equals("ic"))
            {
                if (status.Equals("active") || status.Equals("inactive") || status.Equals("all"))
                {
                    return Ok(await Employee.SearchAsync(s, type, _adryan.ApecCorporationCompany, status));
                }

                return StatusCode(HttpStatusCode.NotImplemented);
            }

            if (view.Equals("all"))
            {
                if (status.Equals("active") || status.Equals("inactive") || status.Equals("all"))
                {
                    return Ok(await Employee.SearchAsync(s, type, view, status));
                }

                return StatusCode(HttpStatusCode.NotImplemented);
            }

            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // GET: api/Adryan/Employee/:huk?view=:view&status=:status
        public async Task<IHttpActionResult> Get(decimal huk, string view, string status = "")
        {
            if (view.Equals("uc"))
            {
                if (status.Equals("active") || status.Equals("inactive") || status.Equals("all"))
                {
                    return Ok(await Employee.GetByPidmAndCompanyIdAndStatusAsync(huk,
                        _adryan.ContinentalUniversityCompany, status));
                }

                return StatusCode(HttpStatusCode.NotImplemented);
            }

            if (view.Equals("ic"))
            {
                if (status.Equals("active") || status.Equals("inactive") || status.Equals("all"))
                {
                    return Ok(await Employee.GetByPidmAndCompanyIdAndStatusAsync(huk, _adryan.ApecCorporationCompany,
                        status));
                }

                return StatusCode(HttpStatusCode.NotImplemented);
            }

            if (view.Equals("all"))
            {
                if (status.Equals("active") || status.Equals("inactive") || status.Equals("all"))
                {
                    return Ok(await Employee.GetByPidmAndCompanyIdAndStatusAsync(huk, view, status));
                }

                return StatusCode(HttpStatusCode.NotImplemented);
            }

            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // POST: api/Adryan/Employee
        public async Task<IHttpActionResult> Post()
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PUT: api/Adryan/Employee
        public async Task<IHttpActionResult> Put()
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // DELETE: api/Adryan/Employee/:huk
        public async Task<IHttpActionResult> Delete()
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
