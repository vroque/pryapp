using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Adryan;
using _adryan = ACTools.Constants.AdryanConstants;

namespace ACClient.Areas.rest.Controllers.Adryan
{
    public class FunctionalUnitController : ApiController
    {
        // GET: api/Adryan/FunctionalUnit
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await FunctionalUnit.GetAllMainFunctionalUnitByFunctionalUnitIdAndCompanyIdAsync());
        }

        // GET: api/Adryan/FunctionalUnit/:huk
        public async Task<IHttpActionResult> Get(string huk)
        {
            return Ok(await FunctionalUnit.GetByIdAndCompanyIdAsync(huk));
        }

        // GET: api/Adryan/FunctionalUnit/:huk?view=:view
        public async Task<IHttpActionResult> Get(string huk, string view)
        {
            var functionalUnit = await FunctionalUnit.GetByIdAndCompanyIdAsync(huk);

            if (functionalUnit == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }

            if (view.Equals("parent"))
            {
                return Ok(await FunctionalUnit.GetByIdAndCompanyIdAsync(functionalUnit
                    .unidad_funcional_superior));
            }

            if (view.Equals("children"))
            {
                return Ok(await new FunctionalUnit(functionalUnit).GetAllChildFunctionalUnitByCompanyIdAsync());
            }

            if (view.Equals("members"))
            {
                return Ok(await Employee.GetAllEmployeesByFunctionalUnitAndCompanyIdAsync(huk));
            }

            if (view.Equals("ic"))
            {
                return Ok(await FunctionalUnit.GetByIdAndCompanyIdAsync(huk,
                    _adryan.ApecCorporationCompany));
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: api/Adryan/FunctionalUnit?s=:s&type=:type&view=:view
        public async Task<IHttpActionResult> Get(string s, string type, string view)
        {
            if (type.Equals("name"))
            {
                if (view.Equals("uc"))
                {
                    return Ok(await FunctionalUnit.SearchAsync(s,_adryan.ContinentalUniversityCompany));
                }

                if (view.Equals("ic"))
                {
                    return Ok(await FunctionalUnit.SearchAsync(s, _adryan.ApecCorporationCompany));
                }

                if (view.Equals("all"))
                {
                    return Ok(await FunctionalUnit.SearchAsync(s, view));
                }

                return StatusCode(HttpStatusCode.Forbidden);
            }
            
            return StatusCode(HttpStatusCode.Forbidden);
        }

        // POST: api/Adryan/FunctionalUnit
        public async Task<IHttpActionResult> Post(FunctionalUnit functionalUnit)
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PUT: api/Adryan/FunctionalUnit
        public async Task<IHttpActionResult> Put(FunctionalUnit functionalUnit)
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // DELETE: api/Adryan/FunctionalUnit/:huk
        public async Task<IHttpActionResult> Delete(string huk)
        {
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
