﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Apoderapp;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.Apoderapp
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_pt-personal")]
    public class StudentController : ApiController
    {
        // GET: api/apoderapp/Student
        public IHttpActionResult Get()
        {
            return Ok(HttpStatusCode.NoContent);
        }

        // GET: api/apoderapp/Student/:studentId
        public async Task<IHttpActionResult> Get(string huk)
        {
            return Ok(await StudentDetail.GetTask(huk));
        }

        // POST: api/Apoderado
        public async Task<IHttpActionResult> Post(Data data)
        {
            var type = data.Type;
            var studentCodes = data.StudentCodes.ToArray();

            switch (type)
            {
                // Búsqueda de estudiantes por código de DNI
                case "search":
                    return Ok(await Student.SearchByDniTask(studentCodes));
                default:
                    return Ok(HttpStatusCode.NoContent);
            }
        }

        // PUT: api/Apoderado/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Apoderado/5
        public void Delete(int id)
        {
        }

        public class Data
        {
            public string Type { get; set; }
            public string[] StudentCodes { get; set; }
        }
    }
}