﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.Apoderapp;
using ACBusiness.Personal;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.Apoderapp
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_pt-personal")]
    public class ApoderadoController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // GET: api/Apoderado
        public IEnumerable<string> Get()
        {
            return new string[] {"value1", "value2"};
        }

        // GET: api/Apoderado/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/apoderapp/Apoderado
        public async Task<IHttpActionResult> Post(Data data)
        {
            var type = data.Type;

            ACAuth acAuth = (ACAuth) _httpSessionState["AuthBackoffice"];

            switch (type)
            {
                // Busca personas para obtener info si son apoderados del estudiante o no
                case "searchStudentApoderado":
                    return Ok(await Apoderado.SearchStudentApoderadoByDocumentNumberTask(data.DocumentNumber,
                        data.Student));
                // Registra nuevo apoderado de estudiante
                case "new":
                    return Ok(await Apoderado.AddApoderadoTask(data.Student, data.Person, data.KinshipId,
                        data.Comment, acAuth.person_id));
                case "update":
                    return Ok(await Apoderado.UpdateApoderadoAccessTask(data.Student, data.Apoderado,
                        acAuth.person_id));
                default:
                    return Ok(HttpStatusCode.NoContent);
            }
        }

        // PUT: api/Apoderado/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Apoderado/5
        public void Delete(int id)
        {
        }

        public class Data
        {
            public string Type { get; set; }
            public Student Student { get; set; }
            public Person Person { get; set; }
            public Apoderado Apoderado { get; set; }

            /// <summary>
            /// Parentesco ID
            /// </summary>
            public int KinshipId { get; set; }

            public string Comment { get; set; }
            public string[] DocumentNumber { get; set; }
        }
    }
}