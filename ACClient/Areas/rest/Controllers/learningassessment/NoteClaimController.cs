﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.LearningAssessment;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.learningassessment
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class NoteClaimController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        NoteClaim objNoteClaim = new NoteClaim();

        [HasModule("fd_estudiante")]
        public async Task<IHttpActionResult> Get([FromUri] string department, [FromUri] string term, [FromUri] string nrc, [FromUri] int component, [FromUri] int? subComponent)
        {
            ACAuth authObj = (ACAuth)session["Auth"];
            Student student = new Student
            {
                id = authObj.id,
                person_id = authObj.person_id,
                first_name = authObj.first_name,
                last_name = authObj.last_name,
                profile = authObj.profile
            };
            return Ok(await objNoteClaim.GetDataNrcByStudent(student, department, term, nrc, component, subComponent));
        }
        [HasModule("fd_estudiante", "bo_cau-personal", "bo_rau-personal", "bo_learning_assessment", "bo_teacher_management")]
        public IHttpActionResult Get([FromUri] string status, [FromUri] string officeId)
        {
            ACAuth authObj = (ACAuth)session["AuthBackoffice"];
            List<NoteClaim.Module> moduleList = authObj.module_list.Select(
                    row => new NoteClaim.Module
                    {
                        id = row.id,
                        campus = row.campus,
                        description = row.description,
                        name = row.name
                    }
                ).ToList();
            return Ok(objNoteClaim.GetListNotesClaimByStatusAndOffice(int.Parse(status),int.Parse(officeId), moduleList));
        }

        [HasModule("fd_estudiante", "bo_cau-personal", "ANONYMOUS", "bo_rau-personal", "bo_learning_assessment", "bo_teacher_management")]
        public IHttpActionResult Get(long id)
        {
            return Ok(objNoteClaim.GetAllDataById(id));
        }

        [HasModule("fd_estudiante")]
        public IHttpActionResult Get(string term) {
            ACAuth authObj = (ACAuth)session["Auth"];
            return Ok(objNoteClaim.GetClaimsByStudentAndTerm(authObj.person_id, term));
        }

        [HasModule("ANONYMOUS")]
        public IHttpActionResult Get()
        {
            ACAuth authObj = (ACAuth)session["AuthBackoffice"];
            return Ok(objNoteClaim.GetListNotesClaimByTeacher(authObj.person_id));
        }

        [HasModule("fd_estudiante")]
        public async Task<IHttpActionResult> Post([FromBody] NoteClaim noteClaim) {
            ACAuth authObj = (ACAuth)session["Auth"];
            Student student = new Student
            {
                id = authObj.id,
                person_id = authObj.person_id,
                first_name = authObj.first_name,
                last_name = authObj.last_name,
                profile = authObj.profile
            };
            return Ok(await objNoteClaim.Save(student, noteClaim, student.person_id));
        }

        [HasModule("bo_cau-personal")]
        public IHttpActionResult Put([FromBody] NoteClaim noteClaim) {
            ACAuth authObj = (ACAuth)session["AuthBackoffice"];
            return Ok(objNoteClaim.SaveDerivateTeacher(noteClaim, authObj.person_id));
        }

        [HasModule("bo_rau-personal")]
        public IHttpActionResult Patch([FromBody] NoteClaim noteClaim)
        {
            ACAuth authObj = (ACAuth)session["AuthBackoffice"];
            List<NoteClaim.Module> moduleList = authObj.module_list.Select(
                    row => new NoteClaim.Module
                    {
                        id = row.id,
                        campus = row.campus,
                        description = row.description,
                        name = row.name
                    }
                ).ToList();
            return Ok(objNoteClaim.FinalizeClaim(noteClaim, authObj.person_id, moduleList));
        }

    }
}