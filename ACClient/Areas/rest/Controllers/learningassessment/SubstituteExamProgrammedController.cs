﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.LearningAssessment;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.learningassessment
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class SubstituteExamProgrammedController: ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        [HasModule("fd_estudiante")]
        public async Task<IHttpActionResult> Get()
        {
            ACAuth authObj = (ACAuth)session["Auth"];
            SubstituteExamProgrammed objProgram = new SubstituteExamProgrammed();
            return Ok(await objProgram.GetSubstituteExamProgrammedbyStudentAndByCurrentPeriod(authObj.profile));
        }

        [HasModule("fd_estudiante")]
        public async Task<IHttpActionResult> Post([FromBody]data data)
        {
            SubstituteExamProgrammed objProgram = new SubstituteExamProgrammed();
            ACAuth authObj = (ACAuth)session["Auth"];
            Student student = new Student
            {
                id = authObj.id,
                person_id = authObj.person_id,
                first_name = authObj.first_name,
                last_name = authObj.last_name,
                profile = authObj.profile
            };
            return Ok(await objProgram.SaveSustiByStudentAndLastTerm(student, authObj.id, data.NrcProgram, data.Justification, true));
        }

        [HasModule("bo_learning_assessment")]
        public async Task<IHttpActionResult> Put([FromBody]data data)
        {
            SubstituteExamProgrammed objProgram = new SubstituteExamProgrammed();
            ACAuth authObj = (ACAuth)session["AuthBackoffice"];
            switch (data.Type)
            {
                case 1:
                    return Ok(await objProgram.GetSubstituteExamProgrammedbyStudentAndTerm(data.Student.profile.person_id, data.Term));
                case 2:
                    return Ok(await objProgram.SaveSustiByStudentAndTerm(data.Student, authObj.id, data.NrcProgram, data.Comment, data.Term, false));
                case 3:
                    return Ok(await objProgram.ModifySustiByStudent(data.Student, authObj.id, data.SubstituteExams, data.Comment));
                default:
                    throw new Exception("Campo tipo incorrecto.");
            }
        }
    }

    public class data {
        public List<NrcForSubstituteExam> NrcProgram { get; set; }
        public string Justification { get; set; }
        public string Comment { get; set; }
        public Student Student { get; set; }
        public string Term { get; set; }
        public List<SubstituteExamProgrammed> SubstituteExams { get; set; }
        public int Type { get; set; }

    }

}