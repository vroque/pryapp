﻿using ACClient.Helpers.ActionFilters;
using ACBusiness.LearningAssessment;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using _acad = ACTools.Constants.AcademicConstants;
using System.Web.SessionState;
using System.Web;
using ACAccess.Authorization;

namespace ACClient.Areas.rest.Controllers.learningassessment
{
    [HasModule("bo_learning_assessment", "bo_cau-personal", "bo_rau-personal", "bo_teacher_management")]
    public class ReportsController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        Reports reports = new Reports();
        public async Task<IHttpActionResult> Get()
        {
            List<string> list = await reports.getTerms();
            return Ok(list);
        }
        public async Task<IHttpActionResult> post([FromBody]Reports dataReport) {
            ACAuth authObj = (ACAuth)session["AuthBackoffice"];
            dataReport.input_div = _acad.DEFAULT_DIV;
            switch (dataReport.NumberReport)
            {
                case 1:
                    return Ok(await reports.ReportSusbstituteProgrammed(dataReport.input_div, dataReport.input_term, dataReport.input_group, dataReport.input_modality));
                case 2:
                    return Ok(await reports.ReportOfNrcsProgrammed(dataReport.input_modality, dataReport.InputCampus, dataReport.input_term, dataReport.input_group));
                case 3:
                    return Ok(await reports.ReportGeneralNoteClaim(dataReport.input_modality,
                        dataReport.InputCampus, dataReport.input_term, 
                        dataReport.inputType, dataReport.inputStatus, 
                        dataReport.inputComponent, dataReport.inputStudenId));
                case 4:
                    return Ok(reports.SendMailReportNoteClaim(dataReport.emailSubject, dataReport.emailBody, dataReport.lstNotesClaim, authObj.id));
                default:
                    throw new System.Exception("Numero de reporte invalido");
            }
        }

    }
}