﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.LearningAssessment;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.learningassessment
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class NrcForSubstituteExamController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        NrcForSubstituteExam objSubstitute = new NrcForSubstituteExam();

        [HasModule("fd_estudiante")]
        public async Task<IHttpActionResult> Get()
        {
            ACAuth authObj = (ACAuth)session["Auth"];
            return Ok(await objSubstitute.GetListsubjectsbyStudentAndByCurrentPeriod(authObj.profile, true, true));
        }

        [HasModule("bo_learning_assessment")]
        public async Task<IHttpActionResult> Post([FromBody] DataGetNrcs data)
        {
            switch (data.Type)
            {
                case 1:
                    return Ok(await objSubstitute.GetListNrcsByTermAndDepartment(data.Department, data.Term, data.ListNrcs));
                case 2:
                    return Ok(await objSubstitute.GetListSubjectsbyStudentAndTerm(data.Profile, data.Term, false, true));
                default:
                    throw new System.Exception("Campo tipo incorrecto.");
            }
            
        }

        [HasModule("bo_learning_assessment")]
        public async Task<IHttpActionResult> Get([FromUri] string departament, [FromUri] string term, [FromUri] string nrc)
        {
            return Ok(await objSubstitute.GetDetailsbyDepartmentAndTermAndNrc(departament, term, nrc));
        }

        [HasModule("bo_learning_assessment")]
        public async Task<IHttpActionResult> put([FromBody]NrcForSubstituteExam nrc)
        {
            ACAuth authObj = (ACAuth)session["AuthBackoffice"];
            return Ok(await objSubstitute.SaveNrcForSubstituteExam(nrc, authObj.id, true));
        }

    }
    public class DataGetNrcs
    {
        public string Department { get; set; }
        public string Term { get; set; }
        public List<string> ListNrcs { get; set; }
        public AcademicProfile Profile { get; set; }
        public int Type { get; set; }
    }
}