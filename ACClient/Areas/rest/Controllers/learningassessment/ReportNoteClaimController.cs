﻿using ACAccess.Authorization;
using ACBusiness.LearningAssessment;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.learningassessment
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class ReportNoteClaimController: ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        ReportNoteClaim objReportNoteClaim = new ReportNoteClaim();

        [HasModule("ANONYMOUS", "bo_rau-personal", "bo_learning_assessment", "bo_teacher_management")]
        public IHttpActionResult Post([FromBody] ReportNoteClaim data)
        {
            ACAuth authObj = (ACAuth)session["AuthBackoffice"];
            return Ok(objReportNoteClaim.Save(data, authObj.person_id));
        }
        [HasModule("bo_rau-personal", "bo_learning_assessment", "bo_teacher_management")]
        public IHttpActionResult Put([FromBody] ReportNoteClaim data)
        {
            ACAuth authObj = (ACAuth)session["AuthBackoffice"];
            List<NoteClaim.Module> moduleList = authObj.module_list.Select(
                    row => new NoteClaim.Module
                    {
                        id = row.id,
                        campus = row.campus,
                        description = row.description,
                        name = row.name
                    }
                ).ToList();
            return Ok(objReportNoteClaim.ProcessReport(data, authObj.person_id, moduleList));
        }
    }
}