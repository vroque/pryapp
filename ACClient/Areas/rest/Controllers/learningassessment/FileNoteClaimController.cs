﻿using ACBusiness.LearningAssessment;
using ACClient.Helpers;
using ACClient.Helpers.ActionFilters;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.learningassessment
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class FileNoteClaimController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        FileNoteClaim objFileNoteClaim = new FileNoteClaim();

        [HasModule("fd_estudiante", "bo_cau-personal", "ANONYMOUS")]
        public IHttpActionResult Get(long id)
        {
            Tuple<string, string> file = objFileNoteClaim.GetFileDataById(id);
            if (file == null) return NotFound();
            else return new FileResult(file.Item1, file.Item2);
        }

        [HasModule("fd_estudiante", "ANONYMOUS")]
        public IHttpActionResult Post() {
            if (!Request.Content.IsMimeMultipartContent()) throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            var request = HttpContext.Current.Request;
            int noteClaimId = int.Parse(request.Form.GetValues("noteClaimId")[0]);
            string reportNoteClaimId = request.Form.GetValues("reportNoteClaimId")[0];
            HttpPostedFile file = request.Files[0];
            return Ok(objFileNoteClaim.SaveFile(noteClaimId, (reportNoteClaimId == "null" ? null: (int?)(int.Parse(reportNoteClaimId))), file));
        }
    }
}