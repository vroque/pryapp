﻿using ACBusiness.Benefit;
using ACClient.Helpers.ActionFilters;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using static ACBusiness.Benefit.CreateAgreement;
using System.Web;

namespace ACClient.Areas.rest.Controllers.benefit
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_convenios", "bo_beneficios")]
    public class AgreementController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        private readonly ACAuth _backofficeAuth =
            (ACAuth)SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current)["AuthBackoffice"];

        // GET: rest/Agreement
        public IHttpActionResult Get()
        {
            Agreement agreement = new Agreement();
            return Ok(agreement.list());
        }
        public IHttpActionResult Get(string id)
        {
            return Ok(new Agreement().getAgreement(id));
        }
        public IHttpActionResult Put(Agreement convenio)
        {
            return Ok(new Agreement().PostAgreement(convenio));
        }
        public IHttpActionResult Post(CreateAgreement data)
        {
            var acAuth = (ACAuth)_httpSessionState["AuthBackoffice"];

            return Ok(new CreateAgreement().Create(data, acAuth.id));
        }
    }
}