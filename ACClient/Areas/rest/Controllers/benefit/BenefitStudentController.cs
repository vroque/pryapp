﻿using ACAccess.Authorization;
using ACBusiness.Benefit;
using ACClient.Helpers.ActionFilters;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.benefit
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_convenios", "bo_beneficios")]
    public class BenefitStudentController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public IHttpActionResult Get(string id)
        {
            return Ok(new BenefitStudent().list(id));
        }

        [HttpPut]
        public IHttpActionResult Put(BenefitStudent model)
        {
            var auth = (ACAuth)_httpSessionState["AuthBackoffice"];
            return Ok(new BenefitStudent().DeleteBenefit(model,auth.id));
        }
    }
}