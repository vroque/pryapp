﻿using ACBusiness.Benefit;
using ACClient.Helpers.ActionFilters;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using static ACBusiness.Benefit.AsignBenefit;
using System.Web;

namespace ACClient.Areas.rest.Controllers.benefit
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_convenios", "bo_beneficios")]
    public class AsignBenefitController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        private readonly ACAuth _backofficeAuth =
            (ACAuth)SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current)["AuthBackoffice"];
        public IHttpActionResult Post(AsignBenefit data)
        {
            var acAuth = (ACAuth)_httpSessionState["AuthBackoffice"];

            return Ok(new AsignBenefit().Create(data, acAuth.id));
        }
    }
}
