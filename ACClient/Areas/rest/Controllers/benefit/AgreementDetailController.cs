﻿using ACBusiness.Benefit;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.benefit
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_convenios", "bo_beneficios")]
    public class AgreementDetailController : ApiController
    {
        // GET: rest/AgreementDetail
        public IHttpActionResult Get(string id)
        {
            return Ok(new AgreementDetail().getdetail (id));
        }
        public IHttpActionResult Post(List<AgreementDetail> detalle)
        {
            return Ok(new AgreementDetail().postDetail(detalle));
        }
    }
}