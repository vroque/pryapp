﻿using ACBusiness.Benefit;
using ACClient.Helpers.ActionFilters;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.benefit
{
    public class DegreeFamiliarityController : ApiController
    {
        [HasAuth("AuthBackoffice")]
        [HasModule("bo_convenios", "bo_beneficios")]
        public IHttpActionResult Get()
        {
            DegreeFamiliarity degreefamiliarity = new DegreeFamiliarity();
            return Ok(degreefamiliarity.list());
        }
    }
}