﻿using System.Web.Http;
using ACBusiness.Benefit;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.benefit
{
    public class DiscountController : ApiController
    {
        [HasAuth("AuthBackoffice")]
        [HasModule("bo_convenios", "bo_beneficios")]
        public IHttpActionResult Get()
        {
            Discount discount = new Discount();
            return Ok(discount.list());
        }
    }
}
