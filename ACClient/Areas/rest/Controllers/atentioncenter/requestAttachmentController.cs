﻿using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    [HasAuth("AuthBackoffice")]
    public class requestAttachmentController : ApiController
    {
        /// <summary>
        /// File document requiriments from a request
        /// </summary>
        /// <param name="id">request id</param>
        /// <returns>list of requeriment docs</returns>
        public async Task<IHttpActionResult> get(int id)
        {
            List<ACRequestAttachments> doc = await ACRequestAttachments.list(id);
            if (doc != null)
                return Ok(doc);
            return InternalServerError();
        }

    }
}