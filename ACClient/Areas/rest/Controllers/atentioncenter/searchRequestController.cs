﻿using ACAccess.Authorization;
using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// 
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class searchRequestController: ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> get([FromUri] Data data)
        {
            ACAuth auth_obj = (ACAuth)session["AuthBackoffice"];
            List<ACRequest> list = await ACRequest.search(
                data.id, data.student_id, data.document_id, 
                data.state, data.date, auth_obj.campus.id);
            return Ok(list);
        }
    }

    public class Data
    {
        public int? id { get; set; }
        public string student_id { get; set; }
        public DateTime? date { get; set; }
        public int? state { get; set; }
        public int? document_id { get; set; }
    }
}