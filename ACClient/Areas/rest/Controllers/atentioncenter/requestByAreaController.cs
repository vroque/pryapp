﻿using ACAccess.Authorization;
using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACBusiness.Academic;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api para listar solicitudes para un area
    /// </summary>
    [HasAuth("AuthBackoffice", "Auth")]
    public class requestByAreaController : ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Obtiene las solicitudes de una area
        /// **Generalmente es estado -> Derivado/Pendiente de Aprovación
        /// </summary>
        /// <param name="id">Area ID</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> get(string id)
        {
            int area = 0;
            
            int.TryParse(id, out area);
            if (area != 0) { //solicitudes para areas
                ACAuth auth_obj = (ACAuth)session["AuthBackoffice"];
                List<ACRequest> list = await ACRequest.listByArea(auth_obj.campus.id, area);
                return Ok(list);
            }
            else //Solicitudes para el alumno logeado
            {
                ACAuth auth_obj = (ACAuth)session["Auth"];
                List<ACRequest> list = await ACRequest.listByPerson(auth_obj.person_id);
                return Ok(list);
            }
            // return InternalServerError();
        }
        public async Task<IHttpActionResult> Post(AcademicProfile profile)
        {
            if(profile == null)
            {
                ACAuth auth_obj = (ACAuth)session["Auth"];
                List<ACRequest> list = await ACRequest.listByPerson(auth_obj.person_id);
                return Ok(list);
            }
            else
            {
                List<ACRequest> list = await ACRequest.listByPerson(profile.person_id);
                return Ok(list);
            }
        }

        public async Task<IHttpActionResult> Put(ACRequest request)
        {
            ACRequest objRequest = new ACRequest();
            ACRequest result = await objRequest.cancelRequest(request);
            return Ok(result);
        }


    }

}