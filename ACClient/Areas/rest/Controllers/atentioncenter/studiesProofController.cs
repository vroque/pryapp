﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using CABusiness.AtentionCenter.Documents;
using System.Collections.Generic;

namespace CentroAtencionClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api paara Constancias de Estudios
    /// </summary>
    [HasAuth("Auth")]
    public class studiesProofController: ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">studies proof type={
        /// 0:"general",
        /// 1:"history",
        /// 2:"by_term",
        /// 3:"avarage_score",
        /// 4:"career",
        /// 5:"custom",
        /// 6:"with dates"
        /// }
        /// </param>
        /// <returns>StudiesProof object</returns>
        public async Task<IHttpActionResult> get(int id)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            StudiesProof doc = new StudiesProof(auth_obj.profile,id);
            if (await doc.validate() && id==2)
            {
                //include include list of terms(period_list) 
                await  doc.getTermList();
            }
            return Ok(doc);
        }
        
        public async Task<IHttpActionResult> post([FromBody]Data data)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            StudiesProof doc = new StudiesProof(auth_obj.profile,data.type);
            Student student = new Student(auth_obj.person_id);
            ACRequest request = await doc.createRequest(
                student, auth_obj.profile, data.description,
                data.type, data.term_list, auth_obj.id);
            if (ACRequest.isNull(request))
                return InternalServerError();
            return Ok(request);
        }
        
        public class Data
        {
            public string description { get; set; }
            /// <summary>
            /// studies proof type={
            /// 0:"general",
            /// 1:"history",
            /// 2:"by_term",
            /// 3:"avarage_score",
            /// 4:"career",
            /// 5:"custom",
            /// 6:"with dates"
            /// }
            /// </summary>
            public int type { get; set; }
            public List<string> term_list { get; set; }
        }
        
        


    }
}