﻿using ACAccess.Authorization;
using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api para listar solicitudes que generan resoluciones
    /// para un alumno logeado
    /// </summary>
    [HasAuth("Auth")]
    public class ResolutionRequestController : ApiController
    {
        readonly IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Obtiene las solicitudes de resolucion
        /// </summary>
        /// <param name="id">person id</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            List<ACRequest> list = await ACRequest.listResolutionsByPerson(auth_obj.person_id);
            return Ok(list);
          
        }
    }
}