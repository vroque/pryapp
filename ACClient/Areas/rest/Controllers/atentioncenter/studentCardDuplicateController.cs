﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACBusiness.AtentionCenter.Documents;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api para Carné de ingreso
    /// </summary>
    [HasAuth("Auth")]
    public class studentCardDuplicateController : ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            StudentCardDuplicate doc = new StudentCardDuplicate(auth_obj.profile);
            await doc.validate();
            return Ok(doc);
        }
        public async Task<IHttpActionResult> post([FromBody]Data data)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            StudentCardDuplicate doc = 
                new StudentCardDuplicate(auth_obj.profile);
            Student student = new Student(auth_obj.person_id);
            ACRequest request = await doc.createRequest(
                student,data.description, auth_obj.id);
            if (ACBusiness.AtentionCenter.ACRequest.isNull(request))
                return InternalServerError();
            return Ok(request);
        }

        public class Data
        {
            public string description { get; set; }
        }
    }
}