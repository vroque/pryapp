﻿using ACAccess.Authorization;
using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api general para solicitud
    /// </summary>
    
    public class requestController : ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        [HasAuth("AuthBackoffice")]
        [HasModule("bo_cau-personal", "bo_rau-personal")]
        public async Task<IHttpActionResult> get()
        {
            var authBackoffice = (ACAuth)session["AuthBackoffice"];
            List<ACRequest> list = await ACRequest.listInAtentionCampus(authBackoffice.campus.id);
            return Ok(list);
        }
        /// <summary>
        /// Obtiene información sobre una solicitud especifica
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HasAuth("Auth", "AuthBackoffice")]
        [HasModule("bo_cau-personal", "bo_rau-personal", "fd_academic", "bo_cic-persona")]
        public async Task<IHttpActionResult> get(string id)
        {
            int requestId;
            if (int.TryParse(id, out requestId))
            {
                var request = await ACRequest.get(requestId);
                return Ok(request);
            }
            return InternalServerError();
        }

        /// <summary>
        /// Deriva la solicitud al area/usuario correspondiente
        /// </summary>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        [HasModule("bo_cau-personal", "bo_rau-personal", "bo_cac-personal", "bo_deu-personal")]
        public async Task<IHttpActionResult> put(string id, [FromBody] derivateObj obj)
        {
            ACAuth auth_obj = (ACAuth)session["AuthBackoffice"];
            int request_id;
            if (int.TryParse(id, out request_id))
            {
                ACRequest request = await ACRequest.get(request_id);
                int r;
                switch (obj.type)
                {
                    case "derive":
                        r = await request.derive($"Derivación por CAU {obj.message}", auth_obj.id);
                        //int i = await request.goAprovalPending(obj.area_id, obj.message, auth_obj.id);
                        if (r == 1)
                            return Ok(request);
                        break;
                    case "aprove":
                        r = await request.goAprove(obj.message, auth_obj.id);
                        if (r == 1)
                            return Ok(request);
                        break;
                    case "refuse":
                        r = await request.goCancel(obj.message, auth_obj.id);
                        if (r == 1)
                            return Ok(request);
                        break;
                    case "debt":
                        r = await request.setDebt(obj.message, auth_obj.id);
                        
                        if (r == 1)
                            return Ok(request);
                        break;
                    default:
                        break;
                        
                }
                
            }
            return InternalServerError();
        }

        /// <summary>
        /// Marca la Solicitud como entregada
        /// El paso final de una solicitud
        /// </summary>
        /// <returns></returns>
        [HasAuth("AuthBackoffice")]
        public async Task<IHttpActionResult> post(string id)
        {
            ACAuth auth_obj = (ACAuth)session["AuthBackoffice"];
            int request_id;
            if (int.TryParse(id, out request_id))
            {
                ACRequest request = await ACRequest.get(request_id);
                int i = await request.goFinal("Flujo de la solicitud finalizado.", auth_obj.id);
                if ( i == 1)
                    return Ok(request);
            }
            return InternalServerError();
        }

        public class derivateObj
        {
            //public int area_id { get; set; }
            public string message { get; set; }
            public string type { get; set; }
        }
    }

}