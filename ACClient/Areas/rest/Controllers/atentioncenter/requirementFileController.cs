﻿using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class requirementFileController : ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">ID de la Solicitud</param>
        /// <param name="file">Informacion de requisito a agregar</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> post(int id, [FromBody]ACRequirementFile file)
        {
            ACRequirementFile rf = await file.create(id);
            if(rf != null)
                return Ok(rf);
            return InternalServerError();
        }

        public async Task<IHttpActionResult> delete(int id)
        {
            ACRequirementFile file = new ACRequirementFile();
            bool delete = await file.delete(id);
            if (delete) return Ok(delete);
            return InternalServerError();
        }
    }
}