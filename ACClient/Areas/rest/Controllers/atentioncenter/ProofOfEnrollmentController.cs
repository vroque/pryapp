﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACBusiness.AtentionCenter.Documents;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <inheritdoc />
    /// <summary>
    /// Constancia de Matrícula
    /// </summary>
    [HasAuth("Auth", "AuthBackoffice")]
    public class ProofOfEnrollmentController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // GET: api/AtentionCenter/ProofOfEnrollment
        public async Task<IHttpActionResult> Get()
        {
            var acAuth = (ACAuth) _httpSessionState["Auth"];
            var doc = new EnrollmentProof(acAuth.profile);
            await doc.validate();
            return Ok(doc);
        }

        // POST: api/AtentionCenter/ProofOfEnrollment
        public async Task<IHttpActionResult> Post([FromBody] Data data)
        {
            var type = data.Type;

            var studentAuth = (ACAuth) _httpSessionState["Auth"];
            var backofficeAuth = (ACAuth) _httpSessionState["AuthBackoffice"];

            switch (type)
            {
                // Crea Solicitud de Constancia de Matrícula
                case "createRequest":
                    var doc = new EnrollmentProof(studentAuth.profile);
                    var student = new Student(studentAuth.person_id);
                    var request = await doc.createRequest(student, studentAuth.profile, data.PeriodList,
                        data.Description, studentAuth.id);
                    if (ACRequest.isNull(request))
                    {
                        return InternalServerError();
                    }

                    return Ok(request);
                // Constancia de Matrícula PRONABEC
                case "pronabec":
                    return Ok(await new PronabecProofOfEnrollment().GenerateAsync(data.ScholarshipStudents,
                        data.ScholarshipType, data.SchoolId, data.AcademicPeriod, backofficeAuth.id));
                // Constancia de Matrícula Promocional
                case "promotional":
                    return Ok(await new PromotionalProofOfEnrollment().GenerateAsync(data.Graduates,
                        data.AcademicPeriod, backofficeAuth.id));
                case "1stEnrollment":
                    return Ok(await new PromotionalProofOfEnrollment().Generate1stEnrollmentAsync(data.Graduates,
                        backofficeAuth.id));
                default:
                    return Ok(HttpStatusCode.NotFound);
            }
        }

        public class Data
        {
            public string Type { get; set; }
            public string Description { get; set; }
            public List<string> PeriodList { get; set; }
            public ScholarshipStudent[] ScholarshipStudents { get; set; }
            public string ScholarshipType { get; set; }
            public string SchoolId { get; set; }
            public string AcademicPeriod { get; set; }
            public Graduated[] Graduates { get; set; }
        }
    }
}