﻿using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;


namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api para Registro de solicitudes (Log)
    /// </summary>
    [HasAuth("AuthBackoffice", "Auth")]
    public class requestLogController: ApiController
    {
        public async Task<IHttpActionResult> get(string id)
        {
            int request_id;
            if (int.TryParse(id, out request_id))
            {
                ACRequestLog log = new ACRequestLog();
                List<ACRequestLog> logs = await log.listByRequest(request_id);
                return Ok(logs);
            }
            return InternalServerError();
        }
    }
}