﻿using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Flujo de Documentos de CAU
    /// </summary>
    [HasAuth("Auth", "AuthBackoffice")]
    public class documentFlowController : ApiController
    {
        public async Task<IHttpActionResult> get(string id)
        {
            int document_id;
            if (int.TryParse(id, out document_id))
            {
                ACDocumentState state = new ACDocumentState();
                List<ACDocumentState> flow = state.flow(document_id);
                return Ok(flow);
            }
            return NotFound();
        }
    }
}