﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    [HasAuth("AuthBackoffice")]
    public class ProofOfGraduationController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // POST: api/AtentionCenter/ProofOfGraduation
        public async Task<IHttpActionResult> Post(Data data)
        {
            /**
             * string to DateTime
             * Usamos string 'Date' para el valor de viene del frontend debido a que si se usa DateTime
             * se ingresará 05:00 horas (valor que ingresa el datepicker) en lugar de 00:00
             */
            var date = DateTime.Parse(data.Date);
            var graduates = data.Graduates.ToArray();
            var acAuth = (ACAuth) _httpSessionState["AuthBackoffice"];

            return Ok(await ProofOfGraduation.GetProofOfGraduation(graduates, date, acAuth.id));
        }

        public class Data
        {
            public string Date { get; set; }
            public ProofOfGraduation[] Graduates { get; set; }
        }
    }
}