﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACBusiness.AtentionCenter.Documents;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api para Constancia de 3ro, 5to y 10mo Superior por Periodo
    /// </summary>
    [HasAuth("Auth", "AuthBackoffice")]
    public class proofOf3510PerPeriodController : ApiController
    {
        private IHttpSessionState session = 
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            ProofOf3510PerPeriod doc = new ProofOf3510PerPeriod(auth_obj.profile);
            await doc.validate();
            return Ok(doc);
        }
        public async Task<IHttpActionResult> post([FromBody]Data data)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            ProofOf3510PerPeriod doc = new ProofOf3510PerPeriod(auth_obj.profile);
            Student student = new Student(auth_obj.person_id);
            ACRequest request = await doc.createRequest(
                student, auth_obj.profile, data.period_list, 
                data.description, auth_obj.id);
            if (ACBusiness.AtentionCenter.ACRequest.isNull(request))
                return InternalServerError();
            return Ok(request);
        }

        public async Task<IHttpActionResult> put([FromBody]DataBatch data) {
            ACAuth auth_obj = (ACAuth)session["AuthBackoffice"];
            ProofOf3510PerPeriod obj_proff = new ProofOf3510PerPeriod();
            return Ok(await obj_proff.generateBatch(data.student, data.term, auth_obj.id));
        }

        //public async Task<IHttpActionResult> post([FromBody]Graduated student) {        }

        public class Data
        {
            public string description { get; set; }
            public List<string> period_list { get; set; }
        }

        public class DataBatch
        {
            public string term { get; set; }
            public Graduated student { get; set; }
        }
    }
}
