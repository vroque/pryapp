﻿using ACAccess.Authorization;
using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api para listar solicitudes que estan en un determinado estado
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class requestByStateController : ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Obtiene las solicitudes de una area
        /// **Generalmente es estado -> Derivado/Pendiente de Aprovación
        /// </summary>
        /// <param name="id">Area ID</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> get(int id)
        {
            //solicitudes para areas
            ACAuth auth_obj = (ACAuth)session["AuthBackoffice"];
            List<ACRequest> list = await ACRequest.listByState(auth_obj.campus.id, id);
            return Ok(list);
        }
    }
}