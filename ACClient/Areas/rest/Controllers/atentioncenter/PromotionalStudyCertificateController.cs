﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter.Documents;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <inheritdoc />
    /// <summary>
    /// API: Certificado de Estudios Promocional
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class PromotionalStudyCertificateController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        private readonly ACAuth _backofficeAuth =
            (ACAuth) SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current)["AuthBackoffice"];

        /// <summary>
        /// GET: api/AtentionCenter/PromotionalStudiesCertificate
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get()
        {
            return Ok(HttpStatusCode.NotImplemented);
        }

        /// <summary>
        /// GET: api/AtentionCenter/PromotionalStudiesCertificate/{id}
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get(string id)
        {
            id = id.ToLower();
            switch (id)
            {
                case "request":
                    return Ok(
                        await PromotionalStudyCertificate.RequestListByCampusAsync(_backofficeAuth.campus.id));
                default:
                    return Ok(HttpStatusCode.NoContent);
            }
        }

        /// <summary>
        /// POST: api/AtentionCenter/PromotionalStudiesCertificate
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Post(Data data)
        {
            var acAuth = (ACAuth) _httpSessionState["AuthBackoffice"];
            var type = data.Type;

            switch (type)
            {
                // Crea Solicitud derivando a Registros Académicos para su aprobación
                case "createRequest":
                    return Ok(await new PromotionalStudyCertificate().GenerateRequestAsync(data.Students, acAuth.id));
                default:
                    return Ok(HttpStatusCode.NoContent);
            }
        }

        public class Data
        {
            public string Type { get; set; }
            public Graduated[] Students { get; set; }
        }
    }
}