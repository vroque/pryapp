﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACBusiness.AtentionCenter.Documents;
using ACBusiness.Convalidations;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using static ACBusiness.AtentionCenter.Documents.ConvalidationExtensionResolution;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api para solicitud de convalidación
    /// </summary>
    [HasAuth("Auth")]
    public class convalidationExtensionController: ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            ConvalidationExtensionResolution doc = new ConvalidationExtensionResolution(auth_obj.profile);
            doc.validate();
            return Ok(doc);
        }
        public async Task<IHttpActionResult> post([FromBody]Data data)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            ConvalidationExtensionResolution doc = new ConvalidationExtensionResolution(auth_obj.profile);
            Student student = new Student(auth_obj.person_id);
            ACRequest request = await doc.createRequest(
                student, auth_obj.profile, data.description, data.transfer, data.courses, auth_obj.id);
            if (ACRequest.isNull(request))
                return InternalServerError();
            return Ok(request);
        }

        public class Data
        {
            public string description { get; set; }
            public TranferAbstact transfer { get; set; }
            public List<object> courses { get; set; }
        }

    }
}