﻿using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    [HasAuth("AuthBackoffice")]
    public class requestAbstractController : ApiController
    {
        // private IHttpSessionState session = SessionStateUtility
        //         .GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// inserta o actualiza el campo descriptcion para 
        /// agregar los detalles cualquiera
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> put(int id, [FromBody] List<ACReqData> data)
        {
            ACRequest req = await ACRequest.get(id);
            JObject json_obj = JObject.Parse(req.description);
            foreach (ACReqData item in data)
                json_obj[item.name] = item.value;
            req.description = json_obj.ToString();
            int i = await req.update();
            if (i > 0)
                return Ok(new { description = req.description });
            return InternalServerError();
        }
    }

    public class ACReqData
    {
        public string name { get; set; }
        public string value { get; set; }

    }
}