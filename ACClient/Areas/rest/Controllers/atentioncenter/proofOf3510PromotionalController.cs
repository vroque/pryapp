﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACBusiness.AtentionCenter.Documents;
using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Constancia de 3ro, 5to y 10mo Superior promocional
    /// </summary>
    [HasAuth("Auth", "AuthBackoffice")]
    public class proofOf3510PromotionalController : ApiController
    {
        private IHttpSessionState session = SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            ProofOf3510Promotional doc = new ProofOf3510Promotional(auth_obj.profile);
            await doc.validate();
            return Ok(doc);
        }
        public async Task<IHttpActionResult> post([FromBody]Data data)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            ProofOf3510Promotional doc = new ProofOf3510Promotional(auth_obj.profile);
            Student student = new Student(auth_obj.person_id);
            ACRequest request = await doc.createRequest(
                student, auth_obj.profile, data.description, auth_obj.id);
            if (ACBusiness.AtentionCenter.ACRequest.isNull(request))
                return InternalServerError();
            return Ok(request);
        }

        [HasModule("bo_rau-personal")]
        public async Task<IHttpActionResult> put([FromBody]Graduated student)
        {
            ACAuth authObj = (ACAuth)session["AuthBackoffice"];
            ProofOf3510Promotional objProff = new ProofOf3510Promotional();
            return Ok(await objProff.GenerateBatch(student, authObj.id));
        }

        public class Data
        {
            public string description { get; set; }
        }
    }
}
