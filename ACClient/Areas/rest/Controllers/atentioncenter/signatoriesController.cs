﻿using ACBusiness.AtentionCenter;
using System.Collections.Generic;
using System.Web.Http;
using ACClient.Helpers.ActionFilters;
using ACBusiness.Academic;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <inheritdoc />
    /// <summary>
    /// API para Firmas
    /// </summary>
    [HasAuth("Auth", "AuthBackoffice")]
    public class signatoriesController : ApiController
    {
        public IHttpActionResult get(int id, [FromUri] string program) // , [FromUri] string college
        {
            var acSignatory = new ACSignatory();
            var college = College.getByProgramID(program);
            var list = acSignatory.listSignatoriesNames(id, college?.id);
            return Ok(list);
        }
    }
}