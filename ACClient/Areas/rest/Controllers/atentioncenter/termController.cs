﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <url> api/atencioncenter/term</url>
    /// <summary>
    /// 
    /// </summary>
    [HasAuth("Auth", "AuthBackoffice")]
    public class termController : ApiController
    {
        //private IHttpSessionState session =
        //    SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Devuelve el periodo actual de matrícula para un estudiante
        /// en base a la fecha actual y su campus y departament
        /// </summary>
        /// <returns></returns>
        [HttpGet, ActionName("GetTermCurrentEnrollment")]
        public IHttpActionResult GetTermCurrentEnrollment(string campus, string departament)
        {
            ACTerm term = new ACTerm().getCurrentEnrollmentByFechEnroll(campus, departament);
            return Ok(term);
        }

        /// <summary>
        /// Lista todos los periodos
        /// </summary>
        /// <returns>StudiesProof object</returns>
        public async Task<IHttpActionResult> get()
        {
            List<ACTerm> terms = await ACTerm.query();

            return Ok(terms);
        }

        public async Task<IHttpActionResult> put([FromBody] ACTerm term)
        {
            bool t = await term.update();
            if(t)
                return Ok(term);
            return InternalServerError();
        }

        public async Task<IHttpActionResult> post([FromBody] ACTerm term)
        {
            bool t = await term.create();
            if (t)
                return Ok(term);
            return InternalServerError();
        }
    }
}