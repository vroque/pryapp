﻿using ACAccess.Authorization;
using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api para Generar PDF
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class generateDocumentController : ApiController
    {
        private IHttpSessionState session =
          SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// GEnerar el PDF del Documento
        /// * o devolver el pdf ya generado
        /// </summary>
        /// <param name="id">Codigo de Solicitus</param>
        /// <param name="ext">parametros adicionales </param>
        /// <returns></returns>
        public async Task<IHttpActionResult> post(int id, Extends ext)
        {
            if (ext.term == null && ext.index == 0)
                ext = new Extends() { term = "", index = 1};
            ACAuth auth_obj = (ACAuth)session["AuthBackoffice"];
            ACDocument doc_object = new ACDocument();

            string doc_path = await doc_object.generateDocument(id, auth_obj.id, ext.term, ext.index);
            if (doc_path != null)
                return Ok(new { status = "OK", path = doc_path });
            return NotFound();
        }
    }
    public class Extends {
        public string term { get; set; }
        public int index { get; set; }
    }

}