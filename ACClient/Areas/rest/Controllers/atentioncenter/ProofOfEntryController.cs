using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.AtentionCenter.Documents;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class ProofOfEntryController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        
        // POST: api/AtentionCenter/ProofOfEntry
        public async Task<IHttpActionResult> Post(Data data)
        {
            var type = data.Type;
            var students = data.Students.ToArray();
            
            var casAuth = (ACAuth) _httpSessionState["AuthBackoffice"];
            switch (type)
            {
                // Constancia de Ingreso Promocional
                case "promotional":
                    return Ok(await PromotionalProofOfEntry.GenerateProofOfEntry(students, casAuth.id));
                default:
                    return Ok(HttpStatusCode.NotFound);
            }
        }
        
        public class Data
        {
            public string Type { get; set; }
            public PromotionalProofOfEntry[] Students { get; set; }
        }
    }
}