﻿using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter.Documents;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <inheritdoc />
    /// <summary>
    /// Boleta de Notas PRONABEC
    /// </summary>
    public class PronabecReportCardController : ApiController
    {
        private readonly IHttpSessionState _httpSessionState =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        // POST: api/AtentionCenter/PronabecReportCard
        public async Task<IHttpActionResult> Post(Data data)
        {
            var type = data.Type;
            var acAuth = (ACAuth) _httpSessionState["AuthBackoffice"];

            switch (type)
            {
                case "pronabec":
                    return Ok(await new PronabecReportCard().GenerateAsync(data.ScholarshipStudents,
                        data.PronabecType, data.SchoolId, data.AcademicPeriod, acAuth.id));
                default:
                    return Ok(HttpStatusCode.NotFound);
            }
        }

        public class Data
        {
            public string Type { get; set; }
            public ScholarshipStudent[] ScholarshipStudents { get; set; }
            public string PronabecType { get; set; }
            public string SchoolId { get; set; }
            public string AcademicPeriod { get; set; }
        }
    }
}