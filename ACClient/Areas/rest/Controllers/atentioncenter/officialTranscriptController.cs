﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACBusiness.AtentionCenter.Documents;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api para Historial Académico (A.K.A Record Académico)
    /// </summary>
    [HasAuth("Auth", "AuthBackoffice")]
    public class officialTranscriptController : ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> get()
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            OfficialTranscript doc = new OfficialTranscript(auth_obj.profile);
            await doc.validate();
            return Ok(doc);
        }
        public async Task<IHttpActionResult> post([FromBody]Data data)
        {
            ACAuth auth_obj = (ACAuth)session["Auth"];
            OfficialTranscript doc = new OfficialTranscript(auth_obj.profile);
            Student student = new Student(auth_obj.person_id);
            ACRequest request = await doc.createRequest(
                student, auth_obj.profile, data.description, auth_obj.id);
            if (ACBusiness.AtentionCenter.ACRequest.isNull(request))
                return InternalServerError();
            return Ok(request);
        }

        [HasModule("bo_rau-personal")]
        public async Task<IHttpActionResult> put([FromBody]Graduated student)
        {
            ACAuth authObj = (ACAuth)session["AuthBackoffice"];
            OfficialTranscript doc = new OfficialTranscript();
            return Ok(await doc.GenerateBatch(student, authObj.id));
        }

        public class Data
        {
            public string description { get; set; }
            //public List<string> period_list { get; set; }
        }
    }
}
