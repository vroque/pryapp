﻿using ACBusiness.AtentionCenter;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class requirementController : ApiController
    {
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public async Task<IHttpActionResult> get(int id, [FromUri]int request_id = 0)
        {
            ACRequirement r = new ACRequirement();
            List<ACRequirement> requirements;

            if (request_id == 0)
                requirements = await r.getByDocument(id);
            else
                requirements = await r.getByRequest(request_id, id);

            return Ok(requirements);
        }
    }
}