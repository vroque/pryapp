﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACBusiness.AtentionCenter;
using ACBusiness.AtentionCenter.Documents.Graduates;
using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.atentioncenter
{
    /// <summary>
    /// Api para duplicado de constancia de egresado
    /// </summary>

    [HasAuth("Auth")]
    public class DuplicateProofGraduateController: ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Obtener información de documento
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> get() {
            ACAuth authObj = (ACAuth)session["Auth"];
            DuplicateProofGraduate document = new DuplicateProofGraduate(authObj.profile);
            return Ok(document);
        }

        /// <summary>
        /// Crear Solicitud
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> post([FromBody]Data data)
        {
            ACAuth authObj = (ACAuth)session["Auth"];
            DuplicateProofGraduate document = new DuplicateProofGraduate(authObj.profile);
            Student student = new Student(authObj.person_id);
            ACRequest request = await document.createRequest(
                student, authObj.profile, data.description, authObj.id);
            if (ACRequest.isNull(request))
                return InternalServerError();
            return Ok(request);
        }


        public class Data
        {
            public string description { get; set; }
        }
    }
}