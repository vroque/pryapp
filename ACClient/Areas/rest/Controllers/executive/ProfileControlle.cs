﻿using ACAccess.Authorization;
using ACBusiness.Institutional;
using ACBusiness.Personal;
using ACClient.Helpers.ActionFilters;
using System;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.executive
{
    [HasAuth("AuthBackoffice")]
    public class ProfileController : ApiController
    {
        
        public IHttpActionResult Post(string id)
        {
             IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

            //obtener los datos del alumno logeado
            ACAuth auth_obj = (ACAuth)session["AuthBackoffice"];
            auth_obj.campus = Campus.get(id);
            auth_obj.module_list = new Module().getByUser(auth_obj.id, auth_obj.campus.id);
            session["AuthBackoffice"] = auth_obj;
            return Ok(new { status = true });
        }

    }
}