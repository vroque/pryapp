﻿using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Settings;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.Settings
{
    [HasSession("AuthBackoffice", "Auth")]
    public class TermController : ApiController
    {
        // GET: api/Settings/Term
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await new Term().GetAllTermListAsync());
        }

        //POST: api/Settings/Term
        [HasSession("AuthBackoffice")]
        public async Task<IHttpActionResult> Post(Term term)
        {
            return Ok(await new Term(term).CreateAsync());
        }
        
        // PUT: api/Settings/Term
        [HasSession("AuthBackoffice")]
        public async Task<IHttpActionResult> Put(Term term)
        {
            return Ok(await new Term(term).UpdateAsync());
        }

        // DELETE: api/Settings/Term/:huk
        [HasSession("AuthBackoffice")]
        public async Task<IHttpActionResult> Delete(string huk)
        {
            return Ok(await new Term().DeleteAsync(huk));
        }
    }
}