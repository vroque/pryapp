﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.TermsAndConditions;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.Settings
{
    [HasSession("AuthBackoffice")]
    public class TermsAndConditionsController : ApiController
    {
        // GET: api/Settings/TermsAndConditions
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await new TermsAndConditions().GetTermsListAsync());
        }

        // GET: api/Settings/TermsAndConditions/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await new TermsAndConditions().GetByIdAsync(huk));
        }

        // GET: api/Settings/TermsAndConditions/:huk/:iskay
        public async Task<IHttpActionResult> Get(string huk, string iskay)
        {
            switch (iskay)
            {
                case "module":
                    return Ok(await new TermsAndConditions().GetModuleTermsAsync());
                default:
                    return Ok(HttpStatusCode.NotFound);
            }
        }

        // POST: api/Settings/TermsAndConditions
        public async Task<IHttpActionResult> Post(TermsAndConditions termsAndConditions)
        {
            return Ok(await new TermsAndConditions(termsAndConditions).CreateAsync());
        }

        // PUT: api/Settings/TermsAndConditions
        public async Task<IHttpActionResult> Put(TermsAndConditions termsAndConditions)
        {
            return Ok(await new TermsAndConditions(termsAndConditions).UpdateAsync());
        }

        // DELETE: api/Settings/TermsAndConditions/:huk
        public async Task<IHttpActionResult> Delete(int huk)
        {
            return Ok(await new TermsAndConditions().DeleteAsync(huk));
        }
    }
}