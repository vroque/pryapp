using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Settings;
using ACClient.Helpers.ActionFilters;

//using _config = ACBusiness.Settings;

namespace ACClient.Areas.rest.Controllers.Settings
{
    [HasSession("AuthBackoffice")]
    public class SettingsDetailController : ApiController
    {
        // GET: api/Settings/SettingsDetail
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await new SettingsDetail().GetAsync());
        }
        
        // GET: api/Settings/SettingsDetail/:huk
        public async Task<IHttpActionResult> Get(int huk)
        {
            return Ok(await new SettingsDetail().GetByIdAsync(huk));
        }
        
        // GET: api/Settings/SettingsDetail/:huk/:iskay
        public async Task<IHttpActionResult> Get(string huk, string iskay)
        {
            switch (iskay)
            {
                case "terms-enrollment":
                    return Ok(await new SettingsDetail().GetTermsForEnrollment());
                case "terms-enroll-act-ext":
                    return Ok(await new SettingsDetail().GetTermsForExtracurricular());
                default:
                    return Ok(HttpStatusCode.NotFound);
            }
        }
        
        // POST: api/Settings/SettingsDetail
        public async Task<IHttpActionResult> Post(SettingsDetail settingsDetail)
        {
            return Ok(await new SettingsDetail(settingsDetail).CreateAsync());
        }
        
        // PUT: api/Settings/SettingsDetail
        public async Task<IHttpActionResult> Put(SettingsDetail settingsDetail)
        {
            return Ok(await new SettingsDetail(settingsDetail).UpdateAsync());
        }
        
        // DELETE: api/Settings/SettingsDetail/:huk
        public async Task<IHttpActionResult> Delete(int huk)
        {
            return Ok(await new SettingsDetail().DeleteAsync(huk));
        }
    }
}