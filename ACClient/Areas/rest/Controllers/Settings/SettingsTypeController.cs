﻿using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Settings;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.Settings
{
    [HasSession("AuthBackoffice")]
    public class SettingsTypeController : ApiController
    {
        // GET: api/Settings/SettingsType
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await new SettingsType().GetSettingsTypeListAsync());
        }
        
        // GET: api/Settings/SettingsType/:huk
        public IHttpActionResult Get(int huk)
        {
            return Ok(new SettingsType().GetById(huk));
        }

        // POST: api/Settings/SettingsType
        public async Task<IHttpActionResult> Post(SettingsType settingsType)
        {
            return Ok(await new SettingsType(settingsType).CreateAsync());
        }

        // PUT: api/Settings/SettingsType
        public async Task<IHttpActionResult> Put(SettingsType settingsType)
        {
            return Ok(await new SettingsType(settingsType).UpdateAsync());
        }
        
        // DELETE: api/Settings/SettingsType/:huk
        public async Task<IHttpActionResult> Delete(int huk)
        {
            return Ok(await new SettingsType().DeleteAsync(huk));
        }
    }
}