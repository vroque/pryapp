﻿using System.Threading.Tasks;
using System.Web.Http;
using ACClient.Helpers.ActionFilters;
using _config = ACBusiness.Settings;

namespace ACClient.Areas.rest.Controllers.Settings
{
    [HasSession("AuthBackoffice")]
    public class SettingsController : ApiController
    {
        // GET: api/Settings/Settings
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await new _config.Settings().GetSettingsListAsync());
        }

        // GET: api/Settings/Settings/:huk
        public IHttpActionResult Get(int huk)
        {
            return Ok(new ACBusiness.Settings.Settings().GetById(huk));
        }

        // POST: api/Settings/Settings
        public async Task<IHttpActionResult> Post(_config.Settings setting)
        {
            return Ok(await new _config.Settings(setting).CreateAsync());
        }

        // PUT: api/Settings/Settings
        public async Task<IHttpActionResult> Put(_config.Settings setting)
        {
            return Ok(await new _config.Settings(setting).UpdateAsync());
        }

        // DELETE: api/Settings/Settings/:huk
        public async Task<IHttpActionResult> Delete(int huk)
        {
            return Ok(await new _config.Settings().DeleteAsync(huk));
        }
    }
}