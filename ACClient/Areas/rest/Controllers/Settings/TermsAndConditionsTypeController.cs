using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.TermsAndConditions;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.Settings
{
    [HasSession("AuthBackoffice")]
    public class TermsAndConditionsTypeController : ApiController
    {
        // GET: api/Settings/TermsAndConditionsType
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await TermsAndConditionsType.GetAll());
        }
    }
}