﻿using System.Threading.Tasks;
using System.Web.Http;
using ACBusiness.Survey;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.Settings
{
    [HasSession("AuthBackoffice")]
    public class SurveyController : ApiController
    {
        // GET: api/Settings/Survey
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await new ACBusiness.Survey.Survey().GetSurveyListAsync());
        }

        // POST: api/Settings/Survey
        public async Task<IHttpActionResult> Post(Survey survey)
        {
            return Ok(await new Survey(survey).CreateAsync());
        }

        // PUT: api/Settings/Survey
        public async Task<IHttpActionResult> Put(Survey survey)
        {
            return Ok(await new Survey(survey).UpdateAsync());
        }

        // DELETE: api/Settings/Survey/:huk
        public async Task<IHttpActionResult> Delete(int huk)
        {
            return Ok(await new Survey().DeleteAsync(huk));
        }
    }
}