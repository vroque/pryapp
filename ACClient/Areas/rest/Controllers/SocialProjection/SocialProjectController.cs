﻿using ACBusiness.SocialProjection;
using ACClient.Helpers.ActionFilters;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.SocialProjection
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class SocialProjectController : ApiController
    {
        public IHttpActionResult Get([FromUri]int pidm)
        {
            SocialProject req = new SocialProject();
            return Ok(req.getSocialProjectionSocialByStudent(pidm));
        }
    }
}