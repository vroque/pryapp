﻿using ACBusiness.Aplication;
using ACClient.Helpers.ActionFilters;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.aplication
{
    /// <summary>
    /// comentario cute 1.1
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class groupUserFilterController : ApiController
    {
        private IHttpSessionState session =
              SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public IHttpActionResult get(int id)
        {
            return Ok(AppUser.queryFilter(id));
        }

    }
}