﻿using ACBusiness.Aplication;
using ACClient.Helpers.ActionFilters;
using System;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.aplication
{
    /// <summary>
    /// Grupos y usuarios
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class groupUserController : ApiController
    {
        /// <summary>
        /// api de grupo y usuarios 
        /// nota:Arturo es un mariquita
        /// </summary>
        private IHttpSessionState session =
           SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);
        public IHttpActionResult get(string id)
        {
            return Ok(AppModule.query());
        }   
        public IHttpActionResult delete(string id, [FromUri]AppGroupUser data)
        {
            data.update_date = DateTime.Now;
            bool v = data.remove(data.group_id, data.user_id);
            if (v)
                return Ok(data);
            else
                return InternalServerError();
        }

        public IHttpActionResult post(string id, [FromUri]AppGroupUser data)
        {
            data.update_date = DateTime.Now;
            bool v = data.add();
            if (v)
                return Ok(data);
            else
                return InternalServerError();
        }
    }
}