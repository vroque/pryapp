﻿using ACAccess.Authorization;
using ACBusiness.Aplication;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;


namespace ACClient.Areas.rest.Controllers.aplication
{
    /// <summary>
    /// GRupos y modulos de la aplicación
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class groupModuleController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public IHttpActionResult get()
        {
            return Ok(AppGroupModule.query());
        }

        public IHttpActionResult post(string id, [FromUri]Data data)
        {
            AppGroup group = new AppGroup();

            group.name = data.group_name;
            group.save();


            AppGroupModule group_module = new AppGroupModule();

            group_module.moduleID = data.module_id;
            group_module.groupID = group.id;
            group_module.campus = data.campus_id;
            group_module.updateDate = DateTime.Now;

            bool v = group_module.save();
            if (v)
                return Ok(group_module);
            else
                return InternalServerError();
        }

        public IHttpActionResult delete(string id, [FromUri]AppGroupModule data)
        {
            AppGroupModule group_module = new AppGroupModule();

            group_module.moduleID = data.moduleID;
            group_module.groupID = data.groupID;
            group_module.campus = data.campus;

            bool v = group_module.remove();
            if (v)
                return Ok(new { ok = "ok" });
            else
                return InternalServerError();
        }

        public class Data
        {
            public string group_name { get; set; }
            public string campus_id { get; set; }
            public int module_id { get; set; }
        }
    }
    
}