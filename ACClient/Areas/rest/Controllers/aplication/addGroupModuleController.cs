﻿using ACAccess.Authorization;
using ACBusiness.Aplication;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;


namespace ACClient.Areas.rest.Controllers.aplication
{
    /// <summary>
    /// Agregar modulos agrupos
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class addGroupModuleController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public IHttpActionResult post(string id, [FromUri]Data data)
        {

            AppGroupModule group_module = new AppGroupModule();

            group_module.moduleID = data.module_id;
            group_module.groupID = data.group_id;
            group_module.campus = data.campus_id;
            group_module.updateDate = DateTime.Now;

            bool v = group_module.save();
            if (v)
                return Ok(group_module);
            else
                return InternalServerError();
        }
        public class Data
        {
            public int group_id { get; set; }
            public string campus_id { get; set; }
            public int module_id { get; set; }
        }
    }

}