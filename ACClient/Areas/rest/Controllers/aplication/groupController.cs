﻿using ACBusiness.Aplication;
using ACClient.Helpers.ActionFilters;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.aplication
{
    /// <summary>
    /// Grupos de la aplicación
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class groupController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public IHttpActionResult get()
        {
            return Ok(AppGroup.query());
        }

        public async Task<IHttpActionResult> get(string id,[FromUri]Data data)
        {
            //CASAuth auth_obj = (CASAuth)session["Auth"];
          
            return Ok(AppGroup.queryFilter(data.module_id, data.campus_id));
        }

        public class Data
        {
            public string campus_id { get; set; }
            public int module_id { get; set; }
        }
    }
}