﻿using ACBusiness.Aplication;
using ACClient.Helpers.ActionFilters;
using System;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.aplication
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_cau-administrativo")]
    public class moduleController : ApiController
    {
        /// <summary>
        /// Modulos de Backoffice por defecto
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult get()
        {
            return Ok(AppModule.query());
        }

        /// <summary>
        /// Obtiene el modulo por tipo *fd o *bo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IHttpActionResult get(string id)
        {
            if (id != "fd") return Ok(AppModule.query());
            return NotFound();
        }

        /// <summary>
        /// Crear un nuevo modulo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public IHttpActionResult post(string id, [FromUri] AppModule data)
        {
            data.app_id = 1;
            data.update_date = DateTime.Now;

            bool v = data.create();
            if (v) return Ok(data);
            
            return InternalServerError();
        }
    }
}