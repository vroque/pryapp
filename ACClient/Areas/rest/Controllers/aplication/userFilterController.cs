﻿using ACAccess.Authorization;
using ACBusiness.Aplication;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;

namespace ACClient.Areas.rest.Controllers.aplication
{
    /// <summary>
    /// comentario cute 1.1
    /// </summary>
    [HasAuth("AuthBackoffice")]
    public class userFilterController : ApiController
    {
        private IHttpSessionState session =
              SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        public IHttpActionResult get(string id)
        {
            return Ok(AppUser.queryFilterName(id));
        }
    }
}