﻿using ACAccess.Authorization;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.aplication
{
    [HasAuth("AuthBackoffice")]
    [HasModule("bo_cau-administrativo")]
    public class menuController : ApiController
    {
        /// <summary>
        /// lista los menus por ID de modulo
        /// </summary>
        /// <returns> lista de menus</returns>
        public IHttpActionResult get([FromUri] int module)
        {
            List<Menu> menus = new Menu().listFromModule(module);
            return Ok(menus);
        }

        /// <summary>
        /// Crear un nuevo menu
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public IHttpActionResult post([FromBody] Menu menu)
        {
            if (menu.create())
                return Ok(menu);
            return InternalServerError();
        }

        /// <summary>
        /// Editar un menu por su id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="menu"></param>
        /// <returns></returns>
        public IHttpActionResult put([FromBody] Menu menu)
        {
            if (menu.update()) return Ok(menu);
            return InternalServerError();
        }

        /// <summary>
        /// Eliminar un menu por su id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="menu"></param>
        /// <returns></returns>
        public IHttpActionResult delete([FromBody] Menu menu)
        {
            if (menu.delete())
                return Ok(new {status = "ok"});
            return InternalServerError();
        }

        //public IHttpActionResult get(string id)
        //{
        //    Menu menus = new Menu().get(id)
        //    return Ok(menu);
        //}
    }
}