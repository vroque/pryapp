﻿using ACBusiness.Reports;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.report
{
    [HasAuth("AuthBackoffice")]
    public class DocumentSignerController : ApiController
    {
        [HasSession("AuthBackoffice")]
        public IHttpActionResult Get(decimal id)
        {
            DocumentSigners ds = new DocumentSigners();
            return Ok(ds.listByPerson(id));
        }
    }
}