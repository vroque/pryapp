﻿using ACBusiness.Reports;
using ACClient.Helpers.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.report
{
    [HasAuth("AuthBackoffice")]
    public class RequestController : ApiController
    {
        public IHttpActionResult Get()
        {
            RequestReport rr = new RequestReport();
            return Ok(rr.all());
        }
    }
}