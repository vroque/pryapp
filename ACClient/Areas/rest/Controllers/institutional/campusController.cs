﻿using ACBusiness.Institutional;
using ACClient.Helpers.ActionFilters;
using System.Web.Http;

namespace ACClient.Areas.rest.Controllers.institutional
{
    [HasAuth("Auth", "AuthBackoffice")]
    public class campusController : ApiController
    {
        // GET: api/institutional/campus
        public IHttpActionResult get()
        {
            return Ok(Campus.query());
        }
    }
}