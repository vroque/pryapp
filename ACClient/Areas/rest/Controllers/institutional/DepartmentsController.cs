﻿using ACBusiness.Institutional;
using System.Threading.Tasks;
using System.Web.Http;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.institutional
{
    // API: api/institutional/Departaments

    [HasAuth("Auth", "AuthBackoffice")]
    public class DepartamentsController : ApiController
    {
        /// <summary>
        /// Obtiene la lista de modalidades activas
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await Department.GetAllAsync());
        }
    }
}