﻿using ACBusiness.Admision;
using System.Web.Http;
using ACClient.Helpers.ActionFilters;

namespace ACClient.Areas.rest.Controllers.admision
{
    /// <summary>
    /// Servicio de Requisitos de Admisión
    /// </summary>
    [HasAuth("Auth", "AuthBackoffice")]
    public class requerimentController : ApiController
    {
        /// <summary>
        /// Obtiene los requisitos de admision segun el perfil de estudiante
        /// </summary>
        /// <uri>.../api/admision/requeriment</uri>
        /// <param name="profile"></param>
        /// <returns></returns>
        public IHttpActionResult Get([FromUri] Profile profile, [FromUri] bool all)
        {
            var req = new Requirement();
            return Ok(all
                ? req.GetAllByStudent(profile.person_id, profile.department, profile.program)
                : req.GetPendingByStudent(profile.person_id, profile.department, profile.program));
        }

        public class Profile
        {
            public string department { get; set; }
            public string program { get; set; }
            public string college { get; set; }
            public decimal person_id { get; set; }
        }
    }
}