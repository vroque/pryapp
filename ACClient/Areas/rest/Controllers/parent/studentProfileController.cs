﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using _acad = ACTools.Constants.AcademicConstants;
using _app = ACTools.Constants.AppConstants;


namespace ACClient.Areas.rest.Controllers.parent
{

    /// <summary>
    /// Api para el perfil academico de un alumno
    /// </summary>
    [HasSession("AuthParent")]
    public class studentProfileController : ApiController
    {
        private IHttpSessionState session =
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current);

        /// <summary>
        /// Envia datos y setea el perfil de un estudiantes hijo/aporante
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public IHttpActionResult Post([FromBody] Data data)
        {
            ACAuth auth = (ACAuth)session["AuthParent"];
            // verificar si el pidm de data es un child del padre
            // FIXME

            
            Student student = new Student(data.student_id);
            student.syncProfile(data.div, data.levl, data.program);
            ACAuth student_obj = new ACAuth();
            student_obj.campus = student.profile.campus;
            student_obj.div = student.profile.div;
            student_obj.first_name = student.first_name;
            student_obj.id = student.id;
            student_obj.last_name = student.last_name;
            student_obj.person_id = student.person_id;
            student_obj.profile = student.profile;
            if (student_obj.type == null) student_obj.type = new List<string>();
            student_obj.type.Add(_app.TYPE_STUDENT);

            student_obj.module_list = new Module().getByStudent(student_obj.profile);
            
            session.Add("AuthChild", student_obj);
            return Ok(new { status = true });
        }

        public class Data
        {
            public string div { get; set; }
            public string levl { get; set; }
            public string program { get; set; }
            public decimal student_id { get; set; }
        }
    }
}
