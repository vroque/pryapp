using System.Web.Http;
using System.Web.Mvc;

namespace ACClient.Areas.rest.Registration
{
    public class AdryanRegistration : AreaRegistration
    {
        public override string AreaName => "Adryan";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                name: "AdryanDefaultApi",
                routeTemplate: "api/" + AreaName + "/{controller}/{huk}/{iskay}/{kimsa}/{tawa}/{pichqa}",
                defaults: new
                {
                    area = AreaName,
                    huk = RouteParameter.Optional,
                    iskay = RouteParameter.Optional,
                    kimsa = RouteParameter.Optional,
                    tawa = RouteParameter.Optional,
                    pichqa = RouteParameter.Optional
                });
        }
    }
}
