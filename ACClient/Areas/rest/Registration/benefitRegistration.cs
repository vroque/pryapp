﻿using System;
using System.Web.Http;
using System.Web.Mvc;

namespace ACClient.Areas.rest.Registration
{
    public class benefitRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get
            {
                return "benefit";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                name: "BenefitDefaultApi",
                routeTemplate: "api/" + AreaName + "/{controller}/{id}",
                defaults: new { area = AreaName, id = RouteParameter.Optional }
            );

        }
    }
}