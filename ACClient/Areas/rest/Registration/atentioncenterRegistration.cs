﻿using System;
using System.Web.Http;
using System.Web.Mvc;

namespace ACClient.Areas.rest.Registration
{
    public class atentioncenterRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get
            {
                return "atentioncenter";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                name: "GetTermCurrentEnrollmentTermApi",
                routeTemplate: "api/" + AreaName + "/term/term-current-enrollment/{campus}/{departament}",
                defaults: new { area = AreaName, controller = "Term", action = "GetTermCurrentEnrollment" }
            );

            context.MapHttpRoute(
                name: "AtentionCenterDefaultApi",
                routeTemplate: "api/" + AreaName + "/{controller}/{id}",
                defaults: new { area = AreaName, id = RouteParameter.Optional }
            );
        }
    }
}