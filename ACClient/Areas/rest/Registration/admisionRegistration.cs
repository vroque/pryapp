﻿using System;
using System.Web.Http;
using System.Web.Mvc;

namespace ACClient.Areas.rest.Registration
{
    public class admisionRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get
            {
                return "admision";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                name: "AdmisionDefaultApi",
                routeTemplate: "api/" + AreaName + "/{controller}/{id}",
                defaults: new { area = AreaName, id = RouteParameter.Optional }
            );

        }
    }
}