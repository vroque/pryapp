﻿using System.Web.Http;
using System.Web.Mvc;

namespace ACClient.Areas.rest.Registration
{
    public class ContinentalProjectRegistration: AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ContinentalProject";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                name: "ContinentalProjectDefaultApi",
                routeTemplate: "api/" + AreaName + "/{controller}/{id}",
                defaults: new { area = AreaName, id = RouteParameter.Optional }
            );
        }
    }
}
