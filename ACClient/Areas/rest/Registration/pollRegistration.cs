﻿using System.Web.Http;
using System.Web.Mvc;

namespace ACClient.Areas.rest.Registration
{
    public class pollRegistration: AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "poll";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                name: "PollDefaultApi",
                routeTemplate: "api/" + AreaName + "/{controller}/{id}",
                defaults: new { area = AreaName, id = RouteParameter.Optional }
            );

        }
    }
}