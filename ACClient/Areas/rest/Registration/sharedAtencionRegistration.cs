﻿using System.Web.Http;
using System.Web.Mvc;

namespace ACClient.Areas.rest.Registration
{
    public class restAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "shared";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapHttpRoute(
                name: "RestSharedApi",
                routeTemplate:  "api/"+AreaName+"/{controller}/{id}",
                defaults: new { area = AreaName, id = RouteParameter.Optional }
            );  

        }
    }
}