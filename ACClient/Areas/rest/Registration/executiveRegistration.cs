﻿using System;
using System.Web.Http;
using System.Web.Mvc;

namespace ACClient.Areas.rest.Registration
{
    public class executiveRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get
            {
                return "executive";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                name: "ExecutiveDefaultApi",
                routeTemplate: "api/" + AreaName + "/{controller}/{id}",
                defaults: new { area = AreaName, id = RouteParameter.Optional }
            );

        }
    }
}