﻿using System;
using System.Web.Http;
using System.Web.Mvc;


namespace ACClient.Areas.rest.Registration
{
    public class aplicationRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "aplication";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                name: "aplicationDefaultApi",
                routeTemplate: "api/" + AreaName + "/{controller}/{id}",
                defaults: new { area = AreaName, id = RouteParameter.Optional }
            );
        }
    }
}