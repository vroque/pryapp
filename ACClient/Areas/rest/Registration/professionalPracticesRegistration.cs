﻿using System.Web.Http;
using System.Web.Mvc;


namespace ACClient.Areas.rest.Registration
{
    public class professionalPracticesRegistration: AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "professionalpractices";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                name: "ProfessionalPracticesDefaultApi",
                routeTemplate: "api/" + AreaName + "/{controller}/{id}",
                defaults: new { area = AreaName, id = RouteParameter.Optional }
            );

        }


    }
}