﻿using System.Web.Http;
using System.Web.Mvc;

namespace ACClient.Areas.rest.Registration
{
    public class ExtracurricularActivitiesRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExtracurricularActivities";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiChangeVisibility",
                routeTemplate: "api/" + AreaName + "/Activities/Desactivate/{id}",
                defaults: new { area = AreaName, controller = "Activities", action = "Desactivate" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiGetAttendance",
                routeTemplate: "api/" + AreaName + "/AccreditedStudent/GetAttendance/{pidm}/{term}",
                defaults: new { area = AreaName, controller = "AccreditedStudent", action = "GetAttendance" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiProcessIsReplicated",
                routeTemplate: "api/" + AreaName + "/Activities/ProcessIsReplicated",
                defaults: new { area = AreaName, controller = "Activities", action = "ProcessIsReplicated" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiProcessCreateActivity",
                routeTemplate: "api/" + AreaName + "/Activities/CreateActivity",
                defaults: new { area = AreaName, controller = "Activities", action = "CreateActivity" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiProcessCreateActivityProgramming",
                routeTemplate: "api/" + AreaName + "/ProgrammingActivity/CreateActivityProgramming",
                defaults: new { area = AreaName, controller = "ProgrammingActivity", action = "CreateActivityProgramming" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiReplicateActivitySchedules",
                routeTemplate: "api/" + AreaName + "/ProgrammingActivity/ReplicateActivitySchedules",
                defaults: new { area = AreaName, controller = "ProgrammingActivity", action = "ReplicateActivitySchedules" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiGetWorkshopActivities",
                routeTemplate: "api/" + AreaName + "/Activities/GetWorkshopActivities",
                defaults: new { area = AreaName, controller = "Activities", action = "GetWorkshopActivities" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiGetProgrammingActivity",
                routeTemplate: "api/" + AreaName + "/ProgrammingActivity/GetAll",
                defaults: new { area = AreaName, controller = "ProgrammingActivity", action = "GetAll" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiGetProgrammingWorkshopActivity",
                routeTemplate: "api/" + AreaName + "/ProgrammingActivity/GetProgrammingWorkshopActivity",
                defaults: new { area = AreaName, controller = "ProgrammingActivity", action = "GetProgrammingWorkshopActivity" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiGetActivityByShortName",
                routeTemplate: "api/" + AreaName + "/Activities/GetByShortName/{shortname}",
                defaults: new { area = AreaName, controller = "Activities", action = "GetByShortName" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiGetEnrolledStudents",
                routeTemplate: "api/" + AreaName + "/EnrolledStudents/GetRelationStudent/{idProgramming}/{idActivityType}",
                defaults: new { area = AreaName, controller = "EnrolledStudents", action = "GetRelationStudent" }
            );

            //CERTIFICADO
            context.MapHttpRoute(
              name: "ExtracurricularActivitiesDefaultApiGetCertificate",
              routeTemplate: "api/" + AreaName + "/Tracing/GetCertificate/{pidm}/{idProgrammingActivity}",
              defaults: new { area = AreaName, controller = "Tracing", action = "GetCertificate" }
            );

            context.MapHttpRoute(
               name: "ExtracurricularActivitiesDefaultApiGetActivityByActivityType",
               routeTemplate: "api/" + AreaName + "/Activities/GetByActivityType/{idActivityType}",
               defaults: new { area = AreaName, controller = "Activities", action = "GetByActivityType" }
            );
            
            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiGetAxiActivityByName",
                routeTemplate: "api/" + AreaName + "/{controller}/GetByName/{name}",
                defaults: new { area = AreaName, action = "GetByName" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiDelete",
                routeTemplate: "api/" + AreaName + "/{controller}/Desactivate/{id}",
                defaults: new { area = AreaName, action = "Desactivate" }
            );

            context.MapHttpRoute(
                name: "ExtracurricularActivitiesDefaultApiGet",
                routeTemplate: "api/" + AreaName + "/{controller}/{id}",
                defaults: new { area = AreaName, id = RouteParameter.Optional }
            );
        }
    }
}