﻿using System;
using System.Web.Http;
using System.Web.Mvc;


namespace ACClient.Areas.rest.Registration
{
    public class documentaryRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "documentary";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                name: "RestDocumentaryApi",
                routeTemplate: "api/" + AreaName + "/{controller}/{id}",
                defaults: new { area = AreaName, id = RouteParameter.Optional }
            );

        }
    }
}