﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACClient.Areas.frontdesk.Controllers

{
    public class BUWController : Controller
    {
        /// <url>/frontdesk/orientacion/bienestar/ficha-socioconomica</url>
        /// <summary>
        /// Pagina de la ficha socieconomica
        /// </summary>
        /// <param name="seccion"></param>
        /// <returns></returns> 
        [HasSession("Auth")]
        [HasProfile]
        public ActionResult Index()
        {

            ACAuth auth_obj = (ACAuth)Session["Auth"];
            List<string> js_list = new List<string>();
            js_list.Add("frontdesk/buw/config/routes.js");
            //js_list.Add("frontdesk/buw/controller/socrecord.js");
            Student student = new Student(auth_obj.id);
            ViewBag.js_list = js_list;
            ViewBag.seccion = "vida-academica";
            /* INICIO DE CIERRE DE FICHA POR PERMISOS */
            //bool has_permission = true; //FichaSocioeconomica.has_access(auth_obj.usuario_id);
            student.profile = auth_obj.profile;
            Menu menu_obj = new Menu();

            List<Menu> menus = menu_obj.getFromStudentStatus(student.profile.status);
            ViewBag.menus = menus;
            ViewBag.choices = JsonConvert.SerializeObject(menus, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }
            );
            ViewBag.student = student;
            ViewBag.person = auth_obj;
            ViewBag.profile = auth_obj.profile;
            ViewBag.title_seccion = "Mi vida universitaria";
            ViewBag.app = "frontdesk";
            return View();
        }
    }
}