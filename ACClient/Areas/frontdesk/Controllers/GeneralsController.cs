﻿using System.Collections.Generic;
using ACAccess.Authorization;
using ACBusiness.Academic;
using ACClient.Helpers.ActionFilters;
using Newtonsoft.Json;
using System.Linq;
using System.Web.Mvc;

namespace ACClient.Areas.frontdesk.Controllers
{
    public class GeneralsController : Controller
    {
        /// <url>/frontdesk</url>
        /// <summary>
        /// Pagina de inico de forntesk
        /// </summary>
        /// <returns></returns>
        [HasSession("Auth")]
        //[HasProfile]
        public ActionResult Index()
        {
            var auth_obj = (ACAuth) Session["Auth"];
            var student = new Student(auth_obj.id);
            //Trick para evitar violacion de capas con CAPermanecia
            ViewBag.profile = auth_obj.profile;
            if (ViewBag.profile == null)
            {
                if (student.profiles.Count == 0)
                    return RedirectToRoute("General_login", new {entonces = Request.Url.AbsoluteUri}); //PathAndQuery

                if (student.profiles.Count > 1)
                    return RedirectToRoute("frontdesk_set_profile", new {entonces = Request.Url.AbsoluteUri});

                auth_obj.profile = student.profiles.First();
                auth_obj.module_list = new Module().getByStudent(auth_obj.profile);
                Session["Auth"] = auth_obj;
            }

            student.profile = auth_obj.profile;
            auth_obj.campus = auth_obj.profile.campus;
            var menu_obj = new Menu();

            List<Menu> menus = menu_obj.getFromStudentStatus(student.profile.status);
            ViewBag.choices = JsonConvert.SerializeObject(menus, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }
            );
            ViewBag.student = student;
            ViewBag.person = auth_obj;
            ViewBag.profile = auth_obj.profile;
            ViewBag.sidebar = "profileStudent";
            // buscar posible administrativo logeado 
            var executive_obj = (ACAuth) Session["AuthBackoffice"];
            ViewBag.executive = executive_obj;

            // Terms and conditions
            var terms = executive_obj == null
                ? new TermsAndConditions(auth_obj).GetTermsJsonConverted()
                : new TermsAndConditions().GetTermsJsonConverted();
            ViewBag.terms = terms;
            // Surveys
            var surveys = executive_obj == null
                ? new Survey(auth_obj).GetSurveysJsonConverted()
                : new Survey().GetSurveysJsonConverted();
            ViewBag.surveys = surveys;

            return View();
        }

        /// <url>/frontdesk/set-profile</url>
        /// <summary>
        /// Pagina para cambiar el perfil de alumno
        /// en caso de un solo perfil
        /// </summary>
        /// <returns></returns>
        [HasSession("Auth")]
        public ActionResult setProfile(string seccion = "vida-academica")
        {
            Session.Remove("AuthChild");

            var auth_obj = (ACAuth) Session["Auth"];
            var student = new Student(auth_obj.id);
            var redirect = Request.QueryString["entonces"];
            ViewBag.redirect = string.IsNullOrEmpty(redirect) ? "" : redirect;

            // verificamos si tiene o no una cuenta de padre asociada, siendo esta mixta
            ViewBag.mix = false;
            var student_obj = (ACAuth) Session["AuthParent"];

            if (student_obj != null) ViewBag.mix = true;

            ViewBag.profile = auth_obj.profile;
            ViewBag.person = student;
            return View();
        }
    }
}