﻿using System.Web.Mvc;
using ACClient.Helpers.ActionFilters;
using _util = ACTools.Util;

namespace ACClient.Areas.frontdesk.Controllers
{
    [HasSession("Auth")]
    public class ExtracurricularActivitiesController : Controller
    {
        /// <summary>
        /// Imágenes de actividades
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public ActionResult GetExtracurricularActivitiesImagesActivities(string fileName)
        {
            if (_util.ExtracurricularActivities.ExistActivityImage(fileName))
            {
                return File(_util.ExtracurricularActivities.GetActivityImagePath(fileName), "image/jpeg");
            }

            return HttpNotFound();
        }

        /// <summary>
        /// Imágenes de ejes
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public ActionResult GetExtracurricularActivitiesImagesAxis(string fileName)
        {
            if (_util.ExtracurricularActivities.ExistAxiImage(fileName))
            {
                return File(_util.ExtracurricularActivities.GetAxiImagePath(fileName), "image/jpeg");
            }

            return HttpNotFound();
        }
    }
}