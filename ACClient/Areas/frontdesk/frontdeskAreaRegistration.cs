﻿using System.Web.Mvc;

namespace ACClient.Areas.frontdesk
{
    public class frontdeskAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "frontdesk";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "frontdesk",
                "frontdesk",
                new { controller = "Generals", action = "Index" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );
            context.MapRoute(
                "frontdesk_set_profile",
                "frontdesk/set-profile",
                new { controller = "Generals", action = "setProfile" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );
            // Deprecated Exceptions
            context.MapRoute(
                "frontdesk_socieconomic_record",
                "frontdesk/vida-academica/ficha-socioconomica",
                new { controller = "BUW", action = "Index" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );

            // Extracurricular Activities images activities route
            context.MapRoute("frontdesk.vuc_images_activities", "frontdesk/media/actividades-extracurriculares/images/activities/{fileName}/",
                new { controller = "ExtracurricularActivities", action = "GetExtracurricularActivitiesImagesActivities" },
                new[] { "ACClient.Areas.frontdesk.Controllers" });

            // Extracurricular Activities images axis route
            context.MapRoute("frontdesk.vuc_images_axis", "frontdesk/media/actividades-extracurriculares/images/axis/{fileName}/",
                new { controller = "ExtracurricularActivities", action = "GetExtracurricularActivitiesImagesAxis" },
                new[] { "ACClient.Areas.frontdesk.Controllers" });

            #region Admision
            context.MapRoute(
                "frontdesk_admision_test_de_motivacion",
                "frontdesk/admision/test-motivacion",
                new { controller = "ADM", action = "test_de_motivacion" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );
            context.MapRoute(
                "frontdesk_admision_test_de_interes_ocupacional",
                "frontdesk/admision/test-ocupacional",
                new { controller = "ADM", action = "test_de_interes_ocupacional" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );
            context.MapRoute(
                "frontdesk_admision_test_de_habilidades",
                "frontdesk/admision/test-habilidades",
                new { controller = "ADM", action = "test_de_habilidades" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );
            context.MapRoute(
                "frontdesk_admision_test_pendientes",
                "frontdesk/admision/test/pendientes/",
                new { controller = "ADM", action = "test_pendientes" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );

            context.MapRoute(
                "frontdesk_admision_preguntas_test_idtest_json",
                "frontdesk/adm/post/preguntas/test/idtest/",
                new { controller = "ADM", action = "Json_preguntas_by_test" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );
            context.MapRoute(
                "frontdesk_admision_update_respuesta_test_json",
                "frontdesk/adm/post/update/respuesta/test/",
                new { controller = "ADM", action = "Json_update_respuesta_test" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );
            context.MapRoute(
                "frontdesk_admision_respuesta_test_json",
                "frontdesk/adm/post/respuesta/test/",
                new { controller = "ADM", action = "Json_respuesta_test_json" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );
            context.MapRoute(
                "frontdesk_admision_estado_test_json",
                "frontdesk/adm/post/estado/test/",
                new { controller = "ADM", action = "Json_estado_test_json" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );
            #endregion

            // SPA routes
            context.MapRoute(
                "frontdesk_extend",
                "frontdesk/{*ext}",
                new { controller = "Generals", action = "Index" },
                new[] { "ACClient.Areas.frontdesk.Controllers" }
            );            
        }
    }
}