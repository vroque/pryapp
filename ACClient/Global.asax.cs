﻿using System;
using ACClient.Areas;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ACTools.Configuration;

namespace ACClient
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_PostAuthorizeRequest()
        {
            //agrega sesion a webapi
            System.Web.HttpContext.Current.SetSessionStateBehavior(
                System.Web.SessionState.SessionStateBehavior.Required);
        }

        /// <summary>
        /// Check if the app is in maintenance mode for show maintenance view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Application_BeginRequest(object sender, EventArgs e)
        {
            if (AppSettings.MaintenanceMode)
            {
                HttpContext.Current.RewritePath("maintenance");
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            // Intercepta llamadas api
            GlobalConfiguration.Configuration.Services.Replace(
                typeof(IHttpControllerSelector),
                new AreaHttpControllerSelector(GlobalConfiguration.Configuration));
        }
    }
}