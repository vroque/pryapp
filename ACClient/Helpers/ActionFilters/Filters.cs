﻿using ACAccess.Authorization;
using ACBusiness.Academic;
using System.Web;
using System.Web.Mvc;

namespace ACClient.Helpers.ActionFilters
{

    public class HasGroup : AuthorizeAttribute
    {
        private string[] _groups;
        public HasGroup(params string[] groups)
        {
            this._groups = groups;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            ACAuth auth = (ACAuth)httpContext.Session["Auth"];
            bool flag = false;
            if (auth == null || auth.group_list == null)
            {
                return flag;
            }

            foreach (string obj in _groups)
            {
                if (auth.group_list.Contains(obj))
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.HttpContext.Response.RedirectToRoute("General_inicio");
            base.HandleUnauthorizedRequest(filterContext);
        }

    }
    public class HasGroupBackOffice : AuthorizeAttribute
    {
        /// <summary>
        /// 
        ///	return Content(JsonConvert.SerializeObject(doReturn, 
        ///		new JsonSerializerSettings {
        ///			ContractResolver = new JsonIgnoreField("id")
        ///	}), "application/json");
        /// </summary>
        private string[] _groups;
        public HasGroupBackOffice(params string[] groups)
        {
            this._groups = groups;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            ACAuth authbackoffice = (ACAuth)httpContext.Session["AuthBackoffice"];
            bool flag = false;
            if (authbackoffice == null || authbackoffice.group_list == null)
            {
                return flag;
            }

            foreach (string obj in _groups)
            {
                if (authbackoffice.group_list.Contains(obj))
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.HttpContext.Response.RedirectToRoute("General_inicio");
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
    
    public class NormalizeBackoffice : AuthorizeAttribute
    {
        private string _status = "";
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            ACAuth authbackoffice = (ACAuth)httpContext.Session["AuthBackoffice"];
            if (authbackoffice != null)
            {
                if (authbackoffice.profile == null)
                {
                    this._status = "profile";
                    return false;
                }

                return true;
            }
            return false;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (this._status == "profile")
            {
                filterContext.HttpContext.Response.RedirectToRoute("backoffice_set_profile");
            }
            else
            {
                filterContext.HttpContext.Response.RedirectToRoute("General_inicio");
            }

            base.HandleUnauthorizedRequest(filterContext);
        }
    }
    public class HasSession : AuthorizeAttribute
    {
        private string[] _sessions;
        public HasSession(params string[] sessions)
        {
            this._sessions = sessions;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool flag = true;
            foreach (string key in httpContext.Session.Keys)
            {
                // Debug.Print("::::::", key);
            }
            foreach (string obj in _sessions)
            {
                if (httpContext.Session[obj] == null)
                {
                    flag = false;
                    break;
                }
                else
                {
                    //Debug.Print(httpContext.Session[auth_obj].ToString() + ":sessioon");
                }
            }
            return flag;
        }
        

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //filterContext.HttpContext.Response.Redirect("/404/");
            //filterContext.HttpContext.Response.StatusCode = 404;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.RedirectToRoute("General_login", new { entonces = filterContext.HttpContext.Request.Url.AbsoluteUri });
            filterContext.HttpContext.Response.End();
            //Response.StatusCode = 404;
            //else do normal process
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
    
    /// <summary>
    /// Valida si se tiene el perfil de estudiante seteado
    /// en caso contrario resirige a perfil
    /// </summary>
    public class HasProfile : AuthorizeAttribute
    {

        //public HasProfile(params string[] sessions)
        //{
        //    this._sessions = sessions;
        //}
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool flag = true;
            ACAuth auth_obj = (ACAuth)httpContext.Session["Auth"];
            Student student = new Student(auth_obj.id);
            //Trick para evitar violacion de capas con CAPermanecia
            var profile = auth_obj.profile;
            if (profile == null)
            {
                if (student.profiles.Count == 1)
                {
                    student.profile = student.profiles[0];
                    httpContext.Session["Auth"] = auth_obj;
                }
                else if (student.profiles.Count == 0)
                    flag = false;
                else if (student.profiles.Count > 1)
                    flag = false;
            }
            
            return flag;
        }


        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //filterContext.HttpContext.Response.Redirect("/404/");
            //filterContext.HttpContext.Response.StatusCode = 404;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.RedirectToRoute("frontdesk_set_profile", new { entonces = filterContext.HttpContext.Request.Url.AbsoluteUri });
            filterContext.HttpContext.Response.End();
            //Response.StatusCode = 404;
            //else do normal process
            base.HandleUnauthorizedRequest(filterContext);
        }
    }

    public class HasSessionOrSession : AuthorizeAttribute
    {
        private string[] _sessions;
        public HasSessionOrSession(params string[] sessions)
        {
            this._sessions = sessions;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool flag = true;
            foreach (string obj in _sessions)
            {
                if (httpContext.Session[obj] == null)
                {
                    flag = false;
                }
                else
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {

            //filterContext.HttpContext.Response.Redirect("/404/");
            filterContext.HttpContext.Response.Clear();
            if(filterContext.HttpContext.Session["unauthorize"]!= null)
                filterContext.HttpContext.Response.RedirectToRoute("General_notauthorized", true);
            filterContext.HttpContext.Response.RedirectToRoute("General_login", true);
            filterContext.HttpContext.Response.End();
            //filterContext.HttpContext.Response.StatusCode = 302;
            //Response.StatusCode = 404;
            //else do normal process
            base.HandleUnauthorizedRequest(filterContext);
        }
    }

}