﻿using ACAccess.Authorization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.SessionState;

namespace ACClient.Helpers.ActionFilters
{

    public class HasAuthAttribute : ActionFilterAttribute
    {
        public string[] _sessions;

        public HasAuthAttribute(params string[] sessions)
        {
            this._sessions = sessions;
        }

        /// <summary>
        /// Antes de llamar a un API verificar si tienen una de las sessiones enviadas
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            HttpSessionState session = System.Web.HttpContext.Current.Session;
            bool has_perm = false;
            foreach(string se in this._sessions)
                if (session[se] != null)
                    has_perm = true;
            if(!has_perm)
                throw new HttpException(401, "No tiene accesos a esta pagina");

        }
    }

    public class HasModuleAttribute : ActionFilterAttribute
    {
        private Module module_obj = new Module();
        private string[] _modules;
        public HasModuleAttribute(params string[] modules)
        {
            this._modules = modules;
        }
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            HttpSessionState session = System.Web.HttpContext.Current.Session;
            ACAuth auth_obj;
            int has_modules = 0;
            // para estudiantes
            if (session["Auth"] != null) { 
                auth_obj = (ACAuth)session["Auth"];
                has_modules += auth_obj.module_list.Where(f => _modules.Contains(f.name)).Count();
            }
            if (session["AuthBackoffice"] != null) { 
                auth_obj = (ACAuth)session["AuthBackoffice"];
                has_modules += auth_obj.module_list.Where(f => _modules.Contains(f.name)).Count();
            }
            if (session["AuthParent"] != null)
            {
                if(session["AuthChild"] != null)
                    auth_obj = (ACAuth)session["AuthChild"];
                else
                    auth_obj = (ACAuth)session["Auth"];

                has_modules += auth_obj.module_list.Where(f => _modules.Contains(f.name)).Count();
            }

            if (has_modules <= 0)
                throw new HttpException(401, "No tiene accesos a este modulo.");
            
        }
    }

    public class CacheFilterAttribute: ActionFilterAttribute
    {
        public int TimeDuration { get; set; }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            actionExecutedContext.Response.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue
            {
                MaxAge = TimeSpan.FromMinutes(TimeDuration),
                MustRevalidate = true,
                Public = true
            };
        }
    }

}