﻿using System;
using System.Linq;
using System.Web;
using System.IO;

namespace ACClient.Helpers
{
	public class Upload
	{
		private HttpPostedFileBase file { get; set; }
		private string bucket { get; set; }
		public string filename { get; set; }
		private string path {get; set;}
		private string client {get;set;}
		private DateTime time { get; set; }
		private string extension { get; set; }
		public string timeText { get {
			return this.time.ToString("yyyyMMddHHmmss");
		} }
		public Upload(string _bucket, string _filename)
		{
			this.bucket = _bucket;
			this.filename = _filename;
		}
		public Upload(HttpPostedFileBase _file, string _filename, string _bucket, string _client)
		{
			this.file = _file;
			if (this.file == null)
			{

			}
			else
			{
				this.bucket = _bucket;
				this.client = _client;
				this.time = DateTime.Now;
				switch (this.file.ContentType)
				{
					case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
						this.extension = "docx";
						break;
					case "application/pdf":
						this.extension = "pdf";
						break;
					case "application/vnd.oasis.opendocument.text":
						this.extension = "odt";
						break;
					case "image/jpeg":
						this.extension = "jpeg";
						break;
					case "image/png":
						this.extension = "png";
						break;
					default:
						this.extension = "undefined";
						break;
				}

				this.filename = string.Format("{0}-{1}.{2}", _filename, this.timeText, this.extension);
			}
		}
		public bool isValid()
		{
			string[] extensiones = new string[] {
				"application/vnd.openxmlformats-officedocument.wordprocessingml.document", 
				"application/pdf", 
				"application/vnd.oasis.opendocument.text",
				"image/png","image/jpeg"};
			return extensiones.Contains(this.file.ContentType);
		}
		public string save()
		{
			this.path = string.Format("{0}{1}", this.bucket, this.filename);
			//try
			//{
			this.file.SaveAs(this.path);
			return this.path;
			//}
				//catch (Exception e)
			//{
				//return null;
			//}
		}
		public bool delete()
		{
		   if((File.Exists(string.Format("{0}{1}", this.bucket, this.filename))))
		   {
			//try
			//{
			File.Delete(string.Format("{0}{1}", this.bucket, this.filename));
			//}
			//catch (Exception e)
			//{

			//				}			   
		   }
		   if ((File.Exists(string.Format("{0}{1}", this.bucket, this.filename))))
		   {
			   return false;
		   }
		   else
		   {
			   return true;
		   }
		}
		public bool exists()
		{
			string [] files = null;
				//try
				//{
				files = Directory.GetFiles(this.bucket, this.filename);
				//}
					//catch (Exception e)
				//{

					//				}

				if (files != null && files.Length > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
		}
		public string getServe()
		{
			string[] files = Directory.GetFiles(this.bucket, this.filename);
			if (files.Count() > 0)
			{
				return files.First().ToString();
			}
			else
			{
				return null;
			}
		}
		public string getContentType()
		{
			string contentType = "";
			if (this.filename.Split('.').Count() <= 1)
			{
				return "undefined";
			}
				switch (this.filename.Split('.').Last())
				{
					case "docx":
						contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
						break;
					case "pdf":
						contentType = "application/pdf";
						break;
					case "odt":
						contentType = "application/vnd.oasis.opendocument.text";
						break;
					case "jpeg":
						contentType = "image/jpeg";
						break;
					case "png":
						contentType = "image/png";
						break;
					default:
						contentType = "undefined";
						break;
				}
			return contentType;
		}
		public string getClient()
		{
			return string.Format("{0}{1}", this.client, this.filename);
		}
	}
}