﻿using System;
using System.IO;

namespace ACClient.Helpers
{
    public class ImageBase64Upload
    {
        /// <summary>
        /// Almacena una imagen, la cual está codificada en base64
        /// </summary>
        /// <param name="path">Ruta donde se almacenará la imagen. E: "~/Content/static/images/vuc/axis/"</param>
        /// <param name="base64">Imagen en base64</param>
        /// <returns></returns>
        public static string Save(string path, string base64)
        {
            var fileName = $"{Guid.NewGuid().ToString()}.jpeg";
            var filePath = Path.Combine(path, fileName);

            var bytes = Convert.FromBase64String(base64);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            using (var imageFile = new FileStream(filePath, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }
            return fileName;
        }
    }
}