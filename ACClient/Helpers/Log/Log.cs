﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACClient.Helpers.Log
{
	public class Log
	{
		public string msg { get; set; }
		public bool status { get; set; }
		public Log(string msg)
		{
			this.msg = msg;
		}
		public Log(string msg, bool status)
		{
			this.msg = msg;
			this.status = status;
		}
	}
}