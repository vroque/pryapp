﻿using System;
using System.IO;

namespace ACClient.Helpers
{
    public class PathFile
    {
        /// <summary>
        /// Almacena un archivo .zip codificado en base 64
        /// </summary>
        /// <param name="path">Ruta física donde se almacenará</param>
        /// <param name="base64">Base64 del archivo</param>
        /// <returns></returns>
        public static string Save(string path, string base64)
        {
            var fileName = $"{Guid.NewGuid().ToString()}.zip";
            var filePath = Path.Combine(path, fileName);

            var bytes = Convert.FromBase64String(base64);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            using (var imageFile = new FileStream(filePath, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }

            return fileName;
        }
    }
    
}