﻿using ACTools.Configuration;
using System.Web.Mvc;
using System.Web.Routing;

namespace ACClient
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var env = AppSettings.app_env;

            /* HOME ROUTE */
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                "General_inicio",
                "",
                new {controller = "Generals", action = "Index"},
                new[] {"ACClient.Controllers"}
            );
            routes.MapRoute(
                "General_notauthorized",
                "notauthorized/",
                new {controller = "Generals", action = "Notauthorized"},
                new[] {"ACClient.Controllers"}
            );
            routes.MapRoute(
                "General_sessionexpired",
                "adios/",
                new {controller = "Generals", action = "SessionExpired"},
                new[] {"ACClient.Controllers"}
            );
            routes.MapRoute(
                "Manteminiemto",
                "maintenance/",
                new {controller = "Generals", action = "Maintenance"},
                new[] {"ACClient.Controllers"}
            );

            #region Authentication

            routes.MapRoute(
                "General_set_authentication",
                "ingresar/auth/",
                new {controller = "Generals", action = "Login_authentication"},
                new[] {"ACClient.Controllers"}
            );

            #endregion

            #region OAuth2

            routes.MapRoute(
                "General_set_auth_oauth2_json",
                "general/set/auth/oauth2/json/",
                new {controller = "Generals", action = "Login_oauth2"},
                //new { distrito = "" }
                new[] {"ACClient.Controllers"}
            );
            routes.MapRoute(
                "General_unset_auth_oauth2_one_json",
                "frontdesk/general/unset/auth/oauth2/json/",
                new {controller = "Generals", action = "Logout_oauth2"},
                //new { distrito = "" }
                new[] {"ACClient.Controllers"}
            );
            routes.MapRoute(
                "General_unset_auth_oauth2_two_json",
                "backoffice/general/unset/auth/oauth2/json/",
                new {controller = "Generals", action = "Logout_oauth2"},
                //new { distrito = "" }
                new[] {"ACClient.Controllers"}
            );

            #endregion

            switch (env)
            {
                case ("dev"):
                    routes.MapRoute(
                        "General_login", "ingresar/",
                        new {controller = "Generals", action = "Login_debug"},
                        new[] {"ACClient.Controllers"}
                    );
                    routes.MapRoute(
                        "General_logout", "salir/",
                        new {controller = "Generals", action = "Logout_debug"},
                        new[] {"ACClient.Controllers"}
                    );
                    break;
                case ("test"):
                    //login para usuario final en test
                    routes.MapRoute(
                        "General_login", "ingresar/",
                        new {controller = "Generals", action = "Login_production"},
                        new[] {"ACClient.Controllers"}
                    );
                    routes.MapRoute(
                        "General_logout", "salir/",
                        new {controller = "Generals", action = "Logout_production"},
                        new[] {"ACClient.Controllers"}
                    );
                    //login para dev en test
                    routes.MapRoute(
                        "test_login", "test/ingresar/",
                        new {controller = "Generals", action = "Login_debug"},
                        new[] {"ACClient.Controllers"}
                    );
                    routes.MapRoute(
                        "test_logout", "test/salir/",
                        new {controller = "Generals", action = "Logout_debug"},
                        new[] {"ACClient.Controllers"}
                    );
                    break;
                case ("prod"):
                    routes.MapRoute(
                        "General_login", "ingresar/",
                        new {controller = "Generals", action = "Login_production"},
                        new[] {"ACClient.Controllers"}
                    );
                    routes.MapRoute(
                        "General_logout", "salir/",
                        new {controller = "Generals", action = "Logout_production"},
                        new[] {"ACClient.Controllers"}
                    );
                    break;
                default:
                    break;
            }

            routes.MapRoute(
                "404",
                "404/",
                new {controller = "Generals", action = "NoFound"},
                new[] {"ACClient.Controllers"}
            );

            /* END ROUTE */
        }
    }
}