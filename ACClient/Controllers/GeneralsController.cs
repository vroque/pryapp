﻿using ACAccess.Authentication;
using ACAccess.Authentication.Login;
using ACAccess.Authorization;
using ACClient.Helpers.ActionFilters;
using ACClient.Helpers.CustomRequest;
using System.Collections.Generic;
using System.Web.Mvc;
using ACTools.Configuration;
using _app = ACTools.Constants.AppConstants;

namespace ACClient.Controllers
{
    public class GeneralsController : Controller
    {
        string env = AppSettings.app_env;
        /// <summary>
        /// Controlador de la pagina de inicio principal
        /// -> Verifica la sesión y segun esto deriva a 
        ///    -> Backoffice
        ///    -> Frontdesk
        ///    -> Inicio de sesion
        /// </summary>
        /// <param name="seccion"></param>
        /// <returns></returns>
		[HasSessionOrSession("Auth", "AuthBackoffice", "AuthParent")]
        public ActionResult Index(string seccion = "solicitudes")
        {
            ACAuth auth_obj = (ACAuth)Session["Auth"];
            ACAuth authbackoffice_obj = (ACAuth)Session["AuthBackoffice"];
            ACAuth authParent_obj = (ACAuth)Session["AuthParent"];
            if (authbackoffice_obj != null)
            {
                //if (authbackoffice_obj.module_list.Count > 0)
                return RedirectToRoute("backoffice");
                //return RedirectToRoute("General_notauthorized");
            }
            else if(authParent_obj != null && auth_obj != null)
                return RedirectToRoute("parent");
            else if (authParent_obj != null)
                return RedirectToRoute("parent");
            
            else if (auth_obj != null)
                return RedirectToRoute("frontdesk");
            

            return RedirectToRoute("General_notauthorized");
        }

        /// <summary>
        /// Controlador para pagina informativa si inicio sesion pero no tiene 
        /// ningun modulo asignado
        /// </summary>
        /// <returns></returns>
		public ActionResult Notauthorized()
        {
            Session.Remove("Auth");
            Session.Remove("AuthBackoffice");
            Session.Remove("AuthPArent");
            Session.RemoveAll();
            return View();
        }

        /// <summary>
        /// Page for session expire timeout page
        /// </summary>
        /// <returns></returns>
        public ActionResult SessionExpired()
        {
            Session.Remove("Auth");
            Session.Remove("AuthBackoffice");
            Session.Remove("AuthPArent");
            Session.RemoveAll();
            return View();
        }

        #region Maintenance

        /// <summary>
        /// Maintenance mode
        /// </summary>
        /// <returns></returns>
        public ActionResult Maintenance()
        {
            Session.Remove("Auth");
            Session.Remove("AuthBackoffice");
            Session.Remove("AuthParent");
            Session.RemoveAll();
            return View();
        }

        #endregion Maintenance

        #region production
        /// <summary>
        /// Pagina de incio de sesion para produccion (OAUTH)
        /// </summary>
        /// <returns></returns>
        public ActionResult Login_production()
        {
            string redirect = Request.QueryString["entonces"];
            string oriented = Request.QueryString["oriented"];
            if (redirect != null && redirect.Length > 0)
                ViewBag.redirect = redirect;
            else
                ViewBag.redirect = "";
            if (oriented != null && oriented.Length > 0)
            {
                if (Session["oriented"] == null)
                {
                    Session["oriented"] = oriented;
                }
            }

            if (Session["just_close_session"] != null)
            {
                ViewBag.just_close_session = true;
                Session.Remove("just_close_session");
            }

            return View("~/Views/Generals/Login_production.cshtml");
        }

        /// <summary>
        /// Pagina de cierre de sesion
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout_production()
        {
            ACAuth auth_obj = (ACAuth)Session["Auth"];
            ACAuth authbackoffice_obj = (ACAuth)Session["AuthBackoffice"];
            string token = null;
            if (auth_obj != null)
            {
                token = auth_obj.token;
            }
            else if (authbackoffice_obj != null)
            {
                token = authbackoffice_obj.token;
            }
            // cerrar sesion en google
            Requests r = new Requests(string.Format("https://accounts.google.com/o/oauth2/revoke?token={0}", token));
            r.GetResponse();
            if (r.Status == "OK")
            {
                Session.Remove("Auth");
                Session.Remove("AuthBackoffice");
                Session.Remove("AuthParent");
                Session.Remove("AuthChild");
                Session.RemoveAll();
            }
            string redirect = Request.QueryString["entonces"];
            if (redirect != null && redirect.Length > 0)
            {
                return Redirect(redirect);
            }
            else
            {
                return RedirectToRoute("General_sessionexpired");
            }
        }
        #endregion

        #region OAuth2
        /// <summary>
        /// Inicia sesion con el token de google (AJAX)
        /// </summary>
        /// <param name="collection">coleccion con el token</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login_oauth2(FormCollection collection)
        {
            Session.RemoveAll();
            string redirect = Request.QueryString["entonces"];
            string token = collection["access_token"];
            if (token != null && token.Length > 0)
            {
                OAuthLogin oauth = new OAuthLogin();
                // Se ignora email ya que se consigue desde el token
                ACUser user = oauth.login("", token);
                if (user != null)
                {
                    ACAuth auth_obj = ACAuth.getAsValid(user);
                    Dictionary<string, object> doReturn = new Dictionary<string, object>();
                    if (auth_obj != null)
                    {
                        if (user.type.Contains(_app.TYPE_EXECUTIVE))
                        {
                            Session.Add("AuthBackoffice", auth_obj);
                        }
                        if (user.type.Contains(_app.TYPE_STUDENT))
                        {
                            Session.Add("Auth", auth_obj);
                        }
                        if (user.type.Contains(_app.TYPE_PARENT))
                        {
                            Session.Add("AuthParent", auth_obj);
                        }
                           
                        doReturn.Add("status", true);
                        doReturn.Add("types", user.type);
                        return Json(doReturn, JsonRequestBehavior.AllowGet);
                    }
                }

                //    if (auth_obj != null)
                //    {
                //        if (user.type.Contains(_app.TYPE_STUDENT))
                //            Session.Add("Auth", auth_obj);
                //        else if (user.type.Contains(_app.TYPE_EXECUTIVE))
                //            Session.Add("AuthBackoffice", auth_obj);
                //        doReturn.Add("status", true);
                //        return Json(doReturn, JsonRequestBehavior.AllowGet);
                //    }

            }
            Response.StatusCode = 405;
            return Json(new ACClient.Helpers.Log.Log("Vaya! Algo salió mal. Puedes intentarlo en otro momento."), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Cierra sesion
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout_oauth2()
        {
            Session.Remove("Auth");
            Session.Remove("AuthBackoffice");
            Session.RemoveAll();
            Session["just_close_session"] = true;
            return new HttpStatusCodeResult(200);
        }
        #endregion

        #region debug
        public ActionResult Logout_debug()
        {
            Session.Remove("Auth");
            Session.Remove("AuthBackoffice");
            Session.Remove("AuthPArent");
            Session.RemoveAll();
            string redirect = Request.QueryString["redirect"];
            if (redirect != null && redirect.Length > 0)
            {
                return Redirect(redirect);
            }
            else
            {
                return RedirectToRoute("General_sessionexpired");
            }
        }
        public ActionResult Login_debug()
        {
            string redirect = Request.QueryString["entonces"];
            string oriented = Request.QueryString["oriented"];
            if (redirect != null && redirect.Length > 0)
            {
                ViewBag.redirect = redirect;
            }

            if (oriented != null && oriented.Length > 0)
            {
                if (Session["oriented"] == null)
                {
                    Session["oriented"] = oriented;
                }
            }
            return View("~/Views/Generals/Login_debug.cshtml");
        }
        #endregion

        #region Authentication
        [HttpPost]
        public ActionResult Login_authentication(FormCollection collection)
        {
            string redirect = Request.QueryString["entonces"];
            if (collection.Get("usuario").Trim() != "" && env != "prod") //&& collection.Get("password").Trim() != ""
            {
                BridgeLogin bridge = new BridgeLogin();
                ACUser user = bridge.login(collection.Get("usuario").Trim(), "");
                // Se ignora email ya que se consigue desde el token
                if (user != null)
                {
                    ACAuth auth_obj = ACAuth.getAsValid(user);
                    Dictionary<string, object> doReturn = new Dictionary<string, object>();
                    if (auth_obj != null)
                    {
                        if(user.type.Contains(_app.TYPE_EXECUTIVE))
                        {
                            Session.Add("AuthBackoffice", auth_obj);
                            return RedirectToRoute("backoffice");
                        }
                        if (user.type.Contains(_app.TYPE_STUDENT) && user.type.Contains(_app.TYPE_PARENT)==false)
                        {
                            Session.Add("Auth", auth_obj);
                            if (redirect != null && redirect.Length > 0 && redirect.Contains("frontdesk"))
                                return Redirect(redirect);
                            return RedirectToRoute("frontdesk");
                        }
                       else if(user.type.Contains(_app.TYPE_STUDENT) && user.type.Contains(_app.TYPE_PARENT))
                        {
                            Session.Add("Auth", auth_obj);
                            Session.Add("AuthParent", auth_obj);
                            return RedirectToRoute("parent");
                        }
                        else
                        {
                            Session.Add("AuthParent", auth_obj);
                            return RedirectToRoute("parent");
                        }
                    }
                }
            }
            return RedirectToRoute("General_notauthorized");
        }
        #endregion
    }
}